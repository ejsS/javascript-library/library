/*
* Copyright (C) 2020 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Tools for simulations
 * @module core
 */

var EJSS_CORE = EJSS_CORE || {};

/**
 * Tools
 * @class Tools 
 * @constructor  
 */
EJSS_CORE.Tools = {
    showInputDialog : 		EJSS_INTERFACE.BoxPanel.showInputDialog,
    showOkDialog :			EJSS_INTERFACE.BoxPanel.showOkDialog,
    showOkCancelDialog : 	EJSS_INTERFACE.BoxPanel.showOkCancelDialog,
    
    downloadText : 			EJSS_TOOLS.File.downloadText,
    uploadText : 			function(action) { EJSS_TOOLS.File.uploadText(_model,action); },
 
	isMobile: function() { return (navigator===undefined) ? false : navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|Opera Mini|IEMobile/i); },	
	isIOS: function() { return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream; },
	isAndroid: function() { return /Android|android/i.test(navigator.userAgent); },
	isIPad: function() { return /iPad/.test(navigator.userAgent) && !window.MSStream; },
	isIPhone: function() { return /iPhone|iPod/.test(navigator.userAgent) && !window.MSStream; },
	isIOSapp: function() { return (typeof parent != 'undefined' && typeof parent.device != 'undefined' && parent.device.platform == "iOS"); },
	isAndroidapp: function() { return (typeof parent != 'undefined' && typeof parent.device != 'undefined' && parent.device.platform == "Android"); },
	isApp: function() { return _iOSapp || _Androidapp; },
	
	accelerometer: EJSS_HARDWARE.accelerometer,
	gyroscope: EJSS_HARDWARE.gyroscope,
	orientation: EJSS_HARDWARE.orientation,
	ambientLight: EJSS_HARDWARE.ambientLight,
	magnometer: EJSS_HARDWARE.magnometer,
	audio: EJSS_HARDWARE.audio
	
};
