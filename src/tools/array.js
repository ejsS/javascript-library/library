/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Provides tools for main classes.
 * @module tools  
 */

var EJSS_TOOLS = EJSS_TOOLS || {};

/**
 * Array
 * @class Array 
 * @constructor  
 */

/**
 * Adds an element to an array
 * @method addToArray
 */
EJSS_TOOLS.addToArray = function (array,object,position) {
  EJSS_TOOLS.removeFromArray(array,object); // first remove it
  if (position>=0) array.splice(position,0,object);
  else array.push(object);
};

/**
 * Element position in an array
 * @method arrayObjectIndexOf
 */
EJSS_TOOLS.arrayObjectIndexOf = function(array, object) {
  return array.indexOf(object);
};

/**
 * Removes an element from an array
 * @method removeFromArray
 */
EJSS_TOOLS.removeFromArray = function(array, object) {
  var index = array.indexOf(object);
  if (index>=0) array.splice(index,1);
};

/**
 * Compare two array
 * @method compareArrays
 */
EJSS_TOOLS.compareArrays = function(array1, array2) {
    if (!array1 || !array2)
        return false;

	// check arrays
	if (!array1 instanceof Array || !array2 instanceof Array) {
		return (array1 == array2);
	}

    // compare lengths - can save a lot of time
    if (array1.length != array2.length)
        return false;

    for (var i = 0; i < array1.length; i++) {
        // Check if we have nested arrays
        if (array1[i] instanceof Array && array2[i] instanceof Array) {
            // recurse into the nested arrays
            if (!EJSS_TOOLS.compareArrays(array1[i],array2[i]))
                return false;
        }
        else if (array1[i] != array2[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};
