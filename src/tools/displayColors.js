/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 */

/**
 * Framework for 2D drawing 
 */

var EJSS_TOOLS = EJSS_TOOLS || {};

// Taken from the OSP core DisplayColors.java class

EJSS_TOOLS.DisplayColors = {
  phaseColors : {},
  lineColors : [ 
    "rgb(255,0,0)",   // red 
    "rgb(0,240,0)",   // darker green
    "rgb(0,0,255)",   // blue
    "rgb(240,240,0)", // darker yellow
    "rgb(0,240,240)", // darker cyan
    "rgb(255,0,255)"  // magenta
  ],
  markerColors : [ 
    "rgb(255,0,0)",   // red 
    "rgb(0,240,0)",   // darker green
    "rgb(0,0,255)",   // blue
    "rgb(240,240,0)", // darker yellow
    "rgb(0,240,240)", // darker cyan
    "rgb(255,0,255)"  // magenta
  ],
  arrayColors : { 
    "black": [0,0,0],
    "blue": [0,0,255],   		// blue
    "brown": [165,42,42],
    "cyan": [0,255,255],
    "darkblue": [0,0,160],	
    "darkcyan": [0,240,240], 	// darker cyan
    "darkgray": [64,64,64],   	// darker gray
    "darkgreen": [0,240,0],   	// darker green
    "darkgrey": [64,64,64],   	// darker gray
    "darkyellow": [240,240,0], 	// darker yellow
    "gray": [125,125,125],    
    "green": [0,255,0],
    "grey": [125,125,125],
    "lightblue": [173,216,230],
    "lightgray": [192,192,192],
    "lightgrey": [192,192,192],
    "magenta": [255,0,255],  	// magenta
    "maroon": [128,0,0],
    "olive": [128,128,0],
    "orange": [255,165,0],
    "pink": [255,175,175],
    "purple": [128,0,128],
    "red": [255,0,0],   		// red 
    "silver": [192,192,192],
    "white": [255,255,255],
    "yellow": [255,255,0]
  },

  /**
   * Gets a line color that matches the index.
   * @method getLineColor
   * @param index int
   * @return String
   */
  getLineColor : function(index) {
    var color = EJSS_TOOLS.DisplayColors.lineColors[index];
    if (color===undefined) {
      var h = ((index*360)/12)%360;
      color = "hsl("+h+",100%,50%)";
//      console.log("Added lineColor["+index+"] = "+color);
      EJSS_TOOLS.DisplayColors.lineColors[index] = color;
    }
    return color;
  },

  /**
   * Gets a marker color that matches the index.
   * @method getMarkerColor
   * @param index int
   * @return String
   */
  getMarkerColor : function(index) {
    var color = EJSS_TOOLS.DisplayColors.markerColors[index];
    if (color===undefined) {
      var h = ((index*360)/12)%360;
      color = "hsl("+h+",100%,100%)";
      EJSS_TOOLS.DisplayColors.markerColors[index] = color;
    }
    return color;
  },

  /**
   * Gets an array color that matches the color string.
   * @method getArrayColor
   * @param index string
   * @return array
   */
  getArrayColor : function(str) {
  	str = str.toLowerCase();
  	if(str.indexOf("rgb") == 0) { // rgb(r,g,b)
  		var color = str.substring(4, str.length-1).replace(/ /g, '').split(',');
  	} else if(str.indexOf("#") == 0) { // #xxxxxx
 		str = str.slice(1); //Remove the '#' char - if there is one.
		str = str.toUpperCase();
		var hex_alphabets = "0123456789ABCDEF";
		var color = new Array(3);
		var k = 0;
		var int1,int2;
		for(var i=0;i<6;i+=2) {
			int1 = hex_alphabets.indexOf(str.charAt(i));
			int2 = hex_alphabets.indexOf(str.charAt(i+1)); 
			color[k] = (int1 * 16) + int2;
			k++;
		}  
	}	  		
  	else { // black, red, ...
	    var color = EJSS_TOOLS.DisplayColors.arrayColors[str];
	    if (color === undefined) {
	      color = EJSS_TOOLS.DisplayColors.arrayColors["black"];
	    }
	}	
	
    return [color[0]/255, color[1]/255, color[2]/255];
  }

};
