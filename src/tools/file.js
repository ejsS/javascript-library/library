/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Provides tools for main classes.
 * @module tools  
 */

var EJSS_TOOLS = EJSS_TOOLS || {};

/**
 * A utility with Math functions.
 * @class Mathematics 
 * @constructor  
 */

EJSS_TOOLS.File = {
  JSLoaded : "", // list of JS files already added
  SingletonDownload :  document.createElement("input"), 
  SingletonUpload :  document.createElement("input"),

  /**
   * Dynamically load a JS file 
   * @method loadJSfile
   * @return void
   */
  loadJSfile : function(filename, scriptLoaded) {
//    console.log("Wants to load "+filename);
    if (EJSS_TOOLS.File.JSLoaded.indexOf("["+filename+"]")!=-1) { // already added
      console.log("File already loaded "+filename);
      scriptLoaded();
      return;
    }
//    console.log("File loaded "+filename);
    EJSS_TOOLS.File.JSLoaded+="["+filename+"]" //List of files added in the form "[filename1],[filename2],etc"
    var fileref=document.createElement('script')
    fileref.setAttribute("type","text/javascript")
    fileref.setAttribute("src", filename);
    if (typeof scriptLoaded!="undefined") fileref.onload=scriptLoaded;
    document.getElementsByTagName("head")[0].appendChild(fileref);
  },
  
  plainName : function(filename) {
    var index = filename.lastIndexOf("/");
    if (index>=0) filename = filename.substring(index+1);
    index = filename.indexOf(".");
    if (index>0) filename = filename.substring(0,index);
    return filename;
  },
  

  downloadText : function(filename, content) {
	  var blob = new Blob([content]);
	  EJSS_TOOLS.File.SingletonDownload = document.createElement('a');
	  EJSS_TOOLS.File.SingletonDownload.href = window.URL.createObjectURL(blob); //"data:application/text;charset=utf-8," + text);
	  EJSS_TOOLS.File.SingletonDownload.download = filename;
	  EJSS_TOOLS.File.SingletonDownload.click();
	  window.URL.revokeObjectURL(blob);
  },

  uploadText : function(model,action) {
	  EJSS_TOOLS.File.SingletonUpload.type = 'file';
	  EJSS_TOOLS.File.SingletonUpload.accept = 'text/*';
	  EJSS_TOOLS.File.SingletonUpload.multiple = false;
	  EJSS_TOOLS.File.SingletonUpload.onchange = function(event) {
	    var reader = new FileReader();
	    reader.onload = function(progressEvent){
	      var parameter =  this.result;
	      model.performTaskSafely(function() { action(parameter); });
	      EJSS_TOOLS.File.SingletonUpload.value = "";
	    };
	    reader.readAsText(event.target.files[0]);
	  };
	  EJSS_TOOLS.File.SingletonUpload.click();
  },
  
  loadScript: function(scriptsrc) {
    var s = document.createElement('script');
    s.type='text/javascript';
    s.src = scriptsrc;
    document.getElementsByTagName('head')[0].appendChild(s);    
  },  

  loadJSONFile: function(url, callback) {     
	var xmlhttp = new XMLHttpRequest();	
	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	        var myArr = JSON.parse(xmlhttp.responseText);
	        callback(myArr);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
  },

  loadCSVFile: function(url, callback) {     
	var xmlhttp = new XMLHttpRequest();	
	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	// CSV parse
    		var allTextLines = xmlhttp.responseText.split(/\r\n|\n/);    		
    		var lines = [];
    		for (var i=1; i<allTextLines.length; i++) {
        		var data = allTextLines[i].split(',');
            	var tarr = [];
            	for (var j=0; j<data.length; j++) {
                	tarr.push(data[j]);
            	}
            	lines.push(tarr);
        	}
	        callback(lines);
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
  },

  requestShortUrl: function(json, callback) {
    var encoded = escape("http://urlecho.appspot.com/echo?status=200&Content-Type=text/plain&body=" + json);    

	// create ref to callback
	EJSS_TOOLS.File.CALLBACK = callback;
  	EJSS_TOOLS.File.callFunction = function(data) {
		EJSS_TOOLS.File.CALLBACK(data.shortUrl);  	
  	}

	// callback
	if(document.getElementById("__rtShortUrl") == null) {
	    var scb = document.createElement('script');
	    scb.type = 'text/javascript';
	    scb.id = "__rtShortUrl"
	    scb.innerHTML = "function rtShortUrl(data) { EJSS_TOOLS.File.callFunction(data); }"; 
	    document.getElementsByTagName('head')[0].appendChild(scb);    
	}
	
  	var s = document.getElementById("_requestShortUrl");
  	if(s != null) document.getElementsByTagName('head')[0].removeChild(s);
  	 
	s = document.createElement('script');
	s.id = "_requestShortUrl";
	s.type='text/javascript';
    s.src = "http://b1t.co/Site/api/External/MakeUrlWithGet?callback=rtShortUrl&url=" + encoded;
    document.getElementsByTagName('head')[0].appendChild(s);    
  }
  
};




