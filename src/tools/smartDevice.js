function _smartDevice(ws,data){
	this.websocket = ws;
	this.loadMetadata(data);
	return this;
}
  _smartDevice.prototype = {
	metadata: "",
	apis: [],
	models: "",
	modelsTemplate: [],
	apisFn: [],
	//modelsFn:[],
	
	loadMetadata: function(data){
		this.metadata = JSON.parse(data);
	},
	
	loadModels: function(){
		this.models = this.metadata.models;
		//this.loadTemplates();
	},
	
	loadApis: function(){
		this.apis = this.metadata.apis;
	},
	
	getModelTemplate: function(name){
		if(this.models.hasOwnProperty(name)){
			var model = JSON.parse(JSON.stringify(this.models[name]));
			var name = model.id;
			var properties = model.properties;
			return model;
		}else{
			writeToScreen('<span style="color: red;">ERROR:</span> ' + name + " is not a key -> " + "Model Not found");
			return null;
		}
	},

	generateMethodCall: function(operationName){
		//by now this is only to make simple request, not ready to nested models request
		for (var i = 0; i<this.apis.length; i++){
			var api = this.apis[i];
			var operations = api.operations;
			//console.log("API: " + JSON.stringify(api));
			//console.log("First Op: " + JSON.stringify(operations[0]));
			//return operations[0];
			for (var j = 0; j<operations.length; j++){
				var op = operations[j];
				//console.log("nickNames: " + operations[0].nickname);
				//console.log("parameters type : " + parameters.type);
				if (op.nickname == operationName || operationName == undefined){
					//By now we have only one parameter, which is a JSON message
					var parameters = op.parameters[0];
					//console.log("Model to Search: " + parameters.type);
					var usingModel = this.getModelTemplate(parameters.type);
					//console.log("parameters type : " + parameters.type);
					var params = usingModel.properties;	
					var required;
					if(usingModel.hasOwnProperty("required"))	required = usingModel.required;
					/*Esta forma de rellenar el cuerpo de la llamada y los parametros no me convence
					* Seguramente se pueda utilizar stringuify para mejorarlo, de esa forma, sería un 
					* json puro rellenable, y menos problemas para las referencias anidadas
					*/
					var jsonTosend = "{";var paramToSend = [];var notFirstTime = false;
					for(var par in params){
						if (notFirstTime)	{jsonTosend += ",";}
						else 				notFirstTime = true;
						jsonTosend += "'"+ par + "' : " + par;
						paramToSend.push(par);
					}
					jsonTosend += "}";
					var body =  "{";
					if (required != undefined){
						for(var req in required){
							//Hara falta añadir una comprobacion de tipos, de esta forma garantizamos
							//que funcione correctamente todo! Todas las $ref serán entonces objectos JSON
							body += "if( "+ required[req] +" == undefined)  { \n"+ 
							"console.log('Required parameter, "+required[req]+" : must be an input'); \n" + 
							"return null;} \n";
						}
					}
					body += "this.websocket.send(JSON.stringify("+ jsonTosend +"))";
					body += "}";
					//this.apisFn[op.nickname] = new Function(paramToSend,body);
					this[op.nickname] = new Function(paramToSend,body);
				}
			}
		}
		return true;
		
	},
	
	generateModelFilling: function(modelName){ 
		for (var mod in this.models){
			if (mod == modelName || modelName == undefined){
				//console.log("model : " + mod);
				var usingModel = this.models[mod];
				var params = usingModel.properties;	
				var required;
				if(usingModel.hasOwnProperty("required"))	required = usingModel.required;
				var paramToSend = [];
				var body = "";
				body += "{\n";	//Function begins 
				body += "\t var json2send = this.getModelTemplate('"+ mod +"').properties;\n";
				for(var par in params){
					paramToSend.push(par);
					body += "\t if( "+ par +" != undefined)  { \n"; 
					body += "\t\t json2send['"+ par +"'] = "+ par +";  \n";
					body += "\t }else{\n";
					body += "\t\t delete json2send."+ par +";\n";
					body += "\t }\n";
				}
				if (required != undefined){
					for(var req in required){
						body += "\t if( "+ req +" == undefined)  { \n"+ 
							"\t\t console.log('Required parameter, "+req+" : must be an input'); \n" + 
							"\t\t return null; \n";
						//body += "\t }else{ \n";
						//body += "\t\t json2send['"+ required[req] +"'] = "+ required[req] +"  \n";
						body += "\t }\n";
					}
				}
				body += "\t return json2send;";
				body += "}";	//Function ends
				//console.log(body);
				//this.modelsFn[mod] = new Function(paramToSend,body);
				this["fill"+mod] = new Function(paramToSend,body);
			}	
			
		}
		return true;
	},
	
	bindFunctions: function(){
		// We can use this kind of code to bind the methods. Then we can put them into the 
		// EjsS code, and overwrite other vars
		//var getSensorMetadata = Function.prototype.call.bind(_smartDevice.apisFn["getSensorMetadata"],"")
	},
	
	searchMethod: function(methodName){
		for (var i = 0; i<this.apis.length; i++){
			var api = this.apis[i];
			var operations = api.operations;
			for (var j = 0; j<operations.length; j++){
				var op = operations[j];
				//console.log("nickNames: " + op.nickname);
				if (op.nickname == methodName){
					return op;
				}
			}
		}
	},
	
	isValidMessage: function(strMsg,type){
		// This method will obtain the info inside a JSON msg against the metadata info
		// Then identify the method
		//First of all we need to know if is the response to some-call
		//Responses contain the name of the caller.
		//Actuator response contains the updated value
		//Sensor  response contain the value requested
		var jsonMsg = strMsg;// = JSON.parse(strMsg);				
		if(jsonMsg.hasOwnProperty("method")){
			var metadataMethod = this.searchMethod(jsonMsg.method);
		}else if(type!=undefined){
			var metadataMethod = this.searchMethod(type);
		}else{
			return false;
		}
		var responseModel = this.getModelTemplate(metadataMethod.type);
		var validMsg = false;
		var req ="";
		
		for (var i = 0; i<responseModel.required.length;i++){
			req = responseModel.required[i];
			//console.log("Required: " + JSON.stringify(responseModel.required[i]));
			if (!jsonMsg.hasOwnProperty(req)){
				validMsg = false;	
				console.log("Error processinMessage : " + strMsg + 
				" -> Property : " + req + " is a required field");
				return validMsg;
			}else{
				validMsg = true;
			}
		}
		return validMsg;
	},
	
	extract: function(names,strMsg){
		//console.log("is a valid Msg : " + this.isValidMessage(strMsg));
		if (this.isValidMessage(strMsg)){
			var dataExtracted = {};
			for (var i = 0; i<names.length; i++){
				name = names[i];
				//console.log("name : " + name + " values: " + strMsg[name]);
				dataExtracted[name] = strMsg[name];
			}
			return dataExtracted;
		}
		return undefined;
	}
	
};
