/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Provides tools for main classes.
 * @module tools  
 */

var EJSS_TOOLS = EJSS_TOOLS || {};

/**
 * ConnectionWS
 * @class ConnectionWS 
 * @constructor  
 */
EJSS_TOOLS.ConnectionWS = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

};

EJSS_TOOLS.connectionWS = function (mUri) {
  var self = {};							// reference returned 
  var websocket;
  var messages = {};
  var open;

  self.getClass = function() {
  	return "ConnectionWS";
  }

  self.get = function(property) {  	
  	var result = messages[property];
  	if((typeof result != "undefined") || (result == null)) 
  		return result;
  }

  self.set = function(property, args) {
  	console.log("set:"+property+":"+args.toString());
  	console.log("set:is open="+open);
  	if(open) {
  		var message = property + " " + args.toString();
  		websocket.send(message);  	
  		self.reset();
  	}
  }
  
  self.reset = function() {
  	var f = websocket.onmessage;
  	websocket.onmessage = null;
  	websocket.onmessage = f;  	
  }
 
  // ----------------------------------------------------
  // Public methods
  // ----------------------------------------------------

  /**
   * Connect
   */
  function connect() {
  	try {
		websocket = new WebSocket(mUri);
		console.log('Connecting... (readyState ' + websocket.readyState + ')');
	}
	catch(exception) {
		console.log(exception);
	}		
  };
      
  /**
   * Listener for events
   */  
  function listener(event,handler) {  	
	  switch (event) {
	    case "onopen" : 
	      websocket.onopen = function (e) {
	        handler(e);
	      };
	      break;
	    case "onmessage" : 
	      websocket.onmessage = function (e) {
	        handler(e);
	      };
	      break;
	    case "onclose" : 
	      websocket.onclose = function (e) {
	        handler(e);
	      };
	      break;
	    case "onerror" : 
	      websocket.onerror = function (e) {
	        handler(e);
	      };
	      break;      
	    default :
	      break; 
	  }
  }
  
  // ----------------------------------------------------
  // Private methods
  // ----------------------------------------------------
  
  function messagehd (message) {
	// message format: [keyword] [data]
	var words = message.data.split(" ");
	
	// store last message for keyword
	messages[words[0]] = message.data.substring(words[0].length+1);
  }  

  function openhd (message) {
  	open = true;
  	console.log("Openhd Event: " + message.type + " - Message: " + message.data);
  }  

  function closehd (message) {
  	open = false;
  	console.log("Closehd Event: " + message.type + " - Message: " + message.data);
  }  
      
  function errorhd (message) {
  	open = false;
  	console.log("Errorhd Event: " + message.type + " - Message: " + message.data);
  }  
      
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  connect(); 
  listener("onopen", openhd);  
  listener("onclose", closehd);  
  listener("onerror", errorhd);  
  listener("onmessage", messagehd); 
  
  return self;
};

