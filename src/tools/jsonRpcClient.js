/**
 * JSON-RPC Client
 * author: Jesús Chacón <jcsombria@gmail.com>
 *
 */

var EJSS_TOOLS = EJSS_TOOLS || {};

/**
 * @class jsonrpcClient 
 * @constructor  
 */

EJSS_TOOLS.JsonrpcClient =  {
	methods : {
		connect: 'connect',
		open: 'open',
		getMetadata: 'getMetadata',
		getValue: 'getValue',
		setValue: 'setValue',
		close: 'close',
		disconnect: 'disconnect',
	}
};

EJSS_TOOLS.jsonrpcClient = function (mHost, mPort) {
	var self = {};
	var JsonrpcClient = EJSS_TOOLS.JsonrpcClient;
	var JsonrpcBuilder = EJSS_TOOLS.JsonrpcBuilder;
    var websocket;
    var mCallback = {};
    var mOpenCallback;
    var mCloseCallback;
    var mErrorCallback;
    var mMessageCallback;
	
	self.connect = function(callback) {
		return callMethod(JsonrpcClient.methods.connect, null, callback);
	};

	self.open = function(callback) {
		return callMethod(JsonrpcClient.methods.open, null, callback);
	};
	
	self.getValue = function(name, callback) {
		return callMethod(JsonrpcClient.methods.getValue, [name], callback);
	};

	self.setValue = function(data, callback) {
		return callMethod(JsonrpcClient.methods.setValue, data, callback);
	};

	self.getMetadata = function(callback) {
		return callMethod(JsonrpcClient.methods.getMetadata, null, callback);
	};

	self.disconnect = function(callback) {
		return callMethod(JsonrpcClient.methods.disconnect, null, callback);
	};

	self.close = function(callback) {
		return callMethod(JsonrpcClient.methods.close, null, callback);
	};

	function idGenerator(len) {
	    var text = "";
	    var charset = "abcdefghijklmnopqrstuvwxyz";
	    for(var i=0; i<len; i++) text+=charset.charAt(Math.floor(Math.random()*charset.length));
	    return text;
	};

	function callMethod (method, params, callback, _id) {
		var id = (_id)? _id:idGenerator(12);
		if(callback) mCallback[id] = callback;
		var request = JsonrpcBuilder.request(method, params, id);
		websocket.send(JSON.stringify(request));
	}; 
	
  	self.init = function(opencb,messcb,errorcb,closecb) {
  		try {
  			mOpenCallback = opencb;
  			mErrorCallback = errorcb;
  			mCloseCallback = closecb;
  			mMessageCallback = messcb;
  			
  			var uri = "ws://" + mHost;
  			if(mPort) uri += ":" + mPort;
			websocket = new WebSocket(uri);
			addListener();
			console.log('Connecting... (readyState ' + websocket.readyState + ')');
		}
		catch(exception) {
			console.log(exception);
		}		
  	};
  	
  	self.finish = function() {
  		try {
  			websocket.close();
  		}
		catch(exception) {
			console.log(exception);
		}		
  	};
  	
  	
   function addListener() {
    	websocket.onopen = function (message) {
    		if(mOpenCallback) mOpenCallback(message);
       		console.log("Openhd Event: " + message.type + " - Message: " + message.data);
      	};
      	websocket.onmessage = function (message) {
       		  var data = JSON.parse(message.data);
       		/*
       		if (counter%20==0) {
       		  console.log("Event On Message: " + message.type + " - Message: " + JSON.stringify(message));
       		  console.log("mCallback: " + JSON.stringify(mCallbackObject));
       		  console.log("data: " + JSON.stringify(data));
		      console.log ("MeCall["+data.id+"] = "+ mCallbackObject[data.id]);
       		  console.log("self: " + (!!self));
       		  }
       		counter++;
       		*/
    		if(mMessageCallback) mMessageCallback(mCallback);
			if(data.id && mCallback[data.id]) mCallback[data.id](data.result);
      	};
      	websocket.onclose = function (emessage) {
  			if(mCloseCallback) mCloseCallback(message);
  			console.log("Closehd Event: " + message.type + " - Message: " + message.data);
      	};
      	websocket.onerror = function (message) {
      		if(mErrorCallback) mErrorCallback(message);
  			console.log("Errorhd Event: " + message.type + " - Message: " + message.data);
      	};	 		
  	};
  	
  	return self;
}
