/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_HARDWARE = EJSS_HARDWARE || {};

/* 
 * Browser-compatible orientation tools
 */
EJSS_HARDWARE.orientationTools = function (mCallback) {
	var self = {};
	var mIOSkind = (getMobileOperatingSystem()!=="Android");
	var mOrientation = 0;

	self.getOrientation = function() { return mOrientation; }
	
	function readOrientation() {
		var orientation;
		if (mIOSkind) { // Android does it otherwise
			if (window.orientation=="90") orientation = 90;
			else if (window.orientation=="0") orientation = 0;
			else if (window.orientation=="-90") orientation = -90;
			else orientation = 180;
		}
		else { // iOS behaviour
			if (window.orientation=="90") orientation = -90;
			else if (window.orientation=="0") orientation = 180;
			else if (window.orientation=="-90") orientation = 90;
			else orientation = 0;
		}
		return orientation;
	}
	
	function getMobileOperatingSystem() {
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;
		if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || 
				userAgent.match( /iPod/i ) ) return 'iOS';
		if( userAgent.match( /Android/i ) ) return 'Android';
		return 'unknown';
	};

	window.addEventListener("orientationchange", function() {
		mOrientation  = readOrientation();
		if (typeof mCallback != 'undefined') mCallback();
  	  }, false);

	mOrientation = readOrientation();
	
	return self;
};

/*
 * Browser-compatible accelerometer
 */

EJSS_HARDWARE.accelerometer = function () {
	var self = {};
	var mListeners = [];
	var mOrientationTools = EJSS_HARDWARE.orientationTools();
	var mAverageTime = 0;
	var mIsRunning = false;
	var mUseGranted = false;

	var mData = { x : 0, y:0, z:0, interval:0 , alpha: 0, beta: 0, gamma: 0};
	var mHistory = []; // Stores data taken, most recent data goes to [0] 

	function copyData(acceleration,rotation,data) {
		data.x = acceleration.x;
		data.y = acceleration.y;
		data.z = acceleration.z;
	    data.alpha = rotation.alpha;
	    data.beta  = rotation.beta;
        data.gamma = rotation.gamma;
        data.millisAgo = 0;
        return data;
	};
	
	function deviceMotionHandler(eventData) {
		// Grab the acceleration from the results
		//var acceleration = eventData.acceleration;
		var acceleration = eventData.accelerationIncludingGravity;
	    var rotation = eventData.rotationRate;
		var interval = eventData.interval;
		if (mAverageTime>0) {
	    	for (var j=0, numData=mHistory.length; j<numData; j++) {
	    		var data = mHistory[j];
	    		data.millisAgo += interval;
	    		if (data.millisAgo>mAverageTime) {
	    			mHistory.length = j;
	    			break;
	    		}
	    	}
	    	mHistory.unshift(copyData(acceleration,rotation,{}));
		}

		copyData(acceleration,rotation,mData);

		for (var i=0, n=mListeners.length; i<n; i++) {
			mListeners[i](mData);
		}
	};

	self.isPresent = function() { return (typeof(window.DeviceMotionEvent) !== "undefined"); };

	self.requiresUserPermission = function() { 
		if (!self.isPresent()) return false;
		return (typeof(window.DeviceMotionEvent.requestPermission) === "function");
	} 

	self.hasUserPermission = function() { return mUseGranted; }

	self.isReadyToUse = function() { return self.isPresent() && self.hasUserPermission(); }

	self.requestUserPermission = function(action) { // check sensor in device
		if (!self.isPresent()) {
			mUseGranted = true;
			return;
		}
		// get permission - check if is IOS 13 when page loads.
		if (typeof(window.DeviceMotionEvent.requestPermission) === "function") {
			window.DeviceMotionEvent.requestPermission().then( response => {
		    	if ( response == "granted" ) { mUseGranted = true; }
  				if (typeof(action) !== "undefined") action(mUseGranted);
		    })
		    .catch(console.error);
		} 
		else { // no permission needed
			mUseGranted = true;
		}
	}
	
//	if (self.isPresent()) self.requestUserPermission();
	
	self.start = function() {
		if (self.isReadyToUse()) {
			window.addEventListener('devicemotion', deviceMotionHandler, false);
			mIsRunning = true;
		}
	};

	self.stop = function() {
		if (self.isReadyToUse()) window.removeEventListener('devicemotion', deviceMotionHandler);
		mHistory = [];
		mData = { x : 0, y:0, z:0, interval:0 , alpha: 0, beta: 0, gamma: 0};
		mIsRunning = false;
	};

	self.isRunning = function() { return mIsRunning; };

	self.setAverageInterval = function (seconds) {
		mAverageTime = seconds;
		if (mAverageTime<=0) mHistory = [];
	};
	
	function averageData() {
		var data = { x : 0, y:0, z:0, interval:0 , alpha: 0, beta: 0, gamma: 0};
	    data.alpha = mData.alpha;
	    data.beta  = mData.beta;
        data.gamma = mData.gamma;
        var n = mHistory.length;
    	for (var i=0; i<n; i++) {
    		var historicalData = mHistory[i];
    		data.x += historicalData.x;
    		data.y += historicalData.y;
    		data.z += historicalData.z;
    	}
    	data.x /= n;
    	data.y /= n;
    	data.z /= n;
    	return data;
	};
	
	self.getDeviceData = function() { return (mAverageTime>0) ? averageData() : mData; };
	
	self.getViewData = function () {
		var data = (mAverageTime>0) ? averageData() : mData;
		switch (mOrientationTools.getOrientation()) {
		  case   0 : return { x: data.x, y: data.y, z: data.z, alpha: data.alpha, beta: data.beta, gamma: data.gamma }; 
		  case  90 : return { x:-data.y, y: data.x, z: data.z, alpha: data.alpha, beta: data.beta, gamma: data.gamma }; 
		  case -90 : return { x: data.y, y:-data.x, z: data.z, alpha: data.alpha, beta: data.beta, gamma: data.gamma }; 
		  default  : return { x:-data.x, y:-data.y, z: data.z, alpha: data.alpha, beta: data.beta, gamma: data.gamma }; 
	    }
	};

	self.addListener = function(listener) { mListeners.push(listener); };

	self.removeListener = function(listener) {
		var index = mListeners.indexOf(listener);
		if (index>-1) mListeners = mListeners.splice(index,1); 
	};

	if (!self.requiresUserPermission()) mUseGranted = true; 

	return self;
};

/*
 * Browser-compatible gyroscope
 */
EJSS_HARDWARE.gyroscope = function () {
	var self = {};
	var mListeners = [];
	var mIsRunning = false;
	var mData = { alpha: 0, beta: 0, gamma: 0 };
	var mUseGranted = false;

	var mSensor;
	var mLastReadingTimestamp = 0;

	function deviceReadingHandler() {
	    var interval = 1.0/60;
	    if (mLastReadingTimestamp) {
	      interval = Math.round(mSensor.timestamp - mLastReadingTimestamp);
	    }
	    mLastReadingTimestamp = mSensor.timestamp;

	    mData.alpha = rotation.alpha;
	    mData.beta  = rotation.beta;
        mData.gamma = rotation.gamma;

		for (var i=0, n=mListeners.length; i<n; i++) {
			mListeners[i](mData, interval);
		}
	};

	function deviceMotionHandler(eventData) {
	    var rotation = eventData.rotationRate;
		var interval = eventData.interval;

	    mData.alpha = rotation.alpha;
	    mData.beta  = rotation.beta;
        mData.gamma = rotation.gamma;

		for (var i=0, n=mListeners.length; i<n; i++) {
			mListeners[i](mData, interval);
		}
	};

	self.isPresent = function() { return (typeof(window.DeviceMotionEvent) !== "undefined"); };

	self.requiresUserPermission = function() { 
		if (!self.isPresent()) return false;
		return (typeof(window.DeviceMotionEvent.requestPermission) === "function");
	} 

	self.hasUserPermission = function() { return mUseGranted; }

	self.isReadyToUse = function() { return self.isPresent() && self.hasUserPermission(); }

	self.requestUserPermission = function(action) { // check sensor in device
		if (!self.isPresent()) {
			mUseGranted = true;
			return;	
		}
		// get permission - check if is IOS 13 when page loads.
		if (typeof(window.DeviceMotionEvent.requestPermission) === "function") {
			window.DeviceMotionEvent.requestPermission().then( response => {
		    	if ( response == "granted" ) { mUseGranted = true; }
  				if (typeof(action) !== "undefined") action(mUseGranted);
		    })
		    .catch(console.error);
		} 
		else { // no permission needed
			mUseGranted = true;
		}
	}

	self.start = function() {
		if (self.isReadyToUse()) {
			if ('Gyroscope' in window) {
				mSensor = new Gyroscope();
				mSensor.addEventListener('reading', deviceReadingHandler, false);
				mSensor.start();
			} else {
				window.addEventListener('devicemotion', deviceMotionHandler, false);
			}
			mIsRunning = true;
		}
	};

	self.stop = function() {
		if (self.isReadyToUse()) {
			if ('Gyroscope' in window) {
				window.removeEventListener('reading', deviceReadingHandler);
				try { mSensor.stop() } catch (e) { };
			} else {
				window.removeEventListener('devicemotion', deviceMotionHandler);
			}
		} 
		mData = { alpha: 0, beta: 0, gamma: 0 };
		mIsRunning = false;
	};

	self.isRunning = function() { return mIsRunning; };
		
	self.getDeviceData = function() { return mData; };
	
	self.addListener = function(listener) { mListeners.push(listener); };

	self.removeListener = function(listener) {
		var index = mListeners.indexOf(listener);
		if (index>-1) mListeners = mListeners.splice(index,1); 
	};

	if (!self.requiresUserPermission()) mUseGranted = true; 

	return self;
};

/*
 * Browser-compatible orientation
 */
EJSS_HARDWARE.orientation = function () {
	var self = {};
	var mListeners = [];
	var mIsRunning = false;

	var mData = { alpha: 0, beta: 0, gamma: 0};

	function deviceOrientationHandler(eventData) {
		var interval = eventData.interval;
		
	    mData.alpha = eventData.alpha;
	    mData.beta  = eventData.beta;
        mData.gamma = eventData.gamma;
        
		for (var i=0, n=mListeners.length; i<n; i++) {
			mListeners[i](mData, interval);
		}
	};

	self.isPresent = function() { return (typeof window.DeviceOrientationEvent != "undefined"); };

	self.start = function() {
		if (self.isPresent()) {
			window.addEventListener('deviceorientation', deviceOrientationHandler, false);
			mIsRunning = true;
		}
	};

	self.stop = function() {
		if (self.isPresent()) window.removeEventListener('deviceorientation', deviceOrientationHandler);
		mData = { alpha: 0, beta: 0, gamma: 0};
		mIsRunning = false;
	};

	self.isRunning = function() { return mIsRunning; };

	self.getDeviceData = function() { return mData; };

	self.addListener = function(listener) { mListeners.push(listener); };

	self.removeListener = function(listener) {
		var index = mListeners.indexOf(listener);
		if (index>-1) mListeners = mListeners.splice(index,1); 
	};

	return self;
};

/*
 * Chrome-compatible light
 */
EJSS_HARDWARE.ambientLight = function () {
	var self = {};
	var mListeners = [];
	var mIsRunning = false;

	var mData = { lux: 0 };
	var mSensor;
	var mLastReadingTimestamp = 0;

	function deviceHandler() {
	    var interval = 1.0/60;
	    if (mLastReadingTimestamp) {
	      interval = Math.round(mSensor.timestamp - mLastReadingTimestamp);
	    }
	    mLastReadingTimestamp = mSensor.timestamp;

	    mData.lux = mSensor.illuminance;
        
		for (var i=0, n=mListeners.length; i<n; i++) {
			mListeners[i](mData, interval);
		}
	};

	self.isPresent = function() { return (typeof window.AmbientLightSensor != "undefined"); };

	self.start = function() {
		if (self.isPresent()) {
			mSensor = new AmbientLightSensor();
			mSensor.addEventListener('reading', deviceHandler, false);
			mSensor.start();
			mIsRunning = true;
		}
	};

	self.stop = function() {
		if (self.isPresent()) window.removeEventListener('reading', deviceHandler);
		try { mSensor.stop() } catch (e) { };
		mData = { lux: 0 };
		mIsRunning = false;
	};

	self.isRunning = function() { return mIsRunning; };

	self.getDeviceData = function() { return mData; };

	self.addListener = function(listener) { mListeners.push(listener); };

	self.removeListener = function(listener) {
		var index = mListeners.indexOf(listener);
		if (index>-1) mListeners = mListeners.splice(index,1); 
	};

	return self;
};

/*
 * Chrome-compatible magnometer
 */
EJSS_HARDWARE.magnometer = function () {
	var self = {};
	var mListeners = [];
	var mIsRunning = false;

	var mData = { x: 0, y: 0, z: 0 };
	var mSensor;
	var mLastReadingTimestamp = 0;

	function deviceHandler() {
	    var interval = 1.0/60;
	    if (mLastReadingTimestamp) {
	      interval = Math.round(mSensor.timestamp - mLastReadingTimestamp);
	    }
	    mLastReadingTimestamp = mSensor.timestamp;

	    mData.x = mSensor.x;
	    mData.y = mSensor.y;
	    mData.z = mSensor.z;
        
		for (var i=0, n=mListeners.length; i<n; i++) {
			mListeners[i](mData, interval);
		}
	};

	self.isPresent = function() { return (typeof window.Magnometer != "undefined"); };

	self.start = function() {
		if (self.isPresent()) {
			mSensor = new Magnometer();
			mSensor.addEventListener('reading', deviceHandler, false);
			mSensor.start();
			mIsRunning = true;
		}
	};

	self.stop = function() {
		if (self.isPresent()) window.removeEventListener('reading', deviceHandler);
		try { mSensor.stop() } catch (e) { };
		mData = { x: 0, y: 0, z: 0 };
		mIsRunning = false;
	};

	self.isRunning = function() { return mIsRunning; };

	self.getDeviceData = function() { return mData; };

	self.addListener = function(listener) { mListeners.push(listener); };

	self.removeListener = function(listener) {
		var index = mListeners.indexOf(listener);
		if (index>-1) mListeners = mListeners.splice(index,1); 
	};

	return self;
};

/*
 * Browser-compatible audio
 */
EJSS_HARDWARE.audio = function () {
	var self = {};
	var mIsRunning = false;

	var mData = { x: 0, y: 0, z: 0 };
	var mAudioCtx;
	var mAnalyser;

	self.isPresent = function() { 
  		return (navigator.AudioContext || navigator.webkitAudioContext);
	};

	self.start = function(callback_ok, callback_error) {
		if (self.isPresent()) {
  			// Audio Context
  			if (typeof mAudioCtx == 'undefined') {
  				mAudioCtx = new (window.AudioContext || window.webkitAudioContext)();
			   	//set up the analyser
			  	mAnalyser = audioCtx.createAnalyser();
			  	mAnalyser.minDecibels = -20; 
			  	mAnalyser.maxDecibels = -80;
			  	mAnalyser.smoothingTimeConstant = 0.85;
			  	
			  	// filter
			  	var distortion = audioCtx.createWaveShaper();
			  	var gainNode = audioCtx.createGain();
			  	var biquadFilter = audioCtx.createBiquadFilter();
			  	var convolver = audioCtx.createConvolver();
			 
			  	// Call Get User Media
			  	navigator.mediaDevices.getUserMedia({ audio: true, video: false }).then(function(stream) {
			       var source = mAudioCtx.createMediaStreamSource(stream);
			       source.connect(mAnalyser);
			       mAnalyser.connect(distortion);
			       distortion.connect(biquadFilter);
			       biquadFilter.connect(convolver);
			       convolver.connect(gainNode);
			       gainNode.connect(mAudioCtx.destination);
			       
			       if (typeof callback_ok == 'undefined') callback_ok();
					
				   mIsRunning = true;
			  	}).catch(function(err) {
			       console.log('The following getUserMedia error occured: ' + err);
			       if (typeof callback_error == 'undefined') callback_error();
			  	});	  	
			}
		}
	};

	self.stop = function() {
		// nothing to do
	};

	// https://webaudio.github.io/web-audio-api/#baseaudiocontext	
	self.getAudioContext = function() {
		return mAudioContext;
	}
	
	// https://webaudio.github.io/web-audio-api/#analysernode
	self.getAnalyser = function() {
		return mAnalyser;
	}

	self.isRunning = function() { return mIsRunning; };

	self.getDeviceData = function() { return mData; };

	return self;
};
