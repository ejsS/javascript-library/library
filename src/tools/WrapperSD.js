function wrapper(ws,data){
	if(ws.hasOwnProperty("ip")){
		wsUri = "ws://"+ ws.ip + ":" + ws.port + "/";
		this.websocket = new WebSocket(wsUri);
	}else{
		this.websocket = ws;
	}
	this.smartdevice = new _smartDevice(this.websocket,data);
	this.smartdevice.loadApis();
	this.smartdevice.loadModels();
	this.smartdevice.generateMethodCall();
	this.smartdevice.generateModelFilling();
}

wrapper.prototype = {
	methods: {
  	connect: 	'connect', 		// Open a new connection
  	disconnect: 'disconnect', 	// Close the connection
  	get: 		'get', 			// Get some variables from the server
  	set: 		'set', 			// Set some variables in the server
	callAction: 'callAction',	// Calls a method od the model	
	extract:	"extract",		// Get useful info from a message
    eval: 		'eval', 		// Send code to evaluate
    open: 		'open', 		
    step: 		'step', 		
  },
	
  post: function(method, params) {
	//calling some method with no prebuild method, just send message
	if(method == undefined && !params.hasOwnProperty("method"))	return;	
	else if (method != undefined && !params.hasOwnProperty("method"))	params["method"] = method;
	//console.log("Wrapper calls post, method: " + method + " with: " + params);
	websocket.send(params);
  },
  
  sync: function(callback) {
	  
	  if(callback != undefined) {
		callback(response);
	  }
  },

  connect: function(callback) {
	this.smartdevice = new _smartDevice(fullData);
	this.smartdevice.loadApis();
	this.smartdevice.loadModels();
	this.smartdevice.generateMethodCall();
  },

  disconnect: function(callback) {

  },

  get: function(vars, eachStep, callback) {
	//get(var) is a getSensorData method
	//console.log("Wrapper calls get " + vars);
	if(eachStep === undefined || eachStep == false)	this.smartdevice.getSensorData("getSensorData",vars);
	else{
		//console.log("ConfigItem: " + this.smartdevice.fillConfigurationItem("updateFrequency",2.0));
		this.smartdevice.getSensorData("getSensorData",vars, [this.smartdevice.fillConfigurationItem("updateFrequency",2.0)]);
	}
  },
  
  set: function(vars, values, auth, callback) {
	//set(var, value) is a senActuatorData method
	//console.log("Wrapper calls set " + vars + " to: " + values);
	if (vars.length == 1)	this.smartdevice.sendActuatorData(auth, "sendActuatorData", "modelVars", [vars], [values]);
	else 					this.smartdevice.sendActuatorData(auth, "sendActuatorData", "modelVars", vars, values);
  },

  callAction: function(nickname, params, callback){	  
	//console.log("Wrapper calling action " + nickname + " with params: " + params);
	this.smartdevice.callAction("","callAction","modelMethods",nickname, params)
  },
  
  extract: function(strMsg, callback){
	//
	var jsonMsg = JSON.parse(strMsg);
	var vars = {};
	if(jsonMsg.hasOwnProperty("method")){
		var method = jsonMsg.method;
		var extracted = {};
		if(method == "getSensorData"){	
			vars = ["sensorId", "responseData"];
		}else if (method == "sendActuatorData"){
			vars = ["lastMeasured", "accessrole", "payload", "observerMode"];	
		}else if (method == "getSensorMetadata"){
			vars = ["sensors"];
		}else if (method == "getActuatorMetadata"){
			vars = ["actuators"];
		}else if (method == "getActionsMetadata"){
			vars = ["sensors"];
		}else if (method == "callAction"){
			vars = ["method","callerName","nickName","params"];
		}else if (method == "actionDataResponse"){
			vars = ["method","nickName"];
		}else{
			console.log("The method : " + jsonMsg.method + " is not handled in this wrapper. \n" +
			"Will be ignored.");
			return undefined;
		}
	}
	extracted = this.smartdevice.extract(vars, jsonMsg);
	//console.log("Wrapper calls extract " + vars + " from: \n" + JSON.stringify(jsonMsg) + " With value: " + JSON.stringify(extracted));
	return this.toEjssReadable(method,extracted);
  },
  
  eval: function(code, callback) {

  },

  open: function(model, callback) {

  },

  step: function(vars, callback) {

  },

  toEjssReadable: function(method,dataExtracted){
	if(method == "getSensorData"){	
		var nameValue = {};
		var valueNames = dataExtracted.responseData.valueNames;
		var dataValues = dataExtracted.responseData.data;
		for(var i = 0; i<valueNames.length;i++){
			nameValue[valueNames[i]] = dataValues[i];
		}
		//console.log("Data extracted and readable: " + JSON.stringify(nameValue));
		return nameValue;
	}else if (method == "sendActuatorData"){
		//It is just a confirmation, with additional data
		return true;
	}else if (method == "getSensorMetadata"){
		return dataExtracted;
	}else if (method == "getActuatorMetadata"){
		return dataExtracted;
	}else if (method == "getActionsMetadata"){
		return dataExtracted;
	}else if (method == "callAction"){
		return dataExtracted;
	}else if (method == "actionDataResponse"){
		return dataExtracted;
	}else{
		console.log("The method : " + method + " is nor handled in this wrapper. \n" +
		"Will be ignored.");
		return undefined;
	}
  }
	
}
