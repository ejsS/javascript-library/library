/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Provides tools for main classes.
 * @module tools  
 */

var EJSS_TOOLS = EJSS_TOOLS || {};

/**
 * A utility with Math functions.
 * @class Mathematics 
 * @constructor  
 */

EJSS_TOOLS.Mathematics = {
  TWO_PI : 2*3.1415926, //Math.PI*2,
  TO_RADIANS : Math.PI / 180,
  LOG10SCALE : 1/Math.log(10),
  cosineAndSine : [],

  /**
   * Cosine and sine for degrees.
   * @method cosineAndSineForDegrees
   * @return Array[2] cosine and sine
   */
  cosineAndSineForDegrees : function(degrees) {
    degrees %= 360;
    if (degrees<0) degrees += 360;
    var values = this.cosineAndSine[degrees];
    if (values === undefined) {
      var radians = degrees*this.TO_RADIANS;
      values = [Math.cos(radians), Math.sin(radians)];
    }
    return values;
  },

  /**
   * Degrees to radians .
   * @method radians
   * @return double
   */  
  radians : function(degrees) {
  	return degrees * Math.PI / 180;
  },
  
  /**
   * Raians to degrees.
   * @method degrees
   * @return double
   */  
  degrees : function(radians) {
	var angle = radians * 180 / Math.PI;
	var r = angle % 360;
	angle = (r * 360 < 0) ? r + 360 : r;
	return angle;     	
  },
  
  /**
   * Norm.
   * @method norm
   * @param v vector
   * @return norm
   */
  norm : function (v) {
  	var sum = 0;
  	for(var i=0; i<v.length; i++) sum += v[i]*v[i]; 
    return Math.sqrt(sum);
  },

  /**
   * Cross product.
   * @method crossProduct
   * @param v1 vector
   * @param v2 vector
   * @return cross product
   */
  crossProduct : function(v1, v2) {
    return [v1[1]*v2[2]-v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0]];
  },

  /**
   * Normalize.
   * @method normalize
   * @param v vector
   * @return vector normalized
   */
  normalize : function(v) {
    var r = EJSS_TOOLS.Mathematics.norm(v);
    if (r==0.0) return v;
    return [v[0]/r, v[1]/r, v[2]/r];
  },

  /**
   * Normal vector.
   * @method normalTo
   * @param v vector
   * @return normal vector
   */
  normalTo : function (v) {
    if(v[0]==0.0) {
      return [1.0, 0.0, 0.0];
    } else if(v[1]==0.0) {
      return [0.0, 1.0, 0.0];
    } else if(v[2]==0.0) {
      return [0.0, 0.0, 1.0];
    } else {
      var norm = EJSS_TOOLS.Mathematics.norm(v);
      return [-v[1]/norm, v[0]/norm, 0.0];
    }
  },
  
  /**
   * Rotate vector.
   */
  rotate : function(vc, v, radians) {
    var cos = Math.cos(radians),
        sin = Math.sin(radians),
        nx = (cos * (v[0] - vc[0])) + (sin * (v[1] - vc[1])) + vc[0],
        ny = (cos * (v[1] - vc[1])) - (sin * (v[0] - vc[0])) + vc[1];
    return [nx, ny];
  }  
  
};




