/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * TextField
 * @class TextField 
 * @constructor  
 */
EJSS_INTERFACE.TextField = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Value", element.setValue, element.getValue);
    controller.registerProperty("Editable", element.setEditable);
    controller.registerProperty("Size", element.setSize);

    controller.registerAction("OnChange");
  },

};

/**
 * TextField function
 * @method textField
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.textField = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mEdition = false;
  var mTmp = "";
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------


  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.TextField.registerProperties(self,controller);
  };
  
  self.enableEPub = function() {
  	if (self.getDOMElement().disabled) self.getDOMElement().disabled = false;
  };
  
  if (typeof _isEPub !== 'undefined' && _isEPub) {
    self.render = function() {
      self.enableEPub();
    }
  };
    
  // ----------------------------------------
  // Setters and getters
  // ----------------------------------------
  
  /**
   * Set the value displayed by the element
   * @method setValue
   * @param value double
   */
  self.setValue = function(value) { 
  	if(!mEdition) {	
	    if (typeof value == "undefined") // produces and error in Safari 
	    	self.getDOMElement().value = "value undefined";
	    else {
    		self.getDOMElement().value = value;
	    }
	}
  };

  /**
   * Get the value displayed by the element
   * @method getValue
   * @return double
   */
  self.getValue = function() { 
    return self.getDOMElement().value; 
  };

  /**
   * Set the editable property
   * @method setEditable
   * @param editable boolean
   */
  self.setEditable = function(editable) { 
    self.getDOMElement().readOnly = !editable; 
    if (editable) self.getStyle().setBackgroundColor("white");
    else self.getStyle().setBackgroundColor("lightgrey");
  };

  /**
   * @method getEditable
   * @return bool
   */
  self.getEditable = function() { 
    return !self.getDOMElement().readOnly; 
  };

  /**
   * Set the size 
   * @method setSize
   * @param value string
   */
  self.setSize = function(value) { 
    self.getDOMElement().size = value; 
  };

  /**
   * Get the size
   * @method getSize
   * @return double
   */
  self.getSize = function() { 
    return self.getDOMElement().size; 
  };
  
  function checkValue() {
    var value = self.getDOMElement().value;
    self.getStyle().setBackgroundColor("white");
    if (self.parseValue) self.parseValue(value);
    if(mTmp != value) {
	    var controller = self.getController();    		
	    if (controller) {
		  controller.propertiesChanged("Value");
		  controller.invokeAction("OnChange");
		  controller.reportInteractions();
	    }
	}	    
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("input");  
  mElement.id = mName;
  mElement.type = 'text';
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
  
  self.getDOMElement().onkeydown = function(key) {  	
    var dom = self.getDOMElement();
  	if(!dom.readOnly) {
  	  	var ev = key || window.event;
	  	var charCode = (ev.which) ? ev.which : ev.keyCode;  	  
  		if (charCode==13) { // return
  			mEdition = false;
  			checkValue();
  			mTmp = self.getDOMElement().value;
  		} else if (charCode==27) { // escape
  			self.getDOMElement().value = mTmp;  		
  		} else {
  			self.getStyle().setBackgroundColor("yellow");
  			mEdition = true;
  		}
    }      
  }

  self.getDOMElement().onfocus = function(dom) {
  	mTmp = self.getDOMElement().value;
  }

  self.getDOMElement().onblur = function(dom) {
  	if(!dom.readOnly && mEdition) {
  		checkValue();
  		mEdition = false;
  	}
  }
    
  self.getDOMElement().ontouchend = function(e) {
    e.stopPropagation();
  }

  self.getDOMElement().onmouseup = function(e) {
    e.stopPropagation();
  }
      
  // associate event for ipad
  if (typeof _isEPub !== 'undefined' && _isEPub) {
    self.getDOMElement().ontouchstart = function(event) {
	  if (self.getDOMElement().disabled) self.getDOMElement().disabled = false;
	  event.stopPropagation();
    };
  }

  return self;
};

