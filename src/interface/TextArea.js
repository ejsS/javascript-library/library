/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/***
 * TextArea
 * @class TextArea 
 * @constructor  
 */
EJSS_INTERFACE.TextArea = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Value", element.setValue, element.getValue);
    controller.registerProperty("Editable", element.setEditable);
    controller.registerProperty("Rows", element.setRows);
    controller.registerProperty("Columns", element.setCols);
    controller.registerProperty("ChangeColorOnEdit", element.setChangeColorOnEdit);

    controller.registerAction("OnChange");
  },

};

/**
 * TextArea function
 * @method textArea
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.textArea = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  var mChangeColorOnEdit = true;
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------


  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.TextArea.registerProperties(self,controller);
  };
 
  self.enableEPub = function() {
  	if (self.getDOMElement().disabled) self.getDOMElement().disabled = false;
  };
  
  if (typeof _isEPub !== 'undefined' && _isEPub) {
    self.render = function() {
      self.enableEPub();
    }
  };  
   
  // ----------------------------------------
  // Setters and getters
  // ----------------------------------------

  /***
   * Whether to change color onkeydown/onblur
   * @method setChangeColorOnEdit
   * @param change boolean
   */
  self.setChangeColorOnEdit = function(change) { 
    mChangeColorOnEdit = change; 
  };

  /**
   * Set the value displayed by the element
   * @method setValue
   * @param value double
   */
  self.setValue = function(value) { 
    self.getDOMElement().value = value; 
  };
  
  /***
   * Add the text and scroll to the bottom
   * @method addText
   * @param value double
   */
  self.appendText = function(value) { 
	var domElement = self.getDOMElement();
	domElement.value += value
	domElement.scrollTop = domElement.scrollHeight;
  };

  /***
   * Clear the text area
   * @method clear
   */
  self.clear = function() { 
    self.getDOMElement().value = ""; 
  };

  /**
   * Get the value displayed by the element
   * @method getValue
   * @return double
   */
  self.getValue = function() { 
    return self.getDOMElement().value; 
  };

  /**
   * Set the editable property
   * @method setEditable
   * @param editable boolean
   */
  self.setEditable = function(editable) { 
    self.getDOMElement().readOnly = !editable; 
    if (!editable) self.getStyle().setBackgroundColor("lightgrey");
  };

  /**
   * @method getEditable
   * @return bool
   */
  self.getEditable = function() { 
    return !self.getDOMElement().readOnly; 
  };

  /**
   * Set the cols 
   * @method setCols
   * @param value
   */
  self.setCols = function(cols) { 
    self.getDOMElement().cols = cols; 
  };

  /**
   * Get the cols
   * @method getCols
   * @return double
   */
  self.getCols = function() { 
    return self.getDOMElement().cols; 
  };

  /**
   * Set the rows
   * @method setRows
   * @param value
   */
  self.setRows = function(rows) { 
    self.getDOMElement().rows = rows; 
  };

  /**
   * Get the rows
   * @method getRows
   * @return double
   */
  self.getRows = function() { 
    return self.getDOMElement().rows; 
  };
    
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("textarea");  
  mElement.id = mName;
  mElement.style.verticalAlign = "middle";
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);

  self.getDOMElement().onkeydown = function(key) {
    var dom = self.getDOMElement();
  	if(!dom.readOnly) {
  		if (mChangeColorOnEdit) self.getStyle().setBackgroundColor("yellow");
    }
  }

  self.getDOMElement().onblur = function(dom) {
  	if(!dom.readOnly) {
  		if (mChangeColorOnEdit) self.getStyle().setBackgroundColor("white");  
    	var controller = self.getController();    		
    	if (controller) {
        	controller.propertiesChanged("Value");
      		controller.invokeAction("OnChange");
      		controller.reportInteractions();
    	}	    
  	}  	
  }

  return self;
};

