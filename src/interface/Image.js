/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Image
 * @class Image 
 * @constructor  
 */
EJSS_INTERFACE.Image = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("ImageUrl",element.setImageUrl);

  },

};

/**
 * Image function
 * Creates a basic Image
 * @method image
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.image = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  	
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setImageUrl = function(url) {
    var resPathFunction = self.getResourcePath; 
	if (resPathFunction!=null) {
	  url = resPathFunction(url.toString());
//		  console.log (self.getName()+ " SoundUrl set to = "+url+"\n");
	}
	else console.log ("No getResourcePath function for "+self.getName()+". URL = "+url);
    self.getDOMElement().src = url.toString(); 
  };

  self.getImageUrl = function() {
    return self.getDOMElement().src;
  };

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Image.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("img");  
  mElement.id = mName;
  mElement.alt = mName;
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);  

  return self;
};
