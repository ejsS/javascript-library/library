/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Media
 * @class Media 
 * @constructor  
 */
EJSS_INTERFACE.Media = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Url",element.setUrl);
    controller.registerProperty("Controls",element.setControls);
    controller.registerProperty("Loop",element.setLoop);
    controller.registerProperty("CurrentTime",element.setCurrentTime, element.getCurrentTime);
    controller.registerProperty("Volume",element.setVolume);
    controller.registerProperty("Autoplay",element.setAutoplay);
    controller.registerProperty("Type",element.setType);

    controller.registerAction("OnEnded");
    controller.registerAction("OnLoadedData");
    controller.registerAction("OnPlay");
    controller.registerAction("OnPause");
//    controller.registerAction("OnTimeUpdate");
  },

};

/**
 * Media function
 * Protected constructor
 * @method media
 * @param name the name of the element
 * @returns An abstract media element
 */
EJSS_INTERFACE.media = function (mName) {
  var self = EJSS_INTERFACE.element(mName);

  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setUrl = function(url) {
    if (self.getResourcePath) url = self.getResourcePath(url);
	self.getDOMElement().src = url;
  }	

  self.getUrl = function() {
  	return self.getDOMElement().src;
  }	

  self.setType = function(type) {
	self.getDOMElement().type = type;
  }	

  self.getType = function() {
  	return self.getDOMElement().type;
  }	

  self.setControls = function(controls) {
  	if (controls)
  		self.getDOMElement().setAttribute("controls","controls");
  	else
  		self.getDOMElement().removeAttribute("controls");
  }
  
  self.play = function() {
  	self.getDOMElement().play();
  }

  self.pause = function() {
  	self.getDOMElement().pause();
  }

  self.getCurrentTime = function() {
    return self.getDOMElement().currentTime;
  } 

  self.setCurrentTime = function(time) {
    self.getDOMElement().currentTime = time;
  } 
  
  self.setLoop = function(on) {
    self.getDOMElement().loop = on;
  } 
  
  self.getVolume = function() {
  	return self.getDOMElement().volume;
  }

  self.setVolume = function(volume) {
    self.getDOMElement().volume = volume;
  } 

  self.getAutoplay = function() {
  	return self.getDOMElement().autoplay;
  }

  self.setAutoplay = function(autoplay) {
    self.getDOMElement().autoplay = autoplay;
  } 

  self.isPaused = function() {
  	return self.getDOMElement().paused; 
  }

  self.isEnded = function() {
  	return self.getDOMElement().ended; 
  }

  self.getDuration = function() {
  	return self.getDOMElement().duration; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.addDOMEvents = function() {
    self.getDOMElement().onended = function() {
      var controller = self.getController();
      if (controller) controller.invokeAction("OnEnded").reportInteractions();
    };
    self.getDOMElement().onloadeddata = function() {
      var controller = self.getController();
      if (controller) controller.invokeAction("OnLoadedData").reportInteractions();
    };
    self.getDOMElement().onplay = function() {
      var controller = self.getController();
      if (controller) controller.invokeAction("OnPlay").reportInteractions();
    };
    self.getDOMElement().onpause = function() {
      var controller = self.getController();
      if (controller) controller.invokeAction("OnPause").reportInteractions();
    };
  /* This degrades performance by a lot!
    self.getDOMElement().ontimeupdate = function() {
      var controller = self.getController();
      if (controller) {
    	controller.propertiesChanged("CurrentTime");
		controller.invokeAction("OnTimeUpdate");
		controller.reportInteractions();
      }
    };
    */
    
  }
    
  return self;
};
