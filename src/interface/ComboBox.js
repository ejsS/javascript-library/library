/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * ComboBox
 * @class ComboBox 
 * @constructor  
 */
EJSS_INTERFACE.ComboBox = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Options", element.setOptions, element.getOptions);
    controller.registerProperty("SelectedOptions", element.setSelectedOptions, element.getSelectedOptions);
    controller.registerProperty("Multiple", element.setMultiple);
    controller.registerProperty("Size", element.setSize);
	
    controller.registerAction("OnChange");
    controller.registerAction("OnFocus");
  },

};

/**
 * ComboBox function
 * @method comboBox
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.comboBox = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  var mOptions = [];
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.ComboBox.registerProperties(self,controller);
  };
  
  // ----------------------------------------
  // Setters and getters
  // ----------------------------------------
  
  /**
   * @method setSize
   * @param size
   */
  self.setSize = function(size) { 
    self.getDOMElement().size = size; 
  };

  /**
   * @method getSize
   * @return size
   */
  self.getSize = function() { 
    return self.getDOMElement().size; 
  };  
  
  /**
   * @method setMultiple
   * @param multiple
   */
  self.setMultiple = function(multiple) { 
    self.getDOMElement().multiple = multiple; 
  };

  /**
   * @method getMultiple
   * @return multiple
   */
  self.getMultiple = function() { 
    return self.getDOMElement().multiple; 
  };
  
  /**
   * @method setSelectedOptions
   * @param options
   */
  self.setSelectedOptions = function(options) { 
	var dom = self.getDOMElement();
	for(var i=0; i<dom.options.length; i++) {
		dom.options[i].selected = false;
	  	for(var j=0; j<options.length; j++) { 
	    	if (dom.options[i].value == options[j]) {
	    		dom.options[i].selected = true;
	    		break;
	    	}
	  	}
	}	
  };

  /**
   * @method getSelectedOptions
   * @return options
   */
  self.getSelectedOptions = function() {
  	var mSelectedOptions = []; 
	var dom = self.getDOMElement();
	for(var j=0,i=0; i<dom.options.length; i++) {
		if(dom.options[i].selected)
			mSelectedOptions[j++] = dom.options[i].value; 
	}	  	  	
    return mSelectedOptions; 
  };

  self.getSelectedOptionsIndexes = function() {
  	var mSelectedOptionsIs = []; 
	var dom = self.getDOMElement();
	for(var j=0,i=0; i<dom.options.length; i++) {
		if(dom.options[i].selected)
			mSelectedOptionsIs[j++] = i; 
	}	  	  	
    return mSelectedOptionsIs; 
  }

  /**
   * @method setOptions
   * @param values
   */
  self.setOptions = function(values) { 
    if (EJSS_TOOLS.compareArrays(mOptions,values)) return; // Same options, no need to change them
 	mOptions = [];
  	// get selected options
  	var selected = self.getSelectedOptions();
  	// set option list
	var dom = self.getDOMElement();  
	dom.innerHTML = "";
	for(var i=0; i<values.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("value", values[i]);
		option.innerHTML = values[i];		
		dom.appendChild(option);
		mOptions[i] = values[i];      
 	}
 	// set selected options
 	self.setSelectedOptions(selected);  	 
  };

  /**
   * @method getOptions
   * @return values
   */
  self.getOptions = function() {
  	var copyOptions = []; 
	var dom = self.getDOMElement();
	for(var i=0; i<dom.options.length; i++) {
	  copyOptions[i] = dom.options[i].value; 
	}	  	  	
    return copyOptions;   	     
  };

  /**
   * Set the editable property
   * @method setEditable
   * @param editable boolean
   */
  self.setEditable = function(editable) { 
    self.getDOMElement().readOnly = !editable; 
  };

  /**
   * @method getEditable
   * @return bool
   */
  self.getEditable = function() { 
    return !self.getDOMElement().readOnly; 
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  var mElement = document.createElement("select");
  mElement.id = mName;   
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
  
  self.getDOMElement().onfocus = function() {
    var controller = self.getController();
	       
    self.getStyle().setBackgroundColor("white");      
	if (controller) {
		controller.propertiesChanged("Options");
		controller.invokeAction("OnFocus");
		controller.reportInteractions();
	}  	
  }	
  
  self.getDOMElement().onchange = function() {
    var controller = self.getController();
	       
    self.getStyle().setBackgroundColor("white");      
	if (controller) {
		controller.propertiesChanged("SelectedOptions");
		controller.invokeAction("OnChange");
		controller.reportInteractions();
	}
  }

  self.getDOMElement().onkeydown = function() {
    self.getStyle().setBackgroundColor("yellow");
  }
  
  self.getDOMElement().ontouchend = function(e) {
    e.stopPropagation();
  }
  
  self.getDOMElement().onmouseup = function(e) {
    e.stopPropagation();
  }
       
  return self;
};

