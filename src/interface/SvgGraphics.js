/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS SVGGraphicswork for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * SVGGraphics
 * @class SVGGraphics 
 * @constructor  
 */
EJSS_INTERFACE.SvgGraphics = {
  	
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Height", element.setHeight);
	controller.registerProperty("Width", element.setWidth);

  },

};

/**
 * SVGGraphics function
 * Creates a basic SVGGraphics
 * @method svgGraphics
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.svgGraphics = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mHeight = 0;
  var mWidth = 0; 

  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  /**
   * @method setWidth
   * @param width
   */
  self.setWidth = function(width) { 
    if (mWidth!=width) { 
      mWidth = width;
      self.getDOMElement().setAttribute('width', mWidth);
      return true;
    } 
    return false;
  };

  /**
   * @method getWidth
   * @return width
   */
  self.getWidth = function() { 
    return mWidth; 
  };
  
  /**
   * @method setHeight
   * @param height
   */
  self.setHeight = function(height) { 
    if (mHeight!=height) { 
      mHeight = height; 
      self.getDOMElement().setAttribute('height', mHeight);
      return true;
    } 
    return false;
  };

  /**
   * @method getHeight
   * @return height
   */
  self.getHeight = function() { 
    return mHeight; 
  };

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.SvgGraphics.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  mElement.setAttribute('id', mName);
  mElement.style.overflow = "hidden";
  mElement.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
  self.setWidth(500);
  self.setHeight(500);
  
  return self;
};

