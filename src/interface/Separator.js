/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Separator
 * @class Separator 
 * @constructor  
 */
EJSS_INTERFACE.Separator = {
	HORIZONTAL: 0,
	VERTICAL: 1,
	
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Orientation", element.setOrientation);

	controller.registerProperty("Width", element.setWidth);
	
  },

};

/**
 * Separator function
 * Creates a basic Separator
 * @method separator
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.separator = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mOrientation = EJSS_INTERFACE.Separator.HORIZONTAL;
  var mWidth = "100%";
  var mHeight = "2px";
    	
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setOrientation = function(orient) {
   	if (typeof orient == "string") orient = EJSS_INTERFACE.Separator[orient.toUpperCase()];
    if(mOrientation != orient) {
	  mOrientation = orient;
	  if (mOrientation == EJSS_INTERFACE.Separator.VERTICAL) {
	  	self.getDOMElement().style.display = "inline";
  	  	self.getDOMElement().style.height = self.getDOMElement().style.width;
  	  	self.getDOMElement().style.width = mHeight;
  	  }
	  else {
	  	self.getDOMElement().style.display = "none";
	  	self.getDOMElement().style.width = self.getDOMElement().style.height;
	  	self.getDOMElement().style.height = mHeight;
	  }  	  	 
    }
  }

  self.getOrientation = function() {
    return mOrientation;
  }

  /**
   * @method setWidth
   * @param width
   */
  self.setWidth = function(width) { 
    if (mWidth!=width) { 
      mWidth = width; 
	  if (mOrientation == EJSS_INTERFACE.Separator.VERTICAL)
  	  	self.getDOMElement().style.height = mWidth;
	  else
	  	self.getDOMElement().style.width = mWidth;  	  	 
    } 
  };

  /**
   * @method getWidth
   * @return width
   */
  self.getWidth = function() { 
    return mWidth; 
  };
  
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Separator.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("hr");  
  mElement.id = mName;
  mElement.style.height = mHeight;
  mElement.style.width = mWidth;
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);
    
  return self;
};

