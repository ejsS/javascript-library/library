/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * FunctionField
 * @class FunctionField 
 * @constructor  
 */
EJSS_INTERFACE.FunctionField = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.TextField.registerProperties(element,controller); // super class
    controller.registerAction("OnError");
  },

};

/**
 * FunctionField function
 * @method functionField
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.functionField = function (mName) {
  var self = EJSS_INTERFACE.textField(mName);
  
  var mParser = EJSS_DRAWING2D.functionsParser();
  var mExpression = mParser.parse("0");
  var mSuperSetValue = self.setValue;
  var mCurrentValue;
  var mParameters = {};
  var mBackground = self.getStyle().getBackgroundColor();
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------


  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.FunctionField.registerProperties(self,controller);
  };
  
  // ----------------------------------------
  // Setters and getters
  // ----------------------------------------
  
  	/**
	 * Sets parameters for the evaluation of the function
	 * @method setParameters
	 * @param parameters Object { "p1" : value1, "p2" : value2, ...}
	 * @return void
	 */
	self.setParameters = function(parameters) {
	  for (var param in parameters) { 
          mParameters[param] = parameters[param];
	    }
    }
    
   self.parseValue = function (value) {
	if (value) {
	  if (value!=mCurrentValue) {
       //console.log("Parsing value to "+value);
	    try {
	      mExpression = mParser.parse(value);
	      self.getStyle().setBackgroundColor(mBackground);
	    }
	    catch (error) {
	   	  self.getController().invokeAction("OnError");
	      console.log("FunctionField: Error parsing function \""+value+ "\" : " + error.message + "\n");
	      self.getStyle().setBackgroundColor("red");
	    }
	  mCurrentValue = value;
	  }
	}
	else {
	  mCurrentValue = "0";
	  mExpression = mParser.parse("0");
	  self.getStyle().setBackgroundColor(mBackground);
	}
   }
    
  /**
   * Set the value displayed by the element
   * @method setValue
   * @param value double
   */
  self.setValue = function(value) { 
    mSuperSetValue(value);
    self.parseValue(value);
  };

  /**
   * Evaluate expression
   * @return double
   */
  self.evaluate = function(variablesValues) {
    var vblevalue = {};
    for (var param in mParameters) { 
      vblevalue[param] = mParameters[param];
	}
    for (var values in variablesValues) { 
      vblevalue[values] = variablesValues[values];
	}
	try {
      return mExpression.evaluate(vblevalue);
    }
    catch (error) {
      self.getController().invokeAction("OnError");
      return NaN;
    }
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};

