/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * MotionJPEG
 * @class MotionJPEG 
 * @extends Media
 * @constructor  
 */
EJSS_INTERFACE.MotionJPEG = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Canvas.registerProperties(element,controller); // super class

    controller.registerProperty("Url",element.setUrl);
    controller.registerProperty("AsBackground",element.setAsBackground, element.isAsBackground);
    controller.registerProperty("On",element.setOnOff, element.isOnOff);
    controller.registerProperty("UseWebCam",element.useWebCam);
    controller.registerProperty("Flip",element.setFlip);
    controller.registerProperty("UseFrontCam",element.setFrontCam);
  },

};

/**
 * motionJPEG function
 * Creates a basic MotionJPEG
 * @method motionJPEG
 * @param name the name of the element
 * @returns A MotionJPEG element
 */
EJSS_INTERFACE.motionJPEG = function (mName) {
  var self = EJSS_INTERFACE.canvas(mName);
  var mImage = new Image();
  var mVideo = document.createElement("video");
  var isBackground = false;
  var mPlaying = true;
  var mStream;
  var mUrl;
  var mUseWebCam = false;
  var mFlip = false;
  var mFrontCam = true;
  
  function initCam() {  	
      navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
      if (navigator.getUserMedia){
  	    // check if previous videos running
  	    if(mStream) {
  	  	  var tracks = mStream.getTracks();
  	  	  for(var i=0; i<tracks.length; i++)
			tracks[i].stop();
  	    }
        
        function successCallback(stream){
          mStream = stream;
		  if (mPlaying) mVideo.srcObject = mStream; 	          
        	console.log("Success = " + mStream);
        }
        
        function errorCallback(error){
        	console.log("Error = "+error);
        }
        
		var videoConstraints = {
			facingMode: mFrontCam?"user":"environment"
		}
		        
        navigator.getUserMedia({video: videoConstraints}, successCallback, errorCallback);
      }	  	
  }	
  
  self.useWebCam = function(useWebCam) {
  	if(mUseWebCam != useWebCam) {
  		mUseWebCam = useWebCam;
  		if(mUseWebCam) {
  			initCam();
		} else {
			mUrl = null;
			self.pause();
		}
  	}
  }

  self.setFrontCam = function(frontCam) {
	if (mFrontCam != frontCam) {
	  	mFrontCam = frontCam;
  		if(mUseWebCam) {
	  		initCam();	
  		}
	}
  }	

  self.getFrontCam = function() {
  	return mFrontCam;
  }	
  
  self.setUrl = function(url) {
    if (self.getResourcePath) url = self.getResourcePath(url);
    mUrl = url;
    if (mUrl && mPlaying) {
    	mImage.src = mUrl;
    	mImage.crossOrigin = "Anonymous";
    }
    mUseWebCam = false; 
  }	

  self.getUrl = function() {
	if(mUseWebCam)
		return mStream;
	else
  		return mUrl;
  }	

  self.setOnOff = function(on) {
	if (on) {
	  if (!mPlaying) self.play();
	}
	else {
	  if (mPlaying) self.pause();
	}
  }

  self.isOnOff = function() {
	return mPlaying;
  }

  self.play = function() {
	if (mUrl) 
    	if(mUseWebCam)
    		mVideo.srcObject = mStream;
    	else
    		mImage.src = mUrl;	
	mPlaying = true;
  }

  self.pause = function(noclear) {
	mPlaying = false;
	if(typeof noclear == 'undefined' || !noclear) self.clear();
  }

  self.getCanvasContext = function() {
  	return self.getContext();
  }

  self.getSnapshot = function() {
  	return self.getContext().getImageData(0, 0, self.getWidth(), self.getHeight());
  }
  
  self.isAsBackground = function() {
  	return isBackground;
  }

  self.setAsBackground = function(back) {
    if (back != isBackground) {
  	  isBackground = back;
      if (isBackground) self.getStyle().setCSS({ "position":"absolute", "z-index":"-100" });
	  else self.getStyle().setCSS({ "position":" ", "z-index":" " });	  		
  	}
  } 

  self.render = function() {
  	if (mPlaying && (mUrl || mStream)) {
  		var canvas = self.getDOMElement();
		self.getContext().save();
		var flip = mFrontCam? mFlip:!mFlip; // with Back Camera the flip is inverse
   		if (flip) {
			self.getContext().translate(canvas.width,0);
			self.getContext().scale(-1,1);
		}
    	if(mUseWebCam) {
    	   if (mVideo.readyState === mVideo.HAVE_ENOUGH_DATA)
	  			self.getContext().drawImage(mVideo, 0, 0, canvas.width, canvas.height);
		} else
	  		self.getContext().drawImage(mImage,  
  					0, 0, mImage.width, mImage.height, 
               		0, 0, canvas.width, canvas.height);
		self.getContext().restore();
  	}
  }
  
  self.setFlip = function(flip) {
  	mFlip = flip;
  }
  
  self.getFlip = function() {
  	return mFlip;
  }
        
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.MotionJPEG.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  mVideo.autoplay="true";
  mVideo.style="display:none;"

  self.getStyle().setBackgroundColor('rgb(239,239,255)');
  self.setEnabled(false);
  self.setWidth(500);
  self.setHeight(500);

  return self;
};
