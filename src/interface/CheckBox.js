/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * CheckBox
 * @class CheckBox 
 * @constructor  
 */
EJSS_INTERFACE.CheckBox = {
	OFF : false,
	ON: true,
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Text", element.setText);
	controller.registerProperty("ImageUrl",element.setImageUrl);

	controller.registerProperty("TextOn", element.setTextOn);
	controller.registerProperty("ImageOnUrl",element.setImageOnUrl);
	controller.registerProperty("TextOff", element.setTextOff);
	controller.registerProperty("ImageOffUrl",element.setImageOffUrl);

	controller.registerProperty("Checked",element.setChecked, element.getChecked);	
	controller.registerProperty("Value", element.setValue, element.getValue);

    controller.registerProperty("LabelCSS", element.setLabelCSS);
    controller.registerProperty("CheckboxCSS", element.setCheckboxCSS);

    controller.registerAction("OnChange", element.getChecked); 
    controller.registerAction("OnCheckOn");
    controller.registerAction("OnCheckOff");
  },	

};

/**
 * CheckBox function
 * Creates a basic CheckBox
 * @method checkBox
 * @param name the name of the element
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.checkBox = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
    
  var mUrl = new Image();
  var mUrlOn = new Image();
  var mUrlOff = new Image();
  var mText = "";
  var mTextOn = "";
  var mTextOff = "";
    
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setText = function(text) {
  	mText = text;
  	applyChange();
  }

  self.setImageUrl = function(url) {
  	mUrl.src = self.getResourcePath(url);
  	applyChange();
  }

  self.setTextOff = function(text) {
  	mTextOff = text;
  	applyChange();
  }

  self.getTextOff = function() {
    return mTextOff;
  }

  self.setImageOffUrl = function(url) {
  	mUrlOff.src = self.getResourcePath(url);
  	applyChange();
  }

  self.getImageUrlOff = function() {
    return mUrlOff.src;
  }

  self.setTextOn = function(text) {
  	mTextOn = text;
  	applyChange();
  }

  self.getTextOn = function() {
    return mTextOn;
  }

  self.setImageOnUrl = function(url) {
  	mUrlOn.src = self.getResourcePath(url);
  	applyChange();
  }

  self.getImageUrlOn = function() {
    return mUrlOn.src;
  }

  self.setChecked = function(state) {
  	if (mCBElement.checked != state) {
  		mCBElement.checked = state;
  		applyChange();
  	}
  }

  self.getChecked = function() {
  	return mCBElement.checked;
  }

  /**
   * @method setValue
   * @param value double
   */
  self.setValue = function(value) { 
    mCBElement.value = value; 
  };

  /**
   * @method getValue
   * @return double
   */
  self.getValue = function() { 
    return mCBElement.value; 
  };
  
  function applyChange() {
  	mLBElement.innerHTML = ""; 	
	if (mCBElement.checked) { // on			
    	if(mUrlOn.src.length > 0) 
    		mLBElement.appendChild(mUrlOn);    	
    	else if (mTextOn.length > 0)
    		mLBElement.innerHTML = mTextOn.toString();
    	else if (mUrl.src.length > 0)
			mLBElement.appendChild(mUrl);
    	else if (mText.length > 0)	
    		mLBElement.innerHTML = mText.toString();
	} else { // off
    	if(mUrlOff.src.length > 0) 
    		mLBElement.appendChild(mUrlOff);
    	else if (mTextOff.length > 0)
    		mLBElement.innerHTML = mTextOff.toString(); 					
    	else if (mUrl.src.length > 0)
			mLBElement.appendChild(mUrl);
    	else if (mText.length > 0)	
    		mLBElement.innerHTML = mText.toString();
	}
  }
   
  /**
   * @method setLabelCSS
   * @param css
   */
  self.setLabelCSS = function(css) {  	
  	EJSS_INTERFACE.Style.setCSS(mLBElement,css);
  }

  /**
   * @method setCheckboxCSS
   * @param css
   */
  self.setCheckboxCSS = function(css) {  	
  	EJSS_INTERFACE.Style.setCSS(mCBElement,css);
  }
  
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.CheckBox.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  // set
  var mElement = document.createElement("fieldset");  
  mElement.id = mName;  
  mElement.style.border = 0;
  mElement.style.display = "inline-block";  
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
  
  // checkbox
  var mCBElement = document.createElement("input");  
  mCBElement.id = mName + ".checkbox";
  mCBElement.type = "checkbox";
  mElement.appendChild(mCBElement);  

  mCBElement.onchange = function() {
  	applyChange();
    var controller = self.getController();
	if (controller) {
		controller.propertiesChanged("Checked");
    	controller.invokeAction("OnChange");
		if(mCBElement.checked)
			controller.invokeAction("OnCheckOn");
		else
			controller.invokeAction("OnCheckOff");
		controller.reportInteractions();
	}
  };
  
  mCBElement.ontouchend = function(e) {
  	e.stopPropagation();
  };

  mCBElement.onmouseup = function(e) {
  	e.stopPropagation();
  };

  // checkbox label
  var mLBElement = document.createElement("label");  
  mLBElement.id = mName + ".label";
  mLBElement.htmlFor = mName + ".checkbox";  
  mLBElement.style.display = "inline-block";
  mElement.appendChild(mLBElement);     

  return self;
};
