/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * RadioButton
 * @class RadioButton 
 * @constructor  
 */
EJSS_INTERFACE.RadioButton = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Text", element.setText);
	controller.registerProperty("ImageUrl",element.setImageUrl);

	controller.registerProperty("TextOn", element.setTextOn);
	controller.registerProperty("ImageOnUrl",element.setImageOnUrl);
	controller.registerProperty("TextOff", element.setTextOff);
	controller.registerProperty("ImageOffUrl",element.setImageOffUrl);

	controller.registerProperty("Checked",element.setChecked, element.getChecked);	
	controller.registerProperty("Value", element.setValue, element.getValue);

    controller.registerProperty("LabelCSS", element.setLabelCSS);
    controller.registerProperty("RadioCSS", element.setRadioCSS);
    
	controller.registerProperty("Parent", function (parent) {
			element.setParent(parent);			
			element.setKey(element.getDOMElement().parentNode.id);
		});

    controller.registerAction("OnChange", element.getChecked); 
    controller.registerAction("OnCheckOn");
    controller.registerAction("OnCheckOff");

  },

};

/**
 * RadioButton function
 * Creates a basic radioButton
 * @method RadioButton
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.radioButton = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mUrl = new Image();
  var mUrlOn = new Image();
  var mUrlOff = new Image();
  var mText = "";
  var mTextOn = "";
  var mTextOff = "";
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setKey = function(key) {
    mRBElement.name = key;     
  }

  self.getKey = function() {
    return mRBElement.name;
  }

  self.setText = function(text) {
  	mText = text;
  	applyChange();
  }

  self.setImageUrl = function(url) {
  	mUrl.src = self.getResourcePath(url);
  	applyChange();
  }

  self.setTextOff = function(text) {
  	mTextOff = text;
  	applyChange();
  }

  self.getTextOff = function() {
    return mTextOff;
  }

  self.setImageOffUrl = function(url) {
  	mUrlOff.src = self.getResourcePath(url);
  	applyChange();
  }

  self.getImageUrlOff = function() {
    return mUrlOff.src;
  }

  self.setTextOn = function(text) {
  	mTextOn = text;
  	applyChange();
  }

  self.getTextOn = function() {
    return mTextOn;
  }

  self.setImageOnUrl = function(url) {
  	mUrlOn.src = self.getResourcePath(url);
  	applyChange();
  }

  self.getImageUrlOn = function() {
    return mUrlOn.src;
  }

  self.setChecked = function(state) {
  	if (mRBElement.checked != state) {
  		mRBElement.checked = state;
  		applyChange();
  	}
  }
  
  self.getChecked = function() {
  	return mRBElement.checked;
  }

  /**
   * @method setValue
   * @param value double
   */
  self.setValue = function(value) { 
    mRBElement.value = value; 
  };

  /**
   * @method getValue
   * @return double
   */
  self.getValue = function() { 
    return mRBElement.value; 
  };

  function applyChange() {
	mLBElement.innerHTML = "";
	if (mRBElement.checked) { // on
    	if(mUrlOn.src.length > 0) 
    		mLBElement.appendChild(mUrlOn);    	
    	else if (mTextOn.length > 0)
    		mLBElement.innerHTML = mTextOn.toString();
    	else if (mUrl.src.length > 0)
			mLBElement.appendChild(mUrl);
    	else if (mText.length > 0)	
    		mLBElement.innerHTML = mText.toString();
	} else { // off
    	if(mUrlOff.src.length > 0) 
    		mLBElement.appendChild(mUrlOff);
    	else if (mTextOff.length > 0)
    		mLBElement.innerHTML = mTextOff.toString(); 					
    	else if (mUrl.src.length > 0)
			mLBElement.appendChild(mUrl);
    	else if (mText.length > 0)	
    		mLBElement.innerHTML = mText.toString();
	}
  }

  /**
   * @method setLabelCSS
   * @param css
   */
  self.setLabelCSS = function(css) {  	
  	EJSS_INTERFACE.Style.setCSS(mLBElement,css);
  }

  /**
   * @method setRadioCSS
   * @param css
   */
  self.setRadioCSS = function(css) {  	
  	EJSS_INTERFACE.Style.setCSS(mRBElement,css);
  }
  
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.RadioButton.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  // set
  var mElement = document.createElement("fieldset");  
  mElement.id = mName;  
  mElement.style.border = 0;
  mElement.style.display = "inline-block";   
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
  
  // radiobutton
  var mRBElement = document.createElement("input");  
  mRBElement.id = mName + ".radio";
  mRBElement.type = "radio";
  mElement.appendChild(mRBElement);  

  mRBElement.onchange = function() {
  	mRBElement.remoteonchange();

	// onchange for other radiobuttons with the same name
	var eles = document.getElementsByTagName("fieldset");
	for(var i=0; i<eles.length; i++) {
		if (eles[i].childNodes && eles[i].childNodes[0]) {
			var rd = eles[i].childNodes[0];
			if ((rd.type=="radio") && rd.remoteonchange && (rd.name==mRBElement.name) && (rd.id!=mRBElement.id))
				rd.remoteonchange();
		}
	}	
  };

  mRBElement.ontouchend = function(e) {
  	e.stopPropagation();
  }
  
  mRBElement.onmouseup = function(e) {
  	e.stopPropagation();
  }

  // onchange for other radiobuttons
  mRBElement.remoteonchange = function(state) {  	
	applyChange();
    var controller = self.getController();
	if (controller) {		
		controller.propertiesChanged("Checked");
		controller.invokeAction("OnChange");
		if(mRBElement.checked)
			controller.invokeAction("OnCheckOn");
		else
			controller.invokeAction("OnCheckOff");
		controller.reportInteractions();
	}
  }      
  
  // radiobutton label
  var mLBElement = document.createElement("label");  
  mLBElement.id = mName + ".label";
  mLBElement.htmlFor = mName + ".radio";
  mLBElement.style.display = "inline-block";
  mElement.appendChild(mLBElement);     
            
  return self;
};

