/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Audio
 * @class Audio 
 * @extends Media
 * @constructor  
 */
EJSS_INTERFACE.Audio = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Media.registerProperties(element,controller); // super class

    controller.registerProperty("AudioUrl",element.setUrl, element.getUrl);
  },

};

/**
 * Audio function
 * Creates a basic Audio
 * @method audio
 * @param name the name of the element
 * @returns A sound element
 */
EJSS_INTERFACE.audio = function (mName) {
  var self = EJSS_INTERFACE.media(mName);
  var mUrl = null;
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Audio.registerProperties(self,controller);
  };

  self.setUrl = function(url) {
	  mUrl = self.getResourcePath(url,false);
    mElement.setAttribute("src",mUrl);
	  //srcElement.src = url;
  }	

  self.getUrl = function() {
    return mUrl;
	  //return srcElement.src;
  }	

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  var mElement = document.createElement("audio");  
  mElement.id = mName;
  document.body.appendChild(mElement);
  /*
  var srcElement = document.createElement("source");  
  mElement.appendChild(srcElement);
  */
  self.setDOMElement(mElement);
  
  self.addDOMEvents();
    
  return self;
};
