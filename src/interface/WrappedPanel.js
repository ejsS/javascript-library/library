/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * WrappedPanel
 * @class WrappedPanel 
 * @constructor  
 */
EJSS_INTERFACE.WrappedPanel = {

  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Panel.registerProperties(element,controller); // super class
      
  },

};

/**
 * panel wrapper function
 * Creates a special top-level basic panel
 * @method wrappedPanel
 * @param name the name of the element
 * @returns A basic panel
 */
EJSS_INTERFACE.wrappedPanel = function (mName) {
  if (typeof _isEPub === 'undefined' || !_isEPub) { 
    return EJSS_INTERFACE.panel (mName);
  }
  
  // We created this element because of a bug in Apple's ePub reader in iOS.
  // When Apple fixed this, we changed it to (only) make sure the children
  // appear in the same ePub page

  var self = EJSS_INTERFACE.panel(mName);

/*  
  self.adjustPosition = function() {     
    mWrapper.style.height = self.getDOMElement().getBoundingClientRect().height + "px";
    mWrapper.style.width = self.getDOMElement().getBoundingClientRect().width + "px";

    var topShell = mWrapper.getBoundingClientRect().top;
    var leftShell = mWrapper.getBoundingClientRect().left;

    var body = document.body;
    var docElem = document.documentElement;
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

    var clientTop = docElem.clientTop || body.clientTop || 0;

    self.getDOMElement().style.top = (topShell + scrollTop) + "px";
    self.getDOMElement().style.left = (leftShell + scrollLeft) + "px";    
  };
  
  var super_setParent = self.setParent;
  
  self.setParent = function(parent) {
  	if (typeof parent == "string") { // an HTML object
  		var domObject = document.getElementById(parent);
      	domObject.appendChild(mWrapper);
    }
    else if (parent.getDOMElement) { // an interface element 
    	parent.getDOMElement().appendChild(mWrapper);
 	} else { // an DOM element
		parent.appendChild(mWrapper);
    }
  	super_setParent(parent);
  };
*/    
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.WrappedPanel.registerProperties(self,controller);
  };


  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

//  var mWrapper = document.createElement("div");  
//  EJSS_INTERFACE.Style.setCSS(mWrapper,{ "display": "block", "margin-left": "auto", "margin-right": "auto", "page-break-inside":"avoid" });
//  document.body.appendChild(mWrapper);  

//  var mElement = document.createElement("div");  
//  mElement.id = mName;
//  mElement.contenteditable = true;
//  document.body.appendChild(mElement);  
//  self.setDOMElement(mElement);  
  self.getStyle().setPadding("0px");
  self.getStyle().setMargin("0px");
  self.getStyle().setTextAlign("TEXTA_CENTER");  
  // self.getStyle().setCSS({ "page-break-inside":"avoid", "display": "inline-block", "margin-left": "auto", "margin-right": "auto", "position":"absolute" });   
  self.getStyle().setCSS({ "page-break-inside":"avoid", "margin-left": "auto", "margin-right": "auto"});
  
  return self;
};

