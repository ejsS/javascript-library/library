/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * TwoStateButton
 * @class TwoStateButton 
 * @constructor  
 */
EJSS_INTERFACE.TwoStateButton = {
	OFF : false,
	ON: true,
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("TextOn", element.setTextOn);
	controller.registerProperty("ImageOnUrl",element.setImageUrlOn);
	controller.registerProperty("TextOff", element.setTextOff);
	controller.registerProperty("ImageOffUrl",element.setImageUrlOff);

	controller.registerProperty("State",element.setState, element.getState);	

    controller.registerAction("OnClick");
    controller.registerAction("OffClick");
  },

};

/**
 * TwoStateButton function
 * Creates a basic TwoStateButton
 * @method twoStateButton
 * @param name the name of the element
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.twoStateButton = function (mName) {
  var TSButton = EJSS_INTERFACE.TwoStateButton;
  var self = EJSS_INTERFACE.element(mName);
  
  var mUrlOn = "";
  var mUrlOff = "";
  var mTextOn = "";
  var mTextOff = "";
  var mState = -1;
  	
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setTextOff = function(text) {
  	mTextOff = text;
  	applyChange();  	 
  }

  self.getTextOff = function() {
    return mTextOff;
  }

  self.setImageUrlOff = function(url) {  	
  	var newUrl = self.getResourcePath(url);
  	if (newUrl==mUrlOff) return;
  	mUrlOff = newUrl;
  	applyChange();
  }

  self.getImageUrlOff = function() {
    return mUrlOff;
  }

  self.setTextOn = function(text) {
  	mTextOn = text;
  	applyChange();
  }

  self.getTextOn = function() {
    return mTextOn;
  }

  self.setImageUrlOn = function(url) {
  	var newUrl = self.getResourcePath(url);
  	if (newUrl==mUrlOn) return;
  	mUrlOn = newUrl;
  	applyChange();
  }

  self.getImageUrlOn = function() {
    return mUrlOn;
  }

  self.setState = function(state) {
  	if (typeof state == "string") state = TSButton[state.toUpperCase()];
  	if (state != mState) {  		
	  	mState = state;
	  	applyChange();
	}
  }

  self.getState = function() {
  	return mState;
  }

  function applyChange() {
  	self.getDOMElement().innerHTML = "";
	if (mState) { // on
    	if(mUrlOn.length > 0) 
    	    self.getDOMElement().innerHTML = '<img alt="' + mName + '" style="vertical-align:inherit;-webkit-touch-callout:none;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;" src="'+mUrlOn+'"/>';
    	if(mTextOn.length > 0)
    	    self.getDOMElement().innerHTML += '<span style="white-space:pre;vertical-align:inherit;-webkit-touch-callout:none;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;">'+mTextOn.toString()+'</span>';
	} else { // off
    	if(mUrlOff.length > 0) 
    	    self.getDOMElement().innerHTML = '<img alt="' + mName + '" style="vertical-align:inherit;-webkit-touch-callout:none;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;" src="'+mUrlOff+'"/>';
    	if(mTextOff.length > 0)
    	    self.getDOMElement().innerHTML += '<span style="white-space:pre;vertical-align:inherit;-webkit-touch-callout:none;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;">'+mTextOff.toString()+'</span>';
	}  	
  }

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.TwoStateButton.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("button");  
  mElement.id = mName;
  mElement.style.verticalAlign = "middle";
  mElement.style.webkitTouchCallout = "none";
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);


  self.getDOMElement().oncontextmenu = function(e) {
  	e.preventDefault(); e.stopPropagation();
    return false;  	
  }  

  function onClickListener(e) {
  	var state = self.getState();
  	self.setState(!state);
    var controller = self.getController();
    if (controller) {
    	controller.propertiesChanged("State");
    	if(state)
    		controller.invokeAction("OnClick");
    	else
    		controller.invokeAction("OffClick");
    	controller.reportInteractions();
    }
  	e.preventDefault(); e.stopPropagation();    
    return false;    
  };

  self.getDOMElement().addEventListener("mouseup", onClickListener);
  self.getDOMElement().addEventListener("touchend", onClickListener);
    
  return self;
};
