/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * DataTable
 * @class DataTable 
 * @constructor  
 */
EJSS_INTERFACE.DataTable = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Active", element.setActive, element.isActive);
    controller.registerProperty("NoRepeat", element.setNoRepeat, element.isNoRepeat);
    controller.registerProperty("ClearAtInput", element.setClearAtInput, element.isClearAtInput);
    controller.registerProperty("Skip", element.setSkip, element.getSkip);
	controller.registerProperty("AddToTop", element.setAddToTop, element.isAddToTop);
    controller.registerProperty("Maximum", element.setMaximumPoints, element.getMaximumPoints);
    controller.registerProperty("Input", element.appendRow);
    controller.registerProperty("ColumnsWidth", element.setColumnsWidth, element.getColumnsWidth);
    controller.registerProperty("HeadersText", element.setHeadersText, element.getHeadersText);
    controller.registerProperty("HeadersCSS", element.setHeadersCSS, element.getHeadersCSS);
    controller.registerProperty("CellsFormat", element.setCellsFormat, element.getCellsFormat);    
    controller.registerProperty("CellsCSS", element.setCellsCSS, element.getCellsCSS);

	controller.registerProperty("Width", element.setWidth, element.getStyle().getWidth);
	controller.registerProperty("BodyHeight", element.setBodyHeight, element.getBodyHeight);
  },

};

/**
 * DataTable function
 * DataTable implements dataCollected(), which makes it a Collector of data 
 * @see _view._collectData
 * @method dataTable
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.dataTable = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  // Configuration variables
  var mActive = true;
  var mAddToTop = false;
  var mNoRepeat = false;
  var mClearAtInput = false;
  var mSkip = 0;
  var mMaximumPoints = 0;

  var mColumnsWidth = [];
  var mHeadersText = [];
  var mCellsFormat = [];
  var mHeadersCSS = [];
  var mCellsCSS = [];

  // Implementation variables
  var mValues = [];
  var mLastValueAdded = [];
  var mNumRows = 0;
  var mCounter = 0;
  var mMustRecreate = true;
  var mChanged = true;
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  /**
   * Set/unset the active state of the element. 
   * Default value is true.
   * @method setActive
   * @param _active
   */
  self.setActive = function(active) {
    mActive = active;
  }
  
  /**
   * Whether the element accepts points.
   * @method isActive
   * @return
   */
  self.isActive = function() { 
  	return mActive; 
  }
  
    /**
   * Set/unset whether the table adds new points to its top 
   * Default value is true.
   * @method setAddToTop
   * @param _active
   */
  self.setAddToTop = function(toTop) {
    mAddToTop = toTop;
  }
  
  /**
   * Whether the table adds new points to its top
   * @method isAddToTop
   * @return
   */
  self.isAddToTop = function() { 
  	return mAddToTop; 
  }
  
  /**
   * @method setNoRepeat
   * @param norepeat
   */
  self.setNoRepeat = function(norepeat) {
  	mNoRepeat = norepeat;
  };

  /**
   * @method isNoRepeat
   * @return norepeat
   */
  self.isNoRepeat = function() { 
    return norepeat; 
  }; 
  
  /**
   * Sets the element to clear existing points when receiving 
   * a new point or array of points.
   * @method setClearAtInput
   * @param _clear
   */
  self.setClearAtInput = function(clear) {
     mClearAtInput = clear;
  }
  
  /**
   * Whether the trail is in clear at input mode.
   * @method isClearAtInput
   * @return
   */
  self.isClearAtInput = function() { 
  	return mClearAtInput; 
  }
  
  /**
   * @method setSkip
   * @param Skip
   */
  self.setSkip = function(skip) {
  	mSkip = skip;
  };

  /**
   * @method getSkip
   * @return stride
   */
  self.getSkip = function() { 
    return mSkip; 
  };   
 
  /**
   * @method setMaximumPoints
   * @param num
   */
  self.setMaximumPoints = function(maximum) {
  	mMaximumPoints = maximum;
  	if (mValues.length>mMaximumPoints) {
  	  if (mAddToTop) mValues = mValues.slice(0,mMaximumPoints);
  	  else mValues.splice(0,mValues.length-mMaximumPoints);
  	  mMustRecreate = true;
  	}
  };

  /**
   * @method getMaximumPoints
   * @return num
   */
  self.getMaximumPoints = function() { 
    return mMaximumPoints; 
  };    
  
  /**
   * @method setColumnsWidth
   * @param names
   */
  self.setColumnsWidth = function(width) {
    if (!EJSS_TOOLS.compareArrays(mColumnsWidth,width)) {
    	mColumnsWidth = width;
        mMustRecreate = true;
    }
  };

  /**
   * @method getHeadersText
   * @return names
   */
  self.getColumnsWidth = function() { 
    return mColumnsWidth; 
  }; 
  
  /**
   * @method setHeadersText
   * @param names
   */
  self.setHeadersText = function(names) {
    if (!EJSS_TOOLS.compareArrays(mHeadersText,names)) {
    	mHeadersText = names;
        mMustRecreate = true;
    }
  };

  /**
   * @method getHeadersText
   * @return names
   */
  self.getHeadersText = function() { 
    return mHeadersText; 
  };    

  /**
   * @method setCellsFormat
   * @param format
   */
  self.setCellsFormat = function(format) {
    if (!EJSS_TOOLS.compareArrays(mCellsFormat,format)) {
      	mCellsFormat = format;
        mChanged = true;
    }
  };

  /**
   * @method getCellsFormat
   * @return format
   */
  self.getCellsFormat = function() { 
    return mCellsFormat; 
  };    

  /**
   * @method setHeadersCSS
   * @param css Object
   */
  self.setHeadersCSS = function(css) {
    if (!EJSS_TOOLS.compareArrays(mHeadersCSS,css)) {
    	mHeadersCSS = css;
        mMustRecreate = true;
    }
  };
 
 /**
   * @method getHeadersCSS
   * @return Object
   */
  self.getHeadersCSS = function() { 
    return mHeadersCSS; 
  };    
    
  /**
   * @method setCellsCSS
   * @param css Object
   */
  self.setCellsCSS = function(css) {
    if (!EJSS_TOOLS.compareArrays(mCellsCSS,css)) {
    	mCellsCSS = css;
        mMustRecreate = true;
    }
  };
 
 /**
   * @method getCellsCSS
   * @return Object
   */
  self.getCellsCSS = function() { 
    return mCellsCSS; 
  };    
  
  /**
   * Set the width 
   * @method setBodyHeight
   * @param value string or int
   */
  self.setBodyHeight = function(value) { 
    if (typeof value !== "string") value = value + "px";
    tbdy.style.height=value;
  };

  /**
   * Get the width 
   * @method getBodyHeight
   * @return the height
   */
  self.getBodyHeight = function() { 
    return tbdy.style.height;
  };
  
  /**
   * Set the width 
   * @method setWidth
   * @param value string or int
   */
  self.setWidth = function(value) { 
    if (typeof value !== "string") value = value + "px";
    tbdy.style.width=value;
    thead.style.width=value;
    self.getStyle().setWidth(value);
  };

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.DataTable.registerProperties(self,controller);
  };
  
  // ----------------------------------------
  // Operation methods
  // ----------------------------------------
  
  /**
   * Same as clear
   * @method reset
   */
  self.reset = function() {
    self.clear();
  }
  
    /**
   * Same as clear
   * @method reset
   */
  self.initialize = function() {
    self.clear();
  }
  
  /**
   * Set the value displayed by the element
   * @method setValue
   * @param value double
   */
  self.clear = function(value) {
  	mValues  = [];
  	mCounter = 0;
    mMustRecreate = true;
  };

  self.appendRow = function(value) {
    if (!mActive) return;
    if (mClearAtInput) self.clear();
    if (value[0] instanceof Array) {
      for (var i=0,n=value.length; i<n; i++) self.addPoint(value[i]);
    }
    else self.addPoint(value);
  }

  /**
   * Set the value displayed by the element
   * @method setValue
   * @param value double
   */
  self.addPoint = function(value) {
    if (mSkip>0) {
      if (mCounter<mSkip) {
        mCounter++;
        return;
      }
      mCounter = 0; // reset the counter
    } 
    if (mNoRepeat && EJSS_TOOLS.compareArrays(value,mLastValueAdded)) return;
    mLastValueAdded = value.slice(0);
    if (mAddToTop) {
      if (mMaximumPoints>0 && mValues.length>=mMaximumPoints) mValues.pop();
      mValues.unshift(mLastValueAdded);
    }
    else {
      if (mMaximumPoints>0 && mValues.length>=mMaximumPoints) mValues.shift();
      mValues.push(mLastValueAdded);
    }
  	mChanged = true;
  };

   /**
   * Implementation of dataCollecte
   * @method dataCollected
   */
  self.dataCollected  = function() { } // Does nothing, really
  
  self.render = function() {
    if (mMustRecreate) {
      createTable();
      fillData();
    }
    else if (mChanged) {
      fillData();
    }
  }
      
  function createTable() {
    //console.log("Creating table");
   	tbdy.innerHTML = '';
   	thead.innerHTML = '';

    if (typeof mHeadersText != "undefined" && mHeadersText.length > 0) { // column names
      var tr = document.createElement('tr');
      tr.style.border = "inherit";
      for (var i=0,n=mHeadersText.length; i<n; i++) {
          var th = document.createElement('th');
          if (mColumnsWidth[i]) {
            if (typeof mColumnsWidth[i] !== "string") th.style.width = mColumnsWidth[i] + "px";
            else th.style.width = mColumnsWidth[i];
          }
          if (mHeadersCSS[i]) EJSS_INTERFACE.Style.setCSS(th,mHeadersCSS[i]);
       	  th.innerHTML = mHeadersText[i];		        	
       	  tr.appendChild(th);
      }
      thead.appendChild(tr);
    }
    mNumRows = 0;
    mMustRecreate = false;
  }
  
  function createRow(columns) {
      // console.log ("Creating row");
      var tr = document.createElement('tr');
	  tr.style.border = "inherit";
	  for (var i=0; i<columns; i++) {
		var td = document.createElement('td');
        if (mColumnsWidth[i]) {
          if (typeof mColumnsWidth[i] !== "string") td.style.width = mColumnsWidth[i] + "px";
          else td.style.width = mColumnsWidth[i];
        }
        if (mCellsCSS[i]) EJSS_INTERFACE.Style.setCSS(td,mCellsCSS[i]);
	    tr.appendChild(td);
	  }
      tbdy.appendChild(tr);
      mNumRows++;
  }

  function fillData() {
    // console.log("Filling data");
    mChanged = false;
    var hasFormats = (typeof mCellsFormat != "undefined");
    for (var i=0, n=mValues.length; i<n; i++) {
      var row = mValues[i];
      if (i>=mNumRows) createRow(row.length);  
      var tabRow = tbdy.rows[i];
      if (hasFormats) {
        for (var j=0, m=row.length; j<m; j++) {
          //console.log ("cell "+j+" = "+tabRow.cells[j]);
          tabRow.cells[j].innerHTML = formatValue(row[j],mCellsFormat[j]);
        }
      }
      else {
        for (var j=0, m=row.length; j<m; j++) tabRow.cells[j].innerHTML = formatValue(row[j]);
      }
    }
    if (mAddToTop) {
      if (mValues.length>=0) { // Select first row
        var firstOne = tbdy.rows[0];
        tbdy.scrollTop = firstOne.offsetBottom;
      }
    }
    else { 
      if (mValues.length>0) { // Select last row
        var lastOne = tbdy.rows[mValues.length-1];
        tbdy.scrollTop = lastOne.offsetTop;
      }
    }
  }

  function formatValue(value, format) {
	  if (typeof value !== 'number') return value;
  	  if (typeof format != "undefined") { // format
		var index = format.indexOf('.');
  		var indexE = format.toUpperCase().indexOf('E');
		if (indexE == -1) { // decimal notation
	  	  var digits = 0;
	  	  if (index >= 0) digits = Number(format.length-index-1);
	  	  return parseFloat(value).toFixed(digits);
		} 
		else { // scientific notation
	  	  var digits = 0;
	  	  if (index >= 0) digits = Number(indexE-index-1);
		  return parseFloat(value).toExponential(digits);	
		}
	  } 
	  return parseFloat(value).toFixed(3); 
	}
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement('table');
  var tbdy = document.createElement('tbody');
  var thead = document.createElement('thead');
  mElement.style.display="inline-block";
  mElement.appendChild(thead);
  mElement.appendChild(tbdy);
  mElement.className = "DataTable";
  
  mElement.id = mName;
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
    
  return self;
};

