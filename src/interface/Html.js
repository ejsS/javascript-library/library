/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Html
 * @class Html 
 * @constructor  
 */
EJSS_INTERFACE.Html = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Html", element.setHtml);
	controller.registerProperty("Url", element.setUrl);
	controller.registerProperty("InheritCSS", element.setInheritCSS)

  },

};

/**
 * Html function
 * Creates a basic Html
 * @method html
 * @param name the name of the element
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.html = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  var inheritCSS = false;
    
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setHtml = function(text) {
    var windoc = self.getDOMElement().contentWindow;
    var domdoc = windoc.document;
    domdoc.write(text);

	// head
    if(inheritCSS) {
		if (windoc.parent) {
		  var arrStyleSheets = windoc.parent.document.querySelectorAll("link[rel=stylesheet]");
		  for (var i = 0; i < arrStyleSheets.length; i++) {
		    var newLink = document.createElement("link");
		    newLink.rel  = arrStyleSheets[i].rel;
		    newLink.href = arrStyleSheets[i].href;
		    domdoc.head.appendChild(newLink);
		  }
		}    	
    }
  };

  self.getHtml = function() {
    var domdoc = self.getDOMElement().contentWindow.document;
    return domdoc.body.innerHTML;
  };

  self.setUrl = function(url) {
    if( url.indexOf("http:") == 0 || url.indexOf("https:") == 0);
    else url = self.getResourcePath(url);
	self.getDOMElement().src = url;   	  	
  };

  self.setInheritCSS = function(inherit) {
  	interitCSS = inherit;
  };

  self.getInheritCSS = function() {
  	return interitCSS;
  };

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Html.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("iframe");    
  mElement.id = mName;
  mElement.src="about:blank"; 
  mElement.width="100%";
  
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);
  self.getStyle().setPadding("0px");
  self.getStyle().setMargin("0px");
  self.getStyle().setBorderStyle("none");
  
  return self;
};

