/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Label
 * @class Label 
 * @constructor  
 */
EJSS_INTERFACE.Label = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Text", element.setText);

  },

};

/**
 * Label function
 * Creates a basic Label
 * @method label
 * @param name the name of the element
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.label = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
    
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setText = function(text) {
    self.getDOMElement().innerHTML = '<span style="white-space:pre;">'+text.toString()+'</span>'; 
  }

  self.getText = function() {
    return self.getDOMElement().innerHTML;
  }

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Label.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("label");  
  mElement.id = mName;
  mElement.style.display = "inline-block";
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);
    
  return self;
};

