/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Slider
 * @class Slider 
 * @constructor  
 */
EJSS_INTERFACE.Slider = {
	PRECISION : 1.0e5,
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Value", element.setValue, element.getValue);
    controller.registerProperty("Minimum", element.setMin);
    controller.registerProperty("Maximum", element.setMax);
    controller.registerProperty("Step", element.setStep);
    
    controller.registerProperty("TextWidth", element.setTextWidth);    
    controller.registerProperty("ShowText", element.setShowText);
    controller.registerProperty("Format", element.setFormat);
	controller.registerProperty("Disabled", element.setDisabled);

    controller.registerProperty("LabelCSS", element.setLabelCSS);
    controller.registerProperty("RangeCSS", element.setRangeCSS);

    controller.registerAction("OnChange" ,element.getValue); 
    controller.registerAction("OnRelease" ,element.getValue); 
    
  },
 

};

/**
 * Slider function
 * @method slider
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.slider = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
    
  var mShowText = false;
  var mDigits = 3;
  var mStepSetByUser = false;
    
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------


  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Slider.registerProperties(self,controller);
  };
  
  // ----------------------------------------
  // Setters and getters
  // ----------------------------------------
  
  /**
   * Set the value displayed by the element
   * @method setValue
   * @param value double
   */
  self.setValue = function(value) { 
    if (typeof value == "undefined") 
    	mSLElement.value = "value undefined";
    else {
    	if (!isNaN(parseFloat(value)) && isFinite(value)) {
    		mSLElement.value = parseFloat(value);
    		if (mShowText) mLBElement.innerHTML = parseFloat(value).toFixed(mDigits);
    	}
    }
  };

  /**
   * Get the value displayed by the element
   * @method getValue
   * @return double
   */
  self.getValue = function() {
  	var value = mSLElement.value;
  	if (!isNaN(parseFloat(value)) && isFinite(value)) {   	       	       
  	  return parseFloat(value);
  	}     	    	  	 
    return value; 
  };

  /**
   * Set the format with which to display the value
   * @method setFormat
   * @param format String
   */
  self.setFormat = function(format) {
  	var index = format.indexOf('.');
  	var digits = 0;
  	if (index >= 0) digits = Number(format.length-index-1);
    if (mDigits != digits) {
      mDigits = digits;
      self.setValue(self.getValue());
    }
  };

  /**
   * Set the step 
   * @method setStep
   * @param value string
   */
  self.setStep = function(value) { 
	if (isNaN(value)) {
	  mStepSetByUser = false;
	  mSLElement.step = (mSLElement.max-mSLElement.min) / EJSS_INTERFACE.Slider.PRECISION;
	}
	else {
	  mStepSetByUser = true;
      mSLElement.step = value;
	}
  };

  /**
   * Get the step
   * @method getStep
   * @return double
   */
  self.getStep = function() { 
    return mSLElement.step; 
  };

  /**
   * @method setMax
   * @param value string
   */
  self.setMax = function(value) { 
    mSLElement.max = value; 
    if (!mStepSetByUser) mSLElement.step = (mSLElement.max-mSLElement.min) / EJSS_INTERFACE.Slider.PRECISION;
  };

  /**
   * @method getMax
   * @return double
   */
  self.getMax = function() { 
    return mSLElement.max; 
  };

  /**
   * @method setMin
   * @param value string
   */
  self.setMin = function(value) { 
    mSLElement.min = value; 
    if (!mStepSetByUser) mSLElement.step = (mSLElement.max-mSLElement.min) / EJSS_INTERFACE.Slider.PRECISION;
  };

  /**
   * @method getMin
   * @return double
   */
  self.getMin = function() { 
    return mSLElement.min; 
  };

  /**
   * @method setShowText
   * @param value string
   */
  self.setShowText = function(value) {
  	mShowText = value;    
  	if (mShowText) mLBElement.innerHTML = "";  	
  };

  /**
   * @method getShowText
   * @return double
   */
  self.getShowText = function() { 
    return mShowText; 
  };

  /**
   * @method setTextWidth
   * @param value string
   */
  self.setTextWidth = function(value) {
  	mLBElement.style.width = value;    
  };

  /**
   * @method getTextWidth
   * @return value
   */
  self.getTextWidth = function(value) {
  	return mLBElement.style.width;    
  };

  /**
   * @method setDisabled
   * @param disabled
   */
  self.setDisabled = function(disabled) {
  	mSLElement.disabled = disabled; 
  }

  /**
   * @method getDisabled
   * @return value
   */
  self.getDisabled = function() {
    return mSLElement.disabled;
  }

  /**
   * @method setLabelCSS
   * @param css
   */
  self.setLabelCSS = function(css) {  	
  	EJSS_INTERFACE.Style.setCSS(mLBElement,css);
  }

  /**
   * @method setRangeCSS
   * @param css
   */
  self.setRangeCSS = function(css) {  	
  	EJSS_INTERFACE.Style.setCSS(mSLElement,css);
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("div");  
  mElement.id = mName;
  mElement.style.display = "inline-block";
  mElement.style.overflow = "hidden"; /* Makes #outer contain its floated children */
  mElement.style.textAlign = "left";
  mElement.style.verticalAlign = "middle";
  mElement.style.webkitTouchCallout = "none";
  mElement.style.webkitUserSelect = "none";
  mElement.style.userSelect = "none";  
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
  
  var mLBElement = document.createElement("label");  
  mLBElement.id = mName + ".label";  
  mLBElement.style.float = "left"; /* Make this div as wide as its contents */
  mElement.appendChild(mLBElement);

  var mDIElement = document.createElement("div");  
  mDIElement.id = mName + ".div2";
  mDIElement.style.overflow = "hidden"; /* Make this div take up the rest of the horizontal space, and no more */
  mElement.appendChild(mDIElement);

  var mSLElement = document.createElement("input");
  mSLElement.style.verticalAlign = "middle";
  mSLElement.style.width = "98%";  
  mSLElement.style.height = "98%";  
  mSLElement.type = 'range';
  mSLElement.id = mName + ".slider";
  mSLElement.value = 0;
  mDIElement.appendChild(mSLElement);  

  mSLElement.oninput = function() {  	
	  var controller = self.getController();    		
	  if (controller) {
		controller.propertiesChanged("Value");
		controller.invokeAction("OnChange");
		controller.reportInteractions();
	  }	        
	  return false;
  }

  mSLElement.onmouseup = function(e) {  	
	  var controller = self.getController();    		
	  if (controller) {
		controller.invokeAction("OnRelease");
		controller.reportInteractions();
	  }	        
      e.stopPropagation();
  }

  mSLElement.ontouchend = function(e) {  	
	  var controller = self.getController();    		
	  if (controller) {
		controller.invokeAction("OnRelease");
		controller.reportInteractions();
	  }	        
      e.stopPropagation();
  }

  return self;
};

