/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * MathML
 * @class MathML 
 * @constructor  
 */
EJSS_INTERFACE.MathML = {
  	
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Content",element.setContent, element.getContent);

  },

};

/**
 * MathML function
 * Creates a basic MathML
 * @method mathML
 * @param name the name of the element
 * @returns A MathML element
 */
EJSS_INTERFACE.mathML = function (mName) {
  var self = EJSS_INTERFACE.element(mName);

  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  /**
   * @method setContent
   * @param content
   */
  self.setContent = function(content) {
  	self.getDOMElement().innerHTML = content;
  };

  /**
   * @method getContent
   * @return content
   */
  self.getContent = function() { 
    return self.getDOMElement().innerHTML; 
  };

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.MathML.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("math");
  mElement.setAttribute('id', mName);
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);
  
  return self;
};

