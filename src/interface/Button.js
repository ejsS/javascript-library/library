/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Button
 * @class Button 
 * @constructor  
 */
EJSS_INTERFACE.Button = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Text", element.setText);
	controller.registerProperty("Disabled", element.setDisabled);	
	controller.registerProperty("ImageUrl",element.setImageUrl);
		
    controller.registerAction("OnClick");
    controller.registerAction("OnPress");
    controller.registerAction("OnRelease");
  },

};

/**
 * Button function
 * Creates a basic Button
 * @method button
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.button = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mUrl = "";
  var mText = "";
  	
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setDisabled = function(disabled) {
  	self.getDOMElement().disabled = disabled; 
  	self.getDOMElement().style.pointerEvents = (disabled?"none":"");
  }

  self.getDisabled = function() {
    return self.getDOMElement().disabled;
  }

  self.setText = function(text) {
    if (mText==text) return;
  	mText = text;
  	applyChange();
  	/*
  	if(mUrl.length > 0) {
  		self.getDOMElement().innerHTML = '<img style="vertical-align:inherit;" src="'+mUrl+
  			'"/><span style="white-space:pre;">'+mText.toString()+'</span>';
  	} else {
    	self.getDOMElement().innerHTML = '<span style="white-space:pre;">'+mText.toString()+'</span>';
    } 
    */
  }

  self.getText = function() {
    return mText;
  }

  self.setImageUrl = function(url) {
  	var newUrl = self.getResourcePath(url);
  	if (newUrl==mUrl) return;
  	mUrl = newUrl;
  	applyChange();
  	/*
  	if(mText.length > 0) {
  		self.getDOMElement().innerHTML = '<img style="vertical-align:inherit;" src="'+mUrl+
  			'"/><span style="white-space:pre;">'+mText.toString()+'</span>';
  	} else {
  		self.getDOMElement().innerHTML = '<img src="'+mUrl+'"/>';
  	}
  	*/ 
  }
  
  function applyChange() {
  	self.getDOMElement().innerHTML = "";
    if(mUrl.length > 0) {
        var dom_img = document.createElement("img");
        dom_img.onclick = function(e) { 
            e.preventDefault(); e.stopPropagation();
            applyChange();
            return false;
        };
        dom_img.src = mUrl;
        dom_img.style = "vertical-align:inherit;-webkit-touch-callout:none;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;";
        dom_img.alt = mName;
        self.getDOMElement().appendChild(dom_img);
    }
    //if(mUrl.length > 0) 
    //	self.getDOMElement().innerHTML = '<img style="vertical-align:inherit;-webkit-touch-callout:none;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;" src="'+mUrl+'"/>';
    if(mText.length > 0)
    	self.getDOMElement().innerHTML += '<span style="white-space:pre;-webkit-touch-callout:none;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;vertical-align:inherit;">'+mText.toString()+'</span>';
  }
  self.getImageUrl = function() {
    return mUrl;
  }

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Button.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("button");  
  mElement.id = mName;
  mElement.style.verticalAlign = "middle";
  mElement.style.webkitTouchCallout = "none";
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);

  self.getDOMElement().oncontextmenu = function(e) {
  	e.preventDefault(); e.stopPropagation();
    return false;  	
  }  
  
  self.getDOMElement().onmousedown = function(e) {
    var controller = self.getController();
    if (controller) controller.invokeAction("OnPress").reportInteractions();
    e.preventDefault(); e.stopPropagation();
    return false;
    };

  self.getDOMElement().addEventListener("touchstart", function(e) {
    var controller = self.getController();
    if (controller) controller.invokeAction("OnPress").reportInteractions();
    e.preventDefault(); e.stopPropagation();
    return false;
  });

  self.getDOMElement().onmouseup = function(e) {
    var controller = self.getController();
    if (controller) {
      controller.invokeAction("OnRelease");
      controller.invokeAction("OnClick").reportInteractions();
    }
    e.preventDefault(); e.stopPropagation();
    return false;
  };

  self.getDOMElement().addEventListener("touchend", function(e) {
    var controller = self.getController();
    if (controller) {
      controller.invokeAction("OnRelease");
      controller.invokeAction("OnClick").reportInteractions();
    }
    e.preventDefault(); e.stopPropagation();
    return false;
  });
    
  return self;
};

