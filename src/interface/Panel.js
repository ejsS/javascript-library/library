/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * panel
 * @class panel 
 * @constructor  
 */
EJSS_INTERFACE.Panel = {

  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Html", element.setHtml, element.getHtml);

    controller.registerAction("OnClick");
    controller.registerAction("OnPress");
    controller.registerAction("OnRelease");
    controller.registerAction("OnMove");
    
  },

};

/**
 * panel function
 * Creates a basic panel
 * @method panel
 * @param name the name of the element
 * @returns A basic panel
 */
EJSS_INTERFACE.panel = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
   
  /**
   * @method appendChild
   * @param name
   */
  self.appendChild = function(element) {
  	var child = element.getDOMElement();
    self.getDOMElement().appendChild(child);       
  };

  /**
   * @method removeChild
   * @param name
   */
  self.removeChild = function(element) {
  	var child = element.getDOMElement();
    self.getDOMElement().removeChild(child);       
  };

  /**
   * @method insertBefore
   * @param name
   */
  self.insertBefore = function(newele,refele) {
  	var newchild = newele.getDOMElement();
  	var refchild = refele.getDOMElement();
    self.getDOMElement().insertBefore(newchild,refchild);       
  };

  /**
   * Set the inner html
   * @method setHtml
   * @param value string
   */
  self.setHtml = function(value) { 
    self.getDOMElement().innerHTML = value; 
  };

  /**
   * Get the inner html
   * @method getHtml
   * @return string
   */
  self.getHtml = function() { 
    return self.getDOMElement().innerHTML; 
  };

 
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Panel.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("div");  
  mElement.id = mName;
  mElement.contenteditable = true;
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);  
  self.getStyle().setPadding("0px");
  self.getStyle().setMargin("0px");
  self.getStyle().setTextAlign("TEXTA_CENTER");  
  self.getStyle().setCSS({ "display": "block", "margin-left": "auto", "margin-right": "auto" });
  
  // enable inputs (iBooks)
  self.getDOMElement().addEventListener("touchstart", function(event) {
     var ips = document.getElementsByTagName("input");
     for(var i = 0; ips.length > i ; i++) {
  	 	ips[i].disabled = false;
  	 }
  });
    
  // onPress
  self.getDOMElement().addEventListener("mousedown", function(e) {
    var controller = self.getController();
    if (controller) controller.invokeAction("OnPress").reportInteractions();
  });

  self.getDOMElement().addEventListener("touchstart", function(e) {
    var controller = self.getController();
    if (controller) controller.invokeAction("OnPress").reportInteractions();
  });

  // onClick/onRelease
  self.getDOMElement().addEventListener("mouseup", function(e) {
    var controller = self.getController();
    if (controller) {
      controller.invokeAction("OnRelease");
      controller.invokeAction("OnClick").reportInteractions();
    }
  });

  self.getDOMElement().addEventListener("touchend", function(e) {
    var controller = self.getController();
    if (controller) {
      controller.invokeAction("OnRelease");
      controller.invokeAction("OnClick").reportInteractions();
    }
  });

  // onMove
  self.getDOMElement().addEventListener("mousemove", function(e) {
    var controller = self.getController();
    if (controller) {
      controller.invokeAction("OnMove").reportInteractions();
    }
  });

  self.getDOMElement().addEventListener("touchmove", function(e) {
    var controller = self.getController();
    if (controller) {
      controller.invokeAction("OnMove").reportInteractions();
    }
  });
    
  return self;
};

