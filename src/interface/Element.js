/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Element
 * @class Element 
 * @constructor  
 */

EJSS_INTERFACE.Element = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
      element.setController(controller);

      controller.registerProperty("Tooltip",  element.setTitle);	

      controller.registerProperty("Foreground",  element.getStyle().setColor);
      controller.registerProperty("Background",  element.getStyle().setBackgroundColor);
      controller.registerProperty("BackgroundImage",  element.getStyle().setBackgroundImage);
      controller.registerProperty("BorderWidth",  element.getStyle().setBorderWidth);
      controller.registerProperty("BorderColor",  element.getStyle().setBorderColor);
      controller.registerProperty("BorderStyle",   element.getStyle().setBorderStyle);
      controller.registerProperty("Padding",   element.getStyle().setPadding);
      
	  controller.registerProperty("Width", element.getStyle().setWidth, element.getStyle().getWidth);
	  controller.registerProperty("Height", element.getStyle().setHeight, element.getStyle().getHeight);
      controller.registerProperty("LineHeight", element.getStyle().setLineHeight, element.getStyle().getLineHeight);

	  controller.registerProperty("TextAlign", element.getStyle().setTextAlign);      
	  controller.registerProperty("VerticalAlign", element.getStyle().setVerticalAlign);      
	  controller.registerProperty("Visibility", element.getStyle().setVisibility);      
      controller.registerProperty("Display", element.getStyle().setDisplay);      
	  controller.registerProperty("Disabled", element.setDisabled);	

	  controller.registerProperty("Float", element.getStyle().setFloat);
	  controller.registerProperty("Position", element.getStyle().setPosition);
	  controller.registerProperty("Left", element.getStyle().setLeft);
	  controller.registerProperty("Top", element.getStyle().setTop);
	  controller.registerProperty("Bottom", element.getStyle().setBottom);
	  controller.registerProperty("Overflow", element.getStyle().setOverflow);
	  controller.registerProperty("BoxShadow", element.getStyle().setShadow);
	  controller.registerProperty("Margin", element.getStyle().setMargin);
	      
      controller.registerProperty("Parent", element.setParent);
      
      controller.registerProperty("ClassName", element.setClassName);
      
      controller.registerProperty("CSS", element.getStyle().setCSS);
      controller.registerProperty("Font", element.getStyle().setFont);
   
      controller.registerProperty("FontFamily", element.getStyle().setFontFamily);
      controller.registerProperty("FontSize", element.getStyle().setFontSize);
      controller.registerProperty("LetterSpacing", element.getStyle().setLetterSpacing);
      controller.registerProperty("OutlineColor", element.getStyle().setOutlineColor);
      controller.registerProperty("FontWeight", element.getStyle().setFontWeight);		
      controller.registerProperty("FillColor", element.getStyle().setFillColor);
      controller.registerProperty("FontStyle", element.getStyle().setFontStyle);
      
      controller.registerProperty("Transform", element.getStyle().setTransform);
      controller.registerProperty("WhiteSpace", element.getStyle().setWhiteSpace);
  }
};

/**
 * Creates a basic Element
 * @method element
 * @param name the name of the element
 */
EJSS_INTERFACE.element = function (mName) {
  var self = {};
  var mDOMElement;
  var mParent;
  
  // Instance variables
  var mStyle = EJSS_INTERFACE.style(mName);	// style for element 
  var mController = null; // An object that implements invokeAction and propertiesChanged
  
  /**
   * Returns the DOM object element (i.e the button, textfield, etc.)
   */
  self.getDOMElement = function() {
    return mDOMElement;
  };

  /**
   * Set the DOM object element (i.e the button, textfield, etc.)
   */
  self.setDOMElement = function(element) {
    mDOMElement = element;
  };

  /**
   * @method setParent
   * @param name
   */
  self.setParent = function(parent) {
  	mParent = parent;
  	if (typeof parent == "string") { // an HTML object
  		var domObject = document.getElementById(parent);
      	domObject.appendChild(self.getDOMElement());
    }
    else if(parent.getDOMElement) { // an interface element 
    	parent.appendChild(self);
 	} else { // an DOM element
		parent.appendChild(self.getDOMElement());
    }       
  };
  
  /**
   * @method getParent
   * @return name
   */
  self.getParent = function() {
    return mParent;       
  };  

  /**
   * @method getName
   * @return name
   */
  self.getName = function() {
    return mName;       
  };  

  /**
   * Default resource finder
   */
  self.getResourcePath = function(filename) {
  	return filename;
  };
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setClassName = function(name) {
  	self.getDOMElement().className = name; 
  };

  self.getClassName = function() {
    return self.getDOMElement().className;
  };

  self.setDisabled = function(disabled) {
  	self.getDOMElement().disabled = disabled;
  	self.getDOMElement().style.pointerEvents = (disabled?"none":"");
  };

  self.getDisabled = function() {
    return self.getDOMElement().disabled;
  };
  
  /**
   * @method setTitle
   * @param title
   */
  self.setTitle = function(title) {
    self.getDOMElement().title = title;       
  };  

  /**
   * @method getTitle
   * @return title
   */
  self.getTitle = function() {
    return self.getDOMElement().title;       
  };  

  /**
   * Return the style (defined in DrawingPanel.js) of the inner rectangle
   * @method getStyle 
   */
  self.getStyle = function() { 
    return mStyle; 
  };

  /**
   * Returns the controller object
   * @method getController
   * @return Controller
   */
  self.getController = function () {
    return mController;
  };

  /**
   * Set the controller
   * @method setController
   * @param Controller
   */
  self.setController = function (controller) {
    mController = controller;
  };
  
  /**
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Element.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};

