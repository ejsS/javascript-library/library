/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * ImageAndTextButton
 * @class ImageAndTextButton 
 * @constructor  
 */
EJSS_INTERFACE.ImageAndTextButton = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Text", element.setText);
	controller.registerProperty("ImageUrl",element.setImageUrl);

    controller.registerAction("OnClick");
  },

};

/**
 * ImageAndTextButton function
 * Creates a basic ImageAndTextButton
 * @method imageAndTextButton
 * @param name the name of the element
 * @returns Image & Label
 */
EJSS_INTERFACE.imageAndTextButton = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  var mText = "";
  var mUrl = "";
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setText = function(text) {
  	mText = text.toString();
  	self.getDOMElement().innerHTML = mUrl + mText; 
  }

  self.getText = function() {
    return mText;
  }

  self.setImageUrl = function(url) {
  	mUrl = '<img alt="'+mName+'" src="'+self.getResourcePath(url)+'"/>'
  	self.getDOMElement().innerHTML = mUrl + mText;
  }

  self.getImageUrl = function() {
    return mUrl;
  }

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.ImageAndTextButton.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("button");  
  mElement.id = mName;
  mElement.style.borderStyle = "none";
  mElement.style.background = "inherit";
//  mElement.style.backgroundColor = "transparent"; 
  mElement.style.cursor = "inherit";
  mElement.style.webkitBoxShadow = "inset 0 0px 0 rgba(255,255,255,0),0 0px 0px rgba(0,0,0,0)";
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);

  self.getDOMElement().onclick = function(e) {
    var controller = self.getController();
    if (controller) controller.invokeAction("OnClick").reportInteractions();
    e.preventDefault(); e.stopPropagation();
    return false;
  };
    
  return self;
};
