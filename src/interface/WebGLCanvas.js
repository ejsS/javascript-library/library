/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS WebGLCanvaswork for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * WebGLCanvas
 * @class WebGLCanvas 
 * @constructor  
 */
EJSS_INTERFACE.WebGLCanvas = {
  	
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Height", element.setHeight);
	controller.registerProperty("Width", element.setWidth);

  },

};

/**
 * WebGLCanvas function
 * Creates a basic WebGLCanvas
 * @method webGLCanvas
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.webGLCanvas = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mHeight = 0;
  var mWidth = 0; 
  
  var mGL;

  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  /**
   * @method setWidth
   * @param width
   */
  self.setWidth = function(width) { 
    if (mWidth!=width) { 
      mWidth = width;
      self.getStyle().setWidth(mWidth);
      return true;
    } 
    return false;
  };

  /**
   * @method getWidth
   * @return width
   */
  self.getWidth = function() { 
    return mWidth; 
  };
  
   /**
   * @method setHeight
   * @param height
   */
  self.setHeight = function(height) { 
    if (mHeight!=height) { 
      mHeight = height; 
      self.getStyle().setHeight(mHeight);
      return true;
    } 
    return false;
  };

  /**
   * @method getHeight
   * @return height
   */
  self.getHeight = function() { 
    return mHeight; 
  };

  self.getContext = function() {
  	return mGL;
  };
  
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.WebGLCanvas.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("canvas");  
  mElement.setAttribute('id', mName);
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);
//  self.getStyle().setBorderColor("black");
//  self.getStyle().setBorderWidth(1);  
  
  self.setWidth(500);
  self.setHeight(500);
    
  // test support
  try { mGL = mElement.getContext('webgl'); } catch (e) { }    
  try { mGL = mGL || mElement.getContext('experimental-webgl'); } catch (e) { }
  try { mGL = mGL || mElement.getContext('webkit-3d'); } catch (e) { }
  try { mGL = mGL || mElement.getContext('moz-webgl'); } catch (e) { }
    
  if (!mGL)  {
  	self.getStyle().setBackgroundColor("rgb(239,239,255)");
  	
  	var ctx = mElement.getContext('2d');
  	ctx.font = "bold 16px Arial";
  	ctx.fillText("WebGL not supported!", 100, 100);
  } else {
  	// mGL.getExtension('OES_standard_derivatives');
  }
  return self;
};

