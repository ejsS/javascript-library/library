/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * boxPanel
 * @class boxPanel 
 * @constructor  
 */
EJSS_INTERFACE.BoxPanel = {

  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Panel.registerProperties(element,controller); // super class

	controller.registerProperty("Draggable", element.setDraggable);	

  },

  showOkDialog: function(message, okfunction, width) {
  	var mypanel = document.getElementById(".myBoxPanelOk") || EJSS_INTERFACE.boxPanel(".myBoxPanelOk").getDOMElement();
  	var mybackpanel = document.getElementById(".myBoxPanelOk.back");
	// message
    var myMsg = document.getElementById(".myBoxPanelOk.msg");
    myMsg.style.visibility = "visible";
    myMsg.innerHTML = message + "<br/>";
    
    // ok bt
  	var myOkButton =  document.getElementById(".myBoxPanelOk.okbt");
  	myOkButton.onclick = okfunction;
  	myOkButton.style.display="inline";
  	  	
	// width message
   	mypanel.style.width = width || "200px";
   	if (mypanel.style.position == 'fixed')	// it was not draggable
		mypanel.style.marginLeft = parseFloat(mypanel.style.width)/-2 + "px";
    
    mypanel.style.visibility = "visible";
    mybackpanel.style.visibility = "visible";
  },
  
  showOkCancelDialog: function(message, okfunction, cancelfunction, width) {
  	var mypanel = document.getElementById(".myBoxPanelCancelOk") || EJSS_INTERFACE.boxPanel(".myBoxPanelCancelOk").getDOMElement();
  	var mybackpanel = document.getElementById(".myBoxPanelCancelOk.back");

	// message
    var myMsg = document.getElementById(".myBoxPanelCancelOk.msg");
    myMsg.style.visibility = "visible";
    myMsg.innerHTML = message + "<br/>";
    
    // ok bt
  	var myOkButton =  document.getElementById(".myBoxPanelCancelOk.okbt");
  	myOkButton.onclick = okfunction;
  	myOkButton.style.display="inline";
  	
  	// cancel bt  	
  	var myCancelButton =  document.getElementById(".myBoxPanelCancelOk.cancelbt");
  	myCancelButton.onclick = cancelfunction;
  	myCancelButton.style.display="inline";
  	
	// width message
   	mypanel.style.width = width || "200px";    	
   	if (mypanel.style.position == 'fixed')	// it was not draggable
		mypanel.style.marginLeft = parseFloat(mypanel.style.width)/-2 + "px";
    
    mypanel.style.visibility = "visible";
    mybackpanel.style.visibility = "visible";
  },
  
  showInputDialog: function(message, okfunction, cancelfunction, width) {
  	var mypanel = document.getElementById(".BoxPanelInput") || EJSS_INTERFACE.boxPanel(".BoxPanelInput").getDOMElement();
  	var mybackpanel = document.getElementById(".BoxPanelInput.back");

	// message
    var myMsg = document.getElementById(".BoxPanelInput.msg");
    myMsg.style.visibility = "visible";
    myMsg.innerHTML = message;

	// input
    var myIn = document.getElementById(".BoxPanelInput.input");
    myIn.style.display="inline";
    
    // ok bt
  	var myOkButton =  document.getElementById(".BoxPanelInput.okbt");
  	myOkButton.onclick = function() { okfunction(myIn.value); };
  	myOkButton.style.display="inline";
  	
  	// cancel bt  	
  	var myCancelButton =  document.getElementById(".BoxPanelInput.cancelbt");
  	myCancelButton.onclick = cancelfunction;
  	myCancelButton.style.display="inline";
  	
	// width message
   	mypanel.style.width = width || "200px";    	
   	if (mypanel.style.position == 'fixed')	// it was not draggable
		mypanel.style.marginLeft = parseFloat(mypanel.style.width)/-2 + "px";
    
    mypanel.style.visibility = "visible";
    mybackpanel.style.visibility = "visible";
  	
  },  

  showSelectDialog: function(message, options, okfunction, cancelfunction, size, width) {
  	var mypanel = document.getElementById(".BoxPanelSelect") || EJSS_INTERFACE.boxPanel(".BoxPanelSelect").getDOMElement();
  	var mybackpanel = document.getElementById(".BoxPanelSelect.back");

	// message
    var myMsg = document.getElementById(".BoxPanelSelect.msg");
    myMsg.style.visibility = "visible";
    myMsg.innerHTML = message;

	// input
    var mySelect = document.getElementById(".BoxPanelSelect.select");
	mySelect.innerHTML = "";
	for(var i=0; i<options.text.length; i++) {
		var option = document.createElement("option");
		if(option.value && option.value[i])
			option.setAttribute("value", options.value[i]);
		option.innerHTML = options.text[i];		
		mySelect.appendChild(option);      
 	}
 	if(size) mySelect.size = size;
    mySelect.style.display="block";
    mySelect.style.margin="0 auto";
    
    // ok bt
  	var myOkButton =  document.getElementById(".BoxPanelSelect.okbt");
  	myOkButton.onclick = function() { okfunction(options.value[mySelect.selectedIndex]); };
  	myOkButton.style.display="inline";
  	
  	// cancel bt  	
  	var myCancelButton =  document.getElementById(".BoxPanelSelect.cancelbt");
  	myCancelButton.onclick = cancelfunction;
  	myCancelButton.style.display="inline";
  	
	// width message
   	mypanel.style.width = width || "200px";    	
   	if (mypanel.style.position == 'fixed')	// it was not draggable
		mypanel.style.marginLeft = parseFloat(mypanel.style.width)/-2 + "px";
    
    mypanel.style.visibility = "visible";
    mybackpanel.style.visibility = "visible";
  }  

};

/**
 * boxPanel function
 * Creates a basic boxPanel
 * @method boxPanel
 * @param name the name of the element
 * @returns A basic boxPanel
 */
EJSS_INTERFACE.boxPanel = function (mName) {
  var self = EJSS_INTERFACE.panel(mName);
  
  var offset = { x: 0, y: 0 };

  self.setVisibility = function(visible) {
 	mElement.style.visibility = visible;
 	self.getStyle().setVisibility(visible);
  };
  
  self.setDraggable = function(draggable) {
  	if(draggable) {
  		self.getDOMElement().addEventListener('mousedown', mouseDown, false);
  		self.getDOMElement().addEventListener('touchstart', mouseDown, false);
  		window.addEventListener('mouseup', mouseUp, false);
  		window.addEventListener('touchend', mouseUp, false);
  	} else {
  		self.getDOMElement().removeEventListener('mousedown', mouseDown, false);
  		self.getDOMElement().removeEventListener('touchstart', mouseDown, false);
  		window.removeEventListener('mouseup', mouseUp, false);
  		window.removeEventListener('touchend', mouseUp, false);
  	}
  }

  function mouseUp() {
 	window.removeEventListener('mousemove', popupMove, true);
 	window.removeEventListener('touchmove', popupMove, true);
  }

  function mouseDown(e){
  	var box = self.getDOMElement().getBoundingClientRect();
  	offset.x = e.clientX - box.left;
  	offset.y = e.clientY - box.top;
  	window.addEventListener('mousemove', popupMove, true);
  }

  function popupMove(e){
  	self.getDOMElement().style.position = 'absolute';
  	self.getDOMElement().style.margin = '0px';
  	var top = e.clientY - offset.y;
  	var left = e.clientX - offset.x;
  	self.getDOMElement().style.top = top + 'px';
   	self.getDOMElement().style.left = left + 'px';
  }

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.BoxPanel.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  // background
  var mBackElement = document.createElement("div");  
  document.body.appendChild(mBackElement);  
  mBackElement.className = "myBoxPanelBackground";
  mBackElement.id = mName + ".back";
  mBackElement.style.backgroundColor = "rgba(255, 255, 255, 0.3)";
  mBackElement.style.position = "fixed";
  mBackElement.style.top = "1%";
  mBackElement.style.left = "1%";
  mBackElement.style.width = "98%";
  mBackElement.style.height = "98%";  
  mBackElement.style.zIndex = "99998";
  mBackElement.style.visibility = "hidden";

  // message
  var mMsgElement = document.createElement("span");  
  mMsgElement.id = mName + ".msg";
  mMsgElement.style.visibility = "hidden";
  self.getDOMElement().appendChild(mMsgElement);  

  // input
  var mInputElement = document.createElement("input");  
  mInputElement.id = mName + ".input";
  mInputElement.type = 'text';
  mInputElement.style.display="none";
  mInputElement.size = "20";
  self.getDOMElement().appendChild(mInputElement);  

  // select
  var mSelectElement = document.createElement("select");  
  mSelectElement.id = mName + ".select";
  mSelectElement.style.display="none";
  mSelectElement.size = "4";
  self.getDOMElement().appendChild(mSelectElement);  

  // ok button
  var mOkBtElement = document.createElement("button");  
  mOkBtElement.id = mName + ".okbt";
  mOkBtElement.innerHTML = "Ok";
  mOkBtElement.style.verticalAlign = "middle";
  mOkBtElement.style.display="none";
  mOkBtElement.addEventListener('click', function(){
		document.getElementById(mName).style.visibility="hidden";
		document.getElementById(mName + ".back").style.visibility="hidden";
		document.getElementById(mName + ".msg").style.visibility="hidden";
		document.getElementById(mName + ".input").style.display="none";  		
		document.getElementById(mName + ".okbt").style.display="none";  		
		document.getElementById(mName + ".cancelbt").style.display="none";
  	});
  self.getDOMElement().appendChild(mOkBtElement);

  // cancel button
  var mCancelBtElement = document.createElement("button");  
  mCancelBtElement.id = mName + ".cancelbt";
  mCancelBtElement.innerHTML = "Cancel";
  mCancelBtElement.style.verticalAlign = "middle";
  mCancelBtElement.style.display="none";
  mCancelBtElement.addEventListener('click', function(){
		document.getElementById(mName).style.visibility="hidden";
		document.getElementById(mName + ".back").style.visibility="hidden";
		document.getElementById(mName + ".msg").style.visibility="hidden";
		document.getElementById(mName + ".input").style.display="none";
		document.getElementById(mName + ".okbt").style.display="none";  		
		document.getElementById(mName + ".cancelbt").style.display="none";
  	});
  self.getDOMElement().appendChild(mCancelBtElement);  

  // modal  
  self.getDOMElement().className = "BoxPanel";
  self.getStyle().setCSS({position:"fixed", top:"1px", left:"50%"});
  self.getStyle().setCSS({zIndex:"99999", visibility:"hidden"});
  self.getStyle().setCSS({marginLeft:"-250px", width:"500px"});
  self.getStyle().setCSS({background:"lightgray"});
  self.getStyle().setCSS({borderWidth: "1px"});
  self.getStyle().setCSS({borderStyle: "solid"});
  self.getStyle().setCSS({borderColor: "red"});  
  self.getStyle().setCSS({overflow:"hidden", wordWrap:"break-word"});
    
  self.setDraggable(true);
  return self;
};

