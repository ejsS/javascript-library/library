/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Video
 * @class Video 
 * @extends Media
 * @constructor  
 */
EJSS_INTERFACE.Video = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Media.registerProperties(element,controller); // super class

    controller.registerProperty("VideoUrl",element.setUrl);
    controller.registerProperty("Background",element.setBackground, element.getBackground)
    controller.registerProperty("PosterUrl",element.setPoster)
  },

};

/**
 * Video function
 * Creates a basic Video
 * @method video
 * @param name the name of the element
 * @returns A video element
 */
EJSS_INTERFACE.video = function (mName) {
  var self = EJSS_INTERFACE.media(mName);
  var isBackground = false;

  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.setPoster = function(url) {
    if (self.getResourcePath) url = self.getResourcePath(url);
	self.getDOMElement().poster = url;
  }	

  self.getPoster = function() {
  	return self.getDOMElement().poster;
  }	

  self.getBackground = function() {
  	return isBackground;
  }

  self.setBackground = function(back) {
  	if(back != isBackground) {
  		isBackground = back;
  		if(isBackground) {
	  		self.getStyle().setWidth("100%");
	  		self.getStyle().setHeight("100%");
	  		self.getStyle().setCSS({ "position":"absolute", "z-index":"-100" });
	  	} else {
	  		self.getStyle().setWidth(" ");
	  		self.getStyle().setHeight(" ");
	  		self.getStyle().setCSS({ "position":" ", "z-index":" " });	  		
	  	}
  	}
  } 

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Video.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("video");  
  mElement.id = mName;
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);
  self.addDOMEvents();
    
  return self;
};
