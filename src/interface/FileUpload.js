/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

/**
* EJSS framework for interface element.
* @module interface
*/

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
* FileUpload
* @class FileUpload
* @constructor
*/
EJSS_INTERFACE.FileUpload = {
 // ----------------------------------------------------
 // Static methods
 // ----------------------------------------------------

 /**
  * static registerProperties method
  */
  registerProperties : function(element,controller) {
   EJSS_INTERFACE.Button.registerProperties(element,controller); // super class

   controller.registerProperty("Accept", element.setAccept, element.getAccpet);
   controller.registerProperty("Multiple", element.setMultiple, element.getMultiple);

   controller.registerAction("OnChange");
 },

};

/**
* FileUpload function
* @method fileUpload
* @param name the name of the element
* @returns An interface element
*/
EJSS_INTERFACE.fileUpload = function (mName) {
 var self = EJSS_INTERFACE.button(mName);
 var mTarget = {};

 /**
  * @method getValue
  * @return string
  */
 self.getValue = function() {
   return mElement.value;
 };

 self.setValue = function(value) {
   mElement.value = value;
 };

 self.getTarget = function() {
     return mTarget;
 }

 // "audio/*,video/*,image/*,MIME_type"
 self.getAccept = function() {
     return self.getDOMElement().accept;
 }

 self.setAccept = function(accept) {
     self.getDOMElement().accept = accept;
 }

 self.getMultiple = function() {
     return self.getDOMElement().multiple;
 }

 self.setMultiple = function(multiple) {
     self.getDOMElement().multiple = multiple;
 }
 
 self.click = function() {
   	 mElement.click(); 	
 }

 /**
  * Extended registerProperties method. To be used by promoteToControlElement
  * @method registerProperties
  */
 self.registerProperties = function(controller) {
   EJSS_INTERFACE.FileUpload.registerProperties(self,controller);
 };

 // ----------------------------------------------------
 // Final start-up
 // ----------------------------------------------------

 var mElement = document.createElement("input");
 mElement.id = mName+".file";
 mElement.type = 'file';
 mElement.style.display = 'none';
 self.getDOMElement().parentNode.appendChild(mElement);
 
 self.getDOMElement().onmousedown = function(e) {
   mElement.click();
   e.preventDefault(); e.stopPropagation();
   return false;
 }

  self.getDOMElement().addEventListener("touchend", function(e) {
   mElement.click();
   e.stopPropagation();
   return false;   
  }, {passive: true});

  mElement.addEventListener("change", function(e) {
   mTarget = event.target;
   var controller = self.getController();
   if (controller) {
       controller.invokeAction("OnChange");
       controller.reportInteractions();
   }
  }, {passive: true});

 return self;
};