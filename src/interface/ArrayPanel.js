/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * ArrayPanel
 * @class ArrayPanel 
 * @constructor  
 */
EJSS_INTERFACE.ArrayPanel = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Editable", element.setEditable, element.getEditable);
	controller.registerProperty("Transpose", element.setTranspose, element.getTranspose);
	controller.registerProperty("DataArray", element.setDataArray, element.getDataArray);
    
    controller.registerProperty("ColumnsWidth", element.setColumnsWidth, element.getColumnsWidth);
    controller.registerProperty("HeadersText", element.setHeadersText, element.getHeadersText);
    controller.registerProperty("HeadersCSS", element.setHeadersCSS, element.getHeadersCSS);
    controller.registerProperty("CellsFormat", element.setCellsFormat, element.getCellsFormat);    
    controller.registerProperty("CellsCSS", element.setCellsCSS, element.getCellsCSS);

	controller.registerProperty("BodyHeight", element.setBodyHeight, element.getBodyHeight);
	controller.registerProperty("EditableBkgd", element.setEditableBkgd, element.getEditableBkgd);
	controller.registerProperty("NonEditableBkgd", element.setNonEditableBkgd, element.getNonEditableBkgd);

	controller.registerAction("OnChange",element.getElementInteracted); 
  },

};

/**
 * ArrayPanel function
 * ArrayPanel implements dataCollected(), which makes it a Collector of data 
 * @see _view._collectData
 * @method ArrayPanel
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.arrayPanel = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  // Configuration variables
  var mTranspose=false;
  var mColumnsWidth;
  var mHeadersText;
  var mCellsFormat;
  var mHeadersCSS;
  var mCellsCSS;
  var mValues;
  var mEditable;
  var mEditableBkgd="White";
  var mNonEditableBkgd="LightGrey";
  var mElementInteracted = { row: -1, column: -1};

  // Implementation variables
  var mMustRecreate = true;
  
  var mEdition = false;
  var mTmp = "";
  var mBackColor = "";

  self.enableEPub = function() {
  	if (self.getDOMElement().disabled) self.getDOMElement().disabled = false;
  }
   
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  /**
   * @method setTranspose
   * @param transpose
   */
  self.setTranspose = function(transpose) {
    if (mTranspose!=transpose) {
   	  mTranspose = transpose;
  	  mMustRecreate = true;
  	}
  };

  /**
   * @method getTranspose
   */
  self.getTranspose = function() { 
    return mTranspose; 
  }; 

  /**
   * @method setEditable
   * @param editable
   */
  self.setEditable = function(editable) {
   	mEditable = editable;
  };

  /**
   * @method getEditable
   * @return editable
   */
  self.getEditable = function() { 
    return mEditable; 
  }; 

  /**
   * @method setDataArray
   * @param values
   */
  self.setDataArray = function(values) {
  	if (!EJSS_TOOLS.compareArrays(mValues,values)) {
   	  mValues = values.slice();
  	  mMustRecreate = true;
  	}
  };

  /**
   * @method getDataArray
   * @return values
   */
  self.getDataArray = function() { 
    return mValues; 
  }; 
  
  /**
   * @method setColumnsWidth
   * @param names
   */
  self.setColumnsWidth = function(width) {
   	mColumnsWidth = width;
  };

  /**
   * @method getColumnsWidth
   * @return names
   */
  self.getColumnsWidth = function() { 
    return mColumnsWidth; 
  }; 
  
  /**
   * @method setHeadersText
   * @param names
   */
  self.setHeadersText = function(names) {
   	mHeadersText = names;
  };

  /**
   * @method getHeadersText
   * @return names
   */
  self.getHeadersText = function() { 
    return mHeadersText; 
  };    

  /**
   * @method setCellsFormat
   * @param format
   */
  self.setCellsFormat = function(format) {
   	mCellsFormat = format;
  };

  /**
   * @method getCellsFormat
   * @return format
   */
  self.getCellsFormat = function() { 
    return mCellsFormat; 
  };    

  /**
   * @method setHeadersCSS
   * @param css Object
   */
  self.setHeadersCSS = function(css) {
   	mHeadersCSS = css;
  };
 
 /**
   * @method getHeadersCSS
   * @return Object
   */
  self.getHeadersCSS = function() { 
    return mHeadersCSS; 
  };    
    
  /**
   * @method setCellsCSS
   * @param css Object
   */
  self.setCellsCSS = function(css) {
   	mCellsCSS = css;
  };
 
 /**
   * @method getCellsCSS
   * @return Object
   */
  self.getCellsCSS = function() { 
    return mCellsCSS; 
  };    
  
  /**
  * Sets the color for editable cells
  * @method setEditableBkgd
  * @param color The color for editable cells
  */
  self.setEditableBkgd = function(color) {
    if (mEditableBkgd!=color) {
      mEditableBkgd = color;
      mMustRecreate = true;
    }
  }

  /**
  * Gets the color for editable cells
  * @method getEditableBkgd
  * @return The color for editable cells
  */
  self.getEditableBkgd = function(color) {
    return mEditableBkgd;
  }
  
  /**
  * Sets the color for non editable cells
  * @method setNonEditableBkgd
  * @param color The color for non editable cells
  */
  self.setNonEditableBkgd = function(color) {
    if (mNonEditableBkgd!=color) {
      mNonEditableBkgd = color;
      mMustRecreate = true;
    }
  }

  /**
  * Gets the color for non editable cells
  * @method getNonEditableBkgd
  * @return The color for non editable cells
  */
  self.getNonEditableBkgd = function(color) {
    return mNonEditableBkgd;
  }

    /**
   * Set the width 
   * @method setBodyHeight
   * @param value string or int
   */
  self.setBodyHeight = function(value) { 
    if (typeof value !== "string") value = value + "px";
    tbdy.style.height=value;
  };

  /**
   * Get the width 
   * @method getBodyHeight
   * @return the height
   */
  self.getBodyHeight = function() { 
    return tbdy.style.height;
  };
  
  /**
   * Get the last element of the array interacted by the user 
   * @method getElementInteracted
   * @return An object with two properties: cell.row, cell.column
   */
  self.getElementInteracted = function() { 
    return mElementInteracted;
  };
  
  
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.ArrayPanel.registerProperties(self,controller);
  };
  
  self.enableEPub = function() {
  	if (self.getDOMElement().disabled) self.getDOMElement().disabled = false;
  };

  self.render = function() {
    if (mMustRecreate && !mEdition) {
      createTable();
      mMustRecreate = false;
    }
    if (typeof _isEPub !== 'undefined' && _isEPub)
     self.enableEPub();
  }  
  
  // ----------------------------------------
  // Operation methods
  // ----------------------------------------
        
  function createTable() {
    //console.log("Creating table");
   	tbdy.innerHTML = '';
   	thead.innerHTML = '';

	if (mHeadersText) { // column names
      var tr = document.createElement('tr');
      tr.style.border = "inherit";
      var n = mHeadersText.length;            
      for (var i=0; i<n; i++) {
        var th = document.createElement('th');
        if (mColumnsWidth && mColumnsWidth[i]) {
	      if (typeof mColumnsWidth[i] === "number") 
	      	th.style.width = mColumnsWidth[i] + "px";
	      else if (typeof mColumnsWidth[i] === "string") 
	      	th.style.width[i] = mColumnsWidth[i];
	     }
	    if (mHeadersCSS && mHeadersCSS[i]) 
	     	EJSS_INTERFACE.Style.setCSS(th,mHeadersCSS[i]);
		th.innerHTML = mHeadersText[i];		        	
      	tr.appendChild(th);
      }
      if(n == 1) th.colSpan = "100%";      
      thead.appendChild(tr);
    }

    if (mValues) { // column values
      var rows=0, cols=0;
      if (mTranspose) {
        cols = mValues.length;
	    for (var i=0; i<cols; i++) rows = Math.max(rows,mValues[i].length);
      }
      else {
        rows = mValues.length;
	    for (var i=0; i<rows; i++) cols = Math.max(cols,mValues[i].length);
      }
      if (rows<=0 || cols<=0) return;
	  for (var i=0; i<rows; i++) {
	    var tr = document.createElement('tr');
	    tr.style.border = "inherit";
	    for (var j=0; j<cols; j++) {
	      var td = document.createElement('td');
	      if (mColumnsWidth && mColumnsWidth[j]) {
		    if      (typeof mColumnsWidth[j] === "number") td.style.width = mColumnsWidth[j] + "px";
		    else if (typeof mColumnsWidth[j] === "string") td.style.width = mColumnsWidth[j];
		  }
	      var format; // (mCellsFormat && (typeof mCellsFormat[j] !== "undefined"))? mCellsFormat[j] : mCellsFormat;
	      if (mCellsFormat) {
	       if (mCellsFormat instanceof Array) format = mCellsFormat[j];
	       else format = mCellsFormat;
	      } 	
	      var editable; // = ((!mEditable) || mEditable[j]);
	      if (mEditable instanceof Array) {
	        editable = mEditable[j];
	      }
	      else if (typeof mEditable === "undefined") {
	        editable = true;
	      }
	      else editable = mEditable;
 	      if (mTranspose) {
	        if (i>=mValues[j].length) editable = false;
	        else td.innerHTML = formatValue(mValues[j][i],format);
	      }
	      else {
	        if (j>=mValues[i].length) editable = false;
	        else td.innerHTML = formatValue(mValues[i][j],format); // Not transpose
	      }
	      if (editable) {
	        td.contentEditable = true;
	       	td.onblur = self.handleOnBlur;
	       	td.onfocus = self.handleOnFocus;
	       	td.onkeydown = self.handleOnKeydown;
	        td.style.backgroundColor = mEditableBkgd;
	      }
	      else td.style.backgroundColor = mNonEditableBkgd;
	      if (mCellsCSS && mCellsCSS[j]) EJSS_INTERFACE.Style.setCSS(td,mCellsCSS[j]);
	      tr.appendChild(td);
	    }
	    tbdy.appendChild(tr);
      }
    }

  }
  
  self.handleOnBlur = function(ev) {
  	var target = ev.target || ev.srcElement;
  	if(mEdition) {
  		mEdition = false;
  		checkValue(target);
  	}
  }
  
  self.handleOnFocus = function(ev) {
  	var target = ev.target || ev.srcElement;
  	mTmp = target.innerHTML;
  	mBackColor = target.style.backgroundColor;
  }
  
  self.handleOnKeydown = function(key) {
  	var ev = key || window.event;
  	var charCode = (ev.which) ? ev.which : ev.keyCode;  
  	var target = ev.target || ev.srcElement;	  
    // console.log("Key = "+charCode);
	if (charCode==13) { // return
		mEdition = false;
		checkValue(target);  	
		mTmp = target.innerHTML;		
	} else if (charCode==27) { // escape
		target.innerHTML = mTmp;
	} else {
		target.style.backgroundColor = "yellow";
		mEdition = true;
	}
  }
  
  function checkValue(td) { 
    var rex = /(<([^>]+)>)/ig;
    var value = td.innerHTML.replace(rex , "");  // remove HTML tag
    var parsedValue = parseFloat(value);
    if (!isNaN(parsedValue) && isFinite(value)) {
      td.style.backgroundColor = mBackColor;
      if(mTmp != value) {
      	  // set value
          var cellIndex  = td.cellIndex;  
          var rowIndex = td.parentNode.rowIndex;
          if(mHeadersText) rowIndex-=1;  
          if (mTranspose)  mValues[cellIndex][rowIndex] = parsedValue;	
      	  else mValues[rowIndex][cellIndex] = parsedValue; 
      	  
		  var controller = self.getController();    		
		  if (controller) {
			controller.propertiesChanged("DataArray");
			if (mTranspose) {
			  mElementInteracted.row = cellIndex;
			  mElementInteracted.column = rowIndex;
			}
			else{
			  mElementInteracted.row = rowIndex;
			  mElementInteracted.column = cellIndex;
			}
			controller.invokeAction("OnChange");
			controller.reportInteractions();
		  }
	  }	    
    }
    else
      td.style.backgroundColor = "red";
  };
  
  function formatValue(value, format) {  	  
  	  if (typeof format != "undefined" && format.length > 0) { // format
		var index = format.indexOf('.');
  		var indexE = format.toUpperCase().indexOf('E');
		if (indexE == -1) { // decimal notation
	  	  var digits = 0;
	  	  if (index >= 0) digits = Number(format.length-index-1);
	  	  return parseFloat(value).toFixed(digits);
		} 
		else { // scientific notation
	  	  var digits = 0;
	  	  if (index >= 0) digits = Number(indexE-index-1);
		  return parseFloat(value).toExponential(digits);	
		}
	  } 
	  return value; 
	}
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement('table');
  var tbdy = document.createElement('tbody');
  var thead = document.createElement('thead');
  mElement.style.display="inline-block";
  mElement.appendChild(thead);
  mElement.appendChild(tbdy);

  mElement.id = mName;
  document.body.appendChild(mElement);  
  mElement.className = "ArrayPanel";
  self.setDOMElement(mElement);
    
  return self;
};

