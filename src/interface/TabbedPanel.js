/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * TabbedPanel
 * @class TabbedPanel 
 * @constructor  
 */
EJSS_INTERFACE.TabbedPanel = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Titles", element.setTitles, element.getTitles);
    controller.registerProperty("Selected", element.setSelected, element.getSelected);

    
	  controller.registerProperty("Font",  element.setFont);
	  controller.registerProperty("Foreground",  element.setColor);
      controller.registerProperty("Background",  element.setBackgroundColor);
      controller.registerProperty("FillColor", element.setFillColor);

  },

};

/**
 * TabbedPanel function
 * @method TabbedPanel
 * @param name the name of the element
 * @returns A panel with tabs
 */
EJSS_INTERFACE.tabbedPanel = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mTitles = [];
  var mSelected = 0;
  
  var mBackground = null;

  /**
   * @method appendChild
   * @param name
   */
  self.appendChild = function(element) {
  	var child = element.getDOMElement();
    mDIVElement.appendChild(child);       
    select();   
  };

  /**
   * @method removeChild
   * @param name
   */
  self.removeChild = function(element) {
  	var child = element.getDOMElement();
    mDIVElement.removeChild(child);      
    select();    
  };

  /**
   * @method insertBefore
   * @param name
   */
  self.insertBefore = function(newele,refele) {
  	var newchild = newele.getDOMElement();
  	var refchild = refele.getDOMElement();
    mDIVElement.insertBefore(newchild,refchild);    
    select();   
  };
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.getTitles = function() {
    return mTitles;
  };

  self.setTitles = function(items) {
    if (!EJSS_TOOLS.compareArrays(mTitles,items)) {
    	mTitles = items;
        updateTitles();
    }
  };

  self.getSelected = function() {
    return mSelected;
  };

  self.setSelected = function(index) {
    if (mSelected != index) {
    	mSelected = index;
        select();
    }
  };

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.TabbedPanel.registerProperties(self,controller);
  };

  function updateTitles() {
	mTABElement.innerHTML = "";

	for(var i=0; i<mTitles.length; i++) {
		var title = mTitles[i];
		
	    var mLIElement = document.createElement("li");  
	    mLIElement.className="_TabbedPanel.li";
		mLIElement.id = mName + ".item";
	    mTABElement.appendChild(mLIElement);
	    mLIElement.style.background = mBackground ? mBackground : self.getStyle().getBackgroundColor();

		mLIElement.onmouseover = function () {
			this.style.cursor = "pointer";
		};

		mLIElement.onmouseout = function () {
			this.style.cursor = "auto";
		};
	    
	    var mAElement = document.createElement("a");  
	    mAElement.className="_TabbedPanel.a";
		mAElement.id = mName + ".item.a." + i;
		// mAElement.style = "user-select: none; display: inline-block; color: black; text-align: center; padding: 7px 8px; text-decoration: none; transition: 0.3s; font-size: 14px;";
		mAElement.innerHTML = title;
	    mLIElement.appendChild(mAElement);
	    mAElement.style.color = self.getStyle().getColor();
	    mAElement.style.fontStyle = self.getStyle().getFontStyle();
	    mAElement.style.fontWeight = self.getStyle().getFontWeight();
	    mAElement.style.fontSize = self.getStyle().getFontSize();
	    mAElement.style.fontFamily = self.getStyle().getFontFamily();
	    mAElement.style.lineHeight = self.getStyle().getLineHeight();
				
	  	mAElement.onclick = function() {
		    var parts = this.id.split(".");
    		mSelected = parseInt(parts[parts.length-1]);
	  		select();
	  	};
	  	select();
	}
  }

  function select() {
    // active panel
    for (var j = 0; j < mDIVElement.childNodes.length; j++) {
        mDIVElement.childNodes[j].style.display = "none";
    }
    if (mDIVElement.childNodes.length > mSelected)
    	mDIVElement.childNodes[mSelected].style.display = "block";
			
    // active tab
    for (var j = 0; j < mTABElement.childNodes.length; j++) {
        mTABElement.childNodes[j].firstChild.style.backgroundColor = "transparent";
    }
    if (mTABElement.childNodes.length > mSelected)
		mTABElement.childNodes[mSelected].firstChild.style.backgroundColor =
			self.getStyle().getFillColor();
  }

  // ----------------------------------------------------
  // Override parent behaviour
  // ----------------------------------------------------

  /**
   * @method setBackgroundColor
   * @param color
   */
  self.setFont = function(font) {
	  self.getStyle().setFont(font);
	  updateTitles();
  };

  /**
   * @method setBackgroundColor
   * @param color
   */
  self.setFillColor = function(color) {
	  self.getStyle().setFillColor(color);
	  select();
  };

  /**
   * @method setBackgroundColor
   * @param color
   */
  self.setBackgroundColor = function(color) {
	  mBackground = color;
	  updateTitles();
  };
  
  /**
   * @method setColor
   * @param color
   */
  self.setColor = function(color) {
	  self.getStyle().setColor(color);
	  updateTitles();
  };


  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("span");
  mElement.className="_TabbedPanel.span";
  mElement.id = mName;
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);

  var mTABElement = document.createElement("ul");  
  mTABElement.className="_TabbedPanel.ul";
  mTABElement.id = mName + ".ul";
  mElement.appendChild(mTABElement);

  var mDIVElement = document.createElement("span");  
  mDIVElement.id = mName + ".divs";
  mElement.appendChild(mDIVElement);  

  updateTitles();
  self.getStyle().setFillColor("#ccc");

  return self;
};

