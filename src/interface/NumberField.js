/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * NumberField
 * @class NumberField 
 * @constructor  
 */
EJSS_INTERFACE.NumberField = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

    controller.registerProperty("Value", element.setValue, element.getValue);
    controller.registerProperty("Editable", element.setEditable);
    controller.registerProperty("Format", element.setFormat);
    controller.registerProperty("Size", element.setSize);
    
    controller.registerAction("OnChange"); // ,element.getValue); // Not really needed and causes problems with remote models
  },

};

/**
 * NumberField function
 * @method numberField
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.numberField = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mDigits = 3;
  var mCNotation = false;
  var mEdition = false;
  var mTmp = "";
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------


  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.NumberField.registerProperties(self,controller);
  };
  
  self.enableEPub = function() {
  	if (self.getDOMElement().disabled) self.getDOMElement().disabled = false;
  };

  if (typeof _isEPub !== 'undefined' && _isEPub) {
    self.render = function() {
      self.enableEPub();
    }
  };
  
  // ----------------------------------------
  // Setters and getters
  // ----------------------------------------
  
  /**
   * Set the value displayed by the element
   * @method setValue
   * @param value double
   */
  self.setValue = function(value) { 
  	if(!mEdition) {  	
	    if (typeof value == "undefined") 
	    	self.getDOMElement().value = "value undefined";
	    else {
			if (!isNaN(parseFloat(value)) && isFinite(value)) {
	    		if(mCNotation) {
	    			self.getDOMElement().value = parseFloat(value).toExponential(mDigits);	
	    		} else {   	
	    			self.getDOMElement().value = parseFloat(value).toFixed(mDigits);
	    		}	    	
	    	} else if (value === Number.POSITIVE_INFINITY) {
	    		self.getDOMElement().value = "∞";
	    	} else if (value === Number.NEGATIVE_INFINITY) {
	    		self.getDOMElement().value = "-∞";
			} else if (isNaN(value)) {
				self.getDOMElement().value = "∅";
			}
	    }
	}
  };

  /**
   * Get the value displayed by the element
   * @method getValue
   * @return double
   */
  self.getValue = function() {
  	var value = self.getDOMElement().value;
	if (!isNaN(parseFloat(value)) && isFinite(value)) {   	       	       
  	  	value = parseFloat(value);
    } else if (value === "∞") {
		value = Number.POSITIVE_INFINITY;
	} else if (value === "-∞") {
		value = Number.NEGATIVE_INFINITY;
	} else if (value === "∅") {
		value = Number.NaN;
  	}      	    	  	 
    return value; 
  };

  /**
   * Set the editable property
   * @method setEditable
   * @param editable boolean
   */
  self.setEditable = function(editable) { 
    self.getDOMElement().readOnly = !editable; 
    if (editable) self.getStyle().setBackgroundColor("white");
    else self.getStyle().setBackgroundColor("lightgrey");
  };

  /**
   * @method getEditable
   * @return bool
   */
  self.getEditable = function() { 
    return !self.getDOMElement().readOnly; 
  };

  /**
   * Set the size 
   * @method setSize
   * @param value string
   */
  self.setSize = function(value) { 
    self.getDOMElement().size = value; 
  };

  /**
   * Get the size
   * @method getSize
   * @return double
   */
  self.getSize = function() { 
    return self.getDOMElement().size; 
  };

  /**
   * Set the format with which to display the value
   * @method setFormat
   * @param format String
   */
  self.setFormat = function(format) {
  	var index = format.indexOf('.');
  	var indexE = format.toUpperCase().indexOf('E');
	if (indexE == -1) { // decimal notation
	  	var digits = 0;
	  	if (index >= 0) digits = Number(format.length-index-1);
	    if (mCNotation || mDigits != digits) {
	      mDigits = digits;
	      mCNotation = false;
	      self.setValue(self.getValue());
	    }
	} else { // scientific notation
	  	var digits = 0;
	  	if (index >= 0) digits = Number(indexE-index-1);
	    if (!mCNotation || mDigits != digits) {
	      mDigits = digits;
	      mCNotation = true;
	      self.setValue(self.getValue());
	    }			
	} 
  };

  function checkValue() {
    var value = self.getDOMElement().value;
    if (!isNaN(parseFloat(value)) && isFinite(value)) {
      self.getStyle().setBackgroundColor("white");
      if(mTmp != value) {
		  var controller = self.getController();    		
		  if (controller) {
			controller.propertiesChanged("Value");
			controller.invokeAction("OnChange");
			controller.reportInteractions();
		  }
	  }	    
    }
    else if (value === "∞" || value === "-∞" || value === "∅")
      self.getStyle().setBackgroundColor("white");
    else {
      self.getStyle().setBackgroundColor("red");
      return false; 
    }      
    return true;
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("input");  
  // mElement.type = 'text';
  mElement.type = 'number';	// directly numeric keyboard in tablets and mobiles  
  mElement.id = mName;
  mElement.value = 0.0;
  document.body.appendChild(mElement);  
  self.setDOMElement(mElement);

  self.getDOMElement().onkeydown = function(key) {  	
    var dom = self.getDOMElement();
  	if(!dom.readOnly) {
  	  	var ev = key || window.event;
	  	var charCode = (ev.which) ? ev.which : ev.keyCode;  	  
  	    // console.log("Key = "+charCode);
  		if (charCode==13) { // return
  			mEdition = false;
  			if (checkValue())
  			  mTmp = self.getDOMElement().value;		
  		} else if (charCode==27) { // escape
  			self.getDOMElement().value = mTmp;
  		} else {
  			self.getStyle().setBackgroundColor("yellow");
  			mEdition = true;
  		}
    }
  }

  self.getDOMElement().onfocus = function(dom) {
  	mTmp = self.getDOMElement().value;
  }

  self.getDOMElement().onblur = function(dom) {
  	if(!dom.readOnly && mEdition) {
  		mEdition = false;
  		checkValue();
  	}
  }

  self.getDOMElement().ontouchend = function(e) {
    e.stopPropagation();
  }      

  self.getDOMElement().onmouseup = function(e) {
    e.stopPropagation();
  }

  // associate event for ipad
  if (typeof _isEPub != "undefined" && _isEPub) {
	  self.getDOMElement().ontouchstart = function(event) {
  	    if (self.getDOMElement().disabled) self.getDOMElement().disabled = false;
	  	event.stopPropagation();
    };
  }

  return self;
};

