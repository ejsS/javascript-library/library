/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Style object for 2D drawing
 * @class Style 
 * @constructor  
 */
EJSS_INTERFACE.Style = { 
  BORDER_SOLID: "solid",
  BORDER_DOTTED: "dotted",
  BORDER_DASHED: "dashed",
  
  TEXTA_LEFT : "left", 				// Aligns the text to the left 	
  TEXTA_RIGHT : "right", 			// Aligns the text to the right 
  TEXTA_CENTER : "center", 			// Centers the text 	
  TEXTA_JUSTIFY : "justify", 		// Stretches the lines so that each line has equal width (like in newspapers and magazines)  	
  
  VISIB_VISIBLE: "visible",
  VISIB_HIDDEN: "hidden",
       
  FLOAT_LEFT :  "left",
  FLOAT_RIGHT:  "right",
  FLOAT_NONE: "none",
  FLOAT_CENTER: "none",
  	
  POSITION_STATIC : "static",		// Elements render in order, as they appear in the document flow 		
  POSITION_ABSOLUTE: "absolute", 	// The element is positioned relative to its first positioned (not static) ancestor element	
  POSITION_FIXED: "fixed",			// The element is positioned relative to the browser window 	
  POSITION_RELATIVE: "relative", 	// The element is positioned relative to its normal position, so "left:20" adds 20 pixels to the element's LEFT position	
  POSITION_INHERIT: "inherit", 		// The value of the position property is inherited from the parent element

  OVERFLOW_VISIBLE: "visible", 		// The overflow is not clipped. It renders outside the element's box. This is default 		
  OVERFLOW_HIDDEN: "hidden", 		// The overflow is clipped, and the rest of the content will be invisible 	
  OVERFLOW_SCROLL: "scroll", 		// The overflow is clipped, but a scroll-bar is added to see the rest of the content 		
  OVERFLOW_AUTO: "auto", 			// If overflow is clipped, a scroll-bar should be added to see the rest of the content
  	       

  setCSS: function (element,css) {
	var rdashAlpha = /-([\da-z])/gi;
	var fcamelCase = function(all, letter) {return ( letter + "" ).toUpperCase();};

    var style = element.style;
    if(typeof css !== 'object') {
    	// CSS with addition
    	var cssvals = css.trim().split('{');
    	if(cssvals[0] && (cssvals[0].length > 0)) { // for example ".myBorder { border-width: 5px }" 
    		var classname = cssvals[0].trim();			
	    	var x, classes = document.styleSheets[0].rules || document.styleSheets[0].cssRules; // fails in local tests, use web server (CORS)
	    	for(x = 0; x<classes.length; x++) {
	        	if(classes[x].selectorText == classname) break;
	    	}
	    	// set values
	    	var classesCss = (classes[x].cssText)? classes[x].cssText:classes[x].style.cssText; 
	    	classesCss = classesCss.match(/{.*}/g)[0]; // properties
		    var css_rows = classesCss.slice(1, classesCss.length-2).split(';');    	
			for (elem in css_rows) {
	        	var elem_parts = css_rows[elem].split(':');
	        	if(elem_parts.length == 2) {
	        		var property_name = elem_parts[0].trim();
	        		var property_value = elem_parts[1].trim();
	        	
	        		var camelCase = property_name.replace(rdashAlpha, fcamelCase);
	        		style[camelCase]=property_value;
	        	}        	
			}	    
		}
		if(cssvals[1]) { // for example "{ border-width: 5px }" 
			var addition = cssvals[1].trim();  
		    var css_rows = addition.slice(0, addition.length-2).split(';');    	
			for (elem in css_rows) {
	        	var elem_parts = css_rows[elem].split(':');
	        	if(elem_parts.length == 2) {
	        		var property_name = elem_parts[0].trim();
	        		var property_value = elem_parts[1].trim();
	        	
	        		var camelCase = property_name.replace(rdashAlpha, fcamelCase);
	        		style[camelCase]=property_value;
	        	}        	
			}	    				
		}
	} else {
		// object, for example {"xx":"value",...}
	    for (var prop in css) {
	    	if(typeof css[prop] !== 'object') { // property for current element
				var camelCase = prop.replace(rdashAlpha, fcamelCase);
				style[camelCase]=css[prop];
			} else { // property for child element, for example {".checkbox": {"xx":"value",...} }
				try {
					var childelement = document.getElementById(element.id + prop);
					var childcss = css[prop];
					var childstyle = childelement.style;
	    			for (var childprop in childcss) {
						var camelCase = childprop.replace(rdashAlpha, fcamelCase);
						childstyle[camelCase]=childcss[childprop];
	    			}
				} catch(error) {
  					console.error(error);
  				}
			}
	    }
	}				
  	
  }  	       
};

/**
 * Creates a Style object for 2D drawing
 */
EJSS_INTERFACE.style = function (mName) {
  var Style = EJSS_INTERFACE.Style;
  var self = {};

  var mBorderColor = "white";
  var mBorderWidth = 0;
  var mBorderStyle = Style.BORDER_SOLID;  


  //---------------------------------
  // General
  //---------------------------------  

  /**
   * Sets a number of CSS properties
   * @param css object
   */ 
  self.setCSS = function (css) {
  	Style.setCSS(document.getElementById(mName),css);
  };
  

  /**
   * Set the visibility 
   * @method setVisibility
   * @param visible boolean
   */
  self.setVisibility = function(visible) { 
    document.getElementById(mName).style.visibility = visible ? "visible" : "hidden";
  	//if (value.substring(0,6) == "VISIB_") value = Style[value.toUpperCase()];
	//document.getElementById(mName).style.visibility = value; 
  };

  /**
   * Whether the element is visible
   * @method getVisibility
   * @return boolean
   */
  self.getVisibility = function() { 
    return document.getElementById(mName).style.visibility=="visible"; 
  };

  //---------------------------------
  // Align
  //---------------------------------  

  /**
   * Set the vertical align 
   * @method setVerticalAlign
   * @param value string
   */
  self.setVerticalAlign = function(value) { 
	document.getElementById(mName).style.verticalAlign = value; 
  };

  /**
   * Get the vertical align
   * @method getVerticalAlign
   * @return string
   */
  self.getVerticalAlign = function() { 
    return document.getElementById(mName).style.verticalAlign; 
  };


  /**
   * Set the text align 
   * @method setTextAlign
   * @param value string
   */
  self.setTextAlign = function(value) { 
  	if (value.substring(0,6) == "TEXTA_") value = Style[value.toUpperCase()];
	document.getElementById(mName).style.textAlign = value; 
  };

  /**
   * Get the text align
   * @method getTextAlign
   * @return string
   */
  self.getTextAlign = function() { 
    return document.getElementById(mName).style.textAlign; 
  };

  //---------------------------------
  // Size
  //---------------------------------  

  /**
   * @method setLineHeight
   * @param value string or int
   */
  self.setLineHeight = function(value) { 
    if (typeof value !== "string") value = value + "px";
    document.getElementById(mName).style.lineHeight = value;
  };

  /**
   * @method getLineHeight
   * @return height
   */
  self.getLineHeight = function() { 
    return document.getElementById(mName).style.lineHeight; 
  };
  
  /**
   * @method setHeight
   * @param value string or int
   */
  self.setHeight = function(value) { 
    if (typeof value !== "string") value = value + "px";
    document.getElementById(mName).style.height = value;
  };

  /**
   * @method getHeight
   * @return height
   */
  self.getHeight = function() { 
    return document.getElementById(mName).style.height; 
  };

  /**
   * Set the width 
   * @method setWidth
   * @param value string or int
   */
  self.setWidth = function(value) { 
    if (typeof value !== "string") value = value + "px";
    document.getElementById(mName).style.width = value;
  };

  /**
   * Get the width
   * @method getWidth
   * @return string
   */
  self.getWidth = function() { 
    return document.getElementById(mName).style.width; 
  };

  //---------------------------------
  // position
  //---------------------------------
 
  /**
   * @method setOverflow
   * @param overflow
   */
  self.setOverflow = function(overflow) {
    if (overflow.substring(0,6) == "OVERFL") overflow = Style[overflow.toUpperCase()];
    document.getElementById(mName).style.overflow = overflow;       
  };
  
  /**
   * @method getOverflow
   * @return overflow
   */
  self.getOverflow = function() { 
    return document.getElementById(mName).style.overflow;
  };
    
  /**
   * @method setPosition
   * @param position
   */
  self.setPosition = function(position) {
    if (position.substring(0,6) == "POSITI") position = Style[position.toUpperCase()];
    document.getElementById(mName).style.position = position;       
  };
  
  /**
   * @method getPosition
   * @return position
   */
  self.getPosition = function() { 
    return document.getElementById(mName).style.position;
  };
  
  /**
   * @method setFloat
   * @param floatstyle
   */
  self.setFloat = function(floatstyle) {
    if (floatstyle.substring(0,6) == "FLOAT_") floatstyle = Style[floatstyle.toUpperCase()];
    document.getElementById(mName).style.float = floatstyle;       
  };
  
  /**
   * @method getFloat
   * @return float
   */
  self.getFloat = function() { 
    return document.getElementById(mName).style.float;
  };

  /**
   * @method setDisplay
   * @param displayStyle
   */
  self.setDisplay = function(displayStyle) {
    document.getElementById(mName).style.display = displayStyle;       
  };
  
  /**
   * @method getDisplay
   * @return displayStyle
   */
  self.getDisplay = function() { 
    return document.getElementById(mName).style.display;
  };
  
  /**
   * @method setTop
   * @param top
   */
  self.setTop = function(top) { 
    document.getElementById(mName).style.top = top;
  };

  /**
   * @method getTop
   * @return top
   */
  self.getTop = function() { 
    return document.getElementById(mName).style.top; 
  };

  /**
   * @method setBottom
   * @param bottom
   */
  self.setBottom = function(bottom) { 
    document.getElementById(mName).style.bottom = bottom;
  };

  /**
   * @method getBottom
   * @return left
   */
  self.getBottom = function() { 
    return document.getElementById(mName).style.bottom; 
  };

  /**
   * @method setLeft
   * @param left
   */
  self.setLeft = function(left) { 
    document.getElementById(mName).style.left = left;
  };

  /**
   * @method getLeft
   * @return left
   */
  self.getLeft = function() { 
    return document.getElementById(mName).style.left; 
  };
     
  //---------------------------------
  // borders, margin, shadow, padding
  //---------------------------------

  /**
   * @method setBorderStyle
   * @param style
   */
  self.setBorderStyle = function(style) {
    if (style.substring(0,6) == "BORDER") style = Style[style.toUpperCase()];
    mBorderStyle = style;
    document.getElementById(mName).style.border = mBorderWidth + "px " + mBorderStyle + " " + mBorderColor;       
  };
  
  /**
   * @method getBorderStyle
   * @return style
   */
  self.getBorderStyle = function() { 
    return mBorderStyle;
  };

  /**
   * @method setBorderWidth
   * @param width
   */
  self.setBorderWidth = function(width) {
    mBorderWidth = width;
    document.getElementById(mName).style.border = mBorderWidth + "px " + mBorderStyle + " " + mBorderColor;
  };
  
  /**
   * @method getBorderWidth
   * @return width
   */
  self.getBorderWidth = function() { 
    return mBorderWidth; 
  };

  /**
   * @method setBorderColor
   * @param color
   */
  self.setBorderColor = function(color) {
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    mBorderColor = color;
    document.getElementById(mName).style.border = mBorderWidth + "px " + mBorderStyle + " " + mBorderColor;
  };
  
  /**
   * @method getBorderColor
   * @return color
   */
  self.getBorderColor = function() { 
    return mBorderColor; 
  };

  /**
   * @method setPadding
   * @param padding
   */
  self.setPadding = function(padding) {
    document.getElementById(mName).style.padding = padding;
  };
  
  /**
   * @method getPadding
   * @return padding
   */
  self.getPadding = function() { 
    return document.getElementById(mName).style.padding; 
  };

  /**
   * @method setShadow
   * @param boolean
   */
  self.setShadow = function(shadow) {
    if (shadow)
  	  document.getElementById(mName).style.boxShadow = shadow; //"4px 4px 8px #888888";
    else
  	  document.getElementById(mName).style.boxShadow = "";
  };

  /**
   * @method getShadow
   * @return bool
   */
  self.getShadow = function() { 
    return (document.getElementById(mName).style.boxShadow != ""); 
  };

  /**
   * @method setMargin
   * @param margin
   */
  self.setMargin = function(margin) { 
    document.getElementById(mName).style.margin = margin;
  };

  /**
   * @method getMargin
   * @return width
   */
  self.getMargin = function() { 
    return document.getElementById(mName).style.margin; 
  };


  //---------------------------------
  // interior fill
  //---------------------------------

 
 /**
   * @method setBackgroundImage
   * @param url
   */
  self.setBackgroundImage = function(url) {  	  	
  	document.getElementById(mName).style.backgroundImage="url(" + url + ")";
  	document.getElementById(mName).style.backgroundSize="100% 100%";
  };
  
  /**
   * @method getBackgroundImage
   * @return url
   */
  self.getBackgroundImage = function() { 
    return document.getElementById(mName).style.backgroundImage; 
  };
  
  /**
   * @method setBackgroundColor
   * @param color
   */
  self.setBackgroundColor = function(color) {
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    document.getElementById(mName).style.backgroundColor = color;
  };
  
  /**
   * @method getBackgroundColor
   * @return color
   */
  self.getBackgroundColor = function() { 
    return document.getElementById(mName).style.backgroundColor; 
  };
  
  /**
   * @method setColor
   * @param color
   */
  self.setColor = function(color) {
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    document.getElementById(mName).style.color = color;
  };
  
  /**
   * @method getColor
   * @return color
   */
  self.getColor = function() { 
    return document.getElementById(mName).style.color; 
  };

  //---------------------------------
  // Font
  //---------------------------------
  
  /**
   * @method getFont
   * @return font
   */
  self.getFont = function() { 
    return document.getElementById(mName).style.font; 
  };

  /**
   * @method setFont
   * @param recap string, format: [style weight size[/lineHeight] [family]]
   */ 
  self.setFont = function (recap) {
    if ((typeof(recap) !== 'string') || recap.length<=0) return;	
  	var style = document.getElementById(mName).style;
    var params = recap.split(" ");        
   	style.fontStyle = params[0];		// style   	
   	style.fontWeight = params[1];		// weight	
	var sizes = params[2].split("/");      	   	
   	style.fontSize = sizes[0];			// size
   	if (sizes[1]) 
   		style.lineHeight = sizes[1];	// lineHeight   	
   	if (params[3]) 
   		style.fontFamily = recap.substring(recap.indexOf(params[3])); // family
  };
      
  /**
   * Set the font style.
   * @method setFontStyle
   * @param fontStyle
   */
  self.setFontStyle = function(fontStyle) {
  	document.getElementById(mName).style.fontStyle = fontStyle;
  };
      
  /**
   * Get the font style.
   * @method getFontStyle
   * @return fontStyle
   */
  self.getFontStyle = function() { 
    return document.getElementById(mName).style.fontStyle;
  };
      
  /**
   * Set the family of the font.
   * @method setFontFamily
   * @param fontFamily
   */
  self.setFontFamily = function(fontFamily) {
    document.getElementById(mName).style.fontFamily = fontFamily;
  };
      
  /**
   * Get the family of the font.
   * @method getFontFamily
   * @return font family
   */
  self.getFontFamily = function() { 
    return document.getElementById(mName).style.fontFamily; 
  };

  /**
   * Set the size of the font.
   * @method setFontSize
   * @param fontSize
   */
  self.setFontSize = function(fontSize) {
    document.getElementById(mName).style.fontSize = fontSize;
  };
      
  /**
   * Get the size of the font.
   * @method getFontSize
   * @return font size
   */
  self.getFontSize = function() { 
    return document.getElementById(mName).style.fontSize; 
  };

  /**
   * Set the letter spacing of the font.
   * @method setLetterSpacing
   * @param letterSpacing
   */
  self.setLetterSpacing = function(letterSpacing) {
    document.getElementById(mName).style.letterSpacing = letterSpacing;
  };
      
  /**
   * Get the letter spacing of the font.
   * @method getLetterSpacing
   * @return letter spacing
   */
  self.getLetterSpacing = function() { 
    return document.getElementById(mName).style.letterSpacing; 
  };

  /**
   * Set the line color of the font.
   * @method setOutlineColor
   * @param lineColor
   */
  self.setOutlineColor = function(lineColor) {
    document.getElementById(mName).style.stroke = lineColor;
  };
      
  /**
   * Get the line color of the font.
   * @method getOutlineColor
   * @return line color
   */
  self.getOutlineColor = function() { 
    return document.getElementById(mName).style.stroke; 
  };

  /**
   * Set the line width of the font.
   * @method setFontWeight
   * @param lineWidth
   */
  self.setFontWeight = function(lineWidth) {
    document.getElementById(mName).style.fontWeight = lineWidth;
  };
      
  /**
   * Get the line width of the font.
   * @method getFontWeight
   * @return line width
   */
  self.getFontWeight = function() { 
    return document.getElementById(mName).style.fontWeight; 
  };

  /**
   * Set the fill color of the font.
   * @method setFillColor
   * @param fillColor
   */
  self.setFillColor = function(fillColor) {
    document.getElementById(mName).style.fill = fillColor;
  };
      
  /**
   * Get the fill color of the font.
   * @method getFillColor
   * @return fill color
   */
  self.getFillColor = function() { 
    return document.getElementById(mName).style.fill; 
  };

  /**
   * Set transform.
   * @method setTransform
   * @param transform
   */
  self.setTransform = function(transform) {
  	document.getElementById(mName).style.transform = transform;
  	document.getElementById(mName).style["-ms-transform"] = transform;
  	document.getElementById(mName).style["-webkit-transform"] = transform;
  };
  
  /**
   * Get transform.
   * @method getTransform
   * @return transform
   */
  self.getTransform = function() {
  	return document.getElementById(mName).style.transform;
  };
  
  /**
   * Set how to show white spaces.
   * @method setWhiteSpace
   * @param whiteSpace
   */
  self.setWhiteSpace = function(whiteSpace) {
  	document.getElementById(mName).style.whiteSpace = whiteSpace;
  };
  
  /**
   * Get how to show white spaces.
   * @method getWhiteSpace
   * @return whiteSpace
   */
  self.getWhiteSpace = function() {
  	return document.getElementById(mName).style.whiteSpace;
  };
  
  
  //---------------------------------
  // final initialization
  //---------------------------------
  
  return self;
};

