/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS Canvaswork for interface element.
 * @module interface 
 */

var EJSS_INTERFACE = EJSS_INTERFACE || {};

/**
 * Canvas
 * @class Canvas 
 * @constructor  
 */
EJSS_INTERFACE.Canvas = {
  	
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_INTERFACE.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Height", element.setHeight);
	controller.registerProperty("Width", element.setWidth);
	controller.registerProperty("ImageData", element.setImageData);

	controller.registerProperty("Enabled", element.setEnabled, element.isEnabled);

	controller.registerAction("OnPress",element.getInteractionPoint);	
	controller.registerAction("OnDrag",element.getInteractionPoint);	
	controller.registerAction("OnRelease",element.getInteractionPoint);	
	controller.registerAction("OnEnter",element.getInteractionPoint);	
	controller.registerAction("OnExit",element.getInteractionPoint);	
	controller.registerAction("OnMove",element.getInteractionPoint);	
  },

};

/**
 * Canvas function
 * Creates a basic Canvas
 * @method canvas
 * @param name the name of the element
 * @param drawingPanel2D
 * @returns An abstract 2D element
 */
EJSS_INTERFACE.canvas = function (mName) {
  var self = EJSS_INTERFACE.element(mName);
  
  var mHeight = 0;
  var mWidth = 0; 
  var mLastPoint = [0,0];
  var mImageData;
  var mDragging = false;
  var mEnabled;
  
  self.render = function() {
    if (mImageData) mContext.putImageData(mImageData, 0, 0);
  }

  /**
   * @param data an array of [r,g,b,alpha]
   */
  self.setImageData = function (data) {
    if (!mImageData) mImageData = mContext.createImageData(mWidth, mHeight);
    if (data.data) data = data.data;
    for (var i=0; i<data.length; i++) mImageData.data[i] = data[i];
  }
  
  self.clear = function() {
    mContext.clearRect(0, 0, mWidth, mHeight);
  }
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  /**
   * @method setWidth
   * @param width
   */
  self.setWidth = function(width) { 
  	var element = self.getDOMElement();

    if (mWidth != width) {
      mWidth = width;
      if((typeof width === 'string') && width.indexOf("%") != -1 && 
      		element.parentNode && element.parentNode.offsetWidth) {
      	width = element.parentNode.offsetWidth * parseFloat(width)/100
      } else if(typeof width === 'string') {
      	width = parseFloat(width);
      }

      if(typeof width === 'number'){
      	element.setAttribute('width', width);     	
      } 
      return true;
    } 
    return false;
  };

  /**
   * @method getWidth
   * @return width
   */
  self.getWidth = function() { 
  	var element = self.getDOMElement();

  	// check values %
	if((typeof mWidth === 'string') && mWidth.indexOf("%") != -1 && 
     		element.parentNode && element.parentNode.offsetWidth) {
      	var value = element.parentNode.offsetWidth * parseFloat(mWidth)/100

	  	if(typeof value === 'number'){
	  		var orig = element.getAttribute('width');
	  		if(value!=orig) element.setAttribute('width', value);     	
	  	} 
    }
  	
    return mWidth; 
  };
  
  /**
   * @method setHeight
   * @param height
   */
  self.setHeight = function(height) { 
  	var element = self.getDOMElement();

    if (mHeight != height) {
      mHeight = height;
      if((typeof height === 'string') && height.indexOf("%") != -1 && 
      		element.parentNode && element.parentNode.offsetHeight) {
      	height = element.parentNode.offsetHeight * parseFloat(height)/100
      }

   	  self.getDOMElement().setAttribute('height', height);      
      return true;
    } 
    return false;
  };
  
  /**
   * @method getHeight
   * @return height
   */
  self.getHeight = function() { 
  	var element = self.getDOMElement();
  	
  	// check values %
	if((typeof mHeight === 'string') && mHeight.indexOf("%") != -1 && 
     		element.parentNode && element.parentNode.offsetHeight) {
      	value = element.parentNode.offsetHeight * parseFloat(mHeight)/100

	  	if(typeof value === 'number'){
	  		var orig = element.getAttribute('height');
	  		if(value!=orig) element.setAttribute('height', value);     	
	  	} 
    }  
 
    return mHeight; 
  };

  self.getContext = function() {
    return mContext;
  }

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_INTERFACE.Canvas.registerProperties(self,controller);
  };
  
  // ----------------------------------------------------
  // Interaction
  // ----------------------------------------------------
  
  self.setEnabled = function(enabled) {
    mEnabled = enabled;
    var element = self.getDOMElement();
    if (mEnabled) {
      element.onmousedown = handlerOnPress; 
      element.onmouseup = handlerOnRelease; 
      element.onmouseover = handlerOnEnter; 
      element.onmouseout = handlerOnExit; 
      element.onmousemove = handlerOnMove; 
      element.addEventListener("touchstart", handlerOnPress, false);
      element.addEventListener("touchend", handlerOnRelease, false);
      element.addEventListener("touchmove", handlerOnMove, false);
    }
    else {
      element.onmousedown = undefined;
      element.onmouseup = undefined; 
      element.onmouseover = undefined; 
      element.onmouseout = undefined; 
      element.onmousemove = undefined; 
      element.removeEventListener("touchstart", handlerOnPress);
      element.removeEventListener("touchend", handlerOnRelease);
      element.removeEventListener("touchmove", handlerOnMove);
    }
  }
  
  self.isEnabled = function() {
    return mEnabled;
  }

  self.getInteractionPoint = self.getMousePosition = function() {
  	return mLastPoint;
  }  

  // ----------------------------------------------------
  // Interaction functions and handlers
  // ----------------------------------------------------
 
  function getLastEventLocation(e) {
  	var pos = EJSS_GRAPHICS.CanvasGraphics.getOffsetRect(self);
	var locations = [];

	if ( typeof e.changedTouches != "undefined") {
		for(var i=0; i<e.changedTouches.length; i++) {
			locations[i] = [];		
	    	locations[i][0] = e.changedTouches[i].pageX - window.pageXOffset - pos.left;
	    	locations[i][1] = e.changedTouches[i].pageY - window.pageYOffset - pos.top;
	   }
	} else {
		locations[0] = [];
		locations[0][0] = (e.clientX || e.x) - pos.left;
		locations[0][1] = (e.clientY || e.y) - pos.top;
	}

	return locations[0];
  }

  // OnDrag
  function handlerOnPress(e) {
	e.preventDefault();
    var controller = self.getController();    
    if (controller) {
    	mLastPoint = getLastEventLocation(e);
    	controller.invokeAction("OnPress").reportInteractions();
    }
    mDragging = true;
    return false;
  }  
  
  // OnRelease
  function handlerOnRelease(e) {
	e.preventDefault();
    var controller = self.getController();
    if(mDragging && controller) {
    	mLastPoint = getLastEventLocation(e);
    	controller.invokeAction("OnRelease").reportInteractions();
    }
    mDragging = false;
    return false;
  }  
    
  // OnEnter
  function handlerOnEnter(e) {
	e.preventDefault();
    var controller = self.getController();
    mDragging = false;    
  	if (controller) {
  		mLastPoint = getLastEventLocation(e);
  		controller.invokeAction("OnEnter").reportInteractions();
  	}
    return false;
  }  
    
  // OnExit
  function handlerOnExit(e) {
	e.preventDefault();
    var controller = self.getController();
	mDragging = false;        
  	if (controller) {
  		mLastPoint = getLastEventLocation(e);
  		controller.invokeAction("OnExit").reportInteractions();
  	}
    return false;
  }  
  
  // OnMove
  function handlerOnMove(e) {
	e.preventDefault();
    var controller = self.getController();    
  	if (controller) {  		
  		mLastPoint = getLastEventLocation(e);
  		if(mDragging)
  			controller.invokeAction("OnDrag").reportInteractions();
  		else
  			controller.invokeAction("OnMove").reportInteractions(); 
  	}
    return false;
  }  

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  var mElement = document.createElement("canvas");  
  mElement.setAttribute('id', mName);
  mElement.style.overflow = "hidden";
  var mContext = mElement.getContext('2d');
  document.body.appendChild(mElement);
  self.setDOMElement(mElement);
  self.setEnabled(true);
  self.setWidth(500);
  self.setHeight(500);
      
  return self;
};

