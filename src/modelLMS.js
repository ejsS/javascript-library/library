/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia and Félix J. García 
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for Animations
 * @module core
 */

var EJSS_CORE = EJSS_CORE || {};

/**
 * Creates an animation for a model.
 * The model must implement the function:
 * - stepModel()  : steps the model for each animation frame
 * and have a view.
 * @method createAnimation
 */
EJSS_CORE.createAnimationLMS = function() {
	var self = EJSS_CORE.createAnimation();
    var SAVETEXT_Q = "Choose a name for the file";
    var READFILE_Q = "Select a file to read";

	// --- Configuration variables
	
	// ------------------------------------------------------------------------
	// States functions (UNED - Luis de la Torre)
	// ------------------------------------------------------------------------
    
    var context_id;
    var user_id;
    var ejsapp_id;
    var moodle_upload_file;
    var moodle_send_file_list;

    self.sendCallback = function() {};
    
    self.setStatusParams = function(_context_id, _user_id, _ejsapp_id, _moodle_upload_file, _moodle_send_file_list, _moodle_callback) {
    	context_id = _context_id;
    	user_id = _user_id;
    	ejsapp_id = _ejsapp_id;
    	moodle_upload_file = _moodle_upload_file;
    	moodle_send_file_list = _moodle_send_file_list;
    	if(_moodle_callback) self.sendCallback = _moodle_callback;
    	self.declareRegisterFuncs_UNED();
    };
    
    self.getUserID = function() {
    	return user_id;
	};

    self.getContextID = function() {
        return context_id;
    };

    self.getActivityID = function() {
        return ejsapp_id;
    };
        
    self.sendSnapshot = function(svgpanel, user_file, callback_ok, callback_error) {
    	if(!moodle_upload_file) return;

    	svgpanel.importGraphics(function(png) {
			var http = new XMLHttpRequest();
			var params = "user_file="+user_file+"&file="+encodeURIComponent(png)+"&type=png"+"&context_id="+context_id+"&user_id="+user_id+"&ejsapp_id="+ejsapp_id;
			http.open("POST", moodle_upload_file, true);

			//Send the proper header information along with the request
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			http.onreadystatechange = function() {//Call a function when the state changes.
			    if(http.readyState == 4 && http.status == 200) {
			        if(callback_ok) callback_ok();
			    } else {
			    	if(callback_error) callback_error();
			    }
			};
			http.send(params);
	     });
    };
    
    self.sendState = function(user_file, callback_ok, callback_error) {
    	if(!moodle_upload_file) return;

    	var json = self.serialize();
		var http = new XMLHttpRequest();
		var params = "user_file="+user_file+"&file="+encodeURIComponent(json)+"&type=json"+"&context_id="+context_id+"&user_id="+user_id+"&ejsapp_id="+ejsapp_id;
		http.open("POST", moodle_upload_file, true);

		//Send the proper header information along with the request
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
		        if(callback_ok) callback_ok();
		    } else {
		    	if(callback_error) callback_error();
		    }
		};
		http.send(params);
    };

    self.sendText = function(user_file, content, type, callback_ok, callback_error) {
    	if(!moodle_upload_file) return;

		var http = new XMLHttpRequest();
		var params = "user_file="+user_file+"&file="+encodeURIComponent(content)+"&type="+type+"&context_id="+context_id+"&user_id="+user_id+"&ejsapp_id="+ejsapp_id;
		http.open("POST", moodle_upload_file, true);

		//Send the proper header information along with the request
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
		        if(callback_ok) callback_ok();
		    } else {
		    	if(callback_error) callback_error();
		    }
		};
		http.send(params);
    };

    self.getState = function(url, callback_ok, callback_error) {
		var http = new XMLHttpRequest();
		http.open("GET", url, true);
		http.setRequestHeader("Cache-Control", "no-cache");
		
		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
		    	self.unserialize(http.responseText);
		        if(callback_ok) callback_ok();
		    } else {
		    	if(callback_error) callback_error();
		    }
		};
		http.send();
   };

    self.getText = function(url, callback_ok, callback_error) {
		var http = new XMLHttpRequest();
		http.open("GET", url, true);
		http.setRequestHeader("Cache-Control", "no-cache");
		
		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
		        if(callback_ok) callback_ok(http.responseText);
		    } else {
		    	if(callback_error) callback_error();
		    }
		};
		http.send();
   };

    self.getStateFiles = function(type, callback_ok, callback_error) {
    	if(!ejsapp_id) return;

		var http = new XMLHttpRequest();
		var get_url = moodle_send_file_list + "?ejsapp_id="+ejsapp_id+"&type="+type;
		http.open("GET", get_url, true);

		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
                var list = {};
                list["name"] = [];
                list["url"] = [];
                var data = JSON.parse(http.responseText);
                for (var i = 0; i < data.file_names.length; i++) {
                    list["name"].push(data.file_names[i].file_name);
                    list["url"].push(data.file_paths[i].file_path);
                }
		        if(callback_ok) callback_ok(list);
		    } else {
		    	if(callback_error) callback_error();
		    }
		};
		http.send();
    };
    
    self.saveText = function(name,type,content) {
        if (!content) { // then the second parameter is actually content, not type
            var content = type;
            type = null;
        }
    	if(context_id)
			EJSS_INTERFACE.BoxPanel.showInputDialog(SAVETEXT_Q, function(name) {
	  			self.sendText(name,content,type,function(){self.sendCallback();});
			});
		else
			self.sendText(name,content,type,function(){self.sendCallback();});
    };
    
    self.saveState = function(name) {
    	if(context_id) 
			EJSS_INTERFACE.BoxPanel.showInputDialog(SAVETEXT_Q, function(name) {
	  			self.sendState(name,function(){self.sendCallback();});
			});
		else     	    	
			self.sendState(name,function(){self.sendCallback();});
    };
    
    self.saveImage = function(name,panelname) {
    	var view = self.getView();
   		if(context_id) 
			EJSS_INTERFACE.BoxPanel.showInputDialog(SAVETEXT_Q, function(name) {
			  self.sendSnapshot(view[panelname], name, function(){self.sendCallback();});
			});    	
		else     	    	
			self.sendSnapshot(view[panelname], name, function(){self.sendCallback();});
    };
    
    self.readState = function(url,type) {
    	if(!url) {
    		var tp = (typeof type !== "undefined")? type:".json";
			self.getStateFiles(tp,function(lf){
			    var options = {text: lf.name, value: lf.url};
			    EJSS_INTERFACE.BoxPanel.showSelectDialog(READFILE_Q,options, function(url) {
			      self.getState(url);
			    });
			});
		} else    	
			self.getState(url);
    };
    
    self.readText = function(url,type,varname_or_callback) {
    	if(!url) {
			self.getStateFiles(type,function(lf){
			    var options = {text: lf.name, value: lf.url};
			    EJSS_INTERFACE.BoxPanel.showSelectDialog(READFILE_Q,options, function(url) {
			      self.getText(url,function(text){
			      	if(typeof varname_or_callback === "function") {
			      		varname_or_callback(text);	
			      	} else {
				      	var json = {};
				      	json[varname_or_callback] = text;
				      	self._userUnserialize(json);			      		
			      	}
			      });
			    });
			});
		} else    	
			self.getText(url,function(text){
		      	if(typeof varname_or_callback === "function") {
					varname_or_callback(text);	      		
		      	} else {
			      	var json = {};
			      	json[varname_or_callback] = text;
			      	self._userUnserialize(json);			
		      	}
			});
   };

	// ------------------------------------------------------------------------
	// Interactions functions (UNED - Luis de la Torre)
	//	CaptureStream functions: save interactions data in remote server (interoperability)
	// 	Capture functions: save interactions data in local (save experience)
	// ------------------------------------------------------------------------

	var mUserEvents = [];
	var mWebSocket;
	var mEventSource;
	var _stylesBak = {};
	var _isPlayingCaptureStream = false;
	
	self.getEventInteractionsStream = function(event) {
	    var target = event.target || event.srcElement;		

		var myevent = {"pageX": event.pageX, "pageY": event.pageY, 
			"offsetLeft": target.offsetLeft, "offsetTop": target.offsetTop,
			"target": target.id, "timeStamp": +new Date()}; 
			// "timeStamp": event.timeStamp}; the timestamp might be relative! (it depends on the browser)

		myevent["runCount"] = self.getRunCount();
   		myevent["runTime"] = self.getRunTime();   	
   		myevent["type"] = event.type;			   					
		myevent["event"] = "touch";
		
		mWebSocket.send(JSON.stringify(myevent));
	};

	self.startCaptureStream = function(port,win,fail,open,close) {
		// build uri
		var loc = window.location;
		var uri = (loc.protocol === "https:")? "wss:":"ws:";
		uri += "//" + loc.host + ":" + port;
		// uri += loc.pathname + "/to/ws";	
			
	  	try {
			mWebSocket = new WebSocket(uri);
			if(typeof win !== "undefined") win();
 			
 			mWebSocket.onopen = function () {
				self.initRunCountAndTime(); 				
				self.getView().registerInteractions(mWebSocket, false);
				
				document.addEventListener('mousemove', self.getEventInteractionsStream);
				document.addEventListener('touchmove', self.getEventInteractionsStream);
				document.addEventListener('mousedown', self.getEventInteractionsStream);
				document.addEventListener('touchstart', self.getEventInteractionsStream);
 			}
            mWebSocket.onclose = function(evt) {
            	self.stopCaptureStream();
            	close(evt);
            };
            mWebSocket.onmessage = function (evt) { 
              console.log("Message is received: " + evt.data);
           };				
		}
		catch(exception) {
			if(typeof fail !== "undefined") fail(exception);
			return;
		}
	};

	self.stopCaptureStream = function() {
	  	try { mWebSocket.close(); } catch(exception) {}

		self.getView().unregisterInteractions();

		document.removeEventListener('mousemove', self.getEventInteractionsStream);
		document.removeEventListener('touchmove', self.getEventInteractionsStream);
		document.removeEventListener('mousedown', self.getEventInteractionsStream);
		document.removeEventListener('touchstart', self.getEventInteractionsStream);
	};

	self.playCaptureStream = function(uri,win,fail,open,close) {
		var FREC = 10;
		
       	if(_isPlayingCaptureStream) return;
       	_isPlayingCaptureStream = true;
       	 
       	var captured = [];
		
		// start event source
		// var lastCapturedTime = 0;
	  	try {
			mEventSource = new EventSource(uri);
			if(typeof win !== "undefined") win();
 			
 			mEventSource.onopen = open;
            mEventSource.onclose = function(evt) {
				var s = document.getElementById("_my_mouse");   
				if(s) document.body.removeChild(s);            	
            	close(evt);
            };
            mEventSource.onmessage = function (evt) {
              	var alldata = JSON.parse(evt.data);
              	console.log("Now: " + Date.now() + "-" + alldata.timeStamp);
				captured = captured.concat(alldata.interactions);
           };			
		}
		catch(exception) {
			if(typeof fail !== "undefined") fail(exception);
			return;
		}

		// start play capture stream
		setTimeout(function() {

		// set captured interactions
		self.initRunCountAndTime();
		_capturedInteractions = [];

		var lastTime = 0;
		var ixCaptured = 0;

		var crun = 0;
		var ctime = 0;
		var ltime = 0;
		var diffLeft = 0;
		var diffTop = 0;

		var myTimer = setInterval(function() {
			var view = self.getView();
			var ix = ixCaptured;
			while (ix < captured.length) {
				var data = captured[ix++];
				
				if (typeof data["isPlaying"] !== "undefined" && data["isPlaying"]) { // isPlaying
	              	_capturedInteractions.push(data);	
	              	ixCaptured++;              	
	            } else if(data.runCount == crun && ctime >= data.timeStamp - data.runTime 
	            			&& ltime <= data.timeStamp - data.runTime) {
	              	ixCaptured++;
	              	ltime = data.timeStamp - data.runTime;
	              	
	              	//console.log(" -timestamps (now-server-created): " + 
	              	//	Date.now() + "-" + data.timeStamp);
	
					// styles bak
					for(var i in _stylesBak) {
						var element = view[i];
						element.getDOMElement().style.border = _stylesBak[i];
					}
					_stylesBak = {};
	
	              	if(typeof data.event !== "undefined") {
						// do event
						var x = data.pageX;
						var y = data.pageY;
						var target = data.target;
						if(typeof target !== "undefined" && target.length > 0) {
							var element = document.getElementById(target);
							if(element != null) {
								diffLeft = data.offsetLeft - element.offsetLeft;
								diffTop = data.offsetTop - element.offsetTop;								
							}
						}
						x -= diffLeft;
						y += diffTop;
						
						var s = document.getElementById("_my_mouse");
						if(!s) {
							var s = document.createElement('div');
							s.id = "_my_mouse";
							s.style.position = 'absolute';
						  	s.style.margin = '0';
						  	s.style.border = '3px solid red';
						  	document.body.appendChild(s);					
						}
						s.style.left  = x + 'px';
						s.style.top = y + 'px';              		
	              	} else if(typeof data.property !== "undefined") { // property
						var element = view[data.element];
						element.propertyChanged(data.property,data.data).reportInteractions();
					} else if(typeof data.action !== "undefined") { // action
						var element = view[data.element];
						element.invokeAction(data.action).reportInteractions();
						if(typeof _stylesBak[element.getName()] == "undefined") 
							_stylesBak[element.getName()] = element.getDOMElement().style.border;
						element.getDOMElement().style.border = '1px solid red';							
	          		}
				}				
			}
			
			// update time
			var runCount = self.getRunCount();
			if(crun != runCount) {
				crun = runCount;
				ctime = 0;
				ltime = 0;
			} else {
				ctime += FREC;
			}
			
			if(!_isPlayingCaptureStream) {
				clearInterval(myTimer);
			}
		}, FREC);

			
		}, 1000);
	};

	self.pauseCaptureStream = function() {
		try { mEventSource.close(); } catch(exception) {}
		_isPlayingCaptureStream = false;
	};
	
	self.getEventInteractions = function(event) {
	    var target = event.target || event.srcElement;		

		var myevent = {"pageX": event.pageX, "pageY": event.pageY, 
			"offsetLeft": target.offsetLeft, "offsetTop": target.offsetTop,
			"target": target.id, "timeStamp": +new Date()}; 
			// "timeStamp": event.timeStamp}; the timestamp might be relative! (it depends on the browser)

		myevent["runCount"] = self.getRunCount();
   		myevent["runTime"] = self.getRunTime();   	
   		myevent["type"] = event.type;			
		myevent["event"] = "touch";

		mUserEvents.push(myevent);
	};

	self.startCapture = function() {	
		self.initRunCountAndTime();
		self.getView().registerInteractions(false, false);
		
		mUserEvents = [];
		document.addEventListener('mousemove', self.getEventInteractions);
		document.addEventListener('touchmove', self.getEventInteractions);
		document.addEventListener('mousedown', self.getEventInteractions);
		document.addEventListener('touchstart', self.getEventInteractions);
	};
	
	self.getCapture = function() {		
		var interactions = {"interactions": self.getView().getRegInteractions(), "events": mUserEvents};
		// console.log(JSON.stringify(interactions));
		return interactions;
	}
		
	self.stopCapture = function() {
		self.getView().unregisterInteractions();

		document.removeEventListener('mousemove', self.getEventInteractions);
		document.removeEventListener('touchmove', self.getEventInteractions);
		document.removeEventListener('mousedown', self.getEventInteractions);
		document.removeEventListener('touchstart', self.getEventInteractions);
		
		return self.getCapture();
	};
	
	self._readCapturedInteractions = function() {
		var view = self.getView();
		// read captured interactions
		var n = _capturedInteractions.length;
		for(var i = 0; i < n; i++) {
			var inter = _capturedInteractions[i];
			if(inter["runCount"] == self.getRunCount() && inter["isPlaying"]) {  
				if(inter.property !== "undefined") { // property
					var element = view[inter.element];
					element.propertyChanged(inter.property,inter.data).reportInteractions();
				}
				if(inter.action !== "undefined") { // action
					var element = view[inter.element];
					element.invokeAction(inter.action).reportInteractions();
					if(typeof _stylesBak[element.getName()] == "undefined") 
						_stylesBak[element.getName()] = element.getDOMElement().style.border;
					element.getDOMElement().style.border = '1px solid red';							
				}
				inter["runCount"] = -1; // done
			}
		}
	};
	
	var _isPlayingCapture = false;
	var _resetPlayingCapture = false;
	var _stepPlayingCapture = 1;
	var _isPlayingBeforePause = false;
	var _capturedInteractions = [];
	self.playCapture = function(captured, callback, step) {
		var FREC = 10;
		var actions = captured.interactions;
		var lactions = actions.length;
		var caction = 0;
		var events = captured.events;
		var levents = events.length;
		var cevent = 0;

		if(_isPlayingCapture) return;
		_isPlayingCapture = true;
		_resetPlayingCapture = false;
		self.reset();
				
		// set step in playing
		if(typeof step !== "undefined") _stepPlayingCapture = step;
		self.setDelay(self.getDelay() / _stepPlayingCapture); 

		// set captured interactions
		self.initRunCountAndTime();
		_capturedInteractions = actions;
		
		// start playing
		var crun = 0;
		var ctime = 0;
		var diffLeft = 0;
		var diffTop = 0;
		var myTimer = setInterval(function() {
			var view = self.getView();
			
			// styles bak
			for(var i in _stylesBak) {
				var element = view[i];
				element.getDOMElement().style.border = _stylesBak[i];
			}
			_stylesBak = {};

			// play capture
			if(_isPlayingCapture) {

				while(caction < lactions && actions[caction]["runCount"] <= crun
					&& ctime >= actions[caction].timeStamp - actions[caction].runTime) {
					// do action
					// console.log("Action: " + JSON.stringify(actions[caction]));
					if(!actions[caction]["isPlaying"]) {  
						if(typeof actions[caction].property !== "undefined") { // property
							var element = view[actions[caction].element];
							element.propertyChanged(actions[caction].property,actions[caction].data).reportInteractions();
						}
						if(typeof actions[caction].action !== "undefined") { // action
							var element = view[actions[caction].element];
							element.invokeAction(actions[caction].action).reportInteractions();
							if(typeof _stylesBak[element.getName()] == "undefined") 
								_stylesBak[element.getName()] = element.getDOMElement().style.border;
							element.getDOMElement().style.border = '1px solid red';							
						}
					}
					caction++;
				}
				
				while(cevent < levents && events[cevent]["runCount"] <= crun
					&& ctime >= events[cevent].timeStamp - events[cevent].runTime) {
					// do event
					var x = events[cevent].pageX;
					var y = events[cevent].pageY;
					var target = events[cevent].target;
					if(typeof target !== "undefined" && target.length > 0) {
						var element = document.getElementById(target);
						if(element != null) {
							diffLeft = events[cevent].offsetLeft - element.offsetLeft;
							diffTop = events[cevent].offsetTop - element.offsetTop;
						}
					}
					x -= diffLeft;
					y += diffTop;
					
					var s = document.getElementById("_my_mouse");
					if(!s) {
						var s = document.createElement('div');
						s.id = "_my_mouse";
						s.style.position = 'absolute';
					  	s.style.margin = '0';
					  	s.style.border = '3px solid red';
					  	document.body.appendChild(s);					
					}
					s.style.left  = x + 'px';
					s.style.top = y + 'px';
					 
					cevent++;
				}
				
				// update time
				var runCount = self.getRunCount();
				if(crun != runCount) {
					crun = runCount;
					ctime = 0;
				} else {
					ctime += FREC * _stepPlayingCapture;
				}
				
				if(cevent >= levents && caction >= lactions) {
					var s = document.getElementById("_my_mouse");   
					if(s) document.body.removeChild(s);
					clearInterval(myTimer);
					_isPlayingCapture = false;
					self.setDelay(self.getDelay() * _stepPlayingCapture); 
					_capturedInteractions = [];
					callback();
				}
			}
			if(_resetPlayingCapture) {
				clearInterval(myTimer);
				_isPlayingCapture = false;
				self.setDelay(self.getDelay() * _stepPlayingCapture); 
				_capturedInteractions = [];
				callback();
			}
			
		}, FREC);
	};

	self.pauseCapture = function() {
		_isPlayingBeforePause = self.isPlaying();
		self.pause();
		_isPlayingCapture = false;
	};
	
	self.resumeCapture = function() {
		if(_isPlayingBeforePause) self.play();
		_isPlayingCapture = true;
	};

	self.resetCapture = function() {
		_resetPlayingCapture = true;
	};

	self.changeCaptureStep = function(step) {
		self.setDelay(self.getDelay() * _stepPlayingCapture); 
		_stepPlayingCapture = step;
		self.setDelay(self.getDelay() / _stepPlayingCapture); 
	};
	
	// ------------------------------------------------------------------------
	// More interactions functions (UNED - Luis de la Torre)
	//	Register functions: save interactions, model calls, ... in local (learning analytics)
	// ------------------------------------------------------------------------

	// Only declared when moodle server
	self.declareRegisterFuncs_UNED = function() {
		var mRegModelCalls = [];
		var mRegUserEvents = [];
		var mIsRegModelCalls = false;

		self.pushRegModelCalls = function(func) {
			var mycall = {"function": func, "timeStamp": +new Date()};
			mycall["runCount"] = self.getRunCount();
	   		mycall["runTime"] = self.getRunTime();   				
	
			mRegModelCalls.push(mycall);		
		}

		self.pushRegUserEvents = function(event) {
		    var target = event.target || event.srcElement;
			var myevent = {"pageX": event.pageX, "pageY": event.pageY, 
				"offsetLeft": target.offsetLeft, "offsetTop": target.offsetTop,
				"target": target.id, "timeStamp": +new Date()}; 
				// "timeStamp": event.timeStamp}; the timestamp might be relative! (it depends on the browser)
	
			myevent["runCount"] = self.getRunCount();
	   		myevent["runTime"] = self.getRunTime();   	
	   		myevent["type"] = event.type;			
			myevent["event"] = "touch";
	
			mRegUserEvents.push(myevent);
		}
	
		self.startRegister = function(events) {	
			if (!mIsRegModelCalls) {
				mIsRegModelCalls = true;
				self.initRunCountAndTime();
				self.getView().registerInteractions(false, false);
				mRegModelCalls = [];				
				mRegUserEvents = [];
				if(events) {
					document.addEventListener('mousemove', self.pushRegUserEvents);
					document.addEventListener('touchmove', self.pushRegUserEvents);
					document.addEventListener('mousedown', self.pushRegUserEvents);
					document.addEventListener('touchstart', self.pushRegUserEvents);			
				}			
			}
		}
		
		self.getRegister = function(empty) {	
			var interactions = {"interactions": self.getView().getRegInteractions(empty), 
				"model": mRegModelCalls, "events": mRegUserEvents};
			// console.log(JSON.stringify(interactions));
			if(empty) {
				mRegModelCalls = [];
				mRegUserEvents = [];
			}
			return interactions;
		}
			
		self.stopRegister = function() {
			self.getView().unregisterInteractions();
			mIsRegModelCalls = false;
			
			document.removeEventListener('mousemove', self.pushRegUserEvents);
			document.removeEventListener('touchmove', self.pushRegUserEvents);
			document.removeEventListener('mousedown', self.pushRegUserEvents);
			document.removeEventListener('touchstart', self.pushRegUserEvents);
			
			return self.getRegister();
		}
		
	    self.sendRegister = function(empty, callback_ok, callback_error) {	
	    	if(!moodle_upload_file) return;
	    	
			var json = JSON.stringify(self.getRegister(empty), function(key, value) {
				if(typeof value == 'number') {
					if (isNaN(value)) value = "__NaN";
					else if (!isFinite(value)) value = "__Infinity";
				}
				return value;
			});				
	
			var http = new XMLHttpRequest();
			var params = "file="+encodeURIComponent(json)+"&type=actions"+
				"&user_id="+user_id+"&ejsapp_id="+ejsapp_id;
			http.open("POST", moodle_upload_file, true);
	
			//Send the proper header information along with the request
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
			http.onreadystatechange = function() {//Call a function when the state changes.
			    if(http.readyState == 4 && http.status == 200) {
			        if(callback_ok) callback_ok();
			    } else {
			    	if(callback_error) callback_error();
			    }
			};
			http.send(params);
	    }
	 }
	    	
	// ------------------------------------------------------------------------
	// Interactions functions (Loo Kang - Singapore)
	//	Register functions: save interactions, model calls, ... in local (learning analytics)
	// ------------------------------------------------------------------------

	// Only declared when moodle server
	var _sg_url = new URL(document.location);
	var _sg_view_id = (typeof _sg_url.searchParams != 'undefined')?_sg_url.searchParams.get('view_id'):false; 
	if (_sg_view_id) {
		var _sg_user_id = _sg_url.searchParams.get('user_id');
		var _sg_wstoken = _sg_url.searchParams.get('wstoken');
	    var _sg_moodle_url = _sg_url.searchParams.get('url');
		var _sg_wsfunction = _sg_url.searchParams.get('wsfunction');
	
		var mRegModelCalls = [];
		var mRegUserEvents = [];
		var mIsRegModelCalls = false;
		
		self.pushRegModelCalls = function(func) {
			var mycall = {"function": func};
			mRegModelCalls.push(mycall);		
		}
	    
		self.pushRegUserEvents = function(event) {
		    var target = event.target || event.srcElement;		
			var myevent = {"type": event.type, "target": target.id}; 
			mRegUserEvents.push(myevent);
		};
	
		self.startRegister = function(events) {	
			if (!mIsRegModelCalls) {
				mIsRegModelCalls = true;
				self.getView().registerInteractions(false, false);
				self.getView().setShortRegInteractions(true);
				mRegModelCalls = [];
				mRegUserEvents = [];
				if(events) {
					document.addEventListener('mousedown', self.pushRegUserEvents);
					document.addEventListener('touchstart', self.pushRegUserEvents);			
				}			
			}
		};
		
		self.isRegisterStarted = function() {
			return mIsRegModelCalls;
		};
		
		self.getRegister = function(empty) {	
			var interactions = {
				"interactions": self.getView().getRegInteractions(empty), 
				"model": mRegModelCalls, 
				"events": mRegUserEvents };
			// console.log(JSON.stringify(interactions));
			if(empty) {
				mRegModelCalls = [];
				mRegUserEvents = [];
			}
			return interactions;
		};
			
		self.stopRegister = function() {
			self.getView().unregisterInteractions();
			mIsRegModelCalls = false;
			
			document.removeEventListener('mousedown', self.pushRegUserEvents);
			document.removeEventListener('touchstart', self.pushRegUserEvents);
			
			return self.getRegister();
		};
		
	    self.sendRegister = function(empty, callback_ok, callback_error) {
	    	if(!_sg_moodle_url) return;
	
			var userdata = self.getRegister(empty);
			if (userdata.interactions.length == 0 && 
				userdata.model.length == 0 &&
				userdata.events.length == 0) 
				return; // nothing to send
			
			var json = JSON.stringify(userdata, function(key, value) {
				if(typeof value == 'number') {
					if (isNaN(value)) value = "__NaN";
					else if (!isFinite(value)) value = "__Infinity";
				}
				return value;
			});
			
			var http = new XMLHttpRequest();
			var params = "view_id="+_sg_view_id+"&info="+encodeURIComponent(json)+"&user_id="+_sg_user_id;
			http.open("POST", _sg_moodle_url + "?wstoken=" + _sg_wstoken + "&wsfunction=" + _sg_wsfunction, true);
	
			//Send the proper header information along with the request
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
			http.onreadystatechange = function() {//Call a function when the state changes.
			    if(http.readyState == 4 && http.status == 200) {
			        if(callback_ok) callback_ok();
			    } else {
			    	if(callback_error) callback_error();
			    }
			};
			http.send(params);
	    };


		// sending info to moodle		
		setInterval(function(){ 
			if (_model) {
				if(!_model.isRegisterStarted()) _model.startRegister();		
				_model.sendRegister(true);
			}
		}, 3000); 
	}
    
	// ------------------------------------------------------------------------
	// Extending model functions
	// ------------------------------------------------------------------------
	 
	self.___play = self.play;   
	self.play = function() {
		if(mIsRegModelCalls) self.pushRegModelCalls("play");
		self.___play();
	}    
	
	self.___pause = self.pause;   
	self.pause = function() {
		if(mIsRegModelCalls) self.pushRegModelCalls("pause");
		self.___pause();
	}
	
	self.___reset = self.reset;   
	self.reset = function(reuseview) {
		if(mIsRegModelCalls) self.pushRegModelCalls("reset");
		self.___reset();	
    }

	self.___initialize = self.initialize;
	self.initialize = function() {
		if(mIsRegModelCalls) self.pushRegModelCalls("initialize");
		self.___initialize();
    }
    
    
    return self;
	
};

