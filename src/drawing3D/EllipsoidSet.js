/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//EllipsoidSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * EllipsoidSet
 * @class EllipsoidSet 
 * @constructor  
 */
EJSS_DRAWING3D.EllipsoidSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller);
       
      controller.registerProperty("MinAngleU", 
          function(v) { set.setToEach(function(element,value) { element.setMinAngleU(value); }, v); }
      );    
      controller.registerProperty("MaxAngleU", 
          function(v) { set.setToEach(function(element,value) { element.setMaxAngleU(value); }, v); }
      );    
      controller.registerProperty("MinAngleV", 
          function(v) { set.setToEach(function(element,value) { element.setMinAngleV(value); }, v); }
      );    
      controller.registerProperty("MaxAngleV", 
          function(v) { set.setToEach(function(element,value) { element.setMaxAngleV(value); }, v); }
      );                             
    }

};


/**
 * Creates a set of ellipsoids
 * @method ellipsoidSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.ellipsoidSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.ellipsoid,mName);

  // Static references
  var EllipsoidSet = EJSS_DRAWING3D.EllipsoidSet;		// reference for EllipsoidSet
  
  self.registerProperties = function(controller) {
    EllipsoidSet.registerProperties(self,controller);
  };

  return self;
};
