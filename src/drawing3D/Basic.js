/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Basic
 * @class Basic 
 * @constructor  
 */
EJSS_DRAWING3D.Basic = {
    object_files:{},
    
    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  	
		dest.setVertices(source.getVertices());
		dest.setTriangles(source.getTriangles());
		dest.setNormals(source.getNormals());
		dest.setColors(source.getColors());
  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Description", element.setDescription);
		controller.registerProperty("Vertices", element.setVertices);
		controller.registerProperty("Triangles", element.setTriangles);		
		controller.registerProperty("Normals", element.setNormals);
		controller.registerProperty("Colors", element.setColors);
	}

};

/**
 * Creates a 3D Basic
 * @method basic
 */
EJSS_DRAWING3D.basic = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  // Implementation variables
  var mDataChanged = true;

  var mVertices = [];
  var mTriangles = [];
  var mNormals = [];
  var mColors = [];

  self.getClass = function() {
  	return "ElementBasic";
  }

  /**
   * Sets all parts of an object
   */
  self.setDescription = function(description) {
    var panel = self.getPanel();
    if (!panel) return;
    if (!panel.supportsWebGL()) return; // Or an error will block the rest of the interface
//      console.log("Setting description to "+description);
    if (typeof description === 'string') { // read description from file. The file must have an object called "Object3D_" + the file name (without extensions)
      var descName = "Object3D_"+EJSS_TOOLS.File.plainName(description);
//      console.log(description+" variable = "+descName);
      self.setDescription(window[descName]);
      //EJSS_TOOLS.File.loadJSfile(description, function() { console.log ("Setting description object to "+descName); self.setDescription(window[descName]); });
      return;
    }
//    console.log("Do setting description to "+description);
  
    if (description["vertices"])  self.setVertices(description["vertices"]);
    if (description["triangles"]) self.setTriangles(description["triangles"]);
    if (description["normals"])   self.setNormals(description["normals"]);
    if (description["colors"])    self.setColors(description["colors"]);
  }

  self.setVertices = function(vertices) {
    if (!EJSS_TOOLS.compareArrays(mVertices,vertices)) {
        mVertices = vertices.slice(); 
  	    self.setMeshChanged(true);      
    }
  }

  self.getVertices = function() {
  	return mVertices;
  }

  self.setTriangles = function(triangles) {
    if (!EJSS_TOOLS.compareArrays(mTriangles,triangles)) {
        mTriangles = triangles.slice(); 
  	    self.setMeshChanged(true);      
    }
  }

  self.getTriangles = function() {
  	return mTriangles;
  }

  self.setNormals = function(normals) {
    if (!EJSS_TOOLS.compareArrays(mNormals,normals)) {
        mNormals = normals.slice(); 
  	    self.setMeshChanged(true);      
    }
  }

  self.getNormals = function() {
  	return mNormals;
  }

  self.setColors = function(colors) {
    if (!EJSS_TOOLS.compareArrays(mColors,colors)) {
        mColors = colors.slice(); 
  	    self.setMeshChanged(true);      
    }
  }

  self.getColors = function() {
  	return mColors;
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Basic.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Basic.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);

  return self;
};



