/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Surface
 * @class Surface 
 * @constructor  
 */
EJSS_DRAWING3D.Surface = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  	
		dest.setData(source.getData());
  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Data", element.setData);
		
	}

};

/**
 * Creates a 3D Surface
 * @method surface
 */
EJSS_DRAWING3D.surface = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  // Implementation variables
  var mData = [];

  self.getClass = function() {
  	return "ElementSurface";
  }

  self.setData = function(data) {
    if (!EJSS_TOOLS.compareArrays(mData,data)) {
        mData = data.slice(); 
  	    self.setMeshChanged(true);      
    }  	
  }

  self.getData = function() {
  	return mData;
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Surface.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Surface.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);

  return self;
};



