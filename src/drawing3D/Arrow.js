/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/***
 * Arrow is a class to display vectors in 3D
 * @class EJSS_DRAWING3D.Arrow 
 * @constructor  
 */
EJSS_DRAWING3D.Arrow = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy

		dest.setHeadHeight(source.getHeadHeight());
		dest.setHeadWidth(source.getHeadWidth());
        dest.setLineWidth(source.getLineWidth());

  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

  	 /*** 
	  * Ratio arrow_length/head_length 
	  * @property HeadHeight 
	  * @type double
	  * @default "8"
	  */ 
      	controller.registerProperty("HeadHeight", element.setHeadHeight);
  	 /*** 
	  * Ratio arrow_length/head_width 
	  * @property HeadWidth 
	  * @type double
	  * @default "20"
	  */ 
      	controller.registerProperty("HeadWidth", element.setHeadWidth);
     
      	controller.registerProperty("LineWidth",  element.setLineWidth, element.getLineWidth);
      	
	}

};

/**
 * Creates a 3D Arrow
 * @method arrow
 */
EJSS_DRAWING3D.arrow = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  // Implementation variables
  var mX = 0;
  var mY = 0;
  var mZ = 0;
  var mSizeX = 1;				
  var mSizeY = 1;					
  var mSizeZ = 1;		
  var mHeadHeight = 8;  // ratio arrow length / head long
  var mHeadWidth = 20;  // ratio arrow length / head width
  var mLineWidth = 0.01;

  self.getClass = function() {
  	return "ElementArrow";
  } 

  self.setHeadHeight = function(h) {
    if (mHeadHeight!=h) { 
      mHeadHeight = h; 
      self.setMeshChanged(true); 
    }   	
  }

  self.getHeadHeight = function() { 
    return mHeadHeight; 
  };

  self.setHeadWidth = function(w) {
    if (mHeadWidth!=w) { 
      mHeadWidth = w; 
      self.setMeshChanged(true); 
    }   	
  }

  self.getHeadWidth = function() { 
    return mHeadWidth; 
  };

  self.setX = function(x) { 
    if (mX!=x) { 
      mX = x; 
      self.setMeshChanged(true); 
    } 
  };

  self.getX = function() { 
    return mX; 
  };

  self.setY = function(y) {  
    if (mY!=y) { 
      mY = y; 
      self.setMeshChanged(true);
    } 
  };

  self.getY = function() { 
    return mY; 
  };

  self.setZ = function(z) {  
    if (mZ!=z) { 
      mZ = z; 
      self.setMeshChanged(true);
    } 
  };

  self.getZ = function() { 
    return mZ; 
  };

  self.setPosition = function(position) {
    self.setX(position[0]);
    self.setY(position[1]);
    self.setZ(position[2]);
  };

  self.getPosition = function() { 
    return [mX, mY, mZ]; 
  };

  self.setSizeX = function(sizeX) { 
    if (mSizeX!=sizeX) { 
      mSizeX = sizeX; 
      self.setMeshChanged(true);
    } 
  };

  self.setSizeY = function(sizeY) { 
    if (mSizeY!=sizeY) { 
      mSizeY = sizeY; 
      self.setMeshChanged(true);
    }
  };

  self.setSizeZ = function(sizeZ) { 
    if (mSizeZ!=sizeZ) { 
      mSizeZ = sizeZ; 
      self.setMeshChanged(true);
    }
  };

  self.getSizeX = function() { 
    return mSizeX; 
  };

  self.getSizeY = function() { 
    return mSizeY; 
  };

  self.getSizeZ = function() { 
    return mSizeZ; 
  };

  self.setSize = function(size) {
    self.setSizeX(size[0]);
    self.setSizeY(size[1]);
    self.setSizeZ(size[2]);
  };

  self.getSize = function() {
    return [self.getSizeX(), self.getSizeY(), self.getSizeZ()];
  };

  /**
   * Set the line width
   */
  self.setLineWidth = function(width) { 
    if (width!=mLineWidth) {
      mLineWidth = width; 
  	  self.setMeshChanged(true);
    }
  };

  /**
   * Get the line width
   */
  self.getLineWidth = function() { 
  	return mLineWidth; 
  };

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Arrow.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Arrow.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.getStyle().setDrawFill(true);
  self.getStyle().setDrawLines(false);
  self.setResolution([10,2]);
  // self.setPrivateTransformation([[54.735,-0.5,0.5,0,0,0,0]]); // 54.735 angle (arcsin(1/sqrt(3)))

  return self;
};



