/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Surface
 * @class Surface 
 * @constructor  
 */
EJSS_DRAWING3D.AnalyticCurve = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("NumPoints", element.setNumPoints);
		controller.registerProperty("MinValue", element.setMinValue);
		controller.registerProperty("MaxValue", element.setMaxValue);
		controller.registerProperty("Variable", element.setVariable);

		controller.registerProperty("FunctionX", element.setExpressionX);
		controller.registerProperty("FunctionY", element.setExpressionY);
		controller.registerProperty("FunctionZ", element.setExpressionZ);
	}

};

/**
 * Creates a 3D Surface
 * @method surface
 */
EJSS_DRAWING3D.analyticCurve = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  // Implementation variables
  var mParser = EJSS_DRAWING2D.functionsParser();
  var mExpressionX = "t";
  var mExpressionY = "t";
  var mExpressionZ = "0";

  var mNumPoints = screen.width / 10;
  var mMinValue = 0;
  var mMaxValue = 0;
  var mVariable = "t";
  
  self.getClass = function() {
  	return "ElementAnalyticCurve";
  }

  self.setNumPoints = function(n) {
  	if(mNumPoints != n) {
  	  mNumPoints = n;
  	  self.setMeshChanged(true);
  	}
  }

  self.setMinValue = function(n) {
  	if(mMinValue != n) {
  	  mMinValue = n;
  	  self.setMeshChanged(true);
  	}
  }

  self.setMaxValue = function(n) {
  	if(mMaxValue != n) {
  	  mMaxValue = n;
  	  self.setMeshChanged(true);
  	}
  }

  self.setVariable = function(n) {
  	if(mVariable != n) {
  	  mVariable = n;
  	  self.setMeshChanged(true);
  	}
  }

  self.setExpressionX = function(exp) {
  	if(mExpressionX != exp) {
  	  mExpressionX = exp;
  	  self.setMeshChanged(true);
  	}
  }

  self.setExpressionY = function(exp) {
  	if(mExpressionY != exp) {
  	  mExpressionY = exp;
  	  self.setMeshChanged(true);
  	}
  }

  self.setExpressionZ = function(exp) {
  	if(mExpressionZ != exp) {
  	  mExpressionZ = exp;
  	  self.setMeshChanged(true);
  	}
  }

  self.getNumPoints = function() {
  	return mNumPoints;
  }

  self.getMinValue = function() {
  	return mMinValue;
  }

  self.getMaxValue = function() {
  	return mMaxValue;
  }

  self.getVariable = function() {
  	return mVariable;
  }

  self.getExpressionX = function() {
  	return mParser.parse(mExpressionX);
  }

  self.getExpressionY = function() {
  	return mParser.parse(mExpressionY);
  }

  self.getExpressionZ = function() {
  	return mParser.parse(mExpressionZ);
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.AnalyticCurve.registerProperties(self, controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);
  self.getStyle().setDrawFill(false);
  self.getStyle().setDrawLines(true);

  return self;
};



