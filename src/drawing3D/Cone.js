/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Cone
 * @class Cone 
 * @constructor  
 */
EJSS_DRAWING3D.Cone = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  		EJSS_DRAWING3D.Cylinder.copyTo(source,dest);  	
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Cylinder.registerProperties(element, controller);
		// super class
		
	}
};

/**
 * Creates a 3D Cone
 * @method cone
 */
EJSS_DRAWING3D.cone = function (name) {
  var self = EJSS_DRAWING3D.cylinder(name);

  // Implementation variables
  self.getClass = function() {
  	return "ElementCone";
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Cone.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Cone.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);
  self.setTopRadius(0);

  return self;
};



