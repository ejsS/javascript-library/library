/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Tetrahedron
 * @class Tetrahedron 
 * @constructor  
 */
EJSS_DRAWING3D.Tetrahedron = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  		EJSS_DRAWING3D.Cylinder.copyTo(source,dest);
  	
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Cylinder.registerProperties(element, controller);
		// super class
		
	}
};

/**
 * Creates a 3D Tetrahedron
 * @method tetrahedron
 */
EJSS_DRAWING3D.tetrahedron = function (name) {
  var self = EJSS_DRAWING3D.cylinder(name);

  // Implementation variables
  self.getClass = function() {
  	return "ElementTetrahedron";
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Tetrahedron.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Tetrahedron.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);
  self.setTopRadius(0);
  self.setResolutionU(4);

  return self;
};



