/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/***
 * Element is the basic class for 3D elements
 * @class EJSS_DRAWING3D.Element 
 * @constructor  
 */
EJSS_DRAWING3D.Element = {
    
    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * Copies one element into another
     * @method copyTo
     */
    copyTo : function(source, dest) {
      EJSS_DRAWING3D.Style.copyTo(source.getStyle(),dest.getStyle());
      dest.setVisible(source.isVisible());
      dest.setMeasured(source.isMeasured());

      dest.setPosition(source.getPosition());
      dest.setSize(source.getSize());
      dest.setTransformation(source.getTransformation());
      dest.setExtraTransformation(source.getExtraTransformation());
      dest.setTextureUrl(source.getTextureUrl());

	  dest.setResolution(source.getResolution());

      dest.setParent(source.getParent());
    },
    
    /**
     * Registers properties in a ControlElement
     * @method registerProperties
     * @param element The element with the properties
     * @param controller A ControlElement that becomes the element controller
     */
    registerProperties : function(element,controller) {
      element.setController(controller);

	 /*** 
	  * Parent of the element
	  * @property Parent 
	  * @type Panel|Group
	  */  
      controller.registerProperty("Parent", element.setParent, element.getParent); 

	 /*** 
	  * Position in X
	  * @property X 
	  * @type double
	  * @default 0
	  */  
      controller.registerProperty("X",element.setX,element.getX);
	 /*** 
	  * Position in Y
	  * @property Y 
	  * @type double
	  * @default 0
	  */  
      controller.registerProperty("Y",element.setY,element.getY);
	 /*** 
	  * Position in Z
	  * @property Z 
	  * @type double
	  * @default 0
	  */  
      controller.registerProperty("Z",element.setZ,element.getZ);
	 /*** 
	  * Coordinates X, Y, and Z
	  * @property Position 
	  * @type double[3]
	  * @default [0,0,0]
	  */        
      controller.registerProperty("Position",element.setPosition,element.getPosition);

	 /*** 
	  * Size along the X axis
	  * @property SizeX 
	  * @type double
	  * @default 1
	  */                          
      controller.registerProperty("SizeX",element.setSizeX,element.getSizeX);
	 /*** 
	  * Size along the Y axis
	  * @property SizeY 
	  * @type double
	  * @default 1
	  */                          
      controller.registerProperty("SizeY",element.setSizeY,element.getSizeY);
	 /*** 
	  * Size along the Z axis
	  * @property SizeZ 
	  * @type double
	  * @default 1
	  */                          
      controller.registerProperty("SizeZ",element.setSizeZ,element.getSizeZ);
	 /*** 
	  * Size along the X, Y, and Z axes 
	  * @property Size 
	  * @type double[3]
	  * @default [1,1,1]
	  */                          
      controller.registerProperty("Size",element.setSize,element.getSize);

      controller.registerProperty("TextureUrl",element.setTextureUrl,element.getTextureUrl);

      controller.registerProperty("Transformation",element.setTransformation);

      controller.registerProperty("Visibility",  element.setVisible, element.isVisible);
      controller.registerProperty("Measured", element.setMeasured, element.isMeasured);

      controller.registerProperty("AlwaysUpdated", element.setAlwaysUpdated, element.isAlwaysUpdated);

      controller.registerProperty("Transparency",  element.getStyle().setTransparency, element.getStyle().getTransparency);

      controller.registerProperty("AmbientColor",  element.getStyle().setAmbientColor, element.getStyle().getAmbientColor);
      controller.registerProperty("FillColor",  element.getStyle().setFillColor, element.getStyle().getFillColor);
      controller.registerProperty("SpecularColor",  element.getStyle().setSpecularColor, element.getStyle().getSpecularColor);
      controller.registerProperty("AmbientReflection",  element.getStyle().setAmbientReflection, element.getStyle().getAmbientReflection);
      controller.registerProperty("ColorReflection",  element.getStyle().setColorReflection, element.getStyle().getColorReflection);
      controller.registerProperty("SpecularReflection",  element.getStyle().setSpecularReflection, element.getStyle().getSpecularReflection);
      controller.registerProperty("Shininess",  element.getStyle().setShininessVal, element.getStyle().getShininessVal);

      controller.registerProperty("PaletteFloor",  element.getStyle().setPaletteFloor, element.getStyle().getPaletteFloor);
      controller.registerProperty("PaletteCeil",  element.getStyle().setPaletteCeil, element.getStyle().getPaletteCeil);
      controller.registerProperty("PaletteFloorColor",  element.getStyle().setPaletteFloorColor, element.getStyle().getPaletteFloorColor);
      controller.registerProperty("PaletteCeilColor",  element.getStyle().setPaletteCeilColor, element.getStyle().getPaletteCeilColor);

      controller.registerProperty("LineColor",  element.getStyle().setLineColor, element.getStyle().getLineColor);
      controller.registerProperty("LineWidth",  element.getStyle().setLineWidth, element.getStyle().getLineWidth);
      controller.registerProperty("DrawLines",  element.getStyle().setDrawLines, element.getStyle().getDrawLines);
      controller.registerProperty("DrawFill",   element.getStyle().setDrawFill, element.getStyle().getDrawFill);
      controller.registerProperty("ClosedTop",   element.getStyle().setClosedTop, element.getStyle().getClosedTop);
      controller.registerProperty("ClosedBottom",   element.getStyle().setClosedBottom, element.getStyle().getClosedBottom);
      controller.registerProperty("ClosedLeft",   element.getStyle().setClosedLeft, element.getStyle().getClosedLeft);
      controller.registerProperty("ClosedRight",   element.getStyle().setClosedRight, element.getStyle().getClosedRight);

	  controller.registerProperty("Resolution", element.setResolution);
      controller.registerProperty("Color", element.setColor);

	 /*** 
	  * Whether the user could change the position   
	  * @property EnabledPosition 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("EnabledPosition",function(enabled) {
        //element.getInteractionTarget(TARGET_POSITION).setMotionEnabled(enabled);
      });

	 /*** 
	  * Whether the group position also changes when the element position changes    
	  * @property MovesGroup 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("MovesGroup", function(affects) {
        //element.getInteractionTarget(TARGET_POSITION).setAffectsGroup(affects);
      });

	 /*** 
	  * Whether the user could change the size   
	  * @property EnabledSize 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("EnabledSize",function(enabled) {
        //element.getInteractionTarget(TARGET_SIZE).setMotionEnabled(enabled);
      });

	 /*** 
	  * Whether the group size also changes when the element size changes    
	  * @property ResizesGroup 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("ResizesGroup", function(affects) {
        //element.getInteractionTarget(TARGET_SIZE).setAffectsGroup(affects);
      });


      // Actions
      controller.registerAction("OnEnter",   element.getPosition);
      controller.registerAction("OnExit",    element.getPosition);
      controller.registerAction("OnPress",   element.getPosition);
      controller.registerAction("OnDrag",    element.getPosition);
      controller.registerAction("OnRelease", element.getPosition);
      
      controller.registerAction("OnLoadTexture", null, null, function() { element.setProjChanged(true);});
    }
};

/**
 * Constructor for Element
 * @method element
 * @param mName string
 * @returns An abstract 3D element
 */
EJSS_DRAWING3D.element = function(mName) {
  var self = {};							// reference returned 

  // Static references
  var Element = EJSS_DRAWING3D.Element;		// reference for Element

  // Instance variables
  var mStyle = EJSS_DRAWING3D.style(mName);	// style for element 
  var mVisible = true;								// whether visible in drawing
  var mMeasured = true;								// whether measure for element

  var mTextureUrl = "";

  // Position and size
  var mX = 0;						// position X
  var mY = 0;						// position Y
  var mZ = 0;						// position Z
  var mSizeX = 1;					// size X
  var mSizeY = 1;					// size Y
  var mSizeZ = 1;					// size Z
  var mTransformation = [];			// transformations for element
  var mExtraTransformations = [];			// extra transformations for element
  var mFirstTransformation = []; // very first transformation, not to be set by the user directly

  // Implementation variables    
  var mPanel = null;				// drawing panel for element
  var mGroup = null;				// group for element
  var mSet = null;				    // The set it belongs to (if any)  
  var mIndexInSet = -1;				// The index of the element in a set (if any)  
  var mMeshChanged = true;		    // whether element mesh changed (resolution, ...)
  var mProjChanged = true;			// whether element projection changed (position, size, or group)
  var mAlwaysUpdated = false;		// whether element mesh must be always drawn

  var mResolutionU = 20;
  var mResolutionV = 20;
  var mResolutionR = 0;
  
  var mController = { 				// dummy controller object
      propertiesChanged : function() {},
      invokeAction : function() {}
  };

  // ----------------------------------------
  // Instance functions
  // ----------------------------------------

  /***
   * Copies itself to another element
   * @method copyTo
   * @param element Element
   */
  self.copyTo = function(element) {
    Element.copyTo(self,element);
  };
  
  /***
   * Get name for element
   * @method getName
   * @return string
   */
  self.getName = function() {
    return mName.replace(/\[|\]/g,"");
  };  

  /***
   * Set texture url
   * @method setTextureUrl
   * @param url
   */
  self.setTextureUrl = function(url) {
//  	console.log ("Texture = "+url);
//  	console.log ("Set = "+mSet);
//  	if (mSet!=null) console.log ("ResPathF = "+mSet.getResourcePath);
  	var resPathFunction = (mSet!=null) ? mSet.getResourcePath : self.getResourcePath; 
  	if (resPathFunction!=null) {
  	  url = resPathFunction(url);
  	  //alert ("  set to = "+url+"\n");
  	}
  	else { 
  		console.log ("No getResourcePath function for "+self.getName()+". Texture = " + url);
  	}
  	mTextureUrl = url;
  }

  /***
   * Get texture url
   * @method getTextureUrl
   * @return url
   */
  self.getTextureUrl = function() {
  	return mTextureUrl;
  }

  // -------------------------------------
  // Visible, style, measure
  // -------------------------------------

  /***
   * Sets the visibility of the element
   * @method setVisible
   * @param visible boolean
   */
  self.setVisible = function(visible) { 
    mVisible = visible; 
  };

  /***
   * Whether the element is visible
   * @method isVisible
   * @return boolean
   */
  self.isVisible = function() { 
    return mVisible; 
  };

  /***
   * Returns the real visibility status of the element, which will be false if
   * it belongs to an invisible group
   * @method isGroupVisible
   * @return boolean
   */
  self.isGroupVisible = function() {
    var el = mGroup;
    while (typeof el != "undefined" && el !== null) {
      if (!el.isVisible()) return false;
      el = el.getGroup();
    }
    return mVisible;
  };

  /***
   * Return the style (defined in DrawingPanel.js) of the inner rectangle
   * @method getStyle 
   * @return boolean
   */
  self.getStyle = function() { 
    return mStyle; 
  };

  /***
   * Sets the measurability of the element
   * @method setMeasured
   * @param measured boolean
   */
  self.setMeasured = function(measured) { 
    mMeasured = measured; 
  };

  /***
   * Whether the element is measured
   * @method isMeasured
   * @return boolean
   */
  self.isMeasured = function() { 
    return mMeasured; 
  };

  /***
   * Set color 
   * @method setColor
   * @param color
   */
  self.setColor = function(color) {
  	self.getStyle().setFillColor(color);
  	self.getStyle().setLineColor(color);
  }

  /***
   * Set color 
   * @method setColor
   * @param color
   */
  self.setResolution = function(resolution) {
  	if (Array.isArray(resolution)) {
  		self.setResolutionU(resolution[0]);
  		self.setResolutionV(resolution[1]);
  		if(resolution[2]) self.setResolutionR(resolution[2]);
  	} else {
  		self.setResolutionU(resolution);
  		self.setResolutionV(resolution);  		
  	}
  }
	
  /** TODO **/
  self.getResolution = function() {
  	return [mResolutionU,mResolutionV,mResolutionR];
  }

  self.setResolutionU = function(resolution) {
  	if (mResolutionU!=resolution) {
  		mResolutionU = resolution;
  		mMeshChanged = true; 
  	}
  }

  self.getResolutionU = function() {
  	return mResolutionU;
  }

  self.setResolutionV = function(resolution) {
  	if (mResolutionV!=resolution) {
  		mResolutionV = resolution;
  		mMeshChanged = true; 
  	}
  }

  self.getResolutionV = function() {
  	return mResolutionV;
  }

  self.setResolutionR = function(resolution) {
  	if (mResolutionR!=resolution) {
  		mResolutionR = resolution;
  		mMeshChanged = true; 
  	}
  }

  self.getResolutionR = function() {
  	return mResolutionR;
  }

  // ----------------------------------------
  // Position of the element
  // ----------------------------------------

  /***
   * Set the X coordinate of the element
   * @method setX
   * @param x double
   */
  self.setX = function(x) { 
    if (mX!=x) { 
      mX = x; 
      mProjChanged = true; 
    } 
  };

  /***
   * Get the X coordinate of the element
   * @method getX
   * @return double
   */
  self.getX = function() { 
    return mX; 
  };

  /***
   * Set the Y coordinate of the element
   * @method setY
   * @param y double
   */
  self.setY = function(y) {  
    if (mY!=y) { 
      mY = y; 
      mProjChanged = true; 
    } 
  };

  /***
   * Get the Y coordinate of the element
   * @method getY
   * @return double
   */
  self.getY = function() { 
    return mY; 
  };

  /***
   * Set the Z coordinate of the element
   * @method setZ
   * @param z double
   */
  self.setZ = function(z) {  
    if (mZ!=z) { 
      mZ = z; 
      mProjChanged = true; 
    } 
  };

  /***
   * Get the Z coordinate of the element
   * @method getZ
   * @return double
   */
  self.getZ = function() { 
    return mZ; 
  };

  /***
   * Set the coordinates of the element
   * @method setPosition
   * @param position double[] an array of dimension 3
   */
  self.setPosition = function(position) {
    self.setX(position[0]);
    self.setY(position[1]);
    self.setZ(position[2]);
  };

  /***
   * Get the coordinates of the element
   * @method getPosition
   * @return double[3]
   */
  self.getPosition = function() { 
    return [mX, mY, mZ]; 
  };
    
  // ----------------------------------------
  // Size of the element
  // ----------------------------------------

  /***
   * Set the size along the X axis of the element
   * @method setSizeX
   * @param sizeX double
   */
  self.setSizeX = function(sizeX) { 
    if (mSizeX!=sizeX) { 
      mSizeX = sizeX; 
      mProjChanged = true; 
    } 
  };

  /***
   * Get the size along the X coordinate of the element
   * @method getSizeX
   * @return double
   */
  self.getSizeX = function() { 
    return mSizeX; 
  };

  /***
   * Set the size along the Y axis of the element
   * @method setSizeY
   * @param sizeY double
   */
  self.setSizeY = function(sizeY) { 
    if (mSizeY!=sizeY) { 
      mSizeY = sizeY; 
      mProjChanged = true; 
    }
  };

  /***
   * Get the size along the Y coordinate of the element
   * @method getSizeY
   * @return double
   */
  self.getSizeY = function() { 
    return mSizeY; 
  };

  /***
   * Set the size along the Z axis of the element
   * @method setSizeZ
   * @param sizeZ double
   */
  self.setSizeZ = function(sizeZ) { 
    if (mSizeZ!=sizeZ) { 
      mSizeZ = sizeZ; 
      mProjChanged = true; 
    }
  };

  /***
   * Get the size along the Z coordinate of the element
   * @method getSizeZ
   * @return double
   */
  self.getSizeZ = function() { 
    return mSizeZ; 
  };

  /***
   * Set the size of the element
   * @method setSize
   * @param position double[] an array of dimension 3 or an object with {x,y,z} properties
   */
  self.setSize = function(size) {
    self.setSizeX(size[0]);
    self.setSizeY(size[1]);
    self.setSizeZ(size[2]);
  };

  /***
   * Get the sizes of the element
   * @method getSize
   * @return double[]
   */
  self.getSize = function() {
    return [self.getSizeX(), self.getSizeY(), self.getSizeZ()];
  };
      
  // ----------------------------------------
  // Panel and group
  // ----------------------------------------

  /***
   * To be used internally by DrawingPanel only! Sets the panel for this element.
   * @method setPanel
   * @param panel DrawingPanel
   */
  self.setPanel = function(panel) {
    mPanel = panel;
    mProjChanged = true;     
  };

  /***
   * For internal use only, use getGroupPanel() instead. Gets the panel for this element.
   * @method getPanel
   * @return DrawingPanel
   */
  self.getPanel = function() { 
    return mPanel;
  };

  /***
   * Returns the DrawingPanel in which it (or its final ancestor group) is displayed.
   * @method getGroupPanel
   * @return drawing3D.DrawingPanel
   */
  self.getGroupPanel = function() { 
    var el = self;
    while (el.getGroup()) el = el.getGroup();
    return el.getPanel();
  };

  /***
   * To be used internally by Group only! Sets the group of this element.
   * @method setGroup
   * @param group Group
   */
  self.setGroup = function(group) {
    mGroup = group;
    mProjChanged = true; 
  };

  /***
   * Get the group of this element, if any
   * @method getGroup
   * @return Group
   */
  self.getGroup = function() { 
    return mGroup; 
  };

  /***
   * To be used internally by ElementSet only! Sets the index of this element in the set
   * @method setSet
   * @param set ElementSet
   * @param index int
   */
  self.setSet = function(set,index) {
    mSet = set;
    mIndexInSet = index;
  };

  /***
   * Get the index of this element in a set, if any
   * @method getSetIndex
   * @return int
   */
  self.getSetIndex = function() { 
    return mIndexInSet; 
  };

  /***
   * Set the parent
   * @method setParent
   * @param parent Panel or Element
   */
  self.setParent = function(parent) {
  	if(parent.render) { // is a panel  		
  		self.setGroup(null);
  		parent.addElement(self);
  		self.setPanel(parent);
  	} else if (parent.getClass() == "ElementGroup") { // is a group
  		self.setGroup(parent);
  		self.getGroupPanel().addElement(self);
  		self.setPanel(self.getGroupPanel());
  	} else { //
  		console.log("WARNING: setParent() - Parent not valid : "+ parent.getName()); 
  	}
  };

  /***
   * Get the parent
   * @method getParent
   * @return parent Panel or Element
   */
  self.getParent = function() {
  	var parent;
  	if(mGroup !== null) parent = self.getGroup();
  	else parent = self.getPanel();
  	return parent  
  };

  // ----------------------------------------------------
  // Transformations
  // ----------------------------------------------------

  /***
   * Sets the internal transformation of the element.
   * @method setTransformation
   * @param tr list of transformation arrays supports: rotation based on vector [angle, x, y, z, cx, cy, cz], 
   *  rotation based on element center [angle, x, y, z], custom axes [x1, x2, x3, y1, y2, y3, z1, z2, z3] or
   *  full transformation with a matrix 4x4 
   */
  self.setTransformation = function(tr) {
  	if (typeof tr == "undefined" || tr === null) tr = [];
  	if(!EJSS_TOOLS.compareArrays(mTransformation, tr)) {
  		mTransformation = tr;
    	mProjChanged = true;
    }
  };

  self.setFirstTransformation = function(tr) {
  	if (typeof tr == "undefined" || tr === null) tr = [];
  	if(!EJSS_TOOLS.compareArrays(mTransformation, tr)) {
  		mFirstTransformation = tr;
    	mProjChanged = true;
    }
  };

  self.setExtraTransformation = function(tr) {
    mExtraTransformations = tr;
  }

  self.getExtraTransformation = function() {
    return mExtraTransformations;
  }

  self.addExtraTransformation = function(tr) {
  	if (typeof tr == "undefined" || tr === null) return;
    mExtraTransformations.push(tr);
    mProjChanged = true;
  };

  self.removeExtraTransformation = function(tr) {
  	if (typeof tr == "undefined" || tr === null) return;
  	for (var i=0; i<mExtraTransformations.length; i++) {
      var oneTr = mExtraTransformations[i];
      if (oneTr===tr) {
        mExtraTransformations.splice(i,1);
        mProjChanged = true;
        return;
      }
    }
  };

  function centerTransform(transform) {
    var centeredTr = [];
    for (var i=0; i<transform.length; i++) { 
      var tr = transform[i]; 
      if (tr.length>4) centeredTr.push(tr);
      else centeredTr.push(tr.concat(self.getPosition())); // set element center
    }
    return centeredTr;
  };
    
  /***
   * Get the internal transformation of the element.
   * @method getTransformation
   * @return Transformation 
   */
  self.getFullTransformation = function() {
    var fullTr = centerTransform(mFirstTransformation).concat(centerTransform(mTransformation)); //.concat(mFirstTransformation);
    for (var i=0; i<mExtraTransformations.length; i++) {
      fullTr.push(mExtraTransformations[i].getArray());
    }
    return fullTr;
  };
  
  /***
   * Get the internal transformation of the element.
   * @method getTransformation
   * @return Transformation 
   */
  self.getTransformation = function() {
    return mTransformation;
  };

  /***
   * Get the group transformation of the element.
   * @method getGroupTransformation
   * @return Transformation 
   */
  self.getGroupTransformation = function() {
  	var tr = EJSS_DRAWING3D.transformation();
    var el = self;
    while (typeof el != "undefined" && el !== null) {
      tr.concatenate(el.getFullTransformation());
      el = el.getGroup();
    }
    return tr;
  };

  // ----------------------------------------
  // Changes
  // ----------------------------------------

  /***
   * Whether element mesh must be always drawn
   * @method isAlwaysUpdated
   * @return boolean
   */
  self.isAlwaysUpdated = function() {
    return mAlwaysUpdated;
  };

  /***
   * Tells whether element mesh must be always drawn
   * @method setAlwaysUpdated
   * @param always boolean
   */
  self.setAlwaysUpdated = function(always) {
    mAlwaysUpdated = always;
  };

  /***
   * Whether the element mesh has changed
   * @method isChanged
   * @return boolean
   */
  self.isMeshChanged = function() {
    return mMeshChanged;
  };

  /***
   * Tells the element mesh that it has changed.
   * Typically used by subclasses when they change something.
   * @method setMeshChanged
   * @param changed boolean
   */
  self.setMeshChanged = function(changed) {
    mMeshChanged = changed;
  };

  /***
   * Whether the element projection has changed
   * @method isChanged
   * @return boolean
   */
  self.isProjChanged = function() {
    return mProjChanged;
  };

  /***
   * Tells the element projection that it has changed.
   * Typically used by subclasses when they change something.
   * @method setProjChanged
   * @param changed boolean
   */
  self.setProjChanged = function(changed) {
    mProjChanged = changed;
  };

  /***
   * Returns whether the element group has changed.
   * @method isGroupChanged
   * @return boolean
   */
  self.isGroupChanged = function() {
    var el = self.getGroup();
    while (typeof el != "undefined" && el !== null) {
      if (el.isChanged()) return true;
      el = el.getGroup();
    }
    return false;
  };

  // ----------------------------------------
  // Interaction
  // ----------------------------------------

  /***
   * Returns the controller object
   * @method getController
   * @return Controller
   */
  self.getController = function () {
    return mController;
  };

  /***
   * Set the controller
   * @method setController
   * @param Controller
   */
  self.setController = function (controller) {
    mController = controller;
  };

  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  /***
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    Element.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  mStyle.setMeshChangeListener(function (change) { mMeshChanged = true; });
  mStyle.setProjChangeListener(function (change) { mProjChanged = true; });
  
  return self;

};

//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------




