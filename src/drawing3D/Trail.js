/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Trail
 * @class Trail 
 * @constructor  
 */
EJSS_DRAWING3D.Trail = {
  NO_CONNECTION : 0, 	// The next point will not be connected to the previous one
  LINE_CONNECTION : 1,	// The next point will be connected to the previous one by a segment

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy  	
  		
		dest.setActive(source.getActive());
		dest.setNoRepeat(source.getNoRepeat());
		dest.setClearAtInput(source.getClearAtInput());
		dest.setSkip(source.getSkip());
		dest.setInputLabels(source.getInputLabels());
		dest.setMaximumPoints(source.getMaximumPoints());
		dest.setConnectionType(source.getConnectionType());
		dest.setPoints(source.getPoints());

		dest.setLineColor(source.getLineColor());
		dest.setLineWidth(source.getLineWidth());
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Active", element.setActive, element.getActive);
		controller.registerProperty("NoRepeat", element.setNoRepeat, element.getNoRepeat);
		controller.registerProperty("ClearAtInput", element.setClearAtInput, element.getClearAtInput);
		controller.registerProperty("Skip", element.setSkip, element.getSkip);
		controller.registerProperty("ColumnNames", element.setInputLabels, element.getInputLabels);
		controller.registerProperty("Maximum", element.setMaximumPoints, element.getMaximumPoints);
		controller.registerProperty("ConnectionType", element.setConnectionType, element.getConnectionType);		
		controller.registerProperty("Connected", element.setConnected, element.isConnected);		
		controller.registerProperty("Points", element.setPoints, element.getPoints);		
		controller.registerProperty("LastPoint", element.addPoint, element.getLastPoint);
		controller.registerProperty("Input", element.addPoints);
		controller.registerProperty("InputX", element.addXPoints);				
		controller.registerProperty("InputY", element.addYPoints);
		controller.registerProperty("InputZ", element.addZPoints);

      	controller.registerProperty("LineColor",  element.setLineColor, element.getLineColor);
      	controller.registerProperty("LineWidth",  element.setLineWidth, element.getLineWidth);		
	}

};

/**
 * Creates a 3D Trail
 * Trail implements dataCollected(), which makes it a Collector of data 
 * @method trail
 */
EJSS_DRAWING3D.trail = function (name) {
  var self = EJSS_DRAWING3D.element(name);

    // Configuration variables
	var mActive = true;
	var mNoRepeat = false;
	var mClearAtInput = false;
	var mSkip = 0;
	var mInputLabels = ["x", "y", "z"];	  
	var mMaximum = 0;
	var mConnectionType = EJSS_DRAWING3D.Trail.LINE_CONNECTION;
	
  	// Implementation variables
  	var mCounter = 0;
  	var mIsEmpty = true;
	//  private int firstPoint = 0; // the first point of the current path 
  	var mLastPoint = 0;	
  	var mFlushPoint = 0; // This is the last point which was not added because of the skip parameter  	
  	var mCurrentList = []; 	// The current list of points  	
  	var mSegmentList = []; // The list of past Segments, if any  	
	var mTmpList = []; // The temporal list of new points
	var mTmpLastXAdded = 0;
	var mTmpLastYAdded = 0;
	var mTmpLastZAdded = 0;
	 
    var mLineWidth = 0.01;
	 
	self.getClass = function() {
		return "ElementTrail";
	}

  /**
   * Set the line color
   */
  self.setLineColor = function(color) { 
  	self.getStyle().setFillColor(color);
  };
    
  /**
   * Get the line color
   */
  self.getLineColor = function() { 
    return self.getStyle().getFillColor();
  };

  /**
   * Set the line width
   */
  self.setLineWidth = function(width) { 
    if (width!=mLineWidth) {
      mLineWidth = width; 
  	  self.setMeshChanged(true);
    }
  };

  /**
   * Get the line width
   */
  self.getLineWidth = function() { 
  	return mLineWidth; 
  };

  /**
   * Set/unset the active state of the trail. 
   * The trail does not use this state, but just keeps an internal value that
   * programs can consult in order not to add data to the trail.
   * In other words, a trail will always honor an addPoint() command.
   * But programs can consult this internal value to decide whether or not
   * to send data to a trail.
   * Default value is true.
   * @method setActive
   * @param _active
   */
  self.setActive = function(active) {
    if(mActive != active) {
    	mActive = active;
    	self.setMeshChanged(true);
    }
  }
  
  /**
   * Whether the trail is in active mode.
   * @method getActive
   * @return
   */
  self.getActive = function() { 
  	return mActive; 
  }
  
  /** 
   * Sets the no repeat state of the trail.
   * When set, a trail will ignore (x,y) points which equal the last added point.
   * @method setNoRepeat
   * @param _noRepeat
   */
  self.setNoRepeat = function (noRepeat) {
    if(mNoRepeat != noRepeat) {
    	mNoRepeat = noRepeat;
    	self.setMeshChanged(true);
    }
  }
  
  /**
   * Whether the trail is in no repeat mode. Default value is false.
   * @method getNoRepeat
   * @return
   */
  self.getNoRepeat = function() { 
  	return mNoRepeat; 
  }

  /**
   * Sets the trail to clear existing points when receiving 
   * a new point or array of points.
   * @method setClearAtInput
   * @param _clear
   */
  self.setClearAtInput = function(clear) {
    if(mClearAtInput != clear) {
    	mClearAtInput = clear;
    	self.setMeshChanged(true);
    }
  }
  
  /**
   * Whether the trail is in clear at input mode.
   * @method getClearAtInput
   * @return
   */
  self.getClearAtInput = function() { 
  	return mClearAtInput; 
  }

  /**
   * Sets the skip parameter. When the skip parameter is larger than zero,
   * the trail only considers one of every 'skip' points. That is, if skip is 3, 
   * the trail will consider only every third point sent to it. 
   * The default is zero, meaning all points must be considered.
   * @method setSkip
   * @param _skip
   */
  self.setSkip = function (skip) {
    if (mSkip != skip) {
      mSkip = skip;
      mCounter = 0;
      self.setMeshChanged(true);
    }
  }
  
  /**
   * Returns the skip parameter of the trail.
   * @method getSkip
   * @return
   */
  self.getSkip = function () { 
  	return mSkip; 
  }

  /**
   * Sets the labels of the X,Y,Z coordinates when the data is displayed in a table
   * @method setInputLabels
   * @param _label
   */
  self.setInputLabels = function (label) {
  	if((mInputLabels[0] != label[0]) || (mInputLabels[1] != label[1]) || (mInputLabels[2] != label[2])) { 
  		mInputLabels[0] = label[0]; 
  		mInputLabels[1] = label[1];
  		mInputLabels[2] = label[2]; 
  		self.setMeshChanged(true);
  	}
  }

  /**
   * Returns the labels of the X,Y,Z coordinates
   * @method getInputLabels
   * @return labels
   */
  self.getInputLabels = function () { 
  	return mInputLabels; 
  }
  
  /**
   * Sets the maximum number of points for the trail.
   * Once the maximum is reached, adding a new point will cause
   * remotion of the first one. This is useful to keep trails
   * down to a reasonable size, since very long trails can slow
   * down the rendering (in certain implementations).
   * If the value is 0 (the default) the trail grows forever
   * without discarding old points.
   * @method setMaximumPoints
   * @param maximum int
   */
  self.setMaximumPoints = function(maximum) {
  	var max = Math.max(maximum, 2);
  	if(mMaximum != max) {
    	mMaximum = max;
    	self.setMeshChanged(true);
    }
  }

  /**
   * Returns the maximum number of points allowed for the trail
   * @method getMaximumPoints
   * @return int
   */
  self.getMaximumPoints = function() {
    return mMaximum;
  }

  /**
   * Sets the type of connection for the next point.
   * @method setConnectionType
   * @param type int
   */
  self.setConnected = function (connected) {
   if (connected) self.setConnectionType(EJSS_DRAWING3D.Trail.LINE_CONNECTION);
   else self.setConnectionType(EJSS_DRAWING3D.Trail.NO_CONNECTION);
  }

  /**
   * Gets the connection type.
   * @method getConnectionType
   * @see #setConnectionType(int)
   */
  self.isConnected = function() {
    return mConnectionType===EJSS_DRAWING3D.Trail.LINE_CONNECTION;
  }

  /**
   * Sets the type of connection for the next point.
   * @method setConnectionType
   * @param type int
   */
  self.setConnectionType = function (type) {
    if (typeof type == "string") type = EJSS_DRAWING3D.Trail[type.toUpperCase()];      	
    if(mConnectionType != type) {
    	mConnectionType = type;
    	self.setMeshChanged(true);
    }
  }

  /**
   * Gets the connection type.
   * @method getConnectionType
   * @see #setConnectionType(int)
   */
  self.getConnectionType = function() {
    return mConnectionType;
  }

  /**
   * Adds a new point to the trail.
   * @method addPoint
   * @param x double The X coordinate of the point 
   * 		or point double[] The double[2] array with the coordinates of the point.
   * @param y double The Y coordinate of the point.
   * @param z double The Z coordinate of the point.
   * @param style
   */
  self.addPoint = function (x, y, z, style) {
  	if (mClearAtInput) self.initialize();
    if(x instanceof Array)    	
    	addPoint(x[0], x[1], x[2], style);
    else 
    	addPoint (x,y,z,style);
    self.setMeshChanged(true);
  }


  /**
   * Adds an array of points to the trail.
   * @method addPoints
   * @param xInput double The double[] array with the X coordinates of the points.
   * 	or  point double[][] The double[nPoints][2] array with the coordinates of the points.
   * @param yInput double The double[] array with the Y coordinates of the points.
   * @param zInput double The double[] array with the Z coordinates of the points.
   * @param style
   */
  self.addPoints  = function(x, y, z) {
  	if (mClearAtInput) self.initialize();
  	if(x[0] instanceof Array) {    
    	for (var i=0,n=x.length; i<n; i++) addPoint (x[i][0],x[i][1],x[i][2],mConnectionType);  		
  	}
    else if(x && y && z) {    	
    	var n = Math.min(x.length,y.length,z.length);
    	for (var i=0; i<n; i++) addPoint (x[i],y[i],z[i],mConnectionType);
    }
    self.setMeshChanged(true);
  }

  /**
   * Adds an array of X points to the trail.
   * @method addXPoints
   */
  self.addXPoints  = function(x) {
  	var i = 0;
  	if(x instanceof Array) {
  		// fill points with the values  		
    	for (var n=x.length; i<n; i++) {
    		if(mTmpList[i]) mTmpList[i] = [x[i],mTmpList[i][1],mTmpList[i][2],mTmpList[i][3]];
    		else mTmpList[i] = [x[i],mTmpLastYAdded,mTmpLastZAdded,mConnectionType];
    	}  		
    	mTmpLastXAdded = x[i];
  	}
    else { // only one value
    	if(mTmpList[i]) mTmpList[i] = [x,mTmpList[i][1],mTmpList[i][2],mTmpList[i][3]];
    	else mTmpList[i] = [x,mTmpLastYAdded,mTmpLastZAdded,mConnectionType];
    	i++;
    	mTmpLastXAdded = x;    	
    }

	// fill points with the last value
	while (mTmpList[i]) {
		mTmpList[i] = [mTmpLastXAdded,mTmpList[i][1],mTmpList[i][2],mTmpList[i][3]];
		i++;
	}    	

    //self.setMeshChanged(true);
  }

  /**
   * Adds an array of Y points to the trail.
   * @method addYPoints
   */
  self.addYPoints  = function(y) {
  	var i = 0;
  	if(y instanceof Array) {
  		// fill points with the values  		
    	for (var n=y.length; i<n; i++) {
    		if(mTmpList[i]) mTmpList[i] = [mTmpList[i][0],y[i],mTmpList[i][2],mTmpList[i][3]];
    		else mTmpList[i] = [mTmpLastXAdded,y[i],mTmpLastZAdded,mConnectionType];
    	}  		
    	mTmpLastYAdded = y[i];
  	}
    else { // only one value
    	if(mTmpList[i]) mTmpList[i] = [mTmpList[i][0],y,mTmpList[i][2],mTmpList[i][3]];
    	else mTmpList[i] = [mTmpLastXAdded,y,mTmpLastZAdded,mConnectionType];
    	i++;
    	mTmpLastYAdded = y;    	
    }

	// fill points with the last value
	while (mTmpList[i]) {
		mTmpList[i] = [mTmpList[i][0],mTmpLastYAdded,mTmpList[i][2],mTmpList[i][3]];
		i++;
	}    	

    //self.setMeshChanged(true);
  }

  /**
   * Adds an array of Z points to the trail.
   * @method addZPoints
   */
  self.addZPoints  = function(z) {
  	var i = 0;
  	if(z instanceof Array) {
  		// fill points with the values  		
    	for (var n=z.length; i<n; i++) {
    		if(mTmpList[i]) mTmpList[i] = [mTmpList[i][0],mTmpList[i][1],z[i],mTmpList[i][3]];
    		else mTmpList[i] = [mTmpLastXAdded,mTmpLastYAdded,z[i],mConnectionType];
    	}  		
    	mTmpLastZAdded = z[i];
  	}
    else { // only one value
    	if(mTmpList[i]) mTmpList[i] = [mTmpList[i][0],mTmpList[i][1],z,mTmpList[i][3]];
    	else mTmpList[i] = [mTmpLastXAdded,mTmpLastYAdded,z,mConnectionType];
    	i++;
    	mTmpLastZAdded = z;    	
    }

	// fill points with the last value
	while (mTmpList[i]) {
		mTmpList[i] = [mTmpList[i][0],mTmpList[i][1],mTmpLastZAdded,mTmpList[i][3]];
		i++;
	}    	

    // self.setMeshChanged(true);
  }

  /**
   * Adds the temporal array of point to the trail.
   * @method dataCollected
   */
  self.dataCollected  = function() {
  	if (mTmpList.length > 0) {
  	  self.addPoints(mTmpList);
      mTmpList = [];
  	  self.setMeshChanged(true);
  	}
  }   

  /**
   * Sets an array of points to the trail.
   * @method setPoints
   * @param xInput double The double[] array with the X coordinates of the points.
   * 	or  point double[][] The double[nPoints][2] array with the coordinates of the points.
   * @param yInput double The double[] array with the Y coordinates of the points.
   * @param zInput double The double[] array with the Z coordinates of the points.
   * @param style
   */
  self.setPoints  = function(x, y, z, style) {
  	self.clear();
  	self.addPoints(x,y,z,style);
  	self.setMeshChanged(true);
  }
    
  /**
   * Moves to the new point without drawing.
   * (Equivalent to setting the connection type
   * to NO_CONNECTION and adding one single point, then setting the 
   * type back to its previous value.)
   * @method moveToPoint
   * @param x double The X coordinate of the point.
   * 	or point double[] The double[2] array with the coordinates of the point.
   * @param y double The Y coordinate of the point.
   * @param z double The Z coordinate of the point.
   */
  self.moveToPoint = function(x, y, z) { 
  	if (mClearAtInput) self.initialize();
  	if(x instanceof Array) 
    	addPoint(x[0], x[1], x[2], EJSS_DRAWING3D.Trail.NO_CONNECTION);    
    else	
    	addPoint(x, y, z, EJSS_DRAWING3D.Trail.NO_CONNECTION);
    self.setMeshChanged(true);
  }

  /**
   * Returns all points.
   * @method getPoints
   * @return points
   */
  self.getPoints = function () {
    var points = [];
    var size = mSegmentList.length;          	
	for(var i=0; i<size; i++ )
		points = points.concat(mSegmentList[i]);
	points = points.concat(mCurrentList);

    return points; 
  }

  /**
   * Gets last point.
   * @method getLastPoint
   * @return point
   */
  self.getLastPoint = function () {
    return mLastPoint;
  }

  /**
   * Same as clear
   * @method reset
   */
  self.reset = function() {
    self.clear();
  }

  /**
   * Clears all points from all segments of the trail.
   * @method clear
   */
  self.clear = function() {
    mSegmentList = [];
    mCurrentSegment = 0;    
    self.initialize();
  }

  /**
   * Clears all points from the last segment of the trail, 
   * respecting previous segments.
   * @method initialize
   */
  self.initialize  = function() {
	  mCurrentList = [];	  
	  mFlushPoint = 0;
	  mIsEmpty = true;
	  mCounter = 0;
	  mLastPoint = 0;
	  self.setMeshChanged(true);
  }

  /**
   * Creates a new segment of the trail.
   * @method newSegment
   */
  self.newSegment = function() {
    if (mFlushPoint != 0) {
    	var size = mCurrentList.length;
    	mCurrentList[size] = self.formatPoint(mFlushPoint);
    }
    var size = mSegmentList.length;
    mSegmentList[size] = mCurrentList;

	self.initialize();
  }  
  
  function addPoint(_x, _y, _z, _type) {
    //if (!mActive) return;
    if (isNaN(_x) || isNaN(_y)) { 
    	mIsEmpty = true; 
    	return; 
    }
    if (mNoRepeat && mLastPoint[0]==_x && mLastPoint[1]==_y && mLastPoint[2]==_z) 
    	return;
	if ((typeof _type == "undefined") || (_type === null))
		_type = mConnectionType;

    mFlushPoint = 0;
    mLastPoint = self.formatPoint([_x,_y,_z,_type]);
    
    if (mSkip > 0) { // Only if the counter is 0      
      if(mCounter > 0) mFlushPoint = self.formatPoint([_x, _y, _z, _type]);      
      if(++mCounter >= mSkip) mCounter = 0;	        
    }
    
    if (mFlushPoint == 0) {
	    if (mMaximum > 2 && mCurrentList.length >= mMaximum) {
	    	mCurrentList.splice(0,1) // remove the firstPoint
	    }
	    
	    mCurrentList[mCurrentList.length] = self.formatPoint([_x, _y, _z, _type]);
	    mIsEmpty = false;
	}
  }

	// override for subclases (for example Trace)
   self.formatPoint = function(src) {
   		return src;   		
   }

	self.registerProperties = function(controller) {
		EJSS_DRAWING3D.Trail.registerProperties(self, controller);
	};
	 
  	self.copyTo = function(element) {
    	EJSS_DRAWING2D.Trail.copyTo(self,element);
  	};
  	 
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	self.setSize([1,1,1]);
    self.getStyle().setDrawFill(true);
    self.getStyle().setDrawLines(false);
  	self.setResolution([24,2]);

	return self;
}