/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Text
 * @class Text 
 * @constructor  
 */
EJSS_DRAWING3D.Text = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  		EJSS_DRAWING2D.Font.copyTo(source.getFont(),dest.getFont());

		dest.setText(source.getText());
		dest.setBackground(source.getBackground());
  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Text", element.setText);
		controller.registerProperty("Background", element.setBackground);
		controller.registerProperty("FitText", element.setFitText);
				
		controller.registerProperty("Font", element.getFont().setFont);
		controller.registerProperty("FontFamily", element.getFont().setFontFamily);
		controller.registerProperty("FontSize", element.getFont().setFontSize);
		controller.registerProperty("FontColor", element.getFont().setFillColor);
		controller.registerProperty("FontStyle", element.getFont().setFontStyle);		
	}

};

/**
 * Creates a 3D Text
 * @method text
 */
EJSS_DRAWING3D.text = function (name) {
  var self = EJSS_DRAWING3D.element(name);
  
  // Implementation variables
  var mFont = EJSS_DRAWING2D.font(name);	// font for text

  var mText = "";
  var mBackground = "rgba(255,255,255,0)";
  var mFitText = false;

  self.getClass = function() {
  	return "ElementText";
  }

  self.setBackground = function(color) { 
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color != mBackground) {
      mBackground = color; 
  	  self.setMeshChanged(true);
    }
    return self;
  };

  self.getBackground = function() {
  	return mBackground;
  }

  self.setText = function(text) {
  	if(mText != text) {
  	  mText = text;
  	  self.setMeshChanged(true);
    }
  }

  self.getText = function() {
  	return mText;
  }

  self.setFitText = function(fit) {
  	if(mFitText != fit) {
  	  mFitText = fit;
  	  self.setMeshChanged(true);
    }
  }

  self.getFitText = function() {
  	return mFitText;
  }

  self.getFont = function() {
	return mFont;
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Text.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Text.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);
  self.getStyle().setTransparency(255);

  return self;
};



