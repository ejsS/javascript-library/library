/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//SegmentSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * SegmentSet
 * @class SegmentSet 
 * @constructor  
 */
EJSS_DRAWING3D.SegmentSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller);

      controller.registerProperty("LineWidth", 
          function(v) { set.setToEach(function(element,value) { element.setLineWidth(value); }, v); }
      );      
      controller.registerProperty("LineColor", 
          function(v) { set.setToEach(function(element,value) { element.setLineColor(value); }, v); }
      );      
       
    }

};


/**
 * Creates a set of segments
 * @method segmentSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.segmentSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.segment,mName);

  // Static references
  var SegmentSet = EJSS_DRAWING3D.SegmentSet;		// reference for SegmentSet
  
  self.registerProperties = function(controller) {
    SegmentSet.registerProperties(self,controller);
  };

  return self;
};
