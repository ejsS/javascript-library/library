/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//SphereSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * SphereSet
 * @class SphereSet 
 * @constructor  
 */
EJSS_DRAWING3D.SphereSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller); 

      controller.registerProperty("Radius", 
          function(v) { set.setToEach(function(element,value) { element.setRadius(value); }, v); }
      );    

    }

};


/**
 * Creates a set of spheres
 * @method sphereSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.sphereSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.sphere,mName);

  // Static references
  var SphereSet = EJSS_DRAWING3D.SphereSet;		// reference for SphereSet
  
  self.registerProperties = function(controller) {
    SphereSet.registerProperties(self,controller);
  };

  return self;
};
