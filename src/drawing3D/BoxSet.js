/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//BoxSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * BoxSet
 * @class BoxSet 
 * @constructor  
 */
EJSS_DRAWING3D.BoxSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("ReduceZby", 
          function(v) { set.setToEach(function(element,value) { element.setReduceZby(value); }, v); }
      );
    }

};


/**
 * Creates a set of boxs
 * @method boxSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.boxSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.box,mName);

  // Static references
  var BoxSet = EJSS_DRAWING3D.BoxSet;		// reference for BoxSet
  
  self.registerProperties = function(controller) {
    BoxSet.registerProperties(self,controller);
  };

  return self;
};
