/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/***
 * Sphere
 * @class EJSS_DRAWING3D.Sphere 
 * @constructor  
 */
EJSS_DRAWING3D.Sphere = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  		EJSS_DRAWING3D.Ellipsoid.copyTo(source,dest);
  	
		dest.setRadius(source.getRadius());
  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Ellipsoid.registerProperties(element, controller);
		// super class

	 /*** 
	  * Radius
	  * @property Radius 
	  * @type number
	  * @default 0.5
	  */ 
      controller.registerProperty("Radius", element.setRadius);
		
	}

};

/**
 * Creates a 3D Sphere
 * @method sphere
 */
EJSS_DRAWING3D.sphere = function (name) {
  var self = EJSS_DRAWING3D.ellipsoid(name);
  var mRadius = 0.5;
  
  // Implementation variables
  self.getClass = function() {
  	return "ElementSphere";
  }

  self.setRadius = function(radius) {
  	mRadius = radius;
  	self.setSize([2*radius,2*radius,2*radius]);
  }

  self.getRadius = function() {
  	return mRadius;
  }
    
  self.copyTo = function(element) {
	EJSS_DRAWING3D.Sphere.copyTo(self,element);
  };
      
  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Sphere.registerProperties(self, controller);
  };
	    
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);
  self.setResolution([30,20]);

  return self;
};



