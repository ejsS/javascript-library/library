/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Group
 * @class Group 
 * @constructor  
 */
EJSS_DRAWING3D.Group = {

};

/**
 * Creates a group
 * @method group
 */
EJSS_DRAWING3D.group = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  self.getClass = function() {
  	return "ElementGroup";
  }
  
  self.isGroup = function() {
  	return true;
  }
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};



