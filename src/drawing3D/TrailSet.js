/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//TrailSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * TrailSet
 * @class TrailSet 
 * @constructor  
 */
EJSS_DRAWING3D.TrailSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller);
		       
      controller.registerProperty("Active", 
          function(v) { set.setToEach(function(element,value) { element.setActive(value); }, v); }
      );    
      controller.registerProperty("NoRepeat", 
          function(v) { set.setToEach(function(element,value) { element.setNoRepeat(value); }, v); }
      );    
      controller.registerProperty("ClearAtInput", 
          function(v) { set.setToEach(function(element,value) { element.setClearAtInput(value); }, v); }
      );    
      controller.registerProperty("Skip", 
          function(v) { set.setToEach(function(element,value) { element.setSkip(value); }, v); }
      );    
      controller.registerProperty("ColumnNames", 
          function(v) { set.setToEach(function(element,value) { element.setInputLabels(value); }, v); }
      );    
      controller.registerProperty("Maximum", 
          function(v) { set.setToEach(function(element,value) { element.setMaximumPoints(value); }, v); }
      );    
      controller.registerProperty("ConnectionType", 
          function(v) { set.setToEach(function(element,value) { element.setConnectionType(value); }, v); }
      );    
      controller.registerProperty("Connected", 
          function(v) { set.setToEach(function(element,value) { element.setConnected(value); }, v); }
      );    
      controller.registerProperty("Points", 
          function(v) { set.setToEach(function(element,value) { element.setPoints(value); }, v); }
      );    
      controller.registerProperty("LastPoint", 
          function(v) { set.setToEach(function(element,value) { element.addPoint(value); }, v); }
      );           
      controller.registerProperty("Input", 
          function(v) { set.setToEach(function(element,value) { element.addPoints(value); }, v); }
      );           
      controller.registerProperty("InputX", 
          function(v) { set.setToEach(function(element,value) { element.addXPoints(value); }, v); }
      );           
      controller.registerProperty("InputY", 
          function(v) { set.setToEach(function(element,value) { element.addYPoints(value); }, v); }
      );      
      controller.registerProperty("InputZ", 
          function(v) { set.setToEach(function(element,value) { element.addZPoints(value); }, v); }
      );      
      controller.registerProperty("LineWidth", 
          function(v) { set.setToEach(function(element,value) { element.setLineWidth(value); }, v); }
      );
      controller.registerProperty("LineColor", 
          function(v) { set.setToEach(function(element,value) { element.setLineColor(value); }, v); }
      );                  
    }

};


/**
 * Creates a set of trails
 * @method trailSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.trailSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.trail,mName);

  // Static references
  var TrailSet = EJSS_DRAWING3D.TrailSet;		// reference for TrailSet
  
  self.registerProperties = function(controller) {
    TrailSet.registerProperties(self,controller);
  };

  return self;
};
