/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/***
 * ElementSet is the basic class for a set of Elements
 * @class EJSS_DRAWING3D.ElementSet 
 * @constructor  
 */
EJSS_DRAWING3D.ElementSet = {
    
    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * Registers properties in a ControlElement
     * @method 
     * @param element The element with the properties
     * @param controller A ControlElement that becomes the element controller
     */
    registerProperties : function(set,controller) {
      set.setController(controller); // remember it, in case you change the number of elements
      set.setToEach(function(element,value) { element.setController(value); }, controller); // make all elements in the set report to the same controller

      controller.registerProperty("Parent", 
      	  function(panel) {
      	  	set.setParent(panel); 
      		set.setToEach(function(element,value) { element.setParent(value); }, set); 
      	  } );

      controller.registerProperty("NumberOfElements", set.setNumberOfElements);

      controller.registerProperty("ElementInteracted", set.setElementInteracted, set.getElementInteracted);

//      controller.registerProperty("x",function(v) { set.foreach("setX",v); }, function() { return set.getall("getX"); });
      controller.registerProperty("X",
          function(v) { set.setToEach(function(element,value) { element.setX(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getX(); } ); }      
          );
      controller.registerProperty("Y",
          function(v) { set.setToEach(function(element,value) { element.setY(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getY(); } ); }      
      );
      controller.registerProperty("Z",
          function(v) { set.setToEach(function(element,value) { element.setZ(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getZ(); } ); }      
      );
      controller.registerProperty("Position",
          function(v) { set.setToEach(function(element,value) { element.setPosition(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getPosition(); } ); }      
      );

      controller.registerProperty("SizeX",
          function(v) { set.setToEach(function(element,value) { element.setSizeX(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getSizeX(); } ); }      
          );
      controller.registerProperty("SizeY",
          function(v) { set.setToEach(function(element,value) { element.setSizeY(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getSizeY(); } ); }      
      );
      controller.registerProperty("SizeZ",
          function(v) { set.setToEach(function(element,value) { element.setSizeZ(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getSizeZ(); } ); }      
      );
      controller.registerProperty("Size",
          function(v) { set.setToEach(function(element,value) { element.setSize(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getSize(); } ); }      
      );
      controller.registerProperty("TextureUrl", 
          function(v) { set.setToEach(function(element,value) { element.setTextureUrl(value); }, v); }
      );

      controller.registerProperty("Transformation",
      	  function(v) { set.setToEach(function(element,value) { element.setTransformation(value); }, v);
      });

      controller.registerProperty("Visibility", 
          function(v) { set.setToEach(function(element,value) { element.setVisible(value); }, v); }
      );
      controller.registerProperty("Measured", 
          function(v) { set.setToEach(function(element,value) { element.setMeasured(value); }, v); }
      );

      controller.registerProperty("Transparency", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setTransparency(value); }, v); }
      );
      controller.registerProperty("AmbientColor", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setAmbientColor(value); }, v); }
      );
      controller.registerProperty("FillColor", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setFillColor(value); }, v); }
      );
      controller.registerProperty("SpecularColor", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setSpecularColor(value); }, v); }
      );
      controller.registerProperty("AmbientReflection", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setAmbientReflection(value); }, v); }
      );
      controller.registerProperty("ColorReflection", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setColorReflection(value); }, v); }
      );
      controller.registerProperty("SpecularReflection", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setSpecularReflection(value); }, v); }
      );
      controller.registerProperty("Shininess", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setShininessVal(value); }, v); }
      );

      controller.registerProperty("LineColor", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setLineColor(value); }, v); }
      );
      controller.registerProperty("LineWidth", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setLineWidth(value); }, v); }
      );
      controller.registerProperty("DrawLines", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setDrawLines(value); }, v); }
      );
      controller.registerProperty("FillColor", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setFillColor(value); }, v); }
      );
      controller.registerProperty("DrawFill", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setDrawFill(value); }, v); }
      );

      controller.registerProperty("ClosedTop", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setAttributes(value); }, v); }
      );
      controller.registerProperty("ClosedBottom", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setAttributes(value); }, v); }
      );
      controller.registerProperty("ClosedLeft", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setAttributes(value); }, v); }
      );
      controller.registerProperty("ClosedRight", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setAttributes(value); }, v); }
      );

      controller.registerProperty("Resolution", 
          function(v) { set.setToEach(function(element,value) { element.setResolution(value); }, v); }
      );
      controller.registerProperty("Color", 
          function(v) { set.setToEach(function(element,value) { element.setColor(value); }, v); }
      );

	 /*** 
	  * Whether the user could change the position   
	  * @property EnabledPosition 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("EnabledPosition",
        function(v) { set.setToEach(function(element,value) {  }, v); }
        //element.getInteractionTarget(TARGET_POSITION).setMotionEnabled(enabled);
      );

	 /*** 
	  * Whether the group position also changes when the element position changes    
	  * @property MovesGroup 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("MovesGroup", 
        function(v) { set.setToEach(function(element,value) {  }, v); }
        //element.getInteractionTarget(TARGET_POSITION).setAffectsGroup(affects);
      );

	 /*** 
	  * Whether the user could change the size   
	  * @property EnabledSize 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("EnabledSize",
        function(v) { set.setToEach(function(element,value) {  }, v); }
        //element.getInteractionTarget(TARGET_SIZE).setMotionEnabled(enabled);
      );

	 /*** 
	  * Whether the group size also changes when the element size changes    
	  * @property ResizesGroup 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("ResizesGroup", 
        function(v) { set.setToEach(function(element,value) {  }, v); }
        //element.getInteractionTarget(TARGET_SIZE).setAffectsGroup(affects);
      );


      var dataFunction = function() { 
        //var index = set.getGroupPanel().getPanelInteraction().getIndexElement();
        //set.setElementInteracted(index);
        //return { index: index, position: set.getElements()[index].getPosition() };
        var panel = set.getGroupPanel();
        var index = panel.getPanelInteraction().getIndexElement();
        var element = panel.getElements()[index];
        var elementIndex = element.getSetIndex(); 
        set.setElementInteracted(elementIndex);
  	    controller.propertiesChanged("ElementInteracted");
	    //controller.reportInteractions();	 
        return { index: elementIndex, position: element.getPosition() };   
      };
      
      // Actions
      controller.registerAction("OnEnter",   dataFunction);
      controller.registerAction("OnExit",    dataFunction);
      controller.registerAction("OnPress",   dataFunction);
      controller.registerAction("OnDrag",    dataFunction);
      controller.registerAction("OnRelease", dataFunction);
      
      // controller.registerAction("OnLoadTexture", null, null, function() { element.setProjChanged(true);});
    }
};

/**
 * Element set
 * Creates a basic abstract ElementSet
 * @method elementSet
 * @param mConstructor the function that creates new elements (will be used as element = mConstructor(name))
 * @returns An abstract 2D element set
 */
EJSS_DRAWING3D.elementSet = function (mConstructor, mName) {  
  var self = EJSS_DRAWING3D.group(mName);

  // Static references
  var ElementSet = EJSS_DRAWING3D.ElementSet;		// reference for ElementSet
  
  // Configuration variables
  var mElementList = []; 
  var mNumberOfElementsSet = false;

  // Implementation variables  
  var mElementInteracted = -1;

  // Last list of removed elements
  var mLastElementList = [];

  // ----------------------------------------
  // Configuration methods
  // ----------------------------------------

  /**
   * Sets the number of element of this set
   * @method setNumberOfElements
   * @param numberOfElements the number of elements, must be >= 1
   */
  self.setNumberOfElements = function(numberOfElements) {
	mNumberOfElementsSet = true;
	adjustNumberOfElements(numberOfElements);
  };
  
  /*
   * Adjusts the number of element of this set
   * @method adjustNumberOfElements
   * @param numberOfElements the number of elements, must be >= 1
   */
  function adjustNumberOfElements(numberOfElements) {
    // keep original settings for the new elements
    var name = self.getName ? self.getName() : "unnamed";
    numberOfElements = Math.max(1,numberOfElements);
    var diff = mElementList.length-numberOfElements;
    if (diff > 0) {
    	mLastElementList = mElementList.splice(numberOfElements, diff);
		for (var j = 0; j < mLastElementList.length; j++) { 
			var panel = mLastElementList[j].getPanel(); // remove element from panel
			if(panel) panel.removeElement(mLastElementList[j])
		} 
    } else if (diff < 0) {
    	mLastElementList = [];
    	var controller = self.getController();
    	var oldElement = mElementList[mElementList.length-1];
		for (var i = mElementList.length; i < numberOfElements; i++) {
			var element = mConstructor(name+"["+i+"]"); // new element			
			element.setSet(self,i);
			oldElement.copyTo(element);
			element.setController(controller);
		  	mElementList.push(element);	
		} 
    }
  };

  /**
   * Returns the array with all elements in the set
   * @method getElements
   * @return array of Elements
   */
  self.getElements = function() {
    return mElementList;
  };

  /**
   * Returns last list of removed elements and reset the value
   * @method getLastElements
   * @return last list of Elements
   */
  self.getLastElements = function() {
  	var ret = mLastElementList.slice();
  	mLastElementList = [];
    return ret;
  };
  
  self.setElementInteracted = function(index) {
    mElementInteracted = index;
  };

  self.getElementInteracted = function() {
    return mElementInteracted;
  };
  
  // ----------------------------------------
  // Relation to its panel
  // ----------------------------------------
  
  var super_setPanel = self.setPanel;

  self.setPanel = function(panel) {
    super_setPanel(panel);
    for (var i=0,n=mElementList.length;i<n;i++) mElementList[i].setPanel(panel);
  };

  self.setProjChanged = function(needsIt) {
    for (var i=0,n=mElementList.length;i<n;i++) mElementList[i].setProjChanged(needsIt);
  };
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  /**
   * Applies to each element in the set the function f with argument v, or v[i], if it is an array
   * @method setToEach
   * @param f function
   * @param v arguments
   */
  self.setToEach = function(f,v) {
    if (Array.isArray(v)) {
      if (!mNumberOfElementsSet) { 
    	if (mElementList.length < v.length) adjustNumberOfElements(v.length);
      }
      for (var i=0,n=Math.min(mElementList.length,v.length);i<n;i++) f(mElementList[i],v[i]);
    }
    else {
      for (var i=0,n=mElementList.length;i<n;i++) f(mElementList[i],v);
    }
  };

  /**
   * Returns an array with the result of applying the function to each element of the set
   * @method getFromEach
   * @param f function
   * @return f function return 
   */
  self.getFromEach = function(f) {
    var value = [];
    for (var i=0, n=mElementList.length;i<n;i++) value[i] = f(mElementList[i]);
    return value;
  };

  /**
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    ElementSet.registerProperties(self,controller);
  };
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
	
  var element = mConstructor(mName + "[0]"); // new element			
  element.setController(self.getController());
  element.setSet(self,0);
  mElementList.push(element);
  	
  return self;
};
