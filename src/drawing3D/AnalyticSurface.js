/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Surface
 * @class Surface 
 * @constructor  
 */
EJSS_DRAWING3D.AnalyticSurface = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("NumPoints1", element.setNumPoints1);
		controller.registerProperty("MinValue1", element.setMinValue1);
		controller.registerProperty("MaxValue1", element.setMaxValue1);
		controller.registerProperty("Variable1", element.setVariable1);

		controller.registerProperty("NumPoints2", element.setNumPoints2);
		controller.registerProperty("MinValue2", element.setMinValue2);
		controller.registerProperty("MaxValue2", element.setMaxValue2);
		controller.registerProperty("Variable2", element.setVariable2);

		controller.registerProperty("FunctionX", element.setExpressionX);
		controller.registerProperty("FunctionY", element.setExpressionY);
		controller.registerProperty("FunctionZ", element.setExpressionZ);
	}

};

/**
 * Creates a 3D Surface
 * @method surface
 */
EJSS_DRAWING3D.analyticSurface = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  // Implementation variables
  var mParser = EJSS_DRAWING2D.functionsParser();
  var mExpressionX = "u";
  var mExpressionY = "v";
  var mExpressionZ = "0";

  var mNumPoints1 = 100;
  var mMinValue1 = 0;
  var mMaxValue1 = 0;
  var mVariable1 = "u";
  
  var mNumPoints2 = 100;
  var mMinValue2 = 0;
  var mMaxValue2 = 0;
  var mVariable2 = "v";  

  self.getClass = function() {
  	return "ElementAnalyticSurface";
  }

  self.setNumPoints1 = function(n) {
  	if(mNumPoints1 != n) {
  	  mNumPoints1 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setMinValue1 = function(n) {
  	if(mMinValue1 != n) {
  	  mMinValue1 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setMaxValue1 = function(n) {
  	if(mMaxValue1 != n) {
  	  mMaxValue1 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setVariable1 = function(n) {
  	if(mVariable1 != n) {
  	  mVariable1 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setNumPoints2 = function(n) {
  	if(mNumPoints2 != n) {
  	  mNumPoints2 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setMinValue2 = function(n) {
  	if(mMinValue2 != n) {
  	  mMinValue2 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setMaxValue2 = function(n) {
  	if(mMaxValue2 != n) {
  	  mMaxValue2 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setVariable2 = function(n) {
  	if(mVariable2 != n) {
  	  mVariable2 = n;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setExpressionX = function(exp) {
  	if(mExpressionX != exp) {
  	  mExpressionX = exp;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setExpressionY = function(exp) {
  	if(mExpressionY != exp) {
  	  mExpressionY = exp;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.setExpressionZ = function(exp) {
  	if(mExpressionZ != exp) {
  	  mExpressionZ = exp;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getNumPoints1 = function() {
  	return mNumPoints1;
  }

  self.getMinValue1 = function() {
  	return mMinValue1;
  }

  self.getMaxValue1 = function() {
  	return mMaxValue1;
  }

  self.getVariable1 = function() {
  	return mVariable1;
  }

  self.getNumPoints2 = function() {
  	return mNumPoints2;
  }

  self.getMinValue2 = function() {
  	return mMinValue2;
  }

  self.getMaxValue2 = function() {
  	return mMaxValue2;
  }

  self.getVariable2 = function() {
  	return mVariable2;
  }

  self.getExpressionX = function() {
  	return mParser.parse(mExpressionX);
  }

  self.getExpressionY = function() {
  	return mParser.parse(mExpressionY);
  }

  self.getExpressionZ = function() {
  	return mParser.parse(mExpressionZ);
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.AnalyticSurface.registerProperties(self, controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);

  return self;
};



