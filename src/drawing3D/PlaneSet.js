/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//PlaneSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * PlaneSet
 * @class PlaneSet 
 * @constructor  
 */
EJSS_DRAWING3D.PlaneSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller);
       
      controller.registerProperty("DirectionA", 
          function(v) { set.setToEach(function(element,value) { element.setDirectionA(value); }, v); }
      );    
      controller.registerProperty("DirectionB", 
          function(v) { set.setToEach(function(element,value) { element.setDirectionB(value); }, v); }
      );    

      controller.registerProperty("SizeA", 
          function(v) { set.setToEach(function(element,value) { element.setSizeA(value); }, v); }
      );    
      controller.registerProperty("SizeB", 
          function(v) { set.setToEach(function(element,value) { element.setSizeB(value); }, v); }
      );    
    }

};


/**
 * Creates a set of planes
 * @method planeSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.planeSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.plane,mName);

  // Static references
  var PlaneSet = EJSS_DRAWING3D.PlaneSet;		// reference for PlaneSet
  
  self.registerProperties = function(controller) {
    PlaneSet.registerProperties(self,controller);
  };

  return self;
};
