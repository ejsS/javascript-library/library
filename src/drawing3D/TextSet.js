/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//TextSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * TextSet
 * @class TextSet 
 * @constructor  
 */
EJSS_DRAWING3D.TextSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("Text", 
          function(v) { set.setToEach(function(element,value) { element.setText(value); }, v); }
      );
      controller.registerProperty("Background", 
          function(v) { set.setToEach(function(element,value) { element.setBackground(value); }, v); }
      );
      controller.registerProperty("Font", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFont(value); }, v); }
      );           
      controller.registerProperty("FontFamily", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFontFamily(value); }, v); }
      );           
      controller.registerProperty("FontSize", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFontSize(value); }, v); }
      );           
      controller.registerProperty("FontColor", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFillColor(value); }, v); }
      );           
      controller.registerProperty("FontStyle", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFontStyle(value); }, v); }
      );           
    }        

};


/**
 * Creates a set of texts
 * @method textSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.textSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.text,mName);

  // Static references
  var TextSet = EJSS_DRAWING3D.TextSet;		// reference for TextSet
  
  self.registerProperties = function(controller) {
    TextSet.registerProperties(self,controller);
  };

  return self;
};
