/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//TetrahedronSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * TetrahedronSet
 * @class TetrahedronSet 
 * @constructor  
 */
EJSS_DRAWING3D.TetrahedronSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller); 
      
    }

};


/**
 * Creates a set of tetrahedrons
 * @method tetrahedronSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.tetrahedronSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.tetrahedron,mName);

  // Static references
  var TetrahedronSet = EJSS_DRAWING3D.TetrahedronSet;		// reference for TetrahedronSet
  
  self.registerProperties = function(controller) {
    TetrahedronSet.registerProperties(self,controller);
  };

  return self;
};
