/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Plane
 * @class Plane 
 * @constructor  
 */
EJSS_DRAWING3D.Plane = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  	
		dest.setDirectionA(source.getDirectionA());
		dest.setDirectionB(source.getDirectionB());
		dest.setSizeA(source.getSizeA());
		dest.setSizeB(source.getSizeB());
  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("DirectionA", element.setDirectionA);
		controller.registerProperty("DirectionB", element.setDirectionB);

		controller.registerProperty("SizeA", element.setSizeA);
		controller.registerProperty("SizeB", element.setSizeB);
		
	}

};

/**
 * Creates a 3D Plane
 * @method plane
 */
EJSS_DRAWING3D.plane = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  // Implementation variables
  var mDirectionA = [1,0,0];
  var mDirectionB = [0,1,0];
  var mSizeA = 1;
  var mSizeB = 1;

  self.getClass = function() {
  	return "ElementPlane";
  }

  self.setDirectionA = function(direction) {
    if (!EJSS_TOOLS.compareArrays(mDirectionA,direction)) {
        mDirectionA = direction.slice(); 
  	    self.setMeshChanged(true);      
    }  	
  }

  self.getDirectionA = function() {
  	return mDirectionA;
  }

  self.setDirectionB = function(direction) {
    if (!EJSS_TOOLS.compareArrays(mDirectionB,direction)) {
        mDirectionB = direction.slice(); 
  	    self.setMeshChanged(true);      
    }  	
  }

  self.getDirectionB = function() {
  	return mDirectionB;
  }

  self.setSizeA = function(size) {
  	if(mSizeA != size) {
  		mSizeA = size;
  		self.setMeshChanged(true);
  	}
  }

  self.getSizeA = function() {
  	return mSizeA;
  }

  self.setSizeB = function(size) {
  	if(mSizeB != size) {
  		mSizeB = size;
  		self.setMeshChanged(true);
  	}
  }

  self.getSizeB = function() {
  	return mSizeB;
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Plane.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Plane.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);

  return self;
};



