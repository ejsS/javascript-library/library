/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//SpringSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * SpringSet
 * @class SpringSet 
 * @constructor  
 */
EJSS_DRAWING3D.SpringSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("Radius", 
          function(v) { set.setToEach(function(element,value) { element.setRadius(value); }, v); }
      );
      controller.registerProperty("ThinExtremes", 
          function(v) { set.setToEach(function(element,value) { element.setThinExtremes(value); }, v); }
      );
      controller.registerProperty("Solenoid", 
          function(v) { set.setToEach(function(element,value) { element.setSolenoid(value); }, v); }
      );           
      controller.registerProperty("Loops", 
          function(v) { set.setToEach(function(element,value) { element.setLoops(value); }, v); }
      );           
      controller.registerProperty("PointsPerLoop", 
          function(v) { set.setToEach(function(element,value) { element.setPointsPerLoop(value); }, v); }
      );           
    }        
};


/**
 * Creates a set of springs
 * @method springSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.springSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.spring,mName);

  // Static references
  var SpringSet = EJSS_DRAWING3D.SpringSet;		// reference for SpringSet
  
  self.registerProperties = function(controller) {
    SpringSet.registerProperties(self,controller);
  };

  return self;
};
