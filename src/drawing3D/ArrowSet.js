/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//ArrowSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/***
 * ArrowSet is a set of Arrows
 * @class EJSS_DRAWING3D.ArrowSet 
 * @constructor  
 */
EJSS_DRAWING3D.ArrowSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller); 
     
  	 /*** 
	  * Ratio arrow_length/head_length 
	  * @property HeadHeight 
	  * @type double
	  * @default "8"
	  */ 
	  controller.registerProperty("HeadHeight", 
          function(v) { set.setToEach(function(element,value) { element.setHeadHeight(value); }, v); }
      );
      
      /*** 
	  * Ratio arrow_length/head_width 
	  * @property HeadWidth 
	  * @type double
	  * @default "20"
	  */ 
      controller.registerProperty("HeadWidth", 
          function(v) { set.setToEach(function(element,value) { element.setHeadWidth(value); }, v); }
      );
        
      controller.registerProperty("LineWidth", 
          function(v) { set.setToEach(function(element,value) { element.setLineWidth(value); }, v); }
      );      
        
    }

};


/**
 * Creates a set of arrows
 * @method arrowSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.arrowSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.arrow,mName);

  // Static references
  var ArrowSet = EJSS_DRAWING3D.ArrowSet;		// reference for ArrowSet
  
  self.registerProperties = function(controller) {
    ArrowSet.registerProperties(self,controller);
  };

  return self;
};
