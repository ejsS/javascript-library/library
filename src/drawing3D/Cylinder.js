/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/***
 * Cylinder
 * @class EJSS_DRAWING3D.Cylinder
 * @constructor
 */
EJSS_DRAWING3D.Cylinder = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  	
		dest.setBottomRadius(source.getBottomRadius());  	
		dest.setTopRadius(source.getTopRadius());  	

  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

	 /*** 
	  * Bottom radius
	  * @property BottomRadius 
	  * @type number
	  * @default 1
	  */ 
		controller.registerProperty("BottomRadius", element.setBottomRadius);
	 /*** 
	  * Top radius
	  * @property TopRadius 
	  * @type number
	  * @default 1
	  */ 	
		controller.registerProperty("TopRadius", element.setTopRadius);
		
	 /*** 
	  * Minimum angle in drawing
	  * @property MinAngleU 
	  * @type number
	  * @default 0
	  */ 		
        controller.registerProperty("MinAngleU", element.setMinAngleU);
	 /*** 
	  * Maximum angle in drawing
	  * @property MaxAngleU 
	  * @type number
	  * @default 360
	  */ 		
        controller.registerProperty("MaxAngleU", element.setMaxAngleU);		
	}
};

/**
 * Creates a 3D Cylinder
 * @method cylinder
 */
EJSS_DRAWING3D.cylinder = function (name) {
  var self = EJSS_DRAWING3D.element(name);

  // Implementation variables
  var mMinAngleU = 0; 		// the start angle (in degrees) for the parallels
  var mMaxAngleU = 360;		// the end angle (in degrees) for the parallels

  var mTopRadius = 1;
  var mBottomRadius = 1;

  self.getClass = function() {
  	return "ElementCylinder";
  }
  
	 /*** 
	  * Changes the position and Size Z and adds an extra transformation
	  * so that the axis of the element has the prescribed origin and end points   
	  * @method setOriginAndEnd 
	  * @param origin a double[3] array with the origin of the axis
	  * @param end a double[3] array with the end point of the axis
	  */ 
  self.setOriginAndEnd = function(origin,end) {
  	var xc = (origin[0]+end[0])/2;
    var yc = (origin[1]+end[1])/2;
    var zc = (origin[2]+end[2])/2;
    var dx = end[0]-origin[0], dy = end[1]-origin[1], dz = end[2]-origin[2];
    var distance = Math.sqrt(dx*dx+dy*dy+dz*dz);
    var angle = Math.acos(dz/distance);
    self.setPosition([xc,yc,zc]);
    self.setSizeZ(distance);
    self.setFirstTransformation([[angle,-dy, dx, 0, xc,yc,zc]]);
  }

  self.setMinAngleU = function(angle) {
  	if(mMinAngleU != angle) {
  	  mMinAngleU = angle;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getMinAngleU = function() {
  	return mMinAngleU;
  }

  self.setMaxAngleU = function(angle) {
  	if(mMaxAngleU != angle) {
  	  mMaxAngleU = angle;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getMaxAngleU = function() {
  	return mMaxAngleU;
  }

  self.setBottomRadius = function(radius) {
  	if(mBottomRadius != radius) {
  	  mBottomRadius = radius;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getBottomRadius = function() {
  	return mBottomRadius;
  }

  self.setTopRadius = function(radius) {
  	if(mTopRadius != radius) {
  	  mTopRadius = radius;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getTopRadius = function() {
  	return mTopRadius;
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Cylinder.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Cylinder.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);

  return self;
};



