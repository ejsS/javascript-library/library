/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//CylinderSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * CylinderSet
 * @class CylinderSet 
 * @constructor  
 */
EJSS_DRAWING3D.CylinderSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller); 

      controller.registerProperty("MinAngleU", 
          function(v) { set.setToEach(function(element,value) { element.setMinAngleU(value); }, v); }
      );      
      controller.registerProperty("MaxAngleU", 
          function(v) { set.setToEach(function(element,value) { element.setMaxAngleU(value); }, v); }
      );      
      controller.registerProperty("BottomRadius", 
          function(v) { set.setToEach(function(element,value) { element.setBottomRadius(value); }, v); }
      );      
      controller.registerProperty("TopRadius", 
          function(v) { set.setToEach(function(element,value) { element.setTopRadius(value); }, v); }
      );      
    }

};


/**
 * Creates a set of cylinders
 * @method cylinderSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.cylinderSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.cylinder,mName);

  // Static references
  var CylinderSet = EJSS_DRAWING3D.CylinderSet;		// reference for CylinderSet
  
  self.registerProperties = function(controller) {
    CylinderSet.registerProperties(self,controller);
  };

  return self;
};
