/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//SurfaceSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * SurfaceSet
 * @class SurfaceSet 
 * @constructor  
 */
EJSS_DRAWING3D.SurfaceSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller);
       
      controller.registerProperty("Data", 
          function(v) { set.setToEach(function(element,value) { element.setData(value); }, v); }
      );    
    }

};


/**
 * Creates a set of surfaces
 * @method surfaceSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.surfaceSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.surface,mName);

  // Static references
  var SurfaceSet = EJSS_DRAWING3D.SurfaceSet;		// reference for SurfaceSet
  
  self.registerProperties = function(controller) {
    SurfaceSet.registerProperties(self,controller);
  };

  return self;
};
