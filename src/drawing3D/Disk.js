/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};


/***
 * Disk
 * @class EJSS_DRAWING3D.Disk
 * @constructor
 */
EJSS_DRAWING3D.Disk = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  		EJSS_DRAWING3D.Cylinder.copyTo(source,dest);
  	
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Cylinder.registerProperties(element, controller);
		// super class
		
	      controller.registerProperty("Size", function(s) {
	      		s[2] = Math.min(s[0],s[1])/1000;
	      		element.setSize(s);
	      	});
	      controller.registerProperty("SizeX", function(sx) {
	      		var sy = element.getSizeY();
	      		element.setSizeZ(Math.min(sy,sx)/1000)
	      		element.setSizeX(sx);
	      	});
	      controller.registerProperty("SizeY",function(sy) {
	      		var sx = element.getSizeX();
	      		element.setSizeZ(Math.min(sy,sx)/1000)
	      		element.setSizeY(sy);
	      	});		
	}
};

/**
 * Creates a 3D Disk
 * @method disk
 */
EJSS_DRAWING3D.disk = function (name) {
  var self = EJSS_DRAWING3D.cylinder(name);

  // Implementation variables
  self.getClass = function() {
  	return "ElementDisk";
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Disk.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Disk.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,0.001]);

  return self;
};



