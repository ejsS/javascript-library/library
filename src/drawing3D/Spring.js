/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Spring
 * @class Spring 
 * @constructor  
 */
EJSS_DRAWING3D.Spring = {
  DEF_RADIUS : 1,
  DEF_LOOPS : 8,
  DEF_PPL : 50,

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  	
		dest.setRadius(source.getRadius());
		dest.setSolenoid(source.getSolenoid());
		dest.setThinExtremes(source.getThinExtremes());
		dest.setLoops(source.getLoops());
		dest.setPointsPerLoop(source.getPointsPerLoop());		
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class	
		controller.registerProperty("Radius", element.setRadius, element.getRadius);
		controller.registerProperty("ThinExtremes", element.setThinExtremes, element.getThinExtremes);

		controller.registerProperty("Solenoid", element.setSolenoid, element.getSolenoid);
		controller.registerProperty("Loops", element.setLoops, element.getLoops);
		controller.registerProperty("PointsPerLoop", element.setPointsPerLoop, element.getPointsPerLoop);
		
	},
};

/**
 * Creates a 2D Spring
 * @method spring
 */
EJSS_DRAWING3D.spring = function(name) {
	var self = EJSS_DRAWING3D.element(name);
 
	  // Configuration variables
	var mRadius = EJSS_DRAWING3D.Spring.DEF_RADIUS;
	var mSolenoid = 0.0;
	var mThinExtremes = true;
	var mLoops;
	var mPointsPerLoop;
  var mX = 0;
  var mY = 0;
  var mZ = 0;
  var mSizeX = 1;				
  var mSizeY = 1;					
  var mSizeZ = 1;	
  
	self.getClass = function() {
		return "ElementSpring";
	}

	self.setRadius = function(radius) {
		if(mRadius != radius) {
			mRadius = radius;
			self.setMeshChanged(true);
		}		
	}

	self.getRadius = function() {
		return mRadius;
	}

	self.setSolenoid = function(solenoid) {
		if(mSolenoid != solenoid) {
			mSolenoid = solenoid;
			self.setMeshChanged(true);
		}
	}

	self.getSolenoid = function() {
		return mSolenoid;
	}

	self.setThinExtremes = function(thinExtremes) {
		if(mThinExtremes != thinExtremes) {
			mThinExtremes = thinExtremes;
			self.setMeshChanged(true);
		}
	}

	self.getThinExtremes = function() {
		return mThinExtremes;
	}

	self.setLoops = function(loops) {
		if(mLoops != loops) {
			mLoops = loops;
			self.setMeshChanged(true);
		}
	}

	self.getLoops = function() {
		return mLoops;
	}

	self.setPointsPerLoop = function(pointsPerLoop) {
		if(mPointsPerLoop != pointsPerLoop) {
			mPointsPerLoop = pointsPerLoop;
			self.setMeshChanged(true);
		}
	}

	self.getPointsPerLoop = function() {
		return mPointsPerLoop;
	}

  self.setX = function(x) { 
    if (mX!=x) { 
      mX = x; 
      self.setMeshChanged(true); 
    } 
  };

  self.getX = function() { 
    return mX; 
  };

  self.setY = function(y) {  
    if (mY!=y) { 
      mY = y; 
      self.setMeshChanged(true);
    } 
  };

  self.getY = function() { 
    return mY; 
  };

  self.setZ = function(z) {  
    if (mZ!=z) { 
      mZ = z; 
      self.setMeshChanged(true);
    } 
  };

  self.getZ = function() { 
    return mZ; 
  };

  self.setPosition = function(position) {
    self.setX(position[0]);
    self.setY(position[1]);
    self.setZ(position[2]);
  };

  self.getPosition = function() { 
    return [mX, mY, mZ]; 
  };

  self.setSizeX = function(sizeX) { 
    if (mSizeX!=sizeX) { 
      mSizeX = sizeX; 
      self.setMeshChanged(true);
    } 
  };

  self.setSizeY = function(sizeY) { 
    if (mSizeY!=sizeY) { 
      mSizeY = sizeY; 
      self.setMeshChanged(true);
    }
  };

  self.setSizeZ = function(sizeZ) { 
    if (mSizeZ!=sizeZ) { 
      mSizeZ = sizeZ; 
      self.setMeshChanged(true);
    }
  };

  self.getSizeX = function() { 
    return mSizeX; 
  };

  self.getSizeY = function() { 
    return mSizeY; 
  };

  self.getSizeZ = function() { 
    return mSizeZ; 
  };

  self.setSize = function(size) {
    self.setSizeX(size[0]);
    self.setSizeY(size[1]);
    self.setSizeZ(size[2]);
  };

  self.getSize = function() {
    return [self.getSizeX(), self.getSizeY(), self.getSizeZ()];
  };

	self.registerProperties = function(controller) {
		EJSS_DRAWING3D.Spring.registerProperties(self, controller);
	};

  	self.copyTo = function(element) {
    	EJSS_DRAWING3D.Spring.copyTo(self,element);
  	};

	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

  	self.setSize([1,1,1]);
    self.getStyle().setDrawFill(true);
    self.getStyle().setDrawLines(true);
    	
  	self.setLoops(EJSS_DRAWING2D.Spring.DEF_LOOPS);
  	self.setPointsPerLoop(EJSS_DRAWING2D.Spring.DEF_PPL);
	
	return self;
};

