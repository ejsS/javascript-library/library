/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

EJSS_WEBGLGRAPHICS.Shader = {
	//
	// Basic shaders (no lights), using for lines
	// (https://mattdesl.svbtle.com/drawing-lines-is-hard)
	// (https://jsfiddle.net/vy0we5wb/4)
	BasicVS: 
     [
     'void main() {',
     '  mat4 m = gl_ProjectionMatrix * gl_ViewMatrix * gl_ModelMatrix;',
     '  gl_Position = m * vec4(gl_Vertex.xyz, 1.0);',
     '}'
     ].join( '\r\n' ),
          
    BasicFS: 
     [
     'uniform vec3 color;',
     'uniform float opacity;',
     'void main() {',
     '    gl_FragColor = vec4(color, opacity);',
     '}'          
     ].join( '\r\n' ),
     
    //
    // Shaders using color array (only basic element)
    //
	ColorVS:
     [
     'varying vec4 color;',
     'void main() {',
     ' color = gl_Color;',
     ' gl_Position = gl_ProjectionMatrix * gl_ViewMatrix * gl_ModelMatrix * vec4(gl_Vertex.xyz, 1.0);',
     '}'
     ].join( '\r\n' ),
     
    ColorFS: 
     [
     'varying vec4 color;',
     'void main() {',
     ' gl_FragColor = color;',
     '}'    
     ].join( '\r\n' ),
	
	//
	// Shaders with light 
	// see: http://multivis.net/lecture/phong.html
	//
	LightVS:
     [
     'uniform float ka;',
     'uniform float kd;',
     'uniform float ks;',
     'uniform float shininessVal;',
     'uniform vec3 ambientColor;',
     'uniform vec3 diffuseColor;',
     'uniform vec3 specularColor;',
     'uniform vec3 lights[10];',
     'uniform int numlight;',
     'varying vec3 tmpcolor;',
     'void main() {',
     '  vec4 vmPos = gl_ViewMatrix * gl_ModelMatrix * vec4(gl_Vertex.xyz, 1.0);',
     '  vec3 normal = normalize(gl_NormalMatrix * gl_Normal);',
     '  tmpcolor = vec3(0);',
     '	for(int i = 0; i < 10; i++) { ',
     '		if(i == numlight) break;',
     '		vec3 light = normalize(lights[i] - vmPos.xyz);',
     '      float lambertian = abs(dot(normal, light));',
     '      float specular = 0.0;',
     '      if(lambertian > 0.0) {',
     '        vec3 reflect = reflect(-light, normal);',
     '        vec3 vector = normalize(-vmPos.xyz);',
     '        float specAngle = max(dot(reflect, vector), 0.0);',
     '        specular = pow(specAngle, shininessVal);',
     '      }',
     '      tmpcolor += vec3(ka * ambientColor +',
     '        kd * lambertian * diffuseColor +',
     '        ks * specular * specularColor);',
     '	} ',
     '  gl_Position = gl_ProjectionMatrix * vmPos;',
     '}'
     ].join( '\r\n' ),
     
    LightFS: 
     [
     'uniform float opacity;',
     'varying vec3 tmpcolor;',
     'void main() {',
   	 '  gl_FragColor = vec4(tmpcolor, opacity);',
     '}'        
     ].join( '\r\n' ),
     
    // Shaders with light using color array (only basic element)
	LightAndColorVS:
     [
     'uniform float ka;',
     'uniform float kd;',
     'uniform float ks;',
     'uniform float shininessVal;',
     'uniform vec3 ambientColor;',
     'uniform vec3 specularColor;',
     'uniform vec3 lights[10];',
     'uniform int numlight;',
     'varying vec3 tmpcolor;',
     'void main() {',
     '  vec4 vmPos = gl_ViewMatrix * gl_ModelMatrix * vec4(gl_Vertex.xyz, 1.0);',
     '  vec3 normal = normalize(gl_NormalMatrix * gl_Normal);',
     '  tmpcolor = vec3(0);',
     '	for(int i = 0; i < 10; i++) { ',
     '		if(i == numlight) break;',
     '		vec3 light = normalize(lights[i] - vmPos.xyz);',
     '      float lambertian = abs(dot(normal, light));',
     '      float specular = 0.0;',
     '      if(lambertian > 0.0) {',
     '        vec3 reflect = reflect(-light, normal);',
     '        vec3 vector = normalize(-vmPos.xyz);',
     '        float specAngle = max(dot(reflect, vector), 0.0);',
     '        specular = pow(specAngle, shininessVal);',
     '      }',
     '      tmpcolor += vec3(ka * ambientColor +',
     '        kd * lambertian * vec3(gl_Color) +',
     '        ks * specular * specularColor);',
     '	} ',
     '  gl_Position = gl_ProjectionMatrix * vmPos;',
     '}'
     ].join( '\r\n' ),
     
    LightAndColorFS: 
     [
     'uniform float opacity;',
     'varying vec3 tmpcolor;',
     'void main() {',
   	 '  gl_FragColor = vec4(tmpcolor, opacity);',
     '}'     
     ].join( '\r\n' ),

	//
	// Shaders for color scale
	//  Work in process!
	//    
	PaletteColorVS:
     [	
     'uniform vec3 floor;',
     'uniform vec3 ceil;',
     'uniform vec3 floorColor;',
     'uniform vec3 ceilColor;',
     'varying vec3 tmpcolor;',
     'void main() {',
     '  vec4 vmPos = gl_ViewMatrix * gl_ModelMatrix * vec4(gl_Vertex.xyz, 1.0);',
     '  vec3 normal = normalize(gl_NormalMatrix * gl_Normal);',
     '  vec3 diff = ceil - floor;',
     '  vec3 diffFloor = vmPos.xyz - floor;',
     '  vec3 diffCeil = vmPos.xyz - ceil;',
     '  float value1 = dot(diff, diffFloor);',
     '  if(value1 < 0.0) {',
     '    tmpcolor = floorColor;',
     '  } else {',
     '    float value2 = dot(diff, diffCeil);',
     '    if(value2 > 0.0) {',
     '      tmpcolor = ceilColor;',
     '    } else {',
     '      float value = length(diffFloor) / (length(diffFloor) + length(diffCeil));',
     '      tmpcolor = floorColor + value * (ceilColor - floorColor);',
     '    }',
     '  }',
     '  gl_Position = gl_ProjectionMatrix * vmPos;',
     '}'
     ].join( '\r\n' ),
     
    PaletteColorFS: 
     [
     'uniform float opacity;',
     'varying vec3 tmpcolor;',
     'void main() {',
   	 '  gl_FragColor = vec4(tmpcolor, opacity);',
     '}'      
     ].join( '\r\n' ),
          
	//
	// Shaders for texture
	//
	BasicVSwithTex :
     [	
	 'varying vec2 coord;',
	 'void main() {',
	 '  coord = gl_TexCoord.xy;',
	 '  gl_Position = gl_ProjectionMatrix * gl_ViewMatrix * gl_ModelMatrix * vec4(gl_Vertex.xyz, 1.0);',
	 '}'
     ].join( '\r\n' ),

	BasicFSwithTex:
     [	 
	 'uniform sampler2D texture;',
	 'varying vec2 coord;',
	 'void main() {',
	 '  gl_FragColor = texture2D(texture, coord);',
	 '}'	        
     ].join( '\r\n' ),
}

// Provides a convenient wrapper for WebGL shaders. A few uniforms and attributes,
// prefixed with `gl_`, are automatically added to all shader sources to make
// simple shaders easier to write.
// 
// Example usage:
// 
//     var shader = EJSS_WEBGLGRAPHICS.shader(mGL, '\
//       void main() {\
//         gl_Position = gl_ProjectionMatrix * gl_ViewMatrix * gl_ModelMatrix * gl_Vertex;\
//       }\
//     ', '\
//       uniform vec4 color;\
//       void main() {\
//         gl_FragColor = color;\
//       }\
//     ');
// 
//     shader.uniforms({
//       color: [1, 0, 0, 1]
//     }).draw(mesh);


/**
 * Compiles a shader program using the provided vertex and fragment shaders.
 * @method shader
 * @param vertexSource
 * @param fragmentSource 
 */
EJSS_WEBGLGRAPHICS.shader = function(mGL, mName, vertexSource, fragmentSource) {  
  var self = {};
  
  // Headers are prepended to the sources to provide some automatic functionality.
  var header = [
    'uniform mat3 gl_NormalMatrix;',
    'uniform mat4 gl_ModelMatrix;',
    'uniform mat4 gl_ViewMatrix;',
    'uniform mat4 gl_ProjectionMatrix;',''
    ].join('\r\n'); 
  var vertexHeader = [
    // '#extension GL_OES_standard_derivatives : enable', '',
    header, 
    'attribute vec4 gl_Vertex;',
    'attribute vec4 gl_TexCoord;',
    'attribute vec3 gl_Normal;',
    'attribute vec4 gl_Color;',''
    ].join('\r\n');
    
  var fragmentHeader = [
    // '#extension GL_OES_standard_derivatives : enable', '', 
    'precision highp float;', 
    header
    ].join('\r\n');
  
  // Attributes	 
  var attributes = {};
  // Uniforms locations
  var uniformLocations = {};

  // Check for the use of built-in matrices that require expensive matrix
  // multiplications to compute, and record these in `usedMatrices`.  
  function getUsedMatrices(vertexSource, fragmentSource) {
    var source = vertexSource + fragmentSource;
    var usedMatrices = {};
	EJSS_WEBGLGRAPHICS.Utils.regexMap(/\b(gl_[^;]*)\b;/g, header, function(groups) {
	  var name = groups[1];
	  if (source.indexOf(name) != -1) {
	    var capitalLetters = name.replace(/[a-z_]/g, '');
	    usedMatrices[capitalLetters] = mName + name;
	  }
	});
	return usedMatrices;
  }

  // The `gl_` prefix must be substituted for something else to avoid compile
  // errors, since it's a reserved prefix. This prefixes all reserved names with
  // `_`. The header is inserted after any extensions, since those must come
  // first.
  function fix(header, source) {
    var replaced = {};
    var match = /^((\s*\/\/.*\n|\s*#extension.*\n)+)[^]*$/.exec(source);
    source = match ? match[1] + header + source.substr(match[1].length) : header + source;
    EJSS_WEBGLGRAPHICS.Utils.regexMap(/\bgl_\w+\b/g, header, function(result) {
      if (!(result in replaced)) {
        source = source.replace(new RegExp('\\b' + result + '\\b', 'g'), mName + result);
        replaced[result] = true;
      }
    });
    return source;
  }
  
  // Compile and link errors are thrown as strings.
  function compileSource(type, source) {
    var shader = mGL.createShader(type);
    mGL.shaderSource(shader, source);
    mGL.compileShader(shader);
    if (!mGL.getShaderParameter(shader, mGL.COMPILE_STATUS)) {
      throw 'compile error: ' + mGL.getShaderInfoLog(shader);
    }
    return shader;
  }
   
  // 
  // Set a uniform for each property of `uniforms`. The correct `mGL.uniform*()` method is
  // inferred from the value types and from the stored uniform sampler flags.
  self.uniforms = function(uniforms) {
    mGL.useProgram(program);

    for (var name in uniforms) {
      var location = uniformLocations[name] || mGL.getUniformLocation(program, name);
      if (!location) continue;
      uniformLocations[name] = location;
      var value = uniforms[name];
      if (value instanceof Vector) {
        value = [value.x, value.y, value.z];
      } else if (value instanceof Matrix) {
        value = value.m;
      }
      if (EJSS_WEBGLGRAPHICS.Utils.isArray(value)) {
      	if(EJSS_WEBGLGRAPHICS.Utils.isArray(value[0])) {
      		// array of vector 3D (ex. lights)
      		var newvalue = [];
      		for(var j=0; j<value.length; j++) { 
      			newvalue.push(value[j][0]); 
      			newvalue.push(value[j][1]); 
      			newvalue.push(value[j][2]); 
      		}
      		mGL.uniform3fv(location, new Float32Array(newvalue)); 
      	} else {
	        switch (value.length) {
	          case 1: mGL.uniform1fv(location, new Float32Array(value)); break;
	          case 2: mGL.uniform2fv(location, new Float32Array(value)); break;
	          case 3: mGL.uniform3fv(location, new Float32Array(value)); break;
	          case 4: mGL.uniform4fv(location, new Float32Array(value)); break;
	          // Matrices are automatically transposed, since WebGL uses column-major
	          // indices instead of row-major indices.
	          case 9: mGL.uniformMatrix3fv(location, false, new Float32Array([
	            value[0], value[3], value[6],
	            value[1], value[4], value[7],
	            value[2], value[5], value[8]
	          ])); break;
	          case 16: mGL.uniformMatrix4fv(location, false, new Float32Array([
	            value[0], value[4], value[8], value[12],
	            value[1], value[5], value[9], value[13],
	            value[2], value[6], value[10], value[14],
	            value[3], value[7], value[11], value[15]
	          ])); break;
	          default: throw 'don\'t know how to load uniform "' + name + '" of length ' + value.length;
			}
        }
      } else if (EJSS_WEBGLGRAPHICS.Utils.isNumber(value)) { 
        ((name.indexOf('texture') != -1 || name.indexOf('num') != -1) ? mGL.uniform1i : mGL.uniform1f).call(mGL, location, value);
      } else {
        throw 'attempted to set uniform "' + name + '" to invalid value ' + value;
      }
    }
  }

  // 
  // Sets all uniform matrix attributes, binds all relevant buffers, and draws the
  // mesh geometry as indexed triangles or indexed lines. Set `mode` to `mGL.LINES`
  // (and either add indices to `lines` or call `computeWireframe()`) to draw the
  // mesh in wireframe.
  self.draw = function(mesh, mode) {
    self.drawBuffers(mesh.getVertexBuffers(),
      mesh.getIndexBuffers()[mode == mGL.LINES ? 'lines' : 'triangles'],
      arguments.length < 2 ? mGL.TRIANGLES : mode);
  }

  // ### .drawBuffers(vertexBuffers, indexBuffer, mode)
  // 
  // Sets all uniform matrix attributes, binds all relevant buffers, and draws the
  // indexed mesh geometry. The `vertexBuffers` argument is a map from attribute
  // names to `Buffer` objects of type `mGL.ARRAY_BUFFER`, `indexBuffer` is a `Buffer`
  // object of type `mGL.ELEMENT_ARRAY_BUFFER`, and `mode` is a WebGL primitive mode
  // like `mGL.TRIANGLES` or `mGL.LINES`. This method automatically creates and caches
  // vertex attribute pointers for attributes as needed.
  self.drawBuffers = function(vertexBuffers, indexBuffer, mode) {
    // Only construct up the built-in matrices we need for this shader.
    var used = getUsedMatrices(vertexSource, fragmentSource);
    var MM = mGL.modelMatrix;
    var VM = mGL.viewMatrix;
    var PM = mGL.projectionMatrix;
    var matrices = {};
    if (used.MM) matrices[used.MM] = MM;
    if (used.VM) matrices[used.VM] = VM;
    if (used.PM) matrices[used.PM] = PM;
    if (used.NM) {
		var m = VM.multiply(MM).inverse().m; // MVMI
      	matrices[used.NM] = [m[0], m[4], m[8], m[1], m[5], m[9], m[2], m[6], m[10]];
    }
    self.uniforms(matrices);

    // Create and enable attribute pointers as necessary.
    var length = 0;
    for (var attribute in vertexBuffers) {
      var buffer = vertexBuffers[attribute];
      var location = attributes[attribute] ||
        mGL.getAttribLocation(program, attribute.replace(/^(gl_.*)$/, mName + '$1'));
      if (location == -1 || !buffer.getBuffer()) continue;
      attributes[attribute] = location;
      mGL.bindBuffer(mGL.ARRAY_BUFFER, buffer.getBuffer());
      mGL.enableVertexAttribArray(location);
      mGL.vertexAttribPointer(location, buffer.getBuffer().spacing, mGL.FLOAT, false, 0, 0);
      length = buffer.getBuffer().length / buffer.getBuffer().spacing;
    }

    // Disable unused attribute pointers.
    for (var attribute in attributes) {
      if (!(attribute in vertexBuffers)) {
        mGL.disableVertexAttribArray(attributes[attribute]);
      }
    }
    
    // Draw the geometry.
    if (length && (!indexBuffer || indexBuffer.getBuffer())) {
      if (indexBuffer) {
        mGL.bindBuffer(mGL.ELEMENT_ARRAY_BUFFER, indexBuffer.getBuffer());
        mGL.drawElements(mode, indexBuffer.getBuffer().length, mGL.UNSIGNED_SHORT, 0);
      } else {
        mGL.drawArrays(mode, 0, length);
      }
    }
  }
 
  // 
  // Initialization 
  var program = mGL.createProgram();
  mGL.attachShader(program, compileSource(mGL.VERTEX_SHADER, fix(vertexHeader, vertexSource)));
  mGL.attachShader(program, compileSource(mGL.FRAGMENT_SHADER, fix(fragmentHeader, fragmentSource)));
  mGL.linkProgram(program);
  if (!mGL.getProgramParameter(program, mGL.LINK_STATUS)) {
    throw 'link error: ' + mGL.getProgramInfoLog(program);
  }
 
  return self;  
}


