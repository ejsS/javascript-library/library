/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL box
 */
EJSS_WEBGLGRAPHICS.box = function(mGL, mElement) {
	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName());
	  
	  var octant = [[-1,-1,-1], [1,-1,-1], [-1,1,-1], [1,1,-1], [-1,-1,1], [1,-1,1], [-1,1,1], [1,1,1]];
	  
	  var coords = [[0,0],[1,0],[0,1],[1,1]];

	  var normals = [
	   [-1, 0, 0], // -x
	   [+1, 0, 0], // +x
	   [0, -1, 0], // -y
	   [0, +1, 0], // +y
	   [0, 0, -1], // -z
	   [0, 0, +1]  // +z
	  ];
	  
	  var boxData = [
	   [0, 2, 4, 6], // -x
	   [1, 3, 5, 7], // +x
	   [0, 4, 1, 5], // -y
	   [2, 6, 3, 7], // +y
	   [0, 2, 1, 3], // -z
	   [4, 6, 5, 7]  // +z
	  ];
	
  	  var isClosedTop = mElement.getStyle().getClosedTop();
  	  var isClosedBottom = mElement.getStyle().getClosedBottom();
	  var isClosedRight = mElement.getStyle().getClosedRight();
	  var isClosedLeft = mElement.getStyle().getClosedLeft();	
	  var reduceZby = mElement.getReduceZby();
	
	  // build box
	  for (var i = 0; i < boxData.length; i++) {
	    var data = boxData[i], v = i * 4;
	    for (var j = 0; j < 4; j++) {
	      var d = data[j];
	      if(d==6 || d==7)
	      	mesh.vertices.push([octant[d][0], octant[d][1], octant[d][2] - (2*reduceZby)]);
	      else
      	  	mesh.vertices.push(octant[d]);
      	  mesh.coords.push(coords[j]);
      	  mesh.normals.push(normals[i]);
	    }
	    if(!(i == 2 && !isClosedLeft) &&
	       !(i == 3 && !isClosedRight) &&
	       !(i == 5 && !isClosedTop) &&
	       !(i == 4 && !isClosedBottom)) {
	    	mesh.triangles.push([v, v + 1, v + 2]);
	    	mesh.triangles.push([v + 2, v + 1, v]);    
	    	mesh.triangles.push([v + 2, v + 1, v + 3]);    
	    	mesh.triangles.push([v + 3, v + 1, v + 2]);
	    }
	    mesh.lines.push([v,v+1]);
	    mesh.lines.push([v,v+2]);
	    mesh.lines.push([v+2,v+3]);
	    mesh.lines.push([v+1,v+3]);
	  }
	  	  		  	 
	  mesh.compile();
	
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
         
      return mesh;
}

