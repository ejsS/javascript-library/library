/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL custom
 */

EJSS_WEBGLGRAPHICS.analyticCurve = function(mGL, mElement) {         
      evaluate = function(expression, v1, p1, v2, p2) {
       var vblevalue = {};
       vblevalue[v1] = p1;
	   vblevalue[v2] = p2;
       return expression.evaluate(vblevalue);
      };

	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName(), 
	  	{coords:false, normals:false, triangles:false, lines:true, colors:false});

	  var numRows = mElement.getNumPoints();
	  var minRows = mElement.getMinValue();
	  var maxRows = mElement.getMaxValue();
	  var d1 = Math.abs((maxRows - minRows) / (numRows - 1));
	  
	  var exprX = mElement.getExpressionX();		
	  var exprY = mElement.getExpressionY();
	  var exprZ = mElement.getExpressionZ();
	  
	  var var1 = mElement.getVariable();
	  
	  var point1 = minRows;	  					 
	  for (var i = 0; i < numRows; i++) {
		// expresion X
		var x = evaluate(exprX, var1, point1);
		// expresion Y
		var y = evaluate(exprY, var1, point1); 
		// expresion Z		 
		var z = evaluate(exprZ, var1, point1);

	  	mesh.vertices.push([x,y,z]);
	  	if(i!=0) mesh.lines.push([i-1, i]);

		point1 = point1 + d1;
	  }
	  
	  mesh.compile();
	  	  	  
	  mGL.addElement(mElement.getName() + ".mesh", mesh);

      return mesh;
}

