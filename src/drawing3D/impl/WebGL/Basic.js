/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL basic
 */

EJSS_WEBGLGRAPHICS.basic = function(mGL, mElement) {   
	  mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName());
	  // set values
  	  mesh.setVertices(mElement.getVertices());	  
	  mesh.setCoords(mElement.getVertices());
	  mesh.setTriangles(mElement.getTriangles());
      mesh.setColors(mElement.getColors());

	  // compute normals
	  if (mElement.getNormals().length != 0) {
	  	mesh.setNormals(mElement.getNormals());
	  } else {
	  	mesh.computeNormals();
	  };
  
	  // compute wire
	  mesh.computeWireframe();
	  
	  mesh.compile();
	  
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
      
      return mesh;
}

