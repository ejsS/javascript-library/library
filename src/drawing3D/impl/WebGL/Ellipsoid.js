/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL ellipsoid
 */
EJSS_WEBGLGRAPHICS.ellipsoid = function(mGL, mElement) {
	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName());
	  	  
	  var indexer = EJSS_WEBGLGRAPHICS.indexer();
	
	  // generate vertex for body
	  var latitudeBands = mElement.getResolutionV();  	
	  var longitudeBands = mElement.getResolutionU();
	  for (var latNumber = 0; latNumber <= latitudeBands; latNumber++) {
		  var theta = latNumber * Math.PI / latitudeBands;
		  var sinTheta = Math.sin(theta);
		  var cosTheta = Math.cos(theta);
		
		  for (var longNumber = 0; longNumber <= longitudeBands; longNumber++) {
		    var phi = longNumber * 2 * Math.PI / longitudeBands;
		    var sinPhi = Math.sin(phi);
		    var cosPhi = Math.cos(phi);
		
		    var y = cosPhi * sinTheta;
		    var z = -cosTheta;
		    var x = -sinPhi * sinTheta;
		    var u = 1 - (longNumber / longitudeBands);
		    var v = (latNumber / latitudeBands);
		
			mesh.vertices.push([-x, y, z]);
			mesh.coords.push([u,v]);
		  }		  	
	  }
	  	
      // generate vertex for central axis
	  for (var latNumber = 0; latNumber <= latitudeBands; latNumber++) {		
	      var v =  latNumber / latitudeBands;
		  var theta = latNumber * Math.PI / latitudeBands;	      
	      var z = -Math.cos(theta);
	      // var z = - 1 + (latNumber * (2 / latitudeBands)); // proportional in axis
		
	      mesh.vertices.push([0, 0, z]);
		  mesh.coords.push([0,v]);
	  }	  
  		
	  // limits for sphera
	  var mMinAngleU = mElement.getMinAngleU();
	  var mMaxAngleU = mElement.getMaxAngleU();
	  var mMinAngleV = mElement.getMinAngleV() + 90;
	  var mMaxAngleV = mElement.getMaxAngleV() + 90;
	  
	  // build triangles
	  var latNumber;
  	  var minLatNumber = Math.floor(mMinAngleV * latitudeBands / 180); 
  	  var maxLatNumber = Math.floor(mMaxAngleV * latitudeBands / 180);
	  for (latNumber = minLatNumber; latNumber < maxLatNumber; latNumber++) {
	  	  var longNumber;	  	  
	  	  // close sphera
	  	  var minLongNumber = Math.floor(mMinAngleU * longitudeBands / 360); 
	  	  var maxLongNumber = Math.floor(mMaxAngleU * longitudeBands / 360);
	  	  var first, second;
		  for (longNumber = minLongNumber; longNumber < maxLongNumber; longNumber++) {
		    first = (latNumber * (longitudeBands + 1)) + longNumber;
		    second = first + longitudeBands + 1;
		    mesh.triangles.push([first, second, first + 1]);
		    mesh.triangles.push([second, second + 1, first + 1]);
		    
		    mesh.lines.push([first, second]);
		    mesh.lines.push([first, first+1]);
		    mesh.lines.push([second, second+1]);
		  }
		  			  
		  // last close sphera
	      if((mMinAngleU != 0) || (mMaxAngleU != 360)) { 	// quesito					  	  	
			// close left and right
	        var firstAxis = mesh.vertices.length - (latitudeBands - latNumber + 1); // points central axis
			if(mElement.getStyle().getClosedLeft()) {
		    	mesh.triangles.push([first + 1, second + 1, firstAxis]);
		    	mesh.triangles.push([second + 1, firstAxis + 1, firstAxis]);

		    	mesh.lines.push([first+1, second+1]);
		    	mesh.lines.push([first+1, firstAxis]);
		    	mesh.lines.push([second+1, firstAxis+1]);
		    	mesh.lines.push([firstAxis, firstAxis+1]);
		    }
		    if(mElement.getStyle().getClosedRight()) {
				// points first latitude
		    	var firstMin = (latNumber * (longitudeBands + 1)) + minLongNumber;
				var secondMin = firstMin + longitudeBands + 1;
		    	mesh.triangles.push([firstAxis, secondMin, firstMin]);
		    	mesh.triangles.push([firstAxis, firstAxis + 1, secondMin]);

		    	mesh.lines.push([firstMin, secondMin]);
		    	mesh.lines.push([firstMin, firstAxis]);
		    	mesh.lines.push([secondMin, firstAxis+1]);
		    	mesh.lines.push([firstAxis, firstAxis+1]);
		    }			    
		  }			      	
	  }
	  
	  // close bottom
      if(mElement.getStyle().getClosedBottom() && (mMinAngleV > 0)) {      	  
		  var first, longNumber, axis;
		  for (longNumber = minLongNumber; longNumber < maxLongNumber; longNumber++) {
		    first = (minLatNumber * (longitudeBands + 1)) + longNumber;
		    axis = mesh.vertices.length - (latitudeBands - minLatNumber + 1);
		    mesh.triangles.push([first + 1, axis, first]);
		    
		    mesh.lines.push([first+1,axis]);
		    mesh.lines.push([first,first+1]);
		  }      	
      }
      
	  // close top
      if(mElement.getStyle().getClosedTop() && (mMaxAngleV < 180)) {
      	  var first, longNumber, axis;
		  for (longNumber = minLongNumber; longNumber < maxLongNumber; longNumber++) {
		    first = (maxLatNumber * (longitudeBands + 1)) + longNumber;
		    axis = mesh.vertices.length - (latitudeBands - maxLatNumber + 1);
		    mesh.triangles.push([first, axis, first + 1]);

		    mesh.lines.push([first+1,axis]);
		    mesh.lines.push([first,first+1]);
		  }      	      	
      }		      
	    	  
	  // compute normals
	  if((mMinAngleU == 0) && (mMaxAngleU == 360)) 
	  	mesh.normals = mesh.vertices;
	  else
	    mesh.computeNormals();	  
  	 
	  // compute wire
	  // mesh.computeWireframe();
	  
	  mesh.compile();
	  
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
   
      return mesh;
}

