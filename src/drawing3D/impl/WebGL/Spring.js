/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL custom
 */

EJSS_WEBGLGRAPHICS.spring = function(mGL, mElement) {   
	function drawSpring(loops, pointsPerLoop, radius, solenoid, thinExtremes, origp, endp, moveTo, lineTo) {
		var segments = loops * pointsPerLoop;
		var delta = 2.0 * Math.PI / pointsPerLoop;
		if (radius < 0) delta *= -1;
		var pre = pointsPerLoop / 2;
		// normalize sizes        	
		var u1 = EJSS_TOOLS.Mathematics.normalTo([endp.x,endp.y,endp.z]);
		var u2 = EJSS_TOOLS.Mathematics.normalize(EJSS_TOOLS.Mathematics.crossProduct([endp.x,endp.y,endp.z], u1));
		
	    for (var i=0; i<=segments; i++) {
	      var k;
	      if (thinExtremes) {	// extremes
	        if (i < pre) k = 0;
	        else if (i < pointsPerLoop) k = i - pre;
	        else if (i > (segments - pre)) k = 0;
	        else if (i > (segments - pointsPerLoop)) k = segments - i - pre;
	        else k = pre;
	      }
	      else k = pre;
	      var angle = Math.PI/2 + i*delta;
	      var cos = Math.cos(angle), sin = Math.sin(angle);
	      var xx = origp.x + i*endp.x/segments + k*radius*(cos*u1[0] + sin*u2[0])/pre;
	      var yy = origp.y + i*endp.y/segments + k*radius*(cos*u1[1] + sin*u2[1])/pre;
	      var zz = origp.z + i*endp.z/segments + k*radius*(cos*u1[2] + sin*u2[2])/pre;
	      if (solenoid != 0.0)  {
	        var cte = k*Math.cos(i*2*Math.PI/pointsPerLoop)/pre;
	        xx += solenoid*cte*endp.x;
	        yy += solenoid*cte*endp.y;
	        zz += solenoid*cte*endp.z;
	      }
	      if (i==0)
	      	moveTo(xx,yy,zz); 
	      else 
	      	lineTo(xx,yy,zz);
	    }
	}

    var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName());

  	var endpoint = new Vector(mElement.getSize());
  	
  	// after it is translated
  	// var origpoint = new Vector(mElement.getPosition());
    var origpoint = new Vector([0,0,0]);
    
	// set attributes
	var index = 0;
	drawSpring(mElement.getLoops(), mElement.getPointsPerLoop(), 
			mElement.getRadius(), mElement.getSolenoid(), mElement.getThinExtremes(), origpoint, endpoint,
			function(xx,yy,zz) { mesh.vertices.push([xx, yy, zz]); index++;}, 
			function(xx,yy,zz) { mesh.vertices.push([xx, yy, zz]); mesh.lines.push([index-1,index]); index++});   
	
	// compute normals
	mesh.computeNormals();
	   		
	mesh.compile();
	  	  	  
	mGL.addElement(mElement.getName() + ".mesh", mesh);

    return mesh;
    
}