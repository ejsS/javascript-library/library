// Provides a simple wrapper around WebGL textures that supports render-to-texture.

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

EJSS_WEBGLGRAPHICS.Texture = {
// ### GL.Texture.fromImage(image[, options])
// 
// Return a new image created from `image`, an `<img>` tag.
fromImage: function(mGL, image, options) {
  options = options || {};
  var texture = EJSS_WEBGLGRAPHICS.texture(mGL, image.width, image.height, options);
  try {
    mGL.texImage2D(mGL.TEXTURE_2D, 0, texture.getFormat(), texture.getFormat(), texture.getType(), image);
  } catch (e) {
    if (location.protocol == 'file:') {
    	// for Chrome --allow-file-access-from-files
      throw 'image not loaded for security reasons (serve this page over "http://" instead)';
    } else {
      throw 'image not loaded for security reasons (image must originate from the same ' +
        'domain as this page or use Cross-Origin Resource Sharing)';
    }
  }
  if (options.minFilter && options.minFilter != mGL.NEAREST && options.minFilter != mGL.LINEAR) {
    mGL.generateMipmap(mGL.TEXTURE_2D);
  }
  return texture;
},

// ### GL.Texture.fromURL(url[, options])
// 
// Returns a checkerboard texture that will switch to the correct texture when
// it loads.
fromURL: function(mGL, url, options, callback) {
  checkerboardCanvas = (function() {
    var c = document.createElement('canvas').getContext('2d');
    c.canvas.width = c.canvas.height = 128;
    for (var y = 0; y < c.canvas.height; y += 16) {
      for (var x = 0; x < c.canvas.width; x += 16) {
        c.fillStyle = (x ^ y) & 16 ? '#FFF' : '#DDD';
        c.fillRect(x, y, 16, 16);
      }
    }
    return c.canvas;
  })();
  var texture = EJSS_WEBGLGRAPHICS.Texture.fromImage(mGL, checkerboardCanvas, options);
  var image = new Image();
  image.onload = function() {
  	EJSS_WEBGLGRAPHICS.Texture.fromImage(mGL, image, options).swapWith(texture);
  	callback();
  };
  image.src = url;
  return texture;
}

}

// ### new GL.Texture(width, height[, options])
//
// The arguments `width` and `height` give the size of the texture in texels.
// WebGL texture dimensions must be powers of two unless `filter` is set to
// either `mGL.NEAREST` or `mGL.LINEAR` and `wrap` is set to `mGL.CLAMP_TO_EDGE`
// (which they are by default).
//
// Texture parameters can be passed in via the `options` argument.
// Example usage:
// 
//     var t = new GL.Texture(256, 256, {
//       // Defaults to mGL.LINEAR, set both at once with "filter"
//       magFilter: mGL.NEAREST,
//       minFilter: mGL.LINEAR,
// 
//       // Defaults to mGL.CLAMP_TO_EDGE, set both at once with "wrap"
//       wrapS: mGL.REPEAT,
//       wrapT: mGL.REPEAT,
// 
//       format: mGL.RGB, // Defaults to mGL.RGBA
//       type: mGL.FLOAT // Defaults to mGL.UNSIGNED_BYTE
//     });
EJSS_WEBGLGRAPHICS.texture = function(mGL, width, height, options) {
  var self = {};
  options = options || {};

  var framebuffer;
  var renderbuffer;

  var id = mGL.createTexture();
  var format = options.format || mGL.RGBA;
  var type = options.type || mGL.UNSIGNED_BYTE;


  self.getId = function() {
  	return id;
  }
  
  self.setId = function(v) {
  	id = v;
  }
  
  self.getFormat = function() {
  	return format;
  }
  
  self.setFormat = function(v) {
  	format = v;
  }
  
  self.getType = function() {
  	return type;
  }
  
  self.setType = function(v) {
  	type = v;
  }

  self.getWidth = function() {
  	return width;
  }

  self.setWidth = function(v) {
  	width = v;
  }

  self.getHeight = function() {
    return height;	
  }
  
  self.setHeight = function(v) {
  	height = v;
  }

  // ### .bind([unit])
  // 
  // Bind this texture to the given texture unit (0-7, defaults to 0).
  self.bind = function(unit) {
    mGL.activeTexture(mGL.TEXTURE0 + (unit || 0));
    mGL.bindTexture(mGL.TEXTURE_2D, id);
  },

  // ### .unbind([unit])
  // 
  // Clear the given texture unit (0-7, defaults to 0).
  self.unbind = function(unit) {
    mGL.activeTexture(mGL.TEXTURE0 + (unit || 0));
    mGL.bindTexture(mGL.TEXTURE_2D, null);
  },

  // ### .drawTo(callback)
  // 
  // Render all draw calls in `callback` to this texture. This method sets up
  // a framebuffer with this texture as the color attachment and a renderbuffer
  // as the depth attachment. It also temporarily changes the viewport to the
  // size of the texture.
  // 
  // Example usage:
  // 
  //     texture.drawTo(function() {
  //       mGL.clearColor(1, 0, 0, 1);
  //       mGL.clear(mGL.COLOR_BUFFER_BIT);
  //     });
  self.drawTo = function(callback) {
    var v = mGL.getParameter(mGL.VIEWPORT);
    framebuffer = framebuffer || mGL.createFramebuffer();
    renderbuffer = renderbuffer || mGL.createRenderbuffer();
    mGL.bindFramebuffer(mGL.FRAMEBUFFER, framebuffer);
    mGL.bindRenderbuffer(mGL.RENDERBUFFER, renderbuffer);
    if (width != renderbuffer.width || height != renderbuffer.height) {
      renderbuffer.width = width;
      renderbuffer.height = height;
      mGL.renderbufferStorage(mGL.RENDERBUFFER, mGL.DEPTH_COMPONENT16, width, height);
    }
    mGL.framebufferTexture2D(mGL.FRAMEBUFFER, mGL.COLOR_ATTACHMENT0, mGL.TEXTURE_2D, id, 0);
    mGL.framebufferRenderbuffer(mGL.FRAMEBUFFER, mGL.DEPTH_ATTACHMENT, mGL.RENDERBUFFER, renderbuffer);
    mGL.viewport(0, 0, width, height);

    callback();

    mGL.bindFramebuffer(mGL.FRAMEBUFFER, null);
    mGL.bindRenderbuffer(mGL.RENDERBUFFER, null);
    mGL.viewport(v[0], v[1], v[2], v[3]);
  },

  // ### .swapWith(other)
  // 
  // Switch this texture with `other`, useful for the ping-pong rendering
  // technique used in multi-stage rendering.
  self.swapWith = function(other) {
    var temp;
    temp = other.getId(); other.setId(id); id = temp;
    temp = other.getWidth(); other.setWidth(width); width = temp;
    temp = other.getHeight(); other.setHeight(height); height = temp;
  }
  
  mGL.bindTexture(mGL.TEXTURE_2D, id);
  mGL.pixelStorei(mGL.UNPACK_FLIP_Y_WEBGL, 1);
  // mGL.pixelStorei(mGL.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1);
  mGL.texParameteri(mGL.TEXTURE_2D, mGL.TEXTURE_MAG_FILTER, options.filter || options.magFilter || mGL.LINEAR);
  mGL.texParameteri(mGL.TEXTURE_2D, mGL.TEXTURE_MIN_FILTER, options.filter || options.minFilter || mGL.LINEAR);
  mGL.texParameteri(mGL.TEXTURE_2D, mGL.TEXTURE_WRAP_S, options.wrap || options.wrapS || mGL.CLAMP_TO_EDGE);
  mGL.texParameteri(mGL.TEXTURE_2D, mGL.TEXTURE_WRAP_T, options.wrap || options.wrapT || mGL.CLAMP_TO_EDGE);
  mGL.texImage2D(mGL.TEXTURE_2D, 0, format, width, height, 0, format, type, null);

  return self;  
};

