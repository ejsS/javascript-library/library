/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL trail
 */

EJSS_WEBGLGRAPHICS.trail = function(mGL, mElement) {   
  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName(), 
	{coords:false, normals:true, triangles:true, lines:false, colors:false});

  var points = mElement.getPoints();
  if (points.length > 1) {
	  var resolutionU = mElement.getResolutionU();
	  var radius = mElement.getLineWidth() / 2;
	  
	  for(var pos=0; pos<points.length-1; pos++) {
	  	  var origpoint = new Vector(points[pos]);
	  	  var endpoint = new Vector(points[pos+1]);
	  	  var line = endpoint.subtract(origpoint);
	      
		  // generate vertex for body
		  var vertices3D = [];	  
		  for (var x = 0; x < resolutionU; x++ ) {
			var u = x / (resolutionU - 1);
			var xpos = radius * Math.sin( u * Math.PI * 2 );
			var ypos = radius * Math.cos( u * Math.PI * 2 );
	
			vertices3D.push(new Vector([xpos, ypos, 0])); // top vertex
			vertices3D.push(new Vector([xpos, ypos, 0])); // bottom vertex
		  }
		  
		  // rotate and translate top and bottom
		  var cross = line.cross(new Vector(0,0,1));
		  if(cross.length()==0) cross.y = 1; // if endoint in axis Z, get axis Y to rotate 
		  // console.log("cross:" + cross.x + " " + cross.y + " " + cross.z + " ");	  
		  var angle = line.angle(new Vector(0,0,1));
		  // console.log("angle:" + angle);
		  var result = new Matrix();
		  Matrix.rotate(-angle,cross.x,cross.y,cross.z,0,0,0,result);
		  for(var i=0; i<vertices3D.length; i++) {
		  	var vector = result.transformPoint(vertices3D[i]);
		  	if (i % 2 == 0) {
		  		// add endpoint to translate	  	
		  		mesh.vertices.push(vector.add(endpoint).toArray());	  		
		  	} else {
		  		// add origpoint to translate
		  		mesh.vertices.push(vector.add(origpoint).toArray());
		  	}
		  }
		  
		  // create surface triangles
		  for (var x = 0; x < resolutionU - 1; x++ ) {
		    var first = 2 * (x + pos * resolutionU);		    
		    var fourth = 2 * (x  + pos * resolutionU) + 2;
		    var second = 2 * (x  + pos * resolutionU) + 1;
		    var third = 2 * (x  + pos * resolutionU) + 3;
				
	        mesh.triangles.push([third, second, first]);      
	        mesh.triangles.push([fourth, third, first]);
		  }		
	  }		
	
	  // last top cap
	  mesh.vertices.push(new Vector(points[points.length-1]).toArray());		
	  for (var i = 0; i < resolutionU - 1; i++) {
	  	var first = mesh.vertices.length - 1;
	  	var second = 2*i+2 + (2 * (points.length - 2) * resolutionU);
	  	var third = 2*i + (2 * (points.length - 2) * resolutionU);
	  	mesh.triangles.push([first, second, third]);
	  }        
	
	  // first bottom cap
	  mesh.vertices.push(new Vector(points[0]).toArray());
	  for (var i = 0; i < resolutionU - 1; i++) {
	  	var first = 2*i+1;
	  	var second = 2*i+3;
	  	var third = mesh.vertices.length - 1;
	   	mesh.triangles.push([first, second, third]);
	  }        
	
	  // nodes cap
	  for(var pos=1; pos<points.length-1; pos++) {
		for (var x = 0; x < resolutionU - 1; x++ ) {
			var first = 2 * (x + (pos - 1) * resolutionU);		    
			var fourth = 2 * (x + (pos - 1) * resolutionU) + 2;
			var second = 2 * (x + pos * resolutionU) + 1;
			var third = 2 * (x + pos * resolutionU) + 3;			
	        mesh.triangles.push([third, second, first]);      
	        mesh.triangles.push([fourth, third, first]);
		}		    		
	  }
	   
	  mesh.computeNormals();	
		  
	  mesh.compile();		
  }
	  	
  mGL.addElement(mElement.getName() + ".mesh", mesh);
        
  return mesh;
  
  
/* Previously it was possible to manage lineWidth and then the segment was easy ...
  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName(), 
  	{coords:false, normals:false, triangles:false, lines:true, colors:false});
  	  
  var points = mElement.getPoints();
  var vertices = [];
  var lines = [];
  var index = 0;
  for(var i=0; i<points.length; i++) {
  	vertices[i] = [points[i][0],points[i][1],points[i][2]];
  	if(i!=0 && points[i][3] == 1) { // 0 is NOT CONNECTION
  		lines[index++] = [i-1, i];
  	}
  }
  
  // set values
  mesh.setVertices(vertices);	  	  
  if (lines.length > 0) mesh.setLines(lines);
  
  mesh.compile();
  
  mGL.addElement(mElement.getName() + ".mesh", mesh);
   
  return mesh;
*/   
}

