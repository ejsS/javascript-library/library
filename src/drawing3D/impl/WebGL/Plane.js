/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL plane
 */
EJSS_WEBGLGRAPHICS.plane = function(mGL, mElement) {
	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL, mElement.getName());
	  		 
	  var dirA = mElement.getDirectionA();
	  var dirB = mElement.getDirectionB();
	  var sizeA = mElement.getSizeA();
	  var sizeB = mElement.getSizeB();
	  var vecZ = Vector.cross(new Vector(dirA), new Vector(dirB), new Vector()); 
	  var dirZ = vecZ.unit().toArray();
	  
	  var trans = new Matrix( [
	  		dirA[0],dirB[0],dirZ[0],0,
	  		dirA[1],dirB[1],dirZ[1],0,
	  		dirA[2],dirB[2],dirZ[2],0,
	  		0,0,0,1,
	  		]);
	  
	  var resolution = mElement.getResolutionU();     // tesselation level 	
	  for (var y = 0; y <= resolution; y++) {
	    var t = (y / resolution) * sizeB;
	    for (var x = 0; x <= resolution; x++) {
	      var s = (x / resolution) * sizeA;	      
	      var vec = new Vector([2 * s - 1, 2 * t - 1, 0]);
	      var res = trans.transformVector(vec);	      
	      mesh.vertices.push(res.toArray());
	      mesh.coords.push([s, t]);
	      mesh.normals.push([0, 0, 1]);	      
	      if (x < resolution && y < resolution) {
	        var i = x + y * (resolution + 1);
	        mesh.triangles.push([i + resolution + 1, i + 1, i]);
	        mesh.triangles.push([i + resolution + 2, i + 1, i + resolution + 1]);
	        
	        mesh.lines.push([i + resolution + 1, i]);
	        mesh.lines.push([i + 1, i]);
	        mesh.lines.push([i + resolution + 2, i + 1]);
	        mesh.lines.push([i + resolution + 2, i + resolution + 1]);
	      }
	    }
	  }
	  
	  mesh.compile();
	
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
        
      return mesh;
}

