/**
 * Deployment for 3D SVG drawing.
 * @module SVGGraphics 
 */
	
// Represents indexed triangle geometry with arbitrary additional attributes.
// You need a shader to draw a mesh; meshes can't draw themselves.
// 
// A mesh is a collection of `Buffer` objects which are either vertex buffers
// (holding per-vertex attributes) or index buffers (holding the order in which
// vertices are rendered). By default, a mesh has a position vertex buffer called
// `vertices` and a triangle index buffer called `triangles`. New buffers can be
// added using `addVertexBuffer()` and `addIndexBuffer()`. Two strings are
// required when adding a new vertex buffer, the name of the data array on the
// mesh instance and the name of the GLSL attribute in the vertex shader.
// 
// Example usage:
// 
//     var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL, { coords: true, lines: true });
// 
//     // Default attribute "vertices", available as "gl_Vertex" in
//     // the vertex shader
//     mesh.setVertices([[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0]]);
// 
//     // Optional attribute "coords" enabled in constructor,
//     // available as "gl_TexCoord" in the vertex shader
//     mesh.setCoords([[0, 0], [1, 0], [0, 1], [1, 1]]);
// 
//     // Custom attribute "weights", available as "weight" in the
//     // vertex shader
//     mesh.addVertexBuffer('weights', 'weight');
//     mesh.setBuffer('weights',[1, 0, 0, 1]);
// 
//     // Default index buffer "triangles"
//     mesh.setTriangles([[0, 1, 2], [2, 1, 3]]);
// 
//     // Optional index buffer "lines" enabled in constructor
//     mesh.setLines([[0, 1], [0, 2], [1, 3], [2, 3]]);
// 
//     // Upload provided data to GPU memory
//     mesh.compile();

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

// Represents a collection of vertex buffers and index buffers. Each vertex
// buffer maps to one attribute in GLSL and has a corresponding property set
// on the Mesh instance. There is one vertex buffer by default: `vertices`,
// which maps to `gl_Vertex`. The `coords`, `normals`, and `colors` vertex
// buffers map to `gl_TexCoord`, `gl_Normal`, and `gl_Color` respectively,
// and can be enabled by setting the corresponding options to true. There are
// two index buffers, `triangles` and `lines`, which are used for rendering
// `mGL.TRIANGLES` and `mGL.LINES`, respectively. Only `triangles` is enabled by
// default, although `computeWireframe()` will add a normal buffer if it wasn't
// initially enabled.

/**
 * @param mGL Element where draw
 * @returns A WebGL mesh
 */
EJSS_WEBGLGRAPHICS.mesh = function(mGL, id, options) {
  var self = {};
  options = options || {coords:true, normals:true, triangles:true, lines:true, colors:true};
  id = id || "mesh";  
  var vertexBuffers = {};
  var indexBuffers = {};
  
  self.getId = function() {
  	return id;
  }
  
  self.getVertexBuffers = function() {
  	return vertexBuffers;
  }

  self.getIndexBuffers = function() {
  	return indexBuffers;
  }
  
  self.setVertices = function(b) {
  	self.vertices = b;
  }

  self.getVertices = function() {
  	return self.vertices;
  }

  self.setCoords = function(b) {
  	self.coords = b;
  }

  self.getCoords = function() {
  	return self.coords;
  }

  self.setNormals = function(b) {
  	self.normals = b;
  }

  self.getNormals = function() {
  	return self.normals;
  }

  self.setColors = function(b) {
  	self.colors = b;
  }

  self.getColors = function() {
  	return self.colors;
  }

  self.setTriangles = function(b) {
  	self.triangles = b;
  }

  self.getTriangles = function() {
  	return self.triangles;
  }

  self.setLines = function(b) {
  	self.lines = b;
  }

  self.getLines = function() {
  	return self.lines;
  }
  
  self.setBuffer = function(name, b) {
  	self[name] = b;
  }
  
  // Add a new vertex buffer with a list as a property called `name` on this object
  // and map it to the attribute called `attribute` in all shaders that draw this mesh.
  self.addVertexBuffer = function(name, attribute) {
    var buffer = vertexBuffers[attribute] = EJSS_WEBGLGRAPHICS.buffer(mGL, mGL.ARRAY_BUFFER, Float32Array);
    buffer.setName(name);
    self[name] = [];
  }

  // Add a new index buffer with a list as a property called `name` on this object.
  self.addIndexBuffer = function(name) {
    var buffer = indexBuffers[name] = EJSS_WEBGLGRAPHICS.buffer(mGL, mGL.ELEMENT_ARRAY_BUFFER, Uint16Array);
    self[name] = [];
  }

  // Upload all attached buffers to the GPU in preparation for rendering. This
  // doesn't need to be called every frame, only needs to be done when the data
  // changes.
  self.compile = function() {
    for (var attribute in vertexBuffers) {
      var buffer = vertexBuffers[attribute];
      buffer.setData(self[buffer.getName()]);
      buffer.compile();
    }

    for (var name in indexBuffers) {
      var buffer = indexBuffers[name];
      buffer.setData(self[name]);
      buffer.compile();
    }
  }

  // Transform all vertices by `matrix` and all normals by the inverse transpose
  // of `matrix`.
  self.transform = function(matrix) {
    self.vertices = self.vertices.map(function(v) {
      return matrix.transformPoint(Vector.fromArray(v)).toArray();
    });
    if (self.normals) {
      var invTrans = matrix.inverse().transpose();
      self.normals = self.normals.map(function(n) {
        return invTrans.transformVector(Vector.fromArray(n)).unit().toArray();
      });
    }
    self.compile();
    return self;
  }

  // Computes a new normal for each vertex from the average normal of the
  // neighboring triangles. This means adjacent triangles must share vertices
  // for the resulting normals to be smooth.
  self.computeNormals = function() {
    if (!self.normals) self.addVertexBuffer('normals', 'gl_Normal');
    for (var i = 0; i < self.vertices.length; i++) {
      self.normals[i] = new Vector();
    }
    for (var i = 0; i < self.triangles.length; i++) {
      var t = self.triangles[i];
      var a = Vector.fromArray(self.vertices[t[0]]);
      var b = Vector.fromArray(self.vertices[t[1]]);
      var c = Vector.fromArray(self.vertices[t[2]]);
      var normal = b.subtract(a).cross(c.subtract(a)).unit();
      self.normals[t[0]] = self.normals[t[0]].add(normal);
      self.normals[t[1]] = self.normals[t[1]].add(normal);
      self.normals[t[2]] = self.normals[t[2]].add(normal);
    }
    for (var i = 0; i < self.vertices.length; i++) {
      self.normals[i] = self.normals[i].unit().toArray();
    }
    self.compile();
    return self;
  }

  // Populate the `lines` index buffer from the `triangles` index buffer.
  self.computeWireframe = function() {
    var indexer = EJSS_WEBGLGRAPHICS.indexer();
    for (var i = 0; i < self.triangles.length; i++) {
      var t = self.triangles[i];
      for (var j = 0; j < t.length; j++) {
        var a = t[j], b = t[(j + 1) % t.length];
        indexer.add([Math.min(a, b), Math.max(a, b)]);
      }
    }
    if (!self.lines) self.addIndexBuffer('lines');
    self.lines = indexer.getUnique();
    self.compile();
    return self;
  }

  // Computes the axis-aligned bounding box, which is an object whose `min` and
  // `max` properties contain the minimum and maximum coordinates of all vertices.
  self.getAABB = function() {
    var aabb = { min: new Vector(Number.MAX_VALUE, Number.MAX_VALUE, Number.MAX_VALUE) };
    aabb.max = aabb.min.negative();
    for (var i = 0; i < self.vertices.length; i++) {
      var v = Vector.fromArray(self.vertices[i]);
      aabb.min = Vector.min(aabb.min, v);
      aabb.max = Vector.max(aabb.max, v);
    }
    return aabb;
  }

  // Computes a sphere that contains all vertices (not necessarily the smallest
  // sphere). The returned object has two properties, `center` and `radius`.
  self.getBoundingSphere = function() {
    var aabb = self.getAABB();
    var sphere = { center: aabb.min.add(aabb.max).divide(2), radius: 0 };
    for (var i = 0; i < self.vertices.length; i++) {
      sphere.radius = Math.max(sphere.radius,
        Vector.fromArray(self.vertices[i]).subtract(sphere.center).length());
    }
    return sphere;
  }

  self.addVertexBuffer('vertices', 'gl_Vertex');
  if (options.coords) self.addVertexBuffer('coords', 'gl_TexCoord');
  if (options.normals) self.addVertexBuffer('normals', 'gl_Normal');
  if (options.colors) self.addVertexBuffer('colors', 'gl_Color');
  if (!('triangles' in options) || options.triangles) self.addIndexBuffer('triangles');
  if (options.lines) {
  		self.addIndexBuffer('lines');
  }

  return self;
}	

// 
// Generates indices into a list of unique objects from a stream of objects
// that may contain duplicates. This is useful for generating compact indexed
// meshes from unindexed data.
EJSS_WEBGLGRAPHICS.indexer = function() {
  var self = {};
  var unique = [];
  // var indices = [];
  var map = {};

  self.getUnique = function() {
  	return unique;
  }

  // 
  // Adds the object `obj` to `unique` if it hasn't already been added. Returns
  // the index of `obj` in `unique`.
  self.add = function(obj) {
    var key = JSON.stringify(obj);
    if (!(key in map)) {
      map[key] = unique.length;
      unique.push(obj);
    }
    return map[key];
  }
  
  return self;
};

// 
// Provides a simple method of uploading data to a GPU buffer. Example usage:
// 
//     var vertices = EJSS_WEBGLGRAPHICS.buffer(mGL, mGL.ARRAY_BUFFER, Float32Array);
//     var indices = EJSS_WEBGLGRAPHICS.buffer(mGL, mGL.ELEMENT_ARRAY_BUFFER, Uint16Array);
//     vertices.setData([[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0]]);
//     indices.setData([[0, 1, 2], [2, 1, 3]]);
//     vertices.compile();
//     indices.compile();
// 
EJSS_WEBGLGRAPHICS.buffer = function(mGL, target, type) {
  var self = {};
  var buffer = null;
  var target = target;
  var type = type;
  var mName = "";
  var mData = [];

  self.setData = function(b) {
  	mData = b;
  }

  self.setName = function(name) {
  	mName = name;
  }

  self.getBuffer = function() {
  	return buffer;
  }

  self.getName = function() {
  	return mName;
  }

  // 
  // Upload the contents of `data` to the GPU in preparation for rendering. The
  // data must be a list of lists where each inner list has the same length. For
  // example, each element of data for vertex normals would be a list of length three.
  // This will remember the data length and element length for later use by shaders.
  // The type can be either `mGL.STATIC_DRAW` or `mGL.DYNAMIC_DRAW`, and defaults to
  // `mGL.STATIC_DRAW`.
  // 
  // This could have used `[].concat.apply([], this.data)` to flatten
  // the array but Google Chrome has a maximum number of arguments so the
  // concatenations are chunked to avoid that limit.
  self.compile = function(tp) {
    var data = [];
    for (var i = 0, chunk = 10000; i < mData.length; i += chunk) {
      data = Array.prototype.concat.apply(data, mData.slice(i, i + chunk));
    }
    var spacing = mData.length ? data.length / mData.length : 0;
    if (spacing != Math.round(spacing)) throw 'buffer elements not of consistent size, average size is ' + spacing;
    buffer = buffer || mGL.createBuffer();
    buffer.length = data.length;
    buffer.spacing = spacing;
    mGL.bindBuffer(target, buffer);
    mGL.bufferData(target, new type(data), tp || mGL.STATIC_DRAW);
  }
  
  return self;
}
