/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL segment
 */
EJSS_WEBGLGRAPHICS.segment = function(mGL, mElement) {
  	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName(), 
	  	{coords:false, normals:true, triangles:true, lines:false, colors:false});

	  var resolutionU = mElement.getResolutionU();
	  var radius = mElement.getLineWidth() / 2;
      
	  // generate vertex for body
	  var vertices3D = [];	  
	  for (var x = 0; x < resolutionU; x++ ) {
		var u = x / (resolutionU - 1);
		var xpos = radius * Math.sin( u * Math.PI * 2 );
		var ypos = radius * Math.cos( u * Math.PI * 2 );

		vertices3D.push(new Vector([xpos, ypos, 0])); // top vertex
		vertices3D.push(new Vector([xpos, ypos, 0])); // bottom vertex
	  }
	  
	  // rotate and translate top and bottom
  	  var endpoint = new Vector(mElement.getSize());
	  var cross = endpoint.cross(new Vector(0,0,1));
	  if(cross.length()==0) cross.y = 1; // if endoint in axis Z, get axis Y to rotate 
	  // console.log("cross:" + cross.x + " " + cross.y + " " + cross.z + " ");	  
	  var angle = endpoint.angle(new Vector(0,0,1));
	  // console.log("angle:" + angle);
	  var result = new Matrix();
	  Matrix.rotate(-angle,cross.x,cross.y,cross.z,0,0,0,result);
	  for(var i=0; i<vertices3D.length; i++) {
	  	var vector = result.transformPoint(vertices3D[i]);
	  	if (i % 2 == 0) {
	  		// add enpoint to translate	  	
	  		mesh.vertices.push(vector.add(endpoint).toArray());	  		
	  	} else {
	  		mesh.vertices.push(vector.toArray());
	  	}
	  }
	  
	  // create surface triangles
	  for (var x = 0; x < resolutionU - 1; x++ ) {
	    var first = 2*x;		    
	    var fourth = 2*x + 2;
	    var second = 2*x + 1;
	    var third = 2*x + 3;
			
        mesh.triangles.push([third, second, first]);      
        mesh.triangles.push([fourth, third, first]);
	  }		
		
	  // top cap
	  mesh.vertices.push(endpoint.toArray());		
      for (var i = 0; i < resolutionU - 1; i++) {
      	var first = mesh.vertices.length - 1;
      	var second = 2*i+2;
      	var third = 2*i;
      	mesh.triangles.push([first, second, third]);
      }        

	  // bottom cap
 	  mesh.vertices.push([0, 0, 0]);
      for (var i = 0; i < resolutionU - 1; i++) {
      	var first = 2*i+1;
      	var second = 2*i+3;
      	var third = mesh.vertices.length - 1;
       	mesh.triangles.push([first, second, third]);
      }        
	  	        
	  mesh.computeNormals();	
	  
	  mesh.compile();	
	
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
        
     return mesh;

/* Previously it was possible to manage lineWidth and then the segment was easy ...
 	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName(), 
	  	{coords:false, normals:false, triangles:false, lines:true, colors:false});
	  
      mesh.vertices.push([0,0,0]);
      mesh.vertices.push([2,2,2]);

	  mesh.lines.push([0,1]);
	  
	  mesh.compile();
           
      return mesh;
*/      
}

