/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL cylinder
 */
EJSS_WEBGLGRAPHICS.cylinder = function(mGL, mElement) {
  	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName());
  	  	
	  var resolutionV = mElement.getResolutionV();
	  var resolutionU = mElement.getResolutionU() + 1;
	  
	  var radiusTop = mElement.getTopRadius();
	  var radiusBottom = mElement.getBottomRadius();      
	  var mMinAngleU = mElement.getMinAngleU();
	  var mMaxAngleU = mElement.getMaxAngleU();
      
	  // generate vertex for body
      var maxNumber = 0;
      var minNumber = 0;
	  for (var z = 0; z < resolutionV; z ++ ) {	
		var v = z / (resolutionV - 1);
		var radius = v * ( radiusBottom - radiusTop ) + radiusTop;

	  	minNumber = Math.floor(mMinAngleU * resolutionU / 360); 
	  	maxNumber = Math.floor(mMaxAngleU * resolutionU / 360);		  	
		for (var x = minNumber; x < maxNumber; x ++ ) {
			var u = x / (resolutionU - 1);

			var xpos = radius * Math.sin( u * Math.PI * 2 );
			var ypos = radius * Math.cos( u * Math.PI * 2 );
			var zpos = - v * 2 + 1;

			mesh.vertices.push([xpos, ypos, zpos]);
	        mesh.coords.push([u,v]);      			
		}
	  }
	  
	  // build central axis
	  for (var z = 0; z < resolutionV; z ++ ) {	
	      var zpos = - 1 + (z * (2 / (resolutionV - 1))); // proportional in axis
	      	      
		  mesh.vertices.push([0, 0, zpos]);
		  mesh.coords.push([1, z / (resolutionV - 1)]);		
  	  }	  
	  
	  // create surface triangles
	  var sizeU = (maxNumber - minNumber);
      for (var z = 0; z < resolutionV - 1; z++) {			
		  for (var x = 0; x < sizeU - 1; x++ ) {
		    var first = (z * sizeU) + x;		    
		    var second = ((z + 1) * sizeU) + x;
		    var third = ((z + 1) * sizeU) + x + 1;
		    var fourth = (z * sizeU) + x + 1;
			
	        mesh.triangles.push([third, second, first]);
	        mesh.triangles.push([fourth, third, first]);      
	        
		    mesh.lines.push([third, second]);
		    mesh.lines.push([fourth, third]);
		    mesh.lines.push([first, fourth]);
		  }
		
		  // last close sphera
	      if((mMinAngleU != 0) || (mMaxAngleU != 360)) { // quesito 		      		  			  	
		    
			// close left and right
	        var firstAxis = mesh.vertices.length - (z + 1); // points central axis
			if(mElement.getStyle().getClosedLeft()) {
		    	mesh.triangles.push([third, fourth, firstAxis - 1]);
		    	mesh.triangles.push([fourth, firstAxis, firstAxis - 1]);

		    	mesh.lines.push([third, firstAxis - 1]);
		    	mesh.lines.push([fourth, firstAxis]);
		    	mesh.lines.push([firstAxis, firstAxis - 1]);
		    }
		    if(mElement.getStyle().getClosedRight()) {
				// points first latitude
		    	var firstMin = z * sizeU;
				var secondMin = (z + 1) * sizeU;
		    	mesh.triangles.push([firstAxis - 1, firstMin, secondMin]);
		    	mesh.triangles.push([firstAxis - 1, firstAxis, firstMin]);

		    	mesh.lines.push([secondMin, firstMin]);
		    	mesh.lines.push([secondMin, firstAxis - 1]);
		    	mesh.lines.push([firstMin, firstAxis]);
		    	mesh.lines.push([firstAxis, firstAxis - 1]);
		    }			    
		  }			      			
	  }
		
	  // top cap
  	  var isClosedTop = mElement.getStyle().getClosedTop();
	  if ( isClosedTop && radiusTop > 0 ) {
		mesh.vertices.push([0, 0, 1]);		
        for (var i = 0; i < sizeU - 1; i++) {
           	mesh.triangles.push([mesh.vertices.length - 1, i+1, i]);
	        mesh.coords.push([1, (i / (sizeU - 2))]);
	               	
		    mesh.lines.push([mesh.vertices.length - 1, i]);
		    mesh.lines.push([i, i+1]);	               	
        }        
	  }

	  // bottom cap
  	  var isClosedBottom = mElement.getStyle().getClosedBottom();
	  if ( isClosedBottom && radiusBottom > 0 ) {
		mesh.vertices.push([0, 0, -1]);
        for (var i = (resolutionV-1)*sizeU; i < sizeU*resolutionV; i++) {
           	mesh.triangles.push([i, i+1, mesh.vertices.length - 1]);
           	mesh.coords.push([1, (i / (sizeU - 2))]);

		    mesh.lines.push([mesh.vertices.length - 1, i]);
		    mesh.lines.push([i, i+1]);	               	
        }        
	  }			  
	  
	  // compute normals
	  if((mMinAngleU == 0) && (mMaxAngleU == 360)) 
	  	mesh.normals = mesh.vertices;
	  else
	    mesh.computeNormals();	  
	        
	  mesh.compile();	
	
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
        
     return mesh;
}

