/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL custom
 */

EJSS_WEBGLGRAPHICS.surface = function(mGL, mElement) {   
    
    createSurfaceBuffers = function(xpoints, ypoints, data3D, mesh){
        for (var i = 0; i < xpoints - 1; i++) {
            for (var j = 0; j < ypoints - 1; j++) {
                // Create surface vertices.
                var rawP1 = data3D[j + (i * ypoints)];
                var rawP2 = data3D[j + (i * ypoints) + ypoints];
                var rawP3 = data3D[j + (i * ypoints) + ypoints + 1];
                var rawP4 = data3D[j + (i * ypoints) + 1];
                
                mesh.vertices.push(rawP1);
                mesh.vertices.push(rawP2);
                mesh.vertices.push(rawP3);
                mesh.vertices.push(rawP4);                                
            }
        }
                
        var numQuads = ((xpoints - 1) * (ypoints - 1)) / 2;        
        for (var i = 0; i < (numQuads * 8); i += 4) {
            mesh.triangles.push([i, i+1, i+2]);
            mesh.triangles.push([i, i+2, i+3]);
            
            mesh.lines.push([i,i+1]);             
            mesh.lines.push([i,i+3]);             
            mesh.lines.push([i+2,i+3]);             
            mesh.lines.push([i+2,i+1]);             
            
            mesh.coords.push([0,0]);
            mesh.coords.push([0,1]);
            mesh.coords.push([1,0]);
            mesh.coords.push([1,1]);
        }        
    };

	  var data = mElement.getData();
	  if(data || data.length > 0) {
		  var numRows = data.length;
		  var numCols = data[0].length;

		  // prepare points
	      var data3ds = new Array();
	      var index = 0;
	      for (var i = 0; i < numRows; i++) {
	        for (var j = 0; j < numCols; j++) {
	            data3ds[index] = data[i][j];
	            index++; 
	        }
	      }
		  
		  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName());
		  
		  createSurfaceBuffers(numRows, numCols, data3ds, mesh);
	
	  	  // compute normals
	  	  mesh.computeNormals();
		   		
		  mesh.compile();
		  	  	  
		  mGL.addElement(mElement.getName() + ".mesh", mesh);

	  }	  	  
    
      return mesh;
}

