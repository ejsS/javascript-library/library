/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL custom
 */

EJSS_WEBGLGRAPHICS.analyticSurface = function(mGL, mElement) {   
    
    createSurfaceBuffers = function(xpoints, ypoints, data3D, mesh){
        for (var i = 0; i < xpoints - 1; i++) {
            for (var j = 0; j < ypoints - 1; j++) {
                // Create surface vertices.
                var rawP1 = data3D[j + (i * ypoints)];
                var rawP2 = data3D[j + (i * ypoints) + ypoints];
                var rawP3 = data3D[j + (i * ypoints) + ypoints + 1];
                var rawP4 = data3D[j + (i * ypoints) + 1];
                
                mesh.vertices.push(rawP1);
                mesh.vertices.push(rawP2);
                mesh.vertices.push(rawP3);
                mesh.vertices.push(rawP4);                
            }
        }
                
        var numQuads = ((xpoints - 1) * (ypoints - 1)) / 2;        
        for (var i = 0; i < (numQuads * 8); i += 4) {
            mesh.triangles.push([i, i+1, i+2]);
            mesh.triangles.push([i, i+2, i+3]);

            mesh.lines.push([i,i+1]);             
            mesh.lines.push([i,i+3]);             
            mesh.lines.push([i+2,i+3]);             
            mesh.lines.push([i+2,i+1]);             			

            mesh.coords.push([0,0]);
            mesh.coords.push([0,1]);
            mesh.coords.push([1,0]);
            mesh.coords.push([1,1]);            
        }        
    };
  
  evaluate = function(expression, v1, p1, v2, p2) {
    var vblevalue = {};
    vblevalue[v1] = p1;
	vblevalue[v2] = p2;
    return expression.evaluate(vblevalue);
  };

	  var numRows = mElement.getNumPoints1();
	  var minRows = mElement.getMinValue1();
	  var maxRows = mElement.getMaxValue1();
	  var numCols = mElement.getNumPoints2();
	  var minCols = mElement.getMinValue2();
	  var maxCols = mElement.getMaxValue2();	 
	  var d1 = Math.abs((maxRows - minRows) / (numRows - 1));
	  var d2 = Math.abs((maxCols - minCols) / (numCols - 1));
	  
	  var exprX = mElement.getExpressionX();		
	  var exprY = mElement.getExpressionY();
	  var exprZ = mElement.getExpressionZ();
	  
	  var var1 = mElement.getVariable1();
	  var var2 = mElement.getVariable2();
	  
	  var data3ds = new Array();
	  var index = 0;
	  var point1 = minRows;	  					 
	  for (var i = 0; i < numRows; i++) {
		var point2 = minCols;								
		for (var j = 0; j < numCols; j++) {
			// expresion X
			var x = evaluate(exprX, var1, point1, var2, point2);
			// expresion Y
			var y = evaluate(exprY, var1, point1, var2, point2); 
			// expresion Z		 
			var z = evaluate(exprZ, var1, point1, var2, point2);
			data3ds[index] = [x,y,z];
			index++;			
			point2 = point2 + d2;
		}				
		point1 = point1 + d1;
	  }
	  	  	  
	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName());
	  
	  createSurfaceBuffers(numRows, numCols, data3ds, mesh);

	  // compute normals
	  mesh.computeNormals();
	  
	  mesh.compile();
	  	  	  
	  mGL.addElement(mElement.getName() + ".mesh", mesh);

      return mesh;
}

