/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL text
 */
EJSS_WEBGLGRAPHICS.text = function(mGL, mElement) {
	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL, mElement.getName());

	  var font = mElement.getFont();
	  var backGroundColor = mElement.getBackground();
	  var text = mElement.getText();
	  var color = font.getFillColor();	  
	  
	  // build canvas with text
	  var canvas = document.createElement("canvas");		 
	  var context = canvas.getContext("2d");
	  context = canvas.getContext("2d");

	  // fit height
	  var height = font.getFontSize();
	  if(mElement.getFitText())
	  	height = Math.max(canvas.height,height);

	  // font 
 	  var fondtxt = "";
 	  fondtxt += (font.getFontStyle() != 'none')? font.getFontStyle() + ' ':'';
 	  fondtxt += (font.getFontWeight() != 'none')? font.getFontWeight() + ' ':'';
 	  fondtxt += height + 'px ';
 	  fondtxt += font.getFontFamily();
	  context.font = fondtxt;
		 
	  if(backGroundColor) {
		context.fillStyle = backGroundColor;
		context.fillRect(0, 0, canvas.width, canvas.height);
	  }
		 
	  context.textAlign = "center";
	  context.textBaseline = "middle";
	  context.fillStyle = color;
	  context.fillText(text, canvas.width / 2, canvas.height / 2, canvas.width);
		 
      // add texture 
	  var texture = EJSS_WEBGLGRAPHICS.Texture.fromImage(mGL,canvas);
	  mGL.addElement(mElement.getName() + ".texture", texture);
  		  
      // create mesh
	  var detail = 1;     // tesselation level 	
	  for (var y = 0; y <= detail; y++) {
	    var t = y / detail;
	    for (var x = 0; x <= detail; x++) {
	      var s = x / detail;
	      mesh.vertices.push([0 , 2 * t - 1, 2 * s - 1]);
	      mesh.coords.push([t, s]);	      
	      if (x < detail && y < detail) {
	        var i = x + y * (detail + 1);
	        mesh.triangles.push([i, i + 1, i + detail + 1]);
	        mesh.triangles.push([i + detail + 1, i + 1, i + detail + 2]);
	      }
	    }
	  }
	  
	  // compute normals	  	
	  mesh.computeNormals();
	  
	  // compute wire
	  mesh.computeWireframe();
	  	  
	  mesh.compile();
				 	  		  	
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
        
      return mesh;
}

