/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};
EJSS_WEBGLGRAPHICS.Utils = {};

EJSS_WEBGLGRAPHICS.Utils.regexMap = function(regex, text, callback) {
  while ((result = regex.exec(text)) != null) {
    callback(result);
  }
}

EJSS_WEBGLGRAPHICS.Utils.isArray = function(obj) {
  var str = Object.prototype.toString.call(obj);
  return str == '[object Array]' || str == '[object Float32Array]';
}

EJSS_WEBGLGRAPHICS.Utils.isNumber = function(obj) {
  var str = Object.prototype.toString.call(obj);
  return str == '[object Number]' || str == '[object Boolean]';
}
