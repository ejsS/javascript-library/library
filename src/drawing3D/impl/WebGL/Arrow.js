/**
 * Deployment for 3D SVG drawing.
 * @module WebGLGraphics 
 */

var EJSS_WEBGLGRAPHICS = EJSS_WEBGLGRAPHICS || {};

/**
 * @param mGL Element where draw
 * @param mElement Element to draw
 * @returns A WebGL arrow
 */
EJSS_WEBGLGRAPHICS.arrow = function(mGL, mElement) {
  	  var mesh = EJSS_WEBGLGRAPHICS.mesh(mGL,mElement.getName(), 
	  	{coords:false, normals:true, triangles:true, lines:false, colors:false});
  	  	
  	  // end point
  	  var endpoint = new Vector(mElement.getSize());
  	  var lenendpoint = endpoint.length();
  	  
  	  // resolution used to arrow head and segment
	  var resolutionU = mElement.getResolutionU();
	  var headWidth = mElement.getHeadWidth();
	  var headHeight = mElement.getHeadHeight();
	  
	  // generate vertex for head (head point at (0,0,0))
	  var hvertices3D = [new Vector(0, 0, 0)];
	  var hradius = (lenendpoint / headWidth); 	  
	  for (var x = 0; x < resolutionU; x++ ) {
		var u = x / (resolutionU - 1);

		var xpos = hradius * Math.sin( u * Math.PI * 2 );
		var ypos = hradius * Math.cos( u * Math.PI * 2 );
		var zpos = (- lenendpoint / headHeight);   

		hvertices3D.push(new Vector(xpos, ypos, zpos));
	  }

	  // generate vertex for segment
	  var svertices3D = [];
	  var sradius = mElement.getLineWidth() / 2;
	  for (var x = 0; x < resolutionU; x++ ) {
		var u = x / (resolutionU - 1);
		var xpos = sradius * Math.sin( u * Math.PI * 2 );
		var ypos = sradius * Math.cos( u * Math.PI * 2 );
		var zpos = (- lenendpoint / headHeight);   

		svertices3D.push(new Vector([xpos, ypos, zpos])); // top vertex
		svertices3D.push(new Vector([xpos, ypos, 0])); // bottom vertex
	  }
	  	  
	  // rotate and translate head
	  var cross = endpoint.cross(new Vector(0,0,1));
	  if(cross.length()==0) cross.y = 1; // if endoint in axis Z, get axis Y to rotate 
	  // console.log("cross:" + cross.x + " " + cross.y + " " + cross.z + " ");	  
	  var angle = endpoint.angle(new Vector(0,0,1));
	  // console.log("angle:" + angle);
	  var result = new Matrix();
	  Matrix.rotate(-angle,cross.x,cross.y,cross.z,0,0,0,result);
	  for(var i=0; i<hvertices3D.length; i++) { // head vertex
	  	var vector = result.transformPoint(hvertices3D[i]);
	  	mesh.vertices.push(vector.add(endpoint).toArray());	  		
	  }
	  for(var i=0; i<svertices3D.length; i++) { // segment vertex
	  	var vector = result.transformPoint(svertices3D[i]);
	  	if (i % 2 == 0) {
	  		// add enpoint to translate	  	
	  		mesh.vertices.push(vector.add(endpoint).toArray());	  		
	  	} else {
	  		mesh.vertices.push(vector.toArray());
	  	}	  		  		
	  }
	  	  
	  // create surface triangles for head
	  for (var x = 1; x < resolutionU; x++ ) {
	    var first = x;		    
	    var second = x + 1;		
        mesh.triangles.push([0, first, second]);
	  }		
			  
	  // create surface triangles for segment
	  for (var x = 0; x < resolutionU - 1; x++ ) {
	    var first = 2*x + hvertices3D.length;		    
	    var fourth = 2*x + 2 + hvertices3D.length;
	    var second = 2*x + 1 + hvertices3D.length;
	    var third = 2*x + 3 + hvertices3D.length;
			
        mesh.triangles.push([third, second, first]);      
        mesh.triangles.push([fourth, third, first]);
	  }		
			  
	  // bottom cap
 	  mesh.vertices.push([0, 0, 0]);
      for (var i = 0; i < resolutionU - 1; i++) {
      	var first = 2*i+1 + hvertices3D.length;;
      	var second = 2*i+3 + hvertices3D.length;;
      	var third = mesh.vertices.length - 1;
       	mesh.triangles.push([first, second, third]);
      }        
			  
      mesh.computeNormals();	  
	        
	  mesh.compile();	
	
	  mGL.addElement(mElement.getName() + ".mesh", mesh);
          
      return mesh;
}

