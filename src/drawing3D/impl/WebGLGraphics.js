/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Deployment for 3D drawing.
 * @module 3Dgraphics 
 */

var EJSS_GRAPHICS = EJSS_GRAPHICS || {};

/**
 * WebGLGraphics class
 * @class WebGLGraphics 
 * @constructor  
 */
EJSS_GRAPHICS.WebGLGraphics = {
    
  /**
   * Return document box
   * @return box  
   */
  getOffsetRect: function(graphics) {
  	return EJSS_GRAPHICS.GraphicsUtils.getOffsetRect(graphics);
  }
};

/**
 * Constructor for Element
 * @param mName Identifier Canvas element in HTML document
 * @returns A Canvas graphics
 */  
EJSS_GRAPHICS.webGLGraphics = function(mName) {
  var self = EJSS_INTERFACE.webGLCanvas(mName);	// reference returned     
  var canvas = self.getDOMElement();

  // get context
  var mGL = self.getContext();
  
  // blending vars
  var mDepths = {};
  var mmBlendingElements = [];

  var mTransformationOrder = 0; // 0: first trans/scale and then transformation, 1: first transformation and then trans/scale 

  self.supportsWebGL = function() {
    return mGL;
  };
  
  /**
   * Return canvas for event handle
   * @return Canvas element 
   */
  self.getEventContext = function() {  	
    return canvas;
  };
  
  /**
   * Return canvas document box
   * @return width integer 
   */
  self.getBox = function() {
  	var box = EJSS_GRAPHICS.WebGLGraphics.getOffsetRect(self)
  	canvas.width = box.width;
  	canvas.height = box.height; 
  	box.width -= 1;
  	box.height -= 1;
  	return box;
  };
    
  /**
   * Return aspect ratio of canvas 
   * @return double 
   */
  self.getAspect = function() {
  	var box = EJSS_GRAPHICS.WebGLGraphics.getOffsetRect(self)
	var height = Math.max(1, box.height);  // prevent divide by 0

  	return box.width / height;
  }

  /**
   * Set transformation order 
   * @return order 
   */
  self.setTransformationOrder = function(order) {
  	mTransformationOrder = order;
  }
    
  /**
   * Get transformation order 
   * @return order 
   */
  self.getTransformationOrder = function() {
  	return mTransformationOrder;
  }
    
  /**
   * Remove a shape
   * @param name
   */
  self.remove = function(name) {	
    
  };

  /**
   * Reset the canvas
   */
  self.reset = function(ort,dim,fov,near,far) {	  
  	if(!mGL) return;

    addMatrixStack();

	// viewport
    var box = self.getBox();
	var height = Math.max(1, box.height);  // prevent divide by 0
	mGL.viewport(0, 0, box.width, height);

	// projection matrix
    mGL.matrixMode(mGL.PROJECTION);
    mGL.loadIdentity();
	if(!ort) 
		mGL.perspective(fov, box.width / height, near, far);
	else {
		mGL.ortho(dim.left, dim.right, dim.bottom, dim.top, -near, far);								
	}
	
	// view matrix
    mGL.matrixMode(mGL.VIEW);     
    
    // rendering of both sides of each face (front and back)
  	mGL.disable(mGL.CULL_FACE);  
	mGL.enable(mGL.POLYGON_OFFSET_FILL);
	// mGL.polygonOffset(0.5, 0.5);
	
    // without blending for default	
	mGL.enable(mGL.DEPTH_TEST);
	mGL.depthFunc(mGL.LESS);
	mGL.depthMask(true);

    mGL.clear(mGL.COLOR_BUFFER_BIT | mGL.DEPTH_BUFFER_BIT);	
  };

  self.putCamera = function(ort,camera) {
  	if(!mGL) return;
  	
    mGL.clear(mGL.COLOR_BUFFER_BIT | mGL.DEPTH_BUFFER_BIT);

	// ort size
	if(ort) {
    	mGL.matrixMode(mGL.PROJECTION);
    	if (camera.delta != 0) {
			mGL.scale(camera.delta, camera.delta, camera.delta);		    		
	    	camera.delta = 0;    		
    	}
	}
	    	
  	// camera position
    mGL.matrixMode(mGL.VIEW);     	
    mGL.loadIdentity();
    mGL.lookAt(camera.location.x, camera.location.y, camera.location.z,
    			camera.focus.x, camera.focus.y, camera.focus.z,
    			camera.upvector.x, camera.upvector.y, camera.upvector.z); // up Vector
    mGL.rotate(EJSS_TOOLS.Mathematics.radians(camera.tilt), 1, 0, 0, 
    	camera.focus.x, camera.focus.y, camera.focus.z);    
    mGL.rotate(EJSS_TOOLS.Mathematics.radians(camera.altitude), 0, 1, 0, 
    	camera.focus.x, camera.focus.y, camera.focus.z);    
    mGL.rotate(EJSS_TOOLS.Mathematics.radians(camera.azimuth), 0, 0, 1, 
    	camera.focus.x, camera.focus.y, camera.focus.z);    
  }
	 
  // Ordered list of blending elements
  function addToAlphaList(element) {
	// element depth
	var v = new Vector(element.getPosition());
	var pv = mGL.viewMatrix.transformVector(v);
	var depth = pv.z;

	mDepths[element.getName()] = depth;
		
	// Binary search
	var less, more, itteration = 1, inserted = false, index = Math.floor(mBlendingElements.length/2);
	while(!inserted) {
		less = (index === 0 || mDepths[mBlendingElements[index-1].getName()] <= depth);
		more = (index >= mBlendingElements.length || mDepths[mBlendingElements[index].getName()] >= depth);
		if(less && more) {
			mBlendingElements.splice(index, 0, element);
			inserted = true;
		} else {
			itteration++;
			var step = Math.ceil(mBlendingElements.length/Math.pow(2,itteration));
			if(!less) {
				index = Math.max(0, index-step);
			} else {
				index = Math.min(mBlendingElements.length, index+step);
			}
		}
	}
  };

  /**
   * Draw Elements
   * @param elements array of Elements and Element Sets
   * @param force whether force draw elements
   */
  self.draw = function(elements,force) {  	  
  	if(!mGL) return;  		
  	
  	// globals (they could be local vars)
  	mDepths = {};
  	mBlendingElements = [];
  	
	var noBlendingElements = [];  		
  	var allElements = elements.slice();  	

	while (allElements.length) {
      var element = allElements.shift();
      if(element.getElements) {	// whether element set
      	var lasts = element.getLastElements();
  		for(var j=0, m=lasts.length; j<m; j++) { // remove last removed elements
			mGL.removeElement(lasts[j].getName() + ".mesh");
  		}    
  		// update array of elements used in iteration
  		allElements = element.getElements().concat(allElements);
      } else { // else one element
  		// it is neccesary to order elements to blending (http://delphic.me.uk/webglalpha.html)       	
		if(element.getStyle().getTransparency() > 0.0) {
			addToAlphaList(element);
		} else {
			noBlendingElements.push(element);
		}
      }
    }    
   
    // drawing elements without blending
	for(i = 0, l = noBlendingElements.length; i < l; i++) {
		var element = noBlendingElements[i];
		self.drawElement(element,force);
	}    
   
    // drawing elements with blending
	mGL.depthMask(false);
    mGL.enable(mGL.BLEND);
    mGL.blendFunc(mGL.SRC_ALPHA, mGL.ONE_MINUS_SRC_ALPHA);

    // draw ordered elements by depth
	for(i = 0, l = mBlendingElements.length; i < l; i++) {
		var element = mBlendingElements[i];
		self.drawElement(element,force);
		//console.log("Drawing element " + element.getName() + " - " + mDepths[element.getName()]);
	}    
	
	// disable blending
	mGL.disable(mGL.BLEND);
	mGL.depthMask(true);   
  };

  function applyTranformation(gtr) {
	  if (gtr && gtr.length > 0) {
			if (EJSS_WEBGLGRAPHICS.Utils.isArray(gtr[0])) { // array of rotations
				for (var i=0; i<gtr.length; i++) {
					if (gtr[i].length == 7) { 
						// rotation 
						mGL.rotate(gtr[i][0],gtr[i][1],gtr[i][2],gtr[i][3],
							gtr[i][4],gtr[i][5],gtr[i][6]); // angle,x,y,z,cx,cy,cz								
					} else {
						// transformation matrix
						mGL.applyMatrix(gtr[i]);
					}
				}
			} else { // only one rotation
				if (gtr.length == 7) { 
					// rotation 
					mGL.rotate(gtr[0],gtr[1],gtr[2],gtr[3],
						gtr[4],gtr[5],gtr[6]); // angle,x,y,z,cx,cy,cz								
				} else {
					// transformation matrix
					mGL.applyMatrix(gtr);
				}
			}
	   }  	
  }

  /**
   * Draw Element
   * @param element
   */
  self.drawElement = function (element, force) {
    // console.log("Drawing element "+element.getName());
    var build = null;
  	if (element.isGroupVisible()) {
        var build = mGL.getElement(element.getName() + ".mesh");
        if(!build || element.isMeshChanged() || element.isAlwaysUpdated()) {
          // if (build) mGL.removeElement(element.getName() + ".mesh");  		
      	  switch (element.getClass()) {
			case "ElementArrow": 	build = EJSS_WEBGLGRAPHICS.arrow(mGL,element);	break;
			case "ElementSegment": 	build = EJSS_WEBGLGRAPHICS.segment(mGL,element);	break;
			case "ElementSphere": 		build = EJSS_WEBGLGRAPHICS.ellipsoid(mGL,element);	break;
			case "ElementEllipsoid": 	build = EJSS_WEBGLGRAPHICS.ellipsoid(mGL,element);	break;						
			case "ElementBox": 		build = EJSS_WEBGLGRAPHICS.box(mGL,element);	break;
			case "ElementCylinder":	build = EJSS_WEBGLGRAPHICS.cylinder(mGL,element);	break;
			case "ElementCone":		build = EJSS_WEBGLGRAPHICS.cylinder(mGL,element);	break;
			case "ElementTetrahedron":		build = EJSS_WEBGLGRAPHICS.cylinder(mGL,element);	break;
			case "ElementDisk":		build = EJSS_WEBGLGRAPHICS.cylinder(mGL,element);	break;
			case "ElementBasic": 	build = EJSS_WEBGLGRAPHICS.basic(mGL,element);	break;
			case "ElementPlane": 	build = EJSS_WEBGLGRAPHICS.plane(mGL,element);	break;
			case "ElementSurface": 	build = EJSS_WEBGLGRAPHICS.surface(mGL,element);	break;
			case "ElementAnalyticSurface": 	build = EJSS_WEBGLGRAPHICS.analyticSurface(mGL,element);	break;
			case "ElementText": 	build = EJSS_WEBGLGRAPHICS.text(mGL,element);	break;
			case "ElementTrail": 	build = EJSS_WEBGLGRAPHICS.trail(mGL,element);	break;
			case "ElementAnalyticCurve":	build = EJSS_WEBGLGRAPHICS.analyticCurve(mGL,element);	break;
			case "ElementSpring":	build = EJSS_WEBGLGRAPHICS.spring(mGL,element);	break;
		  }
		}
		// if (build && (element.isMeshChanged() || element.isProjChanged() || force)) {
		if (build) {
    		mGL.matrixMode(mGL.MODEL);
    		mGL.loadIdentity();

			var groups = [];
			var el = element;
		    while (el.getGroup()) {
		    	el = el.getGroup();
   		        groups.unshift(el);
   		    }
			
			for (var g=0; g<groups.length; g++) {
			  el = groups[g];
			  
			  // group transformation
		      var gtr = el.getFullTransformation();
			  if (mTransformationOrder != 0) applyTranformation(gtr); 

		      // scale and translate
  	  		  mGL.translate(			
				el.getX()*element.getPanel().getSizeX(),
  	  			el.getY()*element.getPanel().getSizeY(),
  	  			el.getZ()*element.getPanel().getSizeZ());  	  		  	
			  mGL.scale(el.getSizeX(), el.getSizeY(), el.getSizeZ());
		      
			  if (mTransformationOrder == 0) applyTranformation(gtr); 
		    }		
			
			// individual transformation
			var tr = element.getFullTransformation(); // only support rotation
			if (mTransformationOrder != 0) applyTranformation(tr); 

		    // scale and translate
  	  		mGL.translate(element.getX()*element.getPanel().getSizeX(),
  	  			element.getY()*element.getPanel().getSizeY(),
  	  			element.getZ()*element.getPanel().getSizeZ());			

			if (mTransformationOrder == 0) applyTranformation(tr); 

			// adjust sizes
			if(element.getClass() == "ElementArrow" || element.getClass() == "ElementSegment" || element.getClass() == "ElementSpring" ) { // size is used to create the arrow
				mGL.scale(element.getPanel().getSizeX(),
	  	  			element.getPanel().getSizeY(),
	  	  			element.getPanel().getSizeZ());				
			} else if(element.getClass() == "ElementTrail" || element.getClass() == "ElementSurface") { // creados con su tamaño real
				mGL.scale(element.getPanel().getSizeX()*element.getSizeX(),
	  	  			element.getPanel().getSizeY()*element.getSizeY(),
	  	  			element.getPanel().getSizeZ()*element.getSizeZ());								
			} else { // creados con el doble de su tamaño
				mGL.scale(element.getPanel().getSizeX()*element.getSizeX()/2,
	  	  			element.getPanel().getSizeY()*element.getSizeY()/2,
	  	  			element.getPanel().getSizeZ()*element.getSizeZ()/2);				
			}
			
			// draw mesh
			self.drawMesh(mGL,element,build);

			// after transformations
    		mGL.matrixMode(mGL.VIEW);
		}
  	}  	  	
  	return build;
  };
 
  /**
   * Draw panel
   * @param panel
   */
  self.drawPanel = function(panel) {
 	if(!mGL) return;
 	
 	var style = panel.getStyle(); 	// style element	    	   	   

	// set background
    if(!panel.isImageUrl()) {	
		var color = style.getFillColor();
    	mGL.clearColor(color[0], color[1], color[2], (typeof color[3]!=="undefined") ? color[3] : 1.0);
    } else {
    	mGL.clearColor(0, 0, 0, 1.0);
    }
        
    return self;

  };
    
  /**
   * Draw mesh
   * @param mesh
   */
  self.drawMesh = function(mGL,element,mesh) {
  	  var opacity = (1.0 - element.getStyle().getTransparency() / 255);

  	  // get shader and texture
  	  var textureUrl = element.getTextureUrl();
	  if (element.getClass() == "ElementText" || 
	  		(textureUrl && textureUrl.length > 0)) {
		
 	  	 // Shader for texture 
		 var shader = mGL.getElement(mesh.getId() + ".shader_tex");
		 var texture = mGL.getElement(mesh.getId() + ".texture");	 
		 shader = shader || EJSS_WEBGLGRAPHICS.shader(mGL, mesh.getId() + "_tex",
		 	EJSS_WEBGLGRAPHICS.Shader["BasicVSwithTex"], EJSS_WEBGLGRAPHICS.Shader["BasicFSwithTex"]); 
	
	     if(texture) {
		  	texture.bind();
		  	shader.uniforms({ texture: 0 });
		  	shader.draw(mesh);
		  	texture.unbind();   
		  
		  	mGL.addElement(mesh.getId() + ".texture", texture);	
	     } else {
	        var tx = EJSS_WEBGLGRAPHICS.Texture.fromURL(mGL, textureUrl, {}, 
	      	function(tx) {
		  		element.getController().invokeAction("OnLoadTexture");	
		  		element.getController().reportInteractions();	    
	      	});

	    	tx.bind();
	    	shader.uniforms({ texture: 0 });
	    	shader.draw(mesh);
	    	tx.unbind();   	
			mGL.addElement(mesh.getId() + ".texture", tx);	      
	     }	
		
	     mGL.addElement(mesh.getId() + ".shader_tex", shader);
	  } else {		
		 // Shader for wire
		 var drawLines = element.getStyle().getDrawLines();
	  	 if (drawLines) {
	  		var shader = mGL.getElement(mesh.getId() + ".shader_lines");
	    	shader = shader || EJSS_WEBGLGRAPHICS.shader(mGL, mesh.getId() + "_lines", 
		 		EJSS_WEBGLGRAPHICS.Shader["BasicVS"], EJSS_WEBGLGRAPHICS.Shader["BasicFS"]);
		 		shader.uniforms({ color: element.getStyle().getLineColor(), opacity: opacity });	  	 		
	  	  	shader.draw(mesh, mGL.LINES);
	  	  	mGL.addElement(mesh.getId() + ".shader_lines", shader);
	    }            	
	  	// Shader for fill
		var drawFill = element.getStyle().getDrawFill();
	  	if (drawFill) {
		    var shader = mGL.getElement(mesh.getId() + ".shader_fill");
		
			var drawColors = (typeof element.getColors != 'undefined' && element.getColors().length > 0);	
			var drawLights = (element.getGroupPanel().getLights().length > 0);
			var hasPalette = (typeof element.getStyle().getPaletteFloor() != 'undefined');
			
			if(hasPalette) {
		    	shader = shader || EJSS_WEBGLGRAPHICS.shader(mGL, mesh.getId() + "_fill", 
			 		EJSS_WEBGLGRAPHICS.Shader["PaletteColorVS"], EJSS_WEBGLGRAPHICS.Shader["PaletteColorFS"]);
			 		shader.uniforms({ 
			 			floor: element.getStyle().getPaletteFloor(),
			 			ceil: element.getStyle().getPaletteCeil(),
			 			floorColor: element.getStyle().getPaletteFloorColor(),
			 			ceilColor: element.getStyle().getPaletteCeilColor()
			 		}); 					 					
			} else {
				if(!drawColors) { // draw one color
					 if(!drawLights) {
				    	shader = shader || EJSS_WEBGLGRAPHICS.shader(mGL, mesh.getId() + "_fill", 
					 		EJSS_WEBGLGRAPHICS.Shader["BasicVS"], EJSS_WEBGLGRAPHICS.Shader["BasicFS"]);
					 		shader.uniforms({ color: element.getStyle().getFillColor() }); 					 	
					 } else {
				    	shader = shader || EJSS_WEBGLGRAPHICS.shader(mGL, mesh.getId() + "_fill", 
					 		EJSS_WEBGLGRAPHICS.Shader["LightVS"], EJSS_WEBGLGRAPHICS.Shader["LightFS"]);
					 		shader.uniforms({ 
					 			ambientColor: element.getStyle().getAmbientColor(),
					 			diffuseColor: element.getStyle().getFillColor(),
					 			specularColor: element.getStyle().getSpecularColor(),
					 			ka: element.getStyle().getAmbientReflection(),
					 			kd: element.getStyle().getColorReflection(),
					 			ks: element.getStyle().getSpecularReflection(),
					 			shininessVal: element.getStyle().getShininessVal()
					 		}); 					 	
					 }
				} else { // draw with color array (only basic element)
					 if(!drawLights) {
				    	shader = shader || EJSS_WEBGLGRAPHICS.shader(mGL, mesh.getId() + "_fill", 
					 		EJSS_WEBGLGRAPHICS.Shader["ColorVS"], EJSS_WEBGLGRAPHICS.Shader["ColorFS"]);					 	
					 } else {
				    	shader = shader || EJSS_WEBGLGRAPHICS.shader(mGL, mesh.getId() + "_fill", 
					 		EJSS_WEBGLGRAPHICS.Shader["LightAndColorVS"], EJSS_WEBGLGRAPHICS.Shader["LightAndColorFS"]); 
					 		shader.uniforms({ 
					 			ambientColor: element.getStyle().getAmbientColor(),
					 			specularColor: element.getStyle().getSpecularColor(),
					 			ka: element.getStyle().getAmbientReflection(),
					 			kd: element.getStyle().getColorReflection(),
					 			ks: element.getStyle().getSpecularReflection(),
					 			shininessVal: element.getStyle().getShininessVal()
					 		}); 					 	
					 }					
				}				
			}			

			var lights = element.getGroupPanel().getLights();
			var numlight = lights.length;
		    shader.uniforms({ numlight: numlight, light: lights, opacity: opacity });
	  	    shader.draw(mesh, mGL.TRIANGLES);
		    mGL.addElement(mesh.getId() + ".shader_fill", shader);
		}
	  }  	
  }

// A value to bitwise-or with new enums to make them distinguishable from the
// standard WebGL enums.
var ENUM = 0x12340000;

// Implement the OpenGL modelview and projection matrix stacks, along with some
// other useful GLU matrix functions.
function addMatrixStack() {
  mGL.MODEL = ENUM | 1;
  mGL.VIEW = ENUM | 2;
  mGL.PROJECTION = ENUM | 3;
  var tempMatrix = new Matrix();
  var resultMatrix = new Matrix();
  mGL.modelMatrix = new Matrix();
  mGL.viewMatrix = new Matrix();
  mGL.projectionMatrix = new Matrix();
  var modelStack = [];
  var viewStack = [];
  var projectionStack = [];
  var matrix, stack;
  mGL.elements = {};	// shaders and mesh defined
  
  mGL.addElement = function(id, ele) {
  	// add or update  	
  	mGL.elements[id] = ele;
  }  

  mGL.getElement = function(id) {
  	return mGL.elements[id];
  }  

  mGL.removeElement = function(id) {
  	delete mGL.elements[id];
  }  

  mGL.matrixMode = function(mode) {
    switch (mode) {
      case mGL.MODEL:
        matrix = 'modelMatrix';
        stack = modelStack;
        break;
      case mGL.VIEW:
        matrix = 'viewMatrix';
        stack = viewStack;
        break;
      case mGL.PROJECTION:
        matrix = 'projectionMatrix';
        stack = projectionStack;
        break;
      default:
        throw 'invalid matrix mode ' + mode;
    }
  };
  mGL.loadIdentity = function() {
    Matrix.identity(mGL[matrix]);
  };
  mGL.loadMatrix = function(m) {
    var from = m.m, to = mGL[matrix].m;
    for (var i = 0; i < 16; i++) {
      to[i] = from[i];
    }
  };
  mGL.multMatrix = function(m) {
    mGL.loadMatrix(Matrix.multiply(mGL[matrix], m, resultMatrix));
  };
  mGL.perspective = function(fov, aspect, near, far) {
    mGL.multMatrix(Matrix.perspective(fov, aspect, near, far, tempMatrix));
  };
  mGL.frustum = function(l, r, b, t, n, f) {
    mGL.multMatrix(Matrix.frustum(l, r, b, t, n, f, tempMatrix));
  };
  mGL.ortho = function(l, r, b, t, n, f) {
    mGL.multMatrix(Matrix.ortho(l, r, b, t, n, f, tempMatrix));
  };
  mGL.scale = function(x, y, z) {
    mGL.multMatrix(Matrix.scale(x, y, z, tempMatrix));
  };
  mGL.translate = function(x, y, z) {
    mGL.multMatrix(Matrix.translate(x, y, z, tempMatrix));
  };
  mGL.applyMatrix = function(m) {
    mGL.multMatrix(new Matrix(m));
  };
  mGL.rotate = function(a, x, y, z, cx, cy, cz) {
  	cx = cx || 0;
  	cy = cy || 0;
  	cz = cz || 0;
    mGL.multMatrix(Matrix.rotate(a, x, y, z, cx, cy, cz, tempMatrix));
  };
  mGL.lookAt = function(ex, ey, ez, cx, cy, cz, ux, uy, uz) {
    mGL.multMatrix(Matrix.lookAt(ex, ey, ez, cx, cy, cz, ux, uy, uz, tempMatrix));
  };
  mGL.pushMatrix = function() {
    stack.push(Array.prototype.slice.call(mGL[matrix].m));
  };
  mGL.popMatrix = function() {
    var m = stack.pop();
    mGL[matrix].m = hasFloat32Array ? new Float32Array(m) : m;
  };
  mGL.matrixMode(mGL.MODEL);
}

 return self;           
}


