/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Constructor for Interaction
 * @param drawing panel
 * @returns An interaction
 */
EJSS_DRAWING3D.panelInteraction = function(mPanel) {
  var self = {};	// reference returned 

  var mEnabled = false;			// whether interaction is enable 
  var mLastLocation = [0,0];	// last position on panel trying interaction 
  var mDeltaX = 0;
  var mDeltaY = 0;
  var mMouseIsDown = false;		// last event is mouse down
  var mUpLastTime = 0;			// last timestamp for mouse up  
  var mZoomLastTime = 0;		// last timestamp for pinching  
  var mZoomDelta = 0;
  var mLastZoom = 0;
  var mPinching = false;
  var mPinchInitDistance = [0,0];

  /**
   * Return panel
   * @return drawing panel 
   */
  self.getPanel = function() {  	
    return mPanel;
  };  

  self.getEnabled = function() {
  	return mEnabled;
  } 

  self.setEnabled = function(enabled) {
  	if(mEnabled != enabled) {
	  	mEnabled = enabled;
	  	
	  	if(mEnabled) {
		  self.setHandler("move",self.handleMouseMoveEvent);
		  self.setHandler("down",self.handleMouseDownEvent);
		  self.setHandler("up",self.handleMouseUpEvent);     		
		  self.setHandler("mousewheel",self.handleMouseWheelEvent);
		  // self.setHandler("pinch",self.handlePinchGesture)
	  	} else { 
		  self.setHandler("move",(function() {}));
		  self.setHandler("down",(function() {}));
		  self.setHandler("up",(function() {}));    			  		
		  self.setHandler("mousewheel",(function() {}));
		  // self.setHandler("pinch",(function() {}));
	  	}
  	}
  } 

  /**
   * Return the last interaction point
   * @method getInteractionPoint
   * @return double[]
   */
  self.getInteractionPoint = function() {
    // mLastPoint = mPanel.toPanelPosition([location[0],location[1]]);
    return mLastLocation;
  };

  /**
   * Return the last interaction point deltas
   * @method getInteractionDeltas
   * @return double[]
   */
  self.getInteractionDeltas = function() {
    return [mDeltaX, mDeltaY];
  };

  /**
   * Return the last interaction zoom delta
   * @method getInteractionZoomDelta
   * @return double
   */
  self.getInteractionZoomDelta = function() {
    return mZoomDelta;
  };

  /**
   * Handler for mouse wheel event
   * @method handleMouseWheelEvent
   * @param event
   */
  self.handleMouseWheelEvent = function(event) {
	// number of fingers over screen	
	var nFingers = (typeof event.touches != "undefined")? event.touches.length:0;
	
	if(nFingers == 0) {  	
        // Prevent the browser from doing its default thing (scroll, zoom)
	    event.preventDefault(); 

		mZoomDelta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));	
        mPanel.getController().invokeAction("OnZoom");
        
	    // all interactions are reported to View
	    mPanel.getController().reportInteractions();
	}
  };

  /**
   * Handler for pinch gesture (only supported by safari and iOS)
   * @method handlePinchGesture
   * @param event
   */
  self.handlePinchGesture = function(event) {  	
	if(event.timeStamp - mZoomLastTime > 100) { 
		// Prevent the browser from doing its default thing (scroll, zoom)
		event.preventDefault();
				
		if (event.scale < mLastZoom) {
		    // User moved fingers closer together
		    mZoomDelta = 1;                
		} else if (event.scale >= mLastZoom) {
		    // User moved fingers further apart
		    mZoomDelta = -1;
		}
		mLastZoom = event.scale;
		
		mPanel.getController().invokeAction("OnZoom");
		
		// all interactions are reported to View
		mPanel.getController().reportInteractions();
		
		mZoomLastTime = event.timeStamp;
	} 
	
  };
  
  /**
   * Handler for mouse move event
   * @method handleMouseMoveEvent
   * @param event
   * @param location
   */
  self.handleMouseMoveEvent = function(event) {
	// number of fingers over screen	
	var nFingers = (typeof event.touches != "undefined")? event.touches.length:1;
	
	if(nFingers == 1) {  	
	  	var currentX = mLastLocation[0];
	  	var currentY = mLastLocation[1];  	
	    mLastLocation = getEventLocation(event); 
	    
	    mDeltaX = mLastLocation[0] - currentX;
	    mDeltaY = mLastLocation[1] - currentY; 
	    
	    if (mMouseIsDown) { // Mouse is down
      	  mPanel.getController().invokeAction("OnDrag");
	      event.target.style.cursor = 'move';
	    }
	    else { // Mouse is up
	      event.target.style.cursor = 'default';
	    }
	
	}
	
	if (nFingers == 2) {
		if (event.timeStamp - mZoomLastTime > 25) { // time filter
			// Prevent the browser from doing its default thing (scroll, zoom)
			event.preventDefault();
			
			var e0 = event.touches[0]; var e1 = event.touches[1];
			var e0x = (e0.clientX || e0.x); var e0y = (e0.clientY || e0.y);
			var e1x = (e1.clientX || e1.x); var e1y = (e1.clientY || e1.y);
			if(mPinching) {
	    		var newDistance = [Math.abs(e0x-e1x),Math.abs(e0y-e1y)];
	    		var delta = newDistance[0]-mPinchInitDistance[0]; 
	    		if (delta<3 && delta>-3) { // minimum filter
	    			mZoomDelta = 0;
	    		} else {
	    			mZoomDelta = delta;
	    			mPinchInitDistance = newDistance;								
	    		}
				mPanel.getController().invokeAction("OnZoom");
			} else {
				mPinching = true;
	    		mPinchInitDistance = [Math.abs(e0x-e1x),Math.abs(e0y-e1y)];
	    		mZoomDelta = 0;
			}					
			mZoomLastTime = event.timeStamp;
		}
	} else {
		mPinching = false;
		mPinchInitDistance = [0,0];
	}		
	
    // all interactions are reported to View
    mPanel.getController().reportInteractions();
  };

  /**
   * Handler for mouse down event
   * @method handleMouseDownEvent
   * @param event
   * @param location
   */
  self.handleMouseDownEvent = function(event) {
	// number of fingers over screen	
	var nFingers = (typeof event.touches != "undefined")? event.touches.length:1;
	
	if(nFingers == 1) {  	
	  	mMouseIsDown = true;
	  	
	  	mLastLocation = getEventLocation(event);   		
    	mPanel.getController().invokeAction("OnPress");
	    self.pick(mLastLocation[0],mLastLocation[1]);
	    
	    // Prevent the browser from doing its default thing (scroll, zoom)
	    event.preventDefault(); 
	    
	    // all interactions are reported to View
	    mPanel.getController().reportInteractions();
	}    
	
	mPinching = false;
  };

  /**
   * Handler for mouse up event
   * @method handleMouseUpEvent
   * @param event
   * @param location
   */
  self.handleMouseUpEvent = function(event) {
	// number of fingers over screen	
	var nFingers = (typeof event.touches != "undefined")? event.touches.length:1;
	
	if(nFingers == 1) {  	  	
	    mMouseIsDown = false; 
	
	  	mLastLocation = getEventLocation(event);
	
      	mPanel.getController().invokeAction("OnRelease");       
	    event.target.style.cursor = 'default';
	
		if(event.timeStamp - mUpLastTime < 500) { // < 500 ms is dblclick
			mPanel.getController().invokeAction("OnDoubleClick");
		}
		mUpLastTime = event.timeStamp; 
	    
	    // all interactions are reported to View
	    mPanel.getController().reportInteractions();
	} 
	
	mPinching = false;   
  };  

  /**
   * Get location for the touch event 
   * @method getEventLocation 
   * @param {Object} e
   */
  function getEventLocation(e) {
  	var x, y;
	var box = mPanel.getGraphics().getBox();
	var oleft = box.left; // offset left in pixels
	var otop = box.top; // offset top in pixels
  	
    if ((typeof e.changedTouches != "undefined") && (e.changedTouches.length === 1)) {    	
      	x = e.changedTouches[0].pageX;
      	y = e.changedTouches[0].pageY;      
	} else {
	    x = e.x || e.clientX;
	    y = e.y || e.clientY;
	}  	  	

  	return [x-oleft, y-otop];
  }
 
  self.setHandler = function(type, handler) {
	  var graphics = mPanel.getGraphics();
	  var context = graphics.getEventContext();
	  switch (type) {
	    case "move" : 
		  context.addEventListener( 'mousemove', handler, false );
		  context.addEventListener( 'touchmove', handler, false );
	      break;
	    default : 
	    case "down" :
	      context.addEventListener( 'mousedown', handler, false );
	      context.addEventListener( 'touchstart', handler, false );
	      break;
	    case "up" :
	      context.addEventListener( 'mouseup', handler, false );
	      context.addEventListener( 'touchend', handler, false );
	      break;
	    case "mousewheel" :
	      context.addEventListener( 'mousewheel', handler, false );
		  break;	      
	    case "pinch" :
	      context.addEventListener( 'gesturestart', function(event) { mLastZoom = 1; }, false );
	      context.addEventListener( 'gesturechange', handler, false );
	      break;
	  }
	  return false;
	}

  /**
   * Picking in webgl
   */   
  self.pick = function (mousex, mousey) {
    // we want to read the pixel at x, y -- so we really need a rectangle from x-1,y-1 with witdth and height equal to 1
    var x = mousex - 1;
    var y = mousey - 1;
    var w = 1;
    var h = 1;
    
    var gl = mPanel.getGraphics().getContext();
    if(!gl) return;
        
    var data = new Uint8Array(w*h*4); // w * h * 4
    gl.readPixels(x, y, w, h, gl.RGBA, gl.UNSIGNED_BYTE, data);
    if(data.data) data=data.data;

// http://stackoverflow.com/questions/7156971/webgl-readpixels-is-always-returning-0-0-0-0
// http://webgldemos.thoughtsincomputation.com/engine_tests/picking

/*    
    var indices = null, index, i;
    for (i = 2; i < data.length; i += 4) {
      if (data[i] > 0) // check the 'blue' key (2)
      {
        index = decodeFromColor(data[i-2], data[i-1], data[i], data[i+1]);
        if (index)
        {
          if (!indices) indices = {};
          indices[index] = index;
        }
      }
    }
    self.context.bindFramebuffer(GL_FRAMEBUFFER, null);
    self.context.viewport(0,0,self.context.gl.viewportWidth,self.context.gl.viewportHeight);

    self.context.enable(GL_BLEND);
        
    if (indices) {
      var ind = [];
      for (i in indices) ind.push(indices[i]);
      
      if (ind[0]) return this.objects[ind];
    }
      
    return null;  	
*/  }
    
  return self;          
}  
