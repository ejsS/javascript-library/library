/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Transformation object for 3D elements.
 * A transformation is an array of 7 numbers indicating
 *  [ rotation angle (in degrees), 
 *    x,y,z coordinates of the rotation axis, 
 *    x,y,z coordinates of the origin of the rotation ] 
 * @class Transformation 
 * @constructor  
 */
EJSS_DRAWING3D.Transformation = {
    /**
     * Registers properties in a ControlElement
     * @method registerProperties
     * @param element The element with the properties
     * @param controller A ControlElement that becomes the element controller
     */
    registerProperties : function(element,controller) {
      element.setController(controller);
      controller.registerProperty("Parent", element.setParent, element.getParent); 
      controller.registerProperty("Axis", element.setAxis, element.getAxis); 
      controller.registerProperty("Angle", element.setAngle, element.getAngle); 
      controller.registerProperty("Origin", element.setOrigin, element.getOrigin); 
    }
};

/**
 * Creates a Rotation around the X axis
 */
EJSS_DRAWING3D.rotationX = function (mName) {
  var self = EJSS_DRAWING3D.transformation();
  self.setAxis([1,0,0]);
  return self;  
}
  
/**
 * Creates a Rotation around the Y axis
 */
EJSS_DRAWING3D.rotationY = function (mName) {
  var self = EJSS_DRAWING3D.transformation();
  self.setAxis([0,1,0]);
  return self;  
}

/**
 * Creates a Rotation around the Z axis
 */
EJSS_DRAWING3D.rotationZ = function (mName) {
  var self = EJSS_DRAWING3D.transformation();
  self.setAxis([0,0,1]);
  return self;  
}

/**
 * Creates a Transformation object for 3D element
 */
EJSS_DRAWING3D.transformation = function (mName) {
  var self = {};
  
  var mParent = null;
  var mArray = [0,0,0,1,0,0,0];

  var mController = { 				// dummy controller object
      propertiesChanged : function() {},
      invokeAction : function() {}
  };
  
  self.getArray = function() {
    return mArray;
  };
  
  self.setChanged = function(changed) {
    if (changed) mParent.setProjChanged(true);
  };

  /**
   * Set the parent
   * @method setParent
   * @param parent Element
   */
  self.setParent = function(parent) {
    if (mParent) {
      mParent.removeExtraTransformation(self);
    }
    mParent = parent;
    if (mParent) {
      mParent.addExtraTransformation(self);
    }
  };

  self.getParent = function() {
    return mParent;
  };
  
  //---------------------------------
  // Customization
  //---------------------------------

  /**
   * Set the rotation angle
   * @method setAngle
   * @param angle int the angle in degrees
   */
  self.setAngle = function(angle) {
    mArray[0] = angle;
  };

  self.getAngle = function() {
    return mArray[0];
  };

  /**
   * Set the rotation axis
   * @method setAxis
   * @param axis double[3] the coordinates of the axis
   */
  self.setAxis = function(axis) {
    mArray[1] = axis[0];
    mArray[2] = axis[1];
    mArray[3] = axis[2];
  };

  self.getAxis = function() {
    return mArray.slice(1,4);
  };
  
  /**
   * Set the rotation origin
   * @method setOrigin
   * @param origin double[3] the coordinates of the origin
   */
  self.setOrigin = function(origin) {
    mArray[4] = origin[0];
    mArray[5] = origin[1];
    mArray[6] = origin[2];
  };

  self.getOrigin = function() {
    return mArray.slice(5);
  };
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  /**
   * Returns the controller object
   * @method getController
   * @return Controller
   */
  self.getController = function () {
    return mController;
  };

  /**
   * Set the controller
   * @method setController
   * @param Controller
   */
  self.setController = function (controller) {
    mController = controller;
  };
  
  /**
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING3D.Transformation.registerProperties(self,controller);
  };
  
  //---------------------------------
  // final initialization
  //---------------------------------
  
  return self;
};

