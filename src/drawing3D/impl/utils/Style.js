/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Style object for 3D drawing
 * @class Style 
 * @constructor  
 */
EJSS_DRAWING3D.Style = {
    
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * Copies one element into another
   */
  copyTo : function(source, dest) {
    dest.setClosedTop(source.getClosedTop());
    dest.setClosedBottom(source.getClosedBottom());
    dest.setClosedLeft(source.getClosedLeft());
    dest.setClosedRight(source.getClosedRight());
    dest.setDrawLines(source.getDrawLines());
    dest.setLineColor(source.getLineColor());
    dest.setLineWidth(source.getLineWidth());
    dest.setDrawFill(source.getDrawFill());
    dest.setTransparency(source.getTransparency());
    
	dest.setAmbientColor(source.getAmbientColor());
	dest.setFillColor(source.getFillColor());
	dest.setSpecularColor(source.getSpecularColor());
	dest.setAmbientReflection(source.getAmbientReflection());
	dest.setColorReflection(source.getColorReflection());
	dest.setSpecularReflection(source.getSpecularReflection());
	dest.setShininessVal(source.getShininessVal());
	
	dest.setPaletteFloor(source.getPaletteFloor());
	dest.setPaletteCeil(source.getPaletteCeil());
	dest.setPaletteFloorColor(source.getPaletteFloorColor());
	dest.setPaletteCeilColor(source.getPaletteCeilColor());	    
  },
  
};

/**
 * Creates a Style object for 3D drawing
 */
EJSS_DRAWING3D.style = function (mName) {
  var self = {};
  
  var mClosedTop = true;
  var mClosedBottom = true;
  var mClosedLeft = true;
  var mClosedRight = true;
  var mLineColor = [0,0,0];
  var mLineWidth = 0.5;     
  
  // type of material
  var mAmbientColor = [0.0,0.0,0.0];
  var mFillColor = [0.0,0.0,0.0];
  var mSpecularColor = [1.0,1.0,1.0];
  var mAmbientReflection = 1.0;
  var mColorReflection = 1.0;
  var mSpecularReflection = 1.0;
  var mShininessVal = 10;

  // color palette of material
  var mPaletteFloor; // e.g. [0.0,0.0,-5.0];
  var mPaletteCeil;  // e.g. [0.0,0.0,5.0];
  var mPaletteFloorColor = [1.0,0.0,-1.0];
  var mPaletteCeilColor = [-1.0,0.0,1.0];

  var mDrawLines = false;
  var mDrawFill = true;
  var mTransparency = 0.0;  
  var mProjChangeListener;       
  var mMeshChangeListener;       

  /**
   * Set a listener that will be called whenever there are style changes.
   * I.e. a call to listener("change"); will be issued
   */
  self.setProjChangeListener = function(listener) {
    mProjChangeListener = listener;
  };

  self.setMeshChangeListener = function(listener) {
    mMeshChangeListener = listener;
  };

  //---------------------------------
  // Closed
  //---------------------------------

  self.setClosedTop = function(closed) {
    if (closed!=mClosedTop) {
      mClosedTop = closed;
      if (mMeshChangeListener) mMeshChangeListener("closedtop");
    }
  };
  
  self.getClosedTop = function() { 
    return mClosedTop; 
  };

  self.setClosedBottom = function(closed) {
    if (closed!=mClosedBottom) {
      mClosedBottom = closed;
      if (mMeshChangeListener) mMeshChangeListener("closedbottom");
    }
  };
  
  self.getClosedBottom = function() { 
    return mClosedBottom; 
  };
  
  self.setClosedLeft = function(closed) {
    if (closed!=mClosedLeft) {
      mClosedLeft = closed;
      if (mMeshChangeListener) mMeshChangeListener("closedleft");
    }
  };
  
  self.getClosedLeft = function() { 
    return mClosedLeft; 
  };
  
  self.setClosedRight = function(closed) {
    if (closed!=mClosedRight) {
      mClosedRight = closed;
      if (mMeshChangeListener) mMeshChangeListener("closedright");
    }
  };
  
  self.getClosedRight = function() { 
    return mClosedRight; 
  };
  
  //---------------------------------
  // lines
  //---------------------------------

  /**
   * Whether to draw the lines of the element
   */
  self.setDrawLines = function(draw) {
    if (draw!=mDrawLines) {
      mDrawLines = draw;
      if (mProjChangeListener) mProjChangeListener("drawlines");
    }
  };
  
  /**
   * Get the draw lines flag
   */
  self.getDrawLines = function() { 
    return mDrawLines; 
  };
  
  /**
   * Set the line color of the element
   * @param color a stroke style
   */
  self.setLineColor = function(color) { 
    if (typeof color === "string") color = EJSS_TOOLS.DisplayColors.getArrayColor(color);
    if (!EJSS_TOOLS.compareArrays(color,mLineColor)) {
      mLineColor = color.slice(); 
      if (mProjChangeListener) mProjChangeListener("linecolor");
    }
    return self;
  };
    
  /**
   * Get the line color
   */
  self.getLineColor = function() { 
    return mLineColor; 
  };

  /**
   * Set the line width of the element
   * @param width a stroke width (may be double, such as 0.5, the default)
   */
  self.setLineWidth = function(width) { 
    if (width!=mLineWidth) {
      mLineWidth = width; 
      if (mProjChangeListener) mProjChangeListener("linewidth");
    }
  };

  /**
   * Get the line width
   */
  self.getLineWidth = function() { return mLineWidth; };
  
  //---------------------------------
  // interior fill
  //---------------------------------

  /**
   * Whether to fill the interior of the element
   */
  self.setDrawFill = function(draw) {
    if (draw!=mDrawFill) {
      mDrawFill = draw;
      if (mProjChangeListener) mProjChangeListener("drawfill");
    }
  };
  
  /**
   * Get the draw fill flag
   */
  self.getDrawFill = function() { 
    return mDrawFill; 
  };

  /**
   * Set the fill color of the element
   * @param color a fill style
   */
  self.setFillColor = function(color) {
    if (typeof color === "string") color = EJSS_TOOLS.DisplayColors.getArrayColor(color);
    if (!EJSS_TOOLS.compareArrays(color,mFillColor)) {
      mFillColor = color.slice(); 
      if (mProjChangeListener) mProjChangeListener("fillcolor");
    }
  };

  /**
   * Get the fill color
   */
  self.getFillColor = function() { 
    return mFillColor; 
  };
  
  /**
   * Set the Ambient color of the element
   * @param color ambient color
   */
  self.setAmbientColor = function(color) {
    if (typeof color === "string") color = EJSS_TOOLS.DisplayColors.getArrayColor(color);
    if (!EJSS_TOOLS.compareArrays(color,mAmbientColor)) {
      mAmbientColor = color.slice(); 
      if (mProjChangeListener) mProjChangeListener("ambientcolor");
    }
  };
  
  /**
   * Get the Ambient color
   */
  self.getAmbientColor = function() { 
    return mAmbientColor; 
  };

  /**
   * Set the Specular color of the element
   * @param color specular color
   */
  self.setSpecularColor = function(color) {
    if (typeof color === "string") color = EJSS_TOOLS.DisplayColors.getArrayColor(color);
    if (!EJSS_TOOLS.compareArrays(color,mSpecularColor)) {
      mSpecularColor = color.slice(); 
      if (mProjChangeListener) mProjChangeListener("specularcolor");
    }
  };
  
  /**
   * Get the Specular color
   */
  self.getSpecularColor = function() { 
    return mSpecularColor; 
  };

  /**
   * Set the Ambient Reflection of the element
   * @param k coefficient
   */
  self.setAmbientReflection = function(k) {
    if (mAmbientReflection != k) {
      mAmbientReflection = k; 
      if (mProjChangeListener) mProjChangeListener("ambientreflection");
    }
  };
  
  /**
   * Get the Ambient reflection
   */
  self.getAmbientReflection = function() { 
    return mAmbientReflection; 
  };

  /**
   * Set the Color Reflection of the element
   * @param k coefficient
   */
  self.setColorReflection = function(k) {
    if (mColorReflection != k) {
      mColorReflection = k; 
      if (mProjChangeListener) mProjChangeListener("colorreflection");
    }
  };
  
  /**
   * Get the Color reflection
   */
  self.getColorReflection = function() { 
    return mColorReflection; 
  };

  /**
   * Set the Specular Reflection of the element
   * @param k coefficient
   */
  self.setSpecularReflection = function(k) {
    if (mSpecularReflection != k) {
      mSpecularReflection = k; 
      if (mProjChangeListener) mProjChangeListener("specularreflection");
    }
  };
  
  /**
   * Get the Specular reflection
   */
  self.getSpecularReflection = function() { 
    return mSpecularReflection; 
  };

  /**
   * Set the shininess of the element
   * @param shininess coefficient
   */
  self.setShininessVal = function(shininess) {
    if (mShininessVal != shininess) {
      mShininessVal = shininess; 
      if (mProjChangeListener) mProjChangeListener("shininess");
    }
  };
  
  /**
   * Get the shininess 
   */
  self.getShininessVal = function() { 
    return mShininessVal; 
  };

  /**
   * Set transparency
   * @param transparency
   */
  self.setTransparency = function(transparency) {
    if (transparency!=mTransparency) {
      mTransparency = transparency; 
      if (mProjChangeListener) mProjChangeListener("transparency");
    }
  };
  
  /**
   * Get transparency
   */
  self.getTransparency = function() { 
    return mTransparency; 
  };

  /**
   * Set the palette floor of the element
   * @param floor palette floor
   */
  self.setPaletteFloor = function(floor) {
    if (floor && !EJSS_TOOLS.compareArrays(floor,mPaletteFloor)) {
      mPaletteFloor = floor.slice(); 
      if (mProjChangeListener) mProjChangeListener("palettefloor");
    }
  };

  /**
   * Get the palette floor
   */
  self.getPaletteFloor = function() { 
    return mPaletteFloor; 
  };

  /**
   * Set the palette ceil of the element
   * @param ceil palette ceil
   */
  self.setPaletteCeil = function(ceil) {
    if (ceil && !EJSS_TOOLS.compareArrays(ceil,mPaletteCeil)) {
      mPaletteCeil = ceil.slice(); 
      if (mProjChangeListener) mProjChangeListener("paletteceil");
    }
  };

  /**
   * Get the palette ceil
   */
  self.getPaletteCeil = function() { 
    return mPaletteCeil; 
  };

  /**
   * Set the palette floor color of the element
   * @param color palette floor color
   */
  self.setPaletteFloorColor = function(color) {
    if (!EJSS_TOOLS.compareArrays(color,mPaletteFloorColor)) {
      mPaletteFloorColor = color.slice(); 
      if (mProjChangeListener) mProjChangeListener("palettefloorcolor");
    }
  };

  /**
   * Get the palette floor color
   */
  self.getPaletteFloorColor = function() { 
    return mPaletteFloorColor; 
  };

  /**
   * Set the palette ceil color of the element
   * @param color palette ceil color
   */
  self.setPaletteCeilColor = function(color) {
    if (!EJSS_TOOLS.compareArrays(color,mPaletteCeilColor)) {
      mPaletteCeilColor = color.slice(); 
      if (mProjChangeListener) mProjChangeListener("paletteceilcolor");
    }
  };

  /**
   * Get the palette ceil color
   */
  self.getPaletteCeilColor = function() { 
    return mPaletteCeilColor; 
  };


  //---------------------------------
  // final initialization
  //---------------------------------
  
  return self;
};

