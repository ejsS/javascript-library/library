/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Ellipsoid
 * @class Ellipsoid 
 * @constructor  
 */
EJSS_DRAWING3D.Ellipsoid = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  	
		dest.setMinAngleU(source.getMinAngleU());
		dest.setMaxAngleU(source.getMaxAngleU());
		dest.setMinAngleV(source.getMinAngleV());
		dest.setMaxAngleV(source.getMaxAngleV());
		
  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class

      controller.registerProperty("MinAngleU", element.setMinAngleU);
      controller.registerProperty("MaxAngleU", element.setMaxAngleU);
      controller.registerProperty("MinAngleV", element.setMinAngleV);
      controller.registerProperty("MaxAngleV", element.setMaxAngleV);
		
	}

};

/**
 * Creates a 3D Ellipsoid
 * @method ellipsoid
 */
EJSS_DRAWING3D.ellipsoid = function (name) {
  var self = EJSS_DRAWING3D.element(name);
  var mMinAngleU = 0; 		// the start angle (in degrees) for the parallels
  var mMaxAngleU = 360;		// the end angle (in degrees) for the parallels
  var mMinAngleV = -90;		// the start angle (in degrees) for the meridians
  var mMaxAngleV = 90;		// the end angle (in degrees) for the meridians

  // Implementation variables
  self.getClass = function() {
  	return "ElementEllipsoid";
  }

  self.setMinAngleU = function(angle) {
  	if(mMinAngleU != angle) {
  	  mMinAngleU = angle;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getMinAngleU = function() {
  	return mMinAngleU;
  }

  self.setMaxAngleU = function(angle) {
  	if(mMaxAngleU != angle) {
  	  mMaxAngleU = angle;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getMaxAngleU = function() {
  	return mMaxAngleU;
  }
  
  self.setMinAngleV = function(angle) {
  	if(mMinAngleV != angle) {
  	  mMinAngleV = angle;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getMinAngleV = function() {
  	return mMinAngleV;
  }

  self.setMaxAngleV = function(angle) {
  	if(mMaxAngleV != angle) {
  	  mMaxAngleV = angle;
  	  self.setMeshChanged(true);
  	}  	
  }

  self.getMaxAngleV = function() {
  	return mMaxAngleV;
  }
    
  self.copyTo = function(element) {
	EJSS_DRAWING3D.Ellipsoid.copyTo(self,element);
  };
      
  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Ellipsoid.registerProperties(self, controller);
  };
	    
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);
  self.setResolution([30,20]);

  return self;
};



