/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/**
 * Box
 * @class Box 
 * @constructor  
 */
EJSS_DRAWING3D.Box = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING3D.Element.copyTo(source,dest); // super class copy
  	
		dest.setReduceZby(source.getReduceZby());
  	
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING3D.Element.registerProperties(element, controller);
		// super class
		
      	controller.registerProperty("ReduceZby", element.setReduceZby);

	}
};

/**
 * Creates a 3D Box
 * @method box
 */
EJSS_DRAWING3D.box = function (name) {
  var self = EJSS_DRAWING3D.element(name);
  var mReduceZby = 0.0;

  // Implementation variables
  self.getClass = function() {
  	return "ElementBox";
  }

  self.setReduceZby = function(reduce) {
  	if(mReduceZby != reduce) {
  	  mReduceZby = reduce;
  	  self.setMeshChanged(true);
  	}
  }

  self.getReduceZby = function() {
  	return mReduceZby;
  }

  self.registerProperties = function(controller) {
	EJSS_DRAWING3D.Box.registerProperties(self, controller);
  };

  self.copyTo = function(element) {
	EJSS_DRAWING3D.Box.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1,1]);

  return self;
};



