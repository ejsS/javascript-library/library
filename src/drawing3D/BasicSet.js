/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//BasicSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * BasicSet
 * @class BasicSet 
 * @constructor  
 */
EJSS_DRAWING3D.BasicSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("Description", 
          function(v) { set.setToEach(function(element,value) { element.setDescription(value); }, v); }
      );
      controller.registerProperty("Vertices", 
          function(v) { set.setToEach(function(element,value) { element.setVertices(value); }, v); }
      );
      controller.registerProperty("Triangles", 
          function(v) { set.setToEach(function(element,value) { element.setTriangles(value); }, v); }
      );           
      controller.registerProperty("Normals", 
          function(v) { set.setToEach(function(element,value) { element.setNormals(value); }, v); }
      );           
      controller.registerProperty("Colors", 
          function(v) { set.setToEach(function(element,value) { element.setColors(value); }, v); }
      );           
    }        

};


/**
 * Creates a set of basics
 * @method basicSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.basicSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.basic,mName);

  // Static references
  var BasicSet = EJSS_DRAWING3D.BasicSet;		// reference for BasicSet
  
  self.registerProperties = function(controller) {
    BasicSet.registerProperties(self,controller);
  };

  return self;
};
