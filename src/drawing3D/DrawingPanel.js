/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

var EJSS_DRAWING3D = EJSS_DRAWING3D || {};

/***
 * A DrawingPanel is a 3D drawing panel. 
 * @class EJSS_DRAWING3D.DrawingPanel
 * @constructor
 */
 EJSS_DRAWING3D.DrawingPanel = {  
  GRAPHICS3D_WEBGL : "WebGL",

  /**
   * static registerProperties method
   */
  registerProperties : function(element, controller) {
    // No super class
    
    element.setController(controller);
    
	 /*** 
	  * The minimum value for the X coordinates of elements. 
	  * @property MinimumX 
	  * @type int or double
	  * @default "-1"
	  */ 
    controller.registerProperty("MinimumX",element.setWorldXMin,element.getWorldXMin);
	 /*** 
	  * The maximum value for the X coordinates of elements. 
	  * @property MaximumX 
	  * @type int or double
	  * @default "+1"
	  */ 
    controller.registerProperty("MaximumX",element.setWorldXMax,element.getWorldXMax);
	 /*** 
	  * The minimum value for the Y coordinates of elements. 
	  * @property MinimumY 
	  * @type int or double
	  * @default "-1"
	  */ 
    controller.registerProperty("MinimumY",element.setWorldYMin,element.getWorldYMin);
	 /*** 
	  * The maximum value for the Y coordinates of elements. 
	  * @property MaximumY
	  * @type int or double
	  * @default "+1"
	  */ 
    controller.registerProperty("MaximumY",element.setWorldYMax,element.getWorldYMax);
	 /*** 
	  * The minimum value for the Z coordinates of elements. 
	  * @property MinimumZ 
	  * @type int or double
	  * @default "-1"
	  */ 
    controller.registerProperty("MinimumZ",element.setWorldZMin,element.getWorldZMin);
	 /*** 
	  * The maximum value for the Z coordinates of elements. 
	  * @property MaximumZ 
	  * @type int or double
	  * @default "+1"
	  */ 
    controller.registerProperty("MaximumZ",element.setWorldZMax,element.getWorldZMax);
    controller.registerProperty("Bounds",element.setWorldCoordinates,element.getWorldCoordinates);
    
    controller.registerProperty("Size",element.setSize,element.getSize);
	 /*** 
	  * Size for the X axis.
	  * @property SizeX
	  * @type int or double 
	  * @default 1
	  */ 
    controller.registerProperty("SizeX",element.setSizeX,element.getSizeX);
	 /*** 
	  * Size for the Y axis.
	  * @property SizeY
	  * @type int or double 
	  * @default 1
	  */ 
    controller.registerProperty("SizeY",element.setSizeY,element.getSizeY);
	 /*** 
	  * Size for the Z axis.
	  * @property SizeZ
	  * @type int or double 
	  * @default 1
	  */ 
    controller.registerProperty("SizeZ",element.setSizeZ,element.getSizeZ);

	 /*** 
	  * Camera rotation in Z axis.
	  * @property CameraAzimuth
	  * @type int or double (degrees) 
	  * @default 0
	  */ 
    controller.registerProperty("CameraAzimuth",element.setCamAzimuth,element.getCamAzimuth);
	 /*** 
	  * Camera rotation in Y axis.
	  * @property CameraAltitude
	  * @type int or double (degrees) 
	  * @default 0
	  */ 
    controller.registerProperty("CameraAltitude",element.setCamAltitude,element.getCamAltitude);
	 /*** 
	  * Camera rotation in X axis.
	  * @property CameraTilt
	  * @type int or double (degrees) 
	  * @default 0
	  */ 
    controller.registerProperty("CameraTilt",element.setCamTilt,element.getCamTilt);
	 /*** 
	  * Camera position in X axis.
	  * @property CameraX
	  * @type int or double 
	  * @default 4
	  */ 
    controller.registerProperty("CameraX",element.setCamLocX,element.getCamLocX);
	 /*** 
	  * Camera position in Y axis.
	  * @property CameraY
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraY",element.setCamLocY,element.getCamLocY);
	 /*** 
	  * Camera position in Z axis.
	  * @property CameraZ
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraZ",element.setCamLocZ,element.getCamLocZ);
    controller.registerProperty("CameraLocation",element.setCamLoc,element.getCamLoc);
	 /*** 
	  * Camera focus in X axis.
	  * @property CameraFocusX
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraFocusX",element.setCamFocusX,element.getCamFocusX);
	 /*** 
	  * Camera focus in Y axis.
	  * @property CameraFocusY
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraFocusY",element.setCamFocusY,element.getCamFocusY);
	 /*** 
	  * Camera focus in Z axis.
	  * @property CameraFocusZ
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraFocusZ",element.setCamFocusZ,element.getCamFocusZ);
    controller.registerProperty("CameraFocus",element.setCamFocus,element.getCamFocus);
	 /*** 
	  * Camera up vector in X axis.
	  * @property CameraUpVectorX
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraUpVectorX",element.setCamUpVectorX,element.getCamUpVectorX);
	 /*** 
	  * Camera up vector in Y axis.
	  * @property CameraUpVectorY
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraUpVectorY",element.setCamUpVectorY,element.getCamUpVectorY);
	 /*** 
	  * Camera up vector in Z axis.
	  * @property CameraUpVectorZ
	  * @type int or double 
	  * @default 0
	  */ 
    controller.registerProperty("CameraUpVectorZ",element.setCamUpVectorZ,element.getCamUpVectorZ);
    controller.registerProperty("CameraUpVector",element.setCamUpVector,element.getCamUpVector);
	 /*** 
	  * Camera zoom rate.
	  * @property CameraZoomRate
	  * @type int or double 
	  * @default 1.10
	  */ 
    controller.registerProperty("CameraZoomRate",element.setZoomRate,element.getZoomRate);
	 /*** 
	  * Near plane in 3D projection.
	  * @property CameraNear 
	  * @type int
	  * @default 1
	  */ 
    controller.registerProperty("CameraNear",element.setNear,element.getNear);
	 /*** 
	  * Far plane in 3D projection.
	  * @property CameraFar 
	  * @type int
	  * @default 10000
	  */ 
    controller.registerProperty("CameraFar",element.setFar,element.getFar);
	 /*** 
	  *  Type of projection.  
	  * @property Projection 
	  * @type int or string 
	  * @values "PLANAR_XY": 0; "PLANAR_XZ": 1; "PLANAR_YZ": 2; "PERSPECTIVE_OFF": 3; "PERSPECTIVE_ON": 4;
	  * @default "PERSPECTIVE_ON"
	  */ 
	controller.registerProperty("Projection",element.setProjection, element.getProjection);
	 /*** 
	  * Field of view when perspective on. A parameter that gives more or less perspective to the projection.
	  * @property Perspective 
	  * @type int (degrees)
	  * @default "45"
	  */ 
	controller.registerProperty("Perspective",element.setFOV, element.getFOV);
    controller.registerProperty("FieldOfView",element.setFOV,element.getFOV);

	 /*** 
	  * Lights in scene.
	  * @property Lights 
	  * @type array of array 
	  * @default [[1,2,3]]
	  */ 
    controller.registerProperty("Lights",element.setLights,element.getLights);
    
    controller.registerProperty("DecorationType",element.setDecorationType);
    controller.registerProperty("VisibleDecorationBox",element.setVisibleDecorationBox);
    controller.registerProperty("VisibleDecorationAxis",element.setVisibleDecorationAxis);
    controller.registerProperty("VisibleDecorationBasicAxis",element.setVisibleDecorationBasicAxis);

    controller.registerProperty("Parent",element.getGraphics().setParent,element.getGraphics().getParent);
    controller.registerProperty("ClassName", element.getGraphics().setClassName);
    controller.registerProperty("Width",element.getGraphics().setWidth, element.getGraphics().getWidth);
    controller.registerProperty("Height",element.getGraphics().setHeight,element.getGraphics().getHeight);
    
    controller.registerProperty("ImageUrl",element.setImageUrl);

    controller.registerProperty("Graphics",element.setGraphics);
    controller.registerProperty("Enabled",element.setEnabled);    
    controller.registerProperty("Draggable",element.setDraggable);    

	controller.registerProperty("Background", element.getStyle().setFillColor);
    controller.registerProperty("Foreground",  element.getGraphics().getStyle().setBorderColor);
    controller.registerProperty("LineColor",  element.getGraphics().getStyle().setBorderColor);
    controller.registerProperty("LineWidth",  element.getGraphics().getStyle().setBorderWidth);
    controller.registerProperty("DrawLines",  element.getStyle().setDrawLines);
    controller.registerProperty("FillColor",  element.getStyle().setFillColor);
    controller.registerProperty("DrawFill",   element.getStyle().setDrawFill);

    controller.registerProperty("Visibility", element.getGraphics().getStyle().setVisibility);      
    controller.registerProperty("Display", element.getGraphics().getStyle().setDisplay); 
	controller.registerProperty("CSS", element.getGraphics().getStyle().setCSS);

    controller.registerAction("OnDoubleClick", element.getInteraction().getInteractionPoint);      
    controller.registerAction("OnMove", element.getInteraction().getInteractionPoint);      
    controller.registerAction("OnPress", element.getInteraction().getInteractionPoint);    
    controller.registerAction("OnDrag", element.getInteraction().getInteractionPoint, element.getOnDragHandler);    
	controller.registerAction("OnRelease", element.getInteraction().getInteractionBounds);
    controller.registerAction("OnZoom", element.getInteraction().getZoomDelta, element.getOnZoomHandler);    
        
  }

};

/**
 * Constructor for DrawingPanel
 * @method drawingPanel
 * @param mName string
 * @returns An abstract 3D drawing panel
 */
EJSS_DRAWING3D.drawingPanel = function (mName) {
  var self = {};										// reference returned 
  var mGraphics = EJSS_GRAPHICS.webGLGraphics(mName);	// graphics implementation (default: WebGL) 

  // Instance variables
  var mStyle = EJSS_DRAWING3D.style(mName);			// style for panel 
  var mDecorations = [];							// decorations list for panel
  var mElements = [];								// elements list for panel
  var mElementsChanged = true;						// whether elements list has changed 
  var mDraggable = 1;								// how panel responds to interaction
  var mImageBackground = false;						// whether background image 
  var mCollectersList = [];		            // Array of all control elements that need a call to dataCollected() after data collection
   
  // Configuration variables 
  var mWorld = {
  	  // preferred dimensions
      xminPreferred: -1, xmaxPreferred: +1, yminPreferred: -1, ymaxPreferred: +1, zminPreferred: -1, zmaxPreferred: +1,  
  };
  
  // Camera
  var mChangeCamera = true;
  var mAutoCamera = true;
  var mZoomRate = 1.10;
  var mCamera = {
  	location: new Vector(4, 0, 0),
  	focus: new Vector(0, 0, 0),
  	upvector: new Vector(0, 0, 1),
  	azimuth: 0,
  	altitude: 0,
  	tilt: 0,
  	delta: 0
  }    
  var mFOV = 45;
  var mNear = 1;
  var mFar = 10000;
  var mOrthographic = false;
  var mProjection = 4;
  
  // Size  
  var mSizeX = 1;					// size X
  var mSizeY = 1;					// size Y
  var mSizeZ = 1;					// size Z
  
  // Lights
  var mLights = [[1,2,3]];
  
  // Implementation variables
  var mPanelChanged = true;			// whether panel changed (style, decorations)
  var mChangeWorld = true;			// whether decorations must change
  var mInteraction;					// user interaction on panel
  
  var mController = { // dummy controller object
          propertiesChanged : function() {},
          invokeAction : function() {}
  };
  
  // ----------------------------------------
  // Instance functions
  // ----------------------------------------

  /***
   * Get name for drawing panel
   * @method getName
   * @return string
   */
  self.getName = function() {
    return mName;
  };

  /***
   * Returns the graphics implementation
   * @method getGraphics
   * @return Graphics
   */
  self.getGraphics = function() {
    return mGraphics;
  };
 
  /***
   * Whether webgl is supported
   * @method supportsWebGL
   * @return boolean
   */
  self.supportsWebGL = function() {
    return mGraphics.supportsWebGL();
  };
 
  /***
   * Return the drawing style of the inner rectangle for panel
   * @method getStyle
   * @return Style
   */
  self.getStyle = function() { 
    return mStyle; 
  };

  /***
   * Set graphics (only Webgl supported)
   * @method setGraphics
   * @param type
   */
  self.setGraphics = function(type) {
  	if(type == EJSS_DRAWING3D.DrawingPanel.GRAPHICS3D_WEBGL) {
		mGraphics = EJSS_GRAPHICS.webGLGraphics(mName); 
	} else {
		console.log("WARNING: setGraphics() - Graphics not supported");
	}	
  }

  // ----------------------------------------
  // World coordinates
  // ----------------------------------------

  /***
   * Sets the preferred minimum X coordinate for the panel
   * @method setWorldXMin
   * @param xmin
   */
  self.setWorldXMin = function(xmin)  {
    if (xmin !== mWorld.xminPreferred) { 
      mWorld.xminPreferred = xmin;
      mChangeWorld = true; 
    }
  };

  /***
   * Returns the preferred minimum X coordinate for the panel
   * @method getWorldXMin
   * @return double
   */
  self.getWorldXMin = function() {
    return mWorld.xminPreferred;
  };
  
  /***
   * Sets the preferred maximum X coordinate for the panel
   * @method setWorldXMax
   * @param xmax
   */
  self.setWorldXMax = function(xmax)  {
    if (xmax !== mWorld.xmaxPreferred) { 
      mWorld.xmaxPreferred = xmax;
      mChangeWorld = true; 
    }
  };

  /***
   * Returns the preferred maximum X coordinate for the panel
   * @method getWorldXMax
   * @return double
   */
  self.getWorldXMax = function() {
    return mWorld.xmaxPreferred;
  };

  /***
   * Sets the preferred minimum Y coordinate for the panel
   * @method setWorldYMin
   * @param ymin
   */
  self.setWorldYMin = function(ymin)  {
    if (ymin !== mWorld.yminPreferred) { 
      mWorld.yminPreferred = ymin;
      mChangeWorld = true; 
    }
  };
  
  /***
   * Returns the preferred minimum Y coordinate for the panel
   * @method getWorldYMin
   * @return double
   */
  self.getWorldYMin = function() {
    return mWorld.yminPreferred;
  };

  /***
   * Sets the preferred maximum Y coordinate for the panel
   * @method setWorldYMax
   * @param ymax
   */
  self.setWorldYMax = function(ymax)  {
    if (ymax !== mWorld.ymaxPreferred) { 
      mWorld.ymaxPreferred = ymax;
      mChangeWorld = true; 
    }
  };

  /***
   * Returns the preferred maximum Y coordinate for the panel
   * @method getWorldYMax
   * @return double
   */
  self.getWorldYMax = function() {
    return mWorld.ymaxPreferred;
  };

  /***
   * Sets the preferred minimum Y coordinate for the panel
   * @method setWorldZMin
   * @param zmin
   */
  self.setWorldZMin = function(zmin)  {
    if (zmin !== mWorld.zminPreferred) { 
      mWorld.zminPreferred = zmin;
      mChangeWorld = true; 
    }
  };
  
  /***
   * Returns the preferred minimum Z coordinate for the panel
   * @method getWorldZMin
   * @return double
   */
  self.getWorldZMin = function() {
    return mWorld.zminPreferred;
  };

  /***
   * Sets the preferred maximum Z coordinate for the panel
   * @method setWorldZMax
   * @param zmax
   */
  self.setWorldZMax = function(zmax)  {
    if (zmax !== mWorld.zmaxPreferred) { 
      mWorld.zmaxPreferred = zmax;
      mChangeWorld = true; 
    }
  };

  /***
   * Returns the preferred maximum Z coordinate for the panel
   * @method getWorldZMax
   * @return double
   */
  self.getWorldZMax = function() {
    return mWorld.zmaxPreferred;
  };

  /***
   * Sets the preferred user coordinates for the panel
   * @method setWorldCoordinates
   * @param bounds
   */
  self.setWorldCoordinates = function(bounds)  {
    self.setWorldXMin(bounds[0]);
    self.setWorldXMax(bounds[1]);
    self.setWorldYMin(bounds[2]);
    self.setWorldYMax(bounds[3]);
    self.setWorldZMin(bounds[4]);
    self.setWorldZMax(bounds[5]);
  };

  /***
   * Gets the preferred user coordinates for the panel
   * @method getWorldCoordinates
   * @return bounds
   */
  self.getWorldCoordinates = function()  {
  	return [self.getWorldXMin(),self.getWorldXMax(),
  			self.getWorldYMin(),self.getWorldYMax(),
  			self.getWorldZMin(),self.getWorldZMax()];
  };
  
  /***
   * Gets X coordinate for camera
   * @method getCamLocX
   * @return x
   */
  self.getCamLocX = function() {
  	return mCamera.location.getx();
  }

  /***
   * Gets Y coordinate for camera
   * @method getCamLocY
   * @return y
   */
  self.getCamLocY = function() {
  	return mCamera.location.gety();
  }

  /***
   * Gets Z coordinate for camera
   * @method getCamLocZ
   * @return z
   */
  self.getCamLocZ = function() {
  	return mCamera.location.getz();
  }

  /***
   * Gets coordinates for camera
   * @method getCamLoc
    * @return vector
  */
  self.getCamLoc = function() {
  	return mCamera.location.toArray();
  }

  /***
   * Sets X coordinate for camera
   * @method setCamLocX
   * @param x
   */
  self.setCamLocX = function(x) {
  	if(mCamera.location.getx() != x) {
  	  mCamera.location.setx(x);
      mChangeCamera = true;   		
  	}
    mAutoCamera = false;
  }

  /***
   * Sets Y coordinate for camera
   * @method setCamLocY
   * @param y
   */
  self.setCamLocY = function(y) {
  	if(mCamera.location.gety() != y) {
  	  mCamera.location.sety(y);
      mChangeCamera = true;   		
  	}
    mAutoCamera = false;
  }

  /***
   * Sets Z coordinate for camera
   * @method setCamLocZ
   * @param z
   */
  self.setCamLocZ = function(z) {
  	if(mCamera.location.getz() != z) {
  	  mCamera.location.setz(z);
      mChangeCamera = true;   		
  	}
    mAutoCamera = false;
  }
  
  /***
   * Sets coordinates for camera
   * @method setCamLoc
   * @param vector [x,y,z]
   */
  self.setCamLoc = function(v) {
  	self.setCamLocX(v[0]);
  	self.setCamLocY(v[1]);
  	self.setCamLocZ(v[2]);
  }

  /***
   * Sets tilt for camera
   * @method setCamTilt
   * @param tilt (degrees)
   */
  self.setCamTilt = function(t) {
  	if(mCamera.tilt != t) {
  	  mCamera.tilt = t;
      mChangeCamera = true;   		
  	}
  }

  /***
   * Gets tilt for camera
   * @method getCamTilt
   * @return tilt
   */
  self.getCamTilt = function() {
  	return mCamera.tilt;
  }
  
  /***
   * Sets azimuth for camera
   * @method setCamAzimuth
   * @param azimuth (degrees)
   */
  self.setCamAzimuth = function(az) {
  	if(mCamera.azimuth != az) {
  	  mCamera.azimuth = az;
      mChangeCamera = true;   		
  	}
  }

  /***
   * Gets azimuth for camera
   * @method getCamAzimuth
   * @return azimuth
   */
  self.getCamAzimuth = function() {
  	return mCamera.azimuth;
  }

  /***
   * Sets altitude for camera
   * @method setCamAltitude
   * @param altitude (degrees)
   */
  self.setCamAltitude = function(al) {
  	if(mCamera.altitude != al) {
  	  mCamera.altitude = al;
      mChangeCamera = true;   		
  	}
  }

  /***
   * Gets altitude for camera
   * @method getCamAltitude
   * @return altitude
   */
  self.getCamAltitude = function() {
  	return mCamera.altitude;
  }

  /***
   * Gets X focus for camera
   * @method getCamFocusX
   * @return x
   */
  self.getCamFocusX = function() {
  	return mCamera.focus.getx();
  }

  /***
   * Gets Y focus for camera
   * @method getCamFocusY
   * @return y
   */
  self.getCamFocusY = function() {
  	return mCamera.focus.gety();
  }

  /***
   * Gets Z focus for camera
   * @method getCamFocusZ
   * @return z
   */
  self.getCamFocusZ = function() {
  	return mCamera.focus.getz();
  }

  /***
   * Gets focus for camera
   * @method getCamFocus
   * @return vector
   */
  self.getCamFocus = function() {
  	mCamera.focus.toArray();
  }

  /***
   * Sets X focus for camera
   * @method setCamFocusX
   * @param x
   */
  self.setCamFocusX = function(x) {
  	if(mCamera.focus.getx() != x) {
  	  mCamera.focus.setx(x);
      mChangeCamera = true;   		
  	}
    mAutoCamera = false;
  }

  /***
   * Sets Y focus for camera
   * @method setCamFocusY
   * @param y
   */
  self.setCamFocusY = function(y) {
  	if(mCamera.focus.gety() != y) {
  	  mCamera.focus.sety(y);
      mChangeCamera = true;   		
  	}
    mAutoCamera = false;
  }

  /***
   * Sets Z focus for camera
   * @method setCamFocusZ
   * @param z
   */
  self.setCamFocusZ = function(z) {
  	if(mCamera.focus.getz() != z) {
  	  mCamera.focus.setz(z);
      mChangeCamera = true;   		
  	}
    mAutoCamera = false;
  }
  
  /***
   * Sets focus for camera
   * @method setCamFocus
   * @param vector [x,y,z]
   */
  self.setCamFocus = function(v) {
  	self.setCamFocusX(v[0]);
  	self.setCamFocusY(v[1]);
  	self.setCamFocusZ(v[2]);
  }

  /***
   * Gets X Up Vector for camera
   * @method getCamUpVectorX
   * @return x
   */
  self.getCamUpVectorX = function() {
  	return mCamera.upvector.getx();
  }

  /***
   * Gets Y Up Vector for camera
   * @method getCamUpVectorY
   */
  self.getCamUpVectorY = function() {
  	return mCamera.upvector.gety();
  }

  /***
   * Gets Z Up Vector for camera
   * @method getCamUpVectorZ
   * @return z
   */
  self.getCamUpVectorZ = function() {
  	return mCamera.upvector.getz();
  }

  /***
   * Gets Up Vector for camera
   * @method getCamUpVector
   * @return vector [x,y,z]
   */
  self.getCamUpVector = function() {
  	mCamera.upvector.toArray();
  }

  /***
   * Sets X Up Vector for camera
   * @method setCamUpVectorX
   * @param x
   */
  self.setCamUpVectorX = function(x) {
  	if(mCamera.upvector.getx() != x) {
  	  mCamera.upvector.setx(x);
      mChangeCamera = true;   		
  	}
  }

  /***
   * Sets Y Up Vector for camera
   * @method setCamUpVectorY
   * @param y
   */
  self.setCamUpVectorY = function(y) {
  	if(mCamera.upvector.gety() != y) {
  	  mCamera.upvector.sety(y);
      mChangeCamera = true;   		
  	}
  }

  /***
   * Sets Z Up Vector for camera
   * @method setCamUpVectorZ
   * @param z
   */
  self.setCamUpVectorZ = function(z) {
  	if(mCamera.upvector.getz() != z) {
  	  mCamera.upvector.setz(z);
      mChangeCamera = true;   		
  	}
  }
  
  /***
   * Sets Up Vector for camera
   * @method setCamUpVector
   * @param vector [x,y,z]
   */
  self.setCamUpVector = function(v) {
  	self.setCamUpVectorX(v[0]);
  	self.setCamUpVectorY(v[1]);
  	self.setCamUpVectorZ(v[2]);
  }

  /***
   * Gets auto camera
   * @method getAutoCamera
   * @return boolean
   */
  self.getAutoCamera = function() {
  	return mAutoCamera;
  }

  /**
   * Sets auto camera
   * @method setAutoCamera
   * @param boolean
   */
  self.setAutoCamera = function(auto) {
  	if(mAutoCamera != auto) {
  		mAutoCamera = auto;
  		mChangeCamera = true; 
  	}
  }

  /***
   * Gets zoom rate
   * @method getZoomRate
   * @return double
   */
  self.getZoomRate = function() {
  	return mZoomRate;
  }

  /***
   * Sets zoom rate
   * @method setZoomRate
   * @param double
   */
  self.setZoomRate = function(rate) {
	mZoomRate = rate;
  }

  /***
   * Set lights
   * @method setLights
   * @param array of array
   */
  self.setLights = function(lights) {
	mLights = lights;
  }
  
  /***
   * Gets lights
   * @method getLights
   * @return array of array
   */
  self.getLights = function() {
  	return mLights;
  }
  
  // ----------------------------------------
  // Size of the element
  // ----------------------------------------

  /***
   * Set the size along the X axis of the elements
   * @method setSizeX
   * @param sizeX double
   */
  self.setSizeX = function(sizeX) {
    if (mSizeX !== sizeX) { 
      mSizeX = sizeX;
      mChangeWorld = true; 
    }  	 
  };

  /***
   * Get the size along the X coordinate of the elements
   * @method getSizeX
   * @return double
   */
  self.getSizeX = function() { 
    return mSizeX; 
  };

  /***
   * Set the size along the Y axis of the elements
   * @method setSizeY
   * @param sizeY double
   */
  self.setSizeY = function(sizeY) { 
    if (mSizeY !== sizeY) { 
      mSizeY = sizeY;
      mChangeWorld = true; 
    }  	 
  };

  /***
   * Get the size along the Y coordinate of the elements
   * @method getSizeY
   * @return double
   */
  self.getSizeY = function() { 
    return mSizeY; 
  };

  /***
   * Set the size along the Z axis of the elements
   * @method setSizeZ
   * @param sizeZ double
   */
  self.setSizeZ = function(sizeZ) { 
    if (mSizeZ !== sizeZ) { 
      mSizeZ = sizeZ;
      mChangeWorld = true; 
    }  	 
  };

  /***
   * Get the size along the Z coordinate of the elements
   * @method getSizeZ
   * @return double
   */
  self.getSizeZ = function() { 
    return mSizeZ; 
  };

  /***
   * Set the size of the elements
   * @method setSize
   * @param position double[] an array of dimension 3 or an object with {x,y,z} properties
   */
  self.setSize = function(size) {
    self.setSizeX(size[0]);
    self.setSizeY(size[1]);
    self.setSizeZ(size[2]);
  };

  /***
   * Get the sizes of the elements
   * @method getSize
   * @return double[]
   */
  self.getSize = function() {
    return [self.getSizeX(), self.getSizeY(), self.getSizeZ()];
  };
  
  /***
   * Set background image url 
   * @param url
   */
  self.setImageUrl = function(url) {
  	mImageBackground = true;
  	
  	if (self.getResourcePath != null) {
  	  url = self.getResourcePath(url);
  	}
  	else {
  		console.log ("No getResourcePath function for panel. Texture = " + url);
  	}
  	self.getGraphics().getStyle().setBackgroundImage(url);
  }
  
  /***
   * Whether background image exists
   * @return boolean
   */
  self.isImageUrl = function() {
  	return mImageBackground;
  }
  
  // ----------------------------------------
  // Decorations and elements
  // ----------------------------------------

  /***
   * Adds a decoration to the panel. Decorations are drawn before any other elements.
   * @method addDecoration
   * @param drawable decoration element
   * @param position integer
   */
  self.addDecoration = function(drawable, position) {
  	EJSS_TOOLS.addToArray(mDecorations, drawable, position);
    if (drawable.setPanel) // set this panel to decoration element 
    	drawable.setPanel(self);	 
    return self;
  };

  /***
   * Removes a decoration
   * @method removeDecoration
   * @param drawable decoration element
   */
  self.removeDecoration = function(drawable) {
    EJSS_TOOLS.removeFromArray(mDecorations,drawable);
    if (drawable.setPanel) // remove this panel to decoration element 
    	drawable.setPanel(null);
    return self;
  };

  /***
   * Set visible decoration shown basic axis
   * @method setVisibleDecorationBasicAxis
   * @param boolean
   */
  self.setVisibleDecorationBasicAxis = function(visible) {
  	mBasicAxisXDecoration.setVisible(visible);
  	mBasicAxisYDecoration.setVisible(visible);
  	mBasicAxisZDecoration.setVisible(visible);
  }

  /***
   * Set visible decoration shown traditional axis
   * @method setVisibleDecorationAxis
   * @param boolean
   */
  self.setVisibleDecorationAxis = function(visible) {
  	mAxisXDecoration.setVisible(visible);
  	mAxisYDecoration.setVisible(visible);
  	mAxisZDecoration.setVisible(visible);
  }

  /***
   * Set visible decoration shown box
   * @method setVisibleDecorationBox
   * @param boolean
   */
  self.setVisibleDecorationBox = function(visible) {
  	mBoxDecoration.setVisible(visible);  	
  }

  /***
   * Set visible a specific type of decoration
   * @method setDecorationType
   * @param type "NONE", "AXES", "CUBE", "CENTERED_AXIS"
   */
  self.setDecorationType = function(type) {
    if (typeof(type)=='string') {
      switch (type) {
        default : 
        case "NONE" : type = 0; break; 
        case "AXES" : type = 1; break; 
        case "CUBE" : type = 2; break; 
        case "CENTERED_AXES" : type = 3; break; 
      }    
    }
    switch(type) {
      case 0 : 
        self.setVisibleDecorationBox(false); 
        self.setVisibleDecorationBasicAxis(false);
        self.setVisibleDecorationAxis(false);
        break;
      case 1 : 
        self.setVisibleDecorationBox(false); 
        self.setVisibleDecorationBasicAxis(false);
        self.setVisibleDecorationAxis(true);
        break;
      default:
      case 2 : 
        self.setVisibleDecorationBox(true); 
        self.setVisibleDecorationBasicAxis(false);
        self.setVisibleDecorationAxis(false);
        break;
      case 3 : 
        self.setVisibleDecorationBox(false); 
        self.setVisibleDecorationBasicAxis(true);
        self.setVisibleDecorationAxis(false);
        break;
     }
   }

  /***
   * Add a element to the panel. Elements are asked to draw themselves
   * whenever the panel needs to render. For this purpose, they will receive a
   * calls to draw().
   * Elements are reported of changes in the world coordinates of the panel, in case
   * they need to recalculate themselves.
   * @method addElement
   * @param element Element
   * @param position int
   */
  self.addElement = function(element, position) {
    EJSS_TOOLS.addToArray(mElements,element,position);
    // set this panel to decoration element
    element.setPanel(self);
	if (element.dataCollected) mCollectersList.push(element);
	// elements list has changed
	mElementsChanged = true;		
  };
  
  /***
   * Remove a element to the panel.
   * @method removeElement
   * @param element Element 
   */
  self.removeElement = function(element) {
    EJSS_TOOLS.removeFromArray(mElements,element);
    element.setPanel(null);
	if (element.dataCollected) EJSS_TOOLS.removeFromArray(mCollectersList, element);
	// elements list has changed
	mElementsChanged = true;
  };

  /***
   * Return the array of a elements.
   * @method getElements
   * @return Elements 
   */
  self.getElements = function() {
    return mElements;
  };
    
  /***
   * Return the position of a element.
   * @method indexOfElement
   * @param element Element
   * @return integer 
   */
  self.indexOfElement = function(element) {
    return mElements.indexOf(element);
  };
           
  // ----------------------------------------
  // Drawing functions
  // ----------------------------------------

  /***
   * Get the type of projection
   * @method getProjection
   * @return type 
   */
  self.getProjection = function() {
  	return mProjection;
  }
  
  /***
   * Set the type of projection
   * @method setProjection
   * @return type "PLANAR_XY", "PLANAR_XZ", "PLANAR_YZ", "PERSPECTIVE_OFF", "PERSPECTIVE_ON"
   */
  self.setProjection = function(proj) {
    if (typeof(proj)=='string') {
      switch (proj) {
        case "PLANAR_XY" : proj = 0; break;
      	case "PLANAR_XZ" : proj = 1; break; 
      	case "PLANAR_YZ" : proj = 2; break; 
      	case "PERSPECTIVE_OFF" : proj = 3; break; // PLANAR_ZY
        default : 
      	case "PERSPECTIVE_ON" : proj = 4; break; // PLANAR_ZY
      }    
    }
    if(mProjection != proj) {
	    switch (proj) {
	      case 0 : // PLANAR_XY
	  			mProjection = 0;
	  			self.setOrthographic(true);
	  			self.setCamUpVector([0,1,0]);
	  			self.setCamAzimuth(0);
	  			self.setCamAltitude(0);
	  			self.setCamTilt(0);
	  			break; 
	      case 1 : // PLANAR_XZ
	  			mProjection = 1;
	  			self.setOrthographic(true);
	  			self.setCamUpVector([0,0,-1]);
	  			self.setCamAzimuth(0);
	  			self.setCamAltitude(0);
	  			self.setCamTilt(0);
	  			break; 
	      case 2 : // PLANAR_YZ
	  			mProjection = 2;
	  			self.setOrthographic(true);
	  			self.setCamUpVector([0,1,0]);
	  			self.setCamAzimuth(0);
	  			self.setCamAltitude(0);
	  			self.setCamTilt(0);
	  			break; 
	      case 3 : // PLANAR_ZY
	  			mProjection = 3;
	  			self.setOrthographic(true);
	  			self.setCamUpVector([0,0,1]);
	  			self.setCamAzimuth(0);
	  			self.setCamAltitude(0);
	  			self.setCamTilt(0);
	  			break; 
	      default : 
	      case 4 : // PLANAR_ZY
	  			mProjection = 4;
	  			self.setOrthographic(false);
	  			self.setCamUpVector([0,0,1]);
	  			self.setCamAzimuth(0);
	  			self.setCamAltitude(0);
	  			self.setCamTilt(0);
	  			break; 
	    }        	
	    mChangeWorld = true;
    }
  }

  /***
   * Whether the projection is orthographic
   * @method getOrthographic
   * @return boolean
   */
  self.getOrthographic = function() {
  	return mOrthographic;
  }
  
  /***
   * Set orthographic projection
   * @method setOrthographic
   * @param boolean
   */
  self.setOrthographic = function(orth) {
    if (mOrthographic !== orth) { 
      mOrthographic = orth;
      mChangeWorld = true; 
    }  	 
  }

  /***
   * Get field of view in non-orthographic projection
   * @method getFOV
   * @param int
   */
  self.getFOV = function() {
  	return mFOV;
  }
  
  /***
   * Set field of view in non-orthographic projection
   * @method setFOV
   * @return int
   */
  self.setFOV = function(fov) {
    if (mFOV !== fov) { 
      mFOV = fov;
      mChangeWorld = true; 
    }  	 
  }

  /***
   * Get far plane in non-orthographic projection
   * @method getFar
   * @param number
   */
  self.getFar = function() {
  	return mFar;
  }

  /***
   * Set far plane in non-orthographic projection
   * @method setFar
   * @return number
   */
  self.setFar = function(far) {
    if (mFar !== far) { 
      mFar = far;
      mChangeWorld = true; 
    }  	 
  }

  /***
   * Get near plane in non-orthographic projection
   * @method getNear
   * @param number
   */
  self.getNear = function() {
  	return mNear;
  }
  
  /***
   * Set near plane in non-orthographic projection
   * @method setNear
   * @return number
   */
  self.setNear = function(near) {
    if (mNear !== near) { 
      mNear = near;
      mChangeWorld = true; 
    }  	 
  }

  /***
   * Reset the scene
   * @method reset
   */
  self.reset = function() {
  	// dimensions for orthographic view
  	var dim = {};
  	if(mOrthographic) {
 		// using world
		var aspect = mGraphics.getAspect(); // width / height
		var size = new Vector(mSizeX,mSizeY,mSizeZ);
		var min = new Vector(mWorld.xminPreferred,mWorld.yminPreferred,mWorld.zminPreferred).multiply(size);
		var max = new Vector(mWorld.xmaxPreferred,mWorld.ymaxPreferred,mWorld.zmaxPreferred).multiply(size);
		var diff = max.subtract(min);

        switch (mProjection) {
          case 0: // "PLANAR_XY"
			var ratio = (diff.y/diff.x) * aspect;
            if(ratio>=1.0) {
            	var width = (diff.x * ratio) / 2; var height = diff.y / 2;
			} else {
            	var width = diff.x / 2; var height = (diff.y * (1 / ratio - 1)) / 2;
          	} 
          	break; 
          case 1: // "PLANAR_XZ"
			var ratio = (diff.z/diff.x) * aspect;
            if(ratio>=1.0) {
            	var width = (diff.x * ratio) / 2; var height = diff.z / 2;
			} else {
            	var width = diff.x / 2; var height = (diff.z * (1 / ratio - 1)) / 2;
          	} 
          	break; 
          case 2: // "PLANAR_YZ"
			var ratio = (diff.y/diff.z) * aspect;
            if(ratio>=1.0) {
            	var width = (diff.z * ratio) / 2; var height = diff.y / 2;
			} else {
            	var width = diff.z / 2; var height = (diff.y * (1 / ratio - 1)) / 2;
          	} 
            break;
       	  default: // "PERSPECTIVE_OFF" "PLANAR_ZY"
			var ratio = (diff.z/diff.y) * aspect;
            if(ratio>=1.0) {
            	var width = (diff.y * ratio) / 2; var height = diff.z / 2;
			} else {
            	var width = diff.y / 2; var height = (diff.z * (1 / ratio - 1)) / 2;
          	} 
            break;
		}					
		// with margin
		var rate = 1.10;
   		dim = {left: -width*rate, right: width*rate, bottom: -height*rate, top: height*rate}; 
  	} // else pespective view based on fov

    mGraphics.reset(mOrthographic, dim, mFOV, mNear, mFar);
  }
  
  /***
   * Render the scene
   * @method render
   */
  self.render = function() {
    if(mElementsChanged || mChangeWorld)  // whether elements added or removed, reset the scene 
		self.reset();

    // check for data collection
	for (var i = 0, n = mCollectersList.length; i < n; i++)
		mCollectersList[i].dataCollected();
			
    if (mPanelChanged || mElementsChanged) // whether style panel changed or reseted    	    	 
      	mGraphics.drawPanel(self);

	// change decorations
    if(mChangeWorld) {
		// listener for decorations
		for (i = 0, n = mDecorations.length; i < n; i++) {
			if (mDecorations[i].panelChangeListener)
				mDecorations[i].panelChangeListener("bounds");
		}    	

		// auto change camera position	
		if(mAutoCamera) {
			// focus
			var size = new Vector(mSizeX,mSizeY,mSizeZ);
			var min = new Vector(mWorld.xminPreferred,mWorld.yminPreferred,mWorld.zminPreferred).multiply(size);
			var max = new Vector(mWorld.xmaxPreferred,mWorld.ymaxPreferred,mWorld.zmaxPreferred).multiply(size);
	  		mCamera.focus = max.add(min).divide(2);			
			if(!mOrthographic) { // Axis YZ
				// location (it depends on the fov)
				var aspect = mGraphics.getAspect();
				var diff = max.subtract(min);
            	var apply = (diff.z*aspect>diff.y)?diff.z:diff.y; 				
				var near = 1.1 * (apply/2) / Math.tan(mFOV*Math.PI/360); // near, but far 1.1
				mCamera.location = mCamera.focus.clone();
				mCamera.location.x = max.x + near;
			} else {
				mCamera.location = mCamera.focus.clone();
		        switch (mProjection) {
		          case 0: // "PLANAR_XY"
					mCamera.location.z = mWorld.zmaxPreferred*mSizeZ*4; // a little far		          
		          	break; 
		          case 1: // "PLANAR_XZ"
   					mCamera.location.y = mWorld.ymaxPreferred*mSizeY*4; // a little far
		          	break; 
		          case 2: // "PLANAR_YZ"
		            mCamera.location.x = mWorld.xmaxPreferred*mSizeX*4; // a little far
		            break;
		          default: // "PERSPECTIVE_OFF" 
					mCamera.location.x = mWorld.xmaxPreferred*mSizeX*4; // a little far
		          	break;
				}				
			}
	    }
    }
        	
    // check upvector
    if(mCamera.upvector.cross(mCamera.location.subtract(mCamera.focus)).length() == 0) {
    	// upvector and camera direction are paralell 
    	mCamera.upvector = mCamera.upvector.rotate(); // for upvector (0,0,1) to (1,0,0)
    }
    
    // locate camera
    // if(mChangeCamera)
    	mGraphics.putCamera(mOrthographic,mCamera);

    // draw visible elements
   	mGraphics.draw(mDecorations); // draw the decorations
	mGraphics.draw(mElements);
	
	// set changed to false
	mChangeWorld = false;
	mPanelChanged = false;
	mChangeCamera = false;
	mElementsChanged = false;	
	for (var i=0, n=mElements.length; i<n; i++) {
		mElements[i].setProjChanged(false);
		mElements[i].setMeshChanged(false);
	} 
  };

  // ----------------------------------------
  // Interaction
  // ----------------------------------------
  
  /***
   * Whether the panel should respond to user interaction
   * @method setEnabled
   * @param enabled boolean
   */
  self.setEnabled = function(enabled) { 
  	mInteraction.setEnabled(enabled);
  };
 
  /***
   * How the panel should respond to user interaction
   * @method setDraggable
   * @param draggable "NONE", "ANY", "AZIMUTH", "ALTITUDE"
   */
  self.setDraggable = function(draggable) {
    if (typeof(draggable)=='string') {
      switch (draggable) {
        default : 
        case "NONE" : draggable = 0; break; 
        case "ANY" : draggable = 1; break; 
        case "AZIMUTH" : draggable = 2; break; 
        case "ALTITUDE" : draggable = 3; break;
      }    
    }
	mDraggable = draggable;
  };

  /***
   * Return panel interaction
   * @method getPanelInteraction
   * @return panel interaction
   */
  self.getPanelInteraction = function() { 
  	return mInteraction;
  };
 
 
  /***
   * Returns the controller object
   * @method getController
   * @return Controller
   */
  self.getController = function () {
    return mController;
  };

  /***
   * Set the controller
   * @method setController
   * @param Controller
   */
  self.setController = function (controller) {
    mController = controller;
  };
    

  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  self.getInteraction = function() {
    return mInteraction;
  };

  self.registerProperties = function(controller) {
    EJSS_DRAWING3D.DrawingPanel.registerProperties(self,controller);
  };
  
  self.getOnDragHandler = function() {
  	if(mDraggable > 0) {
	  	var deltas = self.getPanelInteraction().getInteractionDeltas();

		// delta[0] move right/left
        switch (mProjection) {
          case 0: // "PLANAR_XY"
		    if(mDraggable == 2 || mDraggable == 1) {
			    mCamera.altitude += deltas[0];
		    }
          	break; 
          case 1: // "PLANAR_XZ"
		    if(mDraggable == 2 || mDraggable == 1) {	    
		    	mCamera.azimuth -= deltas[0];
			}  		
          	break; 
          case 2: // "PLANAR_YZ"
		    if(mDraggable == 2 || mDraggable == 1) {
			    mCamera.altitude += deltas[0];
		    }
          	break; 
          default: // "PERSPECTIVE_OFF": 
		    if(mDraggable == 2 || mDraggable == 1) {
			    mCamera.azimuth += deltas[0];
		    }
		    break;
		}

		// delta[1] move up/down
        switch (mProjection) {
          case 0: // "PLANAR_XY"
		    if(mDraggable == 3 || mDraggable == 1) {	    
		    	mCamera.tilt += deltas[1];
			}  		
          	break; 
          case 1: // "PLANAR_XZ"
		    if(mDraggable == 3 || mDraggable == 1) {	    
		    	mCamera.tilt += deltas[1];
			}  		
          	break; 
          case 2: // "PLANAR_YZ" 
		    if(mDraggable == 3 || mDraggable == 1) {	    
		    	mCamera.azimuth -= deltas[1];
			}  		
		    break;
          default: // "PERSPECTIVE_OFF"
		    if(mDraggable == 3 || mDraggable == 1) {	    
		    	mCamera.altitude += deltas[1];
			}  		
		    break;
		}
		
  	}

	self.getController().propertiesChanged("CameraAzimuth","CameraAltitude","CameraTilt");  	
  	
  	mChangeCamera = true;
  }     

  self.getOnZoomHandler = function() {
  	var delta = self.getPanelInteraction().getInteractionZoomDelta();
	var eyevector = mCamera.focus.subtract(mCamera.location);
	if(delta > 0) { // move 10% distance from location to focus camera
	  	mCamera.location = mCamera.focus.subtract(eyevector.divide(mZoomRate)); 
	  	mCamera.delta = mZoomRate;
	} else if(delta < 0) {
	  	mCamera.location = mCamera.focus.subtract(eyevector.multiply(mZoomRate));
	  	mCamera.delta = 2.0 - mZoomRate; 
	}
	self.getController().propertiesChanged("CameraLocation","CameraX","CameraY","CameraZ");

  	mChangeCamera = true;
  }     
    
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  mStyle.setFillColor([0.83,0.85,1]);
  mStyle.setMeshChangeListener(function (change) { mPanelChanged = true; });
  mStyle.setProjChangeListener(function (change) { mPanelChanged = true; });

  // box decoration
  var mBoxDecoration = EJSS_DRAWING3D.box(mName + "box");
  mBoxDecoration.getStyle().setDrawFill(false);
  mBoxDecoration.getStyle().setDrawLines(true);
  mBoxDecoration.getStyle().setLineColor("black");
  mBoxDecoration.setVisible(true);
  mBoxDecoration.setSize([2,2,2]);  
  mBoxDecoration.panelChangeListener = function(event) {
	if (event == "bounds") {
		var posx = (mWorld.xmaxPreferred + mWorld.xminPreferred)/2;
		var posy = (mWorld.ymaxPreferred + mWorld.yminPreferred)/2;
		var posz = (mWorld.zmaxPreferred + mWorld.zminPreferred)/2;
		var sx = Math.abs(mWorld.xmaxPreferred - mWorld.xminPreferred);
		var sy = Math.abs(mWorld.ymaxPreferred - mWorld.yminPreferred);
		var sz = Math.abs(mWorld.zmaxPreferred - mWorld.zminPreferred);		
		mBoxDecoration.setPosition([posx,posy,posz]);
		mBoxDecoration.setSize([sx,sy,sz]);
	}
  };  
  self.addDecoration(mBoxDecoration, 0);

  // axis decoration
  var mAxisXDecoration = EJSS_DRAWING3D.arrow(mName + "axisX");
  mAxisXDecoration.setColor("red");
  mAxisXDecoration.setVisible(false);
  mAxisXDecoration.panelChangeListener = function(event) {
	if (event == "bounds") {
  		mAxisXDecoration.setPosition([mWorld.xminPreferred,mWorld.yminPreferred,mWorld.zminPreferred]);
		var sx = Math.abs(mWorld.xmaxPreferred - mWorld.xminPreferred);
  		mAxisXDecoration.setSize([sx,0,0]);
  	}
  };  
  self.addDecoration(mAxisXDecoration, 0);

  var mAxisYDecoration = EJSS_DRAWING3D.arrow(mName + "axisY");
  mAxisYDecoration.setPosition([0,0,0]);
  mAxisYDecoration.setSize([0,1,0]);
  mAxisYDecoration.setColor("green");
  mAxisYDecoration.setVisible(false);
  mAxisYDecoration.panelChangeListener = function(event) {
	if (event == "bounds") {
  		mAxisYDecoration.setPosition([mWorld.xminPreferred,mWorld.yminPreferred,mWorld.zminPreferred]);
		var sy = Math.abs(mWorld.ymaxPreferred - mWorld.yminPreferred);  		
  		mAxisYDecoration.setSize([0,sy,0]);
  	}
  };  
  self.addDecoration(mAxisYDecoration, 0);

  var mAxisZDecoration = EJSS_DRAWING3D.arrow(mName + "axisZ");
  mAxisZDecoration.setPosition([0,0,0]);
  mAxisZDecoration.setSize([0,0,1]);
  mAxisZDecoration.setColor("blue");
  mAxisZDecoration.setVisible(false);
  mAxisZDecoration.panelChangeListener = function(event) {
	if (event == "bounds") {		
  		mAxisZDecoration.setPosition([mWorld.xminPreferred,mWorld.yminPreferred,mWorld.zminPreferred]);
		var sz = Math.abs(mWorld.zmaxPreferred - mWorld.zminPreferred);		
  		mAxisZDecoration.setSize([0,0,sz]);
  	}
  };  
  self.addDecoration(mAxisZDecoration, 0);

  // basic axis decoration
  var mBasicAxisXDecoration = EJSS_DRAWING3D.arrow(mName + "baxisX");
  mBasicAxisXDecoration.setPosition([0,0,0]);
  mBasicAxisXDecoration.setSize([1,0,0]);
  mBasicAxisXDecoration.setColor("red");
  mBasicAxisXDecoration.setVisible(false);
  mBasicAxisXDecoration.panelChangeListener = function(event) {
	if (event == "bounds") {
		var sx = Math.abs(mWorld.xmaxPreferred - mWorld.xminPreferred)/2;
  		mBasicAxisXDecoration.setSize([sx,0,0]);
  	}
  };    
  self.addDecoration(mBasicAxisXDecoration, 0);

  var mBasicAxisYDecoration = EJSS_DRAWING3D.arrow(mName + "baxisY");
  mBasicAxisYDecoration.setPosition([0,0,0]);
  mBasicAxisYDecoration.setSize([0,1,0]);
  mBasicAxisYDecoration.setColor("green");
  mBasicAxisYDecoration.setVisible(false);
  mBasicAxisYDecoration.panelChangeListener = function(event) {
	if (event == "bounds") {
		var sy = Math.abs(mWorld.ymaxPreferred - mWorld.yminPreferred)/2;  		
  		mBasicAxisYDecoration.setSize([0,sy,0]);
  	}
  };    
  self.addDecoration(mBasicAxisYDecoration, 0);

  var mBasicAxisZDecoration = EJSS_DRAWING3D.arrow(mName + "baxisZ");
  mBasicAxisZDecoration.setPosition([0,0,0]);
  mBasicAxisZDecoration.setSize([0,0,1]);
  mBasicAxisZDecoration.setColor("blue");
  mBasicAxisZDecoration.setVisible(false);
  mBasicAxisZDecoration.panelChangeListener = function(event) {
	if (event == "bounds") {
		var sz = Math.abs(mWorld.zmaxPreferred - mWorld.zminPreferred)/2;		
  		mBasicAxisZDecoration.setSize([0,0,sz]);
  	}
  };  
  self.addDecoration(mBasicAxisZDecoration, 0);
  
  mInteraction = EJSS_DRAWING3D.panelInteraction(self);
  
  return self;
};






