/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//ConeSet
//---------------------------------

/**
 * Framework for 3D drawing.
 * @module 3Ddrawing 
 */

/**
 * ConeSet
 * @class ConeSet 
 * @constructor  
 */
EJSS_DRAWING3D.ConeSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING3D.ElementSet;
      ElementSet.registerProperties(set,controller); 
      
    }

};


/**
 * Creates a set of cones
 * @method coneSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING3D.coneSet = function (mName) {
  var self = EJSS_DRAWING3D.elementSet(EJSS_DRAWING3D.cone,mName);

  // Static references
  var ConeSet = EJSS_DRAWING3D.ConeSet;		// reference for ConeSet
  
  self.registerProperties = function(controller) {
    ConeSet.registerProperties(self,controller);
  };

  return self;
};
