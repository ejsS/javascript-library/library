/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia and Félix J. García 
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for Animations
 * @module core
 */

var EJSS_CORE = EJSS_CORE || {};

/**
 * Creates a recorder for a model.
 * @method recorder
 */
EJSS_CORE.recorder = function(model) {
	var self = {};

	var mIsStarted = false;

	var mIsRecUserEvents = true;	// record user events (click event)	
	var mIsRecModelCalls = true;	// record model calls (play, stop, ...)
	var mIsRecInteractions = true;	// record view interactions

	var mVars = [];
	var mRecords = [];

	self.isStarted = function() {
		return mIsStarted;
	};

	self.setRecUserEvents = function(value) {
		mIsRecUserEvents = value;
	};

	self.isRecUserEvents = function() {
		return mIsRecUserEvents;
	};

	self.setRecModelCalls = function(value) {
		mIsRecModelCalls = value;
	};

	self.isRecModelCalls = function() {
		return mIsRecModelCalls;
	};

	self.setRecInteractions = function(value) {
		mIsRecInteractions = value;
	};

	self.isRecInteractions = function() {
		return mIsRecInteractions;
	};

	self.selectVars = function(vars) {
		mVars = vars.slice();
	};

	self.clear = function() {
		mRecords = [];
		model.getView().clearRegInteractions();
	};

	self.stop = function() {
		model.getView().unregisterInteractions();
		mIsStarted = false;		
		document.removeEventListener('mousedown', self.addEventRecord);
		document.removeEventListener('touchstart', self.addEventRecord);
		return self.getRecords();
	};
	
	self.start = function() {	
		if (!mIsStarted) {
			mIsStarted = true;
			mRecords = [];
			
			if (mIsRecInteractions) {
				model.getView().registerInteractions(false, self.addAutoRecord);
				model.getView().setShortRegInteractions(true);				
			}
						
			if(mIsRecUserEvents) {
				document.addEventListener('mousedown', self.addEventRecord);
				document.addEventListener('touchstart', self.addEventRecord);	
						
			}		
		}
	};
	    
	self.addEventRecord = function(event) {
		self.addRecord('_event.' + event.type, mVars);		
	};
	
	self.addAutoRecord = function(action, before) {
		if (before) {
			var tmp = {};
			if(typeof action['action'] != 'undefined') {
				tmp['type'] = 'action';
				tmp['name'] = action['action']; 
			} else {
				tmp['type'] = 'property';
				tmp['name'] = action['property']; 
				tmp['value'] = action['data']; 
			}
			self.addRecord('_view.'+action['element'], mVars, tmp);					
		} else {
			// last record
			var serial = model._userSerialize();
			mRecords[mRecords.length-1]['after'] = {};
			
			// select vars
			if (mVars.length == 0) {
				mRecords[mRecords.length-1]['after'] = serial;
			} else {
				for(v in mVars) {
					var e = mVars[v];
					mRecords[mRecords.length-1]['after'][e] = serial[e];
				}			
			}			
		}
	};
	 
	self.addRecord = function(tag, vars, tmp) {
		if (typeof vars == 'undefined')
			var vars = mVars;
			
		var serial = model._userSerialize();
		var record = {};
		record['before'] = {};
		
		// select vars
		if (vars.length == 0) {
			record['before'] = serial;
		} else {
			for(v in vars) {
				var e = vars[v];
				record['before'][e] = serial[e];
			}			
		}
		record["timeStamp"] = Date.now();
		record["tag"] = tag;
		if (typeof tmp != 'undefined')
			for(var key in tmp) record[key] = tmp[key];
		mRecords.push(record);		
	};

	self.getRecords = function() {		
		// console.log(JSON.stringify(mRecords));
		return mRecords;
	};
	
	self.downloadFile = function() {		
		EJSS_TOOLS.File.downloadText("records.json", JSON.stringify(self.getRecords()));
	};
	
	self.sendRecords = function(callback_ok, callback_error) {
		// check something to send
		if (mRecords.length == 0) return;
		// build JSON
		var json = JSON.stringify(mRecords, function(key, value) {
			if(typeof value == 'number') {
				if (isNaN(value)) value = "__NaN";
				else if (!isFinite(value)) value = "__Infinity";
			}
			return value;
		});
		// empty records
		mRecords = [];
		// send JSON to Moodle plugin
		var http = new XMLHttpRequest();
		var params = "view_id="+_record_view_id+"&info="+encodeURIComponent(json)+"&user_id="+_record_user_id;
		http.open("POST", _record_moodle_url + "?wstoken=" + _record_wstoken + "&wsfunction=" + _record_wsfunction, true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.onreadystatechange = function() {
			if(http.readyState == 4 && http.status == 200) {
				if(callback_ok) callback_ok();
			} else {
				if(callback_error) callback_error();
			}
		};
		http.send(params);
	};

	// ------------------------------------------------------------------------
	// Recording with Moodle server
	// ------------------------------------------------------------------------

	// check moodle server and plugin LA EjsS Moodle plugin
	var _record_url = new URL(document.location);
	var _record_view_id = (typeof _record_url.searchParams != 'undefined')?_record_url.searchParams.get('view_id'):false;
	if (_record_view_id) {
		var _record_user_id = _record_url.searchParams.get('user_id');
		var _record_wstoken = _record_url.searchParams.get('wstoken');
		var _record_moodle_url = _record_url.searchParams.get('url');
		var _record_wsfunction = _record_url.searchParams.get('wsfunction');

		// check Moodle server
		if(_record_wsfunction == 'report_get_lainteractions') {
			// sending info to moodle
			setInterval(function(){
				self.sendRecords();
			}, 3000);
		}
	}

	// ------------------------------------------------------------------------
	// Extending model functions
	// ------------------------------------------------------------------------
	 
	model.____play = model.play;   
	model.play = function() {
		if(mIsStarted && mIsRecModelCalls) self.addRecord("play");
		model.____play();
	};
	
	model.____pause = model.pause;   
	model.pause = function() {
		if(mIsStarted && mIsRecModelCalls) self.addRecord("pause");
		model.____pause();
	};
	
	model.____reset = model.reset;   
	model.reset = function(reuseview) {
		if(mIsStarted && mIsRecModelCalls) self.addRecord("reset");
		model.____reset();	
    };

	model.____initialize = model.initialize;
	model.initialize = function() {
		if(mIsStarted && mIsRecModelCalls) self.addRecord("initialize");
		model.____initialize();
    };
    
//	model.____step = model.step;
//	model.step = function() {
//		if(mIsStarted && mIsRecModelCalls) self.addRecord("step");
//		model.____step();
//    }
    
    return self;
	
};

