/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for Animations based on a Model-View-Controller (MVC) architecture
 * @module core
 */

var EJSS_CORE = EJSS_CORE || {};

/**
 * Model
 * @class Model 
 * @constructor  
 */
EJSS_CORE.Model = {};

/**
 * Creates an animation for a model.
 * The model must implement the function:
 * - stepModel()  : steps the model for each animation frame
 * and have a view.
 * @method createAnimation
 */
EJSS_CORE.createAnimation = function() {
	var self = {};
	var MINIMUM_DELAY = 1; 

	// --- Configuration variables
	var mStepsPerDisplay = 1;		// number of model steps before refreshing the view
	var mDelay = MINIMUM_DELAY;		// user-defined delay between steps
	var mMinimumDelay = 10;			// minimum delay between steps (ms)
	var mRunAlways = false;			// run simulation always

	var mAbortSPDLoop = false; 		// abort the Steps Per Display loop in the step() function
	var mShouldUpdateView = true;	// should update view each step
	var mAutoplay = false;			// auto playing

	var mOnloadFunc;				// function to first run onload

	// --- Implementation variables
	var mView = null;
	var mCurrentTimeOut;			// ID of current SetTimeout 
	var mShouldBreak = false;		// break flag for updating model
	var mIsPlaying = false;			// if playing, model must step
	var mRunCount = 0;				// run calls counter	
	var mLastTime = +new Date();	// last step date
	var mRunTime = +new Date();		// last run call date
	var mFocus = true;				// focus in simulation 
	
	var mPendingTasks = []; // array of functions to call when the animation is idle. 
	
	// ------------------------------------------------------------------------
	// To be modified or overwritten by subclasses
	// ------------------------------------------------------------------------
	
	/**
	 * Serializes simulation variables to JSON.
	 * Should be implemented by simulation.
	 * @method _userSerialize
	 * @return JSON
	 */	
	self._userSerialize = function() {
		return {};
	}

	/**
	 * Reads simulation variables from JSON. 
	 * Should be implemented by simulation.
	 * @method _readParameters
	 * @param json JSON
	 */	
	self._readParameters = function(json) {}
		
	// ------------------------------------------------------------------------
	// This part is used by the model subclass at creation time
	// ------------------------------------------------------------------------

	/**
	 * Sets the view for this model.
	 * @method setView
	 */
	self.setView = function(view) {
		mView = view;
		mView._setReportInteractionMethod(function() {
			if (!mIsPlaying) {
				if (mView._readInteractions(mRunCount, mRunTime, mIsPlaying)) {// The user interacted with the view
					self.automaticResetSolvers();
					self.update();
				}
			}
		});
	};

	/**
	 * Returns the view for this model.
	 * @method getView
	 * @return the view of this model
	 */
	self.getView = function() {
		return mView;
	};
  
	// ------------------------------------------------------------------------
	// Serialize and Unserialize
	// ------------------------------------------------------------------------

	/**
	 * Serialize model and view
	 * @method serialize
	 * @return json string with format {model: {...}, view: {...}}
	 */
  	self.serialize = function() {
  		var encodedModel = {};
  		encodedModel = self._userSerialize();
		var encodedView = mView.serialize();
		
		var encoded = {model: encodedModel, view: encodedView};
		
		return JSON.stringify(encoded, function(key, value) {
			if(typeof value == 'number') {
				if (isNaN(value)) value = "__NaN";
				else if (!isFinite(value)) value = "__Infinity";
			}
			return value;
		});				
  	};

	/**
	 * Unserialize model
	 * @method unserialize
	 * @param json string
	 */
  	self.unserialize = function(json) {
		var encoded = JSON.parse(json, function(key, value) {
			if(value == "__NaN") value = NaN;
			else if (value == "__Infinity") value = Infinity;
			return value;
		});		

		if(encoded.model) {
	  		var encodedModel = encoded.model;
	  		self._userUnserialize(encodedModel);
		}
		  		
		if(encoded.view) {
			var encodedView = encoded.view;
			mView.unserialize(encodedView);
		}
  	};

	/**
	 * Serializes simulation variables to JSON.
	 * Could be extended by simulation.
	 * @method _userUnserialize
	 * @param json JSON
	 */		
	self._userUnserialize = function(json) {
    	self._readParameters(json);
		if (_model.resetSolvers) _model.resetSolvers();
   		self.update();
  	};

	// ------------------------------------------------------------------------
	// This part is used for configuration
	// ------------------------------------------------------------------------

	/**
	 * Whether the simulation should always run even when it looses focus
	 * @method setRunAlways
	 * @param always true Default is false
	 */
	self.setRunAlways = function(always) {
		mRunAlways = always;
	};

	/**
	 * Whether the simulation should pause when it looses focus
	 * @method setPauseOnPageExit
	 * @param pause true Default is true
	 */
    self.setPauseOnPageExit = function(pause) {
		mRunAlways = !pause;
    };
    
	/**
	 * Sets the number of frames per second
	 * @method setFPS
	 * @param fps
	 */
	self.setFPS = function(fps) {
		mDelay = Math.max(1000 / fps, MINIMUM_DELAY);
	};

	/**
	 * Returns the number of frames per second
	 * @method getFPS
	 * @return fps
	 */
	self.getFPS = function() {
		return Math.floor(1000.0 / mDelay);
	};
	
	/**
	 * Sets the delay between animation steps
	 * @method setDelay
	 * @param delay
	 */
	self.setDelay = function(delay) {
		mDelay = Math.max(delay, MINIMUM_DELAY);
	};
	
	/**
	 * Gets the delay between animation steps
	 * @method setDelay
	 * @return delay
	 */
	self.getDelay = function(delay) {
		return mDelay;
	};
	
	/**
	 * Sets the minimum delay between animation steps
	 * @method setMinimumDelay
	 * @param delay
	 */
	self.setMinimumDelay = function(delay) {
		mMinimumDelay = Math.max(delay, 0);
	};

	/**
	 * Sets the number of model steps before refreshing the view
	 * @method setStepsPerDisplay
	 * @param steps the number of model steps
	 */
	self.setStepsPerDisplay = function(steps) {
		if (steps >= 1)
			mStepsPerDisplay = steps;
	};

	/**
	 * Sets whether the simulation should update the view in each step
	 * Default is true.
	 * @method setUpdateView
	 * @param mustDo Whether to update the view
	 */
	self.setUpdateView = function(mustDo) {
		mShouldUpdateView = mustDo;
	};

	/**
	 * Sets whether the simulation should be set to play mode when it is reset.
	 * Default is false.
	 * @method setAutoplay
	 * @param play Whether it should play
	 */
	self.setAutoplay = function(play) {
		mAutoplay = play;
	};

	/**
	 * Whether the animation is playing
	 * @method isPlaying
	 */
	self.isPlaying = function() {
		return mIsPlaying;
	};

	/**
	 * Whether the animation is paused
	 * @method isPaused
	 */
	self.isPaused = function() {
		return !mIsPlaying;
	};

	/**
	 * Gets current running count
	 * @method getRunCount 
	 */
	self.getRunCount = function() {
		return mRunCount;
	}
	
	/**
	 * Gets current running time
	 * @method getRunTime 
	 */
	self.getRunTime = function() {
		return mRunTime;
	}

	/**
	 * Initializes running count and time
	 * @method initRunCountAndTime 
	 */
	self.initRunCountAndTime = function() {
		mRunCount = 0;	
		mRunTime = +new Date();
	}

	// -----------------------------
	// Controlling the execution
	// -----------------------------

	/**
	 * Resets the real time
	 * @method resetRealTime
	 */
	self.resetRealTime = function() {
		var now = +Date();
		mInitialRealTime = 1000 * self.getRealTime() - now;
	};

	/**
	 * Starts the animation
	 * @method play
	 */
	self.play = function() {
		if (mIsPlaying) return;

		mLastTime = +new Date();
		// self.resetRealTime();
		mSleepTime = mDelay;
		mIsPlaying = true;
		
		// plays after mMinimunDelay (ms)
		mCurrentTimeOut = window.setTimeout(self.run, mMinimumDelay);
	};

	/**
	 * Stops the animation
	 * @method pause
	 */
	self.pause = function() {
		mIsPlaying = false;
		mAbortSPDLoop = true;
		if (mCurrentTimeOut!=0) {
		  window.clearTimeout(mCurrentTimeOut);
		  mCurrentTimeOut = 0;
		}
	};

	/**
	 * Runs the animation
	 * @method run
	 */
	self.run = function() {
		var timeout = 1; 
		mRunCount++;	
		mRunTime = +new Date(); 				
		if (!mIsPlaying) return;
		if (mFocus || mRunAlways) {
	      	self.step();
	      	mLastTime = +new Date();
		  	timeout = Math.max (mMinimumDelay, mDelay - (mLastTime - mRunTime));
			// this is done as quickly as possible
			var thisTimeOut = mCurrentTimeOut;
			self._readCapturedInteractions();
			if (mView._readInteractions(mRunCount, mRunTime, mIsPlaying) || // The user interacted with the view
			    processSafeTasks()) { // there were safe tasks pending
			 	self.automaticResetSolvers();
			  	self.update();
			}			
			// check if the interaction introduced a new timeout or cancelled the current one.
			if (thisTimeOut != mCurrentTimeOut) return;
		} else {			
		  mLastTime = +new Date();
		}
	    // Just in case
		if (mView._readInteractions(mRunCount, mRunTime, mIsPlaying) || // The user interacted with the view
			processSafeTasks()) { // there were safe tasks pending
		  self.automaticResetSolvers();
		  self.update();
		}

		mCurrentTimeOut = window.setTimeout(self.run, timeout);
		// console.log ("Timeout set to "+mCurrentTimeOut+ " for timeout "+timeout);
	};

	/**
	 * Handles onExit event
	 * @method onExit
	 */
	self.onExit = function() {
		self.pause();
		mView._readInteractions(mRunCount, mRunTime, mIsPlaying); // In case the user interacted with the view
		processSafeTasks();
		//    mView.onExit();
		self.freeMemory();
	};

	/**
	 * Add a task that must be run only
	 * when the model is not doing any calculation.
	 * @method performTaskSafely
	 */
	self.performTaskSafely = function (taskFunction) {
	  if (!mIsPlaying) {
		console.log ("Calling the task now! ");
		taskFunction();
		self.automaticResetSolvers();
		self.update(true); // Force it
	  }
	  else {
		  console.log ("Delaying the task! ");
		  mPendingTasks.push(taskFunction);
	  }
	}

	/*
	 * For internal use only.
	 * Processed tasks that have been signaled to be run only
	 * when the model is not doing any calculation.
	 * @return true if any task was executed, false otherwise 
	 */
	function processSafeTasks() { // call this when the model computation won't be affected
      if (mPendingTasks.length==0) return false;
	  var tasks = mPendingTasks.slice(); // In case a task modifies the pending tasks
	  mPendingTasks = [];
      for (var i=0; i<tasks.length; i++) tasks[i]();
      return true;
	};
	
	//------------------------------------
	//Simulation logic based on the model
	//------------------------------------

	/**
	 * Resets the animation
	 * @reuseview reuse view
	 * @method reset
	 */
	self.reset = function(reuseview) {
		if(!reuseview) {
			self.pause();
			self.resetModel();
		}
		// stepModel must be defined by the 'subclass'

		mView._setReportNeeded(false);
		mView._reset();
		if(!reuseview) {
			mView._initValues();
			mView._initialize();

			self.initializeModel();
		}
		
		self.updateModel();
		if (mShouldUpdateView) {
			mView._update();
			mView._render();
		} else
			mView._collectData();
		mView._setReportNeeded(true);
		if (mAutoplay)
			self.play();
        mView._resized();
	};

	/**
	 * Initializes the animation
	 * @method initialize
	 */
	self.initialize = function() {
		mView._initialize();

		self.initializeModel();
		self.updateModel();

		if (mShouldUpdateView) {
			mView._update();
			mView._render();
		} else
			mView._collectData();
	};

	/**
	 * Updates the simulation
	 * @method update
	 */	
	self.update = function(forceIt) {
		if(! (mFocus || mRunAlways || forceIt) ) return;

		self.updateModel();
		if (mShouldUpdateView) {
			mView._update();
			mView._render();
		} 
		else mView._collectData();
		
		if (processSafeTasks()) { // do it again
		  self.automaticResetSolvers();
		  self.updateModel();
		  if (mShouldUpdateView) {
		    mView._update();
			mView._render();
		  } 
		  else mView._collectData();
		}
	};
	
	/**
	 * Steps the animation once
	 * @method step
	 * @return stepModel time
	 */
	self.step = function(forceStepsPerDisplay) {	
		var now = +new Date();		
		if (!forceStepsPerDisplay) forceStepsPerDisplay = 1;
		var steps = Math.max(forceStepsPerDisplay,mStepsPerDisplay);
		if (steps > 1) {
			mAbortSPDLoop = false;
			for (var i = 1; i < steps; i++) {
				if (mAbortSPDLoop) {// The user called pause() in between this loop
					self.update();
					return;
				}
				self.stepModel();
				self.updateModel();
				mView._collectData();
			}
		}
		// Now the final step
		self.stepModel();
		var stepModelTime = (+new Date()) - now;
		
		// stepModel must be defined by the 'subclass'
		self.update();
		
		return stepModelTime;
	};

	/**
	 * Sets a break flag
	 * @method setShouldBreak
	 */
	self.setShouldBreak = function(shouldDo) {
		mShouldBreak = shouldDo;
	};

	/**
	 * Returns the break flag
	 * @method getShouldBreak
	 */
	self.getShouldBreak = function() {
		return mShouldBreak;
	};

  	self._autoSelectView = function (viewsInfo) {
      var height = screen.height;
      var width = screen.width;
      
      var sel = [];
      var min = Number.MAX_VALUE;
      for(var i=0; i<viewsInfo.length; i++) { // find the best width
          var dis = width - viewsInfo[i]["width"];
          if(dis >= 0) {
              if(dis < min) {
                  min = dis;
                  sel = [i];
              } else if (dis == min) {
                  sel.push(i);
              }
          }
      }
      var ret = -1;
      min = Number.MAX_VALUE;
      for(var i=0; i<sel.length; i++) { // find the best height
          var dis = Math.abs(height - viewsInfo[sel[i]]["height"]);
        if(dis < min) {
            min = dis;
            ret = sel[i];
        }
      }
      return ret;
  	};

 	self.addToOnload = function(onloadfunc) {
 		mOnloadFunc = onloadfunc;
 	}
 	
 	self.onload = function() {
		if(typeof mOnloadFunc == "function")
			mOnloadFunc();
 	}
  	
	/**
	 * Utility to parse input parameters. 
	 * @method parseInputParameters
	 * @param inputParameters a String with the parameters to parse
	 */
	self.parseInputParameters = function(inputParameters) {
		if (typeof inputParameters === 'string' || inputParameters instanceof String) { 
			// The EjsAPP Moodle plug-in codifies them
			try {
			  inputParameters = EJSS_TOOLS.Decode.hex_to_ascii(inputParameters);
			  inputParameters = EJSS_TOOLS.Decode.decode(inputParameters);
			  if      (inputParameters[0]=="'" && inputParameters[inputParameters.length-1]=="'") inputParameters = inputParameters.substring(1,inputParameters.length-1);
			  else if (inputParameters[0]=='"' && inputParameters[inputParameters.length-1]=='"') inputParameters = inputParameters.substring(1,inputParameters.length-1);
			  inputParameters = JSON.parse(inputParameters);
			}
			catch (exception) {
				console.log("Error trying to parse input parameters: "+inputParameters);
				return null;
			}
		}
		return inputParameters;
	};

	//---------------------------------
	// To be modified or overwritten by subclasses
	//---------------------------------

	var mResetList = [];
	var mInitializationList = [];
	var mEvolutionList = [];
	var mFixedRelationsList = [];

	self.getRealTime = function() { return NaN; };
	self.automaticResetSolvers = function() { };
	self.freeMemory = function() { };

	/**
	 * Adds a function to the reset part of the model
	 * @method _addToReset
	 * @param aFunction the function to add
	 */
	self.addToReset = function(aFunction) {
		mResetList.push(aFunction);
	};

	/**
	 * Adds a function to the initialization part of the model
	 * @method _addToReset
	 * @param aFunction the function to add
	 */
	self.addToInitialization = function(aFunction) {
		mInitializationList.push(aFunction);
	};

	/**
	 * Adds a function to the evolution part of the model
	 * @method _addToReset
	 * @param aFunction the function to add
	 */
	self.addToEvolution = function(aFunction) {
		mEvolutionList.push(aFunction);
	};

	/**
	 * Adds a function to the fixed relations part of the model
	 * @method _addToReset
	 * @param aFunction the function to add
	 */
	self.addToFixedRelations = function(aFunction) {
		mFixedRelationsList.push(aFunction);
	};

	 self.addFixedRel = function(code){_model.addToFixedRelations(function() { eval(code);});};
	 
	/**
	 * Resets the model
	 */
	self.resetModel = function() {
		processFunctionList(mResetList);
	};
	
	/**
	 * Initializes the model
	 */
	self.initializeModel = function() {
		processFunctionList(mInitializationList);
	};
	
	/**
	 * Steps the model
	 */
	self.stepModel = function() {
		processFunctionList(mEvolutionList);
	};
	
	/**
	 * Updates the model
	 */
	self.updateModel = function() {
		processFunctionList(mFixedRelationsList);
	};

	function processFunctionList(functionList) {
		for (var i = 0, n = functionList.length; i < n; i++) {
			mShouldBreak = false;
			functionList[i]();
			if (mShouldBreak)
				return;
		}
	}
	
	//---------------------------------
	// final initialization
	//---------------------------------

	window.onblur  = function() { 
		mFocus = false; 
		if (mView) mView._onBlur();
    };
	
    window.onfocus = function() { 
		mFocus = true;  
		if (mView) mView._onFocus();
    };
    
	var _super_onorientationchange = window.onorientationchange;
	window.onorientationchange = function() {		
		if(_super_onorientationchange) _super_onorientationchange();
	    if (mView) mView._resized();
	};
	
	// resize
	var _super_onresize = window.onresize; 
	window.onresize = function() {
		if(_super_onresize) _super_onresize();		
	    if (mView) mView._resized();
	};
	
	return self;

};

