/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.VelocityVerlet = {

};

/**
 * Constructor for RungeKutta4
 * @returns RungeKutta4
 */
EJSS_ODE_SOLVERS.velocityVerlet = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTime(); // reference returned 
  var mAccelerationIndependentOfVelocity;
  var mRate2;

  superSetODE = self.setODE;
  
  self.setODE = function(eventSolver, ode) { 
	superSetODE(eventSolver,ode);
	if (ode.isAccelerationIndependentOfVelocity) mAccelerationIndependentOfVelocity = ode.isAccelerationIndependentOfVelocity();
	else mAccelerationIndependentOfVelocity = false;
  }
  
  self.getNumberOfEvaluations = function() { return 2; }

  self.allocateOtherArrays = function() {
    mRate2 = new Array(self.getDimension());
  };

  self.computeIntermediateStep = function(step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    var i;
    
    var dt2 = (step*step)/2; // the step size squared
    // increment the positions using the velocity and acceleration
    for(i = 0; i<timeIndex; i+=2) state[i] = initialState[i] + step*initialRate[i] + dt2*initialRate[i+1];
    ode.getRate(state, mRate2);
    var halfStep = step/2;
    // increment the velocities with the average rate
    for(i = 1; i<timeIndex; i+=2) state[i] = initialState[i] + halfStep*(initialRate[i]+mRate2[i]);
    // the independent variable
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];
  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    var CODE = EJSS_ODE_SOLVERS.DISCONTINUITY_CODE;
    var i;
    
    var dt2 = (step*step)/2; // the step size squared
    // increment the positions using the velocity and acceleration
    for (i = 0; i<timeIndex; i+=2) state[i] = initialState[i] + step*initialRate[i] + dt2*initialRate[i+1];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate2);

    var halfStep = step/2;
    // increment the velocities with the average rate
    for (i = 1; i<timeIndex; i+=2) state[i] = initialState[i] + halfStep*(initialRate[i]+mRate2[i]);
	state[timeIndex] = self.getInitialTime() + step*timeRate;

    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    var timeIndex  = self.getDimension()-1;

    if (mAccelerationIndependentOfVelocity) {
      for (var i=0,j=1; i<timeIndex; i+=2,j+=2) {
        finalRate[i] = finalState[j];
        finalRate[j] = mRate2[j];
      }
      finalRate[timeIndex] = mRate2[timeIndex];
    }
    else {
      ode.getRate(finalState, finalRate);
      // Accumulate the counter
      self.addToEvaluations(self.getNumberOfEvaluations());
    }
    return new EJSS_ODE_INTERPOLATION.hermiteIntervalData(initialState, initialRate, finalState, finalRate);
  }


  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}