/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

EJSS_ODE_SOLVERS.EVENT_TYPE = {
  STATE_EVENT : 0,
  POSITIVE_EVENT : 1,
  CROSSING_EVENT : 2
};
  
EJSS_ODE_SOLVERS.EVENT_METHOD = {
  BISECTION : 0,
  SECANT    : 1
};
  
EJSS_ODE_SOLVERS.ERROR = {
    NO_ERROR                     : 0, // Everything went ok
    INTERNAL_SOLVER_ERROR        : 1, // The solver produced an internal error, such as when it cannot do an internal step
    EVENT_NOT_FOUND              : 2, // The event was not found after using the maximum number of attempts
    ILLEGAL_EVENT_STATE          : 3, // An event of type STATE_EVENT was left in an illegal (negative) state
    ZENO_EFFECT                  : 4, // A Zeno effect was detected without a user action
    TOO_MANY_STEPS_ERROR         : 5, // The solver exceeded the number of internal steps
    DISCONTINUITY_PRODUCED_ERROR : 6, // Unrecoverable error produced by a discontinuity
    DID_NOT_CONVERGE             : 7  // An adaptive method did not converge
};

EJSS_ODE_SOLVERS.DISCONTINUITY_CODE = {
    DISCONTINUITY_PRODUCED_ERROR  : 0, // Unrecoverable error
    NO_DISCONTINUITY_ALONG_STEP   : 1, // There is no discontinuity along the given step
    DISCONTINUITY_ALONG_STEP      : 2, // There is a  discontinuity along the given step, but not exactly at its end
    DISCONTINUITY_JUST_PASSED     : 3, // There is a  discontinuity along the given step, and it is just before the current step end
    DISCONTINUITY_EXACTLY_ON_STEP : 4  // There is a  discontinuity along the given step, and its is exactly at the end of the step
};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.InterpolatorEventSolver = {
};

/**
 * Constructor for InterpolatorEventSolver
 * @returns InterpolatorEventSolver
 */
EJSS_ODE_SOLVERS.interpolatorEventSolver = function(mSolverEngine, mODE) {
  var self = {};							// reference returned 

  // Configuration variables
  var mEnableExceptions=true;                 // Whether to throw an exception when an error is produced. 
  var mUseBestInterpolation=false;            // Always use the best interpolation the solvers provide (even if it is more time consuming)
  var mStepSize = 0.1;                        // The step size for reading data from the solver
  var mAbsoluteTolerance = Number.NaN;        // The absolute tolerance for adaptive methods
  var mRelativeTolerance = Number.NaN;        // The relative tolerance for adaptive methods
  var mMaxEventStep=Number.POSITIVE_INFINITY; // The maximum advance before checking for an event
  var mZenoMaximumAllowedTimes=500;           // The number of times that a separation smaller than the threshold must occur to declare a Zeno effect
  var mProximityThreshold=2*Number.MIN_VALUE; // The threshold to consider two events as simultaneous or indicate a possible Zeno effect
  var mCoalesceCloseEvents = true;            // whether to coalesce close events (even if the action returns true)
  var mMaxInternalSteps = 10000;
  
  // Implementation variables
  // var mSolverEngine; // The underlying solver engine
  // var mODE;          // The ODE being solved
  
  var mRunsForwards = mStepSize>0;
  var mDimension; // The dimension of the problem (including time)
  var mTimeIndex; // The index of the time state, i.e. dimension-1
  var mErrorCode = EJSS_ODE_SOLVERS.ERROR.NO_ERROR; // The error code
  var mErrorMessage="No error";
  var mNumberOfAttempts=0;     // Actual number of attempts needed to locate the last event
  var mTest_ode_state;         // a place holder for intermediate states
  var mIntermediate_ode_state; // a second place holder for intermediate states
  
  var mEventList = []; // The list of events added by the user (except those of type DISCONTINUITY_EVENT, which are handled separately)
  var mHappened  = []; // The list of events that take place in a given interval
  var mTemp_list = []; // a temporary list of events
  var mLastEventDataTime=Number.NaN; // Remember the time of the last event
  var mLastEventData=null;           // Remember the last event
  var mZenoCounter=0;                // Count how many close events have happened
  var mZenoList = [];                // list of Zeno Listeners

  var mCurrentEventData=null;        // Placeholder for the current event, used to reset some events properly
  var mHasEventsOrDiscontinuities = false;
  var mHasDiscontinuities = false;
  var mDiscontinuityAtEnd = null;   // The first DISCONTINUITY_EVENT event founds in one step // version 2.0
  var mDiscontinuityList = [];      // The list of DISCONTINUITY_EVENT type of events added by the user // version 2.0

  var mDDEdiscontinuity=null;
  var mDDEdiscontinuityMaxIterations = 100;
  var mDDEdiscontinuityTolerance = 1.0e-8;
    
  // --------------------------------------------
  // Static methods
  // --------------------------------------------

  var sEPSILON=Number.NaN;

  self.getEPSILON = function() {
    if (isNaN(sEPSILON)) {
      sEPSILON = 1;
      while (sEPSILON+1!=1) sEPSILON /=2;
      sEPSILON *= 2;
//      console.log("EPSILON ="+sEPSILON+"\n");
    }
    return sEPSILON;
  };

  // --------------------------------------------
  // Configuration and getter methods
  // --------------------------------------------

  /**
   * Enables runtime exceptions if there is any error condition. 
   * If exceptions are disabled, the solver prints a warning to the standard error output and continues.
   * Exceptions are enabled by default.
   * @param enable boolean
   */
  self.setEnableExceptions = function(enable) { mEnableExceptions = enable; }

  /**
   * Request the internal solver to always use the best interpolation it can provide,
   * even if it is more time-consuming than the standard one. For examples, some RK
   * solvers will re-step from the initial step every time they are asked for their best
   * interpolation, which is very expensive in terms of function evaluations.
   * Best interpolation is however always used (irrespective of this parameter) to 
   * return the state after a successful step and after an event takes place. 
   * But standard interpolation is used by default for locating the events.
   * Setting this parameter to true forces the solver to use its best interpolation
   * even for event location.
   * @param best
   */
  self.setBestInterpolation = function(best) { mUseBestInterpolation = best; }
  
  /**
   * Sets the length of the memory requested to the solver. Must be a positive value.
   * The user will then be able to ask for values of the state as far as the current time minus this length.
   * 0 is the default for plain ODE, getMaximumDelay() is the minimum used by DDEs.
   * Setting a value of Infinity makes the solver to remember for ever (i.e. as much as computer memory permits)
   * @param length
   */
  self.setHistoryLength = function(length) { mSolverEngine.getStateHistory().setLength(length); }
    
  /**
   * Sets the reading step size. That is the step at which solutions are read from 
   * the equation. Most of the times, these solutions are obtained by interpolation.
   */
  self.setStepSize = function(stepSize) {
    if (mStepSize==stepSize) return;
    mStepSize = stepSize;
    mRunsForwards = mStepSize>0;
    self.setInternalStepSize(mSolverEngine.getStepSize()); // Make sure the interpolator solver runs in the same direction
  }

  /**
   * Returns the reading step
   */
  self.getStepSize = function() { return mStepSize; }

  /**
   * Asks adaptive solvers to estimate the best initial step after reinitialize().
   * If false, the given initial step (as set by setComputationStepSize()) is used.
   * @param estimate
   */
  self.setEstimateFirstStep = function(estimate) { mSolverEngine.setEstimateFirstStep(estimate); }
  
  /**
   * Sets the interpolator's internal step size. This is the step at which solutions are computed
   * for fixed step methods, and the initial step size (after reinitialize()) for variable
   * step methods. The value is taken absolutely in the direction of the reading step size.
   */
  self.setInternalStepSize = function(stepSize) {
    mSolverEngine.setStepSize(mRunsForwards ? Math.abs(stepSize) : -Math.abs(stepSize));
  }

  /**
   * Sets the interpolator's internal maximum step size. Has no effect on fixed-step solvers and on QSS methods.
   * @param stepSize
   */
  self.setMaximumInternalStepSize = function(stepSize) { mSolverEngine.setMaximumStepSize(stepSize); }
  
  /**
   * Sets the maximum number of internal steps the interpolator can take to reach a 
   * reading step. If the solver takes so large a number (of very small steps) this
   * typically means the solver has reached a singularity and must take too small steps.
   * It can also mean the solver has difficulties to reach the tolerance and the user
   * should consider either increasing this limit, or use another solver. 
   * 
   * @param steps
   */
  self.setMaximumInternalSteps = function(steps) { mMaxInternalSteps = steps; }

  /**
   * The preferred absolute and relative tolerance desired for the solution if the 
   * underlying solver supports it. If the solver does not support this feature, the 
   * method is ignored. Changing the tolerances may involve a re-computation 
   * of the current step.
   * @param tol
   */
  self.setTolerances = function(absTol, relTol) {
    mSolverEngine.setTolerances(mAbsoluteTolerance = Math.abs(absTol), mRelativeTolerance = Math.abs(relTol));
  }

  /**
   * Equivalent to setTolerances (tol,0)
   * @param tol
   */
  self.setTolerance = function(tol) { setTolerances(tol,0); }

  /**
   * Returns the maximum of the absolute and relative tolerances
   */
  self.getTolerance = function() { return Math.max(mAbsoluteTolerance, mRelativeTolerance); }
  
  /**
   * The tolerance for finding discontinuities created by DDE delays
   * @param tol
   */
  self.setDDETolerance = function(tol) { mDDEdiscontinuityTolerance = tol; }

  self.getDDETolerance = function() { return mDDEdiscontinuityTolerance; }

  /**
   * Number of iterations allowed to find discontinuities created by DDE delays
   * @param iterations
   */
  self.setDDEIterations = function(iterations) { mDDEdiscontinuityMaxIterations = iterations; }

  /**
   * Number of iterations needed to find the last discontinuity created by a DDE delay
   */
  self.getDDEIterations = function() { return mDDEdiscontinuityMaxIterations; }

  // -----------------------------------
  // Events and discontinuities
  // -----------------------------------

  self.addEvent = function(event) {
    mEventList.push(EJSS_ODE_SOLVERS.eventData(self,event,mODE.getState()));
    mHasEventsOrDiscontinuities = true;
  }
  
  self.removeEvent = function(event) {
    var index = -1;
    var length = mEventList.length;    
    for (var i=0; i<length; i++) {
      var data = mEventList[i];
      if (data.getEvent()==event) { index = i; break; }
    }
    if (index>=0) {
      var foundData = mEventList[index];
      if (mLastEventData==foundData) { mLastEventData = null; mZenoCounter = 0; }
      if (mCurrentEventData==foundData) mCurrentEventData = null;
      mEventList.splice(index,1);
    }
    mHasEventsOrDiscontinuities = mHasDiscontinuities || (mEventList.length>0);
  }

  self.addDiscontinuity = function(discontinuity) {
    mDiscontinuityList.push(EJSS_ODE_SOLVERS.discontinuityData(self,discontinuity,mODE.getState()));
    mHasDiscontinuities = true;
    mHasEventsOrDiscontinuities = true;
  }

  self.removeDiscontinuity = function(discontinuity) {
    var index = -1;
    var length = mDiscontinuityList.length;
    for (var i=0; i<length; i++) {
      var data = mDiscontinuityList[i];
      if (data.getDiscontinuity()==discontinuity) { index = i; break; }
    }
    if (index>=0) {
      mDiscontinuityList.splice(index,1);
    }
    mHasDiscontinuities = (mDiscontinuityList.length>0);
    mHasEventsOrDiscontinuities = mHasDiscontinuities || (mEventList.length>0);
  }

  self.removeAllEvents = function() {
    mEventList = [];
    mDiscontinuityList = [];
    mHasDiscontinuities = false;
    mHasEventsOrDiscontinuities = false;
    if (mDDEdiscontinuity!=null) self.addDiscontinuity(mDDEdiscontinuity);
  }
    
  /**
   * Sets the maximum step allowed before checking for an event.
   * Default is Number.POSITIVE_INFINITY
   * @param step
   */
  self.setMaximumEventStep = function(step) { mMaxEventStep = Math.abs(step); }

  /**
   * Returns the maximum step allowed before checking for an event.
   * @return
   */
  self.getMaximumEventStep = function() { return mMaxEventStep; }

  /**
   * If true, an event closer than the threshold to the previous one 
   * will not return even if the event action returns true. Default is true.
   * @param coalesce
   */
  self.setCoalesceCloseEvents = function(coalesce) { mCoalesceCloseEvents = coalesce; }
  
  /**
   * Whether an event closer than the threshold to the previous one 
   * will not return even if the event action returns true
   * @return
   */
  self.isCoalesceCloseEvents = function() { return mCoalesceCloseEvents; }
  
  /**
   * Sets the threshold that considers two events as close enough for coalescing
   * or indicating a possible Zeno-like effect. Default is 2*Number.MIN_VALUE
   * @param threshold The small separation that indicates a possible Zeno effect
   */
  self.setEventProximityThreshold = function(threshold) { mProximityThreshold = threshold; }

  /**
   * Return the threshold that considers two events as close enough for coalescing
   * or indicating a possible Zeno-like effect
   */
  self.getEventProximityThreshold = function() { return mProximityThreshold; }

  // -----------------------------------
  // Zeno-like effects 
  // -----------------------------------

  /**
   * If the solver finds more than _times successive events closer than the proximity threshold,
   * it will consider it a Zeno effect and call the registered ZenoEffectListeners. Default is 500.
   * @param times The number of times that a separation smaller than the threshold must occur to declare a Zeno effect. 
   * A zero or negative value disables the detection.
   */
  self.setZenoEffectDetection = function(times) { mZenoMaximumAllowedTimes = times; }

  /**
   * Returns the number of successive events closer than the proximity threshold,
   * that will be considered a Zeno effect.
   */
  self.getZenoEffectDetection = function() { return mZenoMaximumAllowedTimes; }

  /**
   * Adds a ZenoEffectListener that will be called if a Zeno-like effect situation occurs.
   */
  self.addZenoEffectListener = function(listener) { mZenoList.push(listener); }
  
  /**
   * Removes a previously added ZenoEffectListener 
   */
  self.removeZenoEffectListener = function(listener) { 
    var length = mZenoList.length;
    for (var i=0; i<length; i++) {
      if (mZenoList[i]==listener) { mZenoList.splice(i,1); return; }
    }
  }
  
  self.setZeroZenoCounter = function() { mZenoCounter = 0; }
  
  // --------------------------------------------  
  // Getter methods
  // --------------------------------------------

  /**
   * Returns the interpolator solver
   * @return SolverEngine
   */
  self.getSolverEngine = function() { return mSolverEngine; };

  /**
   * Returns the ODE to solve
   */
  self.getODE = function() { return mODE; }
  
  /**
   * Provides access to the internal StateHistory responsible for interpolations.
   * @return
   */
  self.getStateHistory = function() { return mSolverEngine.getStateHistory(); }

  /**
   * Returns the error code after a step
   */
  self.getErrorCode = function() { return mErrorCode; }
  
  /**
   * Returns the error message
   * @return
   */
  self.getErrorMessage = function() { return mErrorMessage; }

  /**
   * Returns the number of function evaluations required by the method to reach the tolerance 
   * @return
   */
  self.getCounter = function() { return mSolverEngine.getCounter(); }

  /**
   * The number of attempts that were required to locate the last event
   * @return
   */
  self.getNumberOfAttempts = function() { return mNumberOfAttempts; }

  self.getInternalStepSize = function() { return mSolverEngine.getInternalStepSize(); }
  
  /**
   * Returns the current value of the independent variable
   * @return
   */
  self.getIndependentVariableValue = function() { return mODE.getState()[mTimeIndex]; }
  
  /**
   * Same as getIndependentVariableValue ()
   * @return
   */
  self.getCurrentTime = function() { return mODE.getState()[mTimeIndex]; }


 // Internal use only
 
  self.getRunsForwards = function() { return mRunsForwards; }

  self.getCurrentEventData = function() { return mCurrentEventData; }

  self.getLastEventData = function() { return mLastEventData; }

  self.getLastEventDataTime = function() { return mLastEventDataTime; }

  // ----------------------------------------------------
  // Operation
  // ----------------------------------------------------
  
  /**
   * Initializes the solver. The step size is used to set the reading step size AND
   * also passed along to the ODESolverInterpolator for internal initialization.
   * The interpolator's internal step size can be changed with setInternalStepSize(double).
   * Calls to setStepSize() will not affect the internal step size of the interpolator.
   */
  self.initialize = function(stepSize) {
    var i, length, state;
    mStepSize = stepSize;
    mRunsForwards = mStepSize>0;
    mSolverEngine.initialize(mStepSize);
    state = mODE.getState(); 
    mDimension = state.length;
    mTimeIndex = mDimension - 1;
    mTest_ode_state = new Array(mDimension);
    mIntermediate_ode_state = new Array(mDimension);
    mErrorCode = EJSS_ODE_SOLVERS.ERROR.NO_ERROR;
    mErrorMessage = "No error";
    mZenoCounter = 0;
    mLastEventData = null;
    mCurrentEventData = null;       
    if (mDDEdiscontinuity!==null) {
      mDDEdiscontinuity.initialize(state);
      self.removeDiscontinuity(mDDEdiscontinuity);
      self.addDiscontinuity(mDDEdiscontinuity);
    }
    length = mEventList.length;
    for (i=0; i<length; i++) mEventList[i].reset(state);
    length = mDiscontinuityList.length;
    for (i=0; i<length; i++) mDiscontinuityList[i].reset(state);
  }
  
  /**
   * Take the next step as indicated by the stepSize
   * If an event is found, it steps up to that event.
   * @return
   */
  self.step = function() {
    if (mHasEventsOrDiscontinuities) return stepWithEvents();
    return stepWithoutEvents();
  }

  /**
   * Take the maximum possible step.
   * For adaptive-step methods, steps up to the maximum possible adaptive step. 
   * For fixed-step methods, steps up to the next step size.
   * If an event is found, it steps up to that event.
   * @return
   */
  self.maxStep = function() {
    if (mHasEventsOrDiscontinuities) return maxStepWithEvents();
    return maxStepWithoutEvents();
  }

  /**
   * Used to reinitialize the solver when the user changes directly the ODE state,
   * model parameters, or anything THAT MAY AFFECT THE EVENTS (such as the events tolerance, for instance).
   * EjsS calls automatically reinitialize(), which respects the currentDataEvent information when it detects
   * any other minor change.
   */
  self.userReinitialize = function() {
    mCurrentEventData = null;
    self.reinitialize();
  }

  /**
   * Does the minimum (soft) initialization of the solver for a given state.
   * Users MUST call reinitialize (or the harder initialize()) whenever they change directly the ODE state,
   * model parameters, or anything that may affect the events (such as the events tolerance, for instance).
   */
  self.reinitialize = function() {
    var i, length;
    var state = mODE.getState();
    mSolverEngine.reinitialize(state);
    mErrorCode = EJSS_ODE_SOLVERS.ERROR.NO_ERROR;
    mErrorMessage = "No error";
    if (mDDEdiscontinuity!=null) mDDEdiscontinuity.reset(state); // This must be called before before discData.reset() 
    length = mEventList.length;
    for (i=0; i<length; i++) mEventList[i].reset(state);
    length = mDiscontinuityList.length;
    for (i=0; i<length; i++) mDiscontinuityList[i].reset(state);
    mDiscontinuityAtEnd = null;
  }

  self.resetDiscontinuities = function(state) {
    if (mDDEdiscontinuity!==null) {
      mDDEdiscontinuity.reset(state); // This must be called before before discData.reset() 
      var length = mDiscontinuityList.length;
      for (var i=0; i<length; i++) mDiscontinuityList[i].reset(state);
    }
  }
  
  /**
   * Checks whether there would be a discontinuity before or at the given state and, in this last case, optionally marks it 
   * @param state The state to check for
   * @param isEndOfInterval Whether the point is the end of the integration interval. In this case, we mark the point as the discontinuity found
   * @return one of DISCONTINUITY_PRODUCED_ERROR, NO_DISCONTINUITY_ALONG_STEP, DISCONTINUITY_ALONG_STEP, or DISCONTINUITY_EXACTLY_ON_STEP
   */
  self.checkDiscontinuity = function(state, isEndOfInterval) {
    var length = mDiscontinuityList.length;
    var justHappened=null;
    for (var i=0; i<length; i++) {
      var data = mDiscontinuityList[i];
      var h = data.getDiscontinuity().evaluate(state);
      data.setH(h); 
      switch (data.getCurrentPosition()) {
        default : 
        case EJSS_ODE_SOLVERS.ProblemData.POSITIVE : 
          if (h<=0) {
//            if (h>-data.getDiscontinuity().getTolerance()) return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED; // h=0 is allowed in this direction
            return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_ALONG_STEP;
          }
          if (h<data.getDiscontinuity().getTolerance()) justHappened = data;
          break;
        case EJSS_ODE_SOLVERS.ProblemData.SMALL_POSITIVE :
          if (h<=0 && data.hasPositiveFlag()) {
            self.error(EJSS_ODE_SOLVERS.ERROR.ILLEGAL_EVENT_STATE, "The system started from an illegal state at "+state[mTimeIndex]+ " for the discontinuity "+data.getDiscontinuity());
            return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR;
          }
          break;
        case EJSS_ODE_SOLVERS.ProblemData.ZERO :
          if (h<0 && data.hasPositiveFlag()) {
            self.error(EJSS_ODE_SOLVERS.ERROR.ILLEGAL_EVENT_STATE, "The system started from an illegal state at "+state[mTimeIndex]+ " for the discontinuity "+data.getDiscontinuity());
            return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR;
          }
          else if (h>0 && data.hasNegativeFlag()) {
            self.error(EJSS_ODE_SOLVERS.ERROR.ILLEGAL_EVENT_STATE, "The system started from an illegal state at "+state[mTimeIndex]+ " for the discontinuity "+data.getDiscontinuity());
            return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR;
          }
          break;
        case EJSS_ODE_SOLVERS.ProblemData.SMALL_NEGATIVE :
          if (data.hasNegativeFlag() && h>=0) {
            self.error(EJSS_ODE_SOLVERS.ERROR.ILLEGAL_EVENT_STATE, "The system started from an illegal state at "+state[mTimeIndex]+ " for the discontinuity "+data.getDiscontinuity());
            return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR;
          }
          break;
        case EJSS_ODE_SOLVERS.ProblemData.NEGATIVE : 
          if (h>0) {
//            if (h>data.getDiscontinuity().getTolerance()) return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED; // h=0 is allowed in this direction
            return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_ALONG_STEP; // h=0 is allowed in this direction
          }
          if (h>-data.getDiscontinuity().getTolerance()) justHappened = data;
          break; 
      } // end of switch
    } // end of for
    if (justHappened!=null) {
      if (isEndOfInterval) {
          mDiscontinuityAtEnd = justHappened;
          mDiscontinuityAtEnd.setTime(state[mTimeIndex]);
//          System.err.println ("Disc at end is now "+justHappened+ " at time "+discontinuityAtEnd.getTime());
        }
      return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_EXACTLY_ON_STEP;
    }
    return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.NO_DISCONTINUITY_ALONG_STEP;  
  }
  
  // ----------------------------------------------------
  // ----------------------------------------------------
  // Private functions
  // ----------------------------------------------------
  // ----------------------------------------------------
    
  /**
   * Does the interpolation at the prescribed time.
   */
  function doTheInterpolation(time, state) {
    if (mUseBestInterpolation) mSolverEngine.bestInterpolate(time, state);
    else mSolverEngine.interpolate(time, state);
  }
  
  self.error = function(code, msg) {
    mErrorCode = code;
    mErrorMessage = msg;
    if (mEnableExceptions) throw {
      name : "ODE Solver exception",
      solver : mSolverEngine,
      message : msg
    };
    return Number.NaN;
  }
  
  // ----------------------------------------
  // Step without events or discontinuities
  // ----------------------------------------
  
  /**
   * Steps the ODE when there are no events
   * @return
   */
  function maxStepWithoutEvents() {
    var state = mODE.getState();
    
    // Check for a 0 rate for time
    var rate = mSolverEngine.getCurrentRate();
    var tBegin = state[mTimeIndex];
    if (tBegin+rate[mTimeIndex]==tBegin) return 0;
    
    var max_t = mSolverEngine.getMaximumTime(false); // false = no discontinuities
    if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver at "+state[mTimeIndex]);
    // Make sure the solver is not already at the maximum step
    if (tBegin==max_t) {
      max_t = mSolverEngine.internalStep(false);
      if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver at max step at "+state[mTimeIndex]);
    }
    doTheInterpolation (max_t, state);
    return max_t-tBegin;
  }
  
  /**
   * Steps the ODE when there are no events
   * @return
   */
  function stepWithoutEvents() {
    var state = mODE.getState();
    
    // Check for a 0 rate for time
    var rate = mSolverEngine.getCurrentRate();
    var tBegin = state[mTimeIndex];
    if (tBegin+rate[mTimeIndex]==tBegin) return 0;
    
    var tEnd = state[mTimeIndex] + mStepSize;
    var max_t = mSolverEngine.getMaximumTime(false); // false = no discontinuities
    if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver at "+state[mTimeIndex]);

    var counter = 0;
    // Make sure the solver can reach the expected time
    if (mRunsForwards) {
      while (max_t<tEnd) { 
        max_t = mSolverEngine.internalStep(false); // false = no discontinuities
        if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver forwards at "+state[mTimeIndex]);
        if ((++counter)>mMaxInternalSteps) { 
          var initTime = state[mTimeIndex];
          var currentTime = mSolverEngine.bestInterpolate(tEnd, new Array(mDimension))[mTimeIndex];
          return self.error (EJSS_ODE_SOLVERS.ERROR.TOO_MANY_STEPS_ERROR,"The solver exceeded the maximum of "+mMaxInternalSteps+  
              " internal steps\nat "+currentTime+ ", starting from "+initTime +" for an step of "+mStepSize);
        }
      }
    }
    else {
      while (max_t>tEnd) {
        max_t = mSolverEngine.internalStep(false); // false = no discontinuities
        if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver backwards at "+state[mTimeIndex]);
        if ((++counter)>mMaxInternalSteps) return self.error(EJSS_ODE_SOLVERS.ERROR.TOO_MANY_STEPS_ERROR,"The solver exceeded the number of internal steps at "+state[mTimeIndex]);
      }
    }
    doTheInterpolation (tEnd, state);
    return mStepSize;
  }

  // ----------------------------------------
  // Step with events and/or discontinuities
  // ----------------------------------------

  /**
   * Steps the ODE with events
   * @return
   */
  function maxStepWithEvents() {
    var state = mODE.getState();
    if (mZenoMaximumAllowedTimes>0 && mZenoCounter>mZenoMaximumAllowedTimes) {
      if (callZenoAction(state)) return 0;
    }

    var tBegin = state[mTimeIndex];
    // Check for a 0 rate for time
    var rate = mSolverEngine.getCurrentRate();
    if (tBegin+rate[mTimeIndex]==tBegin) return 0;

//    discontinuityAtEnd = null; // Do NOT clean the possible discontinuity
    var max_t = mSolverEngine.getMaximumTime(mHasDiscontinuities); // This detects discontinuities, if there are any
    if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver at "+state[mTimeIndex]);
    // Make sure the solver is not already at the maximum step
    if (tBegin==max_t) {
      max_t = mSolverEngine.internalStep(mHasDiscontinuities);
      if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver forwards at "+state[mTimeIndex]);
    }

    if (mLastEventData!=null) {
      if (!isNaN(mLastEventData.getMaxAdvance())) { // If a short-duration step was found, this helps avoid recurrent events
        max_t = mRunsForwards ? Math.min(max_t, mLastEventData.getMaxAdvance()) : Math.max(max_t, mLastEventData.getMaxAdvance());  
      }    
    }
    
    var tTest = mRunsForwards ? Math.min(tBegin+mMaxEventStep,max_t) : Math.max(tBegin-mMaxEventStep,max_t);

    while (true) {
      var problem=null;
      mCurrentEventData = null;       
      doTheInterpolation (tTest, mTest_ode_state);
      problem = self.findFirstEvent(state,tTest,mTest_ode_state); // Find the first event
      if (problem==null && mDiscontinuityAtEnd!=null && mDiscontinuityAtEnd.getTime()<=tTest) problem = mDiscontinuityAtEnd; // If no event, check for discontinuity (but regular events are acted on first)
      if (problem==null) {
        if (tTest==max_t) {
          for (var i=0; i<mDimension; i++) state[i] = mTest_ode_state[i];
          updateEventsAndDiscontinuities(state[mTimeIndex]);
          return max_t-tBegin;
        }
        tTest = mRunsForwards ? Math.min(tTest+mMaxEventStep,max_t) : Math.max(tTest-mMaxEventStep,max_t);
        continue;
      }

      // There was an event
      mCurrentEventData = problem;       
      if (mUseBestInterpolation) mSolverEngine.bestInterpolate(problem.getTime(), state); // So that we reinitialize with the best possible approximation 
      else for (var i=0; i<mDimension; i++) state[i] = mTest_ode_state[i]; // -- unnecessary -- interpolatorSolver.interpolate(eventData.time, state);
      var timeBefore = state[mTimeIndex];
      problem.action();
      self.reinitialize();
      state = mODE.getState(); // who knows if the event changes the ODE array pointer?
      if (timeBefore!=state[mTimeIndex]) { // If the event changes the time, it is meaningless to try to complete a step
        mZenoCounter = 0;
        mLastEventData = null;
        mCurrentEventData = null;
        problem.reset(state); // and that this event just happened has no meaning
        return (problem.getTime()-tBegin);
      }
      if (mLastEventData!=null) {
        if (Math.abs(mLastEventDataTime-problem.getTime())<mProximityThreshold) {
          mZenoCounter++;
        }
        else mZenoCounter = 0;
      }
      mLastEventData = problem;
      mLastEventDataTime = problem.getTime(); // because the internal value of lastEventData will change
      return (problem.getTime()-tBegin);
    }
  }

  /**
   * Steps the ODE with events
   * @return
   */
  function stepWithEvents() {
    var state = mODE.getState();
    if (mZenoMaximumAllowedTimes>0 && mZenoCounter>mZenoMaximumAllowedTimes) {
      if (callZenoAction(state)) return 0;
    }

    var tBegin = state[mTimeIndex];

    // Check for a 0 rate for time
    var rate = mSolverEngine.getCurrentRate();
    if (tBegin+rate[mTimeIndex]==tBegin) return 0;

    var tEnd = tBegin + mStepSize;
    
//    discontinuityAtEnd = null; // Do NOT clean the possible discontinuity
    var max_t = mSolverEngine.getMaximumTime(mHasDiscontinuities); // This detects discontinuities, if there are any
    if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver at "+state[mTimeIndex]);
    var tTest = mRunsForwards ? Math.min(tBegin+mMaxEventStep,tEnd) : Math.max(tBegin-mMaxEventStep,tEnd);

    var counter = 0;
    while (true) {
      var problem=null;
      mCurrentEventData = null;
      var notYetThere = mRunsForwards ? max_t<tTest : max_t>tTest;
      if (notYetThere) { // This is required because the interpolatorSolver cannot do an interpolation backwards from max_t
        mSolverEngine.bestInterpolate(max_t, mTest_ode_state);
        problem = self.findFirstEvent(state,max_t,mTest_ode_state); // Find the first event
        if (problem==null) problem = mDiscontinuityAtEnd; // If no event, check for discontinuity (but regular events are acted on first)
        if (problem==null) {
          for (var i=0; i<mDimension; i++) state[i] = mTest_ode_state[i]; // Update the current state
          // faster than interpolatorSolver.interpolate(max_t, state), since test_ode_state doesn't change if no event is found
          updateEventsAndDiscontinuities(state[mTimeIndex]);
          max_t = mSolverEngine.internalStep(mHasDiscontinuities);
          if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver looking for an event at "+state[mTimeIndex]);
          if ((++counter)>mMaxInternalSteps) return self.error (EJSS_ODE_SOLVERS.ERROR.TOO_MANY_STEPS_ERROR,"The solver exceeded the number of internal steps at "+state[mTimeIndex]);
          continue;
        }
      }
      else {
        doTheInterpolation (tTest, mTest_ode_state);
        problem = self.findFirstEvent(state,tTest,mTest_ode_state); // Find the first event
        if (problem==null && mDiscontinuityAtEnd!=null && mDiscontinuityAtEnd.time<=tTest) problem = mDiscontinuityAtEnd; // If no event, check for discontinuity (but regular events are acted on first)
        if (problem==null) {
          if (tTest==tEnd) {
            for (var i=0; i<mDimension; i++) state[i] = mTest_ode_state[i];
            updateEventsAndDiscontinuities(state[mTimeIndex]);
            return tEnd-tBegin;
          }
          tTest = mRunsForwards ? Math.min(tTest+mMaxEventStep,tEnd) : Math.max(tTest-mMaxEventStep,tEnd);
          continue;
        }
      }
      // There was an event or a discontinuity
      mCurrentEventData = problem;
      if (mUseBestInterpolation) mSolverEngine.bestInterpolate(problem.getTime(), state); // So that we reinitialize with the best possible approximation 
      else for (var i=0; i<mDimension; i++) state[i] = mTest_ode_state[i]; // -- unnecessary -- interpolatorSolver.interpolate(eventData.time, state);
      var timeBefore = state[mTimeIndex];
      var wantsToQuit = problem.action();
      self.reinitialize();
      counter = 0;
      state = mODE.getState(); // who knows if the event changes the ODE array pointer?
      if (timeBefore!=state[mTimeIndex]) { // If the event changes the time, it is meaningless to try to complete a step
        mZenoCounter = 0;
        mLastEventData = null;
        mCurrentEventData = null;
        problem.reset(state);
        return (problem.getTime()-tBegin);
      }
      if (mLastEventData!=null) {
        if (Math.abs(mLastEventDataTime-problem.getTime())<mProximityThreshold) {
          mZenoCounter++;
          if (mCoalesceCloseEvents) wantsToQuit = false; // If the events are too close, do not quit (this avoids unnecessary redraw)
        }
        else mZenoCounter = 0;
      }
      mLastEventData = problem;
      mLastEventDataTime = problem.getTime(); // because the internal value of lastEventData will change
      if (wantsToQuit) return (problem.getTime()-tBegin);
      if (mZenoMaximumAllowedTimes>0 && mZenoCounter>mZenoMaximumAllowedTimes) {
        if (callZenoAction(state)) return (problem.getTime()-tBegin);
      }
//      if (notYetThere) { // Respect the user's reading step (Is this really a good idea???)
//        max_t = runsForwards ? Math.min(max_t, interpolatorSolver.getMaximumTime()) : Math.max(max_t, interpolatorSolver.getMaximumTime());
//      }
//      else 
      mDiscontinuityAtEnd = null;
      max_t = mSolverEngine.getMaximumTime(mHasDiscontinuities); // This detects discontinuities, if there are any

      if (!isNaN(problem.getMaxAdvance())) { // If a short-duration step was found, this helps avoid recurrent events
        max_t = mRunsForwards ? Math.min(max_t, problem.getMaxAdvance()) : Math.max(max_t, problem.getMaxAdvance());  
      }
      if (isNaN(max_t)) return self.error (EJSS_ODE_SOLVERS.ERROR.INTERNAL_SOLVER_ERROR,"Error when stepping the solver after an event at "+state[mTimeIndex]);
    } // end of while
  }

  function updateEventsAndDiscontinuities (time) {
    var i, length;    
    length = mEventList.length;
    for (i=0; i<length; i++) { // Update the events
      var data = mEventList[i];
      data.findPosition(time,data.getH());
    }
    length = mDiscontinuityList.length;
    for (i=0; i<length; i++) { // Update the discontinuities
      var data = mDiscontinuityList[i];
      data.findPosition(time,data.getH());
    }
  }
  
  /**
   * A Zeno effect has been detected. Call the listeners.
   * If there are no listeners registered, then an error is issued, including possibly throwing an exception
   * @return true if the solver should stop the step and return a 0 step size
   */
  function callZenoAction(state) {
    if (mZenoList.length<=0) {
      self.error (EJSS_ODE_SOLVERS.ERROR.ZENO_EFFECT,"A Zeno-like effect has been detected.\nLast event was "+
          mLastEventData.getProblem() +" which took place at "+mLastEventDataTime);
      return true;
    }
    var returnAtZeno = false;
    var i;
    var length = mZenoList.length;
    for (i=0; i<length; i++) {
      var listener = mZenoList[i];
      if (listener.zenoEffectAction(mLastEventData.getProblem(), state)) returnAtZeno = true;
    }
    mZenoCounter = 0;
    return returnAtZeno; 
  }

  /**
   * Returns the first event that will take place next in the interval [current_state[timeIndex],tFinal].
   * @param double[] final_state contains the state at tFinal, if an event is found, it will contain the state at the event.
   * If no event is found, the final_state array will remain untouched.
   * The interpolator solver must be able to interpolate correctly in this interval!!!
   * Returns null if no event takes place in that interval. 
   */
  self.findFirstEvent = function(current_state, tFinal, final_state) {
    var i,length;
    mNumberOfAttempts = 0;
    // -- Find which events have happened
    var happensAtT1 = happensRightNow(current_state[mTimeIndex],final_state, mHappened,"at t1");
    if (happensAtT1!=null) {
      happensAtT1.time = current_state[mTimeIndex]; 
      happensAtT1.maxAdvance=tFinal; // limit the advance of the solver from this event. This helps avoid recurrent events
      for (i=0; i<mDimension; i++) final_state[i] = current_state[i];
      return happensAtT1;
    }
    // -- Check for no event
    if (mHappened.length<=0) return null; // IMPORTANT! final_state remains unchanged

    // --  Search until found or maximum of attempts is reached
    var doItAgain = true;
    length = mEventList.length;
    
    for (i=0; i<length; i++) {
      var eventData = mEventList[i];
      eventData.setHAfter(eventData.getH()); // evaluated in happensRightNow() (since no event happened at T1)
    } 
    while (doItAgain) {
      mNumberOfAttempts++;
      var tTest = nextPointToCheck (mHappened, current_state[mTimeIndex], tFinal); // The value of t where the event is expected to be
      doTheInterpolation (tTest, mIntermediate_ode_state);
      // Check if we discover an event at t1. This (short duration) event was not detected in [t1,tFinal]
      var shortDurationEvent = happensRightNow(current_state[mTimeIndex],mIntermediate_ode_state, mTemp_list, "short");
      if (shortDurationEvent!=null) { // This was a short-duration event!
        shortDurationEvent.time = current_state[mTimeIndex]; 
        shortDurationEvent.maxAdvance=tTest; // Try to avoid infinite loops
        for (i=0; i<mDimension; i++) final_state[i] = current_state[i];
        return shortDurationEvent;
      }
      
      if (mTemp_list.length<=0) { // No event in [t1,tTest]
        // check for event in tTest. This is a reduced check because all other possibilities were dealt with in happensRightNow()
        var happensInTtest=null;
        length = mHappened.length;
        for (i=0; i<length; i++) {
          var eventData = mHappened[i];
          if (eventData.getCurrentPosition()==EJSS_ODE_SOLVERS.ProblemData.POSITIVE) { 
            if (eventData.getH()<eventData.getEvent().getTolerance()) { happensInTtest = eventData; break; } // This one is the event
          }
          else { // NEGATIVE and crossing event
            if (eventData.getH()>-eventData.getEvent().getTolerance()) { happensInTtest = eventData; break; } // This one is the event
          }
        }
        if (happensInTtest!=null) { 
          happensInTtest.time = tTest; 
          happensInTtest.maxAdvance=tFinal; //Number.NaN;
          for (i=0; i<mDimension; i++) final_state[i] = mIntermediate_ode_state[i];
          return happensInTtest;
        }
        // repeat in [tTest,tFinal]
        for (i=0; i<mDimension; i++) current_state[i] = mIntermediate_ode_state[i];
        length = mEventList.length;
        for (i=0; i<length; i++) {
          var eventData = mEventList[i];
          eventData.findPosition(current_state[mTimeIndex],eventData.getH());
        }
        // -- Find which events have happened
        var happensNowInTtest = happensRightNow(current_state[mTimeIndex],final_state, mHappened, "at tTest");
        if (happensNowInTtest!=null) {
          happensNowInTtest.time = current_state[mTimeIndex]; 
          happensNowInTtest.maxAdvance=tFinal; //Number.NaN;
          for (i=0; i<mDimension; i++) final_state[i] = current_state[i];
          return happensNowInTtest;
        }
      }
      else { // temp_list is not null. I.e. there is at least one event in [t1,tTest] 
        // check for event in tTest. This is a reduced check because all other possibilities were dealt with in happensRightNow()
        // If there is any event previous to this, then this is not the point we are looking for.
        var notPreviousFound=true;
        var happensInTtest=null;
        length = mTemp_list.length;
        for (i=0; i<length; i++) {
          var data = mTemp_list[i];
          if (data.getCurrentPosition()==EJSS_ODE_SOLVERS.ProblemData.POSITIVE) { 
            if (data.getH()<=-data.getEvent().getTolerance()) { notPreviousFound = false; break; } // I happened earlier, so this is not the point
            // else if (data.getH()<data.getGeneralEvent().getTolerance()) no need to check since data.h < 0
            happensInTtest = data; // If there is no previous one, then it is me
          }
          else { // NEGATIVE and crossing event
            if (data.getH()>=data.getEvent().getTolerance()) { notPreviousFound = false; break; } // I happened earlier, so this is not the point
            // else if (data.h>-data.generalEvent.getTolerance()) // no need to check since data.h>0 
            happensInTtest = data; // If there is no previous one, then it is me
          }
        }
        if (notPreviousFound && happensInTtest!=null) { // This is the event!
          happensInTtest.time = tTest; 
          happensInTtest.maxAdvance=tFinal; //Number.NaN;
          for (i=0; i<mDimension; i++) final_state[i] = mIntermediate_ode_state[i];
          return happensInTtest; 
        }
        tFinal = tTest; 
        for (i=0; i<mDimension; i++) final_state[i] = mIntermediate_ode_state[i];
        length = mEventList.length;
        for (i=0; i<length; i++) {
          var eventData = mEventList[i];
          eventData.setHAfter(eventData.getH());
        }
        mHappened = [];
        length = mTemp_list.length;
        for (i=0; i<length; i++) mHappened.push(mTemp_list[i]);
      }
//       Check for the iteration limit
//      for (EventData data : happened) {
//        System.err.println ("Happened "+data.generalEvent+"\nCurrent state: ");
//        for (int i=0; i<dimension; i++) System.err.print(current_state[i]+", ");  
//        System.err.println ("\nFinal   state: ");
//        for (int i=0; i<dimension; i++) System.err.print(final_state[i]+", ");  
//        System.err.println ("\n");
//      }
      length = mHappened.length;
      for (i=0; i<length; i++) {
        var data = mHappened[i];
        if (mNumberOfAttempts>data.getEvent().getMaxIterations()) { doItAgain = false; break; }
      }
    }
    
    // If this happens, the event is most likely poorly designed!
    var remaining = mHappened[0]; // The event is any of those which remain in the list of happened
    self.error (EJSS_ODE_SOLVERS.ERROR.EVENT_NOT_FOUND,"Warning : Event not found after "+ mNumberOfAttempts+ " attempts at t=" + current_state[mTimeIndex]
        + " h = "+ remaining.getH()
        + ".\nPlease check the code of your event, decrease the initial step size, the tolerance of the solver,"
        +	"\nor the event maximum step, or increase the maximum number of attempts."
        + "\nFirst event remaining in the queue: "+remaining.getEvent());

    remaining.setTime((current_state[mTimeIndex]+tFinal)/2);
    remaining.setMaxAdvance(Number.NaN);
    mSolverEngine.bestInterpolate(remaining.time, final_state);
    return remaining;
  };
  
  /**
   * Fills the list with all events that happen between the current time and the provided final time.
   * Returns an event if it happens at the initial point, null otherwise
   */
  function happensRightNow(currentTime, final_state, list, id) {
    list.splice(0,list.length);
    var length = mEventList.length;
    for (var i=0; i<length; i++) {
      var eventData = mEventList[i];
      eventData.setH(eventData.getEvent().evaluate(final_state));
      switch (eventData.getCurrentPosition()) {
        default : 
        case EJSS_ODE_SOLVERS.ProblemData.POSITIVE : if (eventData.getH()<=0) list.push(eventData); break; // This event happens! 
        case EJSS_ODE_SOLVERS.ProblemData.SMALL_POSITIVE :
          if (eventData.getH()<=0 && (eventData.hasPositiveFlag() || eventData.getEventType()==EJSS_ODE_SOLVERS.EVENT_TYPE.STATE_EVENT)) return eventData;
          break;
        case EJSS_ODE_SOLVERS.ProblemData.ZERO :
          if (eventData.getH()<0) {
            if (eventData.hasPositiveFlag() || eventData.getEventType()==EJSS_ODE_SOLVERS.EVENT_TYPE.STATE_EVENT) return eventData;
          }
          else if (eventData.getH()>0) {
            if (eventData.hasNegativeFlag() && eventData.getEventType()==EJSS_ODE_SOLVERS.EVENT_TYPE.CROSSING_EVENT) return eventData;
          }
          break;
        case EJSS_ODE_SOLVERS.ProblemData.SMALL_NEGATIVE :
          if (eventData.getEventType()==EJSS_ODE_SOLVERS.EVENT_TYPE.STATE_EVENT) {
            if (eventData.getH()<=-eventData.getEvent().getTolerance()) return eventData;
          }
          else if (eventData.getEventType()==EJSS_ODE_SOLVERS.EVENT_TYPE.CROSSING_EVENT) {
            if (eventData.hasNegativeFlag() && eventData.getH()>=0) return eventData;
          }
          break;
        case EJSS_ODE_SOLVERS.ProblemData.NEGATIVE : 
          if (eventData.getEventType()==EJSS_ODE_SOLVERS.EVENT_TYPE.CROSSING_EVENT) { if (eventData.getH()>=0) list.push(eventData); }
          else if (eventData.getEventType()==EJSS_ODE_SOLVERS.EVENT_TYPE.STATE_EVENT) { // This should NOT happen, but I save the problem by saying that this event happens right now!
            self.error (EJSS_ODE_SOLVERS.ERROR.ILLEGAL_EVENT_STATE, "The system started from an illegal state at "+currentTime+ " for the state event "+eventData.getEvent());
            return eventData;
          }
          break; 
      } // end of switch
    } // end of for
    return null;
  }
  
  /**
   * The first point where events estimate the crossing takes place
   */
  function nextPointToCheck (list, t1, t2) {
    var i, length = list.length;
    var hB, tFirst = t2, dt = (t2 - t1), tMiddle = (t1+t2)/2;
    if (mRunsForwards) {
      for (i=0; i<length; i++) {
        var eventData = list[i];
        switch (eventData.getEvent().getRootFindingMethod()) {
          default : 
          case EJSS_ODE_SOLVERS.EVENT_METHOD.BISECTION : 
            tFirst = Math.min(tFirst, tMiddle); 
            break;
          case EJSS_ODE_SOLVERS.EVENT_METHOD.SECANT : 
//          System.err.println ("hBefore = "+eventData.hBefore);
//          System.err.println ("hAfter = "+eventData.hAfter);
//          System.err.println ("dt = "+dt);
//          System.err.println ("t1 = "+t1);
//          System.err.println ("tFirst = "+tFirst);
            hB = eventData.getHBefore();
            tFirst = Math.min(tFirst, t1 - hB * dt / (eventData.getHAfter() - hB)); 
            break;
        }
      }
    }
    else {
      for (i=0; i<length; i++) {
        var eventData = list[i];
        switch (eventData.getEvent().getRootFindingMethod()) {
          default : 
          case EJSS_ODE_SOLVERS.EVENT_METHOD.BISECTION : 
            tFirst = Math.max(tFirst, tMiddle); 
            break;
          case EJSS_ODE_SOLVERS.EVENT_METHOD.SECANT : 
            hB = eventData.getHBefore();
            tFirst = Math.max(tFirst, t1 - hB * dt / (eventData.getHAfter() - hB)); 
            break;
        }
      }
    }
    return tFirst;
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
 
    if (mODE.getDelays) { // It is a DelayDifferentialEquation
      mDDEdiscontinuity = EJSS_ODE_SOLVERS.ddeDiscontinuity(self,mODE);
    }
    mSolverEngine.setODE(self, mODE);
  
  return self;
}
