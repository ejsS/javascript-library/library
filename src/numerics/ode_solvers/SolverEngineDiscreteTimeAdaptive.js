/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 * @author Francisco Esquembre
 * @version 1.0 Jan 2014
 */
EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive = {
  FAC1 : 0.33,
  FAC2 : 6,
  BETA : 0,
  SAFE : 0.9
};

/**
 * Constructor for SolverEngineDiscreteTimeAdaptive
 * @returns An abstract SolverEngineDiscreteTimeAdaptive
 */
EJSS_ODE_SOLVERS.solverEngineDiscreteTimeAdaptive = function(mMethodOrder) {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTime(); // reference returned 

  // step size estimation's variables
  var mExpO1 = 1.0 / mMethodOrder - EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.BETA * 0.75;
  var mErrOld = 1.e-4;
  var mFactor = 0;

  // variables
  var mEstimate=false; // Whether to mEstimate the first step size
  var mActualStepSize;
  var mAbsoluteTolerance=Number.NaN;
  var mRelativeTolerance=Number.NaN;
  var mAbsTol; // array of absolute tolerances
  var mRelTol; // array of relative tolerances
  
  var superReinitialize = self.reinitialize;
  var superSetMaximumStepSize = self.setMaximumStepSize;

  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  /**
   * Computes the proposed approximation for finalState[] and
   * returns the estimated error obtained
   * @return the estimated error
   */
  self.computeApproximation = function(step) {};

  // --------------------------------------------
  // Protected getters
  // --------------------------------------------
  
  self.getAbsTol = function() { return mAbsTol; }

  self.getRelTol = function() { return mRelTol; }
  
  // --------------------------------------------
  // Functions overriden
  // --------------------------------------------

  self.allocateOtherArrays = function() {
    var dimension = self.getDimension();
    mAbsTol = new Array(dimension);
    mRelTol = new Array(dimension);
    self.setTolerances(1.0e-6,1.0e-6);
  }

  self.reinitialize = function(state) {
    superReinitialize(state);
    if (mEstimate) mActualStepSize = limitStepSize(estimateFirstStepSize(self.getStepSize()));
    else mActualStepSize = limitStepSize(self.getStepSize());
  }

  self.setEstimateFirstStep = function(estimate) {
    mEstimate = estimate;
  }

  self.setMaximumStepSize = function(stepSize) {
    superSetMaximumStepSize(stepSize);
    mActualStepSize = limitStepSize(mActualStepSize);
  }
  
  self.setTolerances = function(absTol, relTol) {
    var i;
    var dimension = self.getDimension();
    if (absTol instanceof Array) {
      mAbsoluteTolerance = mRelativeTolerance = Number.NaN;
      for (i=0; i<dimension; i++) {
        mAbsTol[i] = absTol[i];
        mRelTol[i] = relTol[i];
      }
    }
    else {
      if (mAbsoluteTolerance==absTol && mRelativeTolerance==relTol) return;
      mAbsoluteTolerance = absTol;
      mRelativeTolerance = relTol;
      for (i=0; i<dimension; i++) {
        mAbsTol[i] = absTol;
        mRelTol[i] = relTol;
      }
    }
    mFinalTime = Number.NaN;
    mErrorCode = EJSS_ODE_SOLVERS.ERROR.NO_ERROR;
    if (mEstimate) mActualStepSize = limitStepSize(estimateFirstStepSize(self.getStepSize()));
    else mActualStepSize = limitStepSize(self.getStepSize());
  }
  
  self.computeOneStep = function(hasDiscontinuities) {
    var eventSolver = self.getEventSolver();
    var finalState = self.getFinalState();
    
    mErrorCode = EJSS_ODE_SOLVERS.ERROR.NO_ERROR;
    for (var iterations=0; iterations<500; iterations++) { // maximum number of attempts
      if (hasDiscontinuities) {
        switch (self.computeCarefulIntermediateStep(eventSolver, mActualStepSize, finalState)) {
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR : 
            self.setFinalTime(Number.NaN); 
            return;
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.NO_DISCONTINUITY_ALONG_STEP  :  // no discontinuity in this interval
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_EXACTLY_ON_STEP : // discontinuity just at the end of the step
            self.setFinalTime(finalState[self.getTimeIndex()]);
            break; 
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED :
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_ALONG_STEP : // Iterate until finding the precise point of the discontinuity
            var finalTime = self.findTheDiscontinuity(mActualStepSize);
            self.setFinalTime(finalTime);
            mActualStepSize = finalTime - self.getInitialTime();
            break;
        }
        if (isNaN(self.getFinalTime())) {
          self.setErrorCode(EJSS_ODE_SOLVERS.ERROR.DISCONTINUITY_PRODUCED_ERROR);
          return;
        }
      }
      else {
        self.computeIntermediateStep(mActualStepSize,finalState);
        self.setFinalTime(finalState[self.getTimeIndex()]);
      }
      var err = self.computeApproximation(mActualStepSize);
      self.addToEvaluations(self.getNumberOfEvaluations());
      if (err<=1.0) {  // It has converged
        // Clean memory, if required
        self.getStateHistory().clean(self.getInitialTime());
        // Add new interval data to memory
        self.getStateHistory().addIntervalData(self.computeFinalRateAndCreateIntervalData());

        // adjust the step for next time
        if (iterations>0) mActualStepSize = limitStepSize(mActualStepSize>0 ? Math.min(mActualStepSize, estimatedStepSize(err)) : Math.max(mActualStepSize, estimatedStepSize(err)));
        else mActualStepSize = limitStepSize(estimatedStepSize(err)); // Can grow only if converged at first attempt
        return;
      }
      mActualStepSize = limitStepSize(mActualStepSize>0 ? Math.min(mActualStepSize, estimatedStepSize(err)) : Math.max(mActualStepSize, estimatedStepSize(err)));
    }
    // It did not converge
    self.setFinalTime(Number.NaN);
    self.setErrorCode(EJSS_ODE_SOLVERS.ERROR.DID_NOT_CONVERGE);
  }
  
  self.getActualStepSize = function() { return mActualStepSize; }

  // --------------------------------------------
  // Private or protected functions
  // --------------------------------------------

  /**
   * Based on code by Andrei Goussev and Yuri B. Senichenkov 
   * Adapted by Francisco Esquembre
   * @param compState the state to compare to
   * @return
   */
  self.computeError = function(state) {
    var error = 0;
    var dimension = self.getDimension();
    var initialState = self.getInitialState();
    var finalState = self.getFinalState();
    for(var i = 0; i < dimension; i++) {
      var sk = mAbsTol[i] + mRelTol[i] * Math.max(Math.abs(finalState[i]), Math.abs(initialState[i]));
      var errorI = (finalState[i]-state[i])/sk;
      error += errorI*errorI;
    }
    return Math.sqrt(error/dimension);
  }

  /**
   * Based on code by Andrei Goussev and Yuri B. Senichenkov 
   * Adapted by Francisco Esquembre
   * @param err
   * @return
   */
  function estimatedStepSize(err){
    var fac11 = 0;
    // taking decision for HNEW/H value
    if (err != 0) {
      fac11 = Math.pow(err,mExpO1);
      mFactor = fac11 / Math.exp(EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.BETA * Math.log(mErrOld)); // stabilization
      mFactor = Math.max (1.0/EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.FAC2, 
                  Math.min(1.0/EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.FAC1, 
                           mFactor/EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.SAFE)); // we require FAC1 <= HNEW/H <= FAC2
    }
    else {
      fac11 = 1.0/EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.FAC1;
      mFactor = 1.0/EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.FAC2;
    }
    if (err<=1.0) { // step accepted
      mErrOld = Math.max(err, 1.0e-4);
      return mActualStepSize / mFactor;
    }
    return mActualStepSize/Math.min(1.0/EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.FAC1, fac11/EJSS_ODE_SOLVERS.SolverEngineDiscreteTimeAdaptive.SAFE); // step rejected
  }

  /**
   * Makes sure the intended step does not exceed the maximum step
   * @param intendedStep
   * @return
   */
  function limitStepSize(intendedStep) {
    var ode = self.getODE();
    var delays;
    var i, length;
	if (intendedStep>=0) {
      if (ode.getDelays) { // Don't step more than half the minimum delay
	    delays = ode.getDelays(self.getInitialState());
	    length = delays.length;
	    for (i=0; i<length; i++) intendedStep = Math.min(intendedStep, delays[i]/2);
      }
	  return Math.min(intendedStep, self.getMaximumStepSize());
	}
    if (ode.getDelays) { // Don't step more than half the minimum delay
      delays = ode.getDelays(self.getInitialState());
      length = delays.length;
	  for (i=0; i<length; i++) intendedStep = Math.max(intendedStep, delays[i]/2);
	}
    return Math.max(intendedStep, -self.getMaximumStepSize()); 
  }
  
  /**
   * Based on code by Andrei Goussev and Yuri B. Senichenkov 
   * Adapted by Francisco Esquembre
   * @param hMax
   * @return
   */
  function estimateFirstStepSize(hMax){
    var i;
    var dimension = self.getDimension();
    var initialState = self.getInitialState();
    var initialRate = self.getInitialRate();
    var ode = self.getODE();
    
    var posneg =(hMax<0)?-1:1;
    hMax = Math.abs(hMax);
    var normF = 0.0, normX = 0.0 ;
    for(i = 0; i < dimension; i++) {
      var sk = mAbsTol[i] + mRelTol[i]*Math.abs(initialState[i]);
      var aux = initialRate[i]/sk;
      normF += aux*aux;
      aux = initialState[i]/sk;
      normX += aux*aux;
    }
    var h;
    if ((normF <= 1.e-10) || (normX <= 1.e-10)) h = 1.0e-6;
    else h = Math.sqrt(normX / normF) * 0.01;
    h = posneg*Math.min(h, hMax);
    // perform an Euler step and estimate the rate, reusing finalState (it is SAFE to do so)
    if (ode.getDelays) {
      var eventSolver = self.getEventSolver(); 
      eventSolver.resetDiscontinuities(initialState);
      var counter = 0;
      var done = false;
      while (!done) {
        for (i = 0; i < dimension; i++) finalState[i] = initialState[i] + h * initialRate[i];
        switch (eventSolver.checkDiscontinuity(finalState, false)) {
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR  : return Number.NaN; 
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED     :
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_ALONG_STEP      : h = h/2; break;
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_EXACTLY_ON_STEP : done = true; break;  
          case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.NO_DISCONTINUITY_ALONG_STEP   : done = true; break; 
        }
        if ((++counter)>100) return Number.NaN;
      }
    }
    else {
      for (i = 0; i < dimension; i++) 
        finalState[i] = initialState[i] + h * initialRate[i];
    }
    ode.getRate(finalState, finalRate);
    var der2 = 0.0;
    for (i = 0; i < dimension; i++) {
      var sk = mAbsTol[i] + mRelTol[i] * Math.abs(initialState[i]);
      var aux = (finalRate[i] - initialRate[i]) / sk;
      der2 += aux*aux;
    }
    der2 = Math.sqrt(der2) / h;
    //step size is computed as follows
    //h^order * max ( norm (initialRate), norm (der2)) = 0.01
    var der12 = Math.max(Math.abs(der2), Math.sqrt(normF));
    var h1;
    if (der12 <= 1.0e-15) h1 = Math.max(1.0e-6, Math.abs(h) * 1.0e-3);
    else h1 = Math.exp((1.0 / mMethodOrder) * Math.log(0.01 / der12));
    h = posneg*Math.min(100*h, h1);
    if (hMax != 0) h = posneg*Math.min(Math.abs(h),hMax);
    return h;
  }
  
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
}