/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.Fehlberg8 = {

};

/**
 * Constructor for Fehlberg8
 * @returns Fehlberg8
 */
EJSS_ODE_SOLVERS.fehlberg8 = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTime(); // reference returned 
  var mRate2, mRate3, mRate4, mRate5, mRate6, mRate7, mRate8, mRate9, mRate10, mRate11; // mRate12, mRate13;

  self.getNumberOfEvaluations = function() { return 13; }

  self.allocateOtherArrays = function() {
    mRate2 = new Array(self.getDimension());
    mRate3 = new Array(self.getDimension());
    mRate4 = new Array(self.getDimension());
    mRate5 = new Array(self.getDimension());
    mRate6 = new Array(self.getDimension());
    mRate7 = new Array(self.getDimension());
    mRate8 = new Array(self.getDimension());
    mRate9 = new Array(self.getDimension());
    mRate10 = new Array(self.getDimension());
    mRate11 = new Array(self.getDimension());
//    mRate12 = new Array(self.getDimension());
//    mRate13 = new Array(self.getDimension());
  };

  self.computeIntermediateStep = function(step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
        
    var CTS = EJSS_ODE_SOLVERS.Fehlberg78;
    var i;
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    ode.getRate(state, mRate2);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i]+CTS.A_22*mRate2[i]);
    ode.getRate(state, mRate3);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_33*mRate3[i]);
    ode.getRate(state, mRate4);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    ode.getRate(state, mRate5);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    ode.getRate(state, mRate6);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_61*initialRate[i]+CTS.A_64*mRate4[i]+CTS.A_65*mRate5[i]+CTS.A_66*mRate6[i]);
    ode.getRate(state, mRate7);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_71*initialRate[i]+CTS.A_75*mRate5[i]+CTS.A_76*mRate6[i]+CTS.A_77*mRate7[i]);
    ode.getRate(state, mRate8);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_81*initialRate[i]+CTS.A_84*mRate4[i]+
        CTS.A_85*mRate5[i]+CTS.A_86*mRate6[i]+CTS.A_87*mRate7[i]+CTS.A_88*mRate8[i]);
    ode.getRate(state, mRate9);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_91*initialRate[i]+CTS.A_94*mRate4[i]+
        CTS.A_95*mRate5[i]+CTS.A_96*mRate6[i]+CTS.A_97*mRate7[i]+CTS.A_98*mRate8[i]+CTS.A_99*mRate9[i]);
    ode.getRate(state, mRate10);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_101*initialRate[i]+CTS.A_104*mRate4[i]+
        CTS.A_105*mRate5[i]+CTS.A_106*mRate6[i]+CTS.A_107*mRate7[i]+CTS.A_108*mRate8[i]+CTS.A_109*mRate9[i]+CTS.A_1010*mRate10[i]);
    ode.getRate(state, mRate11);
//    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_111*initialRate[i]+
//        CTS.A_116*mRate6[i]+CTS.A_117*mRate7[i]+CTS.A_118*mRate8[i]+CTS.A_119*mRate9[i]+CTS.A_1110*mRate10[i]);
//    ode.getRate(state, mRate12);
//    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_121*initialRate[i]+CTS.A_124*mRate4[i]+
//        CTS.A_125*mRate5[i]+CTS.A_126*mRate6[i]+CTS.A_127*mRate7[i]+CTS.A_128*mRate8[i]+CTS.A_129*mRate9[i]+CTS.A_1210*mRate10[i]+CTS.A_1212*mRate12[i]);
//    ode.getRate(state, mRate13);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B7_1*initialRate[i]+CTS.B7_6*mRate6[i]+CTS.B7_7*mRate7[i]+CTS.B7_8*mRate8[i]+CTS.B7_9*mRate9[i]+CTS.B7_10*mRate10[i]+CTS.B7_11*mRate11[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex]; 
  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    var timeRate = initialRate[timeIndex];
    var CTS = EJSS_ODE_SOLVERS.Fehlberg78;
    var CODE = EJSS_ODE_SOLVERS.DISCONTINUITY_CODE;
    var i;
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate2);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i]+CTS.A_22*mRate2[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate3);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_33*mRate3[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate4);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate5);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate6);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_61*initialRate[i]+CTS.A_64*mRate4[i]+CTS.A_65*mRate5[i]+CTS.A_66*mRate6[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate7);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_71*initialRate[i]+CTS.A_75*mRate5[i]+CTS.A_76*mRate6[i]+CTS.A_77*mRate7[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate8);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_81*initialRate[i]+CTS.A_84*mRate4[i]+
        CTS.A_85*mRate5[i]+CTS.A_86*mRate6[i]+CTS.A_87*mRate7[i]+CTS.A_88*mRate8[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate9);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_91*initialRate[i]+CTS.A_94*mRate4[i]+
        CTS.A_95*mRate5[i]+CTS.A_96*mRate6[i]+CTS.A_97*mRate7[i]+CTS.A_98*mRate8[i]+CTS.A_99*mRate9[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate10);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_101*initialRate[i]+CTS.A_104*mRate4[i]+
        CTS.A_105*mRate5[i]+CTS.A_106*mRate6[i]+CTS.A_107*mRate7[i]+CTS.A_108*mRate8[i]+CTS.A_109*mRate9[i]+CTS.A_1010*mRate10[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate11);

//    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_111*initialRate[i]+
//        CTS.A_116*mRate6[i]+CTS.A_117*mRate7[i]+CTS.A_118*mRate8[i]+CTS.A_119*mRate9[i]+CTS.A_1110*mRate10[i]);
//    switch (eventSolver.checkDiscontinuity(state, false)) {
//      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
//      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
//      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
//      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
//      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
//    }
//    mODE.getRate(state, mRate12);
//
//    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_121*initialRate[i]+CTS.A_124*mRate4[i]+
//        CTS.A_125*mRate5[i]+CTS.A_126*mRate6[i]+CTS.A_127*mRate7[i]+CTS.A_128*mRate8[i]+CTS.A_129*mRate9[i]+CTS.A_1210*mRate10[i]+CTS.A_1212*mRate12[i]);
//    switch (eventSolver.checkDiscontinuity(state, false)) {
//      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
//      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
//      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
//      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
//      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
//    }
//    mODE.getRate(state, mRate13);

    for (i=0; i<timeIndex; i++)  state[i] = initialState[i] + step*(CTS.B7_1*initialRate[i]+CTS.B7_6*mRate6[i]+CTS.B7_7*mRate7[i]+CTS.B7_8*mRate8[i]+CTS.B7_9*mRate9[i]+CTS.B7_10*mRate10[i]+CTS.B7_11*mRate11[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    
    ode.getRate(finalState, finalRate);
    return EJSS_ODE_INTERPOLATION.bootstrap2IntervalData(initialState, initialRate, finalState, finalRate, ode);
  }


  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}