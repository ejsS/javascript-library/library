/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.Fehlberg78 = {
  A_11 :   2.0/27.0,
  A_21 :   1.0/36.0, A_22 :  1.0/12.0,
  A_31 :   1.0/24.0,                   A_33 :  1.0/8.0,
  A_41 :   5.0/12.0,                   A_43 :-25.0/16.0, A_44 :  25.0/16.0,
  A_51 :   1.0/20.0,                                     A_54 :  1.0/4.0,  A_55 :   1.0/5.0,
  A_61 : -25.0/108.0,                                    A_64 :125.0/108.0, A_65 :-65.0/27.0, A_66 : 125.0/54.0,
  A_71 :  31.0/300.0,                                                       A_75 :  61.0/225.0, A_76 :  -2.0/9.0,   A_77 : 13.0/900.0,
  A_81 :   2.0,                                          A_84 :-53.0/6.0,   A_85 : 704.0/45.0,  A_86 :-107.0/9.0,   A_87 : 67.0/90.0 , 
    A_88 : 3.0,
  A_91 : -91.0/108.0,                                    A_94 : 23.0/108.0, A_95 : -976.0/135.0,A_96 : 311.0/54.0,  A_97 : -19.0/60.0,
    A_98 : 17.0/6.0,  A_99 : -1.0/12.0,
  A_101: 2383.0/4100.0,                                  A_104:-341.0/164.0,A_105: 4496.0/1025.0,A_106:-301.0/82.0, A_107 :2133.0/4100.0,
    A_108:45.0/82.0, A_109:45.0/164.0, A_1010 : 18.0/41.0,
  A_111: 3./205.0,                                                                               A_116:-6.0/41.0,   A_117 : -3.0/205.0,
    A_118:-3.0/41.0, A_119:3.0/41.0,   A_1110 : 6.0/41.0,
  A_121 : -1777.0/4100.0,                                A_124:-341.0/164.0,A_125:4496.0/1025.0, A_126:-289.0/82.0, A_127: 2193.0/4100.0,
    A_128:51.0/82.0, A_129:33.0/164.0, A_1210:12.0/41.0,                    A_1212:1.0,
	  
  // 7th order method
  B7_1 : 41.0/840.0, 
  B7_6 : 34.0/105.0, 
  B7_7 : 9.0/35.0, 
  B7_8 : 9.0/35.0, 
  B7_9 : 9.0/280.0, 
  B7_10 : 9.0/280.0, 
  B7_11 : 41.0/840.0,
		
  // 8th order error control
  B8_6 : 34.0/105.0, 
  B8_7 : 9.0/35.0, 
  B8_8 : 9.0/35.0, 
  B8_9 : 9.0/280.0, 
  B8_10 : 9.0/280.0, 
  B8_12 : 41.0/840.0, 
  B8_13 : 41.0/840.0
	  
};

/**
 * Constructor for Fehlberg78
 * @returns Fehlberg78
 */
EJSS_ODE_SOLVERS.fehlberg78 = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTimeAdaptive(7); // 7 is the method's order 
  var mRate2, mRate3, mRate4, mRate5, mRate6, mRate7, mRate8, mRate9, mRate10, mRate11, mRate12, mRate13;
  var superAllocateOtherArrays = self.allocateOtherArrays;

  self.getNumberOfEvaluations = function() { return 6; }
  
  self.allocateOtherArrays = function() {
    superAllocateOtherArrays();
    mRate2 = new Array(self.getDimension());
    mRate3 = new Array(self.getDimension());
    mRate4 = new Array(self.getDimension());
    mRate5 = new Array(self.getDimension());
    mRate6 = new Array(self.getDimension());
    mRate7 = new Array(self.getDimension());
    mRate8 = new Array(self.getDimension());
    mRate9 = new Array(self.getDimension());
    mRate10 = new Array(self.getDimension());
    mRate11 = new Array(self.getDimension());
    mRate12 = new Array(self.getDimension());
    mRate13 = new Array(self.getDimension());
    mOrder8 = new Array(self.getDimension());
  };

  self.computeIntermediateStep = function(step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    
	var CTS = EJSS_ODE_SOLVERS.Fehlberg78;
    var i;

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    ode.getRate(state, mRate2);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i]+CTS.A_22*mRate2[i]);
    ode.getRate(state, mRate3);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_33*mRate3[i]);
    ode.getRate(state, mRate4);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    ode.getRate(state, mRate5);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    ode.getRate(state, mRate6);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_61*initialRate[i]+CTS.A_64*mRate4[i]+CTS.A_65*mRate5[i]+CTS.A_66*mRate6[i]);
    ode.getRate(state, mRate7);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_71*initialRate[i]+CTS.A_75*mRate5[i]+CTS.A_76*mRate6[i]+CTS.A_77*mRate7[i]);
    ode.getRate(state, mRate8);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_81*initialRate[i]+CTS.A_84*mRate4[i]+
        CTS.A_85*mRate5[i]+CTS.A_86*mRate6[i]+CTS.A_87*mRate7[i]+CTS.A_88*mRate8[i]);
    ode.getRate(state, mRate9);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_91*initialRate[i]+CTS.A_94*mRate4[i]+
        CTS.A_95*mRate5[i]+CTS.A_96*mRate6[i]+CTS.A_97*mRate7[i]+CTS.A_98*mRate8[i]+CTS.A_99*mRate9[i]);
    ode.getRate(state, mRate10);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_101*initialRate[i]+CTS.A_104*mRate4[i]+
        CTS.A_105*mRate5[i]+CTS.A_106*mRate6[i]+CTS.A_107*mRate7[i]+CTS.A_108*mRate8[i]+CTS.A_109*mRate9[i]+CTS.A_1010*mRate10[i]);
    ode.getRate(state, mRate11);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_111*initialRate[i]+
        CTS.A_116*mRate6[i]+CTS.A_117*mRate7[i]+CTS.A_118*mRate8[i]+CTS.A_119*mRate9[i]+CTS.A_1110*mRate10[i]);
    ode.getRate(state, mRate12);
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_121*initialRate[i]+CTS.A_124*mRate4[i]+
        CTS.A_125*mRate5[i]+CTS.A_126*mRate6[i]+CTS.A_127*mRate7[i]+CTS.A_128*mRate8[i]+CTS.A_129*mRate9[i]+CTS.A_1210*mRate10[i]+CTS.A_1212*mRate12[i]);
    ode.getRate(state, mRate13);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B7_1*initialRate[i]+CTS.B7_6*mRate6[i]+CTS.B7_7*mRate7[i]+CTS.B7_8*mRate8[i]+CTS.B7_9*mRate9[i]+CTS.B7_10*mRate10[i]+CTS.B7_11*mRate11[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex]; 

  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    var CTS = EJSS_ODE_SOLVERS.Fehlberg78;
    var CODE = EJSS_ODE_SOLVERS.DISCONTINUITY_CODE;
    var i;
	    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate2);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i]+CTS.A_22*mRate2[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate3);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_33*mRate3[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate4);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate5);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate6);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_61*initialRate[i]+CTS.A_64*mRate4[i]+CTS.A_65*mRate5[i]+CTS.A_66*mRate6[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate7);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_71*initialRate[i]+CTS.A_75*mRate5[i]+CTS.A_76*mRate6[i]+CTS.A_77*mRate7[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate8);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_81*initialRate[i]+CTS.A_84*mRate4[i]+
        CTS.A_85*mRate5[i]+CTS.A_86*mRate6[i]+CTS.A_87*mRate7[i]+CTS.A_88*mRate8[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate9);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_91*initialRate[i]+CTS.A_94*mRate4[i]+
        CTS.A_95*mRate5[i]+CTS.A_96*mRate6[i]+CTS.A_97*mRate7[i]+CTS.A_98*mRate8[i]+CTS.A_99*mRate9[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate10);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_101*initialRate[i]+CTS.A_104*mRate4[i]+
        CTS.A_105*mRate5[i]+CTS.A_106*mRate6[i]+CTS.A_107*mRate7[i]+CTS.A_108*mRate8[i]+CTS.A_109*mRate9[i]+CTS.A_1010*mRate10[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate11);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_111*initialRate[i]+
        CTS.A_116*mRate6[i]+CTS.A_117*mRate7[i]+CTS.A_118*mRate8[i]+CTS.A_119*mRate9[i]+CTS.A_1110*mRate10[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate12);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_121*initialRate[i]+CTS.A_124*mRate4[i]+
        CTS.A_125*mRate5[i]+CTS.A_126*mRate6[i]+CTS.A_127*mRate7[i]+CTS.A_128*mRate8[i]+CTS.A_129*mRate9[i]+CTS.A_1210*mRate10[i]+CTS.A_1212*mRate12[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    mODE.getRate(state, mRate13);

    for (i=0; i<timeIndex; i++)  state[i] = initialState[i] + step*(CTS.B7_1*initialRate[i]+CTS.B7_6*mRate6[i]+CTS.B7_7*mRate7[i]+CTS.B7_8*mRate8[i]+CTS.B7_9*mRate9[i]+CTS.B7_10*mRate10[i]+CTS.B7_11*mRate11[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    ode.getRate(finalState, finalRate);
    return EJSS_ODE_INTERPOLATION.bootstrap2IntervalData(initialState, initialRate,finalState, finalRate, ode);

  }

  self.computeApproximation = function(step) {
    var timeIndex = self.getTimeIndex();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var CTS = EJSS_ODE_SOLVERS.Fehlberg78;

	for (var i=0; i<timeIndex; i++) mOrder8[i] = initialState[i]+step*(CTS.B8_6*mRate6[i]+CTS.B8_7*mRate7[i]+CTS.B8_8*mRate8[i]+CTS.B8_9*mRate9[i]+CTS.B8_10*mRate10[i]+CTS.B8_12*mRate12[i]+CTS.B8_13*mRate13[i]);
	mOrder8[timeIndex] = self.getInitialTime()  + step*initialRate[timeIndex];
	return self.computeError(mOrder8);
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}
