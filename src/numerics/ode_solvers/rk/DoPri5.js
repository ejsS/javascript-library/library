/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.DoPri5 = {
  A_11 :     1.0/5.0,
  A_21 :     3.0/40.0,   A_22 :      9.0/40.0,
  A_31 :    44.0/45.0,   A_32 :    -56.0/15.0,   A_33 :    32.0/9.0,
  A_41 : 19372.0/6561.0, A_42 : -25360.0/2187.0, A_43 : 64448.0/6561.0, A_44 : -212.0/729.0,
  A_51 :  9017.0/3168.0, A_52 :   -355.0/33.0,   A_53 : 46732.0/5247.0, A_54 :   49.0/176.0, A_55 : -5103.0/18656.0,
  
  
  // B5 are the 5th order coefficients
  B5_1 : 35.0/384.0,  
  B5_2 : 0.0, 
  B5_3 : 500.0/1113.0, 
  B5_4 : 125.0/192.0, 
  B5_5 : -2187.0/6784.0, 
  B5_6 : 11.0/84.0,
  
  D_1 : -12715105075.0/11282082432.0,  
  D_2 : 0.0, 
  D_3 : 87487479700.0/32700410799.0, 
  D_4 : -10690763975.0/1880347072.0,
  D_5 : 701980252875.0/199316789632.0, 
  D_6 : -1453857185.0/822651844.0, 
  D_7 : 69997945.0/29380423.0,
  
  // Error coefficients
  E_1 : 71.0/57600.0, 
  E_2 : 0.0, 
  E_3 : -71.0/16695.0, 
  E_4 : 71.0/1920.0, 
  E_5 : -17253.0/339200.0, 
  E_6 : 22.0/525.0, 
  E_7 : -1.0/40.0
};

/**
 * Constructor for DoPri5
 * @returns DoPri5
 */
EJSS_ODE_SOLVERS.doPri5 = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTimeAdaptive(5); // 5 is the method's order 
  var mRate2, mRate3, mRate4, mRate5, mRate6, mCoeffs;
  var superAllocateOtherArrays = self.allocateOtherArrays;

  self.getNumberOfEvaluations = function() { return 6; }
  
  self.allocateOtherArrays = function() {
    superAllocateOtherArrays();
    var dim = self.getDimension();
    mRate2 = new Array(dim);
    mRate3 = new Array(dim);
    mRate4 = new Array(dim);
    mRate5 = new Array(dim);
    mRate6 = new Array(dim);
    mCoeffs = new Array(5);
    for (var i=0; i<5; i++) mCoeffs[i] = new Array(dim);
  };

  self.computeIntermediateStep = function(step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    
	var CTS = EJSS_ODE_SOLVERS.DoPri5;
    var i;

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    ode.getRate(state, mRate2);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i] + CTS.A_22*mRate2[i]);
    ode.getRate(state, mRate3);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_32*mRate2[i]+CTS.A_33*mRate3[i]);
    ode.getRate(state, mRate4);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_42*mRate2[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    ode.getRate(state, mRate5);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_52*mRate2[i]+CTS.A_53*mRate3[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    ode.getRate(state, mRate6);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B5_1*initialRate[i]+CTS.B5_2*mRate2[i]+CTS.B5_3*mRate3[i]+CTS.B5_4*mRate4[i]+CTS.B5_5*mRate5[i]+CTS.B5_6*mRate6[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];
  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    var CTS = EJSS_ODE_SOLVERS.DoPri5;
    var CODE = EJSS_ODE_SOLVERS.DISCONTINUITY_CODE;
    var i;
	    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate2);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i]+CTS.A_22*mRate2[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate3);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_32*mRate2[i]+CTS.A_33*mRate3[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate4);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_42*mRate2[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate5);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_52*mRate2[i]+CTS.A_53*mRate3[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate6);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B5_1*initialRate[i]+CTS.B5_2*mRate2[i]+CTS.B5_3*mRate3[i]+CTS.B5_4*mRate4[i]+CTS.B5_5*mRate5[i]+CTS.B5_6*mRate6[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    // The final rate has already been computed (it is needed to estimate the error) 
    // calculation of interpolation coefficients 
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    
	var CTS = EJSS_ODE_SOLVERS.DoPri5;
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    
    var dt = finalState[timeIndex]-initialState[timeIndex];
    for (var i=0; i<dimension; i++) {
      var initStateI = initialState[i]; // for efficiency
      var initRateI  = initialRate[i];
      var finalRateI = finalRate[i];
      mCoeffs[0][i] = initStateI; 
      var coeff1i = finalState[i] - initStateI;
      var coeff2i = dt*initRateI - coeff1i;
      mCoeffs[3][i] = coeff1i - dt*finalRateI - coeff2i;
      mCoeffs[4][i] = dt*(CTS.D_1*initRateI+CTS.D_2*mRate2[i]+CTS.D_3*mRate3[i]+CTS.D_4*mRate4[i]+CTS.D_5*mRate5[i]+CTS.D_6*mRate6[i]+CTS.D_7*finalRateI);
      mCoeffs[1][i] = coeff1i;
      mCoeffs[2][i] = coeff2i;
    }
    return EJSS_ODE_INTERPOLATION.dopri5IntervalData(initialState, finalState, mCoeffs);
  }

  self.computeApproximation = function(step) {
	var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    var absTol = self.getAbsTol();
    var relTol = self.getRelTol();

    var CTS = EJSS_ODE_SOLVERS.DoPri5;
    var dimension = self.getDimension();

    ode.getRate(finalState, finalRate);
    var error = 0;
    for (var i=0; i<dimension; i++) {
      var sk = absTol[i] + relTol[i] * Math.max(Math.abs(finalState[i]), Math.abs(initialState[i]));
      var errorI = (CTS.E_1*initialRate[i]+CTS.E_2*mRate2[i]+CTS.E_3*mRate3[i]+CTS.E_4*mRate4[i]+CTS.E_5*mRate5[i]+CTS.E_6*mRate6[i]+CTS.E_7*finalRate[i])/sk;
      error += errorI*errorI;
    }
    return Math.sqrt(error/dimension);
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}
