/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.CashKarp45 = {
  A_11 :   1.0/5.0,
  A_21 :   3.0/40.0,     A_22 :  9.0/40.0,
  A_31 :   3.0/10.0,     A_32 : -9.0/10.0,   A_33 :  6.0/5.0,
  A_41 : -11.0/54.0,     A_42 :  5.0/2.0,    A_43 : -70.0/27.0,    A_44 : 35.0/27.0,
  A_51 : 1631.0/55296.0, A_52 : 175.0/512.0, A_53 : 575.0/13824.0, A_54 : 44275.0/110592.0, A_55 : 253.0/4096.0,
  // B4 are the 4th order coefficients
  B4_1 : 2825./27648., B4_2 : 0., B4_3 : 18575./48384., B4_4 : 13525./55296., B4_5 : 277./14336., B4_6 : 1./4.,
  // B5 are the 5th order coefficients
  B5_1 : 37./378.,     B5_2 : 0., B5_3 : 250./621.,     B5_4 : 125./594.,     B5_5 : 0.,          B5_6 : 512./1771. 
  //  E_1 = 277./64512.,   E_2 = 0.,  E_3 = -6925./370944., E_4 = 6925./202752.,  E_5 = 277./14336.,  E_6 = -277./7084.

};

/**
 * Constructor for CashKarp45
 * @returns CashKarp45
 */
EJSS_ODE_SOLVERS.cashKarp45 = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTimeAdaptive(5); // 5 is the method's order 
  var mRate2, mRate3, mRate4, mRate5, mRate6, mOrder4;
  var superAllocateOtherArrays = self.allocateOtherArrays;

  self.getNumberOfEvaluations = function() { return 6; }
  
  self.allocateOtherArrays = function() {
    superAllocateOtherArrays();
    mRate2 = new Array(self.getDimension());
    mRate3 = new Array(self.getDimension());
    mRate4 = new Array(self.getDimension());
    mRate5 = new Array(self.getDimension());
    mRate6 = new Array(self.getDimension());
    mOrder4 = new Array(self.getDimension());
  };

  self.computeIntermediateStep = function(step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    
	var CTS = EJSS_ODE_SOLVERS.CashKarp45;
    var i;

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    ode.getRate(state, mRate2);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i] + CTS.A_22*mRate2[i]);
    ode.getRate(state, mRate3);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_32*mRate2[i]+CTS.A_33*mRate3[i]);
    ode.getRate(state, mRate4);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_42*mRate2[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    ode.getRate(state, mRate5);
    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_52*mRate2[i]+CTS.A_53*mRate3[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    ode.getRate(state, mRate6);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B5_1*initialRate[i]+CTS.B5_2*mRate2[i]+CTS.B5_3*mRate3[i]+CTS.B5_4*mRate4[i]+CTS.B5_5*mRate5[i]+CTS.B5_6*mRate6[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    var CTS = EJSS_ODE_SOLVERS.CashKarp45;
    var CODE = EJSS_ODE_SOLVERS.DISCONTINUITY_CODE;
    var i;
	    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*CTS.A_11*initialRate[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate2);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_21*initialRate[i]+CTS.A_22*mRate2[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate3);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_31*initialRate[i]+CTS.A_32*mRate2[i]+CTS.A_33*mRate3[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate4);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_41*initialRate[i]+CTS.A_42*mRate2[i]+CTS.A_43*mRate3[i]+CTS.A_44*mRate4[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate5);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*(CTS.A_51*initialRate[i]+CTS.A_52*mRate2[i]+CTS.A_53*mRate3[i]+CTS.A_54*mRate4[i]+CTS.A_55*mRate5[i]);
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate6);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B5_1*initialRate[i]+CTS.B5_2*mRate2[i]+CTS.B5_3*mRate3[i]+CTS.B5_4*mRate4[i]+CTS.B5_5*mRate5[i]+CTS.B5_6*mRate6[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    ode.getRate(finalState, finalRate);
    return EJSS_ODE_INTERPOLATION.bootstrap2IntervalData(initialState, initialRate,finalState, finalRate, ode);

  }

  self.computeApproximation = function(step) {
    var timeIndex = self.getTimeIndex();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var CTS = EJSS_ODE_SOLVERS.CashKarp45;

    for (var i=0; i<timeIndex; i++) mOrder4[i] = initialState[i] + step*(CTS.B4_1*initialRate[i]+CTS.B4_2*mRate2[i]+CTS.B4_3*mRate3[i]+CTS.B4_4*mRate4[i]+CTS.B4_5*mRate5[i]+CTS.B4_6*mRate6[i]);
    mOrder4[timeIndex] = self.getInitialTime()  + step*initialRate[timeIndex];
    return self.computeError(mOrder4);
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}
