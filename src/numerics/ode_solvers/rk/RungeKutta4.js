/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.RungeKutta4 = {

};

/**
 * Constructor for RungeKutta4
 * @returns RungeKutta4
 */
EJSS_ODE_SOLVERS.rungeKutta4 = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTime(); // reference returned 
  var mRate2, mRate3, mRate4;

  self.getNumberOfEvaluations = function() { return 4; }

  self.allocateOtherArrays = function() {
    mRate2 = new Array(self.getDimension());
    mRate3 = new Array(self.getDimension());
    mRate4 = new Array(self.getDimension());
  };

  self.computeIntermediateStep = function(step, state) {
    var halfStep = step/2;
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    
    var i;
	  
    for (i=0; i<dimension; i++) state[i] = initialState[i] + halfStep*initialRate[i];
    ode.getRate(state, mRate2);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + halfStep*mRate2[i];
    ode.getRate(state, mRate3);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + step*mRate3[i];
    ode.getRate(state, mRate4);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(initialRate[i] + 2*mRate2[i] + 2*mRate3[i] + mRate4[i])/6.0;
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];
  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
    var halfStep = step/2;
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;

    var CODE = EJSS_ODE_SOLVERS.DISCONTINUITY_CODE;
    var i;
    
	    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + halfStep*initialRate[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
	ode.getRate(state, mRate2);

    for (i=0; i<dimension; i++) state[i] = initialState[i] + halfStep*mRate2[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
	ode.getRate(state, mRate3);

	for (i=0; i<dimension; i++) state[i] = initialState[i] + step*mRate3[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // admissible. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
	ode.getRate(state, mRate4);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(initialRate[i] + 2*mRate2[i] + 2*mRate3[i] + mRate4[i])/6.0;
	state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    ode.getRate(finalState, finalRate);
    return EJSS_ODE_INTERPOLATION.bootstrapIntervalData(initialState, initialRate, finalState, finalRate, ode);
  }


  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}