/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.BogackiShampine23 = {
  B3_1 : 2.0/9.0,  B3_2 : 1.0/3.0, B3_3 : 4.0/9.0,
  B2_1 : 7.0/24.0, B2_2 : 1.0/4.0, B2_3 : 1.0/3.0, B2_4 : 1.0/8.0
};

/**
 * Constructor for BogackiShampine23
 * @returns BogackiShampine23
 */
EJSS_ODE_SOLVERS.bogackiShampine23 = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTimeAdaptive(3); // 3 is the method's order 
  var mRate2, mRate3, mOrder2;
  var superAllocateOtherArrays = self.allocateOtherArrays;

  self.getNumberOfEvaluations = function() { return 3; }
  
  self.allocateOtherArrays = function() {
    superAllocateOtherArrays();
    mRate2 = new Array(self.getDimension());
    mRate3 = new Array(self.getDimension());
    mOrder2 = new Array(self.getDimension());
  };

  self.computeIntermediateStep = function(step, state) {
    var halfStep = step/2;
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    
	var CTS = EJSS_ODE_SOLVERS.BogackiShampine23;
    var i;

    for (i=0; i<dimension; i++) state[i] = initialState[i] + halfStep*initialRate[i];
    ode.getRate(state, mRate2);
    
    var threeQuarterStep = 0.75*step;
    for (i=0; i<dimension; i++) state[i] = initialState[i] + threeQuarterStep*mRate2[i]; // 3/4
    ode.getRate(state, mRate3);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B3_1*initialRate[i]+CTS.B3_2*mRate2[i]+CTS.B3_3*mRate3[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
    var halfStep = step/2;
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var dimension = self.getDimension();
    var timeIndex = dimension-1;
    var CTS = EJSS_ODE_SOLVERS.BogackiShampine23;
    var CODE = EJSS_ODE_SOLVERS.DISCONTINUITY_CODE;
    var i;
	    
    for (i=0; i<dimension; i++) state[i] = initialState[i] + halfStep*initialRate[i];
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate2);

    var threeQuarterStep = 0.75*step;
    for (i=0; i<dimension; i++) state[i] = initialState[i] + threeQuarterStep*mRate2[i]; // 3/4
    switch (eventSolver.checkDiscontinuity(state, false)) {
      case CODE.DISCONTINUITY_PRODUCED_ERROR  : return CODE.DISCONTINUITY_PRODUCED_ERROR; 
      case CODE.DISCONTINUITY_JUST_PASSED     : return CODE.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED;   
      case CODE.DISCONTINUITY_ALONG_STEP      : return CODE.DISCONTINUITY_ALONG_STEP;
      case CODE.DISCONTINUITY_EXACTLY_ON_STEP : break; // doubtful. But check for the real point 
      case CODE.NO_DISCONTINUITY_ALONG_STEP   : break; // go ahead 
    }
    ode.getRate(state, mRate3);

    for (i=0; i<timeIndex; i++) state[i] = initialState[i] + step*(CTS.B3_1*initialRate[i]+CTS.B3_2*mRate2[i]+CTS.B3_3*mRate3[i]);
    state[timeIndex] = self.getInitialTime() + step*initialRate[timeIndex];

    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    // The final rate has already been computed (it was needed to estimate the error) 
    //ode.getRate(finalState, finalRate);
    return EJSS_ODE_INTERPOLATION.hermiteIntervalData(initialState, initialRate, finalState, finalRate);

  }

  self.computeApproximation = function(step) {
    var ode = self.getODE();
    var timeIndex = self.getTimeIndex();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();
    ode.getRate(finalState, finalRate);
    var CTS = EJSS_ODE_SOLVERS.BogackiShampine23;

    for (var i=0; i<timeIndex; i++) mOrder2[i] = initialState[i] + step*(CTS.B2_1*initialRate[i]+CTS.B2_2*mRate2[i]+CTS.B2_3*mRate3[i]+CTS.B2_4*finalRate[i]);
    mOrder2[timeIndex] = self.getInitialTime()  + step*initialRate[timeIndex];
    return self.computeError(mOrder2);
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}
