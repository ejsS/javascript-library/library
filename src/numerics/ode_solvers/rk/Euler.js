/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.Euler = {

};

/**
 * Constructor for Euler
 * @returns Euler
 */
EJSS_ODE_SOLVERS.euler = function() {
  var self = EJSS_ODE_SOLVERS.solverEngineDiscreteTime(); // reference returned 

  self.getNumberOfEvaluations = function() { return 1; }

  self.computeIntermediateStep = function(step, state) {
    var dimension = self.getDimension();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    for (var i=0; i<dimension; i++) state[i] = initialState[i] + step*initialRate[i];    
  }

  self.computeCarefulIntermediateStep = function(eventSolver, step, state) {
	self.computeIntermediateStep(step,state);
    return eventSolver.checkDiscontinuity(state, true);
  }

  self.computeFinalRateAndCreateIntervalData = function() {
    var ode = self.getODE();
    var initialState = self.getInitialState();
    var initialRate  = self.getInitialRate();
    var finalState = self.getFinalState();
    var finalRate  = self.getFinalRate();

    ode.getRate(finalState, finalRate);
    return EJSS_ODE_INTERPOLATION.eulerIntervalData(initialState, initialRate,finalState[self.getTimeIndex()]);
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}