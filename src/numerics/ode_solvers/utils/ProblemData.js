/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * A class that handles the information of a StateEvent for given states
 * @author Francisco Esquembre
 * @version Jan 2014
 */
EJSS_ODE_SOLVERS.ProblemData = {
    POSITIVE       : 2,
    SMALL_POSITIVE : 1,
    ZERO           : 0,
    SMALL_NEGATIVE : -1,
    NEGATIVE       : -2
};

/**
 * Constructor for EventData
 * @returns EventData
 */
EJSS_ODE_SOLVERS.eventData = function(mSolver, mEvent, mState) {
  var self = {};							// reference returned 
  var mEventType = mEvent.getTypeOfEvent(); // The type of event (STATE_EVENT, POSITIVE_EVENT, or CROSSING_EVENT)
  var mTimeIndex = mState.length-1;
  
  var mPositiveFlag; // Whether the state has ever been positive (since last reset)
  var mNegativeFlag; // Whether the state has ever been negative (since last reset)
  var mCurrentPosition; // In which side of the [-Tol,+Tol] is h at the current time (POSITIVE, SMALL_POSITIVE, etc.)
  var mHBefore;  // The value of h at the current time
  var mHAfter;   // The value of h at the end of a period
  var mH;        // The value of time at the test point
  var mTime;     // the next time at which the event took place
  var mMaxAdvance; // The maximum the solver must advance after this event happens

  self.getEvent = function() { return mEvent; }
  self.getEventType = function() { return mEventType; }

  self.setH = function(h) { mH = h; }
  self.getH = function() { return mH; }

  self.getTime = function() { return mTime; }
  self.setTime = function(time) { mTime = time; }
    
  self.hasPositiveFlag = function() { return mPositiveFlag; }

  self.hasNegativeFlag = function() { return mNegativeFlag; }

  self.getHBefore = function() { return mHBefore; }

  self.setHAfter = function(h) { mHAfter = h; }
  self.getHAfter = function() { return mHAfter; }
  
  self.getCurrentPosition = function() { return mCurrentPosition; }

  self.getMaxAdvance = function() { return mMaxAdvance; }
  self.setMaxAdvance = function(advance) { mMaxAdvance = advance; }
    
  self.action = function() { return mEvent.action(); }
    
  self.getProblem = function() { return mEvent; }
    
  self.reset = function (state) {
//      System.out.println ("Resetting event at "+state[timeIndex] + " current h = "+hBefore);
    mPositiveFlag = false;
    mNegativeFlag = false;
    var h = mEvent.evaluate(state);
    self.findPosition (state[mTimeIndex],h);
    // At initialization times, a small posi(nega)tive is actually a posi(nega)tive
    if (mSolver.getCurrentEventData()!=self) { // unless this event just happened
      if (mEventType==EJSS_ODE_SOLVERS.EVENT_TYPE.CROSSING_EVENT) {
        if (h>0) mPositiveFlag = true;
        else if (h<0) mNegativeFlag = true;
      }
      else if (mEventType==EJSS_ODE_SOLVERS.EVENT_TYPE.POSITIVE_EVENT) {
        if (h>0) mPositiveFlag = true;
      }
//        System.out.println ("Positive flag = "+positiveFlag);
//        System.out.println ("Negative flag = "+negativeFlag);
    }
//      System.err.println ("Reiniting event at "+state[timeIndex] + "new h = "+generalEvent.evaluate(state));
  }

  self.findPosition = function (time, hValue) {
    mHBefore = hValue;
    if      (mHBefore>= mEvent.getTolerance()) { mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.POSITIVE; mPositiveFlag = true; }
    else if (mHBefore>0)  mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.SMALL_POSITIVE; 
    else if (mHBefore==0) mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.ZERO;
    else if (mHBefore>-mEvent.getTolerance()) mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.SMALL_NEGATIVE;
    else { mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.NEGATIVE; mNegativeFlag = true; }
    if (mEventType==EJSS_ODE_SOLVERS.EVENT_TYPE.STATE_EVENT && mCurrentPosition==EJSS_ODE_SOLVERS.ProblemData.NEGATIVE) {
      var msg = "The state event " + mEvent+" is in an illegal state: "+mHBefore+ " at "+time;
      if (mSolver.getLastEventData()==null) msg +="\nThere was no previous event"; 
      else msg +="\nLast previous event was "+mSolver.getLastEventData().getProblem()+", which took place at "+mSolver.getLastEventDataTime();
      mSolver.error(EJSS_ODE_SOLVERS.ERROR.ILLEGAL_EVENT_STATE, msg);
    }
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.reset(mState); 
  return self;
}

/**
 * Constructor for DiscontinuityData
 * @returns DiscontinuityData
 */
EJSS_ODE_SOLVERS.discontinuityData = function(mSolver, mDiscontinuity, mState) {
  var self = {};							// reference returned 

  var mTimeIndex = mState.length-1;
  
  var mPositiveFlag; // Whether the state has ever been positive (since last reset)
  var mNegativeFlag; // Whether the state has ever been negative (since last reset)
  var mCurrentPosition; // In which side of the [-Tol,+Tol] is h at the current time (POSITIVE, SMALL_POSITIVE, etc.)
  var mHBefore;  // The value of h at the current time
  var mH;        // The value of time at the test point
  var mTime;     // the next time at which the event took place

  self.getDiscontinuity = function() { return mDiscontinuity; }

  self.setH = function(h) { mH = h; }
  self.getH = function() { return mH; }

  self.setTime = function(time) { mTime = time; }
  self.getTime = function() { return mTime; }

  self.hasPositiveFlag = function() { return mPositiveFlag; }

  self.hasNegativeFlag = function() { return mNegativeFlag; }

  self.getHBefore = function() { return mHBefore; }

  self.setHAfter = function(h) { mHAfter = h; }
  self.getHAfter = function() { return mHAfter; }
  
  self.getCurrentPosition = function() { return mCurrentPosition; }

  self.getMaxAdvance = function() { return Number.NaN; }
    
  self.action = function() { return mDiscontinuity.action(); }
    
  self.getProblem = function() { return mDiscontinuity; }
    
  self.reset = function (state) {
    mPositiveFlag = false;
    mNegativeFlag = false;
    var h = mDiscontinuity.evaluate(state);
    self.findPosition (state[mTimeIndex],h);
    // At initialization times, a small posi(nega)tive is actually a posi(nega)tive
    if (mSolver.getCurrentEventData()!=self) { // unless this event just happened
      if (h>0) mPositiveFlag = true;
      else if (h<0) mNegativeFlag = true;
//        System.out.println ("Positive flag = "+positiveFlag);
//        System.out.println ("Negative flag = "+negativeFlag);
    }
//      System.err.println ("Reiniting event at "+state[timeIndex] + "new h = "+generalEvent.evaluate(state));
  }

  self.findPosition = function (time, hValue) {
    mHBefore = hValue;
    if      (mHBefore>= mDiscontinuity.getTolerance()) { mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.POSITIVE; mPositiveFlag = true; }
    else if (mHBefore>0)  mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.SMALL_POSITIVE; 
    else if (mHBefore==0) mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.ZERO;
    else if (mHBefore>-mDiscontinuity.getTolerance()) mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.SMALL_NEGATIVE;
    else { mCurrentPosition = EJSS_ODE_SOLVERS.ProblemData.NEGATIVE; mNegativeFlag = true; }
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.reset(mState); 
  return self;
}
