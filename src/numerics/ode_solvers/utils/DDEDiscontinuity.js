/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * A Discontinuity that handles the discontinuities of a DDE
 * @author Francisco Esquembre
 * @version Jan 2014
 */
EJSS_ODE_SOLVERS.DDEDiscontinuity = {
};

/**
 * Constructor for DDEDiscontinuity
 * @returns DDEDiscontinuity
 */
EJSS_ODE_SOLVERS.ddeDiscontinuity = function(mSolver, mDDE) {
  var self = {};							// reference returned 

  var mDiscontinuities = [];
  var mDiIndex = [];
  var mRateForCorrections = [];

  self.initialize = function(state) {
    if (mSolver.getRunsForwards()) mDiscontinuities.push(Number.NEGATIVE_INFINITY);
    else mDiscontinuities.push(Number.POSITIVE_INFINITY);
    var initDisc = mDDE.getInitialConditionDiscontinuities();
    if (initDisc!=null) {
      var length = initDisc.length;
      for (var i=0; i<length; i++) mDiscontinuities.push(initDisc[i]);
    }
    mDiscontinuities.push(state[state.length-1]);
    var delays = mDDE.getDelays(state);
    mDiIndex = new Array(delays.length);
    self.reset(state);
    mRateForCorrections = new Array(state.length);
  }
    
  self.reset = function(state) {
    var length = mDiIndex.length
    for (var i=0; i<length; i++) mDiIndex[i] = 0;
    update(state);
  }
    
  function update(state) {
    var i;
    var delays = mDDE.getDelays(state);
    var n = delays.length;
    var discCount = mDiscontinuities.length;
    var timeIndex = state.length-1;
    // Now find the mDi
    if (mSolver.getRunsForwards()) {
      for (i=0; i<n; i++) {
        var limit = state[timeIndex]-delays[i]+self.getTolerance(); // add the tolerance so that to skip round-off errors
        for (var index=mDiIndex[i]; index<discCount; index++) {
          var disc = mDiscontinuities[index];
//          System.err.print("Disc["+index+"] = "+disc+" > limit "+limit+ "... ");
          if (disc>limit) {
//            System.err.println("YES");
            mDiIndex[i] = index;
            break;
          }
//          System.err.println("NO");
        }
      }
    }
    else {
      for (i=0; i<n; i++) {
        var limit = state[timeIndex]-delays[i]-self.getTolerance(); // add the tolerance so that to skip round-off errors
        for (var index=mDiIndex[i]; index<discCount; index++) {
          var disc = mDiscontinuities[index];
          if (disc<limit) {
            mDiIndex[i] = index;
            break;
          }
        }
      }
    }
//      System.err.println("Discont. are : ");
//      for (int i=0; i<discCount; i++) System.err.println("disc["+i+"] = "+mDiscontinuities.get(i));
//      System.err.println("di's are : ");
//      for (int i=0; i<n; i++) System.err.println("mdi["+i+"] = "+mDiIndex[i]);
//      System.err.println("\n");
  }
    
    // implementation of Discontinuity
    
  self.evaluate = function(state) {
    var i;
    var t = state[state.length-1];
    var delays = mDDE.getDelays(state);
    var n = delays.length;
    if (mSolver.getRunsForwards()) {
      var min = Number.POSITIVE_INFINITY;
      for (i=0; i<n; i++) {
        var di = mDiscontinuities[mDiIndex[i]];
//        System.err.println("Discont = "+di);
//        System.err.println("delay = "+delays[i]);
//        System.err.println("time = "+t);
//        System.err.println("min = "+(delays[i]+di-t));
        min = Math.min(min, delays[i]+di-t);
      }
//      System.err.println("Time = "+state[1]+ " Value returned = "+min);
      return min;
    }
    else {
      var max = Number.NEGATIVE_INFINITY;
      for (i=0; i<n; i++) {
        var di = mDiscontinuities[mDiIndex[i]];
        max = Math.max(max, delays[i]+di-t);
      }
      return max;
    }
  }
    
  /**
   * Advances time (and state) past the delay
   * @param state
   * @return
   */
  function correctTime(state) {
    var i;
    var dimension = state.length;
    var timeIndex = dimension-1;
    var step = self.getTolerance()/20;
    var maxIterations = mSolver.getDDEIterations();
    var history = mSolver.getStateHistory();
    var counter=0;
    if (mSolver.getRunsForwards()) {
      while ((++counter)<maxIterations) {
        mDDE.getRate(state, mRateForCorrections); // advance using Euler
        history.addIntervalData(EJSS_ODE_INTERPOLATION.eulerIntervalData(state,mRateForCorrections,state[timeIndex]+step));
        for (i=0; i<dimension; i++) state[i] += step*mRateForCorrections[i];
//        state[timeIndex] += step;
        var h = self.evaluate(state);
        if (h<0) {
//    	  System.out.println("Counter -------------- "+counter+ "/"+mDDEdiscontinuityMaxIterations);
      	  return state[timeIndex];
        }
      }
    }
    else {
      while ((++counter)<maxIterations) {
        mDDE.getRate(state, mRateForCorrections);
         history.addIntervalData(EJSS_ODE_INTERPOLATION.eulerIntervalData(state,mRateForCorrections,state[timeIndex]-step));
         for (i=0; i<dimension; i++) state[i] -= step*mRateForCorrections[i];
//        state[timeIndex] -= step;
        var h = evaluate(state);
        if (h>0) return state[timeIndex];
      }
    }
    return Number.NaN;
  }
    
  self.action = function() {
    var state = mDDE.getState();
//    System.err.println("Found delay discontinuity at "+state[1]+ " --------------------------------------");
//    System.err.println ("Correct state should be "+correctTime(state));
    var time = correctTime(state); // Automatically find the moment where it crosses the discontinuity
//    System.out.println("Corrected time ="+time);
//    interpolatorSolver.interpolate(time, state);
//    state[timeIndex] = time;
    mDiscontinuities.push(time);
    var eventData = mSolver.findFirstEvent(state,time,state); // Find a possible new event because of this time increase!
    if (eventData!=null) {
      mCurrentEventData = eventData;   
      // No need to interpolate because the state was changed in the call to findFirstEvent
//      if (mUseBestInterpolation) mSolverEngine.bestInterpolate(problem.getTime(), state); // So that we reinitialize with the best possible approximation 
//      else System.arraycopy(mTest_ode_state, 0, state, 0, mDimension); // -- unnecessary -- interpolatorSolver.interpolate(eventData.time, state);
      eventData.action();
    }
    
    // update(state); will be done by the reinitialize() method of the solver
    return true; // implies reset
  }
    
  self.getTolerance = function() { return mSolver.getDDETolerance(); }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
 
   return self;
}