/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 * @author Francisco Esquembre
 * @version 1.0 Jan 2014
 */
EJSS_ODE_SOLVERS.SolverEngineDiscreteTime = {

};

/**
 * Constructor for SolverEngineDiscreteTime
 * @returns An abstract SolverEngineDiscreteTime
 */
EJSS_ODE_SOLVERS.solverEngineDiscreteTime = function() {
  var self = {};							// reference returned 

  // ODE variables
  var mErrorCode=EJSS_ODE_SOLVERS.ERROR.NO_ERROR; //InterpolatorEventSolver.NO_ERROR;
  var mDimension; // The length of the state array
  var mTimeIndex; // The index of the independent variable (usually the time)
  var mAccumulatedEvaluations = 0; // Number of evaluations so far
  var mStepSize = 0.1; // the preferred step size
  var mMaximumStepSize = Number.POSITIVE_INFINITY;
  var mInitialTime=0; // The time before the step is taken
  var mFinalTime=0;   // The time after the step is taken
  var mInitialState;  // The state array before the step is taken
  var mInitialRate;   // The rate array before the step is taken
  var mFinalState;    // The state array after the step is taken
  var mFinalRate;     // The rate array after the step is taken
  var mODE;
  var mEventSolver;

  // Memory
  var mStateHistory;
    
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  /**
   * The number of function evaluations per step
   * @return int
   */
  self.getNumberOfEvaluations = function() { return 0; };

  /**
   * Allocates other arrays needed for the method
   */
  self.allocateOtherArrays = function() {}; // do nothing

  /**
   * Computes one intermediate step not caring about precision
   * @param step double the length of the step
   * @param state double[] the target array
   * @return same as state
   */
  self.computeIntermediateStep = function(step, state) { return null; }

  /**
   * Computes one intermediate step not caring about precision but taking care of possible discontinuities
   * @param _step the length of the step
   * @param _state the target array
   * @return one of the flags in the checkDiscontinuity(double[]) method of ODEInterpolatorEventSolver
   * @see ODEInterpolatorEventSolver#checkDiscontinuity(double[])
   */
  self.computeCarefulIntermediateStep = function(solver, step, state) { return EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.NO_DISCONTINUITY_ALONG_STEP; }

  /**
   * Computes the final rate and creates a new IntervalData for interpolation
   */
  self.computeFinalRateAndCreateIntervalData = function() { return null; };
  
  // --------------------------------------------
  // Protected setters and getters
  // --------------------------------------------
  
    self.getODE = function() { return mODE; }

    self.getEventSolver = function() { return mEventSolver; }

    self.getDimension = function() { return mDimension; }

    self.getTimeIndex = function() { return mTimeIndex; }
     
    self.getInitialTime = function() { return mInitialTime; }

    self.getInitialState = function() { return mInitialState; }
  
    self.getInitialRate = function() { return mInitialRate; }

    self.getFinalState = function() { return mFinalState; }

    self.getFinalRate = function() { return mFinalRate; }
  
    self.setFinalTime = function(time) { mFinalTime = time; }

    self.getFinalTime = function() { return mFinalTime; }
    
    self.setErrorCode = function(code) { mErrorCode = code; }
    
    self.addToEvaluations = function(evals) { mAccumulatedEvaluations += evals; }


  // --------------------------------------------
  // Initialization
  // --------------------------------------------

  self.setODE = function(eventSolver, ode) { 
    mEventSolver = eventSolver;
    mODE = ode;
    var state = mODE.getState();
    mDimension = state.length;
    mTimeIndex = mDimension-1;
    mStateHistory = EJSS_ODE_INTERPOLATION.stateHistory(ode);
    if (mODE.getMaximumDelay) {
      mStateHistory.setMinimumLength(mODE.getMaximumDelay());
    }
  } 
  
  // ------------------------------------------------
  // Implementation of SolverEngine
  //------------------------------------------------
  
  self.initialize = function(stepSize) {
    mStepSize = stepSize;
    var state = mODE.getState();
    if (mInitialState==null || mInitialState.length!=state.length) {
      mDimension = state.length;
      mTimeIndex = mDimension-1;
      mInitialState = new Array(mDimension);
      mInitialRate  = new Array(mDimension);
      mFinalState   = new Array(mDimension);
      mFinalRate    = new Array(mDimension);
      self.allocateOtherArrays();
    }
    mAccumulatedEvaluations = 0;
    mStateHistory.clearAll();
    if (mODE.getMaximumDelay) {
      mStateHistory.setMinimumLength(Math.max(Math.abs(mODE.getMaximumDelay()),Math.abs(stepSize))); // Make sure we have enough memory for delays and events
    }
    else mStateHistory.setMinimumLength(stepSize); // Make sure we have enough memory for events
    self.reinitialize(state);
  }

  self.reinitialize = function(state) {
    mInitialTime = state[mTimeIndex];
    for (var i=0; i<mDimension; i++) mInitialState[i] = state[i];
    mODE.getRate(mInitialState, mInitialRate);
    mFinalTime = Number.NaN;
    mErrorCode = EJSS_ODE_SOLVERS.ERROR.NO_ERROR;
  }

  self.getCurrentRate = function() { return mInitialRate; }

  self.setStepSize = function(stepSize) { 
    mStepSize = stepSize; 
    if (mODE.getMaximumDelay) {
      mStateHistory.setMinimumLength(Math.max(Math.abs(mODE.getMaximumDelay()),Math.abs(stepSize))); // Make sure we have enough memory for delays and events
    }
    else mStateHistory.setMinimumLength(stepSize); // Make sure we have enough memory for events
  }

  self.setMaximumStepSize = function(stepSize) { mMaximumStepSize = Math.abs(stepSize); }

  self.getMaximumStepSize = function() { return mMaximumStepSize; }

  self.getStepSize = function() { return mStepSize; }
  
  self.getInternalStepSize = function() { return mFinalTime-mInitialTime; }
  
  self.setEstimateFirstStep = function(estimate) {}
  
  self.setTolerances = function(absTol, relTol) {}

  self.getMaximumTime = function(withDiscontinuities) {
    if (mErrorCode!=EJSS_ODE_SOLVERS.ERROR.NO_ERROR) return Number.NaN;
    if (isNaN(mFinalTime)) {
      self.computeOneStep(withDiscontinuities);
    }
    return mFinalTime; 
  }

  self.internalStep = function(withDiscontinuities) {
    var i;
    mInitialTime = mFinalTime;
    mErrorCode = EJSS_ODE_SOLVERS.ERROR.NO_ERROR;
    for (i=0; i<mDimension; i++) mInitialState[i] = mFinalState[i];
    for (i=0; i<mDimension; i++) mInitialRate[i]  = mFinalRate[i];
    self.computeOneStep(withDiscontinuities);
    return mFinalTime;  // the final time that was computed
  }

  self.getCounter = function()  { return mAccumulatedEvaluations; }
  
  self.getStateHistory = function() { return mStateHistory; }

  self.interpolate = function(time, state) {
    return mStateHistory.interpolateState(time, state);
  }

  /*
   * Provides 'brute-force' interpolation by re-stepping from the initial time every time
   */
  self.bestInterpolate = function(time, state) {
    var i;
    if (isNaN(mFinalTime)) return null;
    if (time==mFinalTime) {
      for (i=0; i<mDimension; i++) state[i] = mFinalState[i];
      return state;
    }
    if (time==mInitialTime) {
      for (i=0; i<mDimension; i++) state[i] = mInitialState[i];
      return state;
    }
    self.computeIntermediateStep(time-mInitialTime, state);
    return state;
  }

  /**
   * Returns the actual step size to take. Adaptive solvers override this method
   * @return
   */
  self.getActualStepSize = function() { return mStepSize; }
    
  self.findTheDiscontinuity = function(step) {
    var counter = 0;
    var left = 0;
    var right = step;
    var lastValid = 0;
    var maxAttempts = mEventSolver.getDDEIterations();
//    System.err.println ("Finding the discontinuity in "+mInitialState[mTimeIndex]+", "+(mInitialState[mTimeIndex]+step)+" ------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    var EPSILON = mEventSolver.getEPSILON();
    while (counter<maxAttempts) {
      if (Math.abs(left-right)<EPSILON) { // 1.0e-15) { // Too close
//        System.err.println("Left and right are equal!: "+counter+" / "+maxAttempts);
        break;
      }
      var testPoint = (left+right)/2;
//      System.err.println ("\nTest point = "+(mInitialState[mTimeIndex]+testPoint));
      switch (self.computeCarefulIntermediateStep(mEventSolver, testPoint, mFinalState)) {
        default : 
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR : return Number.NaN; 
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.NO_DISCONTINUITY_ALONG_STEP  :  
          lastValid = mFinalState[mTimeIndex];
          left = testPoint; // and keep on searching
          break; 
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_EXACTLY_ON_STEP : 
//          System.err.println ("Found at point = "+mFinalState[mTimeIndex]+" ------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
          return mFinalState[mTimeIndex] ; // discontinuity just at the end of the step
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED :
          right = right - (right-left)/4; // and keep on searching but closer to the right end
          break;
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_ALONG_STEP : // Iterate until finding the precise point of the discontinuity
          right = testPoint; // and keep on searching
          break;
      }
      counter++; // try again
    }
//    System.err.println("Not found. taking last valid point = "+lastValid+" ------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    self.computeCarefulIntermediateStep(mEventSolver, lastValid, mFinalState);
    return lastValid; // not found, so take the last valid point
  }
  
  /**
   * Computes mFinalState[] and mFinalTime out of the current mInitialState[] and mInitialTime,
   * taking into account the mStepSize and the tolerance (which ever applies).
   * Not final because Adaptive solvers make it otherwise.
   */
  self.computeOneStep = function(hasDiscontinuities) {
    var step = mStepSize;
//    System.err.println("Trying to step to "+(mInitialState[mTimeIndex]+step));
    if (hasDiscontinuities) {
      switch (self.computeCarefulIntermediateStep(mEventSolver, step, mFinalState)) {
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_PRODUCED_ERROR : 
          mFinalTime = Number.NaN; 
          return;
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.NO_DISCONTINUITY_ALONG_STEP  :  // no discontinuity in this interval
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_EXACTLY_ON_STEP : // discontinuity just at the end of the step
          mFinalTime = mFinalState[mTimeIndex];
          break; 
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_JUST_PASSED :
        case EJSS_ODE_SOLVERS.DISCONTINUITY_CODE.DISCONTINUITY_ALONG_STEP : // Iterate until finding the precise point of the discontinuity
          mFinalTime = self.findTheDiscontinuity(step);
          break;
      }
      if (isNaN(mFinalTime)) {
        mErrorCode=EJSS_ODE_SOLVERS.ERROR.DISCONTINUITY_PRODUCED_ERROR;
        return;
      }
    }
    else {
      self.computeIntermediateStep(step,mFinalState);
      mFinalTime = mFinalState[mTimeIndex];
    }
//    System.err.println("Will really step to "+mFinalState[mTimeIndex]);
    // Accumulate the counter
    mAccumulatedEvaluations += self.getNumberOfEvaluations();
    // Clean memory, if required
    mStateHistory.clean(mInitialTime);
    // Add new interval data to memory
    mStateHistory.addIntervalData(self.computeFinalRateAndCreateIntervalData());
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
}