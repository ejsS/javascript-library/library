/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.ExtraStepsIntervalData = {

};

/**
 * Constructor for ExtraStepsIntervalData
 * @returns ExtraStepsIntervalData
 */
EJSS_ODE_INTERPOLATION.extraStepsIntervalData = function(aState, bState, coeffs) {
  var self = EJSS_ODE_INTERPOLATION.intervalData(aState[aState.length-1],bState[bState.length-1]); // reference returned
  var mTimeIndex;
  var mDeltaTime; 
  var mCoeffs;
  
  // --------------------------------------------
  // Getters
  // --------------------------------------------

  self.getTimeIndex = function() { return mTimeIndex; }

  self.getDeltaTime = function() { return mDeltaTime; }

  self.getCoeffs = function() { return mCoeffs; }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
    var dimension = aState.length;
    mTimeIndex = dimension-1;
    mDeltaTime = bState[mTimeIndex]-aState[mTimeIndex];
      
    var length = coeffs.length; 
    mCoeffs = new Array(length);
	for (var i=0; i<length; i++) {
	  mCoeffs[i] = new Array(dimension);
      for (var j=0; j<dimension; j++) {
	    mCoeffs[i][j] = coeffs[i][j];
	  }
	}

  return self;
}