/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * StateHistory is an object that stores past values of a vector valued real function (i.e. f: R --> Rn) and uses 
 * this information to provide interpolated values at different instants of time.
 * The current implementation uses a linked list of IntervalData, each of which is responsible for a half-open (on the right) interval [left,right).
 * For proper operation, the IntervalData must be ordered from old to new and the left side of each interval 
 * must match the right side of the previous interval. Intervals are closed on the left side, and open on the right: [a,b)
 * However, if a new interval is added that overlaps the last one, this newer interval takes over the responsibility for the intersection of both intervals.
 * Notice that right or left is not actually relevant. If any of the intervals happens to have the left ends greater than the right end, then
 * the collection is consider to run backwards, and everything is taken care of properly.  
 * Finally, there may be an optional 'last resource' IntervalData object that takes care of providing an interpolation for values not covered by the regular
 * intervals. This is useful,for instance, for the pre-initial conditions of DelayDifferentialEquations.
 *  
 * @author Francisco Esquembre
 * @version Jan 2014
 */
EJSS_ODE_INTERPOLATION.StateHistory = {

};

/**
 * Constructor for StateHistory
 * @returns StateHistory
 */
EJSS_ODE_INTERPOLATION.stateHistory = function(ode) {
  var self = {};							// reference returned 

  var mIntervalList = [];
  var mForwards = true;
  var mLastResourceInterval;
  
  var mMinimumLength=0;
  var mUserLength=0;
  var mActualLength=0;

  /**
   * Sets the length of the history requested to the solver.
   * The user will then be able to ask for values of the state as far as the current time minus this length.
   * stepSize is the default for plain ODE, getMaximumDelay() is the minimum used by DDEs.
   * Setting a value of Infinity makes the solver to keep the history for ever (i.e. as much as computer memory permits)
   * @param length
   */
  self.setLength = function(length) {
    mUserLength = Math.abs(length);
    mActualLength = Math.max(mMinimumLength, mUserLength);
  }
	
  /**
   * Sets the minimum length of the history requested to the solver.
   * Not to be used by users
   * @param length
   */
  self.setMinimumLength= function(length) {
    mMinimumLength = Math.abs(length);
    mActualLength = Math.max(mMinimumLength, mUserLength);
  }
  
  /**
   * Adds an IntervalData at the end of the memory
   * @param data
   */
  self.addIntervalData = function (data) {
    mForwards = (data.getLeft()<=data.getRight());
    var length = mIntervalList.length;
	if (mIntervalList.length>0) {
	  var lastIndex = mIntervalList.length-1;
      var lastInterval = mIntervalList[lastIndex]; 
      var toRemove = 0;
//      console.log("Adding interval ["+data.getLeft()+","+data.getRight()+")\n");
      if (mForwards) {
        while (lastInterval!=null && lastInterval.getLeft()>=data.getLeft()) {
          toRemove++;
          lastIndex--;
//          console.log("... will remove interval ["+lastInterval.getLeft()+","+lastInterval.getRight()+")\n");
          if (lastIndex<0) lastInterval = null; 
		  else lastInterval = mIntervalList[lastIndex];
		}
	    if (lastInterval!=null && data.getLeft()<lastInterval.getRight()) lastInterval.setRight(data.getLeft());
      }
	  else {
        while (lastInterval!=null && lastInterval.getLeft()<=data.getLeft()) {
          toRemove++;
          lastIndex--;
		  if (lastIndex<0) lastInterval = null; 
		  else lastInterval = mIntervalList[lastIndex];
		}
	    if (lastInterval!=null && data.getLeft()>lastInterval.getRight()) lastInterval.setRight(data.getLeft());
	  }
      if (toRemove>0) mIntervalList.splice(lastIndex,toRemove);
	}
	mIntervalList.push(data);
//	console.log(self.toString());
  }
	
  self.toString = function() {
	var txt = "History has now "+mIntervalList.length+" intervals: ";
	for (var i=0,n=mIntervalList.length; i<n; i++) {
	  var interval = mIntervalList[i];
	  if (i>0) txt += ", ";
	  txt += " ["+interval.getLeft()+","+ interval.getRight()+")";
	}
	return txt;
  }
	
  /**
   * Clears all the memory data
   */
  self.clearAll = function() { mIntervalList = []; }
	
  /**
   * Clears all intervals whose data is older than the given time. 
   * This method can be used to save memory when the data is not needed anymore.
   * The last resource interval is not cleared.
   * @param currentTime
   */
  self.clean = function(currentTime) {
  	if (!isFinite(mActualLength)) return;
    if (mActualLength==0) { // remember only the new interval
      clearAll();
      return;
    }
    var toBeRemoved = 0;
    var length = mIntervalList.length;
    if (mForwards) {
      currentTime -= mActualLength;
      for (var i=0; i<length; i++) {
	    var interval = mIntervalList[i];
	    if (interval.getRight()>currentTime) break; // This one cannot be removed
//        console.log("... will clean interval ["+interval.getLeft()+","+interval.getRight()+")\n");
	    toBeRemoved++;
      }
    }
    else {
      currentTime += mActualLength;
      for (var i=0; i<length; i++) {
	    var interval = mIntervalList[i];
	    if (interval.getRight()<currentTime) break; // This one cannot be removed
	    toBeRemoved++;
	  }
	}
	if (toBeRemoved>0) mIntervalList.splice(0,toBeRemoved);
  }

  /**
   * Finds the interval responsible for interpolating data at this time, including the last resource interval, if all other intervals fail
   * @param time double the given time for the state
   * @return IntervalData the data responsible for this instant of time
   */
  self.findInterval = function(time) {
    var length = mIntervalList.length;
    if (mForwards) {
      for (var i=length-1; i>=0; i--) {
	    var interval = mIntervalList[i];
	    if (interval.getLeft()<=time) return interval;
	  }
	}
	else { // backwards
      for (var i=length-1; i>=0; i--) {
	    var interval = mIntervalList[i];
	    if (interval.getLeft()>=time) return interval;
	  }
	}
    return mLastResourceInterval;
  }

  /**
   * Retrieve the state for the given time for one index
   * @param time double the given time for the state
   * @param index the index to interpolate 
   * @return double the interpolated value 
   */
  self.interpolate = function(time, index) {
    var interval = self.findInterval(time);
    return interval.interpolate(time,index);
  }

  /**
   * Retrieve the state for the given time for one index or a subset of indexes
   * @param time double the given time for the state
   * @param state double[] a place holder for the returned state
   * @param beginIndex the first index to interpolate. 0 is not specified
   * @param length If specified, only this range of indexes are interpolated, the whole state is interpolated if unspecified   
   * @return double[] the array with the data, same as passed state, 
   */
  self.interpolateState = function(time, state, beginIndex, length) {
//    console.log("Interpolate for t="+time);
//    console.log(self.toString());
    var interval = self.findInterval(time);
    if (!length) length = state.length;
    if (!beginIndex) beginIndex = 0;
    return interval.interpolateState(time,state,beginIndex,length);
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  if (ode.getInitialCondition) { // it is a DelayDifferentialEquation
    mLastResourceInterval = EJSS_ODE_INTERPOLATION.initialConditionData(ode);
    ode.setStateHistory(self);
  }
  else { // it is a regular ODE
    mLastResourceInterval = EJSS_ODE_INTERPOLATION.constantConditionData(ode.getState());
  }
  
  return self;
}
