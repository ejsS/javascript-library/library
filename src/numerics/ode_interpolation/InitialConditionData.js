/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.InitialConditionData = {

};

/**
 * Constructor for InitialConditionData
 * @returns InitialConditionData
 */
EJSS_ODE_INTERPOLATION.initialConditionData = function(mDDE) {
  var self = EJSS_ODE_INTERPOLATION.intervalData(Number.NaN,Number.NaN); // reference returned 
  var mState;
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    mDDE.getInitialCondition(time, mState);
    return mState[index];
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    mDDE.getInitialCondition(time, mState);
    var index = beginIndex;
    for (var i=0; i<length; i++) {
      state[i] = mState[index];
      index++;
    }
    return state; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
    var state = mDDE.getState();
    var dimension = state.length;
    mState = new Array(dimension);
   
  return self;
}