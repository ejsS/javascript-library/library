/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.ConstantConditionData = {

};

/**
 * Constructor for ConstantConditionData
 * @returns A ConstantConditionData
 */
EJSS_ODE_INTERPOLATION.constantConditionData = function(aState) {
  var self = EJSS_ODE_INTERPOLATION.intervalData(Number.NaN,Number.NaN); // reference returned 
  var mState;
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    return mState[index];
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    var index = beginIndex;
    for (var i=0; i<length; i++) {
      state[i] = mState[index];
      index++;
    }
    return state; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
    var dimension = aState.length;
    mState = new Array(dimension);
    for (var i=0; i<dimension; i++) {
      mState[i] = aState[i];
    }
   
  return self;
}