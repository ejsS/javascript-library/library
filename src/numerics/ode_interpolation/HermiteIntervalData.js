/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.HermiteIntervalData = {

};

/**
 * Constructor for HermiteIntervalData
 * @returns A HermiteIntervalData
 */
EJSS_ODE_INTERPOLATION.hermiteIntervalData = function(aState, aRate, bState, bRate) {
  var self = EJSS_ODE_INTERPOLATION.intervalData(aState[aState.length-1],bState[bState.length-1]); // reference returned
  var mTimeIndex;
  var mDeltaTime; 
  var mLeftState;
  var mLeftRate;
  var mRightState;
  var mRightRate;
  
  // --------------------------------------------
  // Getters
  // --------------------------------------------

  self.getTimeIndex = function() { return mTimeIndex; }

  self.getDeltaTime = function() { return mDeltaTime; }
  
  self.getLeftState = function() { return mLeftState; }

  self.getLeftRate  = function() { return mLeftRate; }

  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    var theta = (time - self.getLeft())/mDeltaTime;
    var minus1 = theta - 1;
    var prod1 = theta*minus1;
    var prod2 = prod1*(1 - 2*theta);
    var coefX0 = - minus1 - prod2;
    var coefX1 = theta + prod2;
    var coefF0 = prod1*minus1*mDeltaTime;
    var coefF1 = prod1*theta*mDeltaTime;
    return coefX0*mLeftState[index] + coefX1*mRightState[index] + coefF0*mLeftRate[index] + coefF1*mRightRate[index];
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
	self.hermite((time - self.getLeft())/mDeltaTime, state, beginIndex, length);
    return state; 
  }

  /**
   * Does the Hermite interpolation for a given theta = (t - left)/Delta
   * for the given indexes
   * Will also be used by subclasses
   * @param theta
   * @param state
   */
  self.hermite = function(theta, state, beginIndex, length) {
	var minus1 = theta - 1;
	var prod1 = theta*minus1;
	var prod2 = prod1*(1 - 2*theta);
	var coefX0 = - minus1 - prod2;
	var coefX1 = theta + prod2;
	var coefF0 = prod1*minus1*mDeltaTime;
	var coefF1 = prod1*theta*mDeltaTime;
	var index = beginIndex;
	for (var i=0; i<length; i++) {
	  state[i] = coefX0*mLeftState[index] + coefX1*mRightState[index] + coefF0*mLeftRate[index] + coefF1*mRightRate[index];
      index++;
	}
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
    var dimension = aState.length;
    mLeftState  = new Array(dimension);
    mLeftRate   = new Array(dimension);
    mRightState = new Array(dimension);
    mRightRate  = new Array(dimension);

    for (var i=0; i<dimension; i++) {
      mLeftState[i]  = aState[i];
      mLeftRate[i]   = aRate[i];
      mRightState[i] = bState[i];
      mRightRate[i]  = bRate[i];
    }
    mTimeIndex = dimension-1;
    mDeltaTime = bState[mTimeIndex]-aState[mTimeIndex];		

  return self;
}