/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.BootstrapIntervalData = {
};

/**
 * Constructor for BootstrapIntervalData
 * @returns A BootstrapIntervalData
 */
EJSS_ODE_INTERPOLATION.bootstrapIntervalData = function(aState, aRate, bState, bRate, ode) {
  var self = EJSS_ODE_INTERPOLATION.hermiteIntervalData(aState,aRate,bState,bRate); // reference returned
  
  var mBt1_c2;
  var mBt1_c3;
  var mBt1_c4;
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    var deltaTime = self.getDeltaTime();
    var leftState = self.getLeftState();
    var leftRate  = self.getLeftRate();
	var step = (time - self.getLeft())/deltaTime;
	return leftState[index] + step*(deltaTime*leftRate[index] + step*(mBt1_c2[index] + step*(mBt1_c3[index] + step*mBt1_c4[index])));
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    self.bootstrap1((time - self.getLeft())/self.getDeltaTime(), state, beginIndex, length);
    return state; 
  }

  self.bootstrap1 = function(step, state, beginIndex, length) {
    var deltaTime = self.getDeltaTime();
    var leftState = self.getLeftState();
    var leftRate  = self.getLeftRate();
    var index = beginIndex;
	for (var i=0; i<length; i++) {
      state[i] = leftState[index] + step*(deltaTime*leftRate[index] + step*(mBt1_c2[index] + step*(mBt1_c3[index] + step*mBt1_c4[index])));
      index++;
	}
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

    // Coefficients of the first bootstrap
    var ALPHA = 0.25;
    var bt1_den = ALPHA*(ALPHA-1)*(4*ALPHA-2);
    var bt1_cf1 = ALPHA*(-3*ALPHA+2);
    var bt1_cf0 = ALPHA*(-3*ALPHA+4)-1; 
    var bt1_cys = 6*ALPHA*(ALPHA-1);
    
    var dimension = aState.length;
    var deltaTime = self.getDeltaTime();
  
    var state_bt1 = new Array(dimension);
    var rate_bt1  = new Array(dimension);
    mBt1_c2    = new Array(dimension);
    mBt1_c3    = new Array(dimension);
    mBt1_c4    = new Array(dimension);
    self.hermite(ALPHA, state_bt1, 0, dimension-1);
    state_bt1[dimension-1] = self.getLeft() + ALPHA*deltaTime;
    ode.getRate(state_bt1, rate_bt1);

    for (var i=0; i<dimension; i++) {
      var dif = bState[i]-aState[i];
      var f0 = deltaTime*aRate[i];
      var f1 = deltaTime*bRate[i];
      var c4 = (deltaTime*rate_bt1[i] + bt1_cf1*f1 + bt1_cf0*f0 + bt1_cys*dif)/bt1_den;
      var c3 = f1 + f0 - 2*dif - 2*c4;
      mBt1_c4[i] = c4;
      mBt1_c3[i] = c3;
      mBt1_c2[i] = dif - f0 - c3 - c4;
    }
  
  return self;
}