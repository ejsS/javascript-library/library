/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.Bootstrap2IntervalData = {
};

/**
 * Constructor for Bootstrap2IntervalData
 * @returns A Bootstrap2IntervalData
 */
EJSS_ODE_INTERPOLATION.bootstrap2IntervalData = function(aState, aRate, bState, bRate, ode) {
  var self = EJSS_ODE_INTERPOLATION.bootstrapIntervalData(aState,aRate,bState,bRate,ode); // reference returned
    
  var mBt2_c2;
  var mBt2_c3;
  var mBt2_c4;
  var mBt2_c5;
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    var deltaTime = self.getDeltaTime();
    var leftState = self.getLeftState();
    var leftRate  = self.getLeftRate();
	var step = (time - self.getLeft())/deltaTime;
    return leftState[index] + step*(deltaTime*leftRate[index] + step*(mBt2_c2[index] + step*(mBt2_c3[index] + step*(mBt2_c4[index]+step*mBt2_c5[index]))));
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    self.bootstrap2((time - self.getLeft())/self.getDeltaTime(), state, beginIndex, length);
    return state; 
  }

  self.bootstrap2 = function(step, state, beginIndex, length) {
    var deltaTime = self.getDeltaTime();
    var leftState = self.getLeftState();
    var leftRate  = self.getLeftRate();
    var index = beginIndex;
	for (var i=0; i<length; i++) {
      state[i] = leftState[index] + step*(deltaTime*leftRate[index] + step*(mBt2_c2[index] + step*(mBt2_c3[index] + step*(mBt2_c4[index]+step*mBt2_c5[index]))));
      index++;
	}
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

    // Coefficients of the second bootstrap
    var BETA1 = 0.7;
    var BETA2 = 0.85;
    var bt2_den = 2*BETA1*(BETA1-1)*BETA2*(BETA2-1)*(BETA2-BETA1)*(10*BETA1*BETA2-5*BETA2-5*BETA1+3);
    var bt2_cf11 = BETA1*(-3*BETA1+2);
    var bt2_cf01 = BETA1*(-3*BETA1+4)-1; 
    var bt2_cys1 = 6*BETA1*(BETA1-1);
    var bt2_cf12 = BETA2*(-3*BETA2+2);
    var bt2_cf02 = BETA2*(-3*BETA2+4)-1; 
    var bt2_cys2 = 6*BETA2*(BETA2-1);
    var bt2_m44 = BETA1*(2+BETA1*(-6+4*BETA1));
    var bt2_m54 = BETA2*(2+BETA2*(-6+4*BETA2));
    var bt2_m45 = BETA1*(4+BETA1*(-9+5*BETA1*BETA1));
    var bt2_m55 = BETA2*(4+BETA2*(-9+5*BETA2*BETA2));
    
    var dimension = aState.length;
    var timeIndex = dimension-1;
    var deltaTime = self.getDeltaTime();
  
    var state_bt2 = new Array(dimension);
    var rate_bt21 = new Array(dimension);
    var rate_bt22 = new Array(dimension);
    mBt2_c2 = new Array(dimension);
    mBt2_c3 = new Array(dimension);
    mBt2_c4 = new Array(dimension);
    mBt2_c5 = new Array(dimension);
    
    self.bootstrap1(BETA1,state_bt2,0,timeIndex);
    state_bt2[timeIndex] = self.getLeft() + BETA1*deltaTime;
    ode.getRate(state_bt2, rate_bt21);

    self.bootstrap1(BETA2,state_bt2,0,timeIndex);
    state_bt2[timeIndex] = self.getLeft()  + BETA2*deltaTime;
    ode.getRate(state_bt2, rate_bt22);

    for (var i=0; i<dimension; i++) {
      var dif = bState[i]-aState[i];
      var f0 = deltaTime*aRate[i];
      var f1 = deltaTime*bRate[i];
      var e1 = deltaTime*rate_bt21[i] + bt2_cf11*f1 + bt2_cf01*f0 + bt2_cys1*dif;
      var e2 = deltaTime*rate_bt22[i] + bt2_cf12*f1 + bt2_cf02*f0 + bt2_cys2*dif;
      var c4 = (bt2_m55*e1 - bt2_m45*e2)/bt2_den;
      var c5 = (bt2_m44*e2 - bt2_m54*e1)/bt2_den;
      var c3 = f1 + f0 - 2*dif - 2*c4 - 3*c5;
      mBt2_c5[i] = c5;
      mBt2_c4[i] = c4;
      mBt2_c3[i] = c3;
      mBt2_c2[i] = dif - f0 - c3 - c4 - c5;
    }
 
  return self;
}