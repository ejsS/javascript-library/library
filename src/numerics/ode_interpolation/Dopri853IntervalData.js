/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.Dopri853IntervalData = {

};

/**
 * Constructor for Dopri853IntervalData
 * @returns Dopri853IntervalData
 */
EJSS_ODE_INTERPOLATION.dopri853IntervalData = function(aState, bState, coeffs) {
  var self = EJSS_ODE_INTERPOLATION.extraStepsIntervalData(aState,bState,coeffs); // reference returned
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    var theta = (time-self.getLeft())/self.getDeltaTime();
    var theta1 = 1 - theta;
    var coeffs = self.getCoeffs();
    return coeffs[0][index] + theta * (coeffs[1][index] + theta1 * (coeffs[2][index] + theta * (coeffs[3][index] + 
        theta1 * (coeffs[4][index] + theta * (coeffs[5][index] + theta1 * (coeffs[6][index] + theta * coeffs[7][index]))))));
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    var theta = (time-self.getLeft())/self.getDeltaTime();
    var theta1 = 1 - theta;
    var coeffs = self.getCoeffs();
    var index = beginIndex;
    for (var i=0; i<length; i++) {
      state[i] = coeffs[0][index] + theta * (coeffs[1][index] + theta1 * (coeffs[2][index] + theta * (coeffs[3][index] + 
          theta1 * (coeffs[4][index] + theta * (coeffs[5][index] + theta1 * (coeffs[6][index] + theta * coeffs[7][index]))))));
      index++;
    }
    return state; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
}