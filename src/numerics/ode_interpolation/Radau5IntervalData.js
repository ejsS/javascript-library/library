/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.Radau5IntervalData = {

};

/**
 * Constructor for Radau5IntervalData
 * @returns Radau5IntervalData
 */
EJSS_ODE_INTERPOLATION.radau5IntervalData = function(aState, aRate, coeffs) {
  var self = EJSS_ODE_INTERPOLATION.extraStepsIntervalData(aState,bState,coeffs); // reference returned
  var mFinalTime = bState[bState.length-1];
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
	var theta = (time-mFinalTime)/self.getDeltaTime();
    var coeffs = self.getCoeffs();
    var c1m1 = EJSS_ODE_SOLVERS.Radau5.c1m1;
    var c2m1 = EJSS_ODE_SOLVERS.Radau5.c2m1;
	return coeffs[0][index] + theta * (coeffs[1][index] + (theta - c2m1) * (coeffs[2][index] + (theta - c1m1) * coeffs[3][index]));
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    var theta = (time-mFinalTime)/mDeltaTime;
    var coeffs = self.getCoeffs();
    var c1m1 = EJSS_ODE_SOLVERS.Radau5.c1m1;
    var c2m1 = EJSS_ODE_SOLVERS.Radau5.c2m1;
    var index = beginIndex;
    for (var i=0; i<length; i++) {
      state[i] = coeffs[0][index] + theta * (coeffs[1][index] + (theta - c2m1) * (coeffs[2][index] + (theta - c1m1) * coeffs[3][index]));
      index++;
    }
    return state; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
}