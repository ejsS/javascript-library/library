/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.EulerIntervalData = {

};

/**
 * Constructor for EulerIntervalData
 * @returns An EulerIntervalData
 */
EJSS_ODE_INTERPOLATION.eulerIntervalData = function(state, rate, right) {
  var self = EJSS_ODE_INTERPOLATION.intervalData(state[state.length-1],right); // reference returned 
  var mLeftState;
  var mLeftRate;
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    var step = time - self.getLeft();
    return mLeftState[index] + step*mLeftRate[index];
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    var step = time - self.getLeft();
    var index = beginIndex;
    for (var i=0; i<length; i++) {
      state[i] = mLeftState[index] + step*mLeftRate[index];
      index++;
    }
    return state; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
    var dimension = state.length;
    mLeftState = new Array(dimension);
    mLeftRate = new Array(dimension);
    for (var i=0; i<dimension; i++) {
      mLeftState[i] = state[i];
      mLeftRate[i]  =  rate[i];
    }
   
  return self;
}