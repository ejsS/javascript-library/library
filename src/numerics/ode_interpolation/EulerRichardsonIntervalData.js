/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.EulerRichardsonIntervalData = {

};

/**
 * Constructor for EulerRichardsonIntervalData
 * @returns An EulerRichardsonIntervalData
 */
EJSS_ODE_INTERPOLATION.eulerRichardsonIntervalData = function(state, rate, right, K2) {
  var self = EJSS_ODE_INTERPOLATION.intervalData(state[state.length-1],right); // reference returned
  var mTimeIndex;
  var mStepSize; 
  var mLeftState;
  var mLeftRate;
  var mK2;
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  self.interpolate = function(time, index) { 
    var theta = (time-self.getLeft())/mStepSize;
    var b2 = theta*theta*mStepSize;
    var b1 = mStepSize*theta - b2;
    return mLeftState[index]+ b1*mLeftRate[index] + b2*mK2[index];
  }

  self.interpolateState = function(time, state, beginIndex, length) { 
    var theta = (time-self.getLeft())/mStepSize;
    var b2 = theta*theta*mStepSize;
    var b1 = mStepSize*theta - b2;
    var index = beginIndex;
    for (var i=0; i<length; i++) {
      state[i] = mLeftState[index]+ b1*mLeftRate[index] + b2*mK2[index];
      index++;
    }
    return state; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
    var dimension = state.length;
    mLeftState = new Array(dimension);
    mLeftRate  = new Array(dimension);
    mK2        = new Array(dimension);
    for (var i=0; i<dimension; i++) {
      mLeftState[i] = state[i];
      mLeftRate[i]  =  rate[i];
      mK2[i]        =    K2[i];
    }
    mTimeIndex = dimension-1;
    mStepSize = right - state[mTimeIndex];

  return self;
}