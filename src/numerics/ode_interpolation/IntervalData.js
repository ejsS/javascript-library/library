/*
 * Copyright (C) 2014 Francisco Esquembre
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for ODE state interpolation 
 */

var EJSS_ODE_INTERPOLATION = EJSS_ODE_INTERPOLATION || {};

/**
 * Abstract object to be used as base for ode state interpolation 
 */
EJSS_ODE_INTERPOLATION.IntervalData = {

};

/**
 * Constructor for IntervalData
 * @returns An abstract IntervalData
 */
EJSS_ODE_INTERPOLATION.intervalData = function(mLeft, mRight) {
  var self = {};							// reference returned 

  /**
   * Returns the left side of the interval
   * @return double
   */
  self.getLeft = function() { return mLeft; };

  /**
   * Returns the right side of the interval
   * @return double
   */
  self.getRight = function() { return mRight; };

  /**
   * Changes the right side of the interval
   * @param rightSide
   */
  self.setRight = function(rightSide) { mRight = rightSide; };

  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  /**
   * Retrieve the state for the given time for one index
   * @param time double the given time for the state
   * @param index the index to interpolate 
   * @return double the interpolated value 
   */
  self.interpolate = function(time, index) { return Number.NaN;  }

  /**
   * Retrieve the state for the given time for one index or a subset of indexes
   * @param time double the given time for the state
   * @param state double[] a place holder for the returned state
   * @param beginIndex the first index to interpolate
   * @param length the range of indexes pf the state to interpolate   
   * @return double[] the array with the data, same as passed state, 
   */
  self.interpolateState = function(time, state, beginIndex, length) { return state; }
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
}