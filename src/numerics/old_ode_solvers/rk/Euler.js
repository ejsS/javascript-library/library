/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Constructor for SolverInterpolatorDiscreteTime
 * @returns An abstract SolverInterpolatorDiscreteTime
 */
EJSS_ODE_SOLVERS.createEulerODESolver = function(mODE) {
  var self = EJSS_ODE_SOLVERS.createSolverInterpolatorDiscreteTime(mODE); // reference returned 

  var mRate;

  var super_initialize = self.initialize;

  self.initialize = function(stepSize) {
  	super_initialize(stepSize);
  	
    mRate = new Array(self.getDimension());
  };

  /**
   * Computes an intermmediate step
   * @param step double the step to take
   * @param state double[] the placeholder for the computed state
   */
  self.computeIntermediateStep = function(step, state) {  	
  	mODE.getRate(state,mRate);
  	for (var i=0,n=state.length; i<n; i++) {
  		state[i] = state[i] + step * mRate[i];
  	}
    return state;
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}