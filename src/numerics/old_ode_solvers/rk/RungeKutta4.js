/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Constructor for SolverInterpolatorDiscreteTime
 * @returns An abstract SolverInterpolatorDiscreteTime
 */
EJSS_ODE_SOLVERS.createRungeKutta4ODESolver = function(mODE) {
  var self = EJSS_ODE_SOLVERS.createSolverInterpolatorDiscreteTime(mODE); // reference returned 

  var initialState;
  var mRate1,mRate2,mRate3,mRate4;

  var super_initialize = self.initialize;

  self.initialize = function(stepSize) {
  	super_initialize(stepSize);
  	
    initialState = new Array(self.getDimension());
    mRate1 = new Array(self.getDimension());
    mRate2 = new Array(self.getDimension());
    mRate3 = new Array(self.getDimension());
    mRate4 = new Array(self.getDimension());
  };

  /**
   * Computes an intermmediate step
   * @param step double the step to take
   * @param state double[] the placeholder for the computed state
   */
  self.computeIntermediateStep = function(step, state) {  	
  	mODE.getRate(state,mRate1);
  	for (var i=0,n=state.length; i<n; i++) {
  		initialState[i] = state[i];
  		state[i] = initialState[i] + step * mRate1[i]/2;
  	}
  	mODE.getRate(state,mRate2);
  	for (var i=0,n=state.length; i<n; i++) {
  		state[i] = initialState[i] + step * mRate2[i]/2;
  	}
  	mODE.getRate(state,mRate3);
  	for (var i=0,n=state.length; i<n; i++) {
  		state[i] = initialState[i] + step * mRate3[i];
  	}
  	mODE.getRate(state,mRate4);
  	for (var i=0,n=state.length; i<n; i++) {
  		state[i] = initialState[i] + step * (mRate1[i]+2*mRate2[i]+2*mRate3[i]+mRate4[i])/6;
  	}
    return state;
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  
  return self;
}