/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Multistep solver 
 */
EJSS_ODE_SOLVERS.ODEMultistepSolver = {
  NO_ERROR : 0, 	// No error
  DID_NOT_CONVERGE : 1      	// 

};

/**
 * Constructor for SolverInterpolatorDiscreteTime
 * @returns An abstract SolverInterpolatorDiscreteTime
 */
EJSS_ODE_SOLVERS.createODEMultistepSolver = function(mODE) {
  var self = {}; // reference returned 

  var sMaxMessages = 3; // maximum number of error messages
  var mErr_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR;
  var mMaxIterations = 200;
  var mEnableExceptions = false;
  var mErr_msg = "";
  var mFixedStepSize = 0.1;
  var mInternalODE = EJSS_ODE_SOLVERS.createODEMultistepSolverInternalODE(mODE);
  var mODEEngine = EJSS_ODE_SOLVERS.createCashKarp45(mInternalODE);
  
  // --------------------------------------------
  // Setters and getters
  // --------------------------------------------

  /**
   * Enables runtime exceptions if the solver does not converge.
   * @param enable boolean
   */
  self.enableRuntimeExceptions = function (enable) {
    mEnableExceptions = enable;
  }

  /**
   * Sets the maximum number of iterations.
   * @param n maximum
   */
  self.setMaxIterations = function(n) {
    mMaxIterations = Math.max(1, n);
  }

  /**
   * Sets the tolerance of the adaptive ODE solver.
   * @param tol the tolerance
   */
  self.setTolerance = function(tol) {
    tol = Math.abs(tol);
    mODEEngine.setTolerance(tol);
  }

  /**
   * Gets the tolerance of the adaptive ODE solver.
   * @return
   */
  self.getTolerance = function() {
    return mODEEngine.getTolerance();
  }
  
   /**
   * Gets the error code.
   * Error codes:
   *   EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR
   *   EJSS_ODE_SOLVERS.ODEMultistepSolver.DID_NOT_CONVERGE
   * @return int
   */
  self.getErrorCode = function() {
    return mErr_code;
  }
  
  // --------------------------------------------
  // Private or protected methods
  // --------------------------------------------
  
  /**
   * Initializes the ODE solver.
   *
   * ODE solvers use this method to allocate temporary arrays that may be required to carry out the solution.
   * The number of differential equations is determined by invoking getState().length on the ODE.
   *
   * @param stepSize
   */  
  self.initialize = function(stepSize) {
    mMaxMessages = 4; // reset the message counter to produce more messages
    mErr_msg = "";    //$NON-NLS-1$
    mErr_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR;
    mFixedStepSize = stepSize;
    mInternalODE.setInitialConditions();
    mODEEngine.initialize(stepSize);
  };
    
  /**
   * Sets the initial step size.
   *
   * The step size may change if the ODE solver implements an adaptive step size algorithm
   * such as RK4/5.
   *
   * @param stepSize
   */
  self.setStepSize = function(stepSize) {
    mMaxMessages = 4;          // reset the message counter to produce more messages
    mFixedStepSize = stepSize; // the fixed step size
    if(stepSize<0) {
      mODEEngine.setStepSize(Math.max(-Math.abs(mODEEngine.getStepSize()), stepSize));
    } else { // stepSize is positive
      mODEEngine.setStepSize(Math.min(mODEEngine.getStepSize(), stepSize));
    }
  };
  
  /**
   * Sets the number of error messages if ODE solver did not converge.
   * @param n int
   */
  self.setMaximumNumberOfErrorMessages = function(n) {
    mMaxMessages = n;
  }

  /**
   * Gets the step size.
   * The step size is the fixed step size, not the size of the ODEAdaptiveSolver steps that are combined into a single step.
   *
   * @return the step size
   */
  self.getStepSize = function() {
    return mFixedStepSize;
  }

  // ----------------------------------------------------
  // stepping
  // ----------------------------------------------------
  
  
  /**
   * Steps (advances) the differential equations by the stepSize.
   *
   * The ODESolver invokes the ODE's getRate method to obtain the initial state of the system.
   * The ODESolver then advances the solution and copies the new state into the
   * state array at the end of the solution step if desired tolerance was reached.
   *
   * @return the actual step
   */
  self.step = function() {
    mErr_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR;
    mInternalODE.setInitialConditions(); // stores the ode's initial conditions
    var remainder = 0;
    if(mFixedStepSize>0) {
      remainder = plus();
    } else {
      remainder = minus();
    }
    mInternalODE.update(); // updates the ode
    return mFixedStepSize-remainder; // the step size that was actually taken
  }

  // ----------------------------------------------------
  // Utils
  // ----------------------------------------------------
  
  /**
   * Steps the ode with a positive stepsize.
   *
   * @return the step size
   */
  function plus() {           // positive step size
    var tol = mODEEngine.getTolerance();
    var remainder = mFixedStepSize; // track the remaining step
    if((mODEEngine.getStepSize()<=0)||(                       // is the stepsize postive?
      mODEEngine.getStepSize()>mFixedStepSize)||(              // is the stepsize larger than what is requested?
        mFixedStepSize-mODEEngine.getStepSize()==mFixedStepSize // is the stepsize smaller than the precision?
          )) {
      mODEEngine.setStepSize(mFixedStepSize);                  // reset the step size and let it adapt to an optimum size
    }
    var counter = 0;
    while(remainder>tol*mFixedStepSize) {           // check to see if we are close enough
      counter++;
      var oldRemainder = remainder;
      if(remainder<mODEEngine.getStepSize()) {      // temporarily reduce the step size so that we hit the exact dt value
        var tempStep = mODEEngine.getStepSize(); // save the current optimum step size
        mODEEngine.setStepSize(remainder);          // set the step size to the remainder
        var delta = mODEEngine.step();
        remainder -= delta;
        mODEEngine.setStepSize(tempStep);           // restore the original step size
      } else {
        remainder -= mODEEngine.step();             // do a step and set the remainder
      }
      // check to see if roundoff error prevents further calculation.
      if ( (mODEEngine.getErrorCode()!=EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR) ||
           (Math.abs(oldRemainder-remainder)<=Number.MIN_VALUE) ||
           (tol*mFixedStepSize/10.0>mODEEngine.getStepSize()) || 
           (counter>mMaxIterations)) {
        mErr_msg = "ODEMultiStep did not converge. Remainder="+remainder; //$NON-NLS-1$
        mErr_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.DID_NOT_CONVERGE;
        if (mEnableExceptions) {
          throw mErr_msg;
        }
        if (mMaxMessages>0) {
          mMaxMessages--;
          console.log(mErr_msg);
        }
        break;
      }
    }
    return remainder;
  }

  /**
   * Steps the ode with a negative stepsize.
   *
   * @return the step size
   */
  function minus() { // negative step size
    var tol = mODEEngine.getTolerance();
    var remainder = mFixedStepSize; // track the remaining step
    if ((mODEEngine.getStepSize()>=0)||(                       // is the step negative?
         mODEEngine.getStepSize()<mFixedStepSize)||(              // is the stepsize larger than what is requested?
         mFixedStepSize-mODEEngine.getStepSize()==mFixedStepSize // is the stepsize smaller than the precision?
          )) {
      mODEEngine.setStepSize(mFixedStepSize);                  // reset the step size and let it adapt to an optimum size
    }
    var counter = 0;
    while(remainder<tol*mFixedStepSize) {           // check to see if we are close enough
      counter++;
      var oldRemainder = remainder;
      if(remainder>mODEEngine.getStepSize()) {
        var tempStep = mODEEngine.getStepSize(); // save the current optimum step size
        mODEEngine.setStepSize(remainder);          // set the step RK4/5 size to the remainder
        var delta = mODEEngine.step();
        remainder -= delta;
        mODEEngine.setStepSize(tempStep);           // restore the original step size
      } else {
        remainder -= mODEEngine.step();             // do a step and set the remainder
      }
      // check to see if roundoff error prevents further calculation.
      if ( (mODEEngine.getErrorCode()!=EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR) ||
           (Math.abs(oldRemainder-remainder)<=Number.MIN_VALUE) || 
           (tol*mFixedStepSize/10.0<mODEEngine.getStepSize()) ||
           (counter>mMaxIterations)) {
        mErr_msg = "ODEMultiStep did not converge. Remainder="+remainder; //$NON-NLS-1$
        mErr_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.DID_NOT_CONVERGE;
        if(enableExceptions) {
          throw mErr_msg;
        }
        if(mMaxMessages>0) {
          mMaxMessages--;
          console.log(mErr_msg);
        }
      }
    }
    return remainder;
  }
  


  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
}

EJSS_ODE_SOLVERS.createODEMultistepSolverInternalODE = function(mODE) {
  var self = {}; // reference returned 
  var mEngineState = [];

    /**
     * Gets the rate using the given state.
     *
     * @param state double[]
     * @param rate double[]
     */
    self.getRate = function(state, rate) {
      mODE.getRate(state, rate);
    }

    /**
     * Gets the state.
     *
     * @return double[]
     */
    self.getState = function() {
      return mEngineState;
    }

    /**
     * Sets the initial conditions using the current state of the ODE.
     *
     * @return double[]
     */
    self.setInitialConditions = function() {
      var state = mODE.getState();
      if(state==null) {
        return;
      }
      if((mEngineState==null)||(mEngineState.length!=state.length)) {
        mEngineState = new Array(state.length); // create an engine state with the correct size
      }
      for (var i=0,n=state.length; i<n; i++) {
        mEngineState[i] = state[i];
      }
    }

    /**
     * updates the ODE state using the engine's internal state.
     */
    self.update = function() {
      var state = mODE.getState();
      for (var i=0,n=state.length; i<n; i++) {
        state[i] = mEngineState[i];
      }
    }

  self.setInitialConditions();

  return self;
}

EJSS_ODE_SOLVERS.createCashKarp45 = function(mODE) {
  var self = {}; // reference returned 

  // embedding constants Dormand-Prince 4th and 5th order
//  var sA = [
//    [1.0/5.0], 
//    [3.0/40.0, 9.0/40.0], 
//    [3.0/10.0, -9.0/10.0, 6.0/5.0], 
//    [226.0/729.0, -25.0/27.0, 880.0/729.0, 55.0/729.0], 
//    [-181.0/270.0, 5.0/2.0, -266.0/297.0, -91.0/27.0, 189.0/55.0]
//    ];
  // ch contains the 5th order coefficients
//  var sB5 = [19.0/216.0, 0.0, 1000.0/2079.0, -125.0/216.0, 81.0/88.0, 5.0/56.0];
  // er array contains the error coefficients; the difference between the 4th and 5th order coefficients
  // er[0] is computed to be -11/360 = 31/540-19/216
//  var sEr = [-11.0/360.0, 0.0, 10.0/63.0, -55.0/72.0, 27.0/40.0, -11.0/280.0];

  // embedding constants Cash-Karp 4th and 5th order
  var sA = [
    [1.0/5.0], 
    [3.0/40.0, 9.0/40.0], 
    [3.0/10.0, -9.0/10.0, 6.0/5.0], 
    [-11.0/54.0, 5.0/2.0,  -70.0/27.0, 35.0/27.0],
    [1631.0/55296.0, 175.0/512.0, 575.0/13824.0, 44275.0/110592.0, 253.0/4096.0]
    ];
  // ch contains the 5th order coefficients
  var sB5 = [37.0/378.0, 0.0,  250.0/621.0, 125.0/594.0, 0.0, 512.0/1771.0];
  // er array contains the error coefficients; the difference between the 4th and 5th order coefficients
  // er[0] is computed to be -11/360 = 31/540-19/216
  var sEr = [ 277./64512.,    0.,   -6925./370944.,  6925./202752.,   277./14336.,   -277./7084.];
  var sNumStages = 6; // number of intermediate rate computations
  
  var mError_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR;
  var mStepSize = 0.01;
  var mNumEqn = 0;
  var mTemp_state = [];
  var mK = []; // [][]
  var mTruncErr;
  var mTol = 1.0e-6;
  var mEnableExceptions = false;


  /**
   * Enables runtime exceptions if the solver does not converge.
   * @param enable boolean
   */
  self.enableRuntimeExpecptions = function(enable) {
    mEnableExceptions = enable;
  }

  /**
   * Sets the step size.
   *
   * The step size may change when the step method is invoked.
   *
   * @param stepSize
   */
  self.setStepSize = function(stepSize) {
    mStepSize = stepSize;
  }

  /**
   * Gets the step size.
   *
   * The stepsize is adaptive and may change as the step() method is invoked.
   *
   * @return the step size
   */
  self.getStepSize = function() {
    return mStepSize;
  }

  /**
   * Method setTolerance
   *
   * @param _tol
   */
  self.setTolerance = function(tol) {
    mTol = Math.abs(tol);
    if(mTol<1.0E-12) {
      var err_msg = "Error: Cash-Karp ODE solver tolerance cannot be smaller than 1.0e-12.";
      if(mEnableExceptions) {
        throw err_msg;
      }
      console.log(err_msg);
      mTol = 1.0e-12;
    }
  }

  /**
   * Method getTolerance
   *
   *
   * @return
   */
  self.getTolerance = function() {
    return mTol;
  }

  /**
   * Gets the error code.
   * Error codes:
   *   EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR
   *   EJSS_ODE_SOLVERS.ODEMultistepSolver.DID_NOT_CONVERGE
   * @return int
   */
  self.getErrorCode = function() {
    return mError_code;
  }

  /**
   * Initializes the ODE solver.
   *
   * Temporary state and rate arrays are allocated.
   * The number of differential equations is determined by invoking getState().length on the ODE.
   *
   * @param _stepSize
   */
  self.initialize = function(stepSize) {
    mStepSize = stepSize;
    var state = mODE.getState();
    if(state==null) { // state vector not defined.
      return;
    }
    if (mNumEqn!=state.length) {
      mNumEqn = state.length;
      mTemp_state = new Array(mNumEqn);
      mK = new Array(sNumStages); // six intermediate rates
      for (var i=0; i<sNumStages; i++) mK[i] = new Array(mNumEqn); 
    }
  }

  /**
   * Steps (advances) the differential equations by the stepSize.
   *
   * The ODESolver invokes the ODE's getRate method to obtain the initial state of the system.
   * The ODESolver then advances the solution and copies the new state into the
   * state array at the end of the solution step.
   *
   * @return the step size
   */
  self.step = function() {
    var i, j, s;
    var iterations = 10;
    var currentStep = mStepSize;
    var error = 0;
    var state = mODE.getState();

    mError_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.NO_ERROR;
    mODE.getRate(state, mK[0]); // get the initial rate
    do {
      iterations--;
      currentStep = mStepSize;
      // Compute the k's
      for (s = 1; s<sNumStages; s++) {
        for (i = 0; i<mNumEqn; i++) {
          mTemp_state[i] = state[i];
          for (j = 0; j<s; j++) {
            mTemp_state[i] = mTemp_state[i]+mStepSize*sA[s-1][j]*mK[j][i];
          }
        }
        mODE.getRate(mTemp_state, mK[s]);
      }
      // Compute the error
      error = 0;
      for (i = 0; i<mNumEqn; i++) {
        mTruncErr = 0;
        for (s = 0; s<sNumStages; s++) {
          mTruncErr = mTruncErr+mStepSize*sEr[s]*mK[s][i];
        }
        error = Math.max(error, Math.abs(mTruncErr));
      }
      if (error<=Number.MIN_VALUE) { // error too small to be meaningful,
        error = mTol/1.0e5;         // increase stepSize x10
      }
      // find h step for the next try.
      if (error>mTol) {              // shrink, no more than x10
        var fac = 0.9*Math.pow(error/mTol, -0.25);
        mStepSize = mStepSize*Math.max(fac, 0.1);
      } 
      else if (error<mTol/10.0) {  // grow, but no more than factor of 10
        var fac = 0.9*Math.pow(error/mTol, -0.2);
        if (fac>1) {                // sometimes fac is <1 because error/tol is close to one
          mStepSize = mStepSize*Math.min(fac, 10);
        }
      }
    } while( (error>mTol) && (iterations>0) );
    // advance the state
    for (i = 0; i<mNumEqn; i++) {
      for (s = 0; s<sNumStages; s++) {
        state[i] += currentStep*sB5[s]*mK[s][i];
      }
    }
    if (iterations==0) {
      mError_code = EJSS_ODE_SOLVERS.ODEMultistepSolver.DID_NOT_CONVERGE;
      if(mEnableExceptions) {
        throw "DormanPrince45 ODE solver did not converge.";
      }
    }
    return currentStep; // the value of the step actually taken.
  }

  self.initialize(mStepSize);
  
  return self;
}

