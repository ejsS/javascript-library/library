/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Framework for numerics : ODE solving 
 */

var EJSS_ODE_SOLVERS = EJSS_ODE_SOLVERS || {};

/**
 * Abstract object to be used as base for ode solver which discretize the time 
 */
EJSS_ODE_SOLVERS.SolverInterpolatorDiscreteTime = {

};

/**
 * Constructor for SolverInterpolatorDiscreteTime
 * @returns An abstract SolverInterpolatorDiscreteTime
 */
EJSS_ODE_SOLVERS.createSolverInterpolatorDiscreteTime = function(mODE) {
  var self = {};							// reference returned 

  // ODE variables
  var mErrorCode=0; //InterpolatorEventSolver.NO_ERROR;
  var mDimension; // The length of the state array
  var mTimeIndex; // The index of the independent variable (usually the time)
  var mAccumulatedEvaluations = 0; // Number of evaluations so far
  var mStepSize = 0.1; // the preferred step size
  var mMaximumStepSize = Number.POSITIVE_INFINITY;
  var mInitialTime=0; // The time before the step is taken
  var mFinalTime=0;   // The time after the step is taken
  var mInitialState;  // The state array before the step is taken
  var mInitialRate;   // The rate array before the step is taken
  var mFinalState;    // The state array after the step is taken
  var mFinalRate;     // The rate array after the step is taken
  var mRelativeTolerance = 0;

  // DDE variables
  var mIsDDE = false;
  var mDiscontinuities = [];
  var mNextDiscontinuity;
  
  // Memory
  // var mStateHistory = EJSS_ODE_SOLVERS.createStateHistory();
  var mStateHistoryLength;
  
  // --------------------------------------------
  // Functions to be defined by subclasses
  // --------------------------------------------

  /**
   * The number of function evaluations per step
   * @return int
   */
  self.getNumberOfEvaluations = function() { return 0; };

  /**
   * Allocates other arrays needed for the method
   */
  self.allocateOtherArrays = function() {}; // do nothing

  /**
   * Computes one intermediate step not caring about precision
   * @param step double the length of the step
   * @param state double[] the target array
   * @return same as state
   */
  self.computeIntermediateStep = function(step, state) { return null; }

  /**
   * Computes one intermediate step not caring about precision but taking care of possible discontinuities
   * @param _step the length of the step
   * @param _state the target array
   * @return one of the flags in the checkDiscontinuity(double[]) method of ODEInterpolatorEventSolver
   * @see ODEInterpolatorEventSolver#checkDiscontinuity(double[])
   */
  self.computeCarefulIntermediateStep = function(eventSolver, step, state) { return 0; }

  /**
   * Sets the tolerance of the adaptive ODE solver.
   * @param tol the tolerance
   */
  self.setTolerance = function(tolerance) {
    mRelativeTolerance = Math.abs(tolerance);
  }
  
  // --------------------------------------------
  // Private or protected methods
  // --------------------------------------------

  self.getDimension = function() {
  	return mDimension;
  };
  
  /**
   * Initializes the ODE solver.
   *
   * ODE solvers use this method to allocate temporary arrays that may be required to carry out the solution.
   * The number of differential equations is determined by invoking getState().length on the ODE.
   *
   * @param stepSize
   */  
  self.initialize = function(stepSize) {
  	mStepSize = stepSize;

  	// Extract the components of the state (i.e. its properties)
  	var state = mODE.getState();
  	mDimension = state.length;
  	
  };
  
  /**
   * Steps (advances) the differential equations by the stepSize.
   *
   * The ODESolver invokes the ODE's getRate method to obtain the initial state of the system.
   * The ODESolver then advances the solution and copies the new state into the
   * state array at the end of the solution step.
   *
   * @return the step size
   */
  self.step = function() {
  	var state = mODE.getState();
    self.computeIntermediateStep(mStepSize,state);
  	return 0;
  }
    
  /**
   * Sets the initial step size.
   *
   * The step size may change if the ODE solver implements an adaptive step size algorithm
   * such as RK4/5.
   *
   * @param stepSize
   */
  self.setStepSize = function(stepSize) {
  	mStepSize = stepSize;
  };

  /**
   * Gets the step size.
   *
   * @return the step size
   */
  self.getStepSize = function() {
  	return mStepSize;
  };
  
  // ----------------------------------------------------
  // Utils
  // ----------------------------------------------------
  

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
}
