/*
* Copyright (C) 2020 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for view and control elements
 * @module core
 */

var EJSS_CORE = EJSS_CORE || {};

/**
 * A view with properties and actions
 * @class View
 * @constructor
 */

/**
 * View: A base 'class' for a view that 'talks' (typically to a model)
 * via elements' properties and actions.
 * Make you classes 'inherit' from this one by
 * - declaring:
 *   var _view = EJSS_CORE.createView("_view");
 * - redeclaring (extending) the _reset() function, and
 * - returning _view.
 * @method createView
 */
EJSS_CORE.createView = function(container) {

	var self = {};
	var mTopLevelElement;  // The top level element
	var mReporter;					// function to call whenever there are user interactions
	var mVariablesList = {};		// Object with all variables registered
	var mActionsList = {};			// Object with all actions registered
	var mUnnamedUpdateList = [];	// Listeners without a variable
	var mUpdateList = [];			// Array with all variables with listeners
	var mResetableList = [];		// Array of all control elements that need a call to reset()
	var mInitializableList = [];	// Array of all control elements that need a call to initialize()
	var mRenderList = [];			// Array of all control elements that need a call to render()
	var mTouchList = [];			// Array of all control elements that accept a call to touch()
	var mCollectersList = [];		// Array of all control elements that need a call to dataCollected() after data collection
	var mInteractionList = [];		// Pending interaction commands
	var mEnableEPubList = [];       // Array of all control elements that need a call to enableEPub() when running on an ePub
	var mAdjustPositionList = [];		// Array of all control elements that accept a call to adjustPosition() 
    var mFontResizeListenerList = [];	// Array of listener for font resize events
    var mResizeListenerList = [];	    // Array of listener for view resize
    var mOrientationChangeListenerList = [];	    // Array of listener for view orientation change
	var mReportNeeded = true;
	var mInitValueList = [];
	var mResourcePath;
	var mLibraryPath;

	var mRegInteractions = [];			// Register of action interactions
	var mIsRegInteractions = false;	
	var mIsShortRegInteractions;
	var mWSRegInteractions;
	var mCBRegIngeractions;

    var mDescriptionPages = []; // Place holder for user defined description pages

	var mOnBlurList = [];	// Listeners for window.onblur
	var mOnFocusList = [];	// Listeners for window.onfocus
	var mDummyContainer;

	// ------------------------------------------------------------------------
	// This part is used by the view subclass at creation time
	// ------------------------------------------------------------------------

	self._getTopLevelElement = function() { return mTopLevelElement; };
	
	/**
	 * Clears all elements, links to actual model variable setters/getters, and actions.
	 * Only leaves the connection to the reporter (the model) untouched
	 * @method _clearAll
	 */
	self._clearAll = function() {
    mTopLevelElement.innerHTML = "";   // Remove all elements into the container
		//self[container].innerHTML = "";		// Remove all elements into the container
		mVariablesList = {};				// Object with all variables registered
		mActionsList = {};					// Object with all actions registered
		mUnnamedUpdateList = [];			// Listeners without a variable
		mUpdateList = [];					// Array with all variables with listeners
		mInitValueList = [];				// Array with all variables with init values
		mResetableList = [];				// Array of all control elements that need a call to reset()
		mInitializableList = [];			// Array of all control elements that need a call to initialize()
		mRenderList = [];					// Array of all control elements that need a call to render()
		mTouchList = [];					// Array of all control elements that accept a call to touch()
	    mCollectersList = [];		        // Array of all control elements that need a call to dataCollected() after data collection
	    mEnableEPubList = [];			    // Array of all control elements that need a call to enableEPub() when running on an ePub
	    mAdjustPositionList = [];		    // Array of all control elements that accept a call to adjustPosition() 
        mFontResizeListenerList = [];	    // Array of listener for font resize events
        mResizeListenerList = [];	        // Array of listener for view resize events
        mOrientationChangeListenerList = [];	// Array of listener for view orientation change
		mInteractionList = [];				// Pending interaction commands
		mReportNeeded = true;
	};

	self._setResourcePath = function(path) {
		mResourcePath = path;
	};


    function startsWith(fullStr, str) {
       return (fullStr.match("^"+str)==str);
    }

    /** 
     * Looks for the file in different places.
     * The user may define an array of base64 converted files for direct use 
    */
	self._getResourcePath = function(filename, forIbooks) {
	    // no filename
	    if (filename==null || filename.length<=0) return filename;
	    if (startsWith(filename,"local:")) return filename; 
	    if (startsWith(filename,"data:")) return filename; 
	    if (startsWith(filename,"blob:")) return filename; 
	    if (startsWith(filename,"http:") || startsWith(filename,"https:") || startsWith(filename,"ws:")) return filename; 
	    
        if (typeof __base64Images !== 'undefined') {
          var base64 = __base64Images[filename];
          if (base64) return base64;
        }
	    
	    if ((mLibraryPath) && (!startsWith(filename,mLibraryPath)) && (filename.charAt(0) == '/')) {
	    	// with libraryPath
			//console.log("Path is "+ mLibraryPath + "images" + filename);
			filename = mLibraryPath + "images" + filename;
		} else if ((mResourcePath) && (!startsWith(filename,mResourcePath)) && (filename.charAt(0) != '/')) { 
			// with resourcePath
			//console.log("Path2 is "+ mResourcePath + filename);
		  	filename = mResourcePath + filename;
		}
		
		// change for ibooks 
		if (forIbooks && startsWith(window.location.protocol,"ibooks")) {
			// get path
			var path = window.location.pathname;
			path = path.substring(0,path.lastIndexOf('/'));
			//if (path.charAt(0)=='/') path = path.substring(1);
			if (startsWith(filename,"./")) filename = filename.substring(2);
			filename = window.location.protocol + "://" + window.location.host + "/" + path + "/" + filename;
		}
		
		//console.log("Path3 is "+ filename);
		return filename;
	};

	self._setLibraryPath = function(path) {
		mLibraryPath = path;
	};
	
	
	self._showDocument = function (url) {
        window.open(self._getResourcePath(url));
    };
    
	self._addDescriptionPage = function (name,url) {
		mDescriptionPages[name] = url;
    };

	self._openDescriptionPage = function (name) {
        var url = mDescriptionPages[name];
        if (url) window.open(self._getResourcePath(url));
        else console.log("Desription page not found: "+name);
    };

	/**
	 * Adds a variable to the list of variables which
	 * - their values will be requested (and passed to listeners) when _update() is called.
	 * - can be linked to elements' properties and reported as part of interactions
	 * This is typically called by the view itself to define its input/output API
	 * @method _registerVariable
	 * @param variable the name of the variable
	 * @param initiaValue an optional initial value
	 */
	self._registerVariable = function(variable, initialValue) {
		mVariablesList[variable] = {
			getter : null,
			setter : null,
			listeners : [],
			collectors : [],
			value : initialValue
		};
	};

	/**
	 * Adds a set of variables to the list of variables which
	 * - their values will be requested (and passed to listeners) when _update() is called.
	 * - can be linked to elements' properties and reported as part of interactions
	 * This is typically called by the view itself to define its input/output API
	 * @method _registerVariables
	 * @param variables the names of the variables
	 */
	self._registerVariables = function(variables) {
		for (var i = 0; i < variables.length; i++)
			self._registerVariable(variables[i]);
	};

	/**
	 * Adds an action to the list of possible actions that can be triggered by
	 * user interaction.
	 * This is typically called by the view itself to define its input/output API
	 * @method _registerAction
	 * @param action the name of the action
	 */
	self._registerAction = function(action) {
		mActionsList[action] = {
			action : null
		};
	};

	/**
	 * Adds a set of actions to the list of possible actions that can be triggered by
	 * user interaction.
	 * This is typically called by the view itself to define its input/output API
	 * @method _registerActions
	 * @param action the names of the actions
	 */
	self._registerActions = function(actions) {
		for (var i = 0; i < actions.length; i++)
			self._registerAction(actions[i]);
	};

	/**
	 * Creates and adds a new control element to the view.
	 * If the name is not null, the element is added as a new property to the view.
	 * Example:
	 *   view._addElement (EJSS_DRAWING2D.createDrawingPanel,"drawingPanel",parent);
	 * You can access the new element by view.drawingPanel.
	 * @method _addElement
	 * @param constructor the function used to create the control element. Will be called
	 * as in constructor(name)
	 * @param name the optional name of the element, though some constructors require it
	 * @param parent an optional parent of the element, though you can use setPropertyt("parent",parent) later
	 * @param args optional args of the constructor
	 * @return the newly created control element
	 */
	self._addElement = function(constructor, name, parent, args) {
		var controlElement = EJSS_CORE.promoteToControlElement(constructor(name,args), self, name);
		if (controlElement.render)
			mRenderList.push(controlElement);
		if (controlElement.touch)
			mTouchList.push(controlElement);
		if (controlElement.dataCollected)
			mCollectersList.push(controlElement);
		if (controlElement.reset)
			mResetableList.push(controlElement);
		if (controlElement.initialize)
			mInitializableList.push(controlElement);
		if (controlElement.enableEPub)
			mEnableEPubList.push(controlElement);
		if (controlElement.adjustPosition)
			mAdjustPositionList.push(controlElement);
		if (name)
			self[name] = controlElement;
		if (parent)
			controlElement.setProperty("Parent", parent);
		return controlElement;
	};

	/**
	 * Returns the given initial value (if any) of a registered variable
	 * @method _getInitialValue
	 * @param variable the name of the variable
	 * @return the initial value or "undefined"
	 */
	self._getInitialValue = function(variable) {
		var variableDefinition = mVariablesList[variable];
		if (variableDefinition !== undefined)
			return variableDefinition.value;
		return undefined;
	};

	/**
	 * Starts up a view, even in the absence of a model
	 * @method _startUp
	 */
	self._startUp = function() {
		self._reset();
		self._initValues();
		self._initialize();
		self._update();
		self._render();
	};

	/**
	 * Sets particular behavior of the whole HTML view
	 * @method _startUp
	 */
	self._setRootProperty = function(model,property, value) {
      switch (property) {
		case "RunAlways" : model.setRunAlways(value); break;
		case "OnBlur"  : self._addOnBlurAction(value); break;
		case "OnFocus" : self._addOnFocusAction(value); break;
		case "InnerHTML" : self._setInnerHTML(value); break;
		case "OnResize" : self._addResizeListener(value); break;
		case "OnOrientationChange" : self._addOrientationChangeListener(value); break;
		default : 
			console.log("WARNING: View property not registered : " + property);
		break;
	  }
	};
	
	// ------------------------------------------------------------------------
	// This part is used by the model at creation time
	// ------------------------------------------------------------------------

	/**
	 * Sets the function to call whenever there are interactions
	 * @method _setReportInteractionMethod
	 * @param reporter the function that will be called when interactions must be reported
	 */
	self._setReportInteractionMethod = function(reporter) {
		mReporter = reporter;
	};

	/**
	 * Links a view variable to accessors that allow to read/write its value from/to the model
	 * The view will call:
	 * - getter() to read the value of the variable from the model
	 * - setter(value) to set the value of the variable to a new value
	 * @method _linkVariable
	 * @param variable the name of the view variable (previously registered by the view)
	 * @param getter the function that allows to read the value from the model
	 * @param setter the function that allows to set the value to the model
	 * @param initiaValue an optional initial value
	 * @return the variable definition object
	 */
	self._linkVariable = function(variable, getter, setter, initialValue) {
		var variableDefinition = mVariablesList[variable];
		if (!variableDefinition)
			console.log("WARNING: view._setAccessors() - Variable not found : " + variable);
		else {
			variableDefinition.getter = getter;
			variableDefinition.setter = setter;
			variableDefinition.value = initialValue;
			// Manage the update list
			var index = mUpdateList.indexOf(variableDefinition);
			if (variableDefinition.getter) {
				if ((variableDefinition.listeners.length > 0) && (index < 0))
					mUpdateList.push(variableDefinition);
			} else {// Has no getter, remove it from the update list, if there
				if (index >= 0)
					mUpdateList.splice(index, 1);
			}
			// Manage the init values list
			index = mInitValueList.indexOf(variableDefinition);
			if (variableDefinition.value) {
				if ((variableDefinition.listeners.length > 0) && (index < 0))
					mInitValueList.push(variableDefinition);
			} else {// Has no init value, remove it from the init values list, if there
				if (index >= 0)
					mInitValueList.splice(index, 1);
			}
		}
		return variableDefinition;
	};

	/**
	 * Provides a function to be called when the view invokes an action.
	 * The view will call whatToDo(data) where the optional data is provided
	 * by the element that invokes the action.
	 * @method _setAction
	 * @param action the name of the view action (previously registered by the view)
	 * @param whatToDo the model function to be invoked
	 * @return the action definition object
	 */
	self._setAction = function(action, whatToDo) {
		// console.log("Setting action "+action+ "  to "+whatToDo);
		var actionDefinition = mActionsList[action];
		if (!actionDefinition)
			console.log("WARNING: view._setAction() - Action not found : " + action);
		else
			actionDefinition.action = whatToDo;
		return actionDefinition;
	};

	// ------------------------------------------------------------------------
	// This part is used automatically by the model at run time
	// ------------------------------------------------------------------------

	/**
	 * Whether changes should be reported
	 * @method _setReportNeeded
	 * @param needed true if it is needed, false otherwise
	 */
	self._setReportNeeded = function(needed) {
		mReportNeeded = needed;
	};

	/**
	 * Asks all (resetable) elements to reset themselves
	 * @method _reset
	 */
	self._reset = function() {
		for (var i = 0, n = mResetableList.length; i < n; i++)
			mResetableList[i].reset();
	};

	/**
	 * Initialize elements to init values
	 * @method _initValues
	 */
	self._initValues = function() {
		for (var i = 0, n = mInitValueList.length; i < n; i++) {
			var variableDefinition = mInitValueList[i];
			var value = variableDefinition.value;
			if (value) {
				var listeners = variableDefinition.listeners;
				for (var j = 0, m = listeners.length; j < m; j++)
					listeners[j](value);
			}
		}
	};

	/**
	 * Asks all (initializable) elements to initialize themselves
	 * @method _initialize
	 */
	self._initialize = function() {
		for (var i = 0, n = mInitializableList.length; i < n; i++)
			mInitializableList[i].initialize();
	};

	/**
	 * Updates the view by requesting from the model all variables registered
	 * with the _linkVariable() function or properties of values directly
	 * linked to model getters (using _view.element.linkProperty()).
	 * @method _update
	 */
	self._update = function() {
		var i, n;
		// console.log ("Calling view update for "+mUpdateList.length+ " variables and "+ mUnnamedUpdateList.length+" unknwons");
		for (i = 0, n = mUpdateList.length; i < n; i++) {
			var variableDefinition = mUpdateList[i];
			//    console.log ("Checking variable "+variableDefinition.name);
			var listeners = variableDefinition.listeners;
			var value = variableDefinition.getter();
			for (var j = 0, m = listeners.length; j < m; j++)
				listeners[j](value);
		}
		for (i = 0, n = mUnnamedUpdateList.length; i < n; i++) {
			var unnamedDefinition = mUnnamedUpdateList[i];
			//    console.log ("Checking unnamed "+unnamedDefinition.getter);
			unnamedDefinition.propertySetter(unnamedDefinition.getter());
		}
	};

	/**
	 * Same as _update BUT passing values only to elements
	 * which collect data (i.e. implement a dataCollected() function)
	 * @method _collectData
	 */
	self._collectData = function() {
		var i, n;
		for (i = 0, n = mUpdateList.length; i < n; i++) {
			var variableDefinition = mUpdateList[i];
			//    console.log ("Checking variable "+variableDefinition.name);
			var listeners = variableDefinition.collectors;
			var value = variableDefinition.getter();
			for (var j = 0, m = listeners.length; j < m; j++)
				listeners[j](value);
		}
		for (i = 0, n = mUnnamedUpdateList.length; i < n; i++) {
			var unnamedDefinition = mUnnamedUpdateList[i];
			//    console.log ("Checking unnamed "+unnamedDefinition.getter);
			if (unnamedDefinition.isCollector)
				unnamedDefinition.propertySetter(unnamedDefinition.getter());
		}
		for (i = 0, n = mCollectersList.length; i < n; i++)
			mCollectersList[i].dataCollected();
	};

	/**
	 * Asks all (renderer) elements to render themselves
	 * @method _render
	 */
	self._render = function() {
		for (var i = 0, n = mRenderList.length; i < n; i++)
			mRenderList[i].render();
	};

	/**
	 * Asks all (renderer) elements to render themselves
	 * @method _render
	 */
	self._touch = function() {
		for (var i = 0, n = mTouchList.length; i < n; i++)
			mTouchList[i].touch();
	};

	/**
	 * enable ePub (for internal purposes)
	 * @method _render
	 */
	self._enableEPub = function() {
	  //alert ("enabling ePubs : "+_isEPub);
      if (typeof _isEPub !== 'undefined' && _isEPub)
	  	for (var i = 0, n = mEnableEPubList.length; i < n; i++)
			mEnableEPubList[i].enableEPub();
	};

	/***
	 * Add a function to be called when the font size changes
	 * @method _addFontResizeListener
	 * @param listener a function to be called with possible the following parameters
	 * listener(iBase,iSize,iDelta);
	 * where:
	 *  iBase the base font size in pixels 
	 *  iSize the current font size in pixels 
	 *  iDelta the change in pixels from the last size 
	 */
    self._addFontResizeListener = function(listener) {
      mFontResizeListenerList.push(listener);
    };

	/**
	 * To be called when the font resized
	 * @method _fontResized
	 * @param iBase the base font size in pixels 
	 * @param iSize the current font size in pixels 
	 * @param iDelta the change in pixels from the last size 
	 */
	self._fontResized = function(iBase,iSize,iDelta) {
  	   for (var i = 0, n = mAdjustPositionList.length; i < n; i++)
		 mAdjustPositionList[i].adjustPosition();
	  if (iBase) { // if iBase is undefined, this may have been caused by a resize event
	    for (var j = 0, m = mFontResizeListenerList.length; j < m; j++) 
		  mFontResizeListenerList[j](iBase,iSize,iDelta);
	  }
	};

	/***
	 * Add a function to be called when the view size changes
	 * @method _addResizeListener
	 * @param listener a function to be called with possible the following parameters
	 * listener(data);
	 * where:
	 *  data.width is the new inner width 
	 *  data.height is the new inner height 
	 */
    self._addResizeListener = function(listener) {
      mResizeListenerList.push(listener);
    };
    
	/**
	 * To be called when the view resized
	 * @method _fontResized
	 */
	self._resized = function() {
	   var data = { width : window.innerWidth, height : window.innerHeight };
  	   for (var i = 0, n = mAdjustPositionList.length; i < n; i++)
		 mAdjustPositionList[i].adjustPosition();
	   for (var j = 0, m = mResizeListenerList.length; j < m; j++) 
		  mResizeListenerList[j](data);
	};

	/***
	 * Add a function to be called when the view orientation changes
	 * @method _addOrientationChangeListener
	 * @param listener a function to be called with possible the following parameters
	 * listener(width,height);
	 * where:
	 *  width the new inner width 
	 *  height the new inner height 
	 */
    self._addOrientationChangeListener = function(listener) {
      mOrientationChangeListenerList.push(listener);
    };
    
	/**
	 * To be called when the view resized
	 * @method _fontResized
	 * @param width the new inner width 
	 * @param height the new inner height 
	 */
	self._orientationChanged = function(width,height) {
	   var data = { width : window.innerWidth, height : window.innerHeight };
  	   for (var i = 0, n = mAdjustPositionList.length; i < n; i++)
		 mAdjustPositionList[i].adjustPosition();
	   for (var j = 0, m = mOrientationChangeListenerList.length; j < m; j++) 
		  mOrientationChangeListenerList[j](data);
	};

	/**
	 * Process the accumulated interactions
	 * @method _readInteractions
	 * @return true if there was any interaction to process
	 */
	self._readInteractions = function(runCount, runTime, isPlaying) {
		// read user interactions
		if (mInteractionList.length <= 0)
			return false;

		// note that an interaction can create a new interaction, so we use directly mInteractionList.length
		for (var i = 0; i < mInteractionList.length; i++) { // order of the interactions is important
			var interaction = mInteractionList[i];

   			if (self.isRegInteractions()) { // before updating model vars
   				var inter = interaction["interaction"];
   				inter["data"] = interaction.data;
	   			inter["timeStamp"] = Date.now();   					
   				if(!self.isShortRegInteractions()) {
	   				inter["runCount"] = runCount;
	   				inter["runTime"] = runTime;   				
	   				inter["isPlaying"] = isPlaying;
   				}
   				self.pushRegInteractions(inter, true);
   			}
   			if (interaction.data && interaction.data.info) interaction.what(interaction.data.info,interaction.data); 
   			else interaction.what(interaction.data);

   			if (self.isRegInteractions()) { // after updating model vars
   				self.pushRegInteractions(inter, false);
   			}
		}
		mInteractionList = [];
		return true;
	};

	/**
	 * Return the number of accumulated interactions
	 * @method _interactionsNumber
	 * @return length
	 */
	self._interactionsNumber = function() {
		return mInteractionList.length;
	};

	/***
	 * Add a function to be called when the window in which the model runs looses focus
	 * @method _addOnBlurAction
	 * @param listener the function to be called 
	 */
    self._addOnBlurAction = function(listener) {
    	mOnBlurList.push(listener);
    };

    /***
     * Run OnBlur Actions
     * @method _onBlur
     */
    self._onBlur = function() {
    	for (var j = 0, m = mOnBlurList.length; j < m; j++) mOnBlurList[j]();
    };

    /***
	 * Add a function to be called when the window in which the model runs regains focus
	 * @method _addOnFocusAction
	 * @param listener the function to be called 
	 */
    self._addOnFocusAction = function(listener) {
    	mOnFocusList.push(listener);
    };
    
    /***
     * Run OnFocus Actions
     * @method _onFocus
     */
    self._onFocus = function() {
    	for (var j = 0, m = mOnFocusList.length; j < m; j++) mOnFocusList[j]();
    };

    /***
     * Run OnFocus Actions
     * @method _setInnerHTML
     */
    self._setInnerHTML = function(htmlCode) {
        if (typeof mDummyContainer === "undefined") {
    		mDummyContainer = document.createElement('div');
    		document.body.appendChild(mDummyContainer);
    	} 
    	mDummyContainer.innerHTML = htmlCode;
    };
    
	// ------------------------------------------------------------------------
	// This part is used automatically by ControlElements at run time
	// ------------------------------------------------------------------------

	/**
	 * Reports interactions (typically to a model)
	 * @method _reportInteraction
	 */
	self._reportInteraction = function() {
		if (mReporter && mReportNeeded)
			mReporter();
	};

	/**
	 * Registers a listener for a given variable. Whenever the variable changes,
	 * the element's setter function will be invoked with the value of the function
	 * @method _addListener
	 * @param variable the name of the view variable (previously registered by the view)
	 * @param propertySetter the function to invoke to set the value to the element's property
	 * @param isCollector whether the element registering the listener is a data collector
	 * @returns the variable definition object if the variable is registered, null otherwise
	 */
	self._addListener = function(variable, propertySetter, isCollector) {
		var variableDefinition = mVariablesList[variable];
		if (!variableDefinition)
			console.log("WARNING: view._addListener() - Variable not found : " + variable);
		else {
			if (variableDefinition.listeners.indexOf(propertySetter) < 0) {// avoid repetitions
				variableDefinition.listeners.push(propertySetter);
				if (isCollector)
					variableDefinition.collectors.push(propertySetter);
			}
			if (variableDefinition.getter) {// Add it to the update list
				if (mUpdateList.indexOf(variableDefinition) < 0)
					mUpdateList.push(variableDefinition);
			}
			if (variableDefinition.value) {// Add it to the init values list
				if (mInitValueList.indexOf(variableDefinition) < 0)
					mInitValueList.push(variableDefinition);
			}
		}
		return variableDefinition;
	};

	/**
	 * Registers a listener for a no particular variable. At each update,
	 * the element's setter function will be invoked with the value of the function
	 * @method _addUnkownListener
	 * @param variable the name of the view variable (previously registered by the view)
	 * @param propertySetter the function to invoke to set the value to the element's property
	 * @param isCollector whether the element registering the listener is a data collector
	 * @returns true if the listener existed already for this property, false otherwise
	 */
	self._addUnkownListener = function(getter, propertySetter, isCollector) {
		var found = false;
		for (var i = 0, n = mUnnamedUpdateList.length; i < n; i++) {
			var unnamedDefinition = mUnnamedUpdateList[i];
			if (unnamedDefinition.propertySetter === propertySetter) {
				unnamedDefinition.getter = getter;
				found = true;
				break;
			}
		}
		if (!found) {
			mUnnamedUpdateList.push({
				getter : getter,
				propertySetter : propertySetter,
				isCollector : isCollector
			});
		}
		return found;
	};

	/**
	 * Returns a previously registered action.
	 * @method _getAction
	 * @param name the name of the view variable (previously registered by the view)
	 * @returns the setter of the model variable if the variable is registered and has one, null otherwise
	 */
	self._getAction = function(action) {
		var actionDefinition = mActionsList[action];
		if (!actionDefinition)
			console.log("WARNING: view._getAction() - Action not found : " + action);
		return actionDefinition;
	};

	/**
	 * Adds an interaction to the list of pending interactions
	 * @method _addInteraction
	 * @param action the function to invoke
	 * @param data optional data for the action
	 */
	self._addInteraction = function(action, data, interaction) {		
	    if (!mReporter) { // The view is alone (there is no model)
	    	action(data); 
	    	return; 
	    } 
		if (mReportNeeded)
			mInteractionList.push({
				what : action,
				data : data,
				interaction: interaction
			});
	};

	//---------------------------------
	// utils
	//---------------------------------

	/**
	 * Formats a number
	 * @method _format
	 * @param value the value to format
	 * @param format The format to use
	 */
	self._format = function(value, format) {
		var index = format.indexOf('.');
		var digits = 0;
		if (index >= 0)
			digits = Number(format.length - index - 1);
		return parseFloat(value).toFixed(digits);
	};
	
	/**
	 * Print message
	 */
	self.print = function(str) {
		// find textArea
		var ta = document.getElementsByTagName("textarea");
		if(ta.length) 
			ta[0].innerHTML = str;	
		else 
			console.log(str);			
	};
	
    /**
     * Sets the visibility of the whole view
     */
  	self._setVisible = function(visibility) { 
      if (visibility) mTopLevelElement.style.display = "inherit";
      else mTopLevelElement.style.display = "none";
//  		if (visibility) document.getElementById(container).style.display = "inherit";
//  		else document.getElementById(container).style.display = "none";
  	}	
	
    /**
     * Switch the visibility of the whole view
     */
  	self._switchVisibility = function() {
      self._setVisible (mTopLevelElement.style.display == "none"); 
//  		self._setVisible (document.getElementById(container).style.display == "none"); 
  	}	
	
	/**
	 * Serialize
	 */
	self.serialize = function() {	
		var encoded = {};
		for(key in self) {
			if(self[key].serialize) { // an element
				encoded[key] = self[key].serialize();
			}
		}
		return encoded;		
	}	
	
	/**
	 * Unserialize
	 */	
	self.unserialize = function(decoded) {		
		for(key in decoded) {
			if(self[key] && self[key].unserialize) { // element in view
				self[key].unserialize(decoded[key]);
			} else {
				console.log("Impossible unserialize: " + key);
			}
		}
	}

	/**
	 * Register interactions 
	 */
	self.registerInteractions = function(websocket, callback) {
		mRegInteractions = [];
		mWSRegInteractions = websocket;
		mCBRegIngeractions = callback;
		mIsRegInteractions = true;
		mIsShortRegInteractions = false;
	}

	/**
	 * Unregister action interactions
	 */
	self.unregisterInteractions = function() {
		mIsRegInteractions = false;	
	}

	/**
	 * Get action interactions 
	 */
	self.getRegInteractions = function(empty) {
		var retRegInteractions = mRegInteractions.slice();
		if (empty) mRegInteractions = [];
		return retRegInteractions;
	}

	self.clearRegInteractions = function() {
		mRegInteractions = [];
	}
	
	self.isRegInteractions = function() {
		return mIsRegInteractions;
	}	

	self.setShortRegInteractions = function(shortreg) {
		mIsShortRegInteractions = shortreg;		
	}

	self.isShortRegInteractions = function() {
		return mIsShortRegInteractions;		
	}
	
	self.pushRegInteractions = function(action, before) {
		if (before) {
			if(mWSRegInteractions) 
				mWSRegInteractions.send(JSON.stringify(action)); 
			else if (mCBRegIngeractions)
				mCBRegIngeractions(action, before);							
			else
				mRegInteractions.push(action);			
		} else {
			if (mCBRegIngeractions)
				mCBRegIngeractions(action, before);							
		}
	}
		
	//---------------------------------
	// final initialization
	//---------------------------------

	if (typeof container == "string") {
	  mTopLevelElement = self[container] = document.getElementById(container);
	}
	else mTopLevelElement = container;

	return self;
};

//------------------------------------------------------------------------
//ControlElement: A base class to change elements via properties
//------------------------------------------------------------------------

/**
 * promoteToControlElement: This function enriches any object with property setters and getters
 * that allow it to set its properties/actions via setProperty/setAction methods and also to 'talk' to
 * a View using the linkVariable() method.
 * The object should implement a function called registerProperties(controller) that can
 * call registerProperty() and registerAction() and can then use
 * - controller.invokeAction() and
 * - controller.propertiesChanged()
 * whenever appropriated after user interaction.
 * @method promoteToControlElement
 * @param mElement the object to add properties to
 * @param mView the view with which to communicate
 * @param mName an optional name for the element
 */
EJSS_CORE.promoteToControlElement = function(mElement, mView, mName) {
	// --- Configuration variables
	mName = mName || 'unnamed';
	var mDataObject = null;
	var mProperties = {};
	var mActions = {};

	// ------------------------------------------------------------------------
	// Common properties
	// ------------------------------------------------------------------------

	mElement.getView = function() { return mView; }
	
	/**
	 * Gives a name to the element.
	 * Naming an element is optional, but the element may use its name to identify itself.
	 * @method setName
	 * @param name String
	 */
	mElement.setName = function(name) {
		mName = name;
	};

	/**
	 * Return the name of the element
	 * @method getName
	 */
	mElement.getName = function() {
		return mName;
	};

	mElement.getResourcePath = function(filename) {
		return mView._getResourcePath(filename);
	};

	/**
	 * A place holder for data objects
	 */
	mElement.setDataObject = function(data) {
	  mDataObject = data;
	};
	
  mElement.getDataObject = function(data) {
    return mDataObject;
  };

  // ------------------------------------------------------------------------
	// This part is used by the element at definition time
	// ------------------------------------------------------------------------

	/**
	 * Registers (defines) a property for the element (but gives it no value)
	 * Example: registerProperty("x",setX,getX);
	 * @method registerProperty
	 * @param property the name of the property
	 * @param setter the setter function to set the property value
	 * @param getter the getter function to get the property value (needed only for interaction
	 * with the element)
	 * @return return the element
	 */
	mElement.registerProperty = function(property, setter, getter) {
		mProperties[property] = {
			setter : setter,
			getter : getter,
			variable : null,
			variableSetter : null
		};
		return mElement;
	};

	/**
	 * Registers (defines) an action for the element (but assigns no real action)
	 * Example: registerAction("onPress"); or registerAction("onPress", self.getInteractedElement);
	 * @method registerAction
	 * @param action the name of the action
	 * @param helperFunction a function that will provide additional data about the action
	 * @param processFunction a function that will provide additional process about the action
	 * @param actionFunction a function that will provide default action
	 * @return return the element
	 */
	mElement.registerAction = function(action, helperFunction, processFunction, actionFunction) {
		mActions[action] = {
			action : null,
			helper : helperFunction,
			process : processFunction
		};
		
		// default action
		if(actionFunction) mElement.setAction(action, actionFunction);
		 
		return mElement;
	};

	// ------------------------------------------------------------------------
	// This part is used by the user when creating the view
	// ------------------------------------------------------------------------

	/**
	 * Sets the value of the property of the element to a constant value.
	 * Does it only once, now (different to linking).
	 * Example: setProperty("lineColor","red");
	 * @method setProperty
	 * @param property the name of the property
	 * @param value the value for the property
	 * @return the element, thus allowing for cascading calls
	 */
	mElement.setProperty = function(property, value) {
		var actionDefinition = mActions[property];
		if (actionDefinition) {
			return mElement.setAction(property, value);
		}
		var propertyDefinition = mProperties[property];
		if (!propertyDefinition) {
			console.log("WARNING: ControlElement setProperty() - Property not registered : " + property + " for element : " + mElement.getName());
		} else
			propertyDefinition.setter(value);
		return mElement;
	};

	/**
	 * Sets the values of a set property of the element to the constant values.
	 * Does it only once, now (different to linking).
	 * Example: setProperties({"lineColor":"red", "background":"black"});
	 * @method setProperties
	 * @param properties the names of the properties and their values
	 * @return return the element
	 */
	mElement.setProperties = function(properties) {
		for (var key in properties)
		mElement.setProperty(key, properties[key]);
		return mElement;
	};

	/**
	 * Links the given property to a variable.
	 * Future changes will be communicated:
	 *  - from the model to the view element at each call of view._update()
	 *  - from the model to the view element at each call of view._collectData(), only if the element defines the dataCollected property
	 *  - from the element to the model after interaction (if the getter is not null or undefined)
	 * Example:
	 * - linkProperty("x","ballX"); where "ballX" is a previously registers view variable
	 * - linkProperty("x",function() { return x; }, function(_v) { x = _v; }); where x is a model variable
	 * @method linkProperty
	 * @param property the name of the property
	 * @param variableOrGetter if a string, the name of the variable previously registered by the view,
	 * otherwise a model getter function to obtain the desired value for the property
	 * @param setter the model setter function (if a getter function was given). If variableOrGetter was a string and getter==="InputOnly" the link is only for input 
	 * @return the element, thus allowing for cascading calls
	 */
	mElement.linkProperty = function(property, variableOrGetter, setter) {
		var propertyDefinition = mProperties[property];
		if (!propertyDefinition) {
			console.log("WARNING: ControlElement linkProperty() - Element property not registered : " + property + " for element : " + mElement.getName());
			return mElement;
		}
		if ( typeof variableOrGetter === "string") {
			var viewVariable = mView._addListener(variableOrGetter, propertyDefinition.setter, mElement.dataCollected);
			if (!viewVariable) {
				console.log("WARNING: ControlElement linkProperty() - element : " + mElement.getName() + " - property :" + property + " - View variable not registered : " + variableOrGetter);
				return mElement;
			}
			if (setter==="InputOnly") {
			  //console.log ("ControlElement linkProperty() - element : " + mElement.getName() + " - property :" + property + " - is INPUT_ONLY");
			}
			else propertyDefinition.variable = viewVariable;
			propertyDefinition.variableSetter = null;
		} else {
			mView._addUnkownListener(variableOrGetter, propertyDefinition.setter, mElement.dataCollected);
			propertyDefinition.variableSetter = setter;
			propertyDefinition.variable = null;
		}
		return mElement;
	};

	/**
	 * Links the given property to a variable.
	 * Example:
	 * - linkProperties({"x":"ballX","y":"ballY"}); where "ballX" and "ballY" are previously registers view variables
	 * @method linkProperties
	 * @param properties the names of the properties and their variables
	 * @return return the element
	 */
	mElement.linkProperties = function(properties) {
		for (var key in properties)
		mElement.linkProperty(key, properties[key]);
		return mElement;
	};

	/**
	 * Sets the given action to a function
	 * Interactions with that action name will trigger the given function (one direction only)
	 * Examples:
	 * - setAction("onPress","start"); Where "start" is a previously registered view action
	 * - setAction("onPress",_play); where _play is a function defined by the model
	 * @method setAction
	 * @param name the name of the action
	 * @param action if a string, the name of a previously registered view action,
	 * otherwise it must be the function to call when the action is invoked. Setting to null disables the action
	 * @return the element, thus allowing for cascading calls
	 */
	mElement.setAction = function(name, action) {
		var actionDefinition = mActions[name];
		if (!actionDefinition) {
			console.log("WARNING: ControlElement setAction() - action not registered : " + name + " for element : " + mElement.getName);
			return mElement;
		}
		if ( typeof action === "string") {
			var viewAction = mView._getAction(action);
			if (!viewAction) {
				console.log("WARNING: ControlElement setAction() - element : " + mElement.getName() + " - View action not registered : " + action);
				return mElement;
			}
			actionDefinition.action = viewAction;
		} else {// It must be a function
			actionDefinition.action = {
				action : action
			};
		}
		return mElement;
	};

	/**
	 * Sets the given action to a function
	 * Interactions with that action name will trigger the given function (one direction only)
	 * Examples:
	 * - setAction("onPress","start"); Where "start" is a previously registered view action
	 * - setAction("onPress",_play); where _play is a function defined by the model
	 * @method setActions
	 * @param actions the names of the actions and their view actions
	 * @return return the element
	 */
	mElement.setActions = function(actions) {
		for (var key in actions)
		mElement.setAction(key, actions[key]);
		return mElement;
	};

	// ------------------------------------------------------------------------
	// This part is used by the elements under user interaction
	// ------------------------------------------------------------------------

	/**
	 * Invoke an action
	 * @method invokeAction
	 * @param name the name of the action
	 * @return return the element
	 */
	mElement.invokeAction = function(name) {
		var ia = {};
		if (mView.isRegInteractions()) {
			if(!mView.isShortRegInteractions())
				ia = {"action":name, "element":mElement.getName(), "timeStamp":Date.now()};
			else
				ia = {"action":name, "element":mElement.getName()};			
		}

		var actionDefinition = mActions[name];
		if (actionDefinition) {
			if (actionDefinition.process) {
				mView._addInteraction(actionDefinition.process, null, ia);
			}
			if (actionDefinition.action) {
  				var data = actionDefinition.helper ? actionDefinition.helper() : null;
				mView._addInteraction(actionDefinition.action.action, data, ia);
			}
		}
		return mElement;
	};

	mElement.invokeImmediateAction = function(name) {
		var actionDefinition = mActions[name];
		if (actionDefinition) {
			if (actionDefinition.process) {
				actionDefinition.process(null);
			}
			if (actionDefinition.action) {
  				var data = actionDefinition.helper ? actionDefinition.helper() : null;
				actionDefinition.action.action(data);
			}
		}
		return mElement;
	};

	/**
	 * Reports a change in all given properties caused by interaction with the element
	 * @method propertiesChanged
	 * @return true if any of the properties was linked to a variable
	 */
	mElement.propertiesChanged = function(/* ... properties */) {
		for (var i = 0, n = arguments.length; i < n; i++)
			mElement.propertyChanged(arguments[i]);
		return mElement;
	};

	mElement.propertyChanged = function(property, data) {
		var ia = {};
		if (mView.isRegInteractions()) {
			if(!mView.isShortRegInteractions())
				ia = {"property":property, "element":mElement.getName(), "timeStamp":Date.now()}
			else		
				ia = {"property":property, "element":mElement.getName()}
		}
		
		var propertyDefinition = mProperties[property];
		if (propertyDefinition) {// Linked to a function
			var setter = propertyDefinition.variableSetter; 
			if (!setter) {// It may be linked to a view variable
				if (propertyDefinition.variable)
					setter = propertyDefinition.variable.setter;
			}
			if (!setter) {// It may be linked to its own setter
				if (propertyDefinition.setter)
					setter = propertyDefinition.setter;
			}
			if (setter) {// Linked to anything
				// Obtain the property value now, in case it changes later
				// but delay setting the variable until requested
				var value = (typeof data !== "undefined")?data:propertyDefinition.getter();
				mView._addInteraction(setter, value, ia);					
			}
			// else : don't know what to do with this property
		}
		return mElement;
	};

	mElement.immediatePropertyChanged = function(property, data) {
		var ia = {};
		var propertyDefinition = mProperties[property];
		if (propertyDefinition) {// Linked to a function
			var setter = propertyDefinition.variableSetter; 
			if (!setter) {// It may be linked to a view variable
				if (propertyDefinition.variable)
					setter = propertyDefinition.variable.setter;
			}
			if (!setter) {// It may be linked to its own setter
				if (propertyDefinition.setter)
					setter = propertyDefinition.setter;
			}
			if (setter) {// Linked to anything
				// Obtain the property value now, in case it changes later
				// but delay setting the variable until requested
				var value = (typeof data !== "undefined")?data:propertyDefinition.getter();
				setter(value);					
			}
			// else : don't know what to do with this property
		}
		return mElement;
	};
	/**
	 * Report accumulated interactions including the interactiones derived by
	 * the actions and properties changed specified in the params
	 * @method reportInteractions
	 * @param actions array with the names of the actions
	 * @param properties array with the names of the properties
	 * @return return the element
	 */
	mElement.reportInteractions = function(actions, properties) {
		var i, n;
		// actions
		if (actions)
			for (i = 0, n = actions.length; i < n; i++)
				mElement.invokeAction(actions[i]);
		// properties
		if (properties)
			for (i = 0, n = properties.length; i < n; i++)
				mElement.propertiesChanged(properties[i]);

		if (mView._interactionsNumber() > 0)
			mView._reportInteraction();
		return mElement;
	};

	/**
	 * Sets the value of a property of the element.
	 * Example: getProperty("lineColor");
	 * @method getProperty
	 * @param property the name of the property
	 * @return value the value for the property, undefined if not found
	 */
	mElement.getProperty = function(property, value) {
		var propertyDefinition = mProperties[property];
		if (propertyDefinition && propertyDefinition.getter)
			return propertyDefinition.getter();
		return undefined;
	};

	/**
	 * Gets properties
	 * @method getProperties
	 * @return properties
	 */
	mElement.getProperties = function() {
		return mProperties;
	};
			
	//---------------------------------
	// final initialization
	//---------------------------------

	mElement.registerProperty("name", mElement.setName, mElement.getName);

	if (mElement.registerProperties)
		mElement.registerProperties(mElement);

	return mElement;
};
