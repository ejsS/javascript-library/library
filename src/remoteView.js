/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia, Felix J Garcia-Clemente
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

/**
 * Framework for view and control elements
 * @module core
 */

var EJSS_CORE = EJSS_CORE || {};

/**
 * A view which can connect to a remote model
 * @class RemoteView
 * @constructor
 */

/**
 * RemoteView: A subclass of View that connects to a remote model
 * Make you classes 'inherit' from this one by
 * - declaring:
 *   var _view = EJSS_CORE.createRemoteView(container);
 * - redeclaring (extending) the _reset() function, and
 * - returning _view.
 * 
 * createRemoteView("container") supposes:
 *  - mLocalModelPort is undefined, mLocalModelPort will be 8800 
 * 	- mModelsURL is undefined, mModelsURL will be location.protocol + "\\" + location.host + "/ejsS_library/models"
 *  - mModel is undefined, mModel will be the url substring between mModelsURL and _view, for example 
 *       http://155.54.1.1/ejsS_library/models/examples/mytest/_view/mytest.html -> mModel = "examples/mytest"
 * 
 * examples:
 * - createRemoteView("container")
 *       if this simulation is local, then remote view for an existing WS server (port 8800),
 *       else remote view for a new WS server for model located in the same location for this simulation
 * 
 * - createRemoteView("container", 8889)
 *       if this simulation is local, then remote view for an existing WS server (port 8889), 
 *       else remote view for a new WS server for model located in the location for this simulation
 * 
 *  - createRemoteView("container", 0, "http://millennium.inf.um.es/ejsS_library/models", "/examples/test1")
 *       if this simulation is local, then remote view for an existing WS server (port 8800),
 *       else remote view for a new WS server for model located in "http://millennium.inf.um.es/ejsS_library/models/examples/test1"
 * 
 * Note if history.state.mServerModelPort (see linking.php) exists, 
 *       then the simulation connects to a WS server running in port mServerModelPort (see broker.php)
 * 
 * @param mContainer view container
 * @param mLocalModelPort port for local WS server
 * @param mModelsURL root path for models (where you can find broker.php and linking.php)
 * @param mModel relative path for one model (mModelsURL+mModel is where you can find _view and _jar for the model)
 * 
 * @method createRemoteView
 */
EJSS_CORE.createRemoteView = function(mContainer, mLocalModelPort, mModelsURL, mModel) {  
		var self = EJSS_CORE.createView(mContainer);
	var mNumOpenTries = 10;
	var mOpenTimeout = 500;

	var mWebsocket;
	var mMessages = {};
	var mIsOpen = false;
	var mOpenTry = 0;  
	var mCurrentData = {};
	var mVariableList = [];
	var mActionList = [];
	var mPropertyChangesList = []; // list of property changes to report
	var mActionsInvokedList = []; // list of actions invoked to send
	var mUpdating = false;
	var mDelayedUpdate = false;
	var mVarsMetadata = [];
	var mMethodsMetadata = [];
	var mLinkingURL = "";
	var mShowLinkingButtons = true;
	var mServerModelPort = 0;
	var wrap;
	var mUri = "";	// uri to connect
	var countTo = 0;
	// ------------------------------------------------------------------------
	// Communication functions
	// ------------------------------------------------------------------------

	self.getClass = function() {
	return "ConnectionWS";
	};
	//JSV_CHANGES
	function sendMessage(command, data) {
	if (!mIsOpen) return;
	// console.log ("Sending message "+command+" with data:"+data);
	if (data!==undefined){
		//console.log (" <-- Sending message "+command+" with data: "+ JSON.stringify(data));
		mWebsocket.send(command + JSON.stringify(data));
	}else{
		//console.log (" <-- Sending message " + command + " with NO data: ");
		mWebsocket.send(command);
	}
	// reset socket : is this needed?
	var f = mWebsocket.onmessage;
	mWebsocket.onmessage = null;
	mWebsocket.onmessage = f;
	};

	//JSV_CHANGES
	function processInput(input) {
		console.log (" --> Processing input: "+input);
		var message;
		var usefulValues = {};
		if(input.charAt(0)=="{") {
			//This implies that the model is using pure JSON to send messages, then we are using the SDS (Smart Device Specification)
			message = JSON.parse(input);
			var method = "";
			if(message.hasOwnProperty("method")){
				method = message.method;
				if(method == "reset")	self._reset(); 
				else if(method == "initialize")	self._initialize(); 
				else if(wrap != undefined){
					usefulValues = wrap.extract(input);
					//console.log("Values extracted: "+ JSON.stringify(usefulValues));
					mCurrentData = usefulValues;
					coallesce(self._update);
				}
			}
			if(message.hasOwnProperty("apiVersion")){
				console.log("Metadata Obtained");
				wrap = new wrapper(mWebsocket,input);
				for(var variable in mVariableList){
					wrap.get(mVariableList[variable],true);
				}
			}
		}else{
			processNotSmartInput(input);
		}
		//console.log("mCurrentData : " + JSON.stringify(mCurrentData));
	}; 

   function processNotSmartInput(input) {
		var message;
		switch (input.charAt(0)) {
			case "R" : // reset 
				self._reset(); 
				break;
			case "I" : // initialize 
				self._initialize(); 
				break;
			case "U" : // update 
				mCurrentData = JSON.parse(input.substring(1)); 
				coallesce(self._update);
				break;
			case "C" : // collect
				mCurrentData = JSON.parse(input.substring(1)); 
				coallesce(self._collectData);
				break;
			case "M" : // View method
				message = JSON.parse(input.substring(1));
				if (!self[message.method]) console.log ("View function: <"+message.method+ "> with data: <"+message.data+"> ignored. View function does not exist.");
				else self[message.method](message.data); 

				break;
			case "P" : // Set the property of an element
				message = JSON.parse(input.substring(1));
				if (self[message.element]) self[message.element].setProperty(message.property,message.value);
				else console.log("setProperty<"+message.property+"> with value: <"+message.value+"> to element <"+message.element+"> ignored. Element does not exist!"); 
				break;
			case "E" : // call a method element with a single parameter
				message = JSON.parse(input.substring(1));
				if (self[message.element]) self[message.element][message.method](message.data);
				else console.log("Message <"+message.method+"> to element <"+message.element+"> ignored. Element does not exist!"); 
				break;
			case "F" : // call a method element with a map object (i.e a Javascript object) as parameter
				message = JSON.parse(input.substring(1));
				if (self[message.element]) self[message.element][message.method](message.data); 
				else console.log("Message with object <"+message.method+"> to element <"+message.element+"> ignored. Element does not exist!"); 
				break;
			case "D" : //calls a method to retrieve metadata information
				message = JSON.parse(input.substring(1));
				mVarsMetadata = message.variables;
				mMethodsMetadata = message.methods;
				break; 
			default:
				console.log("Not known message: Ignoring")
				break;
		}
   };
   
   function coallesce(viewFunction) {
     // if (mUpdating) {
       // mDelayedUpdate = true;
       // return;
     // }
     // mUpdating = true;
     // var f = mWebsocket.onmessage;
     // mWebsocket.onmessage = function(){ console.log("esperando ...");};
     viewFunction();
     self._render();
     // mUpdating = false;
     //sendMessage("O"); // OK : done processing Update of Collect
     

// var date = new Date();
// var curDate = null;
// 
// do { curDate = new Date(); } 
// while(curDate-date < 1000);
 
     // mWebsocket.onmessage = f;
    // mWebsocket.onmessage = function(message) {
        // self._processInput(message.data);
      // };
     // if (mDelayedUpdate) {
       // mDelayedUpdate = false;
       // _updateAndRender();
     // }
   };
   
  // ----------------------------------------------------
  // public methods
  // ----------------------------------------------------



  /**
   * Connect
   */
  self._connectToServer = function() {
    try {
      mWebsocket = new WebSocket(mUri);
      console.log('Connecting... (readyState ' + mWebsocket.readyState + ')');
      mWebsocket.onopen = function(message) {
		//console.log("This will be the call to the metadata method");
        mIsOpen = true;
        mOpenTry = 0;
        console.log("Openhd Event: " + message.type + " - Message: " + message.data);
        //sendMessage("D");
		//sendMessage("A", { variables : mVariableList, actions : mActionList });
		sendMessage("", { method : "getMetadata" });
      };
      mWebsocket.onclose = function(message) {
      	if (mOpenTry < mNumOpenTries) {
      		window.setTimeout(function(){ self._connectToServer(); }, mOpenTimeout); // try to connect again in 500 mseg
      		mOpenTry++;       	
        }
        mIsOpen = false;
        console.log("Closehd Event: " + message.type + " - Message: " + message.data + " - Reopen: " + mOpenTry);
      };
      mWebsocket.onerror = function(message) {
        mIsOpen = false;
        console.log("Errorhd Event: " + message.type + " - Message: " + message.data);
      };
      mWebsocket.onmessage = function(message) {
        processInput(message.data);
      };
    }
    catch(exception) {
      console.log(exception);
    }
    self._update();
    self._render();
  };
  
  self._getValue = function(variable) {
    if (mIsOpen) return mCurrentData[variable];
    return self._getInitialValue(variable); 
  };
  
  // ------------------------------------------------------------------------
  // This part is used automatically by ControlElements at run time
  // ------------------------------------------------------------------------

  super_registerVariable = self._registerVariable;
  super_registerAction = self._registerAction;
  super_startUp = self._startUp;
  
  self._startUp = function() {
    super_startUp();
    self._connectToServer();
  };
  
  self._registerVariable = function(variable, initialValue, inputOnly) {
    super_registerVariable(variable, initialValue);
    if (inputOnly) {
      self._linkVariable(variable,
        function() { return self._getValue(variable); },
        null, // no setter
        initialValue);
    } 
    else {
      mVariableList.push(variable);
      self._linkVariable(variable,
        function() { return self._getValue(variable); },
        function(value) { // setter
          // console.log("Adding property change:" + variable); 
          mPropertyChangesList.push( { name: variable, value: value } );
        }, initialValue);
    }
  };
  
  self._registerAction = function(action) {
    super_registerAction(action);
    mActionList.push(action);
    self._setAction(action, function(data) {
      // console.log("Adding action:" + action); 
      mActionsInvokedList.push( { name: action, argument: data } );
    });
  };
    
  self._reportInteraction = function () {
    self._readInteractions();
    //sendMessage("I", { properties : mPropertyChangesList, actions : mActionsInvokedList } );
    var names = {};
	var values = {};
	if(mPropertyChangesList.length>0){
		for(var propChange in mPropertyChangesList){
			if(mPropertyChangesList[propChange].hasOwnProperty("name"))
			names[propChange] = mPropertyChangesList[propChange].name;
			values[propChange] = mPropertyChangesList[propChange].value;
		}
		wrap.set(names, values, "");
	}
	if(mActionsInvokedList.length>0){
		for(var actInvoke in mActionsInvokedList){
			//if(mActionsInvokedList[argument].hasOwnProperty("argument"))
			wrap.callAction(mActionsInvokedList[actInvoke].name, [mActionsInvokedList[actInvoke].argument]);
			//else 
			//	wrap.set(mActionsInvokedList[actInvoke].name, undefined);
		}
	}
	mPropertyChangesList = [];
    mActionsInvokedList = [];
  };

   /*
    * Overrides its parent
   */
	self._interactionsNumber = function() {
		return mActionsInvokedList.length;
	};
	
  // ------------------------------------------------------------------------
  // Linking other server
  // ------------------------------------------------------------------------

  /**
   * Get linking url
   */
  self.getLinkingURL = function() {
  	return mLinkingURL; 
  };
  
  /**
   * Show linking buttons
   */  
  self.createLinkingButtons = function() {
	  self._addElement(EJSS_INTERFACE.button,"_linkingButton_")
	  .setAction("OnClick", function() { var str = self.getLinkingURL(); window.alert(str.toString());})
	  .setProperties({"Text":"@", "FontSize":"8px"});    
	
	  self._addElement(EJSS_INTERFACE.button,"_qrButton_")
	  .setAction("OnClick", function() {
	  		var str = encodeURIComponent(self.getLinkingURL());
	  		var url = "https://chart.googleapis.com/chart?cht=qr&chl=" + str + "&chs=400"; 
	  		window.open( url, '_blank');
	  	})
	  .setProperties({"Text":"QR", "FontSize":"8px"});
  };
  
  /**
   * Hidden linking buttons
   */  
  self.hiddenLinkingButtons = function() {
  	_view._linkingButton_.setVisibility("hidden");
  	_view._qrButton_.setVisibility("hidden");
  };

  /**
   * Show linking buttons
   */  
  self.showLinkingButtons = function() {
  	_view._linkingButton_.setVisibility("visible");
  	_view._qrButton_.setVisibility("visible");
  };
  
  //---------------------------------
  // final initialization
  //---------------------------------
         
   if (window.location.protocol.indexOf("file") != -1) { // is local
   		if (typeof mLocalModelPort == "undefined" || mLocalModelPort === null || mLocalModelPort == 0) { 
   			mLocalModelPort = 8800;
   		}

        // WS server uri
	    mUri =  "ws://localhost:" + mLocalModelPort;

		// not show linking buttons
		mShowLinkingButtons = false;
   } else {
	   if (history.state) {  // get server port
		 mServerModelPort = history.state.mServerModelPort;	// history sets port
		 mShowLinkingButtons = false;					    // not show linking buttons
	   }    

	   // get models url 
	   if (typeof mModelsURL == "undefined" || mModelsURL === null || mModelsURL == "") { // undefined models url
	  	 var host = window.location.host;
	  	 var proto = window.location.protocol;
	  	 mModelsURL = proto + "//" + host + "/ejsS_library/models";  // default models url  
	   }
	      
	   // get model path 
	   if (typeof mModel == "undefined" || mModel === null || mModel == "") { // undefined model path
	  	 var index = window.location.href.lastIndexOf("/_view"); // index for /_view
	  	 mModel = window.location.href.slice(mModelsURL.length, index);    
	   }

	  // run server model
	  var xmlhttp = new XMLHttpRequest();  
	  if(mServerModelPort > 0)	{ // known port
	  	xmlhttp.open("GET", mModelsURL + "/broker.php?model=" + mModel + "&port=" + mServerModelPort, true); 	  	
	  } else { // new connection (and new port)
	  	xmlhttp.open("GET", mModelsURL + "/broker.php?model=" + mModel, true);
	  }
	  
	  xmlhttp.onreadystatechange = function() {
		  // get response
		  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			  var response = JSON.parse(xmlhttp.responseText);
		      // WS server uri
			  mUri = response.webserver;
			  // linking url
			  mLinkingURL =  mModelsURL + "/linking.php?model=" + mModel + "&port=" + response.port; 
			  // console.log("Server response: " + xmlhttp.responseText);
			  
		      if (mShowLinkingButtons) self.createLinkingButtons();
		      
		      self._startUp();  
		  }
		  else 
		    console.log("WARNING: server no response!");
	  };
	  
	  xmlhttp.send();
   }
               
  return self;
};
