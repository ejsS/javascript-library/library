/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * Basic Element 2D 
 * @class EJSS_DRAWING2D.Element
 */
EJSS_DRAWING2D.Element = {
	CENTER   : 0,
	NORTH    : 1,
	SOUTH    : 2,
	EAST     : 3,
	WEST     : 4,
	NORTH_EAST  :  5,
	NORTH_WEST  :  6,
	SOUTH_EAST  :  7,
	SOUTH_WEST  :  8,
        
    /**
     * Registers properties in a ControlElement
     */
    registerProperties : function(element,controller) {
      var TARGET_POSITION = EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION;
      var TARGET_SIZE = EJSS_DRAWING2D.PanelInteraction.TARGET_SIZE;
      var style = element.getStyle();
                        
      element.setController(controller);

	 /*** 
	  * Parent of the element
	  * @property Parent 
	  * @type Panel|Group
	  */  
      controller.registerProperty("Parent",element.setParent,element.getParent); 

	 /*** 
	  * Position in X
	  * @property X 
	  * @type double
	  * @default 0
	  */  
      controller.registerProperty("X",element.setX,element.getX);
      
	 /*** 
	  * Position in Y
	  * @property Y 
	  * @type double
	  * @default 0
	  */        
      controller.registerProperty("Y",element.setY,element.getY);
      
	 /*** 
	  * Coordinates X and Y
	  * @property Position 
	  * @type double[2]
	  * @default [0,0]
	  */        
      controller.registerProperty("Position",element.setPosition,element.getPosition);
      
	 /*** 
	  * Whether the position is in pixels
	  * @property PixelPosition 
	  * @type boolean
	  * @default false
	  */              
      controller.registerProperty("PixelPosition",element.setPixelPosition,element.getPixelPosition);
      
	 /*** 
	  * Diameter, i.e. the value of the width and the height 
	  * @property Diameter 
	  * @type double
	  */                    
      controller.registerProperty("Diameter",function(diameter) {
      	element.setSize([diameter,diameter]);
      });
      
	 /*** 
	  * Radius, i.e. the half of the diameter 
	  * @property Radius 
	  * @type double
	  */                    
      controller.registerProperty("Radius",function(radius) {
      	element.setSize([radius*2,radius*2]);
      });
      
	 /*** 
	  * Size along the X axis
	  * @property SizeX 
	  * @type double
	  * @default 1
	  */                          
      controller.registerProperty("SizeX",element.setSizeX,element.getSizeX);

	 /*** 
	  * Size along the Y axis
	  * @property SizeY 
	  * @type double
	  * @default 1
	  */                          
      controller.registerProperty("SizeY",element.setSizeY,element.getSizeY);

	 /*** 
	  * Size along the X and Y axes 
	  * @property Size 
	  * @type double[2]
	  * @default [1,1]
	  */                          
      controller.registerProperty("Size",element.setSize,element.getSize);
            
	 /*** 
	  * Whether the size is in pixels
	  * @property PixelSize 
	  * @type boolean
	  * @default false
	  */                          
      controller.registerProperty("PixelSize",element.setPixelSize,element.getPixelSize);
	  
	 /*** 
	  * Position of the bounding box of the element
	  * @property Bounds 
	  * @type Object{left,rigth,top,bottom}
	  */                          
      controller.registerProperty("Bounds",element.setBounds,element.getBounds);

	 /*** 
	  * Position of the coordinates X and Y relative to the element 
	  * @property RelativePosition 
	  * @type string|int
	  * @values "CENTER":0,"NORTH":1,"SOUTH":2,"EAST":3,"WEST":4,"NORTH_EAST":5,"NORTH_WEST":6,
	  * "SOUTH_EAST":7,"SOUTH_WEST":8 
	  * @default "CENTER"
	  */                          
      controller.registerProperty("RelativePosition",element.setRelativePosition,element.getRelativePosition);

	 /*** 
	  * Whether the element is visible
	  * @property Visibility 
	  * @type boolean
	  * @default true
	  */                          
      controller.registerProperty("Visibility",element.setVisible,element.isVisible);
            
	 /*** 
	  * Measurability of the element
	  * @property Measured 
	  * @type boolean
	  * @default true
	  */                          
      controller.registerProperty("Measured",element.setMeasured,element.isMeasured);

	 /*** 
	  * Internal transformation of the element
	  * @property Transformation 
	  * @type int|float[6]|string
	  * @values int:angle in radians to rotate the element with origin its position, 
	  * float[9]: transformation matrix - [scaleX, skewY, skewX, scaleY, translateX, translateY]
	  * @see http://www.w3.org/TR/SVG-Transforms/
	  * @default 0
	  */                          
      controller.registerProperty("Transformation",element.setTransformation);

	 /*** 
	  * Rotation of the element
	  * @property Rotate 
	  * @type int
	  * @values int:angle in radians to rotate the element with origin its position 
	  * @see http://www.w3.org/TR/SVG-Transforms/
	  * @default 0
	  */                          
      controller.registerProperty("Rotate",element.setRotate);

	 /*** 
	  * Scale transformation of the element in X
	  * @property ScaleX 
	  * @type float
	  * @values float: scale in X 
	  * @see http://www.w3.org/TR/SVG-Transforms/
	  * @default 1
	  */                          
      controller.registerProperty("ScaleX",element.setScaleX);

	 /*** 
	  * Scale transformation of the element in Y
	  * @property ScaleY 
	  * @type float
	  * @values float: scale in Y 
	  * @see http://www.w3.org/TR/SVG-Transforms/
	  * @default 1
	  */                          
      controller.registerProperty("ScaleY",element.setScaleY);

	 /*** 
	  * Skew transformation of the element in X
	  * @property SkewX 
	  * @type float
	  * @values float: skew angle in X 
	  * @see http://www.w3.org/TR/SVG-Transforms/
	  * @default 1
	  */                          
      controller.registerProperty("SkewX",element.setSkewX);

	 /*** 
	  * Skew transformation of the element in Y
	  * @property SkewY 
	  * @type float
	  * @values float: skew angle in Y 
	  * @see http://www.w3.org/TR/SVG-Transforms/
	  * @default 1
	  */                          
      controller.registerProperty("SkewY",element.setSkewY);
  
	 /*** 
	  * Stroke color
	  * @property LineColor 
	  * @type string
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Black"
	  */                          
      controller.registerProperty("LineColor",style.setLineColor,style.getLineColor);

	 /*** 
	  * Stroke width
	  * @property LineWidth 
	  * @type double
	  * @default 0.5
	  */                          
      controller.registerProperty("LineWidth",style.setLineWidth,style.getLineWidth);
      
	 /*** 
	  * Whether the stroke is drawed
	  * @property DrawLines 
	  * @type boolean
	  * @default true
	  */                          
      controller.registerProperty("DrawLines",style.setDrawLines,style.getDrawLines);

	 /*** 
	  * Fill color
	  * @property FillColor 
	  * @type string
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "none"
	  */                          
      controller.registerProperty("FillColor",style.setFillColor,style.getFillColor);
      
	 /*** 
	  * Whether the fill color is drawed
	  * @property DrawFill 
	  * @type boolean
	  * @default true
	  */                          
      controller.registerProperty("DrawFill",style.setDrawFill,style.getDrawFill);

	 /*** 
	  * SVG shape rendering 
	  * @property ShapeRendering 
	  * @type string
	  * @values "auto","optimizeSpeed","crispEdges","geometricPrecision"  
	  * @default "auto"
	  */                          
      controller.registerProperty("ShapeRendering",style.setShapeRendering,style.getShapeRendering);
      
	 /*** 
	  * Inline SVG attributtes  
	  * @property Attributes 
	  * @type object
	  * @values {nameAttr1:value1,nameAttr2:value2,..}
	  */                                
      controller.registerProperty("Attributes",style.setAttributes,style.getAttributes);

	 /*** 
	  * Whether the user could change the position   
	  * @property EnabledPosition 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("EnabledPosition",function(enabled) {
        element.getInteractionTarget(TARGET_POSITION).setMotionEnabled(enabled);
      });

	 /*** 
	  * Whether the group position also changes when the element position changes    
	  * @property MovesGroup 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("MovesGroup", function(affects) {
        element.getInteractionTarget(TARGET_POSITION).setAffectsGroup(affects);
      });

	 /*** 
	  * Whether the user could change the size   
	  * @property EnabledSize 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("EnabledSize",function(enabled) {
        element.getInteractionTarget(TARGET_SIZE).setMotionEnabled(enabled);
      });

	 /*** 
	  * Whether the group size also changes when the element size changes    
	  * @property ResizesGroup 
	  * @type boolean
	  * @default false
	  */                                
      controller.registerProperty("ResizesGroup", function(affects) {
        element.getInteractionTarget(TARGET_SIZE).setAffectsGroup(affects);
      });
      
	 /*** 
	  * Sensitivity in pixels when the user touchs the element    
	  * @property Sensitivity 
	  * @type int
	  * @default 20
	  */                                      
      controller.registerProperty("Sensitivity", function (sense) {
        element.getInteractionTarget(TARGET_POSITION).setSensitivity(sense);
        element.getInteractionTarget(TARGET_SIZE).setSensitivity(sense);
      });

  /*** 
	  * SensitivityType
	  * @property SensitivityType 
	  * @type int
	  * @default 20
	  */                                      
         controller.registerProperty("SensitivityType", function (type) {
          element.getInteractionTarget(TARGET_POSITION).setSensitivityType(type);
          element.getInteractionTarget(TARGET_SIZE).setSensitivityType(type);
        });

	 /*** 
	  * Event when double click     
	  * @action OnDoubleClick 
	  */                                      
      controller.registerAction("OnDoubleClick", element.getOnDoubleClickInformation);      
	 /*** 
	  * Event when the mouse enters in the element     
	  * @action OnEnter 
	  */                                      
      controller.registerAction("OnEnter",element.getOnEnterInformation);

	 /*** 
	  * Event when the mouse exits the element     
	  * @action OnExit 
	  */                                      
      controller.registerAction("OnExit",element.getOnExitInformation);

	 /*** 
	  * Event when the mouse clicks over the element     
	  * @action OnPress 
	  */                                      
      controller.registerAction("OnPress",element.getOnPressInformation);

	 /*** 
	  * Event when the mouse drags the element     
	  * @action OnDrag 
	  */                                      
      controller.registerAction("OnDrag",element.getOnDragInformation);

	 /*** 
	  * Event when the mouse release the element     
	  * @action OnRelease 
	  */                                      
      controller.registerAction("OnRelease",element.getOnReleaseInformation);
    },
    
    /**
     * Copies one element into another
     */
    copyTo : function(source, dest) {
      var InteractionTarget = EJSS_DRAWING2D.InteractionTarget;
      var TARGET_POSITION = EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION;
      var TARGET_SIZE = EJSS_DRAWING2D.PanelInteraction.TARGET_SIZE;
      
      EJSS_DRAWING2D.Style.copyTo(source.getStyle(),dest.getStyle());
      
      dest.setVisible(source.isVisible());
      dest.setMeasured(source.isMeasured());

      dest.setPosition(source.getPosition());
      dest.setPixelPosition(source.isPixelPosition());
      dest.setSize(source.getSize());
      dest.setPixelSize(source.isPixelSize());
      dest.setTransformation(source.getTransformation());
      dest.setRotate(source.getRotate());

	  dest.setRelativePosition(source.getRelativePosition());

      InteractionTarget.copyTo(source.getInteractionTarget(TARGET_POSITION),dest.getInteractionTarget(TARGET_POSITION));
      InteractionTarget.copyTo(source.getInteractionTarget(TARGET_SIZE),dest.getInteractionTarget(TARGET_SIZE));
      
      dest.setParent(source.getParent(), source);
    },
    
  	/**
   	* Returns offsets of the element with respect to its center.  
   	* @method getRelativePositionOffset
   	* @param position relative position 
   	* @param sx X size
   	* @param sy Y size
   	* @return [x,y]
   	*/
  	getRelativePositionOffset : function(position, sx, sy, invertedy) {
  		var dx = 0;
  		var dy = 0;
	    switch(position) {
	      default : 
	      case EJSS_DRAWING2D.Element.CENTER    : dx = 0; 		  dy = 0; 		break;
	      case EJSS_DRAWING2D.Element.NORTH     : dx = 0; 		  dy = -(sy/2); break;
	      case EJSS_DRAWING2D.Element.SOUTH     : dx = 0; 		  dy = (sy/2);  break;
	      case EJSS_DRAWING2D.Element.EAST      : dx = -(sx/2);    dy = 0; 		break;
	      case EJSS_DRAWING2D.Element.WEST      : dx = (sx/2);     dy = 0; 		break;
	      case EJSS_DRAWING2D.Element.NORTH_EAST: dx = -(sx/2);    dy = -(sy/2); break;
	      case EJSS_DRAWING2D.Element.NORTH_WEST: dx = (sx/2);     dy = -(sy/2); break;
	      case EJSS_DRAWING2D.Element.SOUTH_EAST: dx = -(sx/2);    dy = (sy/2);  break;
	      case EJSS_DRAWING2D.Element.SOUTH_WEST: dx = (sx/2);     dy = (sy/2);  break;
	    }
	    if(typeof invertedy != 'undefined' && invertedy) dy = -dy;
		return [dx,dy]; 
	},

  	/**
   	* Returns offsets of the element with respect to its south-west.  
   	* @method getSWRelativePositionOffset
   	* @param position relative position 
   	* @param sx X size
   	* @param sy Y size
   	* @return [x,y]
   	*/
  	getSWRelativePositionOffset : function(position, sx, sy, invertedy) {
  		var dx = 0;
  		var dy = 0;
	   	switch(position) {
	      default : 
	      case EJSS_DRAWING2D.Element.CENTER    : dx = -(sx/2); 	dy = (sy/2); break;
	      case EJSS_DRAWING2D.Element.NORTH     : dx = -(sx/2); 	dy = sy; 	break;
	      case EJSS_DRAWING2D.Element.SOUTH     : dx = -(sx/2); 	dy = 0;  	break;
	      case EJSS_DRAWING2D.Element.EAST      : dx = -sx;    	dy = (sy/2); break;
	      case EJSS_DRAWING2D.Element.WEST      : dx = 0;     	dy = (sy/2); break;
	      case EJSS_DRAWING2D.Element.NORTH_EAST: dx = -sx;    	dy = sy; 	break;
	      case EJSS_DRAWING2D.Element.NORTH_WEST: dx = 0;     	dy = sy; 	break;
	      case EJSS_DRAWING2D.Element.SOUTH_EAST: dx = -sx;    	dy = 0;  	break;
	      case EJSS_DRAWING2D.Element.SOUTH_WEST: dx = 0;     	dy = 0;  	break;
	    }
	    if(typeof invertedy != 'undefined' && invertedy) dy = -dy;	    
		return [dx,dy]; 
  	}
};

/**
 * Constructor for Element
 * @method element
 * @param mName string
 * @returns An abstract 2D element
 */
EJSS_DRAWING2D.element = function(mName) {
  var self = {};							// reference returned 

  // Static references
  var Element = EJSS_DRAWING2D.Element;		// reference for Element
  var PanelInteraction = EJSS_DRAWING2D.PanelInteraction;	// reference for PanelInteraction

  // Instance variables
  var mStyle = EJSS_DRAWING2D.style(mName);	// style for element 
  var mVisible = true;						// whether visible in drawing
  var mMeasured = true;						// whether measure for element
  var mRelativePosition = Element.CENTER;	// relative position

  // Position and size
  var mX = 0;						// position X
  var mY = 0;						// position Y
  var mPixelPosition = false; 		// whether the position is in pixels
  var mSizeX = 1;					// size X
  var mSizeY = 1;					// size Y
  var mPixelSize = false; 			// whether the size is in pixels
  var mTransformation = [];			// transformation for element
  var mRotate = 0;					// rotation for element

  // Interaction      
  var mInteraction = {
      positionTarget : EJSS_DRAWING2D.interactionTarget(self,PanelInteraction.TARGET_POSITION, Element.CENTER),
      sizeTarget     : EJSS_DRAWING2D.interactionTarget(self,PanelInteraction.TARGET_SIZE, Element.NORTH_EAST)
   };

  // Implementation variables    
  var mPanel = null;				// drawing panel for element
  var mGroup = null;				// group for element
  var mSet = null;				    // The set it belongs to (if any)  
  var mIndexInSet = -1;				// The index of the element in a set (if any)  

  var mProjectedPosition = [];		// projected position for element
  var mProjectedSize = [];			// projected size for element

  var mMustProject = true;			// whether drawing panel changed, then element needs to project
  var mElementChanged = true;		// whether element changed (position, size, or group)

  var mController = { 				// dummy controller object
      propertiesChanged : function() {},
      invokeAction : function() {}
  };

  var mCustomObject = null; // a placeholder to keep and object
  
  // ----------------------------------------
  // Public functions
  // ----------------------------------------
  
  /***
   * Gets name for element
   * @method getName
   * @visibility public
   * @return string
   */
  self.getName = function() {
    return mName;
  };  

  /***
   * Sets the property Parent
   * @method setParent
   * @param parent Panel or Group
   * @visibility public
   */
  self.setParent = function(parent, sibling) {
  	if(parent.render) { // is a panel  		
  		self.setGroup(null);
  		parent.addElement(self);
  		self.setPanel(parent);
  	} else if (parent.getClass() == "ElementGroup") { // is a group
  		self.setGroup(parent);
  		self.getGroupPanel().addElement(self, sibling);
  		self.setPanel(self.getGroupPanel());
  	} else { //
  		console.log("WARNING: setParent() - Parent not valid : "+ parent.getName()); 
  	}
  };

  /***
   * Gets the property Parent
   * @method getParent
   * @return Panel|Group
   * @visibility public
   */
  self.getParent = function() {
  	var parent;
  	if(mGroup !== null) parent = self.getGroup();
  	else parent = self.getPanel();
  	return parent  
  };

  /***
   * Is the element a group  
   * @method isGroup
   * @return boolean
   * @visibility public
   */
  self.isGroup = function() {
  	return false;
  }

  /***
   * Return the style associated to the element
   * @method getStyle
   * @return Style 
   * @visibility public
   */
  self.getStyle = function() { 
    return mStyle; 
  };

  /***
   * Store an object for internal use
   * @method setCustomObject
   * @visibility protected
   */
  self.setCustomObject = function(object) { mCustomObject = object; }

  /***
   * Retrieve an object for internal use
   * @method getCustomObject
   * @return Object
   * @visibility protected
   */
  self.getCustomObject = function() { return mCustomObject; }

  // ----------------------------------------
  // Position of the element
  // ----------------------------------------

  /***
   * Sets the property X
   * @method setX(x)
   * @param x double
   * @visibility public
   */
  self.setX = function(x, dummy) { 
    if (mX!=x) { 
      mX = x; 
      mElementChanged = true; 
    } 
  };

  /***
   * Gets the property X
   * @method getX()
   * @return double
   * @visibility public
   */
  self.getX = function() { 
    return mX; 
  };

  /***
   * Sets the property Y
   * @method setY(y)
   * @param y double
   * @visibility public
   */
  self.setY = function(y) {  
    if (mY!=y) { 
      mY = y; 
      mElementChanged = true; 
    } 
  };

  /***
   * Gets the property Y
   * @method getY()
   * @return double
   * @visibility public
   */
  self.getY = function() { 
    return mY; 
  };

  /***
   * Sets the property Position
   * @method setPosition
   * @param position double[2]
   * @visibility public
   */
  self.setPosition = function(position) {
    self.setX(position[0]);
    self.setY(position[1]);
  };

  /***
   * Gets the property Position
   * @method getPosition
   * @return double[2]
   * @visibility public
   */
  self.getPosition = function() { 
    return [mX, mY]; 
  };
  
  /***
   * Sets the property PixelPosition
   * @method setPixelPosition
   * @param pixel boolean
   * @visibility public
   */
  self.setPixelPosition = function(pixel) {
    if (mPixelPosition!=pixel) { 
      mPixelPosition = pixel;
      mElementChanged = true; 
    }
  };

  /***
   * Returns the property PixelPosition
   * @method isPixelPosition
   * @return boolean
   * @visibility public
   */
  self.isPixelPosition = function() {
    return mPixelPosition;
  };
  
  // ----------------------------------------
  // Size of the element
  // ----------------------------------------

  /***
   * Sets the property SizeX
   * @method setSizeX
   * @param sizeX double
   * @visibility public
   */
  self.setSizeX = function(sizeX) { 
    if (mSizeX!=sizeX) { 
      mSizeX = sizeX; 
      mElementChanged = true; 
    } 
  };

  /***
   * Gets the property SizeX
   * @method getSizeX
   * @return double
   * @visibility public
   */
  self.getSizeX = function() { 
    return mSizeX; 
  };

  /***
   * Sets the property SizeY
   * @method setSizeY
   * @param sizeY double
   * @visibility public
   */
  self.setSizeY = function(sizeY) { 
    if (mSizeY!=sizeY) { 
      mSizeY = sizeY; 
      mElementChanged = true; 
    }
  };

  /***
   * Gets the property SizeY
   * @method getSizeY
   * @return double
   * @visibility public
   */
  self.getSizeY = function() { 
    return mSizeY; 
  };

  /***
   * Sets the property Size
   * @method setSize
   * @param position double[2]
   * @visibility public
   */
  self.setSize = function(size) {
    self.setSizeX(size[0]);
    self.setSizeY(size[1]);
  };

  /***
   * Gets the property Size
   * @method getSize
   * @return double[]
   * @visibility public
   */
  self.getSize = function() {
    return [self.getSizeX(), self.getSizeY()];
  };
  
  /***
   * Sets the property PixelSize
   * @method setPixelSize
   * @param pixel boolean
   * @visibility public
   */
  self.setPixelSize = function(pixel) {
    if (mPixelSize!=pixel) { 
      mPixelSize = pixel;
      mElementChanged = true; 
    }
  };

  /***
   * Gets the property PixelSize
   * @method isPixelSize
   * @return boolean
   * @visibility public
   */
  self.isPixelSize = function() {
    return mPixelSize;
  };
    
  /***
   * Sets bounds for an element
   * @method setBounds
   * @param Object{left,rigth,top,bottom}|[left,rigth,top,bottom]
   * @visibility public
   */
  self.setBounds = function(bounds) {
  	var left,right,top,bottom;
  	
  	if(bounds.left) {
		left = bounds.left;
		right = bounds.right;
		top = bounds.top;
		bottom = bounds.bottom;  		
  	} else {
		left = bounds[0];
		right = bounds[1];
		top = bounds[2];
		bottom = bounds[3];  		  		
  	}
  	
	var sx = right-left;
	var sy = bottom-top;
	  
  	var d = self.getRelativePositionOffset(sx,sy);
  	var mx = sx/2, my = sy/2;  	

	var x = left + mx - d[0];
	var y = top + my - d[1];
	 
    self.setX(x);
    self.setY(y); 
    self.setSizeX(sx);
    self.setSizeY(sy); 
  };
    
  /***
   * Returns bounds for an element
   * @method getBounds
   * @return Object{left,rigth,top,bottom}
   * @visibility public
   */
  self.getBounds = function() {
  	var size = (mPixelSize? mPanel.toPanelMod([mSizeX,-mSizeY]) : [mSizeX,mSizeY]); 
  	
  	var mx = size[0]/2, my = size[1]/2;  	
  	var d = self.getRelativePositionOffset(size[0],size[1]);
	return {
		left: (mX+d[0])-mx,
		right: (mX+d[0])+mx,
		top: (mY+d[1])+my,
		bottom: (mY+d[1])-my
	}
  };

  /***
   * Returns bounds for an element (after applying groups) 
   * @method getAbsoluteBounds
   * @param withTransf whether transformation must be considered, note it will be 
   *  false (or undefinded) when transformations are applied by svg (drawing)
   * @return Object{left,rigth,top,bottom}
   * @visibility public
   */
  self.getAbsoluteBounds = function(withTransf) {
	var bounds = self.getBounds();	
	var p1 = self.toGroupSpace([bounds.left, bounds.top],withTransf);
	var p2 = self.toGroupSpace([bounds.right, bounds.bottom],withTransf);

	return {left: p1[0], top: p1[1], right: p2[0], bottom: p2[1]};
  };

  //---------------------------------
  // relative position
  //---------------------------------

  /**
   * Sets the relative position of the element with respect to its (x,y) coordinates.
   * @method setRelativePosition
   * @param position relative position
   */
  self.setRelativePosition = function(position) {
    if (typeof position == "string") position = Element[position.toUpperCase()];
    if (mRelativePosition != position) {
      mRelativePosition = position;
      mElementChanged = true; 
    }
  };
  
  /**
   * Gets the relative position of the element with respect to its (x,y) coordinates.
   * @method getRelativePosition
   * @return relative position
   */
  self.getRelativePosition = function() { 
    return mRelativePosition;
  };  

  /**
   * Returns offsets of the element with respect to its center.  
   * @method getRelativePositionOffset
   * @param s1 X size or size array
   * @param s2 Y size
   * @return [x,y]
   */
  self.getRelativePositionOffset = function(s1, s2) {
	var sx, sy;
  	if (Array.isArray(s1)) { // is array?
  		sx = s1[0];
  		sy = s1[1];
  	} else {
  		sx = s1;
  		sy = s2;
  	}  	
  	
  	var invertedScale = mPanel.getInvertedScaleY? mPanel.getInvertedScaleY():false;
  	return Element.getRelativePositionOffset(mRelativePosition, sx, sy, invertedScale);
  }

  // -------------------------------------
  // Visible and measure
  // -------------------------------------

  /***
   * Sets the property Visibility
   * @method setVisible
   * @param visible boolean
   * @visibility public
   */
  self.setVisible = function(visible) {
    if (visible!=mVisible) { 
      mVisible = visible; 
      mElementChanged = true;
    } 
  };

  /***
   * Gets the property Visibility
   * @method isVisible
   * @return boolean
   * @visibility public
   */
  self.isVisible = function() { 
    return mVisible; 
  };

  /***
   * Returns the real visibility status of the element, 
   * which will be false if it belongs to an invisible group
   * @method isGroupVisible
   * @return boolean
   * @visibility public
   */
  self.isGroupVisible = function() {
    var el = mGroup;
    while (typeof el != "undefined" && el !== null) {
      if (!el.isVisible()) return false;
      el = el.getGroup();
    }
    return mVisible;
  };

  /***
   * Sets the property Measured
   * @method setMeasured
   * @param measured boolean
   * @visibility public
   */
  self.setMeasured = function(measured) { 
    mMeasured = measured; 
  };

  /***
   * Gets the property Measured
   * @method isMeasured
   * @return boolean
   * @visibility public
   */
  self.isMeasured = function() { 
    return mMeasured; 
  };

  // ----------------------------------------------------
  // Transformations
  // ----------------------------------------------------

  /***
   * Sets the property Transformation
   * @method setTransformation
   * @param trans transformation - [scaleX, skewY, skewX, scaleY, translateX, translateY]
   * @visibility public
   */
  self.setTransformation = function(trans) {
    if (typeof trans == "undefined" || trans === null) { 
    	mTransformation = [];
    } else if (Array.isArray(trans) && trans.length>5) {  // matrix 
	    if (!EJSS_TOOLS.compareArrays(mTransformation,trans)) {
	      mTransformation = trans.slice();
	      mElementChanged = true;
	    }
    } else if (mRotate != trans) {  // rotation
		mRotate = trans;
		mElementChanged = true;    		
    }
  };

  /***
   * Gets the property Transformation
   * @method getTransformation
   * @return Transformation - [scaleX, skewY, skewX, scaleY, translateX, translateY] 
   * @visibility public
   */
  self.getTransformation = function() {
    return mTransformation;
  };

  /***
   * Sets the property Rotate
   * @method setRotate
   * @param Rotate angle 
   * @visibility public
   */
  self.setRotate = function(angle) {
	if (mRotate != angle) {
		mRotate = angle;
		mElementChanged = true;    		
    }
  };

  /***
   * Gets the property Rotate
   * @method getRotate
   * @return Rotate angle 
   * @visibility public
   */
  self.getRotate = function() {
    return mRotate;
  };

  /***
   * Sets the property ScaleX
   * @method setScaleX
   * @param scale rate  
   * @visibility public
   */
  self.setScaleX = function(rate) {
	if (mTransformation.length == 0)
		mTransformation = [1, 0, 0, 1, 0, 0];
	if (mTransformation[0] != rate) {
		mTransformation[0] = rate;
		mElementChanged = true;    		
    }
  };

  /***
   * Gets the property ScaleX
   * @method getScaleX
   * @return scale rate  
   * @visibility public
   */
  self.getScaleX = function() {
	if (mTransformation.length == 0)
		return 1;
	else
		return mTransformation[0];
  };

  /***
   * Sets the property ScaleY
   * @method setScaleY
   * @param scale rate  
   * @visibility public
   */
  self.setScaleY = function(rate) {
	if (mTransformation.length == 0)
		mTransformation = [1, 0, 0, 1, 0, 0];
	if (mTransformation[3] != rate) {
		mTransformation[3] = rate;
		mElementChanged = true;    		
    }
  };

  /***
   * Gets the property ScaleY
   * @method getScaleY
   * @return scale rate  
   * @visibility public
   */
  self.getScaleY = function() {
	if (mTransformation.length == 0)
		return 1;
	else
		return mTransformation[3];
  };

  /***
   * Sets the property SkewX
   * @method setSkewX
   * @param skew angle  
   * @visibility public
   */
  self.setSkewX = function(angle) {
	if (mTransformation.length == 0)
		mTransformation = [1, 0, 0, 1, 0, 0];
	if (mTransformation[2] != angle) {
		mTransformation[2] = angle;
		mElementChanged = true;    		
    }
  };

  /***
   * Gets the property SkewX
   * @method getSkewX
   * @return skew angle  
   * @visibility public
   */
  self.getSkewX = function() {
	if (mTransformation.length == 0)
		return 0;
	else
		return mTransformation[2];
  };

  /***
   * Sets the property SkewY
   * @method setSkewY
   * @param skew angle  
   * @visibility public
   */
  self.setSkewY = function(angle) {
	if (mTransformation.length == 0)
		mTransformation = [1, 0, 0, 1, 0, 0];
	if (mTransformation[1] != angle) {
		mTransformation[1] = angle;
		mElementChanged = true;    		
    }
  };

  /***
   * Gets the property SkewY
   * @method getSkewY
   * @return skew angle  
   * @visibility public
   */
  self.getSkewY = function() {
	if (mTransformation.length == 0)
		return 0;
	else
		return mTransformation[1];
  };

  // ----------------------------------------
  // Private functions
  // ----------------------------------------

  /***
   * Gets information for element
   * @method getInfo
   * @visibility private
   * @return string
   */
  self.getInfo = function() {
  	var info = "x=" + mX.toFixed(2) + " y=" + mY.toFixed(2);
  	  	
    return info;
  };     

  // ----------------------------------------
  // Panel and group 
  // ----------------------------------------

  /***
   * Sets the panel for this element
   * @method setPanel
   * @param panel DrawingPanel
   * @visibility private
   */
  self.setPanel = function(panel) {
    mPanel = panel;
    mMustProject = true;
  };

  /***
   * Gets the panel for this element
   * @method getPanel
   * @return DrawingPanel
   * @visibility private
   */
  self.getPanel = function() { 
    return mPanel;
  };

  /***
   * Returns the DrawingPanel in which it (or its final ancestor group) is displayed
   * @method getGroupPanel
   * @return DrawingPanel
   * @visibility private
   */
  self.getGroupPanel = function() { 
    var el = self;
    while (el.getGroup()) el = el.getGroup();
    return el.getPanel();
  };

  /***
   * Sets the group of this element
   * @method setGroup
   * @param group Group
   * @visibility private
   */
  self.setGroup = function(group) {
//	    if (mGroup) mGroup.removeChild(self);
	    mGroup = group;
//	    if (mGroup) mGroup.addChild(self);
	    mElementChanged = true; 
  };

  /***
   * Get the group of this element, if any
   * @method getGroup
   * @return Group
   * @visibility private
   */
  self.getGroup = function() { 
    return mGroup; 
  };

  // ----------------------------------------
  // Set
  // ----------------------------------------

  /***
   * Sets the index of this element in the set
   * @method setSet
   * @param set ElementSet
   * @param index int
   * @visibility private
   */
  self.setSet = function(set,index) {
    mSet = set;
    mIndexInSet = index;
  };

  /***
   * Gets the set of this element, if any
   * @method getSet
   * @return ElementSet
   * @visibility private
   */
  self.getSet = function() { 
    return mSet; 
  };

  /***
   * Gets the index of this element in a set, if any
   * @method getSetIndex
   * @return int
   * @visibility private
   */
  self.getSetIndex = function() { 
    return mIndexInSet; 
  };

  // ----------------------------------------
  // Changes
  // ----------------------------------------

  /***
   * Whether the element has changed
   * @method isChanged
   * @return boolean
   * @visibility private
   */
  self.isChanged = function() {
    return mElementChanged;
  };

  /***
   * Tells the element that it has changed
   * Typically used by subclasses when they change something.
   * @method setChanged
   * @param changed boolean
   * @visibility private
   */
  self.setChanged = function(changed) {
    mElementChanged = changed;
  };

  /***
   * Returns whether the element group has changed
   * @method isGroupChanged
   * @return boolean
   * @visibility private
   */
  self.isGroupChanged = function() {
    var el = self.getGroup();
    while (typeof el != "undefined" && el !== null) {
      if (el.isChanged()) return true;
      el = el.getGroup();
    }
    return false;
  };

  /***
   * Tells the element whether it should reproject its points because the panel
   * has changed its projection parameters. Or, the other way round, sets it to false
   * if someone (typically methods in subclasses) took care of this already
   * @method setMustProject
   * @param needsIt boolean
   * @visibility private
   */
  self.setMustProject = function(needsIt) {
    mMustProject = needsIt;
  };

  /***
   * Whether the element needs to project. Typically used by the dawing panel 
   * whenever it changes its scales
   * @method isMustProject
   * @return boolean
   * @visibility private
   */
  self.isMustProject = function() { 
    return mMustProject;  
  };

  // ----------------------------------------
  // Conversions
  // ----------------------------------------

  /***
   * Returns pixel coordinates 
   * @method getPixelPosition
   * @param withTransf whether transformation must be considered, note it will be 
   *  false (or undefinded) when transformations are applied by svg (drawing)
   * @return double[2]
   * @visibility private
   */
  self.getPixelPosition = function(withTransf) {
	if (self.isChanged() || self.isGroupChanged() || self.isMustProject() || withTransf) {  	
	  	// get projected position
	  	mProjectedPosition = self.getPixelPositionOf(mX,mY,withTransf);
	  	// get projected size
	  	mProjectedSize = self.getPixelSizeOf(mSizeX,mSizeY);

	    self.setMustProject(withTransf); // must be projected the next time  				  		
	}
	return mProjectedPosition;
  };

  /***
   * Returns pixel sizes 
   * @method getPixelSizes
   * @param withTransf whether transformation must be considered, note it will be 
   *  false (or undefinded) when transformations are applied by svg (drawing)
   * @return double[2]
   * @visibility private
   */
  self.getPixelSizes = function(withTransf) {
	if (self.isChanged() || self.isGroupChanged() || self.isMustProject()) {  	
	  	// get projected position
	  	mProjectedPosition = self.getPixelPositionOf(mX,mY,withTransf);
	  	// get projected size
	  	mProjectedSize = self.getPixelSizeOf(mSizeX,mSizeY);

	    self.setMustProject(withTransf); // must be projected the next time 				  		
	}
	return mProjectedSize;
  };

  /***
   * Returns the absolute world sizes in the panel (after applying groups)  
   * @method getAbsoluteSize
   * @return double[2]
   * @visibility private
   */
  self.getAbsoluteSize = function() {
  	var size = (mPixelSize? mPanel.toPanelMod([mSizeX,-mSizeY]) : [mSizeX,mSizeY]); 
    return self.toGroupSpaceMod(size);
  };

  /***
   * Returns the absolute position in the panel (after applying groups)  
   * @method getAbsolutePosition
   * @param withTransf whether transformation must be considered, note it will be 
   *  false (or undefinded) when transformations are applied by svg (drawing)
   * @return double[2]
   * @visibility private
   */
  self.getAbsolutePosition = function(withTransf) {
  	var pos;
  	if(mPixelPosition) {
  		if(mGroup === null)
  			// pos using the panel scale 
  			pos = mPanel.toPanelPosition([mX,mY])
  		else
  			// with group, only support scale NUM  
  			pos = mPanel.toPanelPosition([mX,mY],EJSS_DRAWING2D.DrawingPanel.SCALE_NUM)
  	} else {
  		pos = [mX,mY]; 
  	}
    return self.toGroupSpace(pos,withTransf);
  };

  /***
   * Sets the absolute position in the panel (after applying groups)  
   * Note: Transformations are not considered and it is not supported when pixel position is used
   * @method setAbsolutePosition
   * @return double[2]
   * @visibility private
   */
  self.setAbsolutePosition = function(position) {
  	if(mPixelPosition) {
  		console.log("setAbsolutePosition not supported!");
  	} else {
  		self.setPosition(self.toElementSpace(position)); 
  	}
  };

  /***
   * Sets the absolute position X in the panel (after applying groups)  
   * Note: Transformations are not considered and it is not supported when pixel position is used
   * @method setAbsoluteX
   * @return double
   * @visibility private
   */
  self.setAbsoluteX = function(x) {
  	if(mPixelPosition) {
  		console.log("setAbsoluteX not supported!");
  	} else {
  		self.setX(self.toElementSpace([x,0])[0]); 
  	}
  };

  /***
   * Sets the absolute position Y in the panel (after applying groups)  
   * Note: Transformations are not considered and it is not supported when pixel position is used
   * @method setAbsoluteY
   * @return double
   * @visibility private
   */
  self.setAbsoluteY = function(y) {
  	if(mPixelPosition) {
  		console.log("setAbsoluteY not supported!");
  	} else {
  		self.setY(self.toElementSpace([0,y])[1]); 
  	}
  };

  /***
   * Returns pixel sizes for the given sizes in the element's world coordinates
   * @method getPixelSizeOf
   * @return double[2]
   * @visibility private
   */
  self.getPixelSizeOf = function(sx,sy) {
	if (mPixelSize) return [sx,-sy];
    else return self.getGroupPanel().toPixelMod(self.toGroupSpaceMod([sx,sy]));
  };
 
  /***
   * Returns pixel coordinates for the given point in the element's world coordinates
   * @method getPixelPositionOf
   * @param withTransf whether transformation must be considered, note it will be 
   *  false (or undefinded) when transformations are applied by svg (drawing)
   * @return double[2]
   * @visibility private
   */
  self.getPixelPositionOf = function(x,y,withTransf) {
  	if(mPixelPosition) {
  	  if (mGroup) {
  	    var groupPos = mGroup.getPixelPosition();
  	    return [groupPos[0]+x,groupPos[1]-y];
  	  }
  	  return [self.getGroupPanel().toPixelAxisX(x), self.getGroupPanel().toPixelAxisY(y)];
  	}
  	else {
  		if(mGroup === null)
  			// pos using the panel scale 
  			return self.getGroupPanel().toPixelPosition(self.toGroupSpace([x,y]));
  			// with group, only support scale NUM  
  		return self.getGroupPanel().toPixelPosition(
  			self.toGroupSpace([x,y],withTransf),
  			EJSS_DRAWING2D.DrawingPanel.SCALE_NUM);
  	}
  };


  /***
   * Transforms a module (longitude or size) in the element's world coordinates to
   * the group's world coordinates
   * @method toGroupSpaceMod
   * @param mod double[] The original module in the body frame
   * @return double[] The same array once transformed
   * @visibility private
   */
  self.toGroupSpaceMod = function(mod) {
    var el = mGroup;
    while (typeof el != "undefined" && el !== null) { // apply group transformations 
      // scale vector considering [sizeX,sizeY]
      mod[0] *= el.getSizeX();
      mod[1] *= el.getSizeY();
      el = el.getGroup(); // next group
    }
    return mod;
  };
  
  /***
   * Transforms a double[] point in the element's world coordinates to
   * the group's world coordinates
   * @method toGroupSpace
   * @param point double[] The original coordinates in the body frame
   * @param withTransf bool Considering group transformations
   * @return double[] The same array once transformed
   * @visibility private
   */
  self.toGroupSpace = function(point, withTransf) {
    var el = mGroup;
    while (typeof el != "undefined" && el !== null) { // apply group transformations
      // scale point considering [sizeX,sizeY]
      point[0] *= el.getSizeX();
      point[1] *= el.getSizeY();
      // translate point
      point[0] += el.getX();
      point[1] += el.getY();
      el = el.getGroup();	// next group
    }

    // transformations
    if(withTransf) {
	   el = mGroup;
	   while (typeof el != "undefined" && el !== null) { // apply group transformations
	      	  // note: ignore transformation matrix (please use Sensitivity = 0) 
		      // rotation point
		      var rot = el.getRotate();
			  if (rot != 0) { // rotation angle 	
			      var pos = el.getAbsolutePosition();
			      point = EJSS_TOOLS.Mathematics.rotate(pos,point,-rot);
			      
			  }	// other transfs not considered      	
	      el = el.getGroup();	// next group
       }
    }

    return point;
  };

  /***
   * Transforms a double[] point in the group's world coordinates to
   * the element's world coordinates
   * Note: Transformations are not considered!
   * @method toElementSpace
   * @param point double[] The original coordinates in the body frame
   * @return double[] The same array once translated
   * @visibility private
   */
  self.toElementSpace = function(point) {
  	var list = [];
    var el = mGroup;
    while (typeof el != "undefined" && el !== null) { 
      list.push(el);
      el = el.getGroup();	// next group
    }
	list = list.reverse();
	
	var listLength = list.length;
	for (var i = 0; i < listLength; i++) {
      var el = list[i];
      // translate point
      point[0] -= el.getX();
      point[1] -= el.getY();
      // scale point considering [sizeX,sizeY]
	  if(el.getSizeX() == 0) point[0] = 0;
      else point[0] /= el.getSizeX();
	  if(el.getSizeY() == 0) point[1] = 0;
      else point[1] /= el.getSizeY();
    }
    return point;
  };

  // ----------------------------------------
  // Interaction
  // ----------------------------------------

  /***
   * Returns the controller object
   * @method getController
   * @return Controller
   * @visibility private
   */
  self.getController = function () {
    return mController;
  };

  /***
   * Sets the controller
   * @method setController
   * @param Controller
   * @visibility private
   */
  self.setController = function (controller) {
    mController = controller;
  };

  /***
   * Returns one of the interaction targets defined by the element
   * @method getInteractionTarget
   * @param target
   * @visibility private
   */
  self.getInteractionTarget = function(target) {
    switch (target) {
      case PanelInteraction.TARGET_POSITION : return mInteraction.positionTarget;
      case PanelInteraction.TARGET_SIZE : return mInteraction.sizeTarget;
    }
    return null;
  };

  /***
   * Returns array of the interaction targets defined by the element
   * @method getInteractionTarget
   * @return targets
   * @visibility private
   */
  self.getInteractionTargets = function() {
  	return [mInteraction.positionTarget, mInteraction.sizeTarget];
  };

  /***
   * Returns array of the interaction targets defined by the element
   * @method getInteractionTarget
   * @return targets
   * @visibility private
   */
  self.getInteractionInformation = function() {
  	return { info : self.getPosition(), element : self, point : self.getGroupPanel().getPanelInteraction().getInteractionPoint() };
  };

  self.getOnDoubleClickInformation = function() {
	var info = self.getInteractionInformation();
	info.action = 'OnDoubleClick';
	return info;
  };

  self.getOnEnterInformation = function() {
	var info = self.getInteractionInformation();
	info.action = 'OnEnter';
	return info;
  };

  self.getOnExitInformation = function() {
	var info = self.getInteractionInformation();
	info.action = 'OnExit';
	return info;
  };
	  
  self.getOnPressInformation = function() {
	var info = self.getInteractionInformation();
	info.action = 'OnPress';
	return info;
  };

  self.getOnDragInformation = function() {
	var info = self.getInteractionInformation();
	info.action = 'OnDrag';
	return info;
  };

  self.getOnReleaseInformation = function() {
	var info = self.getInteractionInformation();
	info.action = 'OnRelease';
	return info;
  };
  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  /***
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   * @visibility private
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Element.registerProperties(self,controller);
  };

  /***
   * Copies itself to another element
   * @method copyTo
   * @param element Element
   * @visibility private
   */
  self.copyTo = function(element) {
    EJSS_DRAWING2D.Element.copyTo(self,element);
  };

  /***
   * Get JSON object with private variables
   * @method serialize
   * @visibility private
   */
  self.serialize = function() {
  	return { 
  		mStyle: mStyle.serialize(), 
  		mVisible: mVisible, 
  		mMeasured: mMeasured, 
  		mRelativePosition: mRelativePosition,
  		
  		mX: mX, mY: mY, mPixelPosition: mPixelPosition,
  		mSizeX: mSizeX, mSizeY: mSizeY, mPixelSize: mPixelSize,
  		mTransformation: mTransformation,
		
		mInteraction: {
			//positionTarget: mInteraction.positionTarget.serialize(),
			//sizeTarget: mInteraction.sizeTarget.serialize()
		},
		
		mPanel: (mPanel?mPanel.getName():mPanel), mGroup: (mGroup?mGroup.getName():mGroup), 
		mSet: (mSet?mSet.getName():mSet), mIndexInSet: mIndexInSet,
		
		mProjectedPosition: mProjectedPosition, mProjectedSize: mProjectedSize,		
		 
  		};
  }
  
  /***
   * Set JSON object with private variables
   * @method unserialize
   * @parem json JSON object
   * @visibility private
   */
  self.unserialize = function(json) {
	mStyle.unserialize(json.mStyle), 
	mVisible = json.mVisible, 
	mMeasured = json.mMeasured, 
	mRelativePosition = json.mRelativePosition,
	
	mX = json.mX, mY = json.mY, mPixelPosition = json.mPixelPosition,
	mSizeX = json.mSizeX, mSizeY = json.mSizeY, mPixelSize = json.mPixelSize,
	mTransformation = json.mTransformation,
	
	// mInteraction.positionTarget = json.mInteraction.positionTarget,
	// mInteraction.sizeTarget = json.mInteraction.sizeTarget,
	
	// not support references changing
	//	mPanel = _view[json.mPanel], mGroup = json.mGroup, mSet = json.mSet, 
	mIndexInSet = json.mIndexInSet,
	
	mProjectedPosition = json.mProjectedPosition, mProjectedSize = json.mProjectedSize,		

    mElementChanged = true; 
  }

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  mStyle.setChangeListener(function (change) { mElementChanged = true; });
  
  return self;
};

