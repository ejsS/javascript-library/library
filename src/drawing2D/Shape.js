/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A Shape is a 2D drawing element that displays a 2D shape (such as a rectangle, ellipse, etc.) 
 * @class EJSS_DRAWING2D.Shape
 * @parent EJSS_DRAWING2D.Element
 */
EJSS_DRAWING2D.Shape = {
    NONE : 0,
    ELLIPSE : 1,
    RECTANGLE : 2,
    ROUND_RECTANGLE : 3,
    WHEEL : 4,
    POINT : 5,

    /**
     * Static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller);

	 /*** 
	  * Type of shape
	  * @property ShapeType 
	  * @type int|String
	  * @values 0:"NONE", 1:"ELLIPSE", 2:"RECTANGLE", 3:"ROUND_RECTANGLE", 4:"WHEEL", 5:"POINT" 
	  * @default "ELLIPSE" 
	  */  
      controller.registerProperty("ShapeType",element.setShapeType);
      
	 /*** 
	  * Radius for the corners of a round rectangular shape
	  * @property CornerRadius 
	  * @type int
	  * @default 10 
	  */  
      controller.registerProperty("CornerRadius",element.setCornerRadius);
    },

    /**
     * Static copyTo method, to be used by sets
     */
    copyTo : function(source, dest) {
      EJSS_DRAWING2D.Element.copyTo(source,dest);

      dest.setShapeType(source.getShapeType());
      dest.setCornerRadius(source.getCornerRadius());
    }

};

/**
 * Creates a 2D shape
 * @method shape
 */
EJSS_DRAWING2D.shape = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  // Instance variables
  var mCornerRadius = 10;
  var mShapeType = EJSS_DRAWING2D.Shape.ELLIPSE;

  // ----------------------------------------------------
  // Public functions
  // ----------------------------------------------------

  /***
   * Sets the value of the property ShapeType
   * @method setShapeType(shapeType)
   * @visibility public
   * @param shapeType int|String 
   */ 
  self.setShapeType = function(shapeType) {
    if (typeof shapeType === 'string')
      mShapeType = EJSS_DRAWING2D.Shape[shapeType.toUpperCase()];
    else 
      mShapeType = shapeType;
  };

  /***
   * Gets the value of the property ShapeType
   * @method getShapeType
   * @visibility public
   * @return int|String 
   */ 
  self.getShapeType = function() {
  	return mShapeType;
  }

  /***
   * Sets the value of the property CornerRadius
   * @method setCornerRadius
   * @param radius int
   */
  self.setCornerRadius = function(radius) {
    if(mCornerRadius != radius) {
    	mCornerRadius = radius;
    	self.setChanged(true);
    }
  };

  /***
   * Gets the value of the property CornerRadius
   * @method getCornerRadius
   * @return int
   */
  self.getCornerRadius = function() {
    return mCornerRadius;
  };

  // ----------------------------------------------------
  // Private functions
  // ----------------------------------------------------

  /***
   * Returns the class name
   * @method getClass
   * @visibility private
   * @return String
   */
  self.getClass = function() {
  	return "ElementShape";
  }

  /***
   * Extended registerProperties method
   * @method registerProperties
   * @param controller
   * @visibility private
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Shape.registerProperties(self,controller);
  };

  /***
   * Extended copyTo method
   * @method copyTo
   * @param Element
   * @visibility private
   */
  self.copyTo = function(element) {
    EJSS_DRAWING2D.Shape.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  /*** @property Size @default [0.1,0.1] */
  self.setSize([0.1,0.1]);
  
  /*** @property RelativePosition @default "CENTER" */
  self.setRelativePosition("CENTER");
  
  /*** @property FillColor @default "Blue" */  
  self.getStyle().setFillColor("Blue");
  
  /*** @property LineColor @default "Black" */  
  self.getStyle().setLineColor("Black");
    
  return self;
};


