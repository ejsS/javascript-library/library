/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia and Félix J. García 
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for 2D drawing.
 * @module 2Ddrawing
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A SimplePanel is a 2D drawing panel with no decoration. 
 * @class EJSS_DRAWING2D.SimplePanel
 * @see EJSS_DRAWING2D.PlottingPanel
 * @constructor
 */
EJSS_DRAWING2D.SimplePanel = {
	GRAPHICS2D_SVG : "SVG",
	GRAPHICS2D_CANVAS : "Canvas",

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		// No super class
		// EJSS_INTERFACE.Element.registerProperties(element.getGraphics(), controller);

		controller.registerProperty("MinimumX", element.setWorldXMin, element.getWorldXMin);
		controller.registerProperty("MaximumX", element.setWorldXMax, element.getWorldXMax);
		controller.registerProperty("MinimumY", element.setWorldYMin, element.getWorldYMin);
		controller.registerProperty("MaximumY", element.setWorldYMax, element.getWorldYMax);
		controller.registerProperty("Bounds", element.setWorldCoordinates, element.getWorldCoordinates);


		controller.registerProperty("MarginX", element.setMarginX,element.getMarginX);
		controller.registerProperty("MarginY", element.setMarginY,element.getMarginY);

		controller.registerProperty("Parent", element.getGraphics().setParent, element.getGraphics().getParent);
		controller.registerProperty("Width", element.getGraphics().setWidth, element.getGraphics().getWidth);
		controller.registerProperty("Height", element.getGraphics().setHeight, element.getGraphics().getHeight);

		controller.registerProperty("Graphics", element.setGraphics);

		controller.registerProperty("Background", element.getStyle().setFillColor);
		controller.registerProperty("Foreground", element.getStyle().setLineColor);
		controller.registerProperty("LineColor", element.getStyle().setLineColor);
		controller.registerProperty("LineWidth", element.getStyle().setLineWidth);
		controller.registerProperty("DrawLines", element.getStyle().setDrawLines);
		controller.registerProperty("FillColor", element.getStyle().setFillColor);
		controller.registerProperty("DrawFill", element.getStyle().setDrawFill);
		controller.registerProperty("ShapeRendering", element.getStyle().setShapeRendering);

	    controller.registerProperty("Visibility", element.getGraphics().getStyle().setVisibility);      
        controller.registerProperty("Display", element.getGraphics().getStyle().setDisplay); 
		controller.registerProperty("CSS", element.getGraphics().getStyle().setCSS);
	}
};

/**
 * Constructor for SimplePanel
 * @method simplePanel
 * @param mName string
 * @returns An abstract 2D drawing panel
 */
EJSS_DRAWING2D.simplePanel = function(mName) {
	var self = {}; // reference returned	
	var mGraphics = EJSS_GRAPHICS.svgGraphics(mName); // graphics implementation (default: SVG)

	// Instance variables
	var mStyle = EJSS_DRAWING2D.style(mName);	// style for panel
	var mElements = [];							// elements list for panel
	var mElementsChanged = false;				// whether elements list has changed
	var mCollectersList = [];		            // Array of all control elements that need a call to dataCollected() after data collection

	// Configuration variables	
	var mWorld = {
		// preferred dimensions
		xminPreferred : -1, xmaxPreferred : 1, yminPreferred : -1, ymaxPreferred : 1,
		// origin in panel
		xorigin : 0, yorigin : 0,
		// pixel per unit for panel
		xscale : 1, yscale : 1		
	};

	// Implementation variables
	var mPanelChanged = true;	// whether panel changed (style, decorations, gutters)
	var mMustScale = true;		// whether panel must scale

	// ----------------------------------------
	// Instance functions
	// ----------------------------------------

	/**
	 * Get name for drawing panel
	 * @method getName
	 * @return string
	 */
	self.getName = function() {
		return mName;
	};

	/**
	 * Returns the graphics implementation
	 * @method getGraphics
	 * @return Graphics
	 */
	self.getGraphics = function() {
		return mGraphics;
	};

	/**
	 * Returns the svg image in Base64 format
	 * @method importGraphics
	 * @return string 
	 */
    self.importGraphics = function(callback) {
    	return mGraphics.importSVG(callback);
    }
    
	/**
	 * Return the drawing style of the inner rectangle for panel
	 * @method getStyle
	 * @return Style
	 */
	self.getStyle = function() {
		return mStyle;
	};

	/**
	 * Set graphics
	 * @method setGraphics
	 * @param type
	 */
	self.setGraphics = function(type) {
		if (type == EJSS_DRAWING2D.SimplePanel.GRAPHICS2D_SVG) {
			mGraphics = EJSS_GRAPHICS.svgGraphics(mName);
		} else if (type == EJSS_DRAWING2D.SimplePanel.GRAPHICS2D_CANVAS) {
			// mGraphics = EJSS_GRAPHICS.canvasGraphics(mName);
			console.log("WARNING: setGraphics() - Canvas not supported");
		} else {
			console.log("WARNING: setGraphics() - Graphics not supported");
		}
	};
	
	// ----------------------------------------
	// World coordinates
	// ----------------------------------------

	/**
	 * Sets the preferred minimum X coordinate for the panel
	 * @method setWorldXMin
	 * @param xmin
	 */
	self.setWorldXMin = function(xmin) {
		if (xmin !== mWorld.xminPreferred) {
			mWorld.xminPreferred = xmin;
			mMustScale = true;
		}
	};

	/**
	 * Returns the preferred minimum X coordinate for the panel
	 * @method getWorldXMin
	 * @return double
	 */
	self.getWorldXMin = function() {
		return mWorld.xminPreferred;
	};

	/**
	 * Sets the preferred maximum X coordinate for the panel
	 * @method setWorldXMax
	 * @param xmax
	 */
	self.setWorldXMax = function(xmax) {
		if (xmax !== mWorld.xmaxPreferred) {
			mWorld.xmaxPreferred = xmax;
			mMustScale = true;
		}
	};

	/**
	 * Returns the preferred maximum X coordinate for the panel
	 * @method getWorldXMax
	 * @return double
	 */
	self.getWorldXMax = function() {
		return mWorld.xmaxPreferred;
	};

	/**
	 * Sets the preferred minimum Y coordinate for the panel
	 * @method setWorldYMin
	 * @param ymin
	 */
	self.setWorldYMin = function(ymin) {
		if (ymin !== mWorld.yminPreferred) {
			mWorld.yminPreferred = ymin;
			mMustScale = true;
		}
	};

	/**
	 * Returns the preferred minimum Y coordinate for the panel
	 * @method getWorldYMin
	 * @return double
	 */
	self.getWorldYMin = function() {
		return mWorld.yminPreferred;
	};

	/**
	 * Sets the preferred maximum Y coordinate for the panel
	 * @method setWorldYMax
	 * @param ymax
	 */
	self.setWorldYMax = function(ymax) {
		if (ymax !== mWorld.ymaxPreferred) {
			mWorld.ymaxPreferred = ymax;
			mMustScale = true;
		}
	};

	/**
	 * Returns the preferred maximum Y coordinate for the panel
	 * @method getWorldYMax
	 * @return double
	 */
	self.getWorldYMax = function() {
		return mWorld.ymaxPreferred;
	};

	/**
	 * Sets the preferred user coordinates for the panel
	 * @method setWorldCoordinates
	 * @param bounds
	 */
	self.setWorldCoordinates = function(bounds) {
		self.setWorldXMin(bounds[0]);
		self.setWorldXMax(bounds[1]);
		self.setWorldYMin(bounds[2]);
		self.setWorldYMax(bounds[3]);
	};

	/**
	 * Gets the preferred user coordinates for the panel
	 * @method getWorldCoordinates
	 * @return bounds
	 */
	self.getWorldCoordinates = function() {
		return [self.getWorldXMin(), self.getWorldXMax(), self.getWorldYMin(), self.getWorldYMax()];
	};

	// ----------------------------------------
	// Elements
	// ----------------------------------------

	/**
	 * Add a element to the panel. Elements are asked to draw themselves
	 * whenever the panel needs to render. For this purpose, they will receive a
	 * calls to draw().
	 * Elements are reported of changes in the world coordinates of the panel, in case
	 * they need to recalculate themselves.
	 * @method addElement
	 * @param element Element
	 * @param position int
	 */
	self.addElement = function(element, position) {
		EJSS_TOOLS.addToArray(mElements, element, position);
		// set this panel to decoration element
		element.setPanel(self);
		if (element.dataCollected) mCollectersList.push(element);
		// elements list has changed
		mElementsChanged = true;
	};

	/**
	 * Remove a element to the panel.
	 * @method removeElement
	 * @param element Element
	 */
	self.removeElement = function(element) {
		EJSS_TOOLS.removeFromArray(mElements, element);
		element.setPanel(null);
		if (element.dataCollected) EJSS_TOOLS.removeFromArray(mCollectersList, element);
		// elements list has changed
		mElementsChanged = true;
	};

	/**
	 * Return the array of a elements.
	 * @method getElements
	 * @return Elements
	 */
	self.getElements = function() {
		return mElements;
	};

	/**
	 * Return the position of a element.
	 * @method indexOfElement
	 * @param element Element
	 * @return integer
	 */
	self.indexOfElement = function(element) {
		return mElements.indexOf(element);
	};

	// ----------------------------------------
	// Apply transformations and conversions
	// ----------------------------------------

	/**
	 * Converts a Y pixel value so that 0 is at the bottom
	 * @method toPixelAxisY
	 * @param y double
	 */
	self.toPixelAxisY = function(y) {
		return (mWorld.yorigin - y) - (mWorld.yscale * mWorld.yminPreferred);
	};

	/**
	 * Converts a X pixel value so that 0 is at the left
	 * @method toPixelAxisX
	 * @param x double
	 */
	self.toPixelAxisX = function(x) {
		return (mWorld.xorigin + x) - (mWorld.xscale * mWorld.xminPreferred);
	};

	/**
	 * To be used only after a call to render()!
	 * Projects a point from world coordinates to pixel coordinates.
	 * @method toPixelPosition
	 * @param point double[] The original coordinates
	 * @return double[] The same array once transformed
	 */
	self.toPixelPosition = function(point) {
		var pos = [];
		pos[0] = mWorld.xorigin + mWorld.xscale * (point[0] - mWorld.xminPreferred);		
		pos[1] = mWorld.yorigin + mWorld.yscale * (point[1] - mWorld.yminPreferred);			
		return pos;
	};

	/**
	 * To be used only after a call to render()!
	 * Projects a module from world coordinates to pixel coordinates
	 * @method toPixelMod
	 * @param point double[] The original module
	 * @return double[] The same array once transformed
	 */
	self.toPixelMod = function(mod) {
		var pmod = [];
		pmod[0] = mod[0] * mWorld.xscale;
		pmod[1] = mod[1] * mWorld.yscale;		
		return pmod;
	};

	/**
	 * To be used only after a call to render()!
	 * Projects a point from pixel coordinates to world coordinates
	 * @method toPanelPosition
	 * @param point double[] The original coordinates
	 * @return double[] The same array once transformed
	 */
	self.toPanelPosition = function(point) {	
		var pos = [];
		pos[0] = mWorld.xminPreferred + (point[0] - mWorld.xorigin) / mWorld.xscale;
		pos[1] = mWorld.yminPreferred + (point[1] - mWorld.yorigin) / mWorld.yscale;
		return pos;
	};

	/**
	 * To be used only after a call to render()!
	 * Projects a module from pixel coordinates to world coordinates
	 * @method toPanelMod
	 * @param point double[] The original module
	 * @return double[] The same array once transformed
	 */
	self.toPanelMod = function(mod) {
		var pmod = [];
		pmod[0] = ((mWorld.xscale == 0)? 0 : mod[0]/mWorld.xscale);
		pmod[1] = ((mWorld.yscale == 0)? 0 : mod[1]/mWorld.yscale);		
		return pmod;
	};

	/**
	 * Get pixel position of the origin
	 * @return array pixel position
	 */
	self.getPixelPositionWorldOrigin = function() {
		return [mWorld.xorigin,mWorld.yorigin];
	}

	/**
	 * Recomputes the scales of the panel.
	 * @method recomputeScales
	 */
	self.recomputeScales = function() {	
		// get sizes and scale
		var width = mGraphics.getWidth(); // width in pixels
		var height = mGraphics.getHeight(); // height in pixels
		var xPixPerUnit = width / (mWorld.xmaxPreferred - mWorld.xminPreferred); // the x scale in pixels
		var yPixPerUnit = height / (mWorld.ymaxPreferred - mWorld.yminPreferred); // the y scale in pixels
		
		// centered
		mWorld.xscale = xPixPerUnit;
		mWorld.yscale = -yPixPerUnit;
		mWorld.xorigin =  0.5;
		mWorld.yorigin = height + 0.5;
		
		mMustScale = false;
	};
	
	// ----------------------------------------
	// Drawing functions
	// ----------------------------------------

	/**
	 * Reset the scene
	 * @method reset
	 */
	self.reset = function() {
		mGraphics.reset();
	};
	
	/**
	 * Render the scene
	 * @method render
	 */
	self.render = function() {
		var reseted = false;
		if (mElementsChanged) {// whether elements added or removed, reset the scene
			mGraphics.reset();
			reseted = true;
			mElementsChanged = false;
		}

        // check for data collection
		for (var i = 0, n = mCollectersList.length; i < n; i++)
			mCollectersList[i].dataCollected();

		// recompute scales
		if (mMustScale) 
			self.recomputeScales();

		if (mPanelChanged || reseted) {// whether panel changed or reseted
			mGraphics.drawPanel(self);
		}

		// draw visible elements
		mGraphics.draw(mElements, reseted);

		// set changed to false
		mPanelChanged = false;
		for (var i = 0, n = mElements.length; i < n; i++)
			mElements[i].setChanged(false);
	};

	// ----------------------------------------------------
	// Properties
	// ----------------------------------------------------

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.SimplePanel.registerProperties(self, controller);
	};

	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	mStyle.setLineColor('black');
	mStyle.setFillColor('rgb(239,239,255)');
	mStyle.setChangeListener(function(change) {
		mPanelChanged = true;
	});


	return self;
};

