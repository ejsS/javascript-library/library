/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * CellLattice
 * @class CellLattice 
 * @constructor  
 */
EJSS_DRAWING2D.CellLattice = {	
	
	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Data", element.setData, element.getData);
		controller.registerProperty("ShowGrid", element.setShowGrid, element.getShowGrid);
		controller.registerProperty("NumColors", element.getColorMapper().setNumberOfColors);
		controller.registerProperty("Colors", element.getColorMapper().setColorPalette);
		controller.registerProperty("Palette", element.getColorMapper().setPaletteType);
        controller.registerProperty("AutoUpdate", element.setAutoupdate);  
	}
			
};

/**
 * Creates a 2D CellLattice
 * @method cellLattice
 */
EJSS_DRAWING2D.cellLattice = function(name) {
	var self = EJSS_DRAWING2D.element(name);
 
 	var mColorMapper = EJSS_DRAWING2D.colorMapper(20, EJSS_DRAWING2D.ColorMapper.REDBLUE_SHADE); 	
 	var mShowGrid = true;
	var mData = [];
	var mAutoUpdate=true;
 
	self.getClass = function() {
		return "ElementCellLattice";
	}


	/** 
	 * @method setData
	 * @param data
	 */
	self.setData = function (data) {
	  if (mAutoUpdate || mData != data) {
	    mData = data;
		mDataChanged = true;
		self.setChanged(true);
	  }
	}
	  
	/**
	 * @method getData
	 * @return
	 */
	self.getData = function() { 
		return mData; 
	}
	 
	self.setAutoupdate= function (auto) {
	  mAutoUpdate = auto;
      if (mAutoUpdate) mDataChanged = true;
    }
	
	/** 
	 * @method setColorMapper
	 * @param colormapper
	 */
	self.setColorMapper = function (colormapper) {
	  	if(mColorMapper != colormapper) {
	  		mColorMapper = colormapper;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getColorMapper
	 * @return
	 */
	self.getColorMapper = function() { 
		return mColorMapper; 
	}
	 
	/** 
	 * @method setShowGrid
	 * @param showgrid
	 */
	self.setShowGrid = function (showgrid) {
	  	if(mShowGrid != showgrid) {
	  		mShowGrid = showgrid;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getShowGrid
	 * @return
	 */
	self.getShowGrid = function() { 
		return mShowGrid; 
	}


	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.CellLattice.registerProperties(self, controller);
	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	return self;
};

 
