/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//PolygonSet
//---------------------------------

/**
 * PolygonSet
 * @class PolygonSet 
 * @constructor  
 */
EJSS_DRAWING2D.PolygonSet = {
    
    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING2D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("Points", 
          function(v) { set.setToEach(function(element,value) { element.setPoints(value); }, v); }
      );
      controller.registerProperty("LastPoint", 
          function(v) { set.setToEach(function(element,value) { element.addPoint(value); }, v); }
      );           
    }        
};

/**
 * Creates a set of Segments
 * @method polygonSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.polygonSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.polygon, mName);

  // Static references
  var PolygonSet = EJSS_DRAWING2D.PolygonSet;		// reference for PolygonSet

  /**
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    PolygonSet.registerProperties(self,controller);
  };

  return self;
};