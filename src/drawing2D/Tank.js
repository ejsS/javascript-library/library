/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Shape
 * @class Shape 
 * @constructor  
 */
EJSS_DRAWING2D.Tank = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

      controller.registerProperty("LevelColor",element.setLevelColor);
      controller.registerProperty("Level",element.setLevel);
      controller.registerProperty("Closed",element.setClosed);
    },

    /**
     * static copyTo method, to be used by sets
     */
    copyTo : function(source, dest) {
      EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy

      dest.setLevel(source.getLevel());
      dest.setLevelColor(source.getLevelColor());
    }

};

/**
 * Creates a 2D shape
 * @method shape
 */
EJSS_DRAWING2D.tank = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  var mLevel = 0;
  var mLevelColor = "blue";

  // Implementation variables
  var Tank = EJSS_DRAWING2D.Tank;

  self.getClass = function() {
  	return "ElementTank";
  }

  // ----------------------------------------------------
  // public functions
  // ----------------------------------------------------

  self.setLevel = function(level) {
      mLevel = level;
      self.setChanged(true);
  };

  self.getLevel = function() {
  	return mLevel;
  }

  /**
   * Set the color of the level
   * @param color a stroke style
   */
  self.setLevelColor = function(color) { 
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color!=mLevelColor) {
      mLevelColor = color; 
      self.setChanged(true);
    }
    return self;
  };
    
  /**
   * Get the line color
   */
  self.getLevelColor = function() { 
    return mLevelColor; 
  };
  
  // ----------------------------------------------------
  // Properties and copies
  // ----------------------------------------------------

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    Tank.registerProperties(self,controller);
  };

  /**
   * Copies itself to another element
   * Extended copyTo method. To be used by Sets
   * @method copyTo
   */
  self.copyTo = function(element) {
    Tank.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([0.1,0.1]);
  self.setRelativePosition("CENTER");
  self.getStyle().setFillColor("White");
  self.getStyle().setLineColor("Black");
  self.getStyle().setDrawFill(false);
    
  return self;
};


