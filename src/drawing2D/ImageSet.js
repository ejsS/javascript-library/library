/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//ImageSet
//---------------------------------

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * ImageSet
 * @class ImageSet 
 * @constructor  
 */
EJSS_DRAWING2D.ImageSet = {
    
    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING2D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      
      controller.registerProperty("ImageUrl", 
          function(v) { set.setToEach(function(element,value) { element.setImageUrl(value); }, v); }
      );
    }    
};


/**
 * Creates a set of Segments
 * @method imageSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.imageSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.image, mName);

  // Static references
  var ImageSet = EJSS_DRAWING2D.ImageSet;		// reference for ImageSet

  /**
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    ImageSet.registerProperties(self,controller);
  };

  return self;
};