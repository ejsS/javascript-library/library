/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Deployment for 2D drawing.
 * @module 2Dgraphics 
 */

var EJSS_GRAPHICS = EJSS_GRAPHICS || {};

/**
 * SVGGraphics class
 * @class SVGGraphics 
 * @constructor  
 */
EJSS_GRAPHICS.SvgGraphics = {

  /**
   * Return document box
   * @return box  
   */
  getOffsetRect: function(graphics) {
  	return EJSS_GRAPHICS.GraphicsUtils.getOffsetRect(graphics);
  }
};

/**
 * Constructor for Element
 * @param mName Identifier SVG element in HTML document
 * @returns A SVG graphics
 */
EJSS_GRAPHICS.svgGraphics = function(mName) {
  var self = EJSS_INTERFACE.svgGraphics(mName);	// reference returned     
  var mInterfaceGraphics = self.getDOMElement();
  var mDefs = document.createElementNS("http://www.w3.org/2000/svg", "defs");
  mInterfaceGraphics.appendChild(mDefs);	     



  /**
   * Return SVG document for event handle
   * @return SVG element 
   */
  self.getEventContext = function() {  	
    return mInterfaceGraphics;
  };
  
  /**
   * Return SVG document box
   * @return width integer 
   */
  self.getBox = function() {
  	var box = EJSS_GRAPHICS.CanvasGraphics.getOffsetRect(self)
  	box.width -= 1;
  	box.height -= 1;
  	return box;
  };

  /**
   * Remove a shape
   * @param name
   */
  self.remove = function(name) {	
    var myElement = mInterfaceGraphics.getElementById(name);		// get the SVG element from document svg
    if (myElement !== null) { 
    	mInterfaceGraphics.removeChild(myElement);
    	return true;
    }
    return false;
  };

  /**
   * Reset the SVG graphics
   */
  self.reset = function() {	
	while (mInterfaceGraphics.firstChild) {
	    mInterfaceGraphics.removeChild(mInterfaceGraphics.firstChild);
	} 
  	// create defs
//  	mDefs = document.createElementNS("http://www.w3.org/2000/svg", "defs");
  	mInterfaceGraphics.appendChild(mDefs);	     
  };

  
  self.getDefs = function() { return mDefs; }
  
  /**
   * Draw Elements
   * @param elements array of Elements and Element Sets
   * @param force whether force draw elements
   */
  self.draw = function(elements,force) {
    for (var i=0, n=elements.length; i<n; i++) { 
      var element = elements[i];
      if(element.getElements) {	// whether element set
      	var lasts = element.getLastElements();
  		for(var j=0, m=lasts.length; j<m; j++) { // remove last removed elements
			var mShape = mInterfaceGraphics.getElementById(lasts[j].getName());
			if(mShape) mShape.parentNode.removeChild(mShape);
  		}
      }
  	  if(force || element.isChanged() || element.isGroupChanged() || element.isMustProject()) {
  		var build = self.drawElement(element);
		if(typeof build != "undefined" && build != null)  		 
			self.transformElement(element, build);
	  }
    }
  };
    
  /**
   * Draw Element
   * @param element
   */
  self.drawElement = function (element) {
  	var Shape = EJSS_DRAWING2D.Shape;			
    // console.log("Drawing element "+element.getName());
    var build = null;
	
  	switch (element.getClass()) {
		case "ElementCustom":
		  {
		    var customFunction = element.getFunction();
		    if (customFunction) build = customFunction(mInterfaceGraphics,element);	
		  }
		  break;
  		case "ElementGroup": 	build = EJSS_SVGGRAPHICS.group(mInterfaceGraphics,element);	break;
  		case "ElementShape": 
			switch (element.getShapeType()) {
			  default : 
			  case Shape.ELLIPSE         : build = EJSS_SVGGRAPHICS.ellipse(mInterfaceGraphics,element); 	break;
			  case Shape.RECTANGLE       : build = EJSS_SVGGRAPHICS.rectangle(mInterfaceGraphics,element);	break;
			  case Shape.ROUND_RECTANGLE : build = EJSS_SVGGRAPHICS.roundRectangle(mInterfaceGraphics,element); break;
			  case Shape.WHEEL           : build = EJSS_SVGGRAPHICS.wheel(mInterfaceGraphics,element); 		break;
			  case Shape.NONE            : // do not break;
			  case Shape.POINT           : build = EJSS_SVGGRAPHICS.point(mInterfaceGraphics,element); 		break;
			}
			break;
		case "ElementSegment": 	build = EJSS_SVGGRAPHICS.segment(mInterfaceGraphics,element);	break;
		case "ElementImage":   	build = EJSS_SVGGRAPHICS.image(mInterfaceGraphics,element);		break;
		case "ElementVideo":   	build = EJSS_SVGGRAPHICS.video(mInterfaceGraphics,element);		break;
		case "ElementArrow":	build = EJSS_SVGGRAPHICS.arrow(mInterfaceGraphics,element);		break;
		case "ElementText":		build = EJSS_SVGGRAPHICS.text(mInterfaceGraphics,element);		break;
		case "ElementSpring":	build = EJSS_SVGGRAPHICS.spring(mInterfaceGraphics,element);		break;
		case "ElementTrail":	build = EJSS_SVGGRAPHICS.trail(mInterfaceGraphics,element);		break;
		case "ElementTrace":	build = EJSS_SVGGRAPHICS.trace(mInterfaceGraphics,element);		break;
		case "ElementGrid":		build = EJSS_SVGGRAPHICS.grid(mInterfaceGraphics,element);		break;
		case "ElementAxis":		build = EJSS_SVGGRAPHICS.axis(mInterfaceGraphics,element);		break;
		case "ElementCursor":	build = EJSS_SVGGRAPHICS.cursor(mInterfaceGraphics,element);		break;
		case "ElementPolygon":	build = EJSS_SVGGRAPHICS.polygon(mInterfaceGraphics,element);	break;
		case "ElementAnalyticCurve":	build = EJSS_SVGGRAPHICS.analyticCurve(mInterfaceGraphics,element);	break;
		case "ElementCellLattice": 	build = EJSS_SVGGRAPHICS.cellLattice(mInterfaceGraphics,element);	break; 			
		case "ElementByteRaster": 	build = EJSS_SVGGRAPHICS.byteRaster(mInterfaceGraphics,element);	break;			
		case "ElementMesh": 	build = EJSS_SVGGRAPHICS.mesh(mInterfaceGraphics,element);		break;			
		case "ElementTank":   	build = EJSS_SVGGRAPHICS.tank(mInterfaceGraphics,element);		break;
		case "ElementPipe":   	build = EJSS_SVGGRAPHICS.pipe(mInterfaceGraphics,element);		break;
		case "ElementHistogram":build = EJSS_SVGGRAPHICS.histogram(mInterfaceGraphics,element);		break;

		case "ElementScalarField":	build = EJSS_SVGGRAPHICS.scalarField(mInterfaceGraphics,element);		break;
		case "ElementCanvas":	build = EJSS_SVGGRAPHICS.canvas(mInterfaceGraphics,element);		break;
	}
	if(build) {
		if (!element.isGroupVisible()) 
		   build.setAttribute("visibility","hidden");
		else
	       build.setAttribute("visibility","visible");
	}			

  	return build;
  };

  /**
   * Transform Element
   * @param element
   */
  self.transformElement = function (element, build) {
	var trstr = "";
	var trans = element.getTransformation();
	var rot = element.getRotate();
	var pos = element.getPixelPosition();	

    if (trans.length > 0) {  // matrix
    	trstr = "translate(" + pos[0] + " " + pos[1] + ")";
		trstr += "matrix(" + trans[0] + " " + trans[1] + " " + trans[2] + " " +
		 			trans[3] + " " + trans[4] + " " + trans[5] + ")";  	
    	trstr += "translate(-" + pos[0] + " -" + pos[1] + ")";
		// build.setAttribute("transform-origin", pos[0] + " " + pos[1]); // not supported in Safari and Mozilla
	}
	if (rot != 0) {  // rotation angle 	
	    var angle = EJSS_TOOLS.Mathematics.degrees(-rot);	
		trstr += " rotate(" + angle + " " + pos[0] + " " + pos[1]+ ")";			
	}	  
    //if (trstr.length>0) 
    	build.setAttribute("transform",trstr);
  }

  /**
   * Draw gutters
   * @param gutters
   */
  self.drawGutters = function(panel) {
  	var gutters = panel.getGutters();
  	
  	if (gutters.visible) {    	 			
	 	var UTILS = EJSS_SVGGRAPHICS.Utils;  	   	    

		var mShape = mInterfaceGraphics.getElementById(".myGutters");
		var mOuterBorder = mInterfaceGraphics.getElementById(".myOuterBorder");
		var mInnerBorder = mInterfaceGraphics.getElementById(".myInnerBorder");
		if (mShape === null) { 	// exits?
		    // create SVG element
		    mShape = document.createElementNS("http://www.w3.org/2000/svg","path"); 
		    mShape.setAttribute("id", ".myGutters");
		    mShape.setAttribute("stroke","none");
	    	mShape.setAttribute("stroke-width",0);    	
		    mInterfaceGraphics.appendChild(mShape);	    

		    mOuterBorder = document.createElementNS("http://www.w3.org/2000/svg","path"); 
		    mOuterBorder.setAttribute("id", ".myOuterBorder");
		    mOuterBorder.setAttribute("fill","none");    
		    mInterfaceGraphics.appendChild(mOuterBorder);	    

		    mInnerBorder = document.createElementNS("http://www.w3.org/2000/svg","path"); 
		    mInnerBorder.setAttribute("id", ".myInnerBorder");
		    mInnerBorder.setAttribute("fill","none");    
		    mInterfaceGraphics.appendChild(mInnerBorder);	
		        
		}

	 	// get gutters position    
	    var inner = panel.getInnerRect(); 

	 	// get panel position
	    var x = 0.5, y = 0.5;
	    var box = self.getBox();
    	var sx = box.width, sy = box.height;

	 	// create paths	    	   	    
	 	var guttersStyle = panel.getGuttersStyle();	 
	 	var panelStyle = panel.getStyle();   
	    if(panelStyle.getShapeRendering() == "crispEdges") {
		    mShape.setAttribute('d', 
		    	 "M " + UTILS.crispValue(x) + " " + UTILS.crispValue(y) + 
		    	" L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y+sy) + 
		    	" L " + UTILS.crispValue(x+sx) + " " + UTILS.crispValue(y+sy) + 
		    	" L " + UTILS.crispValue(x+sx) + " " + UTILS.crispValue(y) +
		    	" L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y) + 
		    	" M " + UTILS.crispValue(inner.x) + " " + UTILS.crispValue(inner.y) + 
		    	" L " + UTILS.crispValue(inner.x+inner.width) + " " + UTILS.crispValue(inner.y) + 
		    	" L " + UTILS.crispValue(inner.x+inner.width) + " " + UTILS.crispValue(inner.y+inner.height) + 
		    	" L " + UTILS.crispValue(inner.x) + " " + UTILS.crispValue(inner.y+inner.height) + 
		    	" L " + UTILS.crispValue(inner.x) + " " + UTILS.crispValue(inner.y) + " z");
		    mOuterBorder.setAttribute('d', 
		    	 "M " + UTILS.crispValue(x) + " " + UTILS.crispValue(y) + 
		    	" L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y+sy) + 
		    	" L " + UTILS.crispValue(x+sx) + " " + UTILS.crispValue(y+sy) + 
		    	" L " + UTILS.crispValue(x+sx) + " " + UTILS.crispValue(y) +
		    	" L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y) + " z");
		    mInnerBorder.setAttribute('d', 
		    	 "M " + UTILS.crispValue(inner.x) + " " + UTILS.crispValue(inner.y) + 
		    	" L " + UTILS.crispValue(inner.x+inner.width) + " " + UTILS.crispValue(inner.y) + 
		    	" L " + UTILS.crispValue(inner.x+inner.width) + " " + UTILS.crispValue(inner.y+inner.height) + 
		    	" L " + UTILS.crispValue(inner.x) + " " + UTILS.crispValue(inner.y+inner.height) + 
		    	" L " + UTILS.crispValue(inner.x) + " " + UTILS.crispValue(inner.y) + " z");
	    } else {
		    mShape.setAttribute('d', 
		    	 "M " + (x) + " " + (y) + 
		    	" L " + (x) + " " + (y+sy) + 
		    	" L " + (x+sx) + " " + (y+sy) + 
		    	" L " + (x+sx) + " " + (y) +
		    	" L " + (x) + " " + (y) + 
		    	" M " + (inner.x) + " " + (inner.y) + 
		    	" L " + (inner.x+inner.width) + " " + (inner.y) + 
		    	" L " + (inner.x+inner.width) + " " + (inner.y+inner.height) + 
		    	" L " + (inner.x) + " " + (inner.y+inner.height) + 
		    	" L " + (inner.x) + " " + (inner.y) + " z");
		    mOuterBorder.setAttribute('d', 
		    	 "M " + (x) + " " + (y) + 
		    	" L " + (x) + " " + (y+sy) + 
		    	" L " + (x+sx) + " " + (y+sy) + 
		    	" L " + (x+sx) + " " + (y) +
		    	" L " + (x) + " " + (y) + " z");
		    mInnerBorder.setAttribute('d',
		    	"M " + (inner.x) + " " + (inner.y) + 
		    	" L " + (inner.x+inner.width) + " " + (inner.y) + 
		    	" L " + (inner.x+inner.width) + " " + (inner.y+inner.height) + 
		    	" L " + (inner.x) + " " + (inner.y+inner.height) + 
		    	" L " + (inner.x) + " " + (inner.y) + " z");
		}  	    
		
		// set style (appling into gutters, i.e. panel, so panel style)
	    if(guttersStyle.getDrawFill())	
	    	mShape.setAttribute("fill",guttersStyle.getFillColor());
	    else 
	    	mShape.setAttribute("fill","none");    
	    if(guttersStyle.getDrawLines()) {
	    	mOuterBorder.setAttribute("stroke",guttersStyle.getLineColor());
	    	mOuterBorder.setAttribute("stroke-width",guttersStyle.getLineWidth());
	    } else {
	    	mOuterBorder.setAttribute("stroke","none");
	    	mOuterBorder.setAttribute("stroke-width",0);    	
	    }        
	    if(panelStyle.getDrawLines()) {
	    	mInnerBorder.setAttribute("stroke",panelStyle.getLineColor());
	    	mInnerBorder.setAttribute("stroke-width",panelStyle.getLineWidth());
	    } else {
	    	mInnerBorder.setAttribute("stroke","none");
	    	mInnerBorder.setAttribute("stroke-width",0);    	
	    }        
		mShape.setAttribute("shapeRendering",panelStyle.getShapeRendering());
		mOuterBorder.setAttribute("shapeRendering",guttersStyle.getShapeRendering());
		mInnerBorder.setAttribute("shapeRendering",panelStyle.getShapeRendering());
	}  	
  };
   /**
   * Draw panel
   * @param panel
   */
  self.drawPanel = function(panel) {
 	var UTILS = EJSS_SVGGRAPHICS.Utils;   

	var mShape = mInterfaceGraphics.getElementById(".myPanel");
	if (mShape === null) { 	// exits?
	    // create SVG element
	    mShape = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mShape.setAttribute("id", ".myPanel");
	    mInterfaceGraphics.appendChild(mShape);	    
	}

 	// get position
    var x = 0.5, y = 0.5;
    var box = self.getBox();
    var sx = box.width, sy = box.height;

 	// create path	    	   	    
 	var style = panel.getStyle();	    	   	   
    if(style.getShapeRendering() == "crispEdges") {
	    mShape.setAttribute('d', 
	    	 "M " + UTILS.crispValue(x) + " " + UTILS.crispValue(y) + 
	    	" L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y+sy) + 
	    	" L " + UTILS.crispValue(x+sx) + " " + UTILS.crispValue(y+sy) + 
	    	" L " + UTILS.crispValue(x+sx) + " " + UTILS.crispValue(y) + " z");
    } else {
	    mShape.setAttribute('d', 
	    	 "M " + (x) + " " + (y) + 
	    	" L " + (x) + " " + (y+sy) + 
	    	" L " + (x+sx) + " " + (y+sy) + 
	    	" L " + (x+sx) + " " + (y) + " z");
	}  	    
	
	// set style (appling into gutters, i.e. panel, so panel style)
    if(style.getDrawFill())	
    	mShape.setAttribute("fill",style.getFillColor());
    else 
    	mShape.setAttribute("fill","none");
    	
    if(style.getDrawLines()) {
    	mShape.setAttribute("stroke",style.getLineColor());
    	mShape.setAttribute("stroke-width",style.getLineWidth());
    } else {
    	mShape.setAttribute("stroke","none");
    	mShape.setAttribute("stroke-width",0);    	
    }        

	mShape.setAttribute("shapeRendering",style.getShapeRendering());
  };
   
  self.importSVG = function(callback) {
    var box = self.getBox();

	// canvas
    var canvas = document.createElement("canvas");
    canvas.width = box.width+1;
    canvas.height = box.height+1;
    var ctx = canvas.getContext("2d");

	// fix size
  	var dom = self.getDOMElement();
  	dom.setAttribute("width", canvas.width);
  	dom.setAttribute("height", canvas.height);
    var svg_xml = (new XMLSerializer()).serializeToString(dom);    

    var img = new Image();
//    img.src = "data:image/svg+xml;base64," + btoa(svg_xml);
    img.src = "data:image/svg+xml;utf8," + svg_xml;

    img.onload = function() {
    	console.log("tam " + canvas.width + " " + canvas.height);
        ctx.drawImage(img,0,0);
        if(callback) callback(canvas.toDataURL("image/png"));
    } 
    return img.src;
  }
    
 return self;     
      
}
