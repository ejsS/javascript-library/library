/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas pipe
 */
EJSS_CANVASGRAPHICS.pipe = function(mContext, mElement) {  
    var points = mElement.getPoints();
    if (!points || points.length<=0) return;  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];

	
	// get half sizes 		
    var sx = Math.abs(size[0]);
    var sy = Math.abs(size[1]);

 	var xmin = x-sx/2;
 	var ymin = y+sy/2;

    var style = mElement.getStyle(); 

	// draw container
    mContext.beginPath();
    mContext.moveTo(xmin,ymin);
    var x = xmin;
    var y = ymin; 
    for (var i=0, n=points.length; i<n; i++) {
      var point = points[i];
      x += Math.round(point[0]*sx);
      y += Math.round(-point[1]*sy);
      mContext.lineTo(x,y);
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = 2*style.getLineWidth()+mElement.getPipeWidth();
	  mContext.strokeStyle = style.getLineColor();
	  mContext.stroke();
    }

	// draw liquid
    mContext.beginPath();
    mContext.moveTo(xmin,ymin);
    var x = xmin;
    var y = ymin;
    for (var i=0, n=points.length; i<n; i++) {
      var point = points[i];
      x += Math.round(point[0]*sx);
      y += Math.round(-point[1]*sy);
      mContext.lineTo(x,y);
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = mElement.getPipeWidth();
	  mContext.strokeStyle = style.getFillColor();
	  mContext.stroke();
    }    
}