/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas histogram
 */
EJSS_CANVASGRAPHICS.histogram = function(mContext, mElement) {  

	// draw area bar	
	function areaBar(barx1, barx2, bary, axis, barstyle) {		
		mContext.beginPath();
		mContext.moveTo(barx1,axis);
		mContext.lineTo(barx1,bary);
		mContext.lineTo(barx2,bary);
		mContext.lineTo(barx2,axis);

		// set style
		if (barstyle.getDrawFill() && barstyle.getFillColor() != 'none') {
		  mContext.fillStyle = barstyle.getFillColor();
		  mContext.fill();
		}
		if (barstyle.getDrawLines()) {
		  mContext.lineWidth = barstyle.getLineWidth();
		  mContext.strokeStyle = barstyle.getLineColor();
		  mContext.stroke();
		}		
	}  	

	// get position of the element center 	
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
	
	// get style        
	var style = mElement.getStyle(); 
        
	// get histogram parameters
	var discrete =  mElement.getDiscrete();
 	var halfBin = mElement.getBinWidth()/2;
 	var bars = mElement.getBars();

	var axisx = mElement.getGroupPanel().getPixelPositionWorldOrigin()[1];
	for (var j=0; j < bars.length; j++) { // create bars
	  var barx1, barx2;
	  var cbin = bars[j][0];
	  if (discrete) {
        barx1 = barx2 = (x-mx) + cbin*size[0];
	  } 
	  else {
		barx1 = (x-mx) + (cbin-halfBin)*size[0];	// Bar x
		barx2 = (x-mx) + (cbin+halfBin)*size[0];	// Bar x
	  }
	  var bary = (y-my) + bars[j][1]*size[1];  // Bar y
	  areaBar(barx1, barx2, bary, axisx, style);
    }
}