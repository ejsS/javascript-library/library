/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas point
 */
EJSS_CANVASGRAPHICS.point = function(mContext, mElement) {  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var xm = pos[0]+offset[0];
    var ym = pos[1]+offset[1];
	
    // draw
  	mContext.beginPath();
  	mContext.arc(xm,ym,2,0,2*Math.PI);
      
	// set style
    var style = mElement.getStyle(); 
    mContext.fillStyle = style.getFillColor();
    mContext.fill();
}