/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas cursor
 */
EJSS_CANVASGRAPHICS.cursor = function(mContext, mElement) {  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    
  	// type of cursor
	var cursorType = mElement.getCursorType();

    // cursor size
    var panel = mElement.getGroupPanel();
    var bounds = panel.getRealWorldCoordinates(); // xmin,xmax,ymin,ymax
	var bottomLeft = panel.toPixelPosition([bounds[0],bounds[2]]);    
	var topRight   = panel.toPixelPosition([bounds[1],bounds[3]]);    

  	mContext.beginPath();	    
	if (cursorType!=EJSS_DRAWING2D.Cursor.VERTICAL) {
	  var x1 = bottomLeft[0], x2 = topRight[0];
	  var y = pos[1];
	  // keep the cursor inside the panel
	  if (y>bottomLeft[1]) y = bottomLeft[1];
	  else if (y<topRight[1]) y = topRight[1];
	  
	  mContext.moveTo(x1,y);
	  mContext.lineTo(x2,y);
	}

	if (cursorType!=EJSS_DRAWING2D.Cursor.HORIZONTAL) {
	  var x = pos[0];
	  var y1 = bottomLeft[1], y2 = topRight[1];
	  // keep the cursor inside the panel
	  if (x<bottomLeft[0]) x = bottomLeft[0];
	  else if (x>topRight[0]) x = topRight[0];
	  
	  mContext.moveTo(x,y1);
	  mContext.lineTo(x,y2);
	}
      
	// set style
    var style = mElement.getStyle(); 
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
}