/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas cellLattice
 */
EJSS_CANVASGRAPHICS.cellLattice = function(mContext, mElement) {  

	function drawGrid(left, top, right, bottom, stepx, stepy) {
		mContext.beginPath();
		
		// vertical lines
		if (stepx == 0) stepx = Math.abs(right-left);
	    for (var i = left; i <= right; i = i+stepx) {
	  	  mContext.moveTo(i,top);
	  	  mContext.lineTo(i,bottom); 
	    }
	    // horizontal lines
	    if (stepy == 0) stepy = Math.abs(top-bottom);
	    for (var i = bottom; i >= top; i = i-stepy) {
	  	  mContext.moveTo(left,i);
	  	  mContext.lineTo(right,i); 
	    }
	}  
	
	function rectCell(x, y, sx, sy, fill) {
		mContext.beginPath();
			    		
		var mx = sx/2, my = sy/2;
		mContext.moveTo(x-mx,y+my);
		mContext.lineTo(x+mx,y+my);
		mContext.lineTo(x+mx,y-my);
		mContext.lineTo(x-mx,y-my);
		mContext.lineTo(x-mx,y+my);
		
		mContext.fillStyle = fill;
        mContext.fill();
      	mContext.lineWidth = 1;
     	mContext.strokeStyle = fill;
     	mContext.stroke();
	}

	function gridCell(x, y, sx, sy, stepx, stepy, style) {
		// set attributes
		var mx = sx/2, my = sy/2;    	   	    
		var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
		drawGrid(left, top, right, bottom, stepx, stepy);
						
		// set style
		if (style.getDrawFill() && style.getFillColor() != 'none') {
	      mContext.fillStyle = style.getFillColor();
	      mContext.fill();
	    }
	    if (style.getDrawLines()) {
	      mContext.lineWidth = style.getLineWidth();
	      mContext.strokeStyle = style.getLineColor();
	      mContext.stroke();
	    }	
	}

	// get position of the mElement center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;

	// draw cells
	var style = mElement.getStyle();     
	var data = mElement.getData();
	var colors = mElement.getColorMapper().getColors();
	var xlen = data.length;
	var ylen = data[0].length;

    var stepx = Math.abs(size[0]/xlen);
    var stepy = Math.abs(size[1]/ylen);
   	
  	var left = x-mx+stepx/2, bottom = y-my-stepy/2;		
  	for(var i=0; i<ylen; i++) { 
  		for(var j=0; j<xlen; j++) {
  			// draw rectangle
  			rectCell(left+stepx*j, bottom-stepy*i, stepx, stepy, colors[data[j][i]]);
  		}  		
  	}
  	 	
  	// draw grid
  	var showGrid = mElement.getShowGrid();
	if (showGrid) gridCell(x, y, size[0], size[1], stepx, stepy, style);	
}