/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas tank
 */
EJSS_CANVASGRAPHICS.tank = function(mContext, mElement) {  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);

    var level = mElement.getPixelSizeOf(0,mElement.getLevel());  

 	var xmin = x-mx, xmax = x+mx;
 	var ymin = y+my, ymax = y-my;
 	var ylevel = ymin + level[1];
    var style = mElement.getStyle();
    
    // draw liquid
  	mContext.beginPath();
    mContext.moveTo(xmin + level[0],ylevel);
    mContext.lineTo(xmin,ymin);
    mContext.lineTo(xmax,ymin);
    mContext.lineTo(xmax + level[0],ylevel);
    mContext.fillStyle = mElement.getLevelColor();
    mContext.fill();

	// draw container
	mContext.beginPath();
	mContext.moveTo(xmin,ymax);
	mContext.lineTo(xmin,ymin);
	mContext.lineTo(xmax,ymin);
	mContext.lineTo(xmax,ymax);
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    mContext.lineWidth = style.getLineWidth();
    mContext.strokeStyle = style.getLineColor();
    mContext.stroke();
}