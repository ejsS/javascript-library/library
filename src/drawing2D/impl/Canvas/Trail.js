/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas trail
 */
EJSS_CANVASGRAPHICS.trail = function(mContext, mElement) {  

  /** adds a segment to the trail
   */
   function addSegment(points,num,style,pX,pY,sX,sY) {
     if (num<=0) return; // Nothing to add
	 	 	
	mContext.beginPath();
	for (var i=0; i<num; i++) {
	  var point = points[i];	  
	  var xx = pX + point[0]*sX;
	  var yy = pY + point[1]*sY;
	  var type = point[2];		
	  if ((i==0) || (type == 0)) // 0 is NOT CONNECTION
	    mContext.moveTo(xx,yy);
      else
        mContext.lineTo(xx,yy);       	
 	}  	  	

/*
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
*/
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
   }

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
	
	var pX = x-mx;
	var pY = y-my;
	var sX = size[0];
	var sY = size[1];
	
	var segmentCount = mElement.getSegmentsCount();
	if (segmentCount>0) {
	  for (var i=0; i<segmentCount; i++) { 
	  	var num = mElement.getSegmentPoints(i).length;
	    addSegment(mElement.getSegmentPoints(i),num,mElement.getSegmentStyle(i),pX,pY,sX,sY);
	  }
	}
	
	//mElement.dataCollected(); // add temporary points
	var num = mElement.getCurrentPoints().length; 
	addSegment(mElement.getCurrentPoints(),num,mElement.getStyle(),pX,pY,sX,sY);
	
}