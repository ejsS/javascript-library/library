/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas image
 */
EJSS_CANVASGRAPHICS.image = function(mContext, mElement, transformCallBack) {  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];

	// get sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);
    
    // draw
    var img;
    if (!mElement.getChangedImage()) img = mElement.getCustomObject();
    if (img) {
		mContext.save();
		transformCallBack(mElement);
    	mContext.drawImage(img, (x-mx), (y-my), Math.abs(size[0]), Math.abs(size[1]));
		mContext.restore();
    }
    else{
    	img = new Image();
    	mElement.setCustomObject(img);
  	    img.onload = function () {
    			mContext.save();
    			transformCallBack(mElement);
    	    	mContext.drawImage(img, (x-mx), (y-my), Math.abs(size[0]), Math.abs(size[1]));
    			mContext.restore();
    	};
	 	var code = mElement.getEncode();
	 	if(code.length > 0) { // http://webcodertools.com/imagetobase64converter/Create
	 		img.src = 'data:png;base64,'+ code;
	 	} else {
	    	img.src = mElement.getImageUrl();
	    }
    }

    // indica que la fuente de la imagen ha sido actualizada  
    mElement.setChangedImage(false);
}  
