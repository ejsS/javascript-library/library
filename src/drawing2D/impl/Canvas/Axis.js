/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas axis
 */
EJSS_CANVASGRAPHICS.axis = function(mContext, mElement) {  

	// draw axis line
	function drawLine(x, y, mx, my, orient, inverted) {
		if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {	// vertical axis
			if(inverted) {
				my = -my;
				y = -y;	
			}
			mContext.moveTo(x,y-my);
			mContext.lineTo(x,y+my);
		} else {	// horizontal axis
			mContext.moveTo(x-mx,y);
			mContext.lineTo(x+mx,y);
		}		
	}

	function drawTick(x, y, ticksize, orient) {
		if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {	// vertical axis
			mContext.moveTo((x-ticksize/2),y);
			mContext.lineTo((x+ticksize/2),y);
		} else {
			mContext.moveTo(x,(y-ticksize/2));
			mContext.lineTo(x,(y+ticksize/2));
		}					
	}
	
	// text for tick in position (x,y) with font and mode
	function tickText (x, y, text, font, horizontal) {
		mContext.save();

	 	var font = mElement.getFont();
	 	var fondtxt = "";
	 	fondtxt += (font.getFontStyle() != 'none')? font.getFontStyle() + ' ':'';
	 	fondtxt += (font.getFontWeight() != 'none')? font.getFontWeight() + ' ':'';
	 	fondtxt += font.getFontSizeString() + 'px ';
	 	fondtxt += font.getFontFamily();
		mContext.font = fondtxt;	
		mContext.fillStyle = font.getFillColor();

		if (horizontal) {
		  x -= mContext.measureText(text).width/2 + 0.5;
		}	
		else { // vertical axis
		  y += font.getFontSize()/2; // - 0.5;
		}

		mContext.fillText(text, x, y);		
				
		mContext.restore();		    
	}		
	
	if(mElement.getShow()) { // show

		// get position of the element center 
	    var pos = mElement.getPixelPosition();
	    var size = mElement.getPixelSizes();     
	    var offset = mElement.getRelativePositionOffset(size);  
	    var x = pos[0]+offset[0];
	    var y = pos[1]+offset[1];
		
		// get sizes 		
	    var mx = size[0]/2;
	    var my = size[1]/2;
				
		// properties	    
	    var style = mElement.getStyle();		// element style   	 	
		var orient = mElement.getOrient();		// axis orientation (vertical or horizontal)
		var inverted = mElement.getInvertedScaleY();
	    
	    // draw the line for axis
	  	mContext.beginPath();
		drawLine(x, y, mx, my, orient, inverted);
		 		
		// get axis size in pixel
		var segsize = (orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL)? Math.abs(size[1]):Math.abs(size[0]);		
		 		
		// draw axis (based on ticks mode)
		var ticksmode = mElement.getTicksMode();			// decimal or logarithmic
		if (ticksmode == EJSS_DRAWING2D.Axis.SCALE_LOG) { // logarithmic

		    var scale = mElement.getScale();		// axis scale 			
			if(scale[0] > 0 && scale[1] > 0) {  // valid scale			
				// get number of axis ticks
				var ticks = mElement.getTicks(); 	
	
			    // scale
				var scalePrecision = mElement.getScalePrecision();	// number of decimals for text
				
				// draw ticks 
				var ticksize = mElement.getTicksSize();				// ticks size in pixels
				EJSS_GRAPHICS.GraphicsUtils.drawLogTicks(x, y, mx, my, segsize, ticksize, scale, ticks, orient, drawTick);		
			
				// draw ticks text 
				var font = mElement.getFont();
			    var textPosition = mElement.getTextPosition();		// text position (UP or DOWN)
				EJSS_GRAPHICS.GraphicsUtils.drawLogTicksText (x, y, mx, my, ticks, ticksize, scale, scalePrecision, font, textPosition, orient, tickText)			
			}
			
		} else if (ticksmode == EJSS_DRAWING2D.Axis.SCALE_NUM) { // decimal
			 			 		
			// calculate step in pixels	
		    var step = 0;     // step for ticks (pixels)
		    var tickstep = 0; // step for ticks (real units)
			if (!mElement.getAutoTicks()) {		// no auto-ticks
		    	var ticks = mElement.getTicks();	// number of ticks
				// whether the number of ticks exits, changes step for ticks and scale 
			    if (ticks != 0) { step = segsize/ticks; } else {
			    	step = mElement.getStep();
			    	tickstep = mElement.getTickStep(); 
			    } 	    	
			} else {	// auto-ticks
				var stepmin = mElement.getAutoStepMin();		// step min in pixels
				var ticksrange = mElement.getAutoTicksRange();	// ticks range
				// find step based on ticks range
				for(var i=ticksrange.length-1; i>=0; i--)	{	
					step = Math.abs(segsize/ticksrange[i]);
					if (step*1.001 >= stepmin) break;
				}
			}
		    
			var scalePrecision = mElement.getScalePrecision();	// number of decimals for text
			var scale = mElement.getScale();		// axis scale 	
			// values for scale
		    if(tickstep == 0) {
				var scalestep = Math.abs((scale[1] - scale[0]) * step / segsize);  // step in axis scale
			} else {
				var scalestep = tickstep;
				step = Math.abs((scalestep * segsize) / (scale[1] - scale[0])); 
			}			
			
			// adjust step to decimals of precision
			var decimals = Math.pow(10,scalePrecision);
			var scalestepTmp = Math.round(scalestep * decimals) / decimals;
			if(scalestepTmp > 0) {
				scalestep = scalestepTmp; 
				step = Math.abs(scalestepTmp * segsize / (scale[1] - scale[0]));
			}
			
			// check fixed tick
			var fixedTicks = mElement.getFixedTick();	  
			var tickfixed = scale[1];
			if (!isNaN(fixedTicks)) {
			  if (fixedTicks < scale[0]) tickfixed = fixedTicks + (Math.floor((scale[0]-fixedTicks)/scalestep)+1)*scalestep;
			  else if (fixedTicks > scale[1])  tickfixed = fixedTicks - (Math.floor((fixedTicks-scale[1])/scalestep)+1)*scalestep;
		      else tickfixed = fixedTicks; 
			}		
	
			// tick fixed in axis scale
			var scaleshift = Math.abs((scale[0] - tickfixed) % scalestep);						
			var dist = Math.abs(scaleshift-scalestep);	// fitting shift
			if(scaleshift < 0.001 || dist < 0.001) scaleshift = 0;						
			var shift = segsize * scaleshift / Math.abs(scale[1] - scale[0]);	// shift in pixels				
										    		
			// draw ticks based on step and shift
			var ticksize = mElement.getTicksSize();				// ticks size in pixels
			EJSS_GRAPHICS.GraphicsUtils.drawDecTicks(x, y, mx, my, ticksize, step, shift, orient, inverted, drawTick);		
		
			// draw ticks text based on scaleshift, scalestep and scale
			var font = mElement.getFont();
		    var textPosition = mElement.getTextPosition();		// text position (UP or DOWN)
			EJSS_GRAPHICS.GraphicsUtils.drawDecTicksText (x, y, mx, my, ticksize, step, shift, scale, scalePrecision, scalestep, scaleshift, font, textPosition, orient, inverted, tickText)
								
		}

		// set style
	    var style = mElement.getStyle(); 
		if (style.getDrawFill() && style.getFillColor() != 'none') {
	      mContext.fillStyle = style.getFillColor();
	      mContext.fill();
	    }
	    if (style.getDrawLines()) {
	      mContext.lineWidth = style.getLineWidth();
	      mContext.strokeStyle = style.getLineColor();
	      mContext.stroke();
	    }						
	}

}