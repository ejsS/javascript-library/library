/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas wheel
 */
EJSS_CANVASGRAPHICS.wheel = function(mContext, mElement) {  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var xm = pos[0]+offset[0];
    var ym = pos[1]+offset[1];
	
	// get sizes 		
    var w = Math.abs(size[0]);
    var h = Math.abs(size[1]);

	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
    
    // draw
    var kappa = .5522848,
      // x = x-middle, y = y-middle
      x = xm - w / 2,       // x-middle
      y = ym - h / 2;       // y-middle      
      ox = (w / 2) * kappa, // control point offset horizontal
      oy = (h / 2) * kappa, // control point offset vertical
      xe = x + w,           // x-end
      ye = y + h,           // y-end

  	mContext.beginPath();
	// set circle atts
  	mContext.moveTo(x, ym);
  	mContext.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
  	mContext.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
  	mContext.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
  	mContext.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
      
	// set cross atts
	mContext.moveTo(xm-mx,ym);
	mContext.lineTo(xm+mx,ym);
	mContext.moveTo(xm,ym-my);
	mContext.lineTo(xm,ym+my);
	
	// set style
    var style = mElement.getStyle(); 
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
}