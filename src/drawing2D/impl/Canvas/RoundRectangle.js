/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas round rectangle
 */
EJSS_CANVASGRAPHICS.roundRectangle = function(mContext, mElement) {  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var mx = pos[0]+offset[0];
    var my = pos[1]+offset[1];
	
	// get sizes 		
    var width = Math.abs(size[0]);
    var height = Math.abs(size[1]);
    var x = mx - width / 2;
    var y = my - height / 2;

    // get element radius
    var radius = Math.abs(mElement.getCornerRadius());
    
    // draw
    mContext.beginPath();
    mContext.moveTo(x + radius, y);
    mContext.lineTo(x + width - radius, y);
    mContext.quadraticCurveTo(x + width, y, x + width, y + radius);
    mContext.lineTo(x + width, y + height - radius);
    mContext.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    mContext.lineTo(x + radius, y + height);
    mContext.quadraticCurveTo(x, y + height, x, y + height - radius);
    mContext.lineTo(x, y + radius);
    mContext.quadraticCurveTo(x, y, x + radius, y);
    mContext.closePath();
      
	// set style
    var style = mElement.getStyle(); 
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
}