/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas byteRaster
 */
EJSS_CANVASGRAPHICS.byteRaster = function(mContext, mElement) {
	
	if(mElement.getDataChanged()) { // new data or colors
		var data = mElement.getData();
		var colors = mElement.getColorMapper().getColors();			
		var xlen = data.length;
		var ylen = data[0].length;
		var num = mElement.getColorMapper().getNumberOfColors();

		// get position of the mElement center 
	    var pos = mElement.getPixelPosition();
	    var size = mElement.getPixelSizes();     
	    var offset = mElement.getRelativePositionOffset(size);  
	    var x = pos[0]+offset[0];
	    var y = pos[1]+offset[1];
		
		// get half sizes 		
	    var mx = Math.abs(size[0]/2);
	    var my = Math.abs(size[1]/2);    
		
		var idcanvas = mContext.canvas.id;
		
		// using worker
		EJSS_SVGGRAPHICS.Utils.ImageDataCanvas (xlen, ylen, num, data, colors,
			function(img) {
				// I do not know why the reference mContext is lost
				var canvas = document.getElementById(idcanvas);
			  	mContext = canvas.getContext("2d");
				mContext.putImageData(img,x-mx,y-my);
			});			    	    
	    mElement.setDataChanged(false);

	}

}

