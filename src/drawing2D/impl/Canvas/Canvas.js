/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas canvas
 */
EJSS_CANVASGRAPHICS.canvas = function(mContext, mElement) {  
  
	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	var dim = mElement.getDimensions();
  	var deltaX = dim[1] - dim[0];
  	var deltaY = dim[3] - dim[2];
    
	var pixWth = Math.abs(size[0]);
	var pixHgt = Math.abs(size[1]);
	var pxPerUnitX = pixWth / deltaX;
	var pxPerUnitY = pixHgt / deltaY;
	var pxTopLeftX = x - pixWth / 2;
	var pxTopLeftY = y + pixHgt / 2;
   
    //draws all objects within this.drawables
    var drawables = mElement.getDrawables();
    for (var i = 0; i < drawables.length; i++) {
      if(typeof drawables[i].imageField != "undefined")
        EJSS_GRAPHICS.GraphicsUtils.drawImageField(mContext, 
        	pxTopLeftX, pxTopLeftY, xMin, yMin, pixWth, pixHgt, pxPerUnitX, pxPerUnitY, drawables[i]);
      else if(typeof drawables[i].run != "undefined")
      	drawables[i].draw(mContext, pxTopLeftX, pxTopLeftY, xMin, yMin, pixWth, pixHgt, pxPerUnitX, pxPerUnitY);
    }
      
}