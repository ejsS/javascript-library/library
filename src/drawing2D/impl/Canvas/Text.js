/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas text
 */
EJSS_CANVASGRAPHICS.text = function(mContext, mElement) {  
	// font 
 	var font = mElement.getFont();
 	var fondtxt = "";
 	fondtxt += (font.getFontStyle() != 'none')? font.getFontStyle() + ' ':'';
 	fondtxt += (font.getFontWeight() != 'none')? font.getFontWeight() + ' ':'';
 	fondtxt += font.getFontSizeString() + ((font.getFontSizeString().toString().indexOf('px') > -1)?' ':'px ');
 	fondtxt += font.getFontFamily();
	mContext.font = fondtxt;

	// txt 	
 	var txt = mElement.getText(); 	

	// get position of the element center 
    var pos = mElement.getPixelPosition();
	var marginFrame = mElement.getFramed()?4:0; 
    var size = [mContext.measureText(txt).width + marginFrame*2, font.getFontSize() + marginFrame*2];
    var offset = EJSS_DRAWING2D.Element.getSWRelativePositionOffset(mElement.getRelativePosition(), size[0], size[1]);  
    var xmargin = mElement.getMarginX();
    var ymargin = mElement.getMarginY();
    var x = pos[0]+offset[0]+xmargin+marginFrame;
    var y = pos[1]+offset[1]+ymargin-marginFrame;

    if (mElement.getFramed()) { // framed    
    	var tbmargin = marginFrame;
    	var rlmargin = marginFrame;
    	var wlen = size[0] - marginFrame*2;
    	var hlen = size[1] - marginFrame*2;    	
    	
	    // draw box
	    if(wlen > 0) {
	    	var bottom = y + tbmargin, top = y - hlen - tbmargin;
	    	var left = x - rlmargin, rigth = x + wlen + tbmargin;
	    	mContext.save();	 		 
		  	mContext.beginPath();
  			mContext.moveTo(left, bottom);
  			mContext.lineTo(rigth, bottom);
  			mContext.lineTo(rigth, top);
  			mContext.lineTo(left, top);
			mContext.lineTo(left, bottom);

			// set style
		    var style = mElement.getStyle(); 
			if (style.getDrawFill() && style.getFillColor() != 'none') {
		      mContext.fillStyle = style.getFillColor();
		      mContext.fill();
		    }
		    if (style.getDrawLines()) {
		      mContext.lineWidth = style.getLineWidth();
		      mContext.strokeStyle = style.getLineColor();
		      mContext.stroke();
		    }	
		    mContext.restore();
		}
	}
		    
	// angle of text  	 
	var angletext = 0;
	switch(mElement.getWritingMode()) {
		case EJSS_DRAWING2D.Text.MODE_TOPDOWN : angletext = Math.PI / 2; break;
		case EJSS_DRAWING2D.Text.MODE_RIGTHLEFT : angletext = Math.PI; break;
		case EJSS_DRAWING2D.Text.MODE_DOWNTOP : angletext = 3 * Math.PI / 2; break;
		case EJSS_DRAWING2D.Text.MODE_LEFTRIGHT: angletext = 0; break;
	} 	  	  	

  	mContext.translate(pos[0]+xmargin,pos[1]+ymargin);
  	mContext.rotate(angletext);
  	mContext.translate(-(pos[0]+xmargin),-(pos[1]+ymargin));

	// draw text		
	mContext.fillStyle = font.getFillColor();
	mContext.fillText(mElement.getText(), x, y);
}