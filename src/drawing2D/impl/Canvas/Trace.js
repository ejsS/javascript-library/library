/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas trace
 */
EJSS_CANVASGRAPHICS.trace = function(mContext, mElement) {  

	// draw path for trail
	function drawTrail(points, x, y, sx, sy) {
	  	mContext.beginPath();
		for(var i=0; i<points.length; i++) {
		  var point = points[i];	  
	      var xx = x + point[0]*sx;
	      var yy = y + point[1]*sy;
	      var type = point[2];		
		  if ((i==0) || (type == 0)) // 0 is NOT CONNECTION
		  	mContext.moveTo(xx,yy);		// move 
	      else       	
	      	mContext.lineTo(xx,yy);		// line
		}  	  	
	}  	

	// creates ellipse mark
	function ellipseMark(markx, marky, size, markstyle) {
		// get position of the element center 
	    var xm = markx;
	    var ym = marky;
		
		// get sizes 		
	    var w = Math.abs(size[0]);
	    var h = Math.abs(size[1]);
	
	    // draw
	    var kappa = .5522848,
	      // x = x-middle, y = y-middle
	      x = xm - w / 2,       // x-middle
	      y = ym - h / 2;       // y-middle      
	      ox = (w / 2) * kappa, // control point offset horizontal
	      oy = (h / 2) * kappa, // control point offset vertical
	      xe = x + w,           // x-end
	      ye = y + h,           // y-end
	
	  	mContext.beginPath();
	  	mContext.moveTo(x, ym);
	  	mContext.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
	  	mContext.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
	  	mContext.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
	  	mContext.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
	      
		// set style
		if (markstyle.getDrawFill() && markstyle.getFillColor() != 'none') {
	      mContext.fillStyle = markstyle.getFillColor();
	      mContext.fill();
	    }
	    if (markstyle.getDrawLines()) {
	      mContext.lineWidth = markstyle.getLineWidth();
	      mContext.strokeStyle = markstyle.getLineColor();
	      mContext.stroke();
	    }	  	
	}  	

	// creates rectangle mark
	function rectangleMark(markx, marky, size, markstyle) {
		// get position of the element center 
	    var x = markx;
	    var y = marky;
		
		// get sizes 		
	    var sx = Math.abs(size[0]);
	    var sy = Math.abs(size[1]);

	    // draw
	  	mContext.beginPath();
	  	mContext.rect(x-sx/2, y-sy/2, sx, sy);
	      
		// set style
		if (markstyle.getDrawFill() && markstyle.getFillColor() != 'none') {
	      mContext.fillStyle = markstyle.getFillColor();
	      mContext.fill();
	    }
	    if (markstyle.getDrawLines()) {
	      mContext.lineWidth = markstyle.getLineWidth();
	      mContext.strokeStyle = markstyle.getLineColor();
	      mContext.stroke();
	    }	
	}  	

	// creates area mark
	function areaMark(lastmarkx, lastmarky, markx, marky, axis, markstyle) {	
	    // draw
	  	mContext.beginPath();
	  	mContext.moveTo(lastmarkx,axis);
	  	mContext.lineTo(lastmarkx,lastmarky);
	  	mContext.lineTo(markx,marky);
	  	mContext.lineTo(markx,axis);
	
		// set style
		if (markstyle.getDrawFill() && markstyle.getFillColor() != 'none') {
	      mContext.fillStyle = markstyle.getFillColor();
	      mContext.fill();
	    }
	    if (markstyle.getDrawLines()) {
	      mContext.lineWidth = markstyle.getLineWidth();
	      mContext.strokeStyle = markstyle.getLineColor();
	      mContext.stroke();
	    }		
	}  	

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
        
	// draw trail
    drawTrail(mElement.getPoints(), x-mx, y-my, size[0], size[1]);

	// set style
    var style = mElement.getStyle(); 
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
     
	// draw marks     
	var points = mElement.getPoints();
	var lastpointX, lastpointY;	// for mark area
	for(var i=0; i<points.length; i++) {
		var point = points[i];	  
		var markx = (x-mx) + point[0]*size[0];		// mark x
		var marky = (y-my) + point[1]*size[1];     // mark y
	    var markType = point[3];    		// mark type
		var markstyle = point[4];			// mark style
		var marksize = [point[5],point[6]];		// mark size

	    if (markType == EJSS_DRAWING2D.Trace.ELLIPSE) { // circle
			ellipseMark(markx, marky, marksize, markstyle);
	    } else if (markType == EJSS_DRAWING2D.Trace.RECTANGLE) { // rectangle
			rectangleMark(markx, marky, marksize, markstyle);
	    } else if (markType == EJSS_DRAWING2D.Trace.BAR) { // bar
		    // create bar  		    	
			//var axisx = mElement.getGroupPanel().getPixelPositionWorldOrigin()[1];
			var axisx = mElement.getGroupPanel().toPixelPosition([0,0])[1];
			
	    	marksize[1] = axisx-marky;			// bar size
	    	marky = marky + marksize[1]/2;		// new mark y	    	
	    	rectangleMark(markx, marky, marksize, markstyle);
		}
		else if (markType == EJSS_DRAWING2D.Trace.AREA) { // area
	    	if(i!=0) {				  
				// create area
				//var axisx = mElement.getGroupPanel().getPixelPositionWorldOrigin()[1];
				var axisx = mElement.getGroupPanel().toPixelPosition([0,0])[1];
				var lastmarkx = (x-mx) + lastpointX*size[0];		// mark x
				var lastmarky = (y-my) + lastpointY*size[1];     // mark y
				areaMark(mMark, lastmarkx, lastmarky, markx, marky, axisx, markstyle);
	    	} 
	    	lastpointX = point[0];    	
	    	lastpointY = point[1];
	    }		  
	}  	  		  

}