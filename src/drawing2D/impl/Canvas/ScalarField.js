/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas canvas
 */
EJSS_CANVASGRAPHICS.canvas = function(mContext, mElement) {  
  
	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
  	var deltaX = mElement.getMaximumX() - mElement.getMinimumX();
  	var deltaY = mElement.getMaximumY() - mElement.getMinimumY();
    
	var pixWth = Math.abs(size[0]);
	var pixHgt = Math.abs(size[1]);
	var pxPerUnitX = pixWth / deltaX;
	var pxPerUnitY = pixHgt / deltaY;
	var pxTopLeftX = x - pixWth / 2;
	var pxTopLeftY = y + pixHgt / 2;
   
    //draws all objects within this.drawables
    EJSS_GRAPHICS.GraphicsUtils.drawImageField(context, 
    	pxTopLeftX, pxTopLeftY, mElement.getMinimumX(), mElement.getMinimumY(), 
    	pixWth, pixHgt, pxPerUnitX, pxPerUnitY, {data: mElement.getData()});
    
}