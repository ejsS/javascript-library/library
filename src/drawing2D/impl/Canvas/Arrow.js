/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas arrow
 */
EJSS_CANVASGRAPHICS.arrow = function(mContext, mElement) {  
	var Arrow = EJSS_DRAWING2D.Arrow;

	function createMarkDef(px, py, mark, width, height, stroke, color, sizex, sizey) {
		mContext.save();	
		// measure
		width /= 2;
		height /= 2;
		var angle = (sizex==0)? Math.PI/2 : Math.atan(sizey/sizex);
		angle += ((sizex < 0)?-Math.PI/2:Math.PI/2);
		switch(mark) {
			case Arrow.TRIANGLE:
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(0,height/2);
	            mContext.lineTo(width,height/2);
	            mContext.lineTo(0,-height/2);
	            mContext.lineTo(-width,height/2);
	            mContext.closePath();           
				break;			
			case Arrow.ANGLE:
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(width,height);
	            mContext.lineTo(0,0);
	            mContext.lineTo(-width,height);
				break;			
			case Arrow.LINE:
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(width,0);
	            mContext.lineTo(-width,0);
				break;			
			case Arrow.RECTANGLE:
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(0,-height);
	            mContext.lineTo(width,-height);
	            mContext.lineTo(width,height);
	            mContext.lineTo(-width,height);
	            mContext.lineTo(-width,-height);
	            mContext.closePath();
				break;			
			case Arrow.POINTED:
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(0,height/4);
	            mContext.lineTo(width,height);
	            mContext.lineTo(0,-height/4);
	            mContext.lineTo(-width,height);
	            mContext.closePath();   	            	       
				break;
			case Arrow.CIRCLE:
				var size = (width < height) ? width : height;
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.arc(0, 0, size, 0, 2 * Math.PI, false);	            
				break;
			case Arrow.DIAMOND:
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(0,-height);
	            mContext.lineTo(width,0);
	            mContext.lineTo(0,height);
	            mContext.lineTo(-width,0);
	            mContext.closePath();
				break;				
			case Arrow.INVTRIANGLE:					
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(0,-height/2);
	            mContext.lineTo(-width,-height/2);
	            mContext.lineTo(0,height/2);
	            mContext.lineTo(width,-height/2);
	            mContext.closePath();           
				break;			
			case Arrow.INVANGLE:
	            mContext.beginPath();
	            mContext.translate(px,py);
	            mContext.rotate(angle);
	            mContext.moveTo(-width,-height);
	            mContext.lineTo(0,0);
	            mContext.lineTo(width,-height);
				break;	
										
			case Arrow.WEDGE:
			case Arrow.CURVE:
				console.log('Type of Arrow: not supported');
				break;								
		}		
		
		// set style
		if (color != 'none') {
			if(mark != Arrow.ANGLE && mark != Arrow.INVANGLE) {
				mContext.fillStyle = color;
		    	mContext.fill();
			}
			if(mark != Arrow.POINTED) {
		      	mContext.lineWidth = stroke;
			    mContext.strokeStyle = color;
			    mContext.stroke();		
			}
	    }
	    mContext.restore();
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;

    // draw
	if((size[0] != 0) || (size[1] != 0)) {
		// draw line
	  	mContext.beginPath();
		mContext.moveTo(x-mx, y-my);
	    mContext.lineTo(x+mx, y+my);
	
		// set style
	    var style = mElement.getStyle(); 
		if (style.getDrawFill()  && style.getFillColor() != 'none') {
	      mContext.fillStyle = style.getFillColor();
	      mContext.fill();
	    }
	    if (style.getDrawLines()) {
	      mContext.lineWidth = style.getLineWidth();
	      mContext.strokeStyle = style.getLineColor();
	      mContext.stroke();
	    }	

		// create end mark	
		if(mElement.getMarkEnd() != Arrow.NONE) {
			var endMax = (mElement.getMarkProportion() * EJSS_TOOLS.Mathematics.norm([size[0],size[1]])) ;
			var endW = (endMax != 0 && endMax < mElement.getMarkEndWidth()) ? endMax : mElement.getMarkEndWidth();
			var endH = (endMax != 0 && endMax < mElement.getMarkEndHeight()) ? endMax : mElement.getMarkEndHeight();
			var color = (mElement.getMarkEndColor() == "none") ? mElement.getStyle().getLineColor() : mElement.getMarkEndColor();
			var stroke = (mElement.getMarkEndStroke() < 0) ? mElement.getStyle().getLineWidth() : mElement.getMarkEndStroke();   

			// mElement.getMarkEndOrient() is ignored
			createMarkDef(x+mx, y+my, mElement.getMarkEnd(), endW, endH, stroke, color, mx, my);
		}
				
		// create mid mark
		if(mElement.getMarkMiddle() != Arrow.NONE) {
			var midMax = (mElement.getMarkProportion() * EJSS_TOOLS.Mathematics.norm([size[0],size[1]])) ;
			var midW = (midMax != 0 && midMax < mElement.getMarkMiddleWidth()) ? midMax : mElement.getMarkMiddleWidth();
			var midH = (midMax != 0 && midMax < mElement.getMarkMiddleHeight()) ? midMax : mElement.getMarkMiddleHeight();
			var color = (mElement.getMarkMiddleColor() == "none") ? mElement.getStyle().getLineColor() : mElement.getMarkMiddleColor();
			var stroke = (mElement.getMarkMiddleStroke() < 0) ? mElement.getStyle().getLineWidth() : mElement.getMarkMiddleStroke();   

			// mElement.getMarkEndOrient() is ignored
			createMarkDef(x, y, mElement.getMarkMiddle(), midW, midH, stroke, color, mx, my);
		}
		
		// create start mark
		if(mElement.getMarkStart() != Arrow.NONE) {
			var stMax = (mElement.getMarkProportion() * EJSS_TOOLS.Mathematics.norm([size[0],size[1]])) ;
			var stW = (stMax != 0 && stMax < mElement.getMarkStartWidth()) ? stMax : mElement.getMarkStartWidth();
			var stH = (stMax != 0 && stMax < mElement.getMarkStartHeight()) ? stMax : mElement.getMarkStartHeight();
			var color = (mElement.getMarkStartColor() == "none") ? mElement.getStyle().getLineColor() : mElement.getMarkStartColor();
			var stroke = (mElement.getMarkStartStroke() < 0) ? mElement.getStyle().getLineWidth() : mElement.getMarkStartStroke();   

			// mElement.getMarkStartOrient() is ignored
			createMarkDef(x-mx, y-my, mElement.getMarkStart(), stW, stH, stroke, color, mx, my);
		}		    	
	}
}