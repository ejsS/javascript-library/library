/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas polygon
 */
EJSS_CANVASGRAPHICS.polygon = function(mContext, mElement) {  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
    
    // draw
  	mContext.beginPath();
	var points = mElement.getPoints();	    	    
	for(var i=0; i<points.length; i++) {
	  var point = points[i];	  
      var xx = (x-mx) + point[0]*size[0];
      var yy = (y-my) + point[1]*size[1];
      var type = point[2];
	  if ((i==0) || (type == 0)) // 0 is NOT CONNECTION
        mContext.moveTo(xx, yy);
      else 
      	mContext.lineTo(xx, yy);
	}  	  	
      
	// set style
    var style = mElement.getStyle(); 
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
}