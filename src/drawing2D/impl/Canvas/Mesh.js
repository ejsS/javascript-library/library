/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas mesh
 */
EJSS_CANVASGRAPHICS.mesh = function(mContext, mElement) {  
  var mA; 			// int[]
  var mB; 			// int[]
  var mCells;		// {cellA, cellB, cellZ, cellVector}
  var mBoundary;	// {boundaryA (int[][]), boundaryB (int[][])}
  
  // fill cell
  function fillCell(xs, ys, numpoints, color, levelIndex) {
  	mContext.beginPath();

	for(var i=0; i<numpoints; i++) {
      if(i==0) 
        mContext.moveTo(xs[0], ys[0]);
      else 
      	mContext.lineTo(xs[i], ys[i]);
	}  	  	
    mContext.lineTo(xs[0], ys[0]);

  	var fill = color.indexToColor(levelIndex);

  	mContext.fillStyle = fill;
  	mContext.fill();
  	mContext.lineWidth = 1.25;
  	mContext.strokeStyle = fill;
  	mContext.stroke();
  }   
 
  // draw cell
  function drawCell(xs, ys, fill, style) {
  	mContext.beginPath();

	var numpoints = xs.length;
	for(var i=0; i<numpoints; i++) {
      if(i==0) 
        mContext.moveTo(xs[0], ys[0]);
      else 
      	mContext.lineTo(xs[i], ys[i]);
	}  	  	
    mContext.lineTo(xs[0], ys[0]);
    
	// set style
	if (fill && style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	    
  }  

  function drawSegment(xs, ys, color, lineWidth) {
  	mContext.beginPath();

	var numpoints = xs.length;
	for(var i=0; i<numpoints; i++) {
      if(i==0) 
        mContext.moveTo(xs[0], ys[0]);
      else 
      	mContext.lineTo(xs[i], ys[i]);
	}  	  	
    mContext.lineTo(xs[0], ys[0]);
    
	// set style
  	mContext.lineWidth = lineWidth + 0.5;
  	mContext.strokeStyle = color;
  	mContext.stroke();
  }  
  
  function drawVectors(cellA,cellB,field,lenVector) { 
  	for(var i=0; i<cellA.length; i++) {
	
		// draw line
		var a = cellA[i];
		var b = cellB[i];
		var a1 = field[i][0];
		var b1 = field[i][1];
		var len = EJSS_TOOLS.Mathematics.norm([a1,b1]);
		var sa = a1*lenVector/len;
		var sb = b1*lenVector/len;
		
	    // draw
		if((sa != 0) || (sb != 0)) {    			
			var stMax = (0.3 * EJSS_TOOLS.Mathematics.norm([sa,sb])) ;
			var stW = (stMax != 0 && stMax < 12) ? stMax : 12;
		    var angle = Math.atan2(sa,sb);
		  	mContext.beginPath();
			mContext.moveTo(a, b);
		    mContext.lineTo(a+sa - stW*Math.cos(angle), b+sb - stW*Math.sin(angle));
		    mContext.lineTo(a+sa - stW*Math.cos(angle-Math.PI/6), b+sb - stW*Math.sin(angle-Math.PI/6));
		    mContext.lineTo(a+sa, y+my);
		    mContext.lineTo(a+sa - stW*Math.cos(angle+Math.PI/6), b+sb - stW*Math.sin(angle+Math.PI/6));
		    mContext.lineTo(a+sa - stW*Math.cos(angle), b+sb - stW*Math.sin(angle));
		
			// set style
	      	mContext.lineWidth = 1;
	     	mContext.strokeStyle = 'black';
	      	mContext.stroke();
		}
	}  	
  }
	
	// get position of the mElement center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
    
	var mPoints = mElement.getPoints();	
    if (mPoints == null) return;    
    
  	// project the points
    var nPoints = mPoints.length;
    mA = new Array(nPoints);
    mB = new Array(nPoints);
    for (var i=0; i<nPoints; i++) {
      mA[i] = x-mx + mPoints[i][0]*size[0];
      mB[i] = y-my + mPoints[i][1]*size[1];
    }

	// build cells
	var cells = mElement.getCells();
	var field = mElement.getFieldAtPoints();
	if (field) {
		mCells = EJSS_GRAPHICS.GraphicsUtils.buildCells(mA,mB,cells,field,false);
	} else {
	  field = mElement.getFieldAtCells();
	  mCells = EJSS_GRAPHICS.GraphicsUtils.buildCells(mA,mB,cells,field,true);
	}

    // first draw the cells
    if (cells != null) {    
	  var style = mElement.getStyle();
      if (field != null) {
	      // draw polygons    	  
		  var color = mElement.getColorCoded();
	      EJSS_GRAPHICS.GraphicsUtils.drawPolygons(mCells, color, 
	      	function(xs, ys) { drawCell(xs, ys, false, style) },
	      	function(xs, ys, numpoints, color, levelIndex) { fillCell(xs, ys, numpoints, color, levelIndex) });
      }
      else {
	      EJSS_GRAPHICS.GraphicsUtils.drawPolygons(mCells, color, 
	      	function(xs, ys) { drawCell(xs, ys, true, style) },
	      	function(xs, ys, numpoints, color, levelIndex) { });
      }
    }

	// build boundary
	var boundary = mElement.getBoundary();
	if(mElement.getDrawBoundary() && boundary != null) {	 
	  var labels = mElement.getBoundaryLabels();
	  var colors = mElement.getBoundaryColors();
	  var lineWidth = mElement.getBoundaryLineWidth();	
	  mBoundary = EJSS_GRAPHICS.GraphicsUtils.buildBoundary(mA, mB, boundary);
 
      // draw boundary
      EJSS_GRAPHICS.GraphicsUtils.drawBoundary(mBoundary, labels, colors, 
      	function(xs, ys, color) { drawSegment(xs,ys,color,lineWidth) }); 
	}

	// draw vectors
	if(mElement.getDataType() >= 4 && field != null) {
		var lenVector = mElement.getVectorLength() * size[0]; // scale for size[0]		
        for (var i=0, n=cells.length; i<n; i++) {
          // draw vectors
          drawVectors(mCellA[i],mCellB[i],field[i],lenVector);
        }
    }	

}