/**
 * Deployment for 2D Canvas drawing.
 * @module CanvasGraphics 
 */

var EJSS_CANVASGRAPHICS = EJSS_CANVASGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A Canvas spring
 */
EJSS_CANVASGRAPHICS.spring = function(mContext, mElement) {  
	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;

 	// get spring radius	    	
    var radius = mElement.getGroupPanel().toPixelMod([mElement.getRadius(),0])[0];

	// set attributes
	mContext.beginPath();
	EJSS_GRAPHICS.GraphicsUtils.drawSpring(mElement.getLoops(), mElement.getPointsPerLoop(), 
			radius, mElement.getSolenoid(), mElement.getThinExtremes(), x-mx, y-my, size[0], size[1],
			function(xx,yy) { mContext.moveTo(xx,yy) }, 
			function(xx,yy) { mContext.lineTo(xx,yy) });    
      
	// set style
    var style = mElement.getStyle(); 
	if (style.getDrawFill() && style.getFillColor() != 'none') {
      mContext.fillStyle = style.getFillColor();
      mContext.fill();
    }
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
}