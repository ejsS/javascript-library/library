/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Deployment for 2D drawing.
 * @module 2Dgraphics 
 */

var EJSS_GRAPHICS = EJSS_GRAPHICS || {};

/**
 * GraphicsUtils class
 * @class GraphicsUtils 
 * @constructor  
 */
EJSS_GRAPHICS.GraphicsUtils = {

	/**
	 * Return document box
	 * @return box  
	 */
	getOffsetRect: function(graphics) {
	    var width, height, top, left;
	    var element = graphics.getDOMElement();
	
		// (0) width and height
		var strw = graphics.getWidth();
		var strh = graphics.getHeight();  		
		width = parseFloat(strw);
		var isWidthNumber = (!isNaN(width) && isFinite(strw));	 
		height = parseFloat(strh);	 
		var isHeightNumber = (!isNaN(height) && isFinite(strh));
	
	    // (1) left and top
	    if(element.getBoundingClientRect) { // this option is better
			// **myPanel shows the VISIBLE elements
			var mypanel = element.getElementById? element.getElementById(".myPanel"):null;
			if(mypanel && mypanel.childElementCount>0) { 
				var box = mypanel.getBoundingClientRect();
			} else {
		    	var box = element.getBoundingClientRect();
		    }
			top = box.top;
			left = box.left;	  		
			if(!isWidthNumber) width = box.width;
			if(!isHeightNumber) height = box.height;
		} else if(element.offsetTop !== undefined) { // else this one
	    	top = element.offsetTop;
	    	left = element.offsetLeft;
			if(!isWidthNumber) width = element.offsetWidth;
			if(!isHeightNumber) height = element.offsetHeight;
	
	    } else { // use parent offset (buggy!)
			// **the parent MUST fit the SVG
	   		var parent = element.parentNode || document.getElementById(element.id).parentNode;
			while ((parent && !parent.offsetTop) && (parent && !parent.previousElementSibling && !parent.nextElementSibling)) {
	    		parent = parent.parentNode;
			};
			top = parent.offsetTop;
			left = parent.offsetLeft;
			if(!isWidthNumber) width = parent.offsetWidth;
			if(!isHeightNumber) height = parent.offsetHeight;
		}				    			 
	
	    var body = document.body;
	    var docElem = document.documentElement;
	     
	    // (2) we don't need absolute position, so scroll is ignored
	    // var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
	    // var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
	     
	    // (3)
	    var clientTop = docElem.clientTop || body.clientTop || 0;
	    var clientLeft = docElem.clientLeft || body.clientLeft || 0;
	     
	    // (4)
	    top =  top - clientTop;
	    left = left - clientLeft;
	
	    return { 
			top: top,
			left: left,
			width: width,
			height: height,
			bottom: top + height,
			right: left + width
		};     
	},

	/**
	 * Draw a spring
	*/
	drawSpring: function(loops, pointsPerLoop, radius, solenoid, thinExtremes, x, y, sx, sy, moveTo, lineTo) {
		var segments = loops * pointsPerLoop;
		var delta = 2.0 * Math.PI / pointsPerLoop;
		if (radius < 0) delta *= -1;
		var pre = pointsPerLoop / 2;
		// normalize sizes        	
		var size = [sx, sy, 0];
		var u1 = EJSS_TOOLS.Mathematics.normalTo(size);
		var u2 = EJSS_TOOLS.Mathematics.normalize(EJSS_TOOLS.Mathematics.crossProduct(size, u1));
		
	    for (var i=0; i<=segments; i++) {
	      var k;
	      if (thinExtremes) {	// extremes
	        if (i < pre) k = 0;
	        else if (i < pointsPerLoop) k = i - pre;
	        else if (i > (segments - pre)) k = 0;
	        else if (i > (segments - pointsPerLoop)) k = segments - i - pre;
	        else k = pre;
	      }
	      else k = pre;
	      var angle = Math.PI/2 + i*delta;
	      var cos = Math.cos(angle), sin = Math.sin(angle);
	      var xx = x + i*sx/segments + k*radius*(cos*u1[0] + sin*u2[0])/pre;
	      var yy = y + i*sy/segments + k*radius*(cos*u1[1] + sin*u2[1])/pre;
	      if (solenoid != 0.0)  {
	        var cte = k*Math.cos(i*2*Math.PI/pointsPerLoop)/pre;
	        xx += solenoid*cte*sx;
	        yy += solenoid*cte*sy;
	      }
	      if (i==0)
	      	moveTo(xx,yy); 
	      else 
	      	lineTo(xx,yy);
	    }
	},	
  
	/**
	 * Draw ticks for axis
	*/
	drawDecTicks: function(x, y, mx, my, ticksize, step, shift, orient, inverted, drawTick) {
		var UTILS = EJSS_SVGGRAPHICS.Utils;
		
		// bounds	
		var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
		var cright = UTILS.crispValue(right);
		var cbottom = UTILS.crispValue(bottom);
	
		// drawing ticks (only support mode NUM (decimal))
	    if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {
	    	// draw vertical axis (inverted or not are the same)
	    	for (var i = cbottom-shift; i >= top-0.5; i -= step) {		
				drawTick(x, i, ticksize, orient);
			}
	    } else {
	    	// draw horizontal axis
	    	for (var i = left+shift; i <= cright+0.5; i += step) {	// left to right	    		
				drawTick(i, y, ticksize, orient);
			}
		}					
	},
	
  
	/**
	 * Draw ticks text for axis
	*/
	drawDecTicksText: function(x, y, mx, my, ticksize, step, shift, scale, scalePrecision, scalestep, scaleshift, font, textPosition, orient, inverted, tickText) {
		var UTILS = EJSS_SVGGRAPHICS.Utils;
		
		// bounds	
		//console.log ("Axis = "+ orient+" scaleshif = "+scaleshift);
		var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
		var cright = UTILS.crispValue(right);
		var cbottom = UTILS.crispValue(bottom);
	
		// drawing ticks (only support mode NUM (decimal))
	    if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {
			// draw tick text			
	    	for (var j = 0, i = cbottom-shift; i >= top-0.5; i -= step, j++) {
	    		if(inverted)
	    			var number = scale[1] - scaleshift - (scalestep*j); // number for text
	    		else							
					var number = scale[0] + scaleshift + (scalestep*j); // number for text
		    	var text = parseFloat(number.toFixed(scalePrecision)).toFixed(scalePrecision);  // fixed precision				
				if(textPosition == EJSS_DRAWING2D.Axis.TICKS_DOWN) {
					var dx = ticksize/2; // tick size middle + 2
					tickText(mGroup, x+dx, i, text, font, false);
				} else {
					var dx = text.length * Math.floor(font.getFontSize()/2); // text long plus tick size 
					dx += text.length * font.getNumberLetterSpacing();
					dx += Math.floor(ticksize/2); 				
					tickText(x-dx, i, text, font, false);
				}
			}    	
	    } else {
			// draw tick text
	    	for (var j = 0, i = left+shift; i <= cright+0.5; i += step, j++) {	// left to right	    		
				var number = scale[0] + scaleshift + (scalestep*j); // number for text
		    	var text = parseFloat(number.toFixed(scalePrecision)).toFixed(scalePrecision);  // fixed precision		    					
				if(textPosition == EJSS_DRAWING2D.Axis.TICKS_DOWN) {
					var dy = Math.floor(ticksize/2) + 2; // tick size middle + 2
					tickText(i, y-ticksize, text, font, true);
				} else {
					var dy = font.getFontSize() + Math.floor(ticksize/2); // size font + tick size middle 
					tickText(i, y+dy, text, font, true);
				}			
		   	}
		}
	
	},
	
	/**
	 * Draw ticks for log axis
	*/
	drawLogTicks: function(x, y, mx, my, segsize, ticksize, scale, numticks, orient, drawTick) {
		var UTILS = EJSS_SVGGRAPHICS.Utils;
		
		// bounds	
		var left = x-mx, bottom = y-my;
		var cbottom = UTILS.crispValue(bottom);
	
		// drawing ticks (only support mode NUM (decimal))
	    if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {
	    	// draw vertical axis
	    	var minscale = Math.log(scale[0])/Math.log(10);		// min log value
	    	var maxscale = Math.log(scale[1])/Math.log(10);		// max log value
	    	var scalesize = maxscale - minscale;			// size of log scale
	    	var numbands = Math.ceil(scalesize);			// num of log bands
	    	var sizeratio = segsize / scalesize;			// pixel per log value
	    	
	    	// generate ticks in each band
	    	for (var i = 0; i <= numbands; i++) {
	    		var bandmin = Math.pow(10,i+minscale);		// band min
	    		var bandmax = Math.pow(10,i+minscale+1);	// band max
	    		var bandshift = bandmax / numticks;			// band part
		    	for (var j = 0; j < numticks-1; j++) {	    		
		    		// log pos	    		
		    		var number = bandmin + j * bandshift;  // number for text
					if(number != 0 && number >= scale[0] && number <= scale[1]) {
			    		var step = Math.log(number) / Math.log(10);
			    		var pos = cbottom - (step - minscale) * sizeratio;   		
						drawTick(x, pos, ticksize, orient);
					}  	   		    		    					
				}
			}
	    } else {
	    	// draw horizontal axis
	    	var minscale = Math.log(scale[0])/Math.log(10);		// min log value
	    	var maxscale = Math.log(scale[1])/Math.log(10);		// max log value
	    	var scalesize = maxscale - minscale;			// size of log scale
	    	var numbands = Math.ceil(scalesize);			// num of log bands
	    	var sizeratio = segsize / scalesize;			// pixel per log value
	    	
	    	// generate ticks in each band
	    	for (var i = 0; i <= numbands; i++) {
	    		var bandmin = Math.pow(10,i+minscale);		// band min
	    		var bandmax = Math.pow(10,i+minscale+1);	// band max
	    		var bandshift = bandmax / numticks;			// band part
		    	for (var j = 0; j < numticks-1; j++) {	    		
		    		// log pos	    		
		    		var number = bandmin + j * bandshift;  // number for text
					if(number != 0 && number >= scale[0] && number <= scale[1]) {
			    		var step = Math.log(number) / Math.log(10);
			    		var pos = left + (step - minscale) * sizeratio;   		
						drawTick(pos, y, ticksize, orient);
					}  	   		    		    					
				}
			}
		}					
	},
		
	/**
	 * Draw ticks text for log axis
	*/
	drawLogTicksText: function(x, y, mx, my, segsize, numticks, ticksize, scale, scalePrecision, font, textPosition, orient, tickText) {
		var UTILS = EJSS_SVGGRAPHICS.Utils;
		
		// bounds	
		var left = x-mx, bottom = y-my;
		var cbottom = UTILS.crispValue(bottom);
	
		// drawing ticks (only support mode NUM (decimal))
	    if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {
			// draw tick text			
	    	var minscale = Math.log(scale[0])/Math.log(10);		// min log value
	    	var maxscale = Math.log(scale[1])/Math.log(10);		// max log value
	    	var scalesize = maxscale - minscale;			// size of log scale
	    	var numbands = Math.ceil(scalesize);			// num of log bands
	    	var sizeratio = segsize / scalesize;			// pixel per log value
	    	
	    	// generate ticks in each band
	    	for (var i = 0; i <= numbands; i++) {
	    		var bandmin = Math.pow(10,i+minscale);		// band min
	    		var bandmax = Math.pow(10,i+minscale+1);	// band max
	    		var bandshift = bandmax / numticks;			// band part
		    	for (var j = 0; j < numticks-1; j++) {	    		
		    		// log pos	 		    		   		
		    		var number = bandmin + j * bandshift;  // number for text
					if(number != 0 && number >= scale[0] && number <= scale[1]) {
			    		var step = Math.log(number) / Math.log(10);
			    		var pos = cbottom - (step - minscale) * sizeratio;   		
						
						// draw text
						if(j == 0) { // exp format
							var text = number.toExponential(scalePrecision); 		    											
						} else {  // fixed precision
				    		var text = parseFloat((number/bandmin).toFixed(scalePrecision)).toFixed(scalePrecision); 		    					
						}
						if(textPosition == EJSS_DRAWING2D.Axis.TICKS_DOWN) {
							var dx = ticksize/2; // tick size middle + 2
							tickText(x+dx, pos, text, font, false);
						} else {
							var dx = text.length * Math.floor(font.getFontSize()/2); // text long plus tick size 
							dx += text.length * font.getNumberLetterSpacing();
							dx += Math.floor(ticksize/2); 				
							tickText(x-dx, pos, text, font, false);
						}
					}  	   		    		    					
				}
			}
	    } else {
	    	// draw horizontal axis
	    	var minscale = Math.log(scale[0])/Math.log(10);		// min log value
	    	var maxscale = Math.log(scale[1])/Math.log(10);		// max log value
	    	var scalesize = maxscale - minscale;			// size of log scale
	    	var numbands = Math.ceil(scalesize);			// num of log bands
	    	var sizeratio = segsize / scalesize;			// pixel per log value
	    	
	    	// generate ticks in each band
	    	for (var i = 0; i <= numbands; i++) {
	    		var bandmin = Math.pow(10,i+minscale);		// band min
	    		var bandmax = Math.pow(10,i+minscale+1);	// band max
	    		var bandshift = bandmax / numticks;			// band part
		    	for (var j = 0; j < numticks-1; j++) {	    		
		    		// log pos	 		    		   		
		    		var number = bandmin + j * bandshift;  // number for text
					if(number != 0 && number >= scale[0] && number <= scale[1]) {
			    		var step = Math.log(number) / Math.log(10);
			    		var pos = left + (step - minscale) * sizeratio;   		
						
						// draw text
						if(j == 0) { // exp format
							var text = number.toExponential(scalePrecision); 		    											
						} else {  // fixed precision
				    		var text = parseFloat((number/bandmin).toFixed(scalePrecision)).toFixed(scalePrecision); 		    					
						}
						if(textPosition == EJSS_DRAWING2D.Axis.TICKS_DOWN) {
							var dy = Math.floor(ticksize/2) + 2; // tick size middle + 2
							tickText(pos, y-ticksize, text, font, true);
						} else {
							var dy = font.getFontSize() + Math.floor(ticksize/2); // size font + tick size middle 
							tickText(pos, y+dy, text, font, true);
						}			
					}  	   		    		    					
				}
			}
		}
	},

	/**
	 * Draw grid
	*/
	drawDecGrid: function (left, top, right, bottom, sh, step, orientation, moveTo, lineTo) {
		var UTILS = EJSS_SVGGRAPHICS.Utils;

		var cleft = UTILS.crispValue(left), cright = UTILS.crispValue(right);
		var ctop = UTILS.crispValue(top), cbottom = UTILS.crispValue(bottom);

    	if(orientation == -1) {
    		if (step == 0) step = Math.abs(ctop-cbottom);
		    for (var i = cbottom-sh; i >= top-0.5; i -= step) { // bottom to top
	    		moveTo(left,i);
	    		lineTo(right,i);
		    }		    	
    	} else {
    		if (step == 0) step = Math.abs(cright-cleft);
		    for (var i = left+sh; i <= cright+0.5; i += step) { // left to right
		    	moveTo(i,top);
		    	lineTo(i,bottom);
		    }    		
    	}
	},
	
	/**
	 * Draw log grid
	*/
	drawLogGrid: function (left, top, right, bottom, numTicks, scale, orientation, moveTo, lineTo) {	
		var UTILS = EJSS_SVGGRAPHICS.Utils;

		var cleft = UTILS.crispValue(left), cright = UTILS.crispValue(right);
		var ctop = UTILS.crispValue(top), cbottom = UTILS.crispValue(bottom);

		var path = "";   

    	var minscale = Math.log(scale[0])/Math.log(10);		// min log value
    	var maxscale = Math.log(scale[1])/Math.log(10);		// max log value
    	var scalesize = maxscale - minscale;			// size of log scale
    	var numbands = Math.ceil(scalesize);			// num of log bands
    	if(orientation == -1) 
    		var sizeratio = (cbottom-ctop) / scalesize;		// pixel per log value
    	else
    		var sizeratio = (cright-cleft) / scalesize;		// pixel per log value
    	
    	// generate ticks in each band
    	for (var i = 0; i <= numbands; i++) {
    		var bandmin = Math.pow(10,i+minscale);		// band min
    		var bandmax = Math.pow(10,i+minscale+1);	// band max
    		var bandshift = bandmax / numTicks;			// band part
	    	for (var j = 0; j < numTicks-1; j++) {	    		
	    		// log pos	    		
	    		var number = bandmin + j * bandshift;  // number for text
				if(number != 0 && number >= scale[0] && number <= scale[1]) {
		    		var step = Math.log(number) / Math.log(10);
		    		if(orientation == -1) {
		    			var pos = cbottom - (step - minscale) * sizeratio;
						moveTo(cleft,pos);
						lineTo(cright,pos);
		    		} else { 
		    			var pos = cleft + (step - minscale) * sizeratio;
						moveTo(pos,ctop);
						lineTo(pos,cbottom);
					}
				}  	   		    		    					
			}
		}
	    return path;
	},
	
	/**
	 * Build the cells for mesh
	*/
	buildCells: function(a, b, cells, field, fieldGivenPerCell) {    
		var cellA;
		var cellB;
		var cellZ;
		var cellVector;
		
	    if (cells != null && cells.length>0) {
	      var nCells = cells.length;
	      cellA = new Array(nCells);
	      cellB = new Array(nCells);
	      cellZ = new Array(nCells);
	      cellVector = new Array(nCells);
	      var dimension = 1;
	      if (field != null) {
	        if (fieldGivenPerCell) dimension = field[0][0].length;
	        else dimension = field[0].length;
	      }
	      for (var i=0; i<nCells; i++) {
	        var cell = cells[i];
	        var nCellPoints = cell.length;
	        cellA[i] = new Array(nCellPoints);
	        cellB[i] = new Array(nCellPoints);
	        for (var j=0; j<nCellPoints; j++) {
	          var p = cell[j];
	          cellA[i][j] = a[p];
	          cellB[i][j] = b[p];
	        }
	        if (field != null) {
		      cellZ[i] = new Array(nCellPoints);        
		      cellVector[i] = new Array(nCellPoints);        
	          if (!fieldGivenPerCell) {
	            for (var j=0; j<nCellPoints; j++) {
	              var p = cell[j];
	              cellZ[i][j] = (dimension<=1) ? field[p][0] : EJSS_TOOLS.Mathematics.norm(field[p]);
	              cellVector[i][j] = new Array(dimension);
	              for (var k=0; k<dimension; k++) {
	                cellVector[i][j][k] = field[p][k];
	              }
	            }
	          }
	          else {
	            var cellField = field[i];
	            for (var j=0; j<nCellPoints; j++) {
	              cellZ[i][j] = (dimension<=1) ? cellField[j][0] : EJSS_TOOLS.Mathematics.norm(cellField[j]);
	              cellVector[i][j] = new Array(dimension);
	              for (var k=0; k<dimension; k++) {
	                cellVector[i][j][k] = cellField[j][k];
	              }
	            }
	          }
	        }        
	      }
	    }
	   	return {cellA: cellA, cellB: cellB, cellZ: cellZ, cellVector: cellVector};
	},
		
	/**
	 * Fill cells with the given values for mesh
	 */
	drawPolygons: function(cells, color, drawCell, fillCell){ 
		var cellA = cells.cellA;
		var cellB = cells.cellB;
		var cellZ = cells.cellZ;
		
	    for (var i=0, n=cellA.length; i<n; i++) {
		    var a = cellA[i];
		    var b = cellB[i];
			var values = cellZ[i]; 
		
			if(typeof values != 'undefined') {
				// fill cell	
			    var nVertex = a.length;
			    var vertexIndex = new Array(nVertex);
			
			    // Compute vertex indexes and their range
			    var minIndex = Number.MAX_VALUE, maxIndex = Number.MIN_VALUE; 
			    for(var j=0; j<nVertex; j++) {
			      var valueIndex = color.doubleToIndex(values[j]); // from -1 to numColors
			      minIndex = Math.min(valueIndex, minIndex);
			      maxIndex = Math.max(valueIndex, maxIndex);
			      vertexIndex[j] = valueIndex; 
			    }
			
			    // Computes the subpoligon in each region
			    var mThresholds = color.getColorThresholds()
			    
			    var newCornersX = new Array(2*nVertex);
			    var newCornersY = new Array(2*nVertex);    
			    for (var levelIndex = minIndex; levelIndex<=maxIndex; levelIndex++) { // for each level affected
			      var pointsAdded = 0;
			      for (var point = 0; point<nVertex; point++) { // for each point
			        var nextPoint = (point+1) % nVertex;
			
			        if (vertexIndex[point]<=levelIndex && vertexIndex[nextPoint]>=levelIndex) { // There is a bottom-up change of level
			          if (vertexIndex[point]==levelIndex) { // the point is on the current level
			            newCornersX[pointsAdded] = a[point];
			            newCornersY[pointsAdded] = b[point];
			            pointsAdded++;
			          }
			          else { // Add the point where the change occurs
			            //(x,y,levels[l]) = (x_p, y_p, z_p) + lambda((x_n, y_n, z_n)-(x_p, y_p, z_p))
			            //where (x_p, y_p) are point's coord and z_p can be calculated as the projection of (x_p, y_p) over levels vector 
			            //and (x_n, y_n) are next's coord and z_n can be calculated as the projection of (x_n, y_n) over levels vector
			            var lambda = (mThresholds[levelIndex]-values[point])/(values[nextPoint]-values[point]);
			            newCornersX[pointsAdded] = Math.round(a[point]+lambda*(a[nextPoint]-a[point]));
			            newCornersY[pointsAdded] = Math.round(b[point]+lambda*(b[nextPoint]-b[point]));
			            /*
					    if (newCornersX[pointsAdded]>300) {
					     console.log("level 1 added = "+newCornersX[pointsAdded]+ " division by "+(values[nextPoint]-values[point]));
					     console.log("Values = "+values[point]+ ", "+values[nextPoint]);
					     console.log("point index = "+vertexIndex[point]+ " next point index = "+vertexIndex[nextPoint]);
					     console.log("Should be point index = "+color.doubleToIndex(values[point])+ " next point index = "+color.doubleToIndex(values[nextPoint]));
					     console.log("Points= "+a[point]+ ", "+a[nextPoint]);
					     console.log("lambda= "+lambda);
			       for (var i=0,n=mThresholds.length; i<n; i++) console.log("Thresholds "+mThresholds[i]);
					     console.log("mThreshold= "+mThresholds[levelIndex]+ " level index = "+levelIndex);
					    }
					    */
			            pointsAdded++;        
			          }
			          if (vertexIndex[nextPoint]>levelIndex) { // This segment contributes with a second point
			            var lambda = (mThresholds[levelIndex+1]-values[point])/(values[nextPoint]-values[point]);
			            newCornersX[pointsAdded] = Math.round(a[point]+lambda*(a[nextPoint]-a[point]));
			            newCornersY[pointsAdded] = Math.round(b[point]+lambda*(b[nextPoint]-b[point]));
			            pointsAdded++;
			          }
			        } //end bottom-up
			        else if (vertexIndex[point]>=levelIndex && vertexIndex[nextPoint]<=levelIndex) { // There is a top-down change of level
			          if (vertexIndex[point]==levelIndex) { // the point is on the current level
			            newCornersX[pointsAdded] = a[point];
			            newCornersY[pointsAdded] = b[point];
			            pointsAdded++;
			          }
			          else { // Add the point where the change occurs
			            var lambda = (mThresholds[levelIndex+1]-values[point])/(values[nextPoint]-values[point]);
			            newCornersX[pointsAdded] = Math.round(a[point]+lambda*(a[nextPoint]-a[point]));
			            newCornersY[pointsAdded] = Math.round(b[point]+lambda*(b[nextPoint]-b[point]));
			            pointsAdded++;
			          }
			          if (vertexIndex[nextPoint]<levelIndex) { // This segment contributes with a second point
			            var lambda = (mThresholds[levelIndex]-values[point])/(values[nextPoint]-values[point]);
			            newCornersX[pointsAdded] = Math.round(a[point]+lambda*(a[nextPoint]-a[point]));
			            newCornersY[pointsAdded] = Math.round(b[point]+lambda*(b[nextPoint]-b[point]));
			            pointsAdded++;
			          }
			        }    
			      } // end for each point
			
			      if (pointsAdded>0) { // Draw the subpoligon
			      	  fillCell(newCornersX, newCornersY, pointsAdded, color, levelIndex);
			      }      	  
			    } //end for each level
			}
			
			// draw cell
			drawCell(a,b);
		}
	},
	
	
	/**
	 * Build the boundary for mesh
	*/
	buildBoundary: function(a, b, boundary) {        
	  	var boundaryA;
	  	var boundaryB;
	  	
	    if (boundary!=null && boundary.length>0) {
	      var num = boundary.length;
	      boundaryA = new Array(num);
	      boundaryB = new Array(num);
	      for (var i=0; i<num; i++) {
	        var segment = boundary[i];
	        var len = segment.length;
	        boundaryA[i] = new Array(len);
	        boundaryB[i] = new Array(len);
	        for (var j=0; j<len; j++) {
	          var p = segment[j];
	          boundaryA[i][j] = a[p];
	          boundaryB[i][j] = b[p];
	        }
	      }
	    }
	    return {boundaryA: boundaryA, boundaryB: boundaryB};
	},
	
	/**
	 * Build the boundary for mesh
	*/
	drawBoundary: function(boundary, labels, colors, drawSegment) {   
 		var boundaryA = boundary.boundaryA;
 		var boundaryB = boundary.boundaryB;
 		var segmentColor = 'black';
	    for (var i=0, n=boundaryA.length; i<n; i++) {
	        if (labels != null) {
	          var colorIndex = labels[i];
	          if ( colors!=null && colorIndex<colors.length) 
	          	segmentColor = colors[colorIndex];
	        }
	        drawSegment(boundaryA[i],boundaryB[i],segmentColor);
	    }     
	},
	
	/**
	 * Draw Image Field in Canvas
	*/
	drawImageField: function(ctx, params) {
		var matchColors = /rgb\((\d{1,3}),(\d{1,3}),(\d{1,3})\)/;
		var scale = (params.colors.length - 1) / (params.upper-params.lower);
	    var imageData = ctx.getImageData(0, 0, params.width, params.height);  // parameters must be integers
	    for (var i=0; i<params.width; i++) {
	      var ii= Math.floor(i*params.nx/params.width);
	      for (var j=0; j<params.height; j++) {
	        var jj=Math.floor(j*params.ny/params.height);
			var color;
	        if(params.data[ii][jj]<params.lower){
			  color = params.colors[0];
	        }else if(params.data[ii][jj]<params.upper){
			  var icolor = Math.round((params.data[ii][jj] - params.lower) * scale);
			  color = params.colors[icolor];
	        }else{ 
			  color = params.colors[params.colors.length-1]
	        }
	        var index = 4*i+4*params.width*j;
	        imageData.data[index+0] = color[0];  //red
	        imageData.data[index+1] = color[1];  //green
	        imageData.data[index+2] = color[2];  // blue
	      }
	    }
	    ctx.putImageData(imageData, 0, 0);		
	},

	/**
	 * Draw Image Field in Canvas using Parallel 
	*/
	drawImageFieldParallel: function(params) {
		var matchColors = /rgb\((\d{1,3}),(\d{1,3}),(\d{1,3})\)/;
		var scale = (params.colors.length - 1) / (params.upper-params.lower);
		var image = new Uint8ClampedArray(params.width*params.height*4);
	    for (var i=0; i<params.width; i++) {
	      var ii= Math.floor(i*params.nx/params.width);
	      for (var j=0; j<params.height; j++) {
	        var jj=Math.floor(j*params.ny/params.height);
			var color;
	        if(params.data[ii][jj]<params.lower){
			  color = params.colors[0];
	        }else if(params.data[ii][jj]<params.upper){
			  var icolor = Math.round((params.data[ii][jj] - params.lower) * scale);
			  color = params.colors[icolor];
	        }else{  
			  color = params.colors[params.colors.length-1]
	        }
	        var index = 4*i+4*params.width*j;
	        image[index+0] = color[0];  //red
	        image[index+1] = color[1];  //green
	        image[index+2] = color[2];  // blue
	      }
	    }
		return image;
	}
	
};

