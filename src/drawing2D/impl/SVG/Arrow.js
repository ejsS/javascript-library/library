/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG arrow
 */
EJSS_SVGGRAPHICS.arrow = function(mGraphics, mElement) {  
	var UTILS = EJSS_SVGGRAPHICS.Utils;
	var Arrow = EJSS_DRAWING2D.Arrow;
	const SPLINE_CUBIC_FACTOR = 0.25;
	
	function createMarkDef(name, mark, orient, width, height, stroke, color, color2, sizex, sizey) {
		// create marker
		var path, path2;		
		var marker = mGraphics.getElementById(name);						
		if(marker === null) {
			marker = document.createElementNS('http://www.w3.org/2000/svg', 'marker');
			marker.setAttribute('id', name);
			path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
			path.setAttribute('id', name + ".markpath");
			marker.appendChild(path); 					
			path2 = document.createElementNS('http://www.w3.org/2000/svg', 'path');
			path2.setAttribute('id', name + ".markpath2");
			marker.appendChild(path2); 					
			mGraphics.getElementsByTagName('defs')[0].appendChild(marker);
		} else {
			path = mGraphics.getElementById(name + ".markpath");		
			path2 = mGraphics.getElementById(name + ".markpath2");
			// for bug in IE
			mGraphics.getElementsByTagName('defs')[0].insertBefore(marker, marker);						
		}
				
		var vwidth = width + 2*stroke;
		var vheight = height + 2*stroke;
		marker.setAttribute('viewBox', (-stroke) + " " + (-stroke) + " " + vwidth + " " + vheight);
		marker.setAttribute('refX', width/2);
		marker.setAttribute('refY', height/2);
		marker.setAttribute('markerUnits', 'userSpaceOnUse');
		marker.setAttribute('markerWidth', vwidth);
		marker.setAttribute('markerHeight', vheight);
		marker.setAttribute('orient', orient);

		// create path
		path.setAttribute('fill', color);								
		path.setAttribute('stroke', color);   
		path.setAttribute('stroke-width', stroke);		
		path.setAttribute('style', '  marker-start :none;   marker-end :none; ');
		path2.setAttribute('fill', color2);								
		path2.setAttribute('stroke', color2);   
		path2.setAttribute('stroke-width', stroke);		
		path2.setAttribute('style', '  marker-start :none;   marker-end :none; ');
		switch(mark) {
			case Arrow.TRIANGLE:				
				var str = "M 0 0 L " + width + " " + (height/2) + " L 0 " + height + " z";
				path.setAttribute('d', str);
				break;			
			case Arrow.INVTRIANGLE:		
				var str = "M " + width + " 0 L 0 " + (height/2) + " L " + width + " " + height + " z";
				path.setAttribute('d', str);
				break;			
			case Arrow.ANGLE:
				// cut extremes
				// marker.setAttribute('viewBox', "0 0 " + vwidth + " " + height);
				marker.setAttribute('markerHeight', height);
				marker.setAttribute('refX', width);
				// create angle
				var p1 = -(stroke-1);
				var p2 = height+(stroke-1);					
				var str = "M 0 " + p1 + " L " + width + " " + (height/2) + " L 0 " + p2;				  
				path.setAttribute('d', str);
				path.setAttribute('fill', 'none');								
				break;			
			case Arrow.INVANGLE:
				// cut extremes
				// marker.setAttribute('viewBox', "0 0 " + vwidth + " " + height);
				marker.setAttribute('markerHeight', height);
				marker.setAttribute('refX', 1);
				// create angle
				var p1 = -(stroke-1);
				var p2 = height+(stroke-1);					
				var str = "M " + width + " " + p1 + " L 0 " + (height/2) + " L " + width + " " + p2;				  
				path.setAttribute('d', str);
				path.setAttribute('fill', 'none');								
				break;			
			case Arrow.LINE:				
				var str = "M " + (width/2) + " 0 L " + (width/2) + " " + height + " z"; 
				path.setAttribute('d', str);
				break;			
			case Arrow.RECTANGLE:			
				var str = "M 0 0 L " + width + " 0 L " + width + " " + height + " L 0 " + height + " z"; 
				path.setAttribute('d', str);
				break;			
			case Arrow.DIAMOND:
				var str = "M " + (width/2) + " 0 L " + width + " " + (height/2) + " L " + (width/2) + " " + height + " L 0 " + (height/2) + " z"; 
				path.setAttribute('d', str);
				break;			
			case Arrow.CIRCLE:
				var radius = (width < height) ? width : height;
				radius /= 2;
				var str = "M " + (width/2) + " " + (height/2) + " m " + (-radius) + ", 0 a " + radius + "," + radius + " 0 1,0 " + (radius*2) + ",0" + 
					" a " + radius + "," + radius + " 0 1,0 " + (-radius*2) + ",0"; 				  
				path.setAttribute('d', str);
				break;
			case Arrow.WEDGE:
				// without stroke
				path.setAttribute('stroke-width', 0);
				path2.setAttribute('stroke-width', 0.5);
				// create wedge (fixed position)
				var angle_rot = ((typeof sizey === "undefined") || (sizey===0))? 0 : Math.atan(sizex/sizey);
				var radius = (width < height) ? width : height;
				radius /= 2;
				var x = (width/2);
				var y = (height/2);
				var p11 = x + radius*Math.cos(angle_rot);
				var p12 = y - radius*Math.sin(angle_rot);		
				var p21 = x + radius*Math.cos(Math.PI/3.25 - angle_rot);
				var p22 = y + radius*Math.sin(Math.PI/3.25 - angle_rot);				
				var wedge = " M " + x + " " + y + " L " + p11 + "," + p12 + " A " + radius + "," + radius + " 0 0,1 " + p21 + "," + p22; 				  
				var circle = "M " + x + " " + y + " m " + (-radius) + ", 0 a " + radius + "," + radius + " 0 1,0 " + (radius*2) + ",0" + 
					 " a " + radius + "," + radius + " 0 1,0 " + (-radius*2) + ",0 ";
				path.setAttribute('d', circle);
				path2.setAttribute('d', wedge);				
				break;		
			case Arrow.POINTED:
				// without stroke
				marker.setAttribute('refX', width);
				path.setAttribute('stroke-width', 0);
				// create pointed mark
				var str = "M 0 0 L " + width + " " + (height/2) + " L 0 " + height + 
					" L " + (width/2) + " " + (height/2) + " L 0 0 z";
				path.setAttribute('d', str);
				break;			
			case Arrow.CURVE:
				// without stroke
				marker.setAttribute('refX', width);
				path.setAttribute('stroke-width', 0);
				// create curve mark
				var radius1 = width*3;
				var radius2 = height/2.5;
				var str = "M 0 0 " +
					" A " + radius1 + "," + radius1 + " 1 0,0 " + width + "," + (height/2) + 
					" A " + radius1 + "," + radius1 + " 1 0,0 " + " 0," + height + 
					" A " + radius2 + "," + radius2 + " 1 0,0 " + " 0," + (height/2) + 
					" A " + radius2 + "," + radius2 + " 1 0,0 " + " 0,0 z";
				path.setAttribute('d', str);
				break;								
		}		
		
	}
		
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get line
	var mLine = mGraphics.getElementById(mElement.getName());	
	if (mLine === null) { 	// exits?
	    mLine = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mLine.setAttribute("id", mElement.getName());
	    group.appendChild(mLine);	    
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
    
	// draw tip
	if((size[0] != 0) || (size[1] != 0)) {    			
		// create end mark	
		if(mElement.getMarkEnd() != Arrow.NONE) {
			var endMax = (mElement.getMarkProportion() * EJSS_TOOLS.Mathematics.norm([size[0],size[1]])) ;
			var endW = (endMax != 0 && endMax < mElement.getMarkEndWidth()) ? endMax : mElement.getMarkEndWidth();
			var endH = (endMax != 0 && endMax < mElement.getMarkEndHeight()) ? endMax : mElement.getMarkEndHeight();
			var color = (mElement.getMarkEndColor() == "none") ? mElement.getStyle().getLineColor() : mElement.getMarkEndColor();
			var stroke = (mElement.getMarkEndStroke() < 0) ? mElement.getStyle().getLineWidth() : mElement.getMarkEndStroke();   
			createMarkDef(mElement.getName() + ".end", mElement.getMarkEnd(), mElement.getMarkEndOrient(), endW, endH, stroke, color);
			mLine.setAttribute('marker-end', 'url(#' + mElement.getName() + ".end" + ')');
		} else {
			mLine.removeAttribute('marker-end');
		}
				
		// create mid mark
		if(mElement.getMarkMiddle() != Arrow.NONE) {
			var midMax = (mElement.getMarkProportion() * EJSS_TOOLS.Mathematics.norm([size[0],size[1]])) ;
			var midW = (midMax != 0 && midMax < mElement.getMarkMiddleWidth()) ? midMax : mElement.getMarkMiddleWidth();
			var midH = (midMax != 0 && midMax < mElement.getMarkMiddleHeight()) ? midMax : mElement.getMarkMiddleHeight();
			var color = (mElement.getMarkMiddleColor() == "none") ? mElement.getStyle().getLineColor() : mElement.getMarkMiddleColor();
			var colorsnd = (mElement.getMarkMiddleColorSnd() == "none") ? mElement.getStyle().getLineColor() : mElement.getMarkMiddleColorSnd();
			var stroke = (mElement.getMarkMiddleStroke() < 0) ? mElement.getStyle().getLineWidth() : mElement.getMarkMiddleStroke();						
			createMarkDef(mElement.getName() + ".mid", mElement.getMarkMiddle(), mElement.getMarkMiddleOrient(), midW, midH, stroke, color, colorsnd, size[0], size[1]);
			mLine.setAttribute('marker-mid', 'url(#' + mElement.getName() + ".mid" + ')');
		} else {
			mLine.removeAttribute('marker-mid');
		}
		
		// create start mark
		if(mElement.getMarkStart() != Arrow.NONE) {
			var stMax = (mElement.getMarkProportion() * EJSS_TOOLS.Mathematics.norm([size[0],size[1]])) ;
			var stW = (stMax != 0 && stMax < mElement.getMarkStartWidth()) ? stMax : mElement.getMarkStartWidth();
			var stH = (stMax != 0 && stMax < mElement.getMarkStartHeight()) ? stMax : mElement.getMarkStartHeight();		
			var color = (mElement.getMarkStartColor() == "none") ? mElement.getStyle().getLineColor() : mElement.getMarkStartColor();
			var stroke = (mElement.getMarkStartStroke() < 0) ? mElement.getStyle().getLineWidth() : mElement.getMarkStartStroke();						
			createMarkDef(mElement.getName() + ".start", mElement.getMarkStart(), mElement.getMarkStartOrient(), stW, stH, stroke, color);		
			mLine.setAttribute('marker-start', 'url(#' + mElement.getName() + ".start" + ')');			
		} else {
			mLine.removeAttribute('marker-start');
		}
	} else {
		mLine.removeAttribute('marker-end');
		mLine.removeAttribute('marker-mid');
		mLine.removeAttribute('marker-start');		
	}
		
	// draw line
	  var CR = UTILS.crispValue;

	  var style = mElement.getStyle();
    switch (mElement.getSplineType()) {
      default : 
      case Arrow.SPLINE_NONE :
        if(style.getShapeRendering() == "crispEdges") {
          if(mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
              mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
                " L " + CR(x) + " " + CR(y) +
                " L " + CR(x+mx) + " " + CR(y+my));   
          } else {
              mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) +
                " L " + CR(x+mx) + " " + CR(y+my));
            }
          } else {
          if(mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
              mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
                  " L " + (x) + " " + (y) + 
                  " L " + (x+mx) + " " + (y+my));   
          } else {
              mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
                  " L " + (x+mx) + " " + (y+my));
           }
        }       
        break;
      case Arrow.SPLINE_CUBIC :
				if (Math.abs(mx)>=Math.abs(my)) {
					var controlX = mx*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
							mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
								" Q " + CR(x-controlX) + " " + CR(y-my) +", " + CR(x) + " " + CR(y) +
								" Q " + CR(x+controlX) + " " + CR(y+my)+ ", " + CR(x+mx) + " " + CR(y+my));
						}
						else {
							mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) +
								" C " + CR(x) + " " + CR(y-my) +", " + CR(x) +
								" " + CR(y+my)+ ", " + CR(x+mx) + " " + CR(y+my));
						}
					} // end crisp edges 
					else { // non crisp edges
						if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
							mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
								" Q " + (x-controlX) + " " + (y-my) + ", " + (x)    + " " + (y) +
								" Q " + (x+controlX) + " " + (y+my) + ", " + (x+mx) + " " + (y+my));
//                  " C " + (x)    + " " + (y-my) +", " + (x) +" " + (y-my)+ ", " + (x)    + " " + (y) +
//                  " C " + (x) + " " + (y+my) +", " + (x)    +" " + (y+my)+ ", " + (x+mx) + " " + (y+my));   
						}
						else {
							mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
								" C " + x + " " + (y-my) +", " + x +" " + (y+my)+ ", " + (x+mx) + " " + (y+my) +
								" L " + (x+mx) + " " + (y+my));
						}
					} // end non crips edges
				} // end mx>=my
				else { // mx < my
					var controlY = my*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
							mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
								" Q " + CR(x-mx) + " " + CR(y-controlY) +", " + CR(x) + " " + CR(y) +
								" Q " + CR(x+mx) + " " + CR(y+controlY)+ ", " + CR(x+mx) + " " + CR(y+my));
						}
						else {
							mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) +
								" C " + CR(x-mx) + " " + CR(y) +", " + CR(x+mx) +
								" " + CR(y)+ ", " + CR(x+mx) + " " + CR(y+my));
						}
					} // end crisp edges 
					else { // non crisp edges
						if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
							mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
								" Q " + (x-mx) + " " + (y-controlY) + ", " + (x)    + " " + (y) +
								" Q " + (x+mx) + " " + (y+controlY) + ", " + (x+mx) + " " + (y+my));
//                  " C " + (x)    + " " + (y-my) +", " + (x) +" " + (y-my)+ ", " + (x)    + " " + (y) +
//                  " C " + (x) + " " + (y+my) +", " + (x)    +" " + (y+my)+ ", " + (x+mx) + " " + (y+my));   
						}
						else {
							mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
								" C " + (x-mx) + " " + (y) +", " + (x+mx) +" " + (y) + ", " + (x+mx) + " " + (y+my) +
								" L " + (x+mx) + " " + (y+my));
						}
					} // end non crips edges
				}
				break;

			case Arrow.SPLINE_CUBIC_HORIZONTAL :
				var controlX = mx*SPLINE_CUBIC_FACTOR;
				if (style.getShapeRendering() == "crispEdges") {
					if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
							" Q " + CR(x-controlX) + " " + CR(y-my) +", " + CR(x) + " " + CR(y) +
							" Q " + CR(x+controlX) + " " + CR(y+my)+ ", " + CR(x+mx) + " " + CR(y+my));
					}
					else {
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) +
							" C " + CR(x) + " " + CR(y-my) +", " + CR(x) +
							" " + CR(y+my)+ ", " + CR(x+mx) + " " + CR(y+my));
					}
				} // end crisp edges 
				else { // non crisp edges
					if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" Q " + (x-controlX) + " " + (y-my) + ", " + (x)    + " " + (y) +
							" Q " + (x+controlX) + " " + (y+my) + ", " + (x+mx) + " " + (y+my));
					}
					else {
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" C " + x + " " + (y-my) +", " + x +" " + (y+my)+ ", " + (x+mx) + " " + (y+my) +
							" L " + (x+mx) + " " + (y+my));
					}
				} // end non crips edges
				break;
			case Arrow.SPLINE_CUBIC_VERTICAL :
				var controlY = my*SPLINE_CUBIC_FACTOR;
				if (style.getShapeRendering() == "crispEdges") {
					if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
							" Q " + CR(x-mx) + " " + CR(y-controlY) +", " + CR(x) + " " + CR(y) +
							" Q " + CR(x+mx) + " " + CR(y+controlY)+ ", " + CR(x+mx) + " " + CR(y+my));
					}
					else {
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) +
							" C " + CR(x-mx) + " " + CR(y) +", " + CR(x+mx) +
							" " + CR(y)+ ", " + CR(x+mx) + " " + CR(y+my));
					}
				} // end crisp edges 
				else { // non crisp edges
					if (mElement.getMarkMiddle() != Arrow.NONE) {  // point in the middle
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" Q " + (x-mx) + " " + (y-controlY) + ", " + (x)    + " " + (y) +
							" Q " + (x+mx) + " " + (y+controlY) + ", " + (x+mx) + " " + (y+my));
					}
					else {
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" C " + (x-mx) + " " + (y) +", " + (x+mx) +" " + (y) + ", " + (x+mx) + " " + (y+my) +
							" L " + (x+mx) + " " + (y+my));
					}
				} // end non crips edges
				break;
/* REPEATED???
			case Arrow.SPLINE_START :
				if(mElement.getMarkMiddle() == Arrow.NONE) break; // makes no sense
				
				if (Math.abs(mx)>=Math.abs(my)) {
					var controlX = mx*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
							" Q " + CR(x-controlX) + " " + CR(y-my) +", " + CR(x) + " " + CR(y));
					} 
					else {
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" Q " + (x-controlX) + " " + (y-my) + ", " + (x)    + " " + (y));
					}
				} // end mx>=my
				else { // mx < my
					var controlY = my*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
							" Q " + CR(x-mx) + " " + CR(y-controlY) +", " + CR(x) + " " + CR(y));
					}
					else {
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" Q " + (x-mx) + " " + (y-controlY) + ", " + (x)    + " " + (y));
					}
				}
				break;
*/
			case Arrow.SPLINE_START :
				if (Math.abs(mx)>=Math.abs(my)) {
					var controlX = mx*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
							" Q " + CR(x-controlX) + " " + CR(y-my) +", " + CR(x) + " " + CR(y));
					} 
					else {
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" Q " + (x-controlX) + " " + (y-my) + ", " + (x)    + " " + (y));
					}
				} // end mx>=my
				else { // mx < my
					var controlY = my*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
							" Q " + CR(x-mx) + " " + CR(y-controlY) +", " + CR(x) + " " + CR(y));
					}
					else {
						mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
							" Q " + (x-mx) + " " + (y-controlY) + ", " + (x)    + " " + (y));
					}
				}
				break;		
			case Arrow.SPLINE_START_HORIZONTAL :
				var controlX = mx*SPLINE_CUBIC_FACTOR;
				if (style.getShapeRendering() == "crispEdges") {
					mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
						" Q " + CR(x-controlX) + " " + CR(y-my) +", " + CR(x) + " " + CR(y));
				} 
				else {
					mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
						" Q " + (x-controlX) + " " + (y-my) + ", " + (x)    + " " + (y));
				}
				break;		
			case Arrow.SPLINE_START_VERTICAL :
				var controlY = my*SPLINE_CUBIC_FACTOR;
				if (style.getShapeRendering() == "crispEdges") {
					mLine.setAttribute('d', "M " + CR(x-mx) + " " + CR(y-my) + 
						" Q " + CR(x-mx) + " " + CR(y-controlY) +", " + CR(x) + " " + CR(y));
				}
				else {
					mLine.setAttribute('d', "M " + (x-mx) + " " + (y-my) + 
						" Q " + (x-mx) + " " + (y-controlY) + ", " + (x)    + " " + (y));
				}
				break;

			case Arrow.SPLINE_END :
				if (Math.abs(mx)>=Math.abs(my)) {
					var controlX = mx*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						mLine.setAttribute('d', "M " + CR(x) + " " + CR(y) + 
							" Q " + CR(x+controlX) + " " + CR(y+my) +", " + CR(x+mx) + " " + CR(y+my));
					} 
					else {
						mLine.setAttribute('d', "M " + (x) + " " + (y) + 
							" Q " + (x+controlX) + " " + (y+my) + ", " + (x+mx)    + " " + (y+my));
					}
				} // end mx>=my
				else { // mx < my
					var controlY = my*SPLINE_CUBIC_FACTOR;
					if (style.getShapeRendering() == "crispEdges") {
						mLine.setAttribute('d', "M " + CR(x) + " " + CR(y) + 
							" Q " + CR(x+mx) + " " + CR(y+controlY) +", " + CR(x+mx) + " " + CR(y+my));
					}
					else {
						mLine.setAttribute('d', "M " + (x) + " " + (y-my) + 
							" Q " + (x+mx) + " " + (y+controlY) + ", " + (x+mx)    + " " + (y+my));
					}
				}
				break;		
			case Arrow.SPLINE_END_HORIZONTAL :
				var controlX = mx*SPLINE_CUBIC_FACTOR;
				if (style.getShapeRendering() == "crispEdges") {
					mLine.setAttribute('d', "M " + CR(x) + " " + CR(y) +
						" Q " + CR(x+controlX) + " " + CR(y+my) +", " + CR(x+mx) + " " + CR(y+my));
				} // end crisp edges 
				else { // non crisp edges
							mLine.setAttribute('d', "M " + (x) + " " + (y) + 
								" Q " + (x+controlX) + " " + (y+my) + ", " + (x+mx) + " " + (y+my));
				} // end non crips edges
				break;		
			case Arrow.SPLINE_END_VERTICAL :
				var controlY = my*SPLINE_CUBIC_FACTOR;
				if (style.getShapeRendering() == "crispEdges") {
					mLine.setAttribute('d', "M " + CR(x) + " " + CR(y) + 
						" Q " + CR(x+mx) + " " + CR(y+controlY) +", " + CR(x+mx) + " " + CR(y+my));
				}
				else {
					mLine.setAttribute('d', "M " + (x) + " " + (y) + 
						" Q " + (x+mx) + " " + (y+controlY) + ", " + (x+mx) + " " + (y+my));
				}
				break;
				
			}

	// set style
    if(style.getDrawFill()) 	
    	mLine.setAttribute("fill",style.getFillColor());    	
    else  
    	mLine.setAttribute("fill","none");
    if(style.getDrawLines()) {
    	mLine.setAttribute("stroke",style.getLineColor());
    	mLine.setAttribute("stroke-width",style.getLineWidth());
    } else {
    	mLine.setAttribute("stroke","none");
    	mLine.setAttribute("stroke-width",0);    	
    }        
	mLine.setAttribute("shapeRendering",style.getShapeRendering());  	

    var attributes = style.getAttributes();
    for (var attr in attributes) {
      mLine.setAttribute(attr,attributes[attr]);
    }

	return mLine;         
}