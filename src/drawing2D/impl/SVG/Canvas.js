/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG canvas
 */
EJSS_SVGGRAPHICS.canvas = function(mGraphics, mElement) {  
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());	
	var myCanvasSrc;	
	if (mShape === null) { 	// exits?
	    // create SVG element
	   	mShape = document.createElementNS("http://www.w3.org/2000/svg","foreignObject");
	    mShape.setAttribute("id",mElement.getName());	   
	    group.appendChild(mShape);
	    // create canvas src 
		myCanvasSrc = document.createElement('canvas');
		myCanvasSrc.setAttribute("id", mElement.getName() + ".canvas");

		mShape.appendChild(myCanvasSrc);	    
	}
	else {
		myCanvasSrc = document.getElementById(mElement.getName() + ".canvas");
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];

	myCanvasSrc.width = Math.abs(size[0]);
	myCanvasSrc.height = Math.abs(size[1]);
	
	mShape.width = Math.abs(size[0]);
	mShape.height = Math.abs(size[1]);
		
	// get half sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);
    
 	// set SVG element	
	mShape.setAttribute("x",(x-mx));
	mShape.setAttribute("y",(y-my));    	    					  	
	
	var dim = mElement.getDimensions();
  	var deltaX = dim[1] - dim[0];
  	var deltaY = dim[3] - dim[2];
    
	var pixWth = Math.abs(size[0]);
	var pixHgt = Math.abs(size[1]);
	var pxPerUnitX = pixWth / deltaX;
	var pxPerUnitY = pixHgt / deltaY;
	var pxTopLeftX = x - pixWth / 2;
	var pxTopLeftY = y + pixHgt / 2;
   
    //draws all objects within this.drawables
    var drawables = mElement.getDrawables();
    var context = myCanvasSrc.getContext('2d');
    context.fillRect(0,0,myCanvasSrc.width-0.5,myCanvasSrc.height-0.5);
    for (var i = 0; i < drawables.length; i++) {
      if(typeof drawables[i].imageField != "undefined")
        EJSS_GRAPHICS.GraphicsUtils.drawImageField(context, 
        	pxTopLeftX, pxTopLeftY, xMin, yMin, pixWth, pixHgt, pxPerUnitX, pxPerUnitY, drawables[i]);
      else if(typeof drawables[i].run != "undefined")
      	drawables[i].draw(context,
      		pxTopLeftX, pxTopLeftY, xMin, yMin, pixWth, pixHgt, pxPerUnitX, pxPerUnitY);
    }

	var attributes = mElement.getStyle().getAttributes();
	for (var attr in attributes) {
	  	mShape.setAttribute(attr,attributes[attr]);
	}    	    	    					  	
	
	return mShape;         
}