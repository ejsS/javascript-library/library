/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG trace
 */
EJSS_SVGGRAPHICS.trace = function(mGraphics, mElement) {  
	
	// generates path for trail
	function pathForTrail(points, x, y, sx, sy) {
		var path = "";
		for(var i=0; i<points.length; i++) {
		  var point = points[i];	  
	      var xx = x + point[0]*sx;
	      var yy = y + point[1]*sy;
	      var type = point[2];		
		  if ((i==0) || (type == 0)) // 0 is NOT CONNECTION
		  	path += " M " + xx + " " + yy;		// move 
	      else       	
	      	path += " L " + xx + " " + yy;		// line
		}  	  	
	    return path;
	}  	

	// creates ellipse mark
	function ellipseMark(mark, markx, marky, size, markstyle) {
		// get position of the element center 
	    var x = markx;
	    var y = marky;
		
		// get half sizes 		
	    var mx = Math.abs(size[0]/2);
	    var my = Math.abs(size[1]/2);

	 	// set attributes	    	    
	    mark.setAttribute("cx",x);
	    mark.setAttribute("cy",y);
	    mark.setAttribute("rx",mx);
	    mark.setAttribute("ry",my);
		
		// set style
	    if(markstyle.getDrawFill())	
	    	mark.setAttribute("fill",markstyle.getFillColor());
	    else 
	    	mark.setAttribute("fill","none");    
	    if(markstyle.getDrawLines()) {
	    	mark.setAttribute("stroke",markstyle.getLineColor());
	    	mark.setAttribute("stroke-width",markstyle.getLineWidth());
	    } else {
	    	mark.setAttribute("stroke","none");
	    	mark.setAttribute("stroke-width",0);    	
	    }        
		mark.setAttribute("shapeRendering",markstyle.getShapeRendering());  	
	}  	

	// creates rectangle mark
	function rectangleMark(mark, markx, marky, size, markstyle) {
		// get position of the element center 
	    var x = markx;
	    var y = marky;
		
		// get half sizes 		
	    var mx = Math.abs(size[0]/2);
	    var my = Math.abs(size[1]/2);

	 	// set attributes	    	    
	    mMark.setAttribute('d', 
	    	 "M " + (x-mx) + " " + (y+my) + 
	    	" L " + (x+mx) + " " + (y+my) + 
	    	" L " + (x+mx) + " " + (y-my) + 
	    	" L " + (x-mx) + " " + (y-my) + " z");
	
		// set style
	    if(markstyle.getDrawFill())	
	    	mark.setAttribute("fill",markstyle.getFillColor());
	    else 
	    	mark.setAttribute("fill","none");    
	    if(markstyle.getDrawLines()) {
	    	mark.setAttribute("stroke",markstyle.getLineColor());
	    	mark.setAttribute("stroke-width",markstyle.getLineWidth());
	    } else {
	    	mark.setAttribute("stroke","none");
	    	mark.setAttribute("stroke-width",0);    	
	    }        
		mark.setAttribute("shapeRendering",markstyle.getShapeRendering());  	
	}  	

	// creates area mark
	function areaMark(mark, lastmarkx, lastmarky, markx, marky, axis, markstyle) {		
	 	// set attributes	    	    
	    mMark.setAttribute('d', 
	    	 "M " + lastmarkx + " " + axis + 
	    	" L " + lastmarkx + " " + lastmarky + 
	    	" L " + markx + " " + marky + 
	    	" L " + markx + " " + axis + " z");
	
		// set style
	    if(markstyle.getDrawFill())	
	    	mark.setAttribute("fill",markstyle.getFillColor());
	    else 
	    	mark.setAttribute("fill","none");    
	    if(markstyle.getDrawLines()) {
	    	mark.setAttribute("stroke",markstyle.getLineColor());
	    	mark.setAttribute("stroke-width",markstyle.getLineWidth());
	    } else {
	    	mark.setAttribute("stroke","none");
	    	mark.setAttribute("stroke-width",0);    	
	    }        
		mark.setAttribute("shapeRendering",markstyle.getShapeRendering());  	
	}  	
		
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
	// create SVG element
	var mTraceGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	group.appendChild(mTraceGroup);	    

    // remove SVG element (not reusing element)
	var lastGroup = mGraphics.getElementById(mElement.getName());	
	if (lastGroup !== null) { 
		group.insertBefore(mTraceGroup,lastGroup);
		group.removeChild(lastGroup);
	}
   	
    // create SVG element group
    mTraceGroup.setAttribute("id", mElement.getName());
    	    
    // create trail element
    var mTrail = document.createElementNS("http://www.w3.org/2000/svg","path"); 
    mTrail.setAttribute("id", mElement.getName() + ".trail");
    mTraceGroup.appendChild(mTrail);	    

    // create marks element
    var mMarks = document.createElementNS("http://www.w3.org/2000/svg","g"); 
    mMarks.setAttribute("id", mElement.getName() + ".marks");
    mTraceGroup.appendChild(mMarks);	    

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
        
	// draw trail
    var trailpath = pathForTrail(mElement.getPoints(), x-mx, y-my, size[0], size[1])     
    if(trailpath !== "") {
    	mTrail.setAttribute('d', trailpath);
    			
		// set style for trail
	    var style = mElement.getStyle(); 
	    if(style.getDrawFill()) 	
	    	mTrail.setAttribute("fill",style.getFillColor());
	    else 
	    	mTrail.setAttribute("fill","none");    
	    if(style.getDrawLines()) {
	    	mTrail.setAttribute("stroke",style.getLineColor());
	    	mTrail.setAttribute("stroke-width",style.getLineWidth());
	    } else {
	    	mTrail.setAttribute("stroke","none");
	    	mTrail.setAttribute("stroke-width",0);    	
	    }        
		mTrail.setAttribute("shapeRendering",style.getShapeRendering());  	
    			
		// draw marks     
		var points = mElement.getPoints();
		var styleList = mElement.getMarkStyleList();
		var lastpointX, lastpointY;	// for mark area
		for(var i=0; i<points.length; i++) {
			var point = points[i];	  
			var markx = (x-mx) + point[0]*size[0];		// mark x
			var marky = (y-my) + point[1]*size[1];     // mark y
		    var markType = point[3];    		// mark type
			var markstyle = styleList.length>i?styleList[i]:point[4];	// mark style
			var marksize = [point[5],point[6]];		// mark size
			var markaxis = point[7];			
	
		    if (markType == EJSS_DRAWING2D.Trace.ELLIPSE) { // circle
			    var mMark = document.createElementNS("http://www.w3.org/2000/svg","ellipse"); 
			    mMark.setAttribute("id", mElement.getName() + ".mark" + i);
			    mMarks.appendChild(mMark);	    		    	
				ellipseMark(mMark, markx, marky, marksize, markstyle);
		    } else if (markType == EJSS_DRAWING2D.Trace.RECTANGLE) { // rectangle
			    var mMark = document.createElementNS("http://www.w3.org/2000/svg","path"); 
			    mMark.setAttribute("id", mElement.getName() + ".mark" + i);
			    mMarks.appendChild(mMark);	    		    	
				rectangleMark(mMark, markx, marky, marksize, markstyle);
		    } else if (markType == EJSS_DRAWING2D.Trace.BAR) { // bar
			    var mMark = document.createElementNS("http://www.w3.org/2000/svg","path"); 
			    mMark.setAttribute("id", mElement.getName() + ".mark" + i);
			    mMarks.appendChild(mMark);	  
			    // create bar  		    	
				//var axisx = mElement.getGroupPanel().getPixelPositionWorldOrigin()[1];
				var axisx = mElement.getGroupPanel().toPixelPosition([0,markaxis])[1];
				
		    	marksize[1] = axisx-marky;			// bar size
		    	marky = marky + marksize[1]/2;		// new mark y	    	
		    	rectangleMark(mMark, markx, marky, marksize, markstyle);
			}
			else if (markType == EJSS_DRAWING2D.Trace.AREA) { // area
		    	if(i!=0) {				  
				    var mMark = document.createElementNS("http://www.w3.org/2000/svg","path"); 
				    mMark.setAttribute("id", mElement.getName() + ".mark" + i);
				    mMarks.appendChild(mMark);	    		    	
					// create area
					//var axisx = mElement.getGroupPanel().getPixelPositionWorldOrigin()[1];
					
    				var axisx = mElement.getGroupPanel().toPixelPosition([0,markaxis])[1];
					var lastmarkx = (x-mx) + lastpointX*size[0];		// mark x
					var lastmarky = (y-my) + lastpointY*size[1];     // mark y
					areaMark(mMark, lastmarkx, lastmarky, markx, marky, axisx, markstyle);
		    	} 
		    	lastpointX = point[0];    	
		    	lastpointY = point[1];
		    }		  
		}  	  		  
	
		var attributes = style.getAttributes();
		for (var attr in attributes) {
		  	mTraceGroup.setAttribute(attr,attributes[attr]);
		}    	    	    			
	}
		
	return mTraceGroup;         
}