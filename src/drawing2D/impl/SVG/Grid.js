/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG grid
 */
EJSS_SVGGRAPHICS.grid = function(mGraphics, mElement) {
	
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // remove SVG element (not reusing element)
	var mGroup = mGraphics.getElementById(mElement.getName());	
	if (mGroup !== null) group.removeChild(mGroup);
	
    // create SVG element
    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
    mGroup.setAttribute("id", mElement.getName());
    group.appendChild(mGroup);	    
    var mShapeX = document.createElementNS("http://www.w3.org/2000/svg","path"); 
    mShapeX.setAttribute("id", mElement.getName() + "X");
    mGroup.appendChild(mShapeX);	    
    var mShapeY = document.createElementNS("http://www.w3.org/2000/svg","path"); 
    mShapeY.setAttribute("id", mElement.getName() + "Y");
    mGroup.appendChild(mShapeY);

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;

    var showx = mElement.getShowX();
    var showy = mElement.getShowY(); 
    
    var pathX = "";
    var pathY = "";
    
	// draw axis X (based on ticks mode)
	if(showx) {
		var ticksXmode = mElement.getTicksXMode();			// decimal or logarithmic
		if (ticksXmode == EJSS_DRAWING2D.Grid.SCALE_LOG) { // logarithmic
				
		    // scale
		    var scaleX = mElement.getScaleX();		// X axis scale 	
			
			if(scaleX[0] > 0 && scaleX[1] > 0) {  // valid scale
				// get number of axis ticks
			    var ticksX = mElement.getTicksX();
			    
				// set attributes
			    var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
		    	EJSS_GRAPHICS.GraphicsUtils.drawLogGrid(left, top, right, bottom, ticksX, scaleX, 1,
					function(xx,yy) { pathX += " M " + xx + " " + yy; }, 
					function(xx,yy) { pathX += " L " + xx + " " + yy; }); 
			} 
					    		
		} else if (ticksXmode == EJSS_DRAWING2D.Grid.SCALE_NUM) { // decimal
	    	    
		 	// create path	    	   	    
		   	var stepx = 0;	// step for ticks
		   	var tickstepx = 0;
			if (!mElement.getAutoTicksX()) {	  // no auto-ticks    
			    var ticksX = mElement.getTicksX();
			    // whether the number of sticks exits, changes step for ticks
			    if (ticksX != 0) { stepx = Math.abs(size[0]/ticksX); } else {
			    	stepx = mElement.getStepX();
			    	tickstepx = mElement.getTickStepX(); 
			    } 	    	

			} else { // auto-ticks
				var stepxmin = mElement.getAutoStepXMin();		// step y min
				var ticksxrange = mElement.getAutoTicksRangeX();		// ticks x range
		
				// find stepx
				for(var i=ticksxrange.length-1; i>=0; i--)	{	
					stepx = Math.abs(size[0]/ticksxrange[i]);
					if (stepx >= stepxmin) break;
				}
			}	
			
		    // step for scale
			var scalePrecisionX = mElement.getScalePrecisionX();	// number of decimals for scale
		    var scaleX = mElement.getScaleX();		// X axis scale 	
		    if(tickstepx == 0) {
				var scalestepX = Math.abs((scaleX[1] - scaleX[0]) * stepx / size[0]);  // step in X axis scale
			} else {
				var scalestepX = tickstepx;
				stepx = Math.abs((scalestepX * size[0]) / (scaleX[1] - scaleX[0])); 
			}			
						
			// adjust step to decimals of precision	
			var decimalsX = Math.pow(10,scalePrecisionX);
			var scalestepXTmp = Math.round(scalestepX * decimalsX) / decimalsX;
			if(scalestepXTmp > 0) {
				scalestepX = scalestepXTmp;
				stepx = Math.abs(scalestepXTmp * size[0] / (scaleX[1] - scaleX[0]));
			}

			// check fixed tick
			var fixedTicks = mElement.getFixedTickX();	  
			var tickfixed = scaleX[1];
			if (!isNaN(fixedTicks)) {
			  if (fixedTicks < scaleX[0]) tickfixed = fixedTicks + (Math.floor((scaleX[0]-fixedTicks)/scalestepX)+1)*scalestepX;
			  else if (fixedTicks > scaleX[1])  tickfixed = fixedTicks - (Math.floor((fixedTicks-scaleX[1])/scalestepX)+1)*scalestepX;
		      else tickfixed = fixedTicks; 
			}		
				
			// tick fixed in axis scale
			var scaleshift = Math.abs((scaleX[0] - tickfixed) % scalestepX);						
			var dist = Math.abs(scaleshift-scalestepX);	// fitting shift
			if(scaleshift < 0.001 || dist < 0.001) scaleshift = 0;						
			var shiftx = Math.abs(size[0] * scaleshift / (scaleX[1] - scaleX[0]));	// shift in pixels				

			// set attributes
		    var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
	    	EJSS_GRAPHICS.GraphicsUtils.drawDecGrid(left, top, right, bottom, shiftx, stepx, 1,
					function(xx,yy) { pathX += " M " + xx + " " + yy; }, 
					function(xx,yy) { pathX += " L " + xx + " " + yy; }); 
		}		
	}

	// draw axis Y (based on ticks mode)
	if(showy) {
		var ticksYmode = mElement.getTicksYMode();			// decimal or logarithmic
		if (ticksYmode == EJSS_DRAWING2D.Grid.SCALE_LOG) { // logarithmic
						
		    // scale
		    var scaleY = mElement.getScaleY();		// Y axis scale 	

			if(scaleY[0] > 0 && scaleY[1] > 0) {  // valid scale
				// get number of axis ticks
			    var ticksY = mElement.getTicksY();
			
				// set attributes
			    var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
		    	EJSS_GRAPHICS.GraphicsUtils.drawLogGrid(left, top, right, bottom, ticksY, scaleY, -1,
					function(xx,yy) { pathY += " M " + xx + " " + yy; }, 
					function(xx,yy) { pathY += " L " + xx + " " + yy; }); 	
			}
					    		
		} else if (ticksYmode == EJSS_DRAWING2D.Grid.SCALE_NUM) { // decimal
	    
		 	// create path	    	   	    
		   	var stepy = 0;	// step for ticks
		   	var tickstepy = 0;
			if (!mElement.getAutoTicksY()) {	  // no auto-ticks    
			    var ticksY = mElement.getTicksY();
			    // whether the number of sticks exits, changes step for ticks
			    if (ticksY != 0) { stepy = Math.abs(size[1]/ticksY); } else {
			    	stepy = mElement.getStepY();
			    	tickstepy = mElement.getTickStepY(); 
			    } 	    	
			} else { // auto-ticks
				var stepymin = mElement.getAutoStepYMin();		// step y min		
				var ticksyrange = mElement.getAutoTicksRangeY();		// ticks y range
		
				// find stepy
				for(var i=ticksyrange.length-1; i>=0; i--)	{	
					stepy = Math.abs(size[1]/ticksyrange[i]);
					if (stepy >= stepymin) break;
				}						
			}	
			
		    // step for scale
			var scalePrecisionY = mElement.getScalePrecisionY();	// number of decimals for scale
		    var scaleY = mElement.getScaleY();		// Y axis scale 	
		    if(tickstepy == 0) {
				var scalestepY = Math.abs((scaleY[1] - scaleY[0]) * stepy / size[1]);  // step in Y axis scale 
			} else {
				var scalestepY = tickstepy;
				stepy = Math.abs((scalestepY * size[1]) / (scaleY[1] - scaleY[0])); 
			}			
			
			// adjust step to decimals of precision	
			var decimalsY = Math.pow(10,scalePrecisionY);
			var scalestepYTmp = Math.round(scalestepY * decimalsY) / decimalsY;
			if(scalestepYTmp > 0) {
				scalestepY = scalestepYTmp;
				stepy = Math.abs(scalestepYTmp * size[1] / (scaleY[1] - scaleY[0]));
			}
			
			// check fixed tick
			var fixedTicks = mElement.getFixedTickY();	  
			var tickfixed = scaleY[1];
			if (!isNaN(fixedTicks)) {
			  if (fixedTicks < scaleY[0]) tickfixed = fixedTicks + (Math.floor((scaleY[0]-fixedTicks)/scalestepY)+1)*scalestepY;
			  else if (fixedTicks > scaleY[1])  tickfixed = fixedTicks - (Math.floor((fixedTicks-scaleY[1])/scalestepY)+1)*scalestepY;
		      else tickfixed = fixedTicks; 
			}		
	
			// tick fixed in axis scale
			var scaleshift = Math.abs((scaleY[0] - tickfixed) % scalestepY);						
			var dist = Math.abs(scaleshift-scalestepY);	// fitting shift
			if(scaleshift < 0.001 || dist < 0.001) scaleshift = 0;						
			var shifty = Math.abs(size[1] * scaleshift / (scaleY[1] - scaleY[0]));	// shift in pixels				
																								            				            
			// set attributes
		    var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
	    	EJSS_GRAPHICS.GraphicsUtils.drawDecGrid(left, top, right, bottom, shifty, stepy, -1,
					function(xx,yy) { pathY += " M " + xx + " " + yy; }, 
					function(xx,yy) { pathY += " L " + xx + " " + yy; }); 	    	
		}
	}
	
	// set path and style X
	if(pathX.length > 0) {
		mShapeX.setAttribute('d', pathX);
		mShapeX.setAttribute("shapeRendering",mElement.getShapeRenderingX());  	
    	mShapeX.setAttribute("stroke",mElement.getLineColorX());
    	mShapeX.setAttribute("stroke-width",mElement.getLineWidthX());    		
	}

	// set path and style Y
	if(pathY.length > 0) {
		mShapeY.setAttribute('d', pathY);
		mShapeY.setAttribute("shapeRendering",mElement.getShapeRenderingY());  	
    	mShapeY.setAttribute("stroke",mElement.getLineColorY());
    	mShapeY.setAttribute("stroke-width",mElement.getLineWidthY());    		
	}

	var attributes = mElement.getStyle().getAttributes();
	for (var attr in attributes) {
	  	mShapeX.setAttribute(attr,attributes[attr]);
	  	mShapeY.setAttribute(attr,attributes[attr]);
	}

	return mGroup;         
}