/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG trail
 */
EJSS_SVGGRAPHICS.trail = function(mGraphics, mElement) {

  /** adds a segment to the trail
   */
   function addSegment(group,points,num,style,pX,pY,sX,sY) {
     if (num<=0) return; // Nothing to add
	
	var segment = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	group.appendChild(segment);	    
	
    var path = "";
	for (var i=0; i<num; i++) {
	  var point = points[i];	  
	  var xx = pX + point[0]*sX;
	  var yy = pY + point[1]*sY;
	  var type = point[2];		
	  if ((i==0) || (type == 0)) // 0 is NOT CONNECTION
	  	path += " M " + xx + " " + yy;		// move 
      else       	
     	path += " L " + xx + " " + yy;		// line
 	}  	  	
    segment.setAttribute('d', path);
	if (style.getDrawFill()) segment.setAttribute("fill",style.getFillColor());
	else segment.setAttribute("fill","none");    
	if (style.getDrawLines()) {
	  segment.setAttribute("stroke",style.getLineColor());
	  segment.setAttribute("stroke-width",style.getLineWidth());
	} else {
	  segment.setAttribute("stroke","none");
	  segment.setAttribute("stroke-width",0);    	
	}        
	segment.setAttribute("shapeRendering",style.getShapeRendering());
		
	var attributes = style.getAttributes();
	for (var attr in attributes) segment.setAttribute(attr,attributes[attr]);
  }

	// get element group
    var group, elementGroup = mElement.getGroup();
    if (elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    
	// create SVG element
	var mTrail = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	group.appendChild(mTrail);	    

    // remove SVG element (not reusing element)
	var lastTrail = mGraphics.getElementById(mElement.getName());	
	if (lastTrail !== null) { 
		group.insertBefore(mTrail,lastTrail);
		group.removeChild(lastTrail);
	}

	// set name 
	mTrail.setAttribute("id", mElement.getName());
    	
	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
	
	var pX = x-mx;
	var pY = y-my;
	var sX = size[0];
	var sY = size[1];
	
	var segmentCount = mElement.getSegmentsCount();
	if (segmentCount>0) {
	  for (var i=0; i<segmentCount; i++) { 
	  	var num = mElement.getSegmentPoints(i).length;
	    addSegment(mTrail,mElement.getSegmentPoints(i),num,mElement.getSegmentStyle(i),pX,pY,sX,sY);
	  }
	}
	
	//mElement.dataCollected(); // add temporary points
	var num = mElement.getCurrentPoints().length; 
	addSegment(mTrail,mElement.getCurrentPoints(),num,mElement.getStyle(),pX,pY,sX,sY);
	
	return mTrail;


}