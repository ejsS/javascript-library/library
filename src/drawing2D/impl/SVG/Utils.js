/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};
EJSS_SVGGRAPHICS.Utils = {};

/**
 * @param value
 * @returns crisp value
 */
EJSS_SVGGRAPHICS.Utils.crispValue = function(value) {
  	return (Math.round(+value.toFixed(4)+0.5)-0.5);  	
}

/**
 * @param value
 * @returns crisp top value
 */
EJSS_SVGGRAPHICS.Utils.crispTop = function(value) {
  	return (Math.round(+value.toFixed(4)+0.5)-0.5);  	
}

/**
 * @param value
 * @returns round value
 */
EJSS_SVGGRAPHICS.Utils.round = function(value) {
  	return +value.toFixed(4);  	
}

/**
 * @param HSL, where H ∈ [0°, 360°), S ∈ [0, 1], L ∈ [0, 1]
 * @return rgb
 */
EJSS_SVGGRAPHICS.Utils.hsl2rgb = function (hsl) {
	var H = hsl[0], S = hsl[1], L = hsl[2];
    /* calculate chroma */
    var C = (1 - Math.abs((2 * L) - 1)) * S;

    /* Find a point (R1, G1, B1) along the bottom three faces of the RGB cube, with the same hue and chroma as our color (using the intermediate value X for the second largest component of this color) */
    var H_ = H / 60;
    var X = C * (1 - Math.abs((H_ % 2) - 1));
    var R1, G1, B1;

    if (H === undefined || isNaN(H) || H === null) {
        R1 = G1 = B1 = 0;
    }
    else {
        if (H_ >= 0 && H_ < 1) { R1 = C; G1 = X; B1 = 0;
        } else if (H_ >= 1 && H_ < 2) { R1 = X; G1 = C; B1 = 0;
        } else if (H_ >= 2 && H_ < 3) { R1 = 0; G1 = C; B1 = X;
        } else if (H_ >= 3 && H_ < 4) { R1 = 0; G1 = X; B1 = C;
        } else if (H_ >= 4 && H_ < 5) { R1 = X; G1 = 0; B1 = C;
        } else if (H_ >= 5 && H_ < 6) { R1 = C; G1 = 0; B1 = X; }
    }

    /* Find R, G, and B by adding the same amount to each component, to match lightness */
    var m = L - (C / 2);
    var R, G, B;
    /* Normalise to range [0,255] by multiplying 255 */
    R = (R1 + m) * 255;
    G = (G1 + m) * 255;
    B = (B1 + m) * 255;

    R = Math.round(R);
    G = Math.round(G);
    B = Math.round(B);

    return { r: R, g: G, b: B };
}


// params: imageData, width, height, depth, data, colors
// return: imageData
EJSS_SVGGRAPHICS.Utils.PNGCanvasWorker = function(params) {
	var imageData = params.data.params.imageData;
	var width = params.data.params.width;
	var height = params.data.params.height;
	var depth = params.data.params.depth;
	var data = params.data.params.data;
	var colors = params.data.params.colors;
	var index = params.data.id;    
	
	function toRgb(rgb){
	    return {r: rgb[0], g: rgb[1], b: rgb[2]};
	}
	
	function Rgb(rgb){
		var rgb = rgb.substring(4, rgb.length-1)
	     	.replace(/ /g, '')
	     	.split(',');
	    return {r: rgb[0], g: rgb[1], b: rgb[2]};
	}
		
	function Hsl(hsl){
		var hsl = hsl.substring(4, hsl.length-1)
	     	.replace(/ /g, '')
	     	.replace(/%/g, '')
	     	.split(',');
	    return {h: hsl[0], s: (hsl[1]/100), l: (hsl[2]/100)};
	}
		
	function setPixel(imageData, x, y, r, g, b, a) {
	    index = (x + y * imageData.width) * 4;
	    imageData.data[index+0] = r;
	    imageData.data[index+1] = g;
	    imageData.data[index+2] = b;
	    imageData.data[index+3] = a;
	}
	
	var cinf = colors[0];
	var csup = colors[colors.length-1];
	for(var i=0; i<width; i++) { 
		for(var j=0; j<height; j++) {
			// get color from palette
			var index = data[i][height-j-1];
			var color = index<0 ? cinf : (index<colors.length) ? colors[index] : csup;
			// check color
			if (typeof color == "undefined") color = cinf;
			// get rgb from color  		
			if (typeof color == "string") {				
			 	if(color.substring(0,3) == "rgb") {
					rgb = Rgb(color);
				} else if (color.substring(0,3) == "hsl") {
					rgb = EJSS_SVGGRAPHICS.Utils.hsl2rgb(Hsl(color));
				}  	
			} else if (Array.isArray(color)){
				rgb = toRgb(color);
			}   				
			// draw pixel
  			setPixel(imageData, i, j, rgb.r, rgb.g, rgb.b, 255);
  		}  		
	}
	
    self.postMessage({ result: imageData, id: index });					  
}

EJSS_SVGGRAPHICS.Utils.PNGCanvasNoWorker = function(imageData, width, height, depth, data, colors) {
	function toRgb(rgb){
	    return {r: rgb[0], g: rgb[1], b: rgb[2]};
	}
	
	function Rgb(rgb){
		var rgb = rgb.substring(4, rgb.length-1)
	     	.replace(/ /g, '')
	     	.split(',');
	    return {r: rgb[0], g: rgb[1], b: rgb[2]};
	}

	function Hsl(hsl){
		var hsl = hsl.substring(4, hsl.length-1)
         	.replace(/ /g, '')
         	.replace(/%/g, '')
         	.split(',');
	    return {h: hsl[0], s: (hsl[1]/100), l: (hsl[2]/100)};
	}
		
	function setPixel(imageData, x, y, r, g, b, a) {
	    index = (x + y * imageData.width) * 4;
	    imageData.data[index+0] = r;
	    imageData.data[index+1] = g;
	    imageData.data[index+2] = b;
	    imageData.data[index+3] = a;
	}
	
	// set data
	var cinf = colors[0];
	var csup = colors[colors.length-1];
  	for(var i=0; i<width; i++) { 
  		for(var j=0; j<height; j++) {
  			// get color from palette
  			var index = data[i][height-j-1];
  			var color = index<0 ? cinf : (index<colors.length) ? colors[index] : csup;
  			// check color
  			if (typeof color == "undefined") color = cinf;
  			// get rgb from color  			
			if (typeof color == "string") {				
			 	if(color.substring(0,3) == "rgb") {
					rgb = Rgb(color);
				} else if (color.substring(0,3) == "hsl") {
					rgb = EJSS_SVGGRAPHICS.Utils.hsl2rgb(Hsl(color));
				}  	
			} else if (Array.isArray(color)){
				rgb = toRgb(color);
			}   					
  			// draw pixel
  			setPixel(imageData, i, j, rgb.r, rgb.g, rgb.b, 255);
  		}  		
	}		
		
};

EJSS_SVGGRAPHICS.Utils.PNGCanvas = function(width, height, depth, data, colors, callback) {	
	var element = document.createElement("canvas");
	element.setAttribute('width',width);
	element.setAttribute('height',height);
	var c = element.getContext("2d");
	
	// create a new pixel array
	var imageData = c.createImageData(width, height);
	
	if(EJSS_TOOLS.Worker && EJSS_TOOLS.Worker.runFunction) {
		EJSS_TOOLS.Worker.runFunction("PNGCanvasWorker", EJSS_SVGGRAPHICS.Utils.PNGCanvasWorker, 
			{ imageData: imageData, 
			  width: width, height: height, depth: depth, 
			  data: data, colors: colors}, 
			function(result){ 			
				c.putImageData(result.result, 0, 0); // at coords 0,0	
				callback(element.toDataURL());
			});		
	} else {
		EJSS_SVGGRAPHICS.Utils.PNGCanvasNoWorker(imageData, width, height, depth, data, colors);
				
		// copy the image data back onto the canvas
		c.putImageData(imageData, 0, 0); // at coords 0,0		
		callback(element.toDataURL());	
	}
	
};

EJSS_SVGGRAPHICS.Utils.ImageDataCanvas = function(width, height, depth, data, colors, callback) {	
	var element = document.createElement("canvas");
	element.setAttribute('width',width);
	element.setAttribute('height',height);
	var c = element.getContext("2d");
	
	// create a new pixel array
	var imageData = c.createImageData(width, height);
	
	EJSS_TOOLS.Worker.runFunction("PNGCanvasWorker", EJSS_SVGGRAPHICS.Utils.PNGCanvasWorker, 
		{ imageData: imageData, 
		  width: width, height: height, depth: depth, 
		  data: data, colors: colors}, 
		function(result){		
			callback(result);
		});
};