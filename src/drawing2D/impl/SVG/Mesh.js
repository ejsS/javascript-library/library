/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG mesh
 */
EJSS_SVGGRAPHICS.mesh = function(mGraphics, mElement) {
  var mA; 			// int[]
  var mB; 			// int[]
  var mCells;		// {cellA, cellB, cellZ, cellVector}
  var mBoundary;	// {boundaryA (int[][]), boundaryB (int[][])}
  
  // fill cell
  function fillCell(group, xs, ys, numpoints, color, levelIndex) {
  	var poly = document.createElementNS("http://www.w3.org/2000/svg","polygon"); 
  	group.appendChild(poly);
	
	var path = "";
	for(var i=0; i<numpoints; i++) {
   	  path += xs[i] + "," + ys[i] + " ";
	}
	path += xs[0] + "," + ys[0] + " ";  	  	
	
	poly.setAttribute('points', path);
  	var fill = color.indexToColor(levelIndex, true);
  	poly.setAttribute("fill",fill);	  
  	poly.setAttribute("stroke",fill);
  	poly.setAttribute("stroke-width",1.25);  
  }   
 
  // draw cell
  function drawCell(group, xs, ys, fill, style) {
    var poly = document.createElementNS("http://www.w3.org/2000/svg","path"); 
    group.appendChild(poly);
    
	var path = "";
	var numpoints = xs.length-1;
	for(var i=0; i<numpoints; i++) {
	  if (i==0)
	  	path += " M " + xs[i] + " " + ys[i];	 
      else       	
      	path += " L " + xs[i] + " " + ys[i];	
	}  	  	
   	path += " L " + xs[i] + " " + ys[i] + " Z";
   	
    poly.setAttribute('d', path);
  
    // set style
    if(fill && style.getDrawFill()) 	
	  poly.setAttribute("fill",style.getFillColor());
    else 
	  poly.setAttribute("fill","none");    
    if(style.getDrawLines()) {
	  poly.setAttribute("stroke",style.getLineColor());
	  poly.setAttribute("stroke-width",style.getLineWidth());
    } else {
	  poly.setAttribute("stroke","none");
	  poly.setAttribute("stroke-width",0);    	
    }        
    poly.setAttribute("shapeRendering",style.getShapeRendering());      
  }  

  function drawSegment(group, xs, ys, color, lineWidth) {
    var mPoly = document.createElementNS("http://www.w3.org/2000/svg","path"); 
    group.appendChild(mPoly);
    
	var path = "";
	var numpoints = xs.length-1;
	for(var i = 0; i<numpoints; i++) {
	  if (i==0) // 0 is NOT CONNECTION
	  	path += " M " + xs[i] + " " + ys[i];	 
      else       	
      	path += " L " + xs[i] + " " + ys[i];	
	}  	  	
   	path += " L " + xs[i] + " " + ys[i] + " Z";
	
    mPoly.setAttribute('d', path);
	
    // set style
    mPoly.setAttribute("fill","none");    
    mPoly.setAttribute("stroke", color);
    mPoly.setAttribute("stroke-width", lineWidth + 0.5);
  }  
  
  function drawVectors(group,cellA,cellB,field,lenVector) { 
    var map = document.createElementNS("http://www.w3.org/2000/svg","g"); 
    group.appendChild(map);	    
  	for(var i=0; i<cellA.length; i++) {
	    // create SVG element group
	    var vector = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    map.appendChild(vector);	    
	    // create path element
	    var line = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    vector.appendChild(line);	    
	    // create cross element
	    var tip = document.createElementNS("http://www.w3.org/2000/svg","polygon"); 
	    vector.appendChild(tip);	    
	
		// draw line
		var a = cellA[i];
		var b = cellB[i];
		var a1 = field[i][0];
		var b1 = field[i][1];
		var len = EJSS_TOOLS.Mathematics.norm([a1,b1]);
		var sa = a1*lenVector/len;
		var sb = b1*lenVector/len;
		
		var str = "M " + a + " " + b + " L " + (a+sa) + " " + (b+sb);
	   	line.setAttribute('d', str);
	
		// draw tip
		if((sa != 0) || (sb != 0)) {
			var endsize;
		    var maxsize = 12; // tip max size
		    var proportion = 0.3; // 30% length
	
			// calculate tip size	    
			var len = EJSS_TOOLS.Mathematics.norm([sa,sb]);
	    	endsize = len * proportion;
	    	if (endsize > maxsize) endsize = maxsize;
		
		    // position of arrow end
		    var tipx = a + sa;
		    var tipy = b + sb;
		
		  	// draw arrow end (only triangle)
		  	var points = tipx +","+ tipy + " " +
		  				 (tipx-endsize) +","+ (tipy+endsize/3) + " " + 
		  				 (tipx-endsize) +","+ (tipy-endsize/3);	        
		    tip.setAttribute('points', points);	
			
			// angle of arrow end  	  	  	  	
		    var anglearrow = Math.atan2(sa,-sb);
		    anglearrow = anglearrow * 180 / Math.PI;
		    tip.setAttribute("transform","rotate(" + (anglearrow-90) + " " + tipx + " " + tipy +")"); // minus 90!
		}
		
	   	vector.setAttribute("stroke","black");
	   	vector.setAttribute("stroke-width",0.5);
	}  	
  }
	
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;

    // create SVG element
    var mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
    mGroup.setAttribute("id", mElement.getName());
    	
    // remove SVG element (not reusing element)
	var mOldGroup = mGraphics.getElementById(mElement.getName());	
	if (mOldGroup !== null) mOldGroup.parentNode.replaceChild(mGroup, mOldGroup);		
	else group.appendChild(mGroup);	

	// get position of the mElement center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
    
	var mPoints = mElement.getPoints();	
    if (mPoints == null) return;    
    
  	// project the points
    var nPoints = mPoints.length;
    mA = new Array(nPoints);
    mB = new Array(nPoints);
    for (var i=0; i<nPoints; i++) {
      mA[i] = x-mx + mPoints[i][0]*size[0];
      mB[i] = y-my + mPoints[i][1]*size[1];
    }

	// build cells
	var cells = mElement.getCells();
	var field = mElement.getFieldAtPoints();
	if (field) {
		mCells = EJSS_GRAPHICS.GraphicsUtils.buildCells(mA,mB,cells,field,false);
	} else {
	  field = mElement.getFieldAtCells();
	  mCells = EJSS_GRAPHICS.GraphicsUtils.buildCells(mA,mB,cells,field,true);
	}

    // first draw the cells
    if (cells != null) {    
	  var style = mElement.getStyle();
      if (field != null) {
	      // draw polygons    	  
		  var color = mElement.getColorCoded();
	      EJSS_GRAPHICS.GraphicsUtils.drawPolygons(mCells, color, 
	      	function(xs, ys) { drawCell(mGroup, xs, ys, false, style) },
	      	function(xs, ys, numpoints, color, levelIndex) { fillCell(mGroup, xs, ys, numpoints, color, levelIndex) });
      }
      else {
	      EJSS_GRAPHICS.GraphicsUtils.drawPolygons(mCells, color, 
	      	function(xs, ys) { drawCell(mGroup, xs, ys, true, style) },
	      	function(xs, ys, numpoints, color, levelIndex) { });
      }
    }

	// build boundary
	var boundary = mElement.getBoundary();
	if(mElement.getDrawBoundary() && boundary != null) {	 
	  var labels = mElement.getBoundaryLabels();
	  var colors = mElement.getBoundaryColors();
	  var lineWidth = mElement.getBoundaryLineWidth();	
	  mBoundary = EJSS_GRAPHICS.GraphicsUtils.buildBoundary(mA, mB, boundary);
 
      // draw boundary
      EJSS_GRAPHICS.GraphicsUtils.drawBoundary(mBoundary, labels, colors, 
      	function(xs, ys, color) { drawSegment(mGroup,xs,ys,color,lineWidth) }); 
	}

	// draw vectors
	if(mElement.getDataType() >= 4 && field != null) {
		var lenVector = mElement.getVectorLength() * size[0]; // scale for size[0]		
        for (var i=0, n=cells.length; i<n; i++) {
          // draw vectors
          drawVectors(mGroup,mCellA[i],mCellB[i],field[i],lenVector);
        }
    }	
	
	var attributes = mElement.getStyle().getAttributes();
	for (var attr in attributes) {
	  	mGroup.setAttribute(attr,attributes[attr]);
	}    	    	    	
	
	return mGroup;         
}

 