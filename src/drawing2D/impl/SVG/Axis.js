/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG axis
 */
EJSS_SVGGRAPHICS.axis = function(mGraphics, mElement) {
	var UTILS = EJSS_SVGGRAPHICS.Utils;

	// draw axis line
	function drawLine(group, x, y, mx, my, style, orient, inverted) {
	    var mLine = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    group.appendChild(mLine);	    

		if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {	// vertical axis
			if(inverted) {
				my = -my;
				y = -y;	
			}
		 	// set attributes	    	
		    if(style.getShapeRendering() == "crispEdges") {
		    	mLine.setAttribute('d', "M " + UTILS.crispValue(x) + " " + UTILS.crispValue(y-0.5-my) 
		    		+ " L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y-0.5+my));
		    } else {
		    	mLine.setAttribute('d', "M " + (x) + " " + (y-my) + " L " + (x) + " " + (y+my));
			}  	   		    		    		
		} else {	// horizontal axis
		 	// set attributes	    	
		    if(style.getShapeRendering() == "crispEdges") {
		    	mLine.setAttribute('d', "M " + UTILS.crispValue(x+0.5-mx) + " " + UTILS.crispValue(y) 
		    		+ " L " + UTILS.crispValue(x+0.5+mx) + " " + UTILS.crispValue(y));
		    } else {
		    	mLine.setAttribute('d', "M " + (x-mx) + " " + (y) + " L " + (x+mx) + " " + (y));
			}  	   		
		}		
	}

	function drawTick(group, x, y, ticksize, style, orient) {
		if(orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL) {	// vertical axis
		    var mTick = document.createElementNS("http://www.w3.org/2000/svg","path"); 
		    group.appendChild(mTick);
		    if(style.getShapeRendering() == "crispEdges") {
		    	mTick.setAttribute('d', "M " + UTILS.crispValue(x-ticksize/2) + " " + UTILS.crispValue(y) 
		    		+ " L " + UTILS.crispValue(x+ticksize/2) + " " + UTILS.crispValue(y));
		    } else {
		    	mTick.setAttribute('d', "M " + (x-ticksize/2) + " " + (y) + " L " + (x+ticksize/2) + " " + (y));
			}  	   					
		} else {
		    var mTick = document.createElementNS("http://www.w3.org/2000/svg","path"); 
		    group.appendChild(mTick);
		    if(style.getShapeRendering() == "crispEdges") {
		    	mTick.setAttribute('d', "M " + UTILS.crispValue(x) + " " + UTILS.crispValue(y-ticksize/2) 
		    		+ " L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y+ticksize/2));
		    } else {
		    	mTick.setAttribute('d', "M " + (x) + " " + (y-ticksize/2) + " L " + (x) + " " + (y+ticksize/2));
			}  	   		    		    								
		}					
	}	
	
	// text for tick in position (x,y) with font and mode
	function tickText (group, x, y, text, font, horizontal) {
	    // create SVG element group
	    var mTxtGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    group.appendChild(mTxtGroup);	    
	    // create text element
	    var mText = document.createElementNS("http://www.w3.org/2000/svg","text"); 
	    mTxtGroup.appendChild(mText);	    
				    
		// set attributes	    
		mText.setAttribute("font-family",font.getFontFamily());
		mText.setAttribute("font-size",font.getFontSizeString());
	    mText.setAttribute("fill",font.getFillColor());
    	mText.setAttribute("stroke",font.getOutlineColor());    	
	    mText.setAttribute("stroke-width",font.getFontWeight());
	    mText.setAttribute("letterSpacing",font.getLetterSpacing());
	    mText.textContent = text;
		if (horizontal) {
		  x -= mText.getComputedTextLength()/2 + 0.5;
		}	
		else { // vertical axis
		  y += font.getFontSize()/2; // - 0.5;
		}
		mText.setAttribute("x",x);
		mText.setAttribute("y",y);
	    
		// angle of text  	 
/*		var angletext = 0;
		switch(mode) {
			case EJSS_DRAWING2D.Text.MODE_TOPDOWN : angletext = 90; break;
			case EJSS_DRAWING2D.Text.MODE_RIGTHLEFT : angletext = 180; break;
			case EJSS_DRAWING2D.Text.MODE_DOWNTOP : angletext = 270; break;
			case EJSS_DRAWING2D.Text.MODE_LEFTRIGHT: angletext = 0; break;
		}
	    mText.setAttribute("transform","rotate(" + angletext + " " + x + " " + y +")"); 		
*/ 	  	  
	}		

	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // remove SVG element (not reusing element)
	var mGroup = mGraphics.getElementById(mElement.getName());	
	if (mGroup !== null) group.removeChild(mGroup);
	
	if(mElement.getShow()) { // show
	
	    // create SVG element
	    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mGroup.setAttribute("id", mElement.getName());
	    group.appendChild(mGroup);	    
	
		// get position of the mElement center 
	    var pos = mElement.getPixelPosition();
	    var size = mElement.getPixelSizes();     
	    var offset = mElement.getRelativePositionOffset(size);  
	    var x = pos[0]+offset[0];
	    var y = pos[1]+offset[1];
		
		// get half sizes 		
	    var mx = size[0]/2;
	    var my = size[1]/2;

		// properties	    
	    var style = mElement.getStyle();		// element style   	 	
		var orient = mElement.getOrient();		// axis orientation (vertical or horizontal)
		var inverted = mElement.getInvertedScaleY();
	    
	    // draw the line for axis
		drawLine(mGroup, x, y, mx, my, style, orient, inverted);
		 		
		// get axis size in pixel
		var segsize = (orient == EJSS_DRAWING2D.Axis.AXIS_VERTICAL)? Math.abs(size[1]):Math.abs(size[0]);		
		 		
		// draw axis (based on ticks mode)
		var ticksmode = mElement.getTicksMode();			// decimal or logarithmic
		if (ticksmode == EJSS_DRAWING2D.Axis.SCALE_LOG) { // logarithmic

		    var scale = mElement.getScale();		// axis scale 			
			if(scale[0] > 0 && scale[1] > 0) {  // valid scale			
				// get number of axis ticks
				var ticks = mElement.getTicks(); 	
	
			    // scale
				var scalePrecision = mElement.getScalePrecision();	// number of decimals for text
				
				// draw ticks 
				var ticksize = mElement.getTicksSize();				// ticks size in pixels
				EJSS_GRAPHICS.GraphicsUtils.drawLogTicks(x, y, mx, my, segsize, ticksize, scale, ticks, orient, 
						function(x, y, ticksize, orient) {
							drawTick(mGroup, x, y, ticksize, style, orient);			
						});		
			
				// draw ticks text 
				var font = mElement.getFont();
			    var textPosition = mElement.getTextPosition();		// text position (UP or DOWN)
				EJSS_GRAPHICS.GraphicsUtils.drawLogTicksText (x, y, mx, my, segsize, ticks, ticksize, scale, scalePrecision, font, textPosition, orient,
						function(x, y, text, font, horizontal) {
							tickText (mGroup, x, y, text, font, horizontal);
						});			
			}
			
		} else if (ticksmode == EJSS_DRAWING2D.Axis.SCALE_NUM) { // decimal
			 			 		
			// calculate step in pixels	
		    var step = 0;     // step for ticks (pixels)
		    var tickstep = 0; // step for ticks (real units)
			if (!mElement.getAutoTicks()) {		// no auto-ticks
		    	var ticks = mElement.getTicks();	// number of ticks
				// whether the number of ticks exits, changes step for ticks and scale 
			    if (ticks != 0) { step = segsize/ticks; } else {
			    	step = mElement.getStep();
			    	tickstep = mElement.getTickStep(); 
			    } 	    	
			} else {	// auto-ticks
				var stepmin = mElement.getAutoStepMin();		// step min in pixels
				var ticksrange = mElement.getAutoTicksRange();	// ticks range
				// find step based on ticks range
				for(var i=ticksrange.length-1; i>=0; i--)	{	
					step = Math.abs(segsize/ticksrange[i]);
					if (step*1.001 >= stepmin) break;
				}
			}
		    
			var scalePrecision = mElement.getScalePrecision();	// number of decimals for text
			var scale = mElement.getScale();		// axis scale 	
			// values for scale
		    if(tickstep == 0) {
				var scalestep = Math.abs((scale[1] - scale[0]) * step / segsize);  // step in axis scale
			} else {
				var scalestep = tickstep;
				step = Math.abs((scalestep * segsize) / (scale[1] - scale[0])); 
			}			
			
			// adjust step to decimals of precision
			var decimals = Math.pow(10,scalePrecision);
			var scalestepTmp = Math.round(scalestep * decimals) / decimals;
			if(scalestepTmp > 0) {
				scalestep = scalestepTmp; 
				step = Math.abs(scalestepTmp * segsize / (scale[1] - scale[0]));
			}
			
			// check fixed tick
			var fixedTicks = mElement.getFixedTick();	  
			var tickfixed = scale[1];
			if (!isNaN(fixedTicks)) {
			  if (fixedTicks < scale[0]) tickfixed = fixedTicks + (Math.floor((scale[0]-fixedTicks)/scalestep)+1)*scalestep;
			  else if (fixedTicks > scale[1])  tickfixed = fixedTicks - (Math.floor((fixedTicks-scale[1])/scalestep)+1)*scalestep;
		      else tickfixed = fixedTicks; 
			}		
	
			// tick fixed in axis scale
			var scaleshift = Math.abs((scale[0] - tickfixed) % scalestep);						
			var dist = Math.abs(scaleshift-scalestep);	// fitting shift
			if(scaleshift < 0.001 || dist < 0.001) scaleshift = 0;						
			var shift = segsize * scaleshift / Math.abs(scale[1] - scale[0]);	// shift in pixels				
										    		
			// draw ticks based on step and shift
			var ticksize = mElement.getTicksSize();				// ticks size in pixels
			EJSS_GRAPHICS.GraphicsUtils.drawDecTicks(x, y, mx, my, ticksize, step, shift, orient, inverted, 
					function(x, y, ticksize, orient) {
						drawTick(mGroup, x, y, ticksize, style, orient);
					});		
		
			// draw ticks text based on scaleshift, scalestep and scale
			var font = mElement.getFont();
		    var textPosition = mElement.getTextPosition();		// text position (UP or DOWN)
			EJSS_GRAPHICS.GraphicsUtils.drawDecTicksText (x, y, mx, my, ticksize, step, shift, scale, scalePrecision, scalestep, scaleshift, font, textPosition, orient, inverted, 
					function(x, y, text, font, horizontal) {
						tickText(mGroup, x, y, text, font, horizontal);
					});
		}
					
		// set style
	    if(style.getDrawFill()) 	
	    	mGroup.setAttribute("fill",style.getFillColor());
	    else 
	    	mGroup.setAttribute("fill","none");    
	    if(style.getDrawLines()) {
	    	mGroup.setAttribute("stroke",style.getLineColor());
	    	mGroup.setAttribute("stroke-width",style.getLineWidth());
	    } else {
	    	mGroup.setAttribute("stroke","none");
	    	mGroup.setAttribute("stroke-width",0);    	
	    }        
		mGroup.setAttribute("shapeRendering",style.getShapeRendering());  	
	
	    var attributes = style.getAttributes();
	    for (var attr in attributes) {
	      mGroup.setAttribute(attr,attributes[attr]);
	    }
	}
	
	return mGroup;         
}
