/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG pipe
 */
EJSS_SVGGRAPHICS.pipe = function(mGraphics, mElement) {
	var UTILS = EJSS_SVGGRAPHICS.Utils;   
	
	// get element group
    var group, elementGroup = mElement.getGroup();
    if (elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());
	if (mShape === null) { 	// exits?
	    // create SVG element
	    mShape = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mShape.setAttribute("id", mElement.getName());
	    group.appendChild(mShape);
	}
	var container = mGraphics.getElementById(mElement.getName()+":container");
	if (container === null) {	    
	    container = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    container.setAttribute("id", mElement.getName()+":container");
	    mShape.appendChild(container);
	 }	
	var liquid = mGraphics.getElementById(mElement.getName()+":liquid");
	if (liquid === null) {    
	    liquid = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    liquid.setAttribute("id", mElement.getName()+":liquid");
	    mShape.appendChild(liquid);	    
	}

    var points = mElement.getPoints();
    if (!points || points.length<=0) return;  

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];

	
	// get half sizes 		
    var sx = Math.abs(size[0]);
    var sy = Math.abs(size[1]);
        
 	// create path	    	   	    
 	var xmin = x-sx/2;
 	var ymin = y+sy/2;
    var style = mElement.getStyle(); 
    var crispy = (style.getShapeRendering() == "crispEdges"); 
    
    var containerPath = "";
    var liquidPath = "";
    var piece;
    if (crispy) piece = "M " + UTILS.crispValue(xmin) + " " + UTILS.crispValue(ymin);
    else        piece = "M " + xmin + " " + ymin; 
    containerPath = piece;
    liquidPath    = piece;
    for (var i=0, n=points.length; i<n; i++) {
      var point = points[i];
      var x = Math.round(point[0]*sx);
      var y = Math.round(-point[1]*sy);
      if (crispy) piece =  " l " + UTILS.crispValue(x) + " " + UTILS.crispValue(y);
      else        piece =  " l " + x + " " + y; 
      containerPath += piece;
      liquidPath += piece;
    }

	container.setAttribute('d',containerPath);
	liquid.setAttribute('d',liquidPath);
				
	// set style
	if (style.getDrawFill())  {
	  liquid.setAttribute("stroke",style.getFillColor());
      liquid.setAttribute("stroke-width",mElement.getPipeWidth());
    }
    else {
	  liquid.setAttribute("stroke","none");
      liquid.setAttribute("stroke-width",0);
    }  
    liquid.setAttribute("fill","none");    
	liquid.setAttribute("shapeRendering",style.getShapeRendering());  	

    if (style.getDrawLines()) {
      container.setAttribute("stroke",style.getLineColor());
      container.setAttribute("stroke-width",2*style.getLineWidth()+mElement.getPipeWidth());
    }
    else {
      container.setAttribute("stroke","none");
      container.setAttribute("stroke-width",0);
    }
    container.setAttribute("fill","none");    
	container.setAttribute("shapeRendering",style.getShapeRendering());  	

	var attributes = style.getAttributes();
	for (var attr in attributes) {
	  	liquid.setAttribute(attr,attributes[attr]);
	  	container.setAttribute(attr,attributes[attr]);
	}

	return mShape;         
}