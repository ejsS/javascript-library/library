/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG cellLattice
 */
EJSS_SVGGRAPHICS.cellLattice = function(mGraphics, mElement) {	
	var UTILS = EJSS_SVGGRAPHICS.Utils;   

	function pathForGrid_crispEdges(left, top, right, bottom, stepx, stepy) {
		var path = "";   
		
		var cleft = UTILS.crispValue(left), cright = UTILS.crispValue(right);
		var ctop = UTILS.crispValue(top), cbottom = UTILS.crispValue(bottom);
		// vertical lines
		if (stepx == 0) stepx = Math.abs(right-left);
	    for (var i = left; i <= Math.max(right,cright)+0.5; i = i+stepx) {
	  	  path += " M " + UTILS.crispValue(i) + " " + ctop + " L " + UTILS.crispValue(i) + " " + cbottom; 
	    }
	    // horizontal lines
	    if (stepy == 0) stepy = Math.abs(top-bottom);
	    for (var i = bottom; i >= Math.min(top,ctop)-0.5; i = i-stepy) {
	  	  path += " M " + cleft + " " + UTILS.crispValue(i) + " L " + cright + " " + UTILS.crispValue(i); 
	    }
	    return path;
	}  
	
	function pathForGrid(left, top, right, bottom, stepx, stepy) {	 	
		var path = "";   
		// vertical lines
		if (stepx == 0) stepx = Math.abs(right-left);
	    for (var i = left; i <= right; i = i+stepx) {
	  	  path += " M " + i + " " + top + " L " + i + " " + bottom; 
	    }
	    // horizontal lines
	    if (stepy == 0) stepy = Math.abs(top-bottom);
	    for (var i = bottom; i >= top; i = i-stepy) {
	  	  path += " M " + left + " " + i + " L " + right + " " + i; 
	    }
	    return path;
	}  
	
	function rectCell(group, x, y, sx, sy, rendering, fill) {
		// create rectangle
	    var mShape = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    group.appendChild(mShape);	   
	    		
		// create path	
		var mx = sx/2, my = sy/2;    	   	    
		if(rendering == "crispEdges") {
		    mShape.setAttribute('d', 
		    	 "M " + UTILS.crispValue(x-mx) + " " + UTILS.crispValue(y+my) + 
		    	" L " + UTILS.crispValue(x+mx) + " " + UTILS.crispValue(y+my) + 
		    	" L " + UTILS.crispValue(x+mx) + " " + UTILS.crispValue(y-my) + 
		    	" L " + UTILS.crispValue(x-mx) + " " + UTILS.crispValue(y-my) + " z");
		} else {
		    mShape.setAttribute('d', 
		    	 "M " + (x-mx) + " " + (y+my) + 
		    	" L " + (x+mx) + " " + (y+my) + 
		    	" L " + (x+mx) + " " + (y-my) + 
		    	" L " + (x-mx) + " " + (y-my) + " z");
		}  	 		
		mShape.setAttribute("fill",fill);
		mShape.setAttribute("stroke",fill);
	}

	function gridCell(group, x, y, sx, sy, stepx, stepy, style) {
		// create rectangle
	    var mShape = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    group.appendChild(mShape);	   

		// set attributes
		var mx = sx/2, my = sy/2;    	   	    
		var left = x-mx, right = x+mx, top = y+my, bottom = y-my;
	    if(style.getShapeRendering() == "crispEdges") 
	    	mShape.setAttribute('d', pathForGrid_crispEdges(left, top, right, bottom, stepx, stepy));
	    else {
	    	mShape.setAttribute('d', pathForGrid(left, top, right, bottom, stepx, stepy));
	    }
						
	    if(style.getDrawLines()) {
	    	mShape.setAttribute("stroke",style.getLineColor());
	    	mShape.setAttribute("stroke-width",style.getLineWidth());
	    } else {
	    	mShape.setAttribute("stroke","none");
	    	mShape.setAttribute("stroke-width",0);    	
	    }        
	    
		var attributes = style.getAttributes();
		for (var attr in attributes) {
		    mShape.setAttribute(attr,attributes[attr]);
		}	    
	}
	
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mGroup = mGraphics.getElementById(mElement.getName());			
	if (mGroup !== null) { 	// exits?
		group.removeChild(mGroup);
	}
    // create SVG mElement group
    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
    mGroup.setAttribute("id", mElement.getName());
    group.appendChild(mGroup);	    

	// get position of the mElement center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;

	// draw cells
	var style = mElement.getStyle();     
	var data = mElement.getData();
	var colors = mElement.getColorMapper().getColors();
	var xlen = data.length;
	var ylen = data[0].length;

    var stepx = Math.abs(size[0]/xlen);
    var stepy = Math.abs(size[1]/ylen);
   	
  	var left = x-mx+stepx/2, bottom = y-my-stepy/2;		
  	for(var i=0; i<ylen; i++) { 
  		for(var j=0; j<xlen; j++) {
  			// draw rectangle
  			rectCell(mGroup, left+stepx*j, bottom-stepy*i, stepx, stepy, style.getShapeRendering(), colors[data[j][i]]);
  		}  		
  	}
  	 	
  	// draw grid
  	var showGrid = mElement.getShowGrid();
	if (showGrid) gridCell(mGroup, x, y, size[0], size[1], stepx, stepy, style);
	  	
	return mGroup;         
}