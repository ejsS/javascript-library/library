/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG analyticCurve
 */
EJSS_SVGGRAPHICS.analyticCurve = function(mGraphics, mElement) {
	
	// generates path for analyticCurve
	function pathForanalyticCurve(points, x, y, sx, sy) {
		var path = "";
		for(var i=0; i<points.length; i++) {
		  var point = points[i];	  
	      var xx = x + point[0]*sx;
	      var yy = y + point[1]*sy;
	      var type = point[2];		
		  if ((i==0) || (type == 0)) // 0 is NOT CONNECTION
		  	path += " M " + xx + " " + yy;		// move 
	      else       	
	      	path += " L " + xx + " " + yy;		// line
		}  	  	
	    return path;
	}  	

	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());	
	if (mShape === null) { 	// exits?
	    // create SVG element
	    mShape = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mShape.setAttribute("id", mElement.getName());
	    group.appendChild(mShape);	    
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;

	// calculate points	    	 
	var numPoints = mElement.getNumPoints();
	var mMinimun = mElement.getMinimun();
	var mMaximun = mElement.getMaximun();
		var min = ( (typeof mMinimun == "undefined" || mMinimun === null) ?  mElement.getPanel().getRealWorldXMin() : mMinimun);
		var max = ( (typeof mMaximun == "undefined" || mMaximun === null) ?  mElement.getPanel().getRealWorldXMax() : mMaximun);
	
//	var min = (mElement.getMinimun()? mElement.getMinimun() : mElement.getPanel().getRealWorldXMin());
//	var max = (mElement.getMaximun()? mElement.getMaximun() : mElement.getPanel().getRealWorldXMax());
	var vble = mElement.getVariable();
	var fx = mElement.getFunctionX();
	var fy = mElement.getFunctionY();	
   	var parser = EJSS_DRAWING2D.functionsParser();
   	
    var exprfx;
	var exprfy;
	var mustReturn = false;
	try {
	  exprfx = parser.parse(fx);
	}
	catch (errorfx) {
  	  console.log ("Analytic curve error parsing FunctionX: "+fx);
	  mustReturn = true;
	}
	if (!mustReturn) {
	  try {
	    exprfy = parser.parse(fy);
	  }
	  catch (errorfy) {
  	    console.log ("Analytic curve error parsing FunctionY: "+fy);
	   	mustReturn = true;
	  }
	}
	if (mustReturn) {
	  mElement.getController().invokeAction("OnError");
	  return;
	}
	
   	var step = (max-min)/(numPoints-1);
   	
	var points = [];
    var vblevalue = {};
    var parameters = mElement.getParameters();
    for (var param in parameters) { 
      vblevalue[param] = parameters[param];
	 }
    
   	try {
   	  for(var j=0, i=min; i<=max; i+=step) {
   		vblevalue[vble] = i;
   		  var fxvalue = exprfx.evaluate(vblevalue);
   		  var fyvalue = exprfy.evaluate(vblevalue);
	   	  if(!isNaN(fxvalue) && !isNaN(fyvalue)) {   		
	   		points[j] = [];
	    	points[j][0] = fxvalue;	
			points[j++][1] = fyvalue;
		  }
		}
	}
	catch(error) {
	  mElement.getController().invokeAction("OnError");
	 } // do not complain
	
	// draw points
    var analyticCurvepath = pathForanalyticCurve(points, x-mx, y-my, size[0], size[1])     
    if(analyticCurvepath !== "") {
    	mShape.setAttribute('d', analyticCurvepath);
    		
		// set style
	    var style = mElement.getStyle(); 
	    if(style.getDrawFill())	
	    	mShape.setAttribute("fill",style.getFillColor());
	    else 
	    	mShape.setAttribute("fill","none");    
	    if(style.getDrawLines()) {
	    	mShape.setAttribute("stroke",style.getLineColor());
	    	mShape.setAttribute("stroke-width",style.getLineWidth());
	    } else {
	    	mShape.setAttribute("stroke","none");
	    	mShape.setAttribute("stroke-width",0);    	
	    }        
		mShape.setAttribute("shapeRendering",style.getShapeRendering());  	
		
	    var attributes = style.getAttributes();
	    for (var attr in attributes) {
	      mShape.setAttribute(attr,attributes[attr]);
	    }		
	}

	return mShape;         
}