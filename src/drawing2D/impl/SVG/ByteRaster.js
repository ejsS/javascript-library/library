/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG byteRaster
 */
EJSS_SVGGRAPHICS.byteRaster = function(mGraphics, mElement) {
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape		
	var mGroup = mGraphics.getElementById(mElement.getName());
	var mShape;
	if (mGroup == null) { 	// exists?
	    // create SVG mElement group
	    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mGroup.setAttribute("id", mElement.getName());
	    group.appendChild(mGroup);	    
	    
		mShape = document.createElementNS("http://www.w3.org/2000/svg","image");
		mShape.setAttribute("id", mElement.getName() + ".img");
		mGroup.appendChild(mShape); 	    
	} else {
		mShape = mGraphics.getElementById(mElement.getName() + ".img");
	}
	
	if(mElement.getDataChanged()) { // new data or colors
		var data = mElement.getData();
		var colors = mElement.getColorMapper().getColors();			
		var xlen = data.length;
		if (xlen>0) {
		  var ylen = data[0].length;
		  var num = mElement.getColorMapper().getNumberOfColors();
		
		// Worker previous
		// var png = EJSS_SVGGRAPHICS.Utils.PNGCanvas (xlen, ylen, num, data, colors);		
	    // mShape.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', png);	    
	    // mElement.setDataChanged(false);		 	    	    	

		  // using worker
		  EJSS_SVGGRAPHICS.Utils.PNGCanvas (xlen, ylen, num, data, colors,
			function(png) {
				mShape.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', png);		
			});		
	    }	    	    
	    mElement.setDataChanged(false);		 	    	    	
	}

	// get position of the mElement center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);    

	mShape.setAttribute("x",x-mx);
	mShape.setAttribute("y",y-my);
	mShape.setAttribute("width",Math.abs(size[0]));
	mShape.setAttribute("height",Math.abs(size[1]));

	var style = mElement.getStyle();				// element style
    var attributes = style.getAttributes();
    for (var attr in attributes) {
      mShape.setAttribute(attr,attributes[attr]);
    }

	return mGroup;         
}

/*  // Alternative using SVG

    // create other SVG mElement group
    var mPixels = document.createElementNS("http://www.w3.org/2000/svg","g"); 
    mPixels.setAttribute("id", mElement.getName() + ".pixels");    	   

	var myBasePixel = document.createElementNS("http://www.w3.org/2000/svg","path");	
	myBasePixel.setAttribute("stroke-width",1.5);	    
     	
  	for(var i=0; i<xlen; i++) { 
  		for(var j=0; j<ylen; j++) {
  			// draw pixel
			color = colors[data[i][j]];
      		var myPixel = myBasePixel.cloneNode(true);      		
			myPixel.setAttribute('d', "M " + (i) + " " + (j) + " l 0 1");
			myPixel.setAttribute("stroke",color);
			mPixels.appendChild(myPixel);			
  		}  		
	}	

	// scale  	  	  	  		
  	var left = x-mx, top = y+my;
    var stepx = Math.abs(size[0]/xlen);
    var stepy = Math.abs(size[1]/ylen);
    mPixels.setAttribute("transform","scale(" + (stepx) + "," + (stepy) +") translate(" + left + "," + top + ")"); 
*/
