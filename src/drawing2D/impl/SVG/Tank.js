/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG rectangle
 */
EJSS_SVGGRAPHICS.tank = function(mGraphics, mElement) {
	var UTILS = EJSS_SVGGRAPHICS.Utils;   
	
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());
	if (mShape === null) { 	// exits?
	    // create SVG element
	    mShape = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mShape.setAttribute("id", mElement.getName());
	    group.appendChild(mShape);
	}
	var container = mGraphics.getElementById(mElement.getName()+":container");
	if (container === null) {	    
	    container = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    container.setAttribute("id", mElement.getName()+":container");
	    mShape.appendChild(container);
	 }	
	var liquid = mGraphics.getElementById(mElement.getName()+":liquid");
	if (liquid === null) {    
	    liquid = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    liquid.setAttribute("id", mElement.getName()+":liquid");
	    mShape.appendChild(liquid);	    
	}


	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];

	
	// get half sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);
        
    var level = mElement.getPixelSizeOf(0,mElement.getLevel());  

 	// create path	    	   	    
 	var xmin = x-mx, xmax = x+mx;
 	var ymin = y+my, ymax = y-my;
 	var ylevel = ymin + level[1];
    var style = mElement.getStyle(); 
    if(style.getShapeRendering() == "crispEdges") {
	    liquid.setAttribute('d', 
	    	 "M " + UTILS.crispValue(xlevel) + " " + UTILS.crispValue(ylevel) + 
	    	" L " + UTILS.crispValue(xmin) + " " + UTILS.crispValue(ymin) + 
	    	" L " + UTILS.crispValue(xmax) + " " + UTILS.crispValue(ymin) + 
	    	" L " + UTILS.crispValue(xlevel) + " " + UTILS.crispValue(ylevel) +
	    	" Z");
	    if (style.getDrawFill()) container.setAttribute('d', 
	    	 "M " + UTILS.crispValue(xmin) + " " + UTILS.crispValue(ymax) + 
	    	" L " + UTILS.crispValue(xmin) + " " + UTILS.crispValue(ymin) + 
	    	" L " + UTILS.crispValue(xmax) + " " + UTILS.crispValue(ymin) + 
	    	" L " + UTILS.crispValue(xmax) + " " + UTILS.crispValue(ymax) +" Z");
	    else container.setAttribute('d', 
	    	 "M " + UTILS.crispValue(xmin) + " " + UTILS.crispValue(ymax) + 
	    	" L " + UTILS.crispValue(xmin) + " " + UTILS.crispValue(ymin) + 
	    	" L " + UTILS.crispValue(xmax) + " " + UTILS.crispValue(ymin) + 
	    	" L " + UTILS.crispValue(xmax) + " " + UTILS.crispValue(ymax));
    } else {
	    liquid.setAttribute('d', 
	    	 "M " + (xmin + level[0]) + " " + (ylevel) + 
	    	" L " + (xmin) + " " + (ymin) + 
	    	" L " + (xmax) + " " + (ymin) + 
	    	" L " + (xmax + level[0]) + " " + (ylevel) +
	    	" Z");
	    if (style.getDrawFill()) container.setAttribute('d', 
	    	 "M " + (xmin) + " " + (ymax) + 
	    	" L " + (xmin) + " " + (ymin) + 
	    	" L " + (xmax) + " " + (ymin) + 
	    	" L " + (xmax) + " " + (ymax) + " Z");
	     else container.setAttribute('d', 
	    	 "M " + (xmin) + " " + (ymax) + 
	    	" L " + (xmin) + " " + (ymin) + 
	    	" L " + (xmax) + " " + (ymin) + 
	    	" L " + (xmax) + " " + (ymax) );
	}  	    
		
	// set style
    liquid.setAttribute("stroke","none");
    liquid.setAttribute("stroke-width",0);    	
    liquid.setAttribute("fill",mElement.getLevelColor());
	liquid.setAttribute("shapeRendering",style.getShapeRendering());  	

    if (style.getDrawFill()) container.setAttribute("fill",style.getFillColor());
    else container.setAttribute("fill","none");    
    container.setAttribute("stroke",style.getLineColor());
    container.setAttribute("stroke-width",style.getLineWidth());
	container.setAttribute("shapeRendering",style.getShapeRendering());  	

	var attributes = style.getAttributes();
	for (var attr in attributes) {
	  	liquid.setAttribute(attr,attributes[attr]);
	  	container.setAttribute(attr,attributes[attr]);
	}

	return mShape;         
}