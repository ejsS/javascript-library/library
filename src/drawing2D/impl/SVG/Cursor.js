/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG cursor
 */
EJSS_SVGGRAPHICS.cursor = function(mGraphics, mElement) {  
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape	
	var mGroup = mGraphics.getElementById(mElement.getName());
	var mLine1, mLine2;
	if (mGroup === null) { 	// not exists
	    // create SVG element group
	    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mGroup.setAttribute("id", mElement.getName());
	    group.appendChild(mGroup);
	    // create line1 element
	    mLine1 = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mLine1.setAttribute("id", mElement.getName() + ".line1");
	    mGroup.appendChild(mLine1);	    
	    // create line2 element
	    mLine2 = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mLine2.setAttribute("id", mElement.getName() + ".line2");
	    mGroup.appendChild(mLine2);	    	    	    
	} else {
		mLine1 = mGraphics.getElementById(mElement.getName() + ".line1");
		mLine2 = mGraphics.getElementById(mElement.getName() + ".line2");
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    
  	// type of cursor
	var cursorType = mElement.getCursorType();

    // cursor size
    var panel = mElement.getGroupPanel();
    var bounds = panel.getRealWorldCoordinates(); // xmin,xmax,ymin,ymax
	var bottomLeft = panel.toPixelPosition([bounds[0],bounds[2]]);    
	var topRight   = panel.toPixelPosition([bounds[1],bounds[3]]);    
	    
	var style = mElement.getStyle();
	mLine1.setAttribute("stroke-width",0);   	 	
	if (cursorType!=EJSS_DRAWING2D.Cursor.VERTICAL) {
	  var x1 = bottomLeft[0], x2 = topRight[0];
	  var y = pos[1];
	  // keep the cursor inside the panel
	  if (y>bottomLeft[1]) y = bottomLeft[1];
	  else if (y<topRight[1]) y = topRight[1];
	  if (style.getShapeRendering() == "crispEdges") {
	    mLine1.setAttribute('d', "M " + UTILS.crispValue(x1) + " " + UTILS.crispValue(y) 
	    		+ " L " + UTILS.crispValue(x2) + " " + UTILS.crispValue(y));
	  } 
	  else {
	    mLine1.setAttribute('d', "M " + x1 + " " + y + " L " + x2 + " " + y);
	  }  	  
	  mLine1.setAttribute("stroke-width",style.getLineWidth());   	 	
	}
	mLine2.setAttribute("stroke-width",0);   	 	
	if (cursorType!=EJSS_DRAWING2D.Cursor.HORIZONTAL) {
	  var x = pos[0];
	  var y1 = bottomLeft[1], y2 = topRight[1];
	  // keep the cursor inside the panel
	  if (x<bottomLeft[0]) x = bottomLeft[0];
	  else if (x>topRight[0]) x = topRight[0];
	  if (style.getShapeRendering() == "crispEdges") {
	    mLine2.setAttribute('d', "M " + UTILS.crispValue(x) + " " + UTILS.crispValue(y1) 
	    		+ " L " + UTILS.crispValue(x) + " " + UTILS.crispValue(y2));
	  } 
	  else {
	    mLine2.setAttribute('d', "M " + x + " " + y1 + " L " + x + " " + y2);
	  }  	  
	  mLine2.setAttribute("stroke-width",style.getLineWidth());   	 	
	}
	if (style.getDrawFill()) mGroup.setAttribute("fill",style.getFillColor());
    else mGroup.setAttribute("fill","none");    
	mGroup.setAttribute("stroke",style.getLineColor());
	//mGroup.setAttribute("stroke-width",style.getLineWidth());
	mGroup.setAttribute("shapeRendering",style.getShapeRendering());  
    
    var attributes = style.getAttributes();
	for (var attr in attributes) {
      mGroup.setAttribute(attr,attributes[attr]);
    }			 	  	
	
	return mGroup;         
}