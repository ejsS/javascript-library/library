/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG text
 */
EJSS_SVGGRAPHICS.text = function(mGraphics, mElement) {  
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mGroup = mGraphics.getElementById(mElement.getName());
	var mInnerGroup, mText, mBox;
	if (mGroup === null) { 	// exits?
	    // create SVG element group
	    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mGroup.setAttribute("id", mElement.getName());
	    group.appendChild(mGroup);	    
		// create SVG inner element group
	    mInnerGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mInnerGroup.setAttribute("id", mElement.getName() + ".inner");
	    mGroup.appendChild(mInnerGroup);	    
	    // create box element
	    mBox = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mBox.setAttribute("id", mElement.getName() + ".box");
	    mInnerGroup.appendChild(mBox);	    
	    // create text element
	    mText = document.createElementNS("http://www.w3.org/2000/svg","text"); 
	    mText.setAttribute("id", mElement.getName() + ".text");
	    mInnerGroup.appendChild(mText);	    
	} else {
		mInnerGroup = mGraphics.getElementById(mElement.getName() + ".inner");
		mText = mGraphics.getElementById(mElement.getName() + ".text");
		mBox = mGraphics.getElementById(mElement.getName() + ".box");
	}

	// remove previous text
	while (mText.firstChild) {
	    mText.removeChild(mText.firstChild);
	}	

	// draw text	    		
	var font = mElement.getFont();
	mText.setAttribute("font-style",font.getFontStyle());
	mText.setAttribute("font-weight",font.getFontWeight());	    	
	mText.setAttribute("font-size",font.getFontSizeString());
	mText.setAttribute("font-family",font.getFontFamily());
	
    mText.setAttribute("fill",font.getFillColor());
   	mText.setAttribute("stroke",font.getOutlineColor());
   	mText.setAttribute("stroke-width",font.getOutlineWidth());
    mText.setAttribute("letterSpacing",font.getLetterSpacing());
    
	// get max length and max height	
	var text = mElement.getText();
	var arr = text.split("\n");
	var maxLength;
	var maxHeight;
	var drawingSize = mElement.getDrawingSize();
	if(drawingSize[0] == -1) {
		if (typeof text == 'undefined' || text == null) text = "";
		maxLength = 0;
		maxHeight = font.getFontSize();
	    mText.textContent = "";
		mText.setAttribute("visibility", "hidden");
	  	for (i = 0; i < arr.length; i++) {
	  		mText.textContent = arr[i];
	  		var len = mText.getComputedTextLength();
	  		if(len > maxLength) maxLength = len;
	  		if(i > 0) maxHeight += 1.8 * font.getFontSize();
		}
		mText.setAttribute("visibility", "inherit");
	    mText.textContent = "";		
	    mElement.setDrawingSize([maxLength,maxHeight]);
	} else {
		maxLength = drawingSize[0];
		maxHeight = drawingSize[1];
	}	
	
	// get position of the element center 
    var pos = mElement.getPixelPosition();
	var marginFrame = mElement.getFramed()?4:0; 
	// var bBox = mText.getBBox();	
    // var size = [bBox.width + marginFrame*2, bBox.height + marginFrame*2];
// Paco    var size = [1.15*maxLength + marginFrame*2, maxHeight + marginFrame*2];
    var size = [maxLength + marginFrame*2, maxHeight/1.5 + marginFrame*2];
    var offset = EJSS_DRAWING2D.Element.getSWRelativePositionOffset(
    		mElement.getRelativePosition(), size[0], size[1]);  
    var xmargin = mElement.getMarginX();
    var ymargin = mElement.getMarginY();
    var x = pos[0]+offset[0]+xmargin+marginFrame;
    var y = pos[1]+offset[1]+ymargin-marginFrame;

	// text position	
	mText.setAttribute("x",x);
	mText.setAttribute("y",y);

	// set text keeping newlines
  	for (i = arr.length-1; i >= 0; i--) {
		var ts = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
        if(i == arr.length-1) ts.setAttribute("y", y);
        else ts.setAttribute("dy", "-1.2em");
        ts.setAttribute("x", x);
		ts.textContent = arr[i];
		mText.appendChild(ts);
	}

    if (mElement.getFramed()) { // framed    
    	var tbmargin = marginFrame;
    	var rlmargin = marginFrame;
    	var wlen = size[0] - marginFrame*2;
    	var hlen = size[1] - marginFrame*2;    	
    	
	    // draw box
	    if(wlen > 0) {
	    	var bottom = y + tbmargin, top = y - hlen - tbmargin;
	    	var left = x - rlmargin, rigth = x + wlen + tbmargin;		 		 
			mBox.setAttribute('d', 
			   	 "M " + left + " " + bottom + 
			   	" L " + rigth + " " + bottom + 
			   	" L " + rigth + " " + top + 
			   	" L " + left + " " + top + " z");    
		    var style = mElement.getStyle(); 
		    if(style.getDrawFill())	
		    	mBox.setAttribute("fill",style.getFillColor());
		    else 
		    	mBox.setAttribute("fill","none");    
		    if(style.getDrawLines()) {
		    	mBox.setAttribute("stroke",style.getLineColor());
		    	mBox.setAttribute("stroke-width",style.getLineWidth());
		    } else {
		    	mBox.setAttribute("stroke","none");
		    	mBox.setAttribute("stroke-width",0);    	
		    }       
			var attributes = style.getAttributes();
			for (var attr in attributes) {
			  	mShape.setAttribute(attr,attributes[attr]);
			}		    
		} else {
	    	mBox.setAttribute("stroke","none");
	    	mBox.setAttribute("fill","none");    		
		}
	}
		    
	// angle of text  	 
	var angletext = 0;
	switch(mElement.getWritingMode()) {
		case EJSS_DRAWING2D.Text.MODE_TOPDOWN : angletext = 90; break;
		case EJSS_DRAWING2D.Text.MODE_RIGTHLEFT : angletext = 180; break;
		case EJSS_DRAWING2D.Text.MODE_DOWNTOP : angletext = 270; break;
		case EJSS_DRAWING2D.Text.MODE_LEFTRIGHT: angletext = 0; break;
	} 	  	  	
    mInnerGroup.setAttribute("transform","rotate(" + angletext + " " + (pos[0]+xmargin) + " " + (pos[1]+ymargin) + ")"); 
    
    // avoid pointer events in mobiles
    mGroup.setAttribute('pointer-events', 'none');

	return mGroup;         
}