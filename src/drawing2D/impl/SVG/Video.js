/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG video
 */
EJSS_SVGGRAPHICS.video = function(mGraphics, mElement) {  
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());	
	var myVidSrc;	
	if (mShape === null) { 	// exits?
	    // create SVG element
	   	mShape = document.createElementNS("http://www.w3.org/2000/svg","foreignObject");
	    mShape.setAttribute("id",mElement.getName());	   
	    group.appendChild(mShape);
	    // create video src 
		myVidSrc = document.createElement('video');
		myVidSrc.setAttribute("id", mElement.getName() + ".video");

		// select src
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
		if (navigator.getUserMedia && mElement.getWebCam()) {       
	   		navigator.getUserMedia({video: true}, function(s) { myVidSrc.src = window.URL.createObjectURL(s); }, function(e) {} );
		} else {	
			myVidSrc.src = mElement.getVideoUrl();
		}		

		mShape.appendChild(myVidSrc);	    
	}
	else {
		myVidSrc = document.getElementById(mElement.getName() + ".video");
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);
    
 	// set video src attrs
 	myVidSrc.loop = mElement.getLoop();	
 	myVidSrc.type = mElement.getType();
 	myVidSrc.poster = mElement.getPoster();
	myVidSrc.autoplay = mElement.getAutoPlay();
	myVidSrc.controls = mElement.getControls();
	if(!isNaN(myVidSrc.duration) && (myVidSrc.duration >= mElement.getCurrentTime()))
		 myVidSrc.currentTime = mElement.getCurrentTime();
	myVidSrc.width = Math.abs(size[0]);
	myVidSrc.height = Math.abs(size[1]);
	
 	// set SVG element	
	mShape.setAttribute("x",(x-mx));
	mShape.setAttribute("y",(y-my));
	
	// play video 
	if(mElement.isPlay()) 
		myVidSrc.play()
	else 
		myVidSrc.pause();	 	
	
	var attributes = mElement.getStyle().getAttributes();
	for (var attr in attributes) {
	  	mShape.setAttribute(attr,attributes[attr]);
	}    	    	    					  	
	
	return mShape;         
}