/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG wheel
 */
EJSS_SVGGRAPHICS.wheel = function(mGraphics, mElement) {  
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape	
	var mGroup = mGraphics.getElementById(mElement.getName());
	var mCircle, mCross;
	if (mGroup === null) { 	// exits?
	    // create SVG element group
	    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mGroup.setAttribute("id", mElement.getName());
	    group.appendChild(mGroup);	    
	    // create ellipse element
	    mCircle = document.createElementNS("http://www.w3.org/2000/svg","ellipse"); 
	    mCircle.setAttribute("id", mElement.getName() + ".circle");
	    mGroup.appendChild(mCircle);	    
	    // create cross element
	    mCross = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mCross.setAttribute("id", mElement.getName() + ".cross");
	    mGroup.appendChild(mCross);	    
	} else {
		mCircle = mGraphics.getElementById(mElement.getName() + ".circle");
		mCross = mGraphics.getElementById(mElement.getName() + ".cross");		
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
    
	// set circle atts
    mCircle.setAttribute("cx",x);
    mCircle.setAttribute("cy",y);
    mCircle.setAttribute("rx",Math.abs(mx));
    mCircle.setAttribute("ry",Math.abs(my));
      	
	// set cross atts
    mCross.setAttribute('d', 
    	 "M " + (x-mx) + " " + y + 
    	" L " + (x+mx) + " " + y + 
    	" M " + x + " " + (y-my) + 
    	" L " + x + " " + (y+my));
	
	// set style
    var style = mElement.getStyle(); 
    if(style.getDrawFill()) 	
    	mGroup.setAttribute("fill",style.getFillColor());
    else 
    	mGroup.setAttribute("fill","none");    
    if(style.getDrawLines()) {
    	mGroup.setAttribute("stroke",style.getLineColor());
    	mGroup.setAttribute("stroke-width",style.getLineWidth());
    } else {
    	mGroup.setAttribute("stroke","none");
    	mGroup.setAttribute("stroke-width",0);    	
    }        
	mGroup.setAttribute("shapeRendering",style.getShapeRendering());  	

	var attributes = style.getAttributes();
	for (var attr in attributes) {
	  	mGroup.setAttribute(attr,attributes[attr]);
	}    	    	    					  	

	return mGroup;         
}