/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG scalarField
 */
EJSS_SVGGRAPHICS.scalarField = function(mGraphics, mElement) {	
	var UTILS = EJSS_SVGGRAPHICS.Utils;   

	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());	
	var myCanvasSrc;	
	if (mShape === null) { 	// exits?
	    // create SVG element
	   	mShape = document.createElementNS("http://www.w3.org/2000/svg","foreignObject");
	    mShape.setAttribute("id",mElement.getName());	   
	    group.appendChild(mShape);
	    // create canvas src 
		myCanvasSrc = document.createElement('canvas');
		myCanvasSrc.setAttribute("id", mElement.getName() + ".canvas");

		mShape.appendChild(myCanvasSrc);	    
	}
	else {
		myCanvasSrc = document.getElementById(mElement.getName() + ".canvas");
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];

    var width = Math.round(Math.abs(size[0])); 
    var height = Math.round(Math.abs(size[1]));
				
	// get half sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);
    

 	// set SVG element	
	mShape.setAttribute("x",(x-mx));
	mShape.setAttribute("y",(y-my));    	
	if(width != +mShape.getAttribute("width") || height != +mShape.getAttribute("height")) {
	    mShape.setAttribute("width",width);	   
	    mShape.setAttribute("height",height);			
	    myCanvasSrc.setAttribute("width",width);	   
	    myCanvasSrc.setAttribute("height",height);	   
	}
	
    //draws all objects within this.drawables
	var colors = mElement.getColorMapper().getColors();
	//if(mElement.getAutoscaleZ()) mElement.getColorMapper().setAutoscaleArray2(data);
	var data = mElement.getData();
	var xlen = data.length;
	if (xlen > 0) {
		var ylen = data[0].length;
		var zmin = mElement.getMinimumZ();
		var zmax = mElement.getMaximumZ();
        var params = {
			width: width,
			height: height,
			data: data,			
			nx: xlen,
			ny: ylen,
			colors: colors,
			lower: zmin,
			upper: zmax	
		}
		if (mElement.getParallelDrawing()) {
			
			const p = new Parallel(params);
			p.spawn(EJSS_GRAPHICS.GraphicsUtils.drawImageFieldParallel)
			.then( function(image) {
		    	var context = myCanvasSrc.getContext("2d");
		    	context.fillRect(0,0,width,height);					    	
				var imageData = context.getImageData(0, 0, width, height);  // parameters must be integers
			    var arr = imageData.data;
				for (var i = 0, l = image.length - 4; i < l; i += 4) {				
					arr[i] = image[i];
					arr[i+1] = image[i+1];
					arr[i+2] = image[i+2];
				}
				context.putImageData(imageData, 0, 0);
			});			
		} else {
			var context = myCanvasSrc.getContext("2d");
		    context.fillRect(0,0,width,height);					    				
			EJSS_GRAPHICS.GraphicsUtils.drawImageField(context, params);
		}
	}

	var attributes = mElement.getStyle().getAttributes();
	for (var attr in attributes) {
	  	mShape.setAttribute(attr,attributes[attr]);
	}    	    	    					  	
	
	return mShape;         
}
