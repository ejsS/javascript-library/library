/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG spring
 */
EJSS_SVGGRAPHICS.spring = function(mGraphics, mElement) {

	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());	
	if (mShape === null) { 	// exits?
	    // create SVG element
	    mShape = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	    mShape.setAttribute("id", mElement.getName());
	    group.appendChild(mShape);	    
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
    
 	// get spring radius	    	
    var radius = mElement.getGroupPanel().toPixelMod([mElement.getRadius(),0])[0];

	// set attributes
	var springpath = "";
	EJSS_GRAPHICS.GraphicsUtils.drawSpring(mElement.getLoops(), mElement.getPointsPerLoop(), 
			radius, mElement.getSolenoid(), mElement.getThinExtremes(), x-mx, y-my, size[0], size[1],
			function(xx,yy) { springpath += " M " + xx + " " + yy; }, 
			function(xx,yy) { springpath += " L " + xx + " " + yy; });   
			
    if (springpath !== "") {
	    mShape.setAttribute('d', springpath);
	    		
		// set style
	    var style = mElement.getStyle(); 	
	    if(style.getDrawFill())	
	    	mShape.setAttribute("fill",style.getFillColor());
	    else 
	    	mShape.setAttribute("fill","none");    
	    if(style.getDrawLines()) {
	    	mShape.setAttribute("stroke",style.getLineColor());
	    	mShape.setAttribute("stroke-width",style.getLineWidth());
	    } else {
	    	mShape.setAttribute("stroke","none");
	    	mShape.setAttribute("stroke-width",0);    	
	    }        
		mShape.setAttribute("shapeRendering",style.getShapeRendering());

		var attributes = style.getAttributes();
		for (var attr in attributes) {
		  	mShape.setAttribute(attr,attributes[attr]);
		}		  	
	}
	
	return mShape;         
}