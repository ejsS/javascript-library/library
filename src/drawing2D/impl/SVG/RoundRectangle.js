/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG round rectangle
 */
EJSS_SVGGRAPHICS.roundRectangle = function(mGraphics, mElement) {  
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape
	var mShape = mGraphics.getElementById(mElement.getName());	
	if (mShape === null) { 	// exits?
	    // create SVG element
	    mShape = document.createElementNS("http://www.w3.org/2000/svg","rect"); 
	    mShape.setAttribute("id", mElement.getName());
	    group.appendChild(mShape);	    
	}

	// get position of the element center 
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = Math.abs(size[0]/2);
    var my = Math.abs(size[1]/2);
    
    // get element radius
    var radius = Math.abs(mElement.getCornerRadius());
    
 	// set attributes	   
	mShape.setAttribute("x", x-mx);
	mShape.setAttribute("y", y-my);
	mShape.setAttribute("width", Math.abs(size[0]));
	mShape.setAttribute("height", Math.abs(size[1]));	    	
	mShape.setAttribute("rx", radius);
	mShape.setAttribute("ry", radius);
	
	// set style
    var style = mElement.getStyle(); 
    if(style.getDrawFill())	
    	mShape.setAttribute("fill",style.getFillColor());
    else 
    	mShape.setAttribute("fill","none");    
    if(style.getDrawLines()) {
    	mShape.setAttribute("stroke",style.getLineColor());
    	mShape.setAttribute("stroke-width",style.getLineWidth());
    } else {
    	mShape.setAttribute("stroke","none");
    	mShape.setAttribute("stroke-width",0);    	
    }        
	mShape.setAttribute("shapeRendering",style.getShapeRendering());  	

	var attributes = style.getAttributes();
	for (var attr in attributes) {
	  	mShape.setAttribute(attr,attributes[attr]);
	}

	return mShape;         
}