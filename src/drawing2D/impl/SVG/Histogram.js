/**
 * Deployment for 2D SVG drawing.
 * @module SVGGraphics 
 */

var EJSS_SVGGRAPHICS = EJSS_SVGGRAPHICS || {};

/**
 * @param mGraphics Element where draw
 * @param mElement Element to draw
 * @returns A SVG histogram
 */
 EJSS_SVGGRAPHICS.histogram = function(mGraphics, mElement) {  
	
	// creates area bar	
	function areaBar(bar, barx1, barx2, bary, axis, barstyle) {		
	 	// set attributes	    	    
	    bar.setAttribute('d', 
	    	 "M " + barx1 + " " + axis + 
	    	" L " + barx1 + " " + bary + 
	    	" L " + barx2 + " " + bary + 
	    	" L " + barx2 + " " + axis + " z");
	
		// set style
	    if(barstyle.getDrawFill())	
	    	bar.setAttribute("fill",barstyle.getFillColor());
	    else 
	    	bar.setAttribute("fill","none");    
	    if(barstyle.getDrawLines()) {
	    	bar.setAttribute("stroke",barstyle.getLineColor());
	    	bar.setAttribute("stroke-width",barstyle.getLineWidth());
	    } else {
	    	bar.setAttribute("stroke","none");
	    	bar.setAttribute("stroke-width",0);    	
	    }        
		bar.setAttribute("shapeRendering",barstyle.getShapeRendering());  	
	}  	
		
	// get element group
    var group, elementGroup = mElement.getGroup();
    if(elementGroup !== null) 
    	group = mGraphics.getElementById(elementGroup.getName());
    else 
    	group = mGraphics;
    	
    // get shape		
	var mGroup = mGraphics.getElementById(mElement.getName());
	if (mGroup === null) { 	// exists?
	    // create SVG element group
	    mGroup = document.createElementNS("http://www.w3.org/2000/svg","g"); 
	    mGroup.setAttribute("id", mElement.getName());
	    group.appendChild(mGroup);	    
	} else {
		// remove Bars
		var mBars = mGraphics.getElementById(mElement.getName() + ".bars");
		mGroup.removeChild(mBars);
	}
    // create Bars element
    var mBars = document.createElementNS("http://www.w3.org/2000/svg","g"); 
    mBars.setAttribute("id", mElement.getName() + ".bars");
    mGroup.appendChild(mBars);	    

	// get position of the element center 	
    var pos = mElement.getPixelPosition();
    var size = mElement.getPixelSizes();     
    var offset = mElement.getRelativePositionOffset(size);  
    var x = pos[0]+offset[0];
    var y = pos[1]+offset[1];
	
	// get half sizes 		
    var mx = size[0]/2;
    var my = size[1]/2;
	
	// get style        
	var style = mElement.getStyle(); 
        
    
	// get histogram parameters
	var discrete =  mElement.getDiscrete();
 	var halfBin = mElement.getBinWidth()/2;
 	var bars = mElement.getBars();

	var axisx = mElement.getGroupPanel().getPixelPositionWorldOrigin()[1];
	for (var j=0; j < bars.length; j++) { // create bars
      var mBar = document.createElementNS("http://www.w3.org/2000/svg","path"); 
	  mBar.setAttribute("id", mElement.getName() + ".bar" + j);
	  mBars.appendChild(mBar);	 
	  var barx1, barx2;
	  var cbin = bars[j][0];
	  if (discrete) {
        barx1 = barx2 = (x-mx) + cbin*size[0];
	  } 
	  else {
		barx1 = (x-mx) + (cbin-halfBin)*size[0];	// Bar x
		barx2 = (x-mx) + (cbin+halfBin)*size[0];	// Bar x
	  }
	  var bary = (y-my) + bars[j][1]*size[1];  // Bar y
	  areaBar(mBar, barx1, barx2, bary, axisx, style);
    }

	var attributes = style.getAttributes();
	for (var attr in attributes) {
	  	mGroup.setAttribute(attr,attributes[attr]);
	}    	    	    			
		
	return mGroup;         
}
