/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * Deployment for 2D drawing.
 * @module 2Dgraphics 
 */

var EJSS_GRAPHICS = EJSS_GRAPHICS || {};

/**
 * SVGGraphics class
 * @class SVGGraphics 
 * @constructor  
 */
EJSS_GRAPHICS.CanvasGraphics = {

  /**
   * Return document box
   * @return box  
   */
  getOffsetRect: function(graphics) {
  	return EJSS_GRAPHICS.GraphicsUtils.getOffsetRect(graphics);
  }
};

/**
 * Constructor for Element
 * @param mName Identifier Canvas element in HTML document
 * @returns A Canvas graphics
 */
EJSS_GRAPHICS.canvasGraphics = function(mName) {
  var self = EJSS_INTERFACE.canvas(mName);	// reference returned     
  var mInterfaceGraphics = self.getDOMElement();
  var mContext = self.getContext();

  /**
   * Return canvas for event handle
   * @return canvas element 
   */
  self.getEventContext = function() {  	
    return mInterfaceGraphics;
  };

  /**
   * Return canvas document box
   * @return width integer 
   */
  self.getBox = function() {
  	var box = EJSS_GRAPHICS.CanvasGraphics.getOffsetRect(self)
  	box.width -= 1;
  	box.height -= 1;
  	return box;
  };

  /**
   * Reset the Canvas graphics
   */
  self.reset = function() {
  	var rect = EJSS_GRAPHICS.CanvasGraphics.getOffsetRect(self);
  	mContext.clearRect(0, 0, rect.width, rect.height);
  };

  /**
   * Draw Elements
   * @param elements array of Elements and Element Sets
   * @param force whether force draw elements
   */
  self.draw = function(elements,force) {  	
    for (var i=0, n=elements.length; i<n; i++) { 
      var element = elements[i];
      if (element.isGroupVisible()) {
		  	self.drawElement(element, self.transformElement);
	  }  	
    }
  };
    
  /**
   * Draw Element
   * @param element
   */
  self.drawElement = function (element, transformCallBack) {
  	var Shape = EJSS_DRAWING2D.Shape;
  	var cl = element.getClass();
	
	if (cl === 'ElementImage') {
		EJSS_CANVASGRAPHICS.image(mContext,element,transformCallBack);
	} else {
		mContext.save();
		transformCallBack(element);

	  	switch (cl) {
			case "ElementCustom": 
			  {
			    var customFunction = element.getFunction();
			    if (customFunction) customFunction(mContext,element);	
			  }
			  break;
	  		case "ElementShape": 
				switch (element.getShapeType()) {
				  default : 
				  case Shape.ELLIPSE         : EJSS_CANVASGRAPHICS.ellipse(mContext,element); 	break;
				  case Shape.RECTANGLE       : EJSS_CANVASGRAPHICS.rectangle(mContext,element);	break;
				  case Shape.ROUND_RECTANGLE : EJSS_CANVASGRAPHICS.roundRectangle(mContext,element); break;
				  case Shape.WHEEL           : EJSS_CANVASGRAPHICS.wheel(mContext,element); 		break;
				  case Shape.NONE            : // do not break;
				  case Shape.POINT           : EJSS_CANVASGRAPHICS.point(mContext,element); 		break;
				}
				break;
			case "ElementSegment": 	EJSS_CANVASGRAPHICS.segment(mContext,element);	break;
			case "ElementArrow":	EJSS_CANVASGRAPHICS.arrow(mContext,element);	break;
			case "ElementText":		EJSS_CANVASGRAPHICS.text(mContext,element);		break;
			case "ElementSpring":	EJSS_CANVASGRAPHICS.spring(mContext,element);		break;
			case "ElementTrail":	EJSS_CANVASGRAPHICS.trail(mContext,element);		break;
			case "ElementTrace":	EJSS_CANVASGRAPHICS.trace(mContext,element);		break;
			case "ElementGrid":		EJSS_CANVASGRAPHICS.grid(mContext,element);		break;
			case "ElementAxis":		EJSS_CANVASGRAPHICS.axis(mContext,element);		break;
			case "ElementCursor":	EJSS_CANVASGRAPHICS.cursor(mContext,element);		break;
			case "ElementPolygon":	EJSS_CANVASGRAPHICS.polygon(mContext,element);	break;
			case "ElementAnalyticCurve":	EJSS_CANVASGRAPHICS.analyticCurve(mContext,element);	break;
			case "ElementCellLattice": 	EJSS_CANVASGRAPHICS.cellLattice(mContext,element);	break; 			
			case "ElementByteRaster": 	EJSS_CANVASGRAPHICS.byteRaster(mContext,element);	break;			
			case "ElementMesh": 	EJSS_CANVASGRAPHICS.mesh(mContext,element);		break;			
			case "ElementTank":   	EJSS_CANVASGRAPHICS.tank(mContext,element);		break;
			case "ElementPipe":   	EJSS_CANVASGRAPHICS.pipe(mContext,element);		break;
			case "ElementHistogram":EJSS_CANVASGRAPHICS.histogram(mContext,element);		break;

			case "ElementScalarField":	EJSS_CANVASGRAPHICS.scalarField(mContext,element);		break;
			case "ElementCanvas":	EJSS_CANVASGRAPHICS.canvas(mContext,element);		break;

			case "ElementVideo":   	Console.log('Not supported ElementVideo in Canvas!');	break;
		}		
		mContext.restore();
	}
  };

  /**
   * Transform Element
   * @param element
   */
  self.transformElement = function (element) {
  	// first, group transform 
  	if(element.getGroup() != null)
  		self.transformElement(element.getGroup())
	
	// second, my transform  	
	var trans = element.getTransformation();
  	var rot = element.getRotate();
  	if (Array.isArray(trans) && trans.length > 0) {  
    	console.log("Array transformations are not supported in Canvas!");  	
	} else if (typeof trans === "string") {  // SVG format
    	console.log("SVG-based transformations are not supported in Canvas!");  	
	} else if (rot != 0) { // && trans!=0) {  // rotation angle
	    var pos = element.getPixelPosition();	
      	mContext.translate(pos[0],pos[1]);
      	mContext.rotate(-rot);
      	mContext.translate(-pos[0],-pos[1]);
	}	  	
  }

  /**
   * Draw gutters
   * @param gutters
   */
  self.drawGutters = function(panel) {
  	var gutters = panel.getGutters();
  	
  	if (gutters.visible) {    	 			
	 	// get gutters position     
	    var inner = panel.getInnerRect();
	    
	 	// get panel position
	    var x = 0.5, y = 0.5;
	    var box = self.getBox();
	    var sx = box.width, sy = box.height;
	    
	 	var guttersStyle = panel.getGuttersStyle();	 
	 	var panelStyle = panel.getStyle();   

        if (guttersStyle.getDrawFill()) { // create path	    	   	    
	      mContext.beginPath();
		  mContext.moveTo(x,y);
		  mContext.lineTo(x,y+sy);
		  mContext.lineTo((x+sx),(y+sy));
		  mContext.lineTo((x+sx),y);
		  mContext.lineTo(x,y);
		  mContext.moveTo(inner.x,inner.y);
		  mContext.lineTo(inner.x + inner.width, inner.y);
		  mContext.lineTo(inner.x + inner.width, inner.y + inner.height);
		  mContext.lineTo(inner.x, inner.y + inner.height);
		  mContext.lineTo(inner.x,inner.y);
		  mContext.moveTo(x,y);
	      mContext.closePath();
	      mContext.fillStyle = guttersStyle.getFillColor();
	      mContext.fill();
	    }
	    if (guttersStyle.getDrawLines()) { // Outer border
	      mContext.beginPath();
		  mContext.moveTo(x,y);
		  mContext.lineTo(x,y+sy);
		  mContext.lineTo((x+sx),(y+sy));
		  mContext.lineTo((x+sx),y);
		  mContext.lineTo(x,y);
	      mContext.closePath();
	      mContext.lineWidth = guttersStyle.getLineWidth();
	      mContext.strokeStyle = guttersStyle.getLineColor();
	      mContext.stroke();
        }
	    if (guttersStyle.getDrawLines()) { // Inner border
	      mContext.beginPath();
		  mContext.moveTo(inner.x,inner.y);
		  mContext.lineTo(inner.x + inner.width, inner.y);
		  mContext.lineTo(inner.x + inner.width, inner.y + inner.height);
		  mContext.lineTo(inner.x, inner.y + inner.height);
		  mContext.lineTo(inner.x,inner.y);
	      mContext.closePath();
	      mContext.lineWidth = panelStyle.getLineWidth();
	      mContext.strokeStyle = panelStyle.getLineColor();
	      mContext.stroke();
        }
	}  	
  };
  
  /**
   * Draw panel
   * @param panel
   */
  self.drawPanel = function(panel) {
 	// get position
    var x = 0.5, y = 0.5;
    var box = self.getBox();
	
    mContext.beginPath();
    mContext.rect(x, y, box.width, box.height);
    
    // style
    var style = panel.getStyle();
    mContext.fillStyle = style.getFillColor();
    mContext.fill();
    if (style.getDrawLines()) {
      mContext.lineWidth = style.getLineWidth();
      mContext.strokeStyle = style.getLineColor();
      mContext.stroke();
    }	
	
  };
  
 return self;     
      
}
