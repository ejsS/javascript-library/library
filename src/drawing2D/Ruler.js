/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Ruler
 * @class Ruler 
 * @constructor  
 */
EJSS_DRAWING2D.Ruler = {
  sROTATE_ICON : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGgAAAB3CAYAAADvnAXvAAAAAXNSR0IArs4c6QAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAGuVJREFUeAHtXQl0HMWZ7qqeGY0kyzaHDYvBNgSzS4zBxEiyzSWwLc3IxiHJOglJFgK5s9nkhU3yICFgcl+b+yAHZMNCksVsSACdxmAIIEu2A0sSctnBa8xlA7Yj2ZJG01X7/T3T456e6p7unp6R8h71nlRVf/3111/11V3VNUybeoadn04fOybEWZzHLpBSrmGadnZYNaWm7UT8X0vDeETyxIBIaE9tv+eeUchD0NQ30H3yzbJly+rF9KPaGdM+gVJrroVGUsjtjMvr68fG7t+8efNYLdIMk8ZkAsSa0+kLuGQ/geLz3JS3KxhFlVfJE5r2PJPsjUN9XQ9DjyiScctOYLpd38CRQ0bgLR3p9zPGvuUVX6VYJSXnV56U2vuG+rq/D90qSc4ra4HCVHoHEhCEuSWVep2m8TuRKPeKl1PKXj6sotIKKg8pC6aJCwd7e6lFTaqpCUDLOjqOFkz/DXI6r1xuVVBIzV1NIYShcf4018RTmuRPC67t50IelIwJJqWOhnA0uq9jGBdzDI0t4Bo73ktekX5CdA/2914CGnrByTHuOY9In9ZU5zUQ9Xk/4oKAw5j8HZfyqkd7e7f6kE35ZOvWrWN7NC1hDI8uZlr2zdLQXst0Xq7SvJiN66dj5veij3QiZ6kaQAvXrUs0DR96GN2Fr1lZEHCsUhCa/I2e5W/bcl/XHyxaULt1xdrjRGziBk2yqzjX6lTxqcuLiexZj/b3/04VXk1aVQBamkrNFxr/I4QrM+zMUBhwbDImMLC/HQP7T220wM62trbkofrGz6BVfhCR43YBln5M8HMH+rsetYdV2x05QABnrdT4r8oqjsEDY8c2VM79GJAN4hcSdVgyjhleHLOCJownp2A2cXRZWWDAgvazQ3091/nh9eJZcsklx8YmJn6GecxK4rPAseJIyS8e7Ot6wPJX244UoNaO9PUaYzeWUfo5FOcu8BhSiDM45zPt/EUDuDAOCK4nAVLSzuPmltL43lBf3/vdwoPQz2lPU5f3HWfapJ8Q7NytNWpJkQHU3N7xXc7197kWAgobedslJJsFUOY4aybFKwInLwjt7DbOtDsR9+MgteTJJZYlDzsE38fM670lDCEIy9vbz8jqeheX2lyKbtcP4J050N392xBiA0WJBKDWjtRPNcYv80j5KQzodZjinkA8VmHa+e2ZJzp6wGfiWX7uo/d3/5/F19reuQoN7w6N60WtzilPCvblwf7uj1nxKrHPW736qIxhPMA0fpZTTlYaJ2zv60OPUD1TMUDnrEp/T9eZssai9o+jpu1Fac/BeGMuTp2FSVlzgiM1sWleU1PHhg0bzLHJnn1z327GzE2AeRnRXeVJ+ZHBvp7/sMcN616yZEk8Nmv2g1aalhzM7sYbxg7PrOZeXkUANbenrkO5f9pS2G4TOGgHGYQ3WXTXwrQYyJby6yjYD9tJCjdrTa2+CaPBu51hRWALuW6wv+dOJ08YP9ZQ+u6RkUFMYpY44u8d7O0+HjTgFb0JDdDSVelzpc7ct0JyszSz1ZDavsDRxOexvUJjjR/DlrWnPic5o4WwaYrAydMMwRZt6++KZP2yZMm743zWnj8jU/Pz4i1rC0AyW7RFiMoOBVBrOj0dNemgXyX8gANFvralt/tqHzJJZ6u2sqUd6S8A/Y+pwCnIYnLGYE/P3wr+Chznrl3blBnNvMB1Xm8XYwhx9bb+3q/ZaVG4CzU8gDAmDPmyX34/4GACcbdPcChZ2YZFZT59uaWv5xrE/y9PfST7E8LD5LVE7CN33z3MY+wcZ4DO+VdpDeWkV+oPrHRrR+ctGFewCVne+AFHk8bjW5umvb68tCMcNCjTmJCnyKHe3iuYoXntyR2PI44fHpFQmQut8Uk04xudUuIT2X1OWqV+6i58m+b29pNwDL3bTwRf4GjawQyTCx7r6ak4Y6em03XHZOUeTefutZjJhVS4fvT3wcOa2ztfwNx0FvFa+ZVSXD/Y16ecOPmQWcISpAUxrvEdJRIUBEtZe5B6jGDvjQIcSmdHT884Z+J8e5olbkNieh5NVwc5ErtSaygNe34Z459CFxwjehTG6ibKylqaTn8Ri9ELyjHalbV4leBI7dbBvu7PWDx+7DRayY4dO0rWRlbcPTt3vnjiglORJ3ahRSuyGZt20qtOTR43c8aDzz33nDXRKGIJ4nl255+fPfHUU05jGltkjzcRjw8/s2NHJJuqvrq4trZ100aTh4btSqjcfsHBGmlfbHj/vIGBAbpdE9SQzl6Fy7DjsBvt5ESnYNIPJ2+HEvrEmclx7SWUo+zpGRwBX5gDOavsJDaIsSBm5gw0XxkNTLsjaUW+urhDyZHbnZl1+v2CY8ZjxkUhwaHoXuCY4RzyzXRs/yz9kOHGrBH7UATgkB6WLub9CltPoWtHJjE2LYI7ywO0fj3GQbbWS7SVeTuPTVk7GR23dsfWvr7fFxEj9gz09e1AyRWOPJz6oQVftXeiiVpA2JZjAWNqrhtGxpnf1pHDN0SRrbIAtQ4MXeGVkDPzxOtU1h7fSOhX2v3VcscTsX+hHVeVfozzafF43eUh0iZQi8CBnz2ycePzWC48VCRPyk9qqNxFtBCecgLQeLRb3OSqMu8FDmruN3G2f9hNXpR0WlBCXsnax9IPxxIfCJieChwSkW+J+jed8s7ZMvQVJy2o3xOg1lT6e24Cg4JDcjAxKOybucmNkh5LJj4qBK6G5I0Fjull2mnL2lcvt8LK2GXA0bS50xt/iQ5zj10OE4KOzysyngChcrxLJT0MOELK/65gYqBSoyyNWhG2YH5KjEXg5GNii+itZYXkWoizW6NoRaDR0QjT2a12ebTjQkcVdlpQtytAdJctr0SRzDDgkIC4NCquTUWK+PTEmPx3FThmdCne6GNRWRYcSxWZZf9puS1bP2a29+LZYnSxXQHKMl7Sp4YFRzO0J3Flaa+LDlUl/9rcRhIPKBPBttDLiYSvm0eO+CrQtMGN9/4Fs9THHLzfcPgDeV0BwuygqPmHBgfqSCb/LZBWETNnOf+Qm0hjxowJt7AwdHyhscEeD3eUzoCfusNQRgkQXeazS6sEHEx1M/NmTHvQLq/W7u10uUOI51XpNh48OEdFD01j8jvOuEs6OujENZRRAsT4kfGiInCgEu623aG6WxBK2woiCcaUU16u6ysrEFsSlQ4G6fqXFUDll9C0d1r+oLYSIKnLj5KgSsEhGdyIfYrsyTaJuvgPlDpI/iYlvQIidmvNC5RW+WHX+9qw4koAwqlgA4TFLeF2wa6zITuTzY2F/DAGTl9HFLZoVXHmF650slpkpCZXgBB6jCgSlvdgq+lllJ99c7nex2xRJQp7vg6jZ8SSKMAhsQD0LtNypDFZXnyPcoMqbVTKovsFKp6gNJTht+1xDtfVLbT7/bpLAOJMXuWMHLTlWPEZi33Jck8Fe9ww7nXqQZUxNjYW7UQBieCaeWGz1iw/xj/sTNuPvwQgiC6aXocFB92b0Tg+XNKl+FGqWjxP9Pcfws7PC5b8Qk/B46+2aFHZycMJM+9W+eFQ74owsosAoitFEFLYmrCEhxGsS/YbXO7IholbzTj4dOLrJL8ATs69LOo0Z83CaaBjaAuz7VMEUCaT+SdLUadwi+7Hpszjk0OzIPzw15IHe3M/s4Njps1kKmod8kuLUbvc2DHHn2n3+3EXAYRav4oiVQoOycgkEv1kTzUz/sILzzp1wvdMZ4EW6Uwul4Ys3mLi4iPOtMv5iwDCNsVbogCHtvhx7vNyucQnI3z79u0TGB4PWGlb+cUnm4Wu3Qqr1JZMe6hYBntDsb+8zw4QExoLNRWkZOzdBmaCj4MU5ji5vMYRcDCmP0FiLHDInRgZmUF2lAafTA455MWDrocKAC1ZuXK6Q5hvrx0cMxLn3/cdeRIY0Zc9YQeHVIgL/YSoVdF1+VenzEPJprOdNC9/ASBd1+d7MbqFlYADRmwP3+/GP9n0/EzKbEFFuojsaUX+CDwj2eyLTjFcMwKth2KWAM70s5WHHBaDwlaBQzUzcaCxZCBWRI+UdGZ7e2O90BdpXM7HpZDZED4d3w814uPiwuCPj5NxFYFlDMlOwWeVRQbvJdAMq+iooIghhOeJ5ctHW7c4ejkhLlqa6lxPemEoSFj64bENHCVpYxgs9mq6tocZsb9kXnp2ZwEgBC4NooMbOJAhBwY2IKHamnqu/4/GWYc1GpJ+ZjeGmY9lMMaaczUnOBTOpLbY4ovMXr9eaKnOvZBHFSZnOMdLJ9oNpJZdP6pGhW4XDJIbmj7rOBwGHDGdR5zeLg9wcPtI7EZsJFFbw2P6O6wUvfSzeJw2Cij0BMkpy+5HQdxm95Pbv37sZ3aATnIKUvnLCUdXsVMVr9q0gXvvfUYKbbCcfh562MvCgy1YEJfG7fYYQfQz9j1/ZU4pnxfs/AhHT7rNrlAt3ei3X+tMr9BtOANyfsxntKfxd1s8rl+gZqmMuqWv7zG8HfFpSBn2U35Wahg/L6I1m9lBY26eHE02FG1LWIyW7V+4uAzfmf7cildru7kj3a0zLU3pOsFhhjzvpJnTtkzyCS9b3NY2I5ms3+/UzyorwxBd2zb2riG/2YIO63qjFaiy/YOD2IztUsmoFU0YE5dRWqrMxxL8yUkGx1QtmUzOVOmXL6NRgHNJ3m0d2NUdYxGcdiBwKHJWf8kpo5b+7ffddxDwfEuV5kQ2e5WKXlMahhPs/bmuE8fHErRgLkyycmNQQpZ8S0NKBwYHcXSWQQFNrhlualJvSjL9mvyRyqQpiHXRZ5D4ySoFMO684fHNvyzsExJPDqCsKFlFhwGHBI7X1Y2QPZnm9xs2ZPBdw78qdDg2O5Y5sLCtbZoirOqk1pVpWmteq0oIq7buLb29v3CGmQCxWKxoHyosOCQcu9g1X6Q6M0X+rcubbxLocEvCcGG6ob5+dQm9yoRFePNHxtgjqmTwxsLIUG+POSlwhucA0rSjrIBKwIEMmraiXKaAwSoe51vmbM6pDW7N3uSkVdNPO9gNhvxfTJlzPZYjsdHM2D+AVBh37MFmBOwHmZOECsEhuTX59seeAS/3lr6u+zQpaKul2Eht5rKL18wpJlbNx0brGu6GdOVGAK13fr95s+uwkG9BbHYE4KDpyENVy2ZYwSJ2njJqwvi6kh4xEY/q4uv43LrMKVpI8SOMO5uddLs/1+SkKBk0Pebp9vgON8uAYC5+HQGT5qUvDlBxBuwKUGVEf/LP9BacnR61G6+bXA6Z5i1dp2zcetqzta/3XU66059rQUxSwRZMOHBy0dHf6gVBU8RRp/PCpMDeU0wYpRfdo1IZn+a34XjjJ27yxEv7TnELs9NNgABIAaBKwMFxCx8expbTFDMPd3Xtxz2JH9jBIRXhv2zJykvmRq3uOek0ztb4A65yDX4a7bO5htsC8l2cZgJUETjqSYgtqcl1HqtrH8SzwkUzTMpvLG7cB82CVipX/paOjvN1w33DGE9pXE3drt/SyAHEtIlIwJEy4TfhWvP14C0fbHN8wEq3kF+pLWhelb6ZNoytsDI2gaOaErNla9bMwa8R4OCQ58rVKUjIAbzM9TUn2ctvdXG05R7K2LsNrC8aQgmpUaRUaytdZjlcACefLtfZlaP1jQXwPNRxBYfGXiObvQPQzFLGF2IMz3OeqwzzIOaQFrL0EoVHJCvIDg7R0H80oG8tXb1bESbZXo/FqyH4KpUa+Ar9Wtxs8rp65QoO5MnDyYYv4mXg5SrZRMuK7PHE5xbuRjcBElwGPgV1gkMJ4IJ4DA/tqZu3mwY1ppsPkgu2w5kslD46FoupN1lzY5SqcE3QWlJrXgfH1U6Zlh83VVbkdtktin/bLEykvMd/FHP2U8JudRt/OnjQb19eIqNmBF1cpE6LfxiPmR+54JFj8mw5eK7gVOwG/FgtD03G0G4a6u+53y28HN0EqJ4x39d0VS3HAocSq08mSxa95ZSodTi+I92DXxe6S5FuY5br19nonuDQxAK/i3Qn+JVdIxaju4Y2dr/PJi+w0wQoe2Ca616QXWI5cIg3lsnMtMeZqu6JOv42lW5MyPfY30NV8BRAG0s0fBvhZyl4MCAL0ZgZW6AMC0A0ARroWDheLo4fcEhGxmA0GE55Yz7qJBRnM5wn/rp/v1svUAAHlw/fLrn2DreMZuviJ0fxfZQJEJ7NKlrAORP1Cw7F0zmruNY406+Wf3B5y5fwAldJ5YwxXTXgF8BZ1tm5CBc7vuuqF5PvQAXY7RoeICAHkEeEIOCQGCDd6iFuagXRmRFja+xKUX5xt+/6hesW2hfdBXDoyBy/dnSn82Hzggxp9GCMu6Xgr9BRAAgzuZKpdlBwSBdcoT2jQp1qGp3OjLAB9Iypu22Z0jQy9xv0Qx6UJfwVpthGJnszKCVXBCg+hp2X8SRzYWOWaJWaAkC4cHirXVgYcCg+LhMrlbfLnmpuzsV5JfmV2nuzM2b2QNcCOBh3PgjPOjf9RV2cDuUK/G58QegFgPBbOHdYEUuUNVOlilTeQODRUTwFWT6l6DhwaLYLv0vUbZdISwdsXV3YklqNC/not9PppSj5r9h57G4jK19jTjzsxAjcBYCaMpldJK8ScCx9Fm9+PPTHYJaMWtvJscybrDTt6zrsPn/O/E0GyagCxy2eIlvK67fd1/NYES0iTwEgTAnHowCH9IonK5//R5Q/32KQ/xEA83E7OBQZreg1sQljCE7qvkqMoYmH8XtHdPe6KqYAEKRT3zlsT8WprD3Myw1Bb/YKn6phg0tbvohZqGqz92SVzuA9tG3p0gtVYVHR7AABoSPvy4QFhxRDrbsyKgVrKgfTbiaNi/2mySbq5pVbQ/qV5cZXBBCmAT8gxkrAoW4SQo+ifSq3RKcyHT/z+Wu8vl72xBOz1QuHNt1V9XvoRQCd1NT0dKXgWIU/GquP/pNCS3i17WzmfK8kMHG4cWt390NePFGFFQFkfprBtKLL234Tck4wsE/1Mb9xpxrf4KZNL2iC/VClFy6fPIRj6/WqsGrQigCiBITUvhQ0ISc4FB8//kfXbv0tnoImWAP+7EvPl16+R+WdP2PaxTVIvpBECUAxadC5vW+jBMdc5GnJlvb2+b4FTTFG81qUIYq2bbITmfm1/gCsBCB6zhHTZMwgyxs3cKyYeK/gGsv992gPbuyl3YU/ku54fuiMsMfWleS9BCAShn055Rdq9oTKgZPj5W+FrUzDLmsqu2OJWAsmTqlq/6SOWxkox4hyPyboD5xckpjxLMegWnQ32k2ZV+ilJaCs3Vv7+zHdLj3IouhBwMnxa58tTfYVit8SUAKUj/xJp5Cg4OTi84taVqwwvz9yynvFX74EXAEaE9miI91w4OQUYPHE58qr8gqHqgRcAaIXchHhtxSpEnAoPh4xuvzvdeuH9J9MU3jtSqWEIY13xhgfdIYF3Q5CLUiOJRuoFakuYzjFV8XfvKpzsWTiq3hUFnmWMfwkCx4AxuNl9IftQ8yWqLLiarXkWGPEKCy3rYjncfPLDvDQTvcY6GMI/Zv5g/NS2w/6Pmw074WkZ7jQd+sx+Rf65CWKjChncXbB+Jm0ETA1WrSg4Fjx6FdQ6jNjx+HcJdRWUkFOCAf9jDSe9nhR15jbdSpTaqU9RUE1yT492Nd1fcFfgcO1i7NkSkO+q+CuYOcG9TUxnqyjca1spbDSi8o+SmpdNQMHSgOcG6LSvSxAQxt7f047C6FbDjS1aiZ6kstaU6lFUSnvR05Le+oKnE+t8OK19LPzhM0vNon/EXJQZNGYsgBRYuijrw2bnDPz+G7zTsiqSSuiBTdK6mYv3Z36EW9YcDAb+vJQd/efvdILGuYHIK1hfPSr1IqCCldmXrIFrR3p64LKCsqPd7ATGo9to19idIur1C9s3RFix2B/d+RHLL4AojvGqPmBbul7Zp6xG5euXH26W8FFQGcNBw8NInOz3WR56ucWyYWOyjs+2N9LXVvkxhdAlOpga/OPYHk++mdp5yPzzODywTRmV1acKO2WdPoXmC+7nuj60C+QOg1jh2ciQuAexk8ivgGiyxHY+KTdaU/jN/MowFn7DEbXmfzr4JlyLrC5o/MWPHF8qRurX/3c4jvpusjS0mHMSY/KH3iwxtMmu5D4PJUCYTIP0O/BbvdalbygtJaOztuxwHyLW7ww+rnJIjq+Oz19e3+/eV7kxVdJWODaq+M2iyrBsJnHgv2S5vbVt6lk+qWtxyuGre2dm2oJDh2jVBscyn/gFkSR0Ip+DOvt5CYTFpxc7Nx/zOX7hvp6OuEL1JcvvvTSmXVjGfpKXXnzk6RHoR/JsQweiV8x1BP+u1NLjh87FEAQzFtSnfRkMPawMIdxmLDrCOxnPdkwNtpK13AdIpVevIezFotfWlep70wjIEr9SAmuidRAb28fuWthAndxeaXw7SxriT7z2qsPJ5L7lqdSzV6Zb+7sPL61o+MxgPMr8NUMHPqcvpbgUBmEbUFm+S1NpR+Ao8304F/YlkPxnWDj0/ZfCcYvp1/2zcvnLR1rFoHv27g0cV6e5mo55RFjRfrhze0tG3secU2wSgEVAQSdOEAySLeKMu/STeKLNQM7AXdgVMKJrDgPhwENfsohanB0TbQ82tu71U/aUfN4ngf5SEwwYZwueOwPPniVLF6Fmd+muSy3UvLXG3vJUypQhojzndMf7e+u6lTaSwXXfSqvSPawPTt3vnjiqQteBZr6vQA7s8MddWFGLQ9fHZ4w1N+9y6F2Tb2VdnEFZTH1/is8JxcIZRxRF2bE8kbrxw7P9jubLJPVioL99Rs+kuAH9y8EG91jKGsiLsySCQYpUMGY+BTAmT4VwKF8RNaCSNjiNiwak5ndcDaRX2WmMjh4reuuLb1db4DepYs7VWZqQIusBZGu9LsDc5sa8ZKufFile2ltILhKqaq4KlppzArkSe1ygPN6pDNlwKE8l+ZRVRIhaGen07Pigm3D/thct4QqKQmV4iHlDcuJ8ZOHNm2q+tdyIYqxegBZyuA9taMNpn8CBfoe/Jm3g0IWpCXStO0AhZWHD1LXbu3puadI8BTz2PNZddXo+tOMWOy4eDZ7kiHYHM7ZXHxJcQIuNs7Gk86z8YwMbkdp03ELogk/FEXfuNKBXgI0/KjWkXMj+AX8Rt7OgIcOEkfROR1Gix3BXbcDkHUA4fQO3j6cBu9FlL2I8zx2YneOTJu2l34hBWGvmFdKoLIS+H9hvd/wFvfEggAAAABJRU5ErkJggg==",
  sMOVE_ICON   : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsTAAALEwEAmpwYAAABWWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgpMwidZAAAJEUlEQVR4Ae2af4xdRRXH37vv5/7o7nZ/1GZ3K2u7LYWFLVq1bER9aaDYBEKiWf9QNBKw8W8g8Q//WWKIiQaVGBNjFENjjH80xZQoJgoxSKLEBihQWjQVSBGppbu1sd12d7uPz3d2zuWm6bv3Prq7j33uJC8z99w5M+d75syZM+e+TGa1rGpgVQOrGmiMBgKmvaVQKHxjeHi41BgRGjTr+Ph4kamfzGaz1SDIVmm/snXr1p4GibO803rwT3nw55l9OpcLnBIGBwe7l1ea5Z8ty5QG/gJtLMCBn1lQSHAUWm75xVqGGScmJrTnDbxWft6DrwKedla0aj6ff45aimqesmfPngJoDPw0bVv5eYHX6nuavTuyfft28TRFiZr9ORAJ/BT13wXcgz+ey+XeWniXPetph/v7+1ubQQM/FiBM3MBrtbfh+PZ6oDL7Z6BtRDGnPe2sd4wvQc+vZCX0ePD/BYSZfUWA8vncExHaIdE6Ozs3Uk168Kd1RBIn3K13K7K0tbWtt1UG1DwgbooA2W9OkPp5o69bt24T7XcE3vPutHcrtf5qsVj4FcILWGZkZERBEBaQP0ClrSGgz4pmpa+vbzM+4RGex43WjPVjWmG/yn9oFECdzQ0pAJ/Lcj5Uq9UMq/16Q4Ro5KQ4t08yf9U7vFsbKUvD5m5padEe/ya/5or8GqbR1Ynr10Cjbl4DpVLpplKpsHN2dm6wu7s9Nz09cxbxZ+uHsII4FOnh+X/PCXBW5z+iW3So4/Acp4GOw90fZEiymEpXV/mqOoXMEvh8Hx537gP+Iu0LRIBa8VmeaTuanQoHent719Q5R2dHR8dwnTzpu5O366D3ywpc/OqlTWHlAPoKvOKbFv9C2yVCLBJ0NH8kuj7w/Jt+aQFdA+//NC5WpKhzccuWLf29jHjUgz9Nu9ra2vqJFLOAIziu/oA/58GfKJWK34N2c29v58e4L4zS3oVP+C71W34Od3MEzElomju2lMvlb9NBc5zxc+yPZajn5ejopnX0f9ULJkclM/0PdaID5canPR2CD4LMo9wFaub9ZGWAftTPYUp4iuekch3g5/jJkvAlzrp+ncSU+N6Dt5V34FnRSRg/lMTc3t7+RfqE4Gn/zHhGR0fbKpVKmedg167RNuqcaD5tlikWiz+HFuXdZLy1aubTrXHeg3fWhjJ/Uat/Iv2SlbdUle71Q4nMRHZ4++dkNQu/zD/HxsZaxCfglu4ymp49Lbd79259I0DP2WPipa2EyZ+oEwunzHY6maN1MhcKuVDxiQNYh5ERl562lXfJSiQ6xvs+65NQb5PwgJihX5WQ93711yrHgHfKoZvLBeIT7hMvK+rGMDp1bOnq6roBWU/RifkDJzuWsDeWKfqyUnEpqCMRAJa9PYgwe9nX++i/j1X5DfV++j3uLzbhMDx/3a/eDEJoFUf0Uquv+jIrH9KsD92u8+bMGNkqjm6jeCNlLTJ8jud7Cah+EASZh5jrYbbPBLSn/bwX4b3gZfkd3yQS/ZbG/4IHbysvAC5z6weSZp1p+n6uvXZty4fFrFIuF79DZf2mNmzY0O9esDXiwMs6xscXEiZbtgwOwDPFHJY1vsWPwfjlT19OlktoUZnPe2X+yMaoWaPJe6VxOpyPADUhDBR1mMyw4CXqqB40XupJsrvuKAOgy/IKaMxWyE5MVJQIXQsglxqjXWVL3G5Ct7WVtPKad051VBabV8qIKMR9hMFi/mxj1KzXrFkjYU+Z+TGIgf8HA/yVdy8yySHof6P9OLQJ6s38wuL3vAQj6eH8wCYSH7ryBgngM6YkorrN4oeHSNFZ3PXhBDSKxfy3qCZ5d4I53qZ9Apnfof4XtLehOcukHSqptTVjlki3mLJ+/Xrt2VMaBCHsi80L0OSh05SK53Wa7+ho+7KYZP4xK5/x+1+rnyFAupOK+QM3Ro3QWHs60G9sbFCnTIu+L+Kn/hLllYXglz4KLX3p6em5BkZ9wJAQ5g+O8uwETBgpC89xz6uVUEAUrm6CH7CPIX9kDFvFN8WfpmCRT9IvVJzAQ/tUHG9cNkbZWRf6MlD4xYbBpPmkMua30TkJUS4X7hHDjh07dKfIKTtsGWKvkLyOST/oXR684xWbp8dWANUJJfA+bnHgPxPLlOLlVbIEv6fsi81B+OIUZ8O+5reCbn9VzPpr9sKiPrMGo1N/yYPXESZHdiTyrmYT8A/zUuBdCC1eaJ+tyVDPi+7u7g30N8c4pcHZU4q6YgvhaZ/60kkhqlMCFrUfRdwMrcNHfRntW6I4HXP7IuDN+bbHTsJLLmZ3UOlUOKO6XvBpVjKDY1QUeFKDe2u4gec05UZA+7383nUYYU/CfJixXgK0vLdWT/2mI3NcnWYC+nzFbzcDH7vnU4552W4BK/9x3qQVzAa5GnCTXnGyBufZ9ewtRODlaPU/ASlYCql3jh0o+qccwWFAZpMn1aksIGmQFO91ejyAkM5MBd4rxFZe9Rl+P6SfuzilGPOKuywX+KigOfbstpmZmc/Pzc1txlEF1G/gFw4Rxx+YmprSjXO1LJsG7DhatglXJ3pPA+3txWs5YhUYNa6kvSMvgYRDdgIwthzk/1fB2z8IYrtKL07E9j5UqJtUthFWMD8/fz1KyFy8OK9/i9z2PmRfMSxlJHV/i4lKDOiDPLtAiHjA5fHtqjw0NETSp/wR3jfimI6KecXtYQtRCYDcbTAy4vMWDJFBfszoC/f54GWe2R6516iXXAlLNgGBjU9qhF9sHjKggH/BnCDW8FvRBwYGeoiLXqUp36CASF+g0mVxNMAVFClB/mCxSzsg3bWWge2LzU80CaurlJqFwb+UH8InHPM0+wijC5O20MotNb7YPAKiQwB2t0SU8TS/Z6FJIQb+NPnJei9EH0xFcdfX/d/S1JZeE3jR7N4v8D6TEzQPeEt7yRJszwM0vA7LEUoRRqMOwZMkbboAiTzhQnIE4EqZuy0A6FmUIAsIwVt6HHrTFb7dhbkAfTpz1hAFH0mONh14B4iPHjfSsE/ZWvkpc3iXSZI2pxJwjDtBdlgRoYHneSmO4uZU4GKiWrJIcDGFXKqxzOSyzXjkpFaaB2+WcGmtcYyWesyV0vFd4FDB1mOmmM4AAAAASUVORK5CYII=",

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

      controller.registerProperty("Minimum", element.setMinimum);
      controller.registerProperty("Maximum", element.setMaximum);

      controller.registerProperty("NumberOfMarks",element.setNumberOfMarks,element.getNumberOfMarks);
      controller.registerProperty("BigMark",element.setBigMark);
      controller.registerProperty("MediumMark",element.setMediumMark);
      controller.registerProperty("MarkFactor",element.setMarkFactor);
      controller.registerProperty("Digits",element.setDigits);

      controller.registerProperty("BorderWidth",element.setBorderWidth);
      controller.registerProperty("BorderColor",element.setBorderColor);
      controller.registerProperty("FillColor",element.setFillColor);
      controller.registerProperty("LineWidth",element.setLineWidth);
      controller.registerProperty("LineColor",element.setLineColor);
      controller.registerProperty("TextColor",element.setTextColor);
      controller.registerProperty("TextFont",element.setTextFont);

      controller.registerProperty("Width",element.setWidth);
      controller.registerProperty("Length",element.setLength);
      
      controller.registerProperty("DrawLines",element.setDrawLines);
      controller.registerProperty("DrawText",element.setDrawText);

      controller.registerProperty("Enabled",element.setEnabled);
      controller.registerProperty("Rotates",element.setRotates);

      controller.registerAction("OnDrag",element.getOnRotationInformation);
      controller.registerAction("OnRotation",element.getOnRotationInformation);

    }
};

/**
 * Creates a 2D ruler
 * @method ruler
 */
EJSS_DRAWING2D.ruler = function (mName) {
  var self = EJSS_DRAWING2D.group(mName);

  // Configuration variables
  var mLength = 1.1;
  var mWidth = 0.2;
  
  var mMinimum = 0;
  var mMaximum = 1;

  var mNumberOfMarks = 101;
  var mBigMark = 10;
  var mMediumMark = 5;
  var mMarkFactor = 0.1;
  var mDigits = 0;

  // Implementation variables
  var mGroup,mBody;
  var mSegments, mTexts;
  var mLeftHandle, mRightHandle;
  var mLeftRotate, mRightRotate;
  var mForegroundCreated = false;

  var mElements = [];
  var mScaleChanged = true;
  
  self.getElement = function(keyword) {
	  return mElements[keyword];
  };
  
  self.superSetChanged = self.setChanged;
  
  self.setChanged = function(changed) {
	self.superSetChanged(changed);
	if (changed) for (var i=0,n=mElements.length; i<n; i++) mElements[i].setChanged(changed);
  }

  
  // ----------------------------------------------------
  // public functions
  // ----------------------------------------------------

  self.setWidth = function(value) { 
	  if (value!=mWidth) {
		  mWidth = value;
		  self.mustAdjust();
	  }
  }
  self.getWidth = function() { return mWidth; }

  self.setLength = function(value) { 
	  if (value!=mLength) {
		  mLength = value;
		  self.mustAdjust();
      }
  }
  self.getLength = function() { return mLength; }

  /**
   * Sets the minimum value of the element. 
   * @method setMinimum
   * @param value
   */
  self.setMinimum = function(value) {
    if (mMinimum!=value) {
    	mMinimum = value;
    	mScaleChanged = true;
    }
  }
  
  /**
   * @method getMinimum
   * @return current minimum
   */
  self.getMinimum = function() { 
    return mMinimum; 
  };    

  /**
   * Sets the minimum value of the element. 
   * @method setMaximum
   * @param value
   */
  self.setMaximum = function(value) {
	if (mMaximum!=value) {
		mMaximum = value;
    	mScaleChanged = true;
	}
  }
  
  /**
   * @method getMaximum
   * @return current maximum
   */
  self.getMaximum = function() { 
    return mMaximum; 
  };    
  
  /***
   * Sets the number of marks
   * @method setNumberOfMarks(value)
   * @visibility public
   * @param value int
   */ 
  self.setNumberOfMarks = function(value) {
	  if (value!=mNumberOfMarks) {
		  mNumberOfMarks = value;
	      mScaleChanged = true;
	  }
  };

  /***
   * Gets the number of marks
   * @method getNumberOfMarks
   * @visibility public
   * @return int
   */
  self.getNumberOfMarks = function() {
  	return mNumberOfMarks;
  };


  /***
   * Sets the interval for big marks
   * @method setBigMark(value)
   * @visibility public
   * @param value int 
   */ 
  self.setBigMark = function(value) {
	  if (value!=mBigMark) {
        mBigMark = value;
    	mScaleChanged = true;
	  }
  };

  /***
   * Gets the interval for big marks
   * @method getBigMark
   * @visibility public
   * @return int
   */
  self.getBigMark = function() {
  	return mBigMark;
  };

  /***
   * Sets the interval for medium marks
   * @method setMediumMark(value)
   * @visibility public
   * @param value int 
   */ 
  self.setMediumMark = function(value) {
	  if (value!=mMediumMark) {
        mMediumMark = value;
    	mScaleChanged = true;
	  }
  };

  /***
   * Gets the interval for medium marks
   * @method getMediumMark
   * @visibility public
   * @return int
   */
  self.getMediumMark = function() {
  	return mMediumMark;
  };
  
  /***
   * Sets the factor that divide the value in marks
   * @method setMarkFactor(value)
   * @visibility public
   * @param value int|double 
   */ 
  self.setMarkFactor = function(value) {
	  if (value!=mMarkFactor) {
        mMarkFactor = value;
    	mScaleChanged = true;
	  }
  };

  /***
   * Gets the factor that divide the displayed value in marks
   * @method getMarkFactor
   * @visibility public
   * @return int|double
   */
  self.getMarkFactor = function() {
  	return mMarkFactor;
  };

  /***
   * Sets the maximum value to display in a mark
   * @method setMaximumMark(value)
   * @visibility public
   * @param value int|double 
   */ 
  self.setMaximumMark = function(value) {
	  if (value!=mMaximumMark) {
        mMaximumMark = value;
    	mScaleChanged = true;
	  }
  };

  /***
   * Gets the maximum value to display in a mark
   * @method getMaximumMark
   * @visibility public
   * @return value int|double 
   */ 
  self.getMaximumMark = function() {
  	return mMaximumMark;
  };

  /***
   * Sets the number of digits for the marks
   * @method setDigits(value)
   * @visibility public
   * @param value int 
   */ 
  self.setDigits = function(value) {
	  if (value!=mDigits) {
		mDigits = value;
	    mScaleChanged = true;
	  }
  };

  /***
   * Gets the number of digits for the marks
   * @method getPrecision
   * @visibility public
   * @return int
   */
  self.getDigits = function() {
  	return mDigits;
  };
  

  // ----------------------------------------------------
  // Properties overwritten
  // ----------------------------------------------------

  self.mustAdjust = function() { mScaleChanged = true; }
  
  self.setBorderWidth = function(value) {  mBody.getStyle().setLineWidth(value); }
  self.setBorderColor = function(value) {  mBody.getStyle().setLineColor(value); }
  self.setFillColor = function(value) { mBody.getStyle().setFillColor(value); }

  self.setLineWidth = function(value) { 
	  mSegments.setToEach(function(element) { element.getStyle().setLineWidth(value); });
  }
  self.setLineColor = function(value) { 
	  mSegments.setToEach(function(element) { element.getStyle().setLineColor(value); });
  }
  self.setDrawLines = function(value) { 
	  mSegments.setToEach(function(element,value) { element.setVisible(value); },value);
  }
  
  self.setTextColor = function(value) { 
	  mTexts.setToEach(function(element) { element.getFont().setFillColor(value); });
  }
  self.setTextFont = function(value) { 
	  mTexts.setToEach(function(element) { element.getFont().setFont(value); });
  }
  self.setDrawText = function(value) { 
	  mTexts.setToEach(function(element,value) { element.setVisible(value); },value);
  }

  self.setEnabled = function(value) { 
	  mLeftHandle.setVisible(value);
	  mRightHandle.setVisible(value);
  };

  self.setRotates = function(value) { 
	  mLeftRotate.setVisible(value);
	  mRightRotate.setVisible(value);
  };
  
  // ----------------------------------------------------
  // Properties and copies
  // ----------------------------------------------------

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Ruler.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // private functions
  // ----------------------------------------------------

  self.superSetParent = self.setParent;
  
  self.setParent = function(parent, sibling) {
	self.superSetParent(parent,sibling);
	if (!mForegroundCreated) {
      self.createRuler();
	  mForegroundCreated = true;
	}
	self.addParticularChildren();
	self.adjustForeground();
  }

  self.dataCollected = function() {
	  if (mScaleChanged) {
		  self.adjustForeground();
		  mScaleChanged = false;
	  }
  };

  self.addParticularChildren = function() { 
	mGroup.setParent(self);
	mBody.setParent(mGroup);
	mSegments.setToEach(function(element) { element.setParent(mGroup); });
	mTexts.setToEach(function(element) { element.setParent(mGroup); });
	mLeftHandle.setParent(self);
	mRightHandle.setParent(self);
	mLeftRotate.setParent(self);
	mRightRotate.setParent(self);
  };

  self.createBody = function() {
	var body = EJSS_DRAWING2D.shape(mName+".body");
	body.setShapeType("ROUND_RECTANGLE");
	body.setCornerRadius(5);
    return body;
  }

  self.createRuler = function() {
	mGroup = EJSS_DRAWING2D.group(mName+".group");
	mElements['group'] = mGroup;

	mBody = self.createBody();
	mBody.getStyle().setFillColor("rgba(0,255,0,0.2)");
	mElements['body'] = mBody;

	mSegments = EJSS_DRAWING2D.segmentSet(mName+".segments");
	mSegments.setToEach(function(element) { element.setRelativePosition("SOUTH_WEST"); });
	mSegments.setToEach(function(element) { element.getStyle().setLineWidth(0.5); });
	mElements['segments'] = mSegments;

	mTexts = EJSS_DRAWING2D.textSet(mName+".texts");
	mTexts.setToEach(function(element) { element.getFont().setFont("normal normal 8px \"Courier New\", Courier, monospace"); });
	mTexts.setToEach(function(element) { element.setRelativePosition("NORTH"); });
	mElements['texts'] = mTexts;

	mLeftHandle = EJSS_CORE.promoteToControlElement(
				EJSS_DRAWING2D.image(mName+".left_handle"),self.getView(),mName+".left_handle");
	mLeftHandle.setImageUrl(EJSS_DRAWING2D.Ruler.sMOVE_ICON);
	mLeftHandle.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setMotionEnabled("ENABLED_ANY");
	mLeftHandle.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setAffectsGroup(true);
	mLeftHandle.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setSensitivity(10);
	mLeftHandle.setSize([10,10]);
	mLeftHandle.setPixelSize(true);
	mLeftHandle.setProperty("OnDrag",moved);
	mElements['left_handle'] = mLeftHandle;

	mRightHandle = EJSS_CORE.promoteToControlElement(
				EJSS_DRAWING2D.image(mName+".right_handle"),self.getView(),mName+".right_handle");
	mRightHandle.setImageUrl(EJSS_DRAWING2D.Ruler.sMOVE_ICON);
	mRightHandle.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setMotionEnabled("ENABLED_ANY");
	mRightHandle.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setAffectsGroup(true);
	mRightHandle.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setSensitivity(10);
	mRightHandle.setProperty("OnDrag",moved);
	mRightHandle.setSize([10,10]);
	mRightHandle.setPixelSize(true);
	mElements['right_handle'] = mRightHandle;
		
	mLeftRotate = EJSS_CORE.promoteToControlElement(
			EJSS_DRAWING2D.image(mName+".left_rotate"),self.getView(),mName+".left_rotate");
	mLeftRotate.setImageUrl(EJSS_DRAWING2D.Ruler.sROTATE_ICON);
	mLeftRotate.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setMotionEnabled("ENABLED_NO_MOVE");
	mLeftRotate.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setSensitivity(10);
	mLeftRotate.setSize([8,8]);
	mLeftRotate.setPixelSize(true);
	mLeftRotate.setProperty("OnDrag",rotated);
	mElements['left_rotate'] = mLeftRotate;

	mRightRotate = EJSS_CORE.promoteToControlElement(
			EJSS_DRAWING2D.image(mName+".right_rotate"),self.getView(),mName+".right_rotate");
	mRightRotate.setImageUrl(EJSS_DRAWING2D.Ruler.sROTATE_ICON);
	mRightRotate.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setMotionEnabled("ENABLED_NO_MOVE");
	mRightRotate.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setSensitivity(10);
	mRightRotate.setSize([8,8]);
	mRightRotate.setPixelSize(true);
	mRightRotate.setProperty("OnDrag",rotated);
	mElements['right_rotate'] = mRightRotate;

  };

  self.adjustBody = function() {
		mBody.setSize([mLength,mWidth]);
  };

  self.adjustForeground = function() {
	mLeftHandle.setPosition([-0.125*mLength,0]);
	mRightHandle.setPosition([0.125*mLength,0]);
	mLeftRotate.setPosition([-0.35*mLength,0]);
	mRightRotate.setPosition([0.35*mLength,0]);
	
	mGroup.setY(mWidth*0.3);
	self.adjustBody();
	
	var MARGIN = (mLength - Math.abs(mMaximum-mMinimum))/2;
	var MARK = mWidth/6;
	  
	mSegments.setNumberOfElements(mNumberOfMarks);
	var dx = Math.abs(mMaximum-mMinimum)/(mNumberOfMarks-1);
	var xPos = -mLength/2+MARGIN;
	var mediumMarksCounter = 0;
	var bigMarksCounter = 0;

	for (var i=0; i<mNumberOfMarks; i++) {
	  var segment = mSegments.getElement(i);
	  segment.setPosition([xPos,mWidth/2]);
	  if (i%mBigMark==0) {
	    bigMarksCounter++;
	    segment.setSize([0,-2*MARK]);
	  }
	  else if (i%mMediumMark==0) {
	    mediumMarksCounter++;
	    segment.setSize([0,-1.5*MARK]);
	  }
	  else segment.setSize([0,-MARK]);
	  xPos += dx;
	}
	
	mTexts.setNumberOfElements(bigMarksCounter);
	var text_counter=0;
	for (i=0; i<mNumberOfMarks; i++) {
	  var segment = mSegments.getElement(i);
	  if (i%mMediumMark==0) {
		  // locate units
	  }
	  if (i%mBigMark==0) {
	    var text = mTexts.getElement(text_counter);
	    text.setX(segment.getX());
	    text.setY(mWidth/2+segment.getSizeY()*1.1);
	    var mark_value = ((mNumberOfMarks-1-i)*mMinimum + i*mMaximum)/(mNumberOfMarks-1) /mMarkFactor;
	    text.setText(""+mark_value.toFixed(mDigits));
	    text_counter++;
	  }
	}
  };
   
  self.getOnRotationInformation = function() {
	  return { position : self.getPosition(), element : self, 
		  point : self.getGroupPanel().getPanelInteraction().getInteractionPoint(),
		  angle : self.getTransformation() };
  };

  function moved(point,info) {
	  var controller = self.getController();    		
	  if (controller) {
		controller.invokeImmediateAction("OnDrag");
	  }
  };

  self.computeRotation = function(rightIcon,point) {
	  if (rightIcon) return Math.atan2(point[1],point[0]);
	  else return Math.atan2(-point[1],-point[0]);
  };
  
  function rotated(point,info) {
	  var point = info.point;
	  var element = info.element;
	  var group_pos = self.getAbsolutePosition(true);
	  point[0] -= group_pos[0];
	  point[1] -= group_pos[1];
	  var rotation = self.computeRotation(element==mRightRotate,point);
	  self.setTransformation(rotation);
	  self.setChanged(true);
	  var controller = self.getController();    		
	  if (controller) {
//		controller.immediatePropertyChanged("Value");
		controller.invokeImmediateAction("OnRotation");
	  }	        

  }
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
};


