/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Mesh
 * @class Mesh 
 * @constructor  
 */
EJSS_DRAWING2D.Mesh = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy
  	
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Data", element.setData);
		controller.registerProperty("DataType", element.setDataType);
		controller.registerProperty("Mesh", element.setMesh);

		controller.registerProperty("Points", element.setPoints);
		controller.registerProperty("Cells", element.setCells);
		controller.registerProperty("FieldAtPoints", element.setFieldAtPoints);
		controller.registerProperty("FieldAtCells", element.setFieldAtCells);
		controller.registerProperty("VectorLength", element.setVectorLength);
		
		controller.registerProperty("Boundary", element.setBoundary);
		controller.registerProperty("BoundaryLabels", element.setBoundaryLabels);
		controller.registerProperty("BoundaryColors", element.setBoundaryColors);
		controller.registerProperty("BoundaryWidth", element.setBoundaryLineWidth);
		controller.registerProperty("DrawBoundary", element.setDrawBoundary);
	}
};


/**
 * Creates a 2D Mesh
 * A Mesh is a collection of polygons, where each vertex has associated a scalar values value to it.
 * The mesh can be drawn using a color for each polygon, or a color code that uses the vertex values to find areas of equal value.
 * This second feature can be configure setting the levels to distinguish and the color of each of the levels (and below and above them)
 * The Mesh can also be given a set of lines which can be drawn to show the boundaries or special areas of the mesh, each with a different color.  
 * 
 * @method mesh
 */
EJSS_DRAWING2D.mesh = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  // Configuration variables
  var mDataType = -1;	    // An int that overrides the mData type
  var mPoints; 				// double[][]
  var mValues; 				// double[][]
  var mCellValues; 			// double[][][]
  var mMeshGeometry; 		// int[][]
  var mAutoscaleZ = true; 
  var mVectorLength = 0.03;

  var mBoundaryGeometry;		// int[][]
  var mBoundaryLabels;			// int[]
  var mBoundaryColors = null; 	// color[] an array with a color for each index. If null, the element color is used
  var mBoundaryStroke = 1;	  	// stroke
  var mBoundaryDraw = true;
  
  // Implementation variables
  var mColor = EJSS_DRAWING2D.colorCoded(16, EJSS_DRAWING2D.ColorMapper.SPECTRUM); 		// ColorCoded

  // -------------------------------------
  // Public methods for mesh geometry
  // -------------------------------------

  self.getClass = function() {
  	return "ElementMesh";
  }

  /**
   * Indicates what type to consider for the setData argument.
   * Allows using the same data for different displays.
   * @param type int or String // one of 0=MESH_2D, 1=MESH_3D, 2=SCALAR_2D_FIELD, 3=SCALAR_3D_FIELD, 4=VECTOR_2D_FIELD, 5=VECTOR_3D_FIELD
   */
  self.setDataType = function(type) {
    switch (type) {
      case "MESH_2D" : mDataType = 0; break;
      case "MESH_3D" : mDataType = 1; break;
      case "SCALAR_2D_FIELD" : mDataType = 2; break;
      case "SCALAR_3D_FIELD" : mDataType = 3; break;
      case "VECTOR_2D_FIELD" : mDataType = 4; break;
      case "VECTOR_3D_FIELD" : mDataType = 5; break;
      default : mDataType = type; 
     }
    self.setChanged(true);
  }
  
  self.getDataType = function() {
  	return mDataType;
  }

  /**
   * Provides the data based on the following structure
   * @method setData
   *     "data": {
   *		"type": int, // one of  0=MESH_2D, 1=MESH_3D, SCALAR_2D_FIELD, SCALAR_3D_FIELD, VECTOR_2D_FIELD, VECTOR_3D_FIELD
   *        "problem_mesh": mesh, // the mesh that defined the problem
   *        "solution_mesh": mesh, // the mesh that contains the solution
   *        "solution_values": double[][][], // the values of the field for the solution mesh
   *	  }
   * @param data 
   */
  self.setData = function(data) {
    if (!data) return false;
    if (mDataType<0 || mDataType>5) {
   		self.setDataType(data["type"]);
    }
    switch (mDataType) {
      case 0 : // 2D mesh
      case 1 : // 3D mesh
        self.setMesh(data["problem_mesh"]); 
        return true;
      case 2 : // scalar (real or complex) 2D solution
      case 3 : // scalar (real or complex) 3D solution
      case 4 : // vector (real or complex) 2D solution
      case 5 : // vector (real or complex) 3D solution
        self.setMesh(data["solution_mesh"]);
        self.setFieldAtCells(data["solution_values"]);
        return true;
     }
     return false; // no correct data
  }

  /**
   * Provides the mesh based on the following structure
   * @method setMesh
   *     "mesh": {
   *		"points":[[x0,y0],[x1,y1],...],
   *		"cells":[[p00,p01,p02],[p10,p11,p12],...],
   *		"boundary":[[p00,p01],[p10,p11],...], // end points for each segment of the boundary		
   *		"boundary_labels":int [l0, l1, l2,...] // one for each boundary segment
   *	} 
   * @param mesh 
   */
  self.setMesh = function(mesh) {
    self.setPoints(mesh["points"]);
    self.setCells(mesh["cells"]);
    self.setBoundary(mesh["boundary"]);
    self.setBoundaryLabels(mesh["boundary_labels"]);
    self.setChanged(true);
  }
  
//  self.getMesh = function() {
//  	return {"points": mPoints,  "cells": mCells, "values": mValues, "boundary": mBoundaryGeometry};
//  }

  /**
   * Provides the array of points that conform the mesh and its boundary
   * @method setPoints
   * @param meshs The double[nPoints][2] where nPoints is the number of points in the mesh
   */
  self.setPoints = function(points) {
    mPoints = points;
    self.setChanged(true);
  }
  
  self.getPoints = function() {
  	return mPoints;
  }

  /**
   * Provides the field value for each point in the mesh
   * @method setValues
   * @param values The double[nPoints]array with the value for each mesh vertex
   */
  self.setFieldAtPoints = function(values) {
    mValues = values;
    mCellValues = null;
    if(mAutoscaleZ && mValues != null) mColor.setAutoscaleArray2(mValues); 
//    console.log ("Field at points changed");
    self.setChanged(true);
  }
  
  self.getFieldAtPoints = function() {
  	return mValues;
  }

  /**
   * Provides the geometry of the mesh
   * @method setCells
   * @param cells the int[nCells][nPoints] array with the points in each mesh.
   * For example,if the first mesh is a triangle joining points 0,3,7, one gets: meshs[0] = { 0, 3, 7 }; 
   */
  self.setCells = function(cells) {
    mMeshGeometry = cells;
    self.setChanged(true);
  }

  self.getCells = function() {
  	return mMeshGeometry;
  }

  /**
   * Provides the field value for each point in the mesh
   * @method setValues
   * @param values The double[nPoints]array with the value for each mesh vertex
   */
  self.setFieldAtCells = function(values) {
    mCellValues = values;
    mValues = null;
    if(mAutoscaleZ && mCellValues != null) mColor.setAutoscaleArray3(mCellValues); 
    self.setChanged(true);
  }
  
    self.getFieldAtCells = function() {
  	return mCellValues;
  }
  
  /**
   * Provides the geometry of the mesh
   * @method setVectorLength
   * @param length the double length for vectors in the field 
   */
  self.setVectorLength = function(length) {
    if (mVectorLength !== length) {
      mVectorLength = length;
      self.setChanged(true);
    }
  }

  self.getVectorLength = function() {
  	return mVectorLength;
  }

  
  /**
   * Provides the data for the boundary.
   * @method setBoundary
   * @param meshs The int[nOfSegments][nPointsInSegment] array with the mesh information, where:
   * <ul>
   * <li>First index = nOfSegments : number of segments in the boundary</li>
   * <li>Second index = nPointsInSegment : the points in this segment</li>
   * </ul>
   */
  self.setBoundary = function(boundary) {
    mBoundaryGeometry = boundary;
    self.setChanged(true);
  }

  self.getBoundary = function() {
  	return mBoundaryGeometry;
  }

  /**
   * Provides the label for each boundary segment
   * @method setBoundaryLabels
   * @param values The int[nOfSegments] array with the label for each boundary segment
   */
  self.setBoundaryLabels = function(labels) {
    mBoundaryLabels = labels;
    self.setChanged(true);
  }

  self.getBoundaryLabels = function() {
  	return mBoundaryLabels;
  }

  /**
   * The color to use for each boundary index 
   * There must be a color for each index 
   * If not enough colors are given, or colors is null, the element draw color is used
   * @method setBoundaryColors
   * @param colors
   */
  self.setBoundaryColors = function(colors) {
    mBoundaryColors = colors;
    self.setChanged(true);
  }
  
  self.getBoundaryColors = function() {
    return mBoundaryColors;
  }
  
  self.setBoundaryLineWidth = function(width) {
    mBoundaryStroke = Math.max(1, width);
    self.setChanged(true);
  }

  self.getBoundaryLineWidth = function() {
    return mBoundaryStroke;
  }

  self.setBoundaryStroke = function(stroke) {
    mBoundaryStroke = stroke;
    self.setChanged(true);
  }

  self.getBoundaryStroke = function() {
    return mBoundaryStroke;
  }

  self.setDrawBoundary = function(draw) {
    mBoundaryDraw = draw;
    self.setChanged(true);
  }

  self.getDrawBoundary = function() {
    return mBoundaryDraw;
  }

  // -------------------------------------
  // Public methods for coloring the meshs
  // -------------------------------------

  /**
   * Returns the ColorCoded for customization
   * @method getDrawer
   * @return
   */
  self.getColorCoded = function() {
    return mColor;
  }
  
  /**
   * Sets the autoscale flag and the floor and ceiling values for the colors.
   *
   * If autoscaling is true, then the min and max values of z are span the colors.
   *
   * If autoscaling is false, then floor and ceiling values limit the colors.
   * Values below min map to the first color; values above max map to the last color.
   *
   * @method setAutoscaleZ
   * @param isAutoscale
   * @param floor
   * @param ceil
   */
  self.setAutoscaleZ = function (isAutoscale, floor, ceil) {
    mAutoscaleZ = isAutoscale;
    if (mAutoscaleZ) {
    	if(mValues != null)	mColor.setAutoscaleArray2(mValues); 
    	else if(mCellValues != null) mColor.setAutoscaleArray3(mCellValues); 
    } else mColor.setScale(floor, ceil);
    self.setChanged(true);
  }
  
  /**
   * Gets the autoscale flag for z.
   *
   * @method isAutoscaleZ
   * @return boolean
   */
  self.isAutoscaleZ = function() {
    return mAutoscaleZ;
  }

  /**
   * Returns bounds for an element
   * @override
   * @method getBounds
   * @return Object{left,rigth,top,bottom}
   */
  self.getBounds = function(element) {
  	var xmin=0, xmax=0, ymin=0, ymax=0;
  	var points = self.getPoints();
  	if (points && points.length>0) {
      var len = points.length;
      xmax = xmin = points[0][0];
	  ymax = ymin = points[0][1];    	
	  for(var j=1; j<len; j++) {
			var x = points[j][0], y = points[j][1];
			if(x>xmax) xmax=x; if(y>ymax) ymax=y; if(x<xmin) xmin=x; if(y<ymin) ymin=y;
	  }
	}    
    var x = self.getX(), y = self.getY();
    var sx = self.getSizeX(), sy = self.getSizeY();
  	var mx = sx/2, my = sy/2;  	
  	var d = self.getRelativePositionOffset(sx,sy);
	var result =  {
		left: ((x+d[0])-mx)+xmin*sx,
		right: ((x+d[0])-mx)+xmax*sx,
		top: ((y+d[1])-my)+ymax*sy,
		bottom: ((y+d[1])-my)+ymin*sy
	}
	return result;
  };  

  self.registerProperties = function(controller) {
	EJSS_DRAWING2D.Mesh.registerProperties(self, controller);
  };
	
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1]);
  self.setRelativePosition("SOUTH_WEST");

  return self;
};



