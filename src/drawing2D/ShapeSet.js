/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//ShapeSet
//---------------------------------

/**
 * ShapeSet
 * @class ShapeSet 
 * @constructor  
 */
EJSS_DRAWING2D.ShapeSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING2D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("ShapeType", 
          function(v) { set.setToEach(function(element,value) { element.setShapeType(value); }, v); }
      );
      controller.registerProperty("CornerRadius", 
          function(v) { set.setToEach(function(element,value) { element.setCornerRadius(value); }, v); }
      );
    }

};


/**
 * Creates a set of shapes
 * @method shapeSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.shapeSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.shape, mName);

  // Static references
  var ShapeSet = EJSS_DRAWING2D.ShapeSet;		// reference for ShapeSet
  
  self.registerProperties = function(controller) {
    ShapeSet.registerProperties(self,controller);
  };

  return self;
};
