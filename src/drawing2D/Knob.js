/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module Drawing2D 
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Knob
 * @class Knob 
 * @constructor  
 */
EJSS_DRAWING2D.Knob = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
	EJSS_DRAWING2D.Meter.registerProperties(element,controller); // super class

    controller.registerAction("OnPress" ,element.getValue); 
    controller.registerAction("OnRelease" ,element.getValue); 
    controller.registerAction("OnChange" ,element.getValue); 
    
  }

};

/**
 * Knob function
 * Creates a 2D element that looks and behaves like a knob
 * @method knob
 * @param mName the name of the element
 * @returns An abstract interface element
 */
EJSS_DRAWING2D.knob = function (mName) {
  var self = EJSS_DRAWING2D.meter(mName);
  
  var mThreshold = Math.PI/5;
  var pressed_shift = [0,0];  // initial dragging shift  

  // Implementation variables
  var mKnobExterior,mKnobMiddle,mKnobInterior;

  var mDummyContainer;
  var mSVGDefs = 
	  "<svg xmlns='http://www.w3.org/2000/svg' width='0' height='0' version='1.1'>"+
      "  <defs>"+
      "    <linearGradient id='KnobExteriorGradient' x1='0%' y1='0%' x2='100%' y2='100%'>"+
      "      <stop offset='0%'   stop-color='rgb(250,250,250)' />"+
      "      <stop offset='100%' stop-color='rgb(100,100,100)' />"+
      "    </linearGradient>"+
      "    <linearGradient id='KnobMiddleGradient' x1='0%' y1='0%' x2='100%' y2='100%'>"+
      "      <stop offset='0%'   stop-color='rgb(200,200,200)' />"+
      "      <stop offset='100%' stop-color='rgb(150,150,150)' />"+
      "    </linearGradient>"+
      "    <linearGradient id='KnobInteriorGradient' x1='0%' y1='0%' x2='100%' y2='100%'>"+
      "      <stop offset='0%'   stop-color='rgb(127,127,127)' />"+
      "      <stop offset='100%' stop-color='rgb(250,250,250)' />"+
      "    </linearGradient>"+
      "    <filter id='KnobFilter' x='0' y='0' width='200%' height='200%'>"+
      "      <feOffset result='offOut' in='SourceGraphic' dx='1' dy='1' />"+
      "      <feGaussianBlur result='blurOut' in='offOut' stdDeviation='2' />"+
      "      <feBlend in='SourceGraphic' in2='blurOut' mode='normal' />"+
      "    </filter>"+
      "  </defs>"+
      "</svg>";

  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  // ----------------------------------------------------
  // Properties overwritten
  // ----------------------------------------------------

  self.super_setRadius = self.setRadius;

  self.setRadius = function(value) {
	  self.super_setRadius(value);
	  mKnobExterior.setChanged(true);
	  mKnobMiddle.setChanged(true);
	  mKnobInterior.setChanged(true);
  }

  self.setFillColor = function(value) { mKnobInterior.getStyle().setFillColor(value); };

  // ----------------------------------------------------
  // Properties and copies
  // ----------------------------------------------------

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
	EJSS_DRAWING2D.Knob.registerProperties(self,controller);
  };
  
  // ----------------------------------------------------
  // private or protected functions
  // ----------------------------------------------------

  self.super_addParticularChildren = self.addParticularChildren;

  self.addParticularChildren = function() {
	  self.super_addParticularChildren();
	  mKnobExterior.setParent(self);
	  mKnobMiddle.setParent(self);
	  mKnobInterior.setParent(self);
  };

  self.createBasics = function() { 
	  /*
	  var stop1 = document.createElementNS("http://www.w3.org/2000/svg", "stop");
	  stop1.setAttribute("offset","0%");
	  stop1.setAttribute("stop-color","rgb(250,250,250)");

	  var stop2 = document.createElementNS("http://www.w3.org/2000/svg", "stop");
	  stop2.setAttribute("offset","100%");
	  stop2.setAttribute("stop-color","rgb(100,100,100)");

	  var linearGradient = document.createElementNS("http://www.w3.org/2000/svg", "linearGradient");
	  linearGradient.setAttribute("id", "#KnobExteriorGradient");
	  linearGradient.setAttribute("x1","0%");
	  linearGradient.setAttribute("y1","0%");
	  linearGradient.setAttribute("x2","100%");
	  linearGradient.setAttribute("y2","100%");
	  linearGradient.appendChild(stop1);
	  linearGradient.appendChild(stop2);

	  var feOffset = document.createElementNS("http://www.w3.org/2000/svg", "feOffset");
	  feOffset.setAttribute("result","offOut");
	  feOffset.setAttribute("in","SourceGraphic");
	  feOffset.setAttribute("dx","1");
	  feOffset.setAttribute("dy","1");

	  var gaussianFilter = document.createElementNS("http://www.w3.org/2000/svg", "feGaussianBlur");
	  gaussianFilter.setAttribute("result","blurOut");
	  gaussianFilter.setAttribute("in","offOut");
	  gaussianFilter.setAttribute("stdDeviation","2");

	  var feBlend = document.createElementNS("http://www.w3.org/2000/svg", "feBlend");
	  feOffset.setAttribute("in","SourceGraphic");
	  feOffset.setAttribute("in2","blurOut");
	  feOffset.setAttribute("mode","normal");

	  var filter = document.createElementNS("http://www.w3.org/2000/svg", "filter");
	  filter.setAttribute("id","#KnobFilter");
	  filter.setAttribute("x","0");
	  filter.setAttribute("y","0");
	  filter.setAttribute("width","200%");
	  filter.setAttribute("height","200%");
	  filter.appendChild(feOffset);
	  filter.appendChild(gaussianFilter);
	  filter.appendChild(feBlend);
	  
	  /*
	  var defs = self.getParentPanel()getGraphics().getDefs(); // document.createElementNS("http://www.w3.org/2000/svg", "defs");
	  defs.appendChild(linearGradient);
	  defs.appendChild(filter);
	  
	 // self.getGraphics().appendChild(defs);
	  */

	  var mDummyContainer = document.getElementById(mName+".dummy_container");	
	  if (mDummyContainer === null) { 	// exits?
		  mDummyContainer = document.createElement('div');
	      mDummyContainer.setAttribute("id", mName+".dummy_container");
	      mDummyContainer.style.width = "0px";
	      mDummyContainer.style.height = "0px";
		  document.body.appendChild(mDummyContainer);
		  mDummyContainer.innerHTML = mSVGDefs;	    
	  }	
	  
	  mKnobExterior = EJSS_DRAWING2D.shape(mName+".exterior_shape");
	  mKnobExterior.setSize([1,1]);
	  mKnobExterior.getStyle().setDrawLines(false);
	  mKnobExterior.getStyle().setAttributes({ fill: "url(#KnobExteriorGradient)", filter:"url(#KnobFilter)"});

	  mKnobMiddle = EJSS_DRAWING2D.shape(mName+".middle_shape");
	  mKnobMiddle.setSize([0.9,0.9]);
	  mKnobMiddle.getStyle().setDrawLines(false);
	  mKnobMiddle.getStyle().setAttributes({ fill: "url(#KnobMiddleGradient)"});

	  mKnobInterior = EJSS_CORE.promoteToControlElement(
			  EJSS_DRAWING2D.shape(mName+".inner_shape"),self.getView(),mName+".inner_shape");
	  mKnobInterior.setY(0.3);
	  mKnobInterior.setSize([0.2,0.2]);
	  mKnobInterior.getStyle().setDrawLines(false);
	  mKnobInterior.getStyle().setAttributes({ fill: "url(#KnobInteriorGradient)"});

	  mKnobInterior.setProperty("EnabledPosition","ENABLED_NO_MOVE");
      mKnobInterior.setProperty("Sensitivity",10);
	  mKnobInterior.setProperty("OnDrag",dragged);
	  mKnobInterior.setProperty("OnPress",pressed);
	  mKnobInterior.setProperty("OnRelease",released);
  }
    
  self.super_createForeground = self.createForeground;
  
  self.createForeground = function() {
	  self.super_createForeground();
	  self.getElement('brand').setPosition([0,-0.7]);
	  self.getElement('units').setPosition([0, 0.9]);
  };

  self.adjustPosition = function() {
	  var actualValue = self.getValue();
	  if (actualValue<self.getMinimum()) {
		  actualValue = self.getMinimum();
//		  mArrow.getStyle().setFillColor(mDangerColor);
	  }
	  else if (actualValue>self.getMaximum()) {
		  actualValue = self.getMaximum();
//		  mArrow.getStyle().setFillColor(mDangerColor);
	  }
	  else {
//		  mArrow.getStyle().setFillColor(mColor);
	  }
      var angle = self.interpolateAngle(actualValue);
	  mKnobInterior.setPosition([0.3*Math.cos(angle),0.3*Math.sin(angle)]);
  }

  self.foregroundIsInside = function() { return false; };

  function pressed(point,info) {
	  var controller = self.getController();    		
	  if (controller) controller.invokeImmediateAction("OnPress");
	  
	  // init dragging	 
	  var point = info.point;
	  var i_point = info.element.getAbsolutePosition(true);
	  pressed_shift = [point[0]-i_point[0], point[1]-i_point[1]];	  
  }

  function released(point,info) {
	  var controller = self.getController();    		
	  if (controller) controller.invokeImmediateAction("OnRelease");	  
  }

  function dragged(point,info) {
	  var point = info.point;
	  var element = info.element;
	  var drag_point = [point[0] - pressed_shift[0], point[1] - pressed_shift[1]];	  
	  var group_pos = self.getAbsolutePosition(true);	  
	  var group_size = self.getSize();
	  drag_point[0] = drag_point[0] - group_pos[0];
	  drag_point[1] = (drag_point[1] - group_pos[1]) * (group_size[0]/group_size[1]);
	  var angle = self.interpolateAngle(self.getValue());
	  var rotation = Math.atan2(-drag_point[0],drag_point[1])+Math.PI/2;
	  if (Math.abs(rotation-angle)>mThreshold) return;
	  rotation = self.valueFromAngle(rotation);
	  element.setPosition([0.3*Math.cos(rotation),0.3*Math.sin(rotation)]);
	  var controller = self.getController();    		
	  if (controller) {
		controller.immediatePropertyChanged("Value");
		controller.invokeImmediateAction("OnChange");
	  }	        
  }
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
};

