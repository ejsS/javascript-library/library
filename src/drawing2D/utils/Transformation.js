/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Transformation
 * @class Transformation 
 * @constructor  
 */
EJSS_DRAWING2D.Transformation = {
    
    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * @method compare
     */
    compare : function(mtr1, mtr2) {    	
    	return( mtr1.radians == mtr2.radians && mtr1.a == mtr2.a && mtr1.b == mtr2.b && mtr1.c == mtr2.c && mtr1.d == mtr2.d
    		 && mtr1.e == mtr2.e && mtr1.f == mtr2.f  )
    }
}

/**
 * Transformation object for 2D drawing
 * @class transformation 
 * @constructor  
 */

/**
 * Creates a transformation object for 2D drawing
 */
EJSS_DRAWING2D.transformation = function(transform) {
  transform = transform || { a:1, b:0, c:0, d:1, e:0, f:0 };
  var self = {
    a : transform.a, c : transform.c, e : transform.e,
    b : transform.b, d : transform.d, f : transform.f,
    radians : 0
  };
  
  self.toString = function() {
    return "a="+self.a+", c="+self.c+", e="+self.e+"\n"+
           "b="+self.b+", d="+self.d+", f="+self.f;  
  };
   
  self.setToIdentity = function() {
    self.a = 1; self.c = 0; self.e = 0; 
    self.b = 0; self.d = 1; self.f = 0;
    radians = 0;
  };
  
  self.setTransform = function(tr) {
    if (Array.isArray(tr) && tr.length>5) {
      self.a = tr[0]; self.c = tr[2]; self.e = tr[4];
      self.b = tr[1]; self.d = tr[3]; self.f = tr[5];
    } 
    else {
      self.a = tr.a; self.c = tr.c; self.e = tr.e; 
      self.b = tr.b; self.d = tr.d; self.f = tr.f;
      self.radians = tr.radians;
    }
  };
  
  self.setToTranslation = function(x,y) {
    self.a = 1; self.c = 0; self.e = x; 
    self.b = 0; self.d = 1; self.f = y;
  };

  self.setToRotation = function(radians) {
    var cos = Math.cos(radians), sin = Math.sin(radians);
    self.a = cos;  self.c = -sin; self.e = 0; 
    self.b = sin; self.d = cos; self.f = 0;
    self.radians = radians;
  };

  self.getRotation = function() {
    return self.radians;
  };
  
  self.translate = function(dx,dy) {
    self.e += dx;
    self.f += dy;
  };
  
  self.scale = function(sx,sy) {
    self.a *= sx; self.b *= sx;
    self.c *= sy; self.d *= sy;
  };
  
  self.concatenate = function(tr) {
    var a = self.a, b=self.b, c=self.c, d=self.d, e=self.e, f=self.f;
    self.a = a*tr.a + c*tr.b;
    self.b = b*tr.a + d*tr.b;
    self.c = a*tr.c + c*tr.d;
    self.d = b*tr.c + d*tr.d;
    self.e = a*tr.e + c*tr.f + e,
    self.f = b*tr.e + d*tr.f + f;
    self.radians += tr.radians;
  };
  
  self.transform = function(point) {
    var x = point[0], y = point[1];
    point[0] = self.a*x + self.c*y + self.e;
    point[1] = self.b*x + self.d*y + self.f;
    return point;
  };

  self.transformVector = function(vector) {
    var x = vector[0], y = vector[1];
    vector[0] = self.a*x + self.c*y;
    vector[1] = self.b*x + self.d*y;
    return vector;
  };
  
  self.inverseTransform = function(point) {
    var a = self.a, b=self.b, c=self.c, d=self.d, e=self.e, f=self.f;
    var den = a*d-b*c;
    if (den===0) return null;
    var x = point[0], y = point[1];
    point[0] = ( d*x - c*y + (c*f - d*e))/den;
    point[1] = (-b*x + a*y + (b*e - a*f))/den;
    return point;
  };
  
  //---------------------------------
  // final initialization
  //---------------------------------
  
  return self;
};



