/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

EJSS_DRAWING2D.ColorMapper = {
  CUSTOM : -1,
  SPECTRUM : 0,
  GRAYSCALE : 1,
  DUALSHADE : 2,
  RED : 3,
  GREEN : 4,
  BLUE : 5,
  BLACK : 6,
  WIREFRAME : 7,     // special SurfacePlotter palette
  NORENDER : 8,      // special SurfacePlotter palette
  REDBLUESHADE : 9, // special SurfacePlotter palette

  toRGB : function (mColors) {
	 var htmlColors = [];
	 for (var i = 0, l = mColors.length - 4; i < l; i += 4) {
		var color = mColors[i];
		htmlColors.push("rgb("+color[0]+","+color[1]+","+color[2]+")");
	 }
	 return htmlColors;
  },

  /**
   * Gets a array of colors for use in data visualization.
   *
   * Colors are similar to the colors returned by a color mapper instance.
   * @param numColors
   * @param paletteType
   * @return
   */
  getColorPalette : function(numColors,paletteType) {

	function HSB2RGB (h,s,b){
		var hsb = { "h": Math.round(360*h), "s": s, "b": b };
		var rgb = { "r": 0, "g": 0, "b": 0 };
 
		if (hsb.s === 0){
			rgb.r = rgb.g = rgb.b = hsb.h;
			return rgb;
		}else{
			var Hi = Math.floor(hsb.h/60),
				f = (hsb.h/60) - Hi,
				p = hsb.b * (1 - hsb.s),
				q = hsb.b * (1 - (f * hsb.s)),
				t = hsb.b * (1 - (1 - f) * hsb.s);
			switch(Hi){
				case 0: rgb.r = hsb.b; rgb.g = t; rgb.b = p; break;
				case 1: rgb.r = q; rgb.g = hsb.b; rgb.b = p; break;
				case 2: rgb.r = p; rgb.g = hsb.b; rgb.b = t; break;
				case 3: rgb.r = p; rgb.g = q; rgb.b = hsb.b; break;
				case 4: rgb.r = t; rgb.g = p; rgb.b = hsb.b; break;
				case 5: rgb.r = hsb.b; rgb.g = p; rgb.b = q; break;
			}
			rgb.r = Math.round(rgb.r * 255);
			rgb.g = Math.round(rgb.g * 255);
			rgb.b = Math.round(rgb.b * 255);
 
			return rgb;
		}
	}
  	
  	var ColorMapper = EJSS_DRAWING2D.ColorMapper;
    var colors = [];    
	
    if(numColors<2) numColors = 2;
    
    for(var i = 0; i < numColors; i++) {
      var level = i/(numColors-1)*0.8;
      var porc1 = 1, porc2 = 1;
      var r = 0, g = 0, b = 0;
      switch(paletteType) {
         case ColorMapper.REDBLUESHADE :
           r = Math.floor((Math.max(0, -numColors-1+i*2)*255)/(numColors-1));           
           b = Math.floor((Math.max(0, numColors-1-i*2)*255)/(numColors-1));
		   colors[i] = [r,g,b];
           break;
         case ColorMapper.SPECTRUM :
           level = 0.8-level;     
           var rgb = HSB2RGB (level,porc1,porc2);      
		   colors[i] = [rgb.r,rgb.g,rgb.b];
           break;
         case ColorMapper.GRAYSCALE :
         case ColorMapper.BLACK :
           r = g = b = Math.floor(i*255/(numColors-1));
		   colors[i] = [r,g,b];
           break;
         case ColorMapper.RED :
           r = Math.floor(i*255/(numColors-1));
		   colors[i] = [r,g,b];
           break;
         case ColorMapper.GREEN :
           g = Math.floor(i*255/(numColors-1));
		   colors[i] = [r,g,b];
           break;
         case ColorMapper.BLUE :
           b = Math.floor(i*255/(numColors-1));
		   colors[i] = [r,g,b];
           break;
         case ColorMapper.DUALSHADE :
         default :
           var tmp = i/(numColors-1);
           level = 0.8 * (1-tmp);
           porc2 = 0.2 + 1.6 * Math.abs(0.5-tmp);
           var rgb = HSB2RGB (level,porc1,porc2);      
		   colors[i] = [rgb.r,rgb.g,rgb.b];
           break;
      }
    }
    return colors;
  }

};

/**
 * Creates a Font object for 2D drawing
 */
EJSS_DRAWING2D.colorMapper = function (mNumColors, mPalette) {
  var ColorMapper = EJSS_DRAWING2D.ColorMapper;
  var self = {};  
  
  var mColors = [];  
  var mFloorColor = "darkgray";
  var mCeilColor = "lightgray";
  var mFloor = -1;
  var mCeil = 1;
  var mChangeListener;    
  var mPaletteType;
  var mThresholds;
      
  /**
   * Set a listener that will be called whenever there are Font changes.
   * I.e. a call to listener("change"); will be issued
   */
  self.setChangeListener = function(listener) {
    mChangeListener = listener;
  };
  
  /**
   * Sets the color palette.
   * @param _paletteType
   */
  self.setPaletteType = function(_paletteType) {
	if (typeof _paletteType == "string") _paletteType = EJSS_DRAWING2D.ColorMapper[_paletteType.toUpperCase()];
	mPaletteType = _paletteType;
    mFloorColor = "darkgray";
    mCeilColor = "lightgray";
    if((mPaletteType == EJSS_DRAWING2D.ColorMapper.GRAYSCALE) || 
    	(mPaletteType == EJSS_DRAWING2D.ColorMapper.BLACK)) {
      mFloorColor = "rgb(64,64,128)";
      mCeilColor = "rgb(255,191,191)";
    }
    mNumColors = Math.max(2, mNumColors); // need at least 2 colors
    mColors = ColorMapper.getColorPalette(mNumColors, mPaletteType);
    if (mChangeListener) mChangeListener("palette");
  }

  /**
   * Gets the colors;
   * @return
   */
  self.getColors = function(htmlformatted) {
	if (typeof htmlformatted != "undefined" && htmlformatted) {
		if (Array.isArray(mColors))
			return EJSS_DRAWING2D.ColorMapper.toRGB(mColors);
		// else mColors is in HTML format (rgb, hsl, color names, ...)
	} 
    return mColors;
  }

  /**
   * Gets the floor color;
   * @return
   */
  self.getFloorColor = function() {
    return mFloorColor;
  }

  /**
   * Gets the ceiling color.
   * @return
   */
  self.getCeilColor = function() {
    return mCeilColor;
  }

  /**
   * Gets the number of colors between the floor and ceiling values.
   * @return
   */
  self.getNumColors = function() {
    return mNumColors;
  }

  /**
   * Sets the floor and ceiling colors.
   *
   * @param _floorColor
   * @param _ceilColor
   */
  self.setFloorCeilColor = function(_floorColor,_ceilColor) {
    mFloorColor = _floorColor;
    mCeilColor = _ceilColor;
  }

  /**
   * Sets the floor and ceiling index.
   *
   * @param _floor
   * @param _ceil
   */
  self.setScale = function(_floor,_ceil) {
    mFloor = _floor;
    mCeil = _ceil;
    
    mThresholds = new Array(mColors.length + 1);
    var delta = (mCeil-mFloor) / mColors.length;
    for (var i=0,n=mColors.length; i<n; i++) mThresholds[i] = mFloor + i*delta;
    mThresholds[mColors.length] = mCeil;
  }

  /**
   * Returns the color palette.
   * @return mode
   */
  self.getPaletteType = function() {
    return mPaletteType;
  }

  /**
   * Sets the color palette.
   * @param _colors
   */
  self.setColorPalette = function(_colors) {  	
  	mFloorColor = "darkgray";
  	mCeilColor = "lightgray";
    mColors = _colors;
    mNumColors = _colors.length;
    mPaletteType = EJSS_DRAWING2D.ColorMapper.CUSTOM;
    if (mChangeListener) mChangeListener("colors");
  }

  /**
   * Sets the number of colors
   * @param _numColors
   */
  self.setNumberOfColors = function(_numColors) {
    if(_numColors == mNumColors) {
      return;
    }
    mNumColors = _numColors;
    if(mPaletteType == EJSS_DRAWING2D.ColorMapper.CUSTOM) {
      var newColors = [];
      for(var i = 0, n = Math.min(colors.length, mNumColors); i<n; i++) {
        newColors[i] = colors[i];
      }
      for(var i = colors.length; i<numColors; i++) {
        newColors[i] = colors[colors.length-1];
      }
      colors = newColors;
    } else {
      self.setPaletteType(mPaletteType);
    }
    if (mChangeListener) mChangeListener("numColors");
  }

  /**
   * Gets the number of colors
   * @param _numColors
   */
  self.getNumberOfColors = function() {
  	return mNumColors;
  }

 /**
   * Converts a double to an index in the color array.
   * @method doubleToIndex
   * @param value
   * @return the index in the array with the following exceptions:
   * <ul>
   *   <li>-1 if floor color</li>
   *   <li>colors.length if ceil color</li>
   * </ul> 
   */
  self.doubleToIndex = function(value) { 
    if(mFloor-value>Number.MIN_VALUE) {
      return -1;
    } else if(value-mCeil>Number.MIN_VALUE) {
      return mColors.length;
    }
    var index = 0;
    if(mCeil != mFloor)
    	index = Math.floor((mColors.length*(value-mFloor)/(mCeil-mFloor)));
    index = Math.max(0, index);
    return Math.min(index, mColors.length-1);
  }
  
  /**
   * Returns the color for an index
   * @method indexToColor
   */
  self.indexToColor = function(index, htmlformatted) { 
    if (index<0) return mFloorColor;
    if (index>=mColors.length) return mCeilColor;
	var color = mColors[index];
    if (htmlformatted && Array.isArray(color)) 
		return "rgb("+color[0]+","+color[1]+","+color[2]+")";
    return mColors[index];
  }

  /**
   * Returns the color for an index
   * @method indexToColor
   */
  self.doubleToColor = function(value, htmlformatted) { 
	return self.indexToColor(self.doubleToIndex(value), htmlformatted); 
  } 
  
  /**
   * Returns the thresholds for color change. One more than colors, includes ceil and floor
   * @method getColorThresholds
   */
  self.getColorThresholds = function() { 
  	return mThresholds;
  }

  self.setPaletteType(mPalette); // default colors
  self.setScale(-1,1);
  return self;	
}