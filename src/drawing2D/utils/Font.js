/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Font object for 2D drawing
 * @class Font 
 * @constructor  
 */
EJSS_DRAWING2D.Font = {
  FONTSTYLES: ["normal", "italic", "oblique", "initial"],
  FONTWEIGHTS: ["normal", "lighter", "bold", "bolder", "initial", 
  	"100", "200", "300", "400", "500", "600", "700", "800", "900"],

  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * Copies one element into another
   */
  copyTo : function(source, dest) {
  	dest.setFontFamily(source.getFontFamily());
  	dest.setFontSize(source.getFontSizeString());
	dest.setLetterSpacing(source.getLetterSpacing());
	dest.setOutlineColor(source.getOutlineColor());
	dest.setOutlineWidth(source.getOutlineWidth());
	dest.setFontWeight(source.getFontWeight());
	dest.setFillColor(source.getFillColor());  	
  },

};

/**
 * Creates a Font object for 2D drawing
 */
EJSS_DRAWING2D.font = function (mName) {
  var self = {};
  
  var mFontFamily = "Arial"; 	// the type of font to use, for instance 'Arial' or 'Verdana'.
  var mFontSize = "20";			// the size of the font, for instance '12' or '24'.
  var mLetterSpacing = "normal";// spacing between letters, for instance '2' or '3'. Similar to kerning.
  var mLineColor = "none";		// the outline color of the font. By default text only has fill color, not stroke.
  var mLineWidth = "1";			// the outline width of the font. By default 1.
  var mFontWeight = "normal";	// the weight of the font.
  var mFillColor = "black";		// the fill color of the font. 	    
  var mFontStyle = "none";
  var mChangeListener;           
      
  /**
   * Set a listener that will be called whenever there are Font changes.
   * I.e. a call to listener("change"); will be issued
   */
  self.setChangeListener = function(listener) {
    mChangeListener = listener;
  };

  /**
   * Set the family of the font.
   * @method setFontFamily
   * @param fontFamily
   */
  self.setFontFamily = function(fontFamily) {
    if (mFontFamily != fontFamily) {
      mFontFamily = fontFamily;
      if (mChangeListener) mChangeListener("fontFamily");
    }
  };
      
  /**
   * Get the family of the font.
   * @method getFontFamily
   * @return font family
   */
  self.getFontFamily = function() { 
    return mFontFamily; 
  };

  /**
   * Set the size of the font.
   * @method setFontSize
   * @param fontSize
   */
  self.setFontSize = function(fontSize) {
    if (mFontSize != fontSize) {
      mFontSize = fontSize;
      if (mChangeListener) mChangeListener("fontSize");
    }
  };
      
  /**
   * Get the size of the font in pixels.
   * @method getFontSize
   * @return font size
   */
  self.getFontSize = function() {
  	var size;
  	if(!isNaN(mFontSize)) { 
  		size = +mFontSize; // number
    } else if(mFontSize.indexOf("em") != -1) {
    	size = 	+(mFontSize.substr(0,mFontSize.indexOf("em"))) * 10;	// aprox
    } else if(mFontSize.indexOf("px") != -1) { 
    	size = +(mFontSize.substr(0,mFontSize.indexOf("px"))); // px
    } else if(mFontSize.indexOf("vh") != -1) { 
		var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    	size = (h/100)*(+(mFontSize.substr(0,mFontSize.indexOf("vh")))); // vh    	
    } else if(mFontSize.indexOf("vw") != -1) { 
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    	size = (w/100)*(+(mFontSize.substr(0,mFontSize.indexOf("vw")))); // vw
    } else {
    	size = 0; // for example, rem
    }
    
    return size; 
  };

  /**
   * Get the size of the font as a String
   * @method getFontSizeString
   * @return font size as a String
   */
  self.getFontSizeString = function() { 
    return mFontSize; 
  };

  /**
   * Set the letter spacing of the font.
   * @method setLetterSpacing
   * @param letterSpacing
   */
  self.setLetterSpacing = function(letterSpacing) {
    if (mLetterSpacing != letterSpacing) {
      mLetterSpacing = letterSpacing;
      if (mChangeListener) mChangeListener("letterSpacing");
    }
  };

  /**
   * Get the letter spacing of the font.
   * @method getLetterSpacing
   * @return letter spacing
   */
  self.getLetterSpacing = function() {
    return mLetterSpacing;
  };
      
  /**
   * Get the letter spacing of the font.
   * @method getNumberLetterSpacing
   * @return letter spacing
   */
  self.getNumberLetterSpacing = function() {
  	var spacing;
  	if(!isNaN(mLetterSpacing)) { 
  		spacing = +mLetterSpacing; // number
    } else if(mLetterSpacing.indexOf("px") != -1) { 
    	spacing = +(mLetterSpacing.substr(0,mLetterSpacing.indexOf("px"))); // px
    } else {
    	spacing = 1; // for example, normal
    }
    return spacing; 
  };

  /**
   * Set the line color of the font.
   * @method setOutlineColor
   * @param lineColor
   */
  self.setOutlineColor = function(lineColor) {
    if (mLineColor != lineColor) {
      mLineColor = lineColor;
      if (mChangeListener) mChangeListener("lineColor");
    }
  };
      
  /**
   * Get the line color of the font.
   * @method getOutlineColor
   * @return line color
   */
  self.getOutlineColor = function() { 
    return mLineColor; 
  };

  /**
   * Set the line width of the font.
   * @method setOutlineWidth
   * @param lineWidth
   */
  self.setOutlineWidth = function(lineWidth) {
    if (mLineWidth != lineWidth) {
      mLineWidth = lineWidth;
      if (mChangeListener) mChangeListener("lineWidth");
    }
  };
      
  /**
   * Get the line color of the font.
   * @method getOutlineWidth
   * @return line width
   */
  self.getOutlineWidth = function() { 
    return mLineWidth; 
  };

  /**
   * Set the weight of the font.
   * @method setFontWeight
   * @param fontWeight
   */
  self.setFontWeight = function(fontWeight) {
	  fontWeight = fontWeight.toLowerCase(); 

    if (mFontWeight != fontWeight && (EJSS_DRAWING2D.Font.FONTWEIGHTS.indexOf(fontWeight) > -1)) {
      mFontWeight = fontWeight;
      if (mChangeListener) mChangeListener("fontWeight");
    }
  };
      
  /**
   * Get the weight of the font.
   * @method getFontWeight
   * @return fontWeight
   */
  self.getFontWeight = function() { 
    return mFontWeight; 
  };

  /**
   * Set the fill color of the font.
   * @method setFillColor
   * @param fillColor
   */
  self.setFillColor = function(fillColor) {
    if (mFillColor != fillColor) {
      mFillColor = fillColor;
      if (mChangeListener) mChangeListener("fillColor");
    }
  };
      
  /**
   * Get the fill color of the font.
   * @method getFillColor
   * @return fill color
   */
  self.getFillColor = function() { 
    return mFillColor; 
  };
  
  /**
   * Set the font style (normal, italic, oblique, initial, none).
   * @method setFontStyle
   * @param fontStyle
   */
  self.setFontStyle = function(fontstyle) {
	fontstyle = fontstyle.toLowerCase(); 
    if (mFontStyle != fontstyle && (EJSS_DRAWING2D.Font.FONTSTYLES.indexOf(fontstyle) > -1)) {
      mFontStyle = fontstyle;
      if (mChangeListener) mChangeListener("fontstyle");
    }
  };
      
  /**
   * Get the font style.
   * @method getFontStyle
   * @return fontStyle
   */
  self.getFontStyle = function() { 
    return mFontStyle; 
  };

  /**
   * @method getFont
   * @return font
   */
  self.getFont = function() {
  	return mFontStyle + " " + mLineWidth + " " + mFontSize + " " + mFontFamily;
  };

  /**
   * @method setFont
   * @param recap string, format: [style weight size[/lineHeight] [family]]
   */ 
  self.setFont = function (recap) {  
    if ((typeof(recap) !== 'string') || recap.length<=0) return;	
    var params = recap.split(" ");        
   	mFontStyle = params[0];		// style   	
   	mFontWeight = params[1];	// weight	
	var sizes = params[2].split("/");   
   	mFontSize = sizes[0];			// size
   	// ignoring sizes[1] lineHeight   	
   	if (params[3]) 
   		mFontFamily = recap.substring(recap.indexOf(params[3])); // family  
  }
      

  
  //---------------------------------
  // final initialization
  //---------------------------------
  
  return self;
};

