/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia and Félix J. García
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for 2D drawing.
 * @module 2Ddrawing
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * PanelInteraction class
 * @class PanelInteraction
 * @constructor
 */
EJSS_DRAWING2D.PanelInteraction = {
	TARGET_POSITION : 0,
	TARGET_SIZE : 1
};

/**
 * Constructor for Interaction
 * @param drawing panel
 * @returns An interaction
 */
EJSS_DRAWING2D.panelInteraction = function(mPanel) {
	var self = {};
	// reference returned

	// Static references
	var PanelInteraction = EJSS_DRAWING2D.PanelInteraction;	// reference for PanelInteraction

	var mEnabled = false;		// whether interaction is enabled
	var mStopGestures = false;  // whether gesture propagation is enabled
	var mStopMoveEvents = false;// wheteher move event listener is enabled when not clicking (recomended in CANVAS)
	var mLastPoint = [];		// last position on panel trying interaction
	var mFirstPoint = [];		// first position on panel trying interaction
	var mPreviousPoint = [];	// position previous to last position
	var mOnlyTarget = null;		// only InteractionTarget with interaction
	var mIsPanelOnlyTarget = false;	// whether the target is the panel 
	var mMouseIsDown = false;	// last event is mouse down
	var mLastTime = 0;			// last timestamp for mouse up
	var mPinching = 0;			// pinching (0: nothing, 1: panel, 2: element)
	var mPinchInitDistance = [0,0];		// distance between touches in pinching
	var mPinchRelDistance = [0,0];		// relative distance when move in pinching
	var mZoomDelta = 0;					// zooming delta
	var mCursorTypeForMove = "move"; // type of cursor for move event (ex., "move", "crosshair")

	var mElementInteracted = 0;	// index of interacted element into its element array

	/**
	 * Reload the interaction configuration
	 */
	self.reload = function() {
		mLastPoint = [];
		mFirstPoint = [];		
		mPreviousPoint = [];
		mOnlyTarget = null;		
		mMouseIsDown = false;	
		mLastTime = 0;			
		mPinching = 0;
		mPinchInitDistance = [0,0];
		mPinchRelDistance = [0,0];
		mZoomDelta = 0;
		mElementInteracted = 0;	
		
		mEnabled = !mEnabled;
		self.setEnabled(!mEnabled);
		mStopGestures = !mStopGestures;
		self.setStopGestures(!mStopGestures);			
	}
	
	/**
	 * Return panel
	 * @return drawing panel
	 */
	self.getPanel = function() {
		return mPanel;
	};

	self.getEnabled = function() {
		return mEnabled;
	};

	self.setStopMoveEvents = function(stop) {
		if (mStopMoveEvents != stop) {
			mStopMoveEvents = stop;

			if (mStopMoveEvents) {
				self.setHandler("move", (function() { return true; }));
			} else {
				self.setHandler("move", self.handleMouseMoveEvent);
			}
		}		
	}

	self.setStopGestures = function(stop) {
		if (mStopGestures != stop) {
			mStopGestures = stop;

			if (mStopGestures) {
				self.setHandler("gesturestart", function(e) {e.preventDefault();e.stopPropagation();return false;});
				self.setHandler("gesturechange", function(e) {e.preventDefault();e.stopPropagation();return false;});
				self.setHandler("gestureend", function(e) {e.preventDefault();e.stopPropagation();return false;});
			} else {
				self.setHandler("gesturestart", function() { return true; });
				self.setHandler("gesturechange", function() { return true; });
				self.setHandler("gestureend", function() { return true; });
			}
		}		
	}

	self.setEnabled = function(enabled) {
		if (mEnabled != enabled) {
			mEnabled = enabled;

			if (mEnabled) {
				if (!mStopMoveEvents) {
					self.setHandler("move", self.handleMouseMoveEvent);
				}
				self.setHandler("down", self.handleMouseDownEvent);
				self.setHandler("up", self.handleMouseUpEvent);
				self.setHandler("mousewheel",self.handleMouseWheelEvent);
			} else {
				self.setHandler("move", (function() { return true; }));
				self.setHandler("down", (function() { return true; }));
				self.setHandler("up", (function() { return true; }));
				self.setHandler("mousewheel", (function() { return true; }));
			}
		}
	};
	
	self.setCursorTypeForMove = function(type) {
		mCursorTypeForMove = type;
	}
	
	/**
	 * Return the last interaction point 
	 * @method getInteractionPoint
	 * @return double[]
	 */
	self.getInteractionPoint = function() {
		return mPanel.toPanelPosition(mLastPoint);
	};

	/**
	 * Return the distance between the previous and last interaction
	 * @method getInteractionDistance
	 * @return double[]
	 */
	self.getInteractionDistance = function() {
		if(mPreviousPoint.length == 0 || mLastPoint.length == 0) return [];
		var p = mPanel.toPanelPosition(mPreviousPoint);
		var l = mPanel.toPanelPosition(mLastPoint);
		return [p[0] - l[0], p[1] - l[1]];
	};

	/**
	 * Return the bounds of the last interaction rectangle 
	 * @method getInteractionBounds
	 * @return double[]
	 */
	self.getInteractionBounds = function() {
		if(mFirstPoint.length == 0 || mLastPoint.length == 0) return [];
		var f = mPanel.toPanelPosition(mFirstPoint);
		var l = mPanel.toPanelPosition(mLastPoint);
		return [f[0], l[0], f[1], l[1]];
	};

	/**
	 * Return the zooming delta
	 * @method getInteractionZoomDelta
	 * @return double[]
	 */
	self.getInteractionZoomDelta = function() {
		// zooming via mousewheel or pinching 
		if(mZoomDelta == 0 && mPinchRelDistance[0] == 0) return 0;
		if(mZoomDelta > 0 || mPinchRelDistance[0] > 0) return 1;
		return -1;
	};
	
	/**
	 * Return the only interaction target (only one touch)
	 * @method getInteractionElement
	 * @return element
	 */
	self.getInteractionElement = function() {
		return mOnlyTarget;
	};

  /**
   * Clears the current interaction elementn if it equals the given one
   * (for example, if it was deleted)
   * @method clearInteractionElement
   * @return element
   */
  self.clearInteractionElement = function(element) {
    if (mOnlyTarget && element===mOnlyTarget.getElement()) mOnlyTarget = null;
  };

	/**
	 * Return the screen orientation
	 * @method getOrientation
	 * @return orientation
	 */
	self.getOrientation = function() {
		return window.orientation;
	};
	
	/**
	 * Handler for mouse move event
	 * @method handleMouseMoveEvent
	 * @param event
	 * @param location
	 */
	self.handleMouseMoveEvent = function(event) {
		if(mStopGestures) {
		  // Prevent the browser from doing its default thing (scroll, zoom)
		  event.preventDefault();
		  // prevent the browser from propagation
		  event.stopPropagation();
		}

		// number of fingers over screen	
		var nFingers = (typeof event.touches != "undefined")? event.touches.length:1;

		// manage pinch		 
		if(nFingers == 2) {
    		var e0 = event.touches[0]; var e1 = event.touches[1];
    		var e0x = (e0.clientX || e0.x); var e0y = (e0.clientY || e0.y);
    		var e1x = (e1.clientX || e1.x); var e1y = (e1.clientY || e1.y);
			if(mPinching != 0) {
	    		var newDistance = [Math.abs(e0x-e1x),Math.abs(e0y-e1y)];
	    		var delta = [mPinchInitDistance[0]-newDistance[0],mPinchInitDistance[1]-newDistance[1]]; // used in zooming
	    		if (delta[0]<8 && delta[0]>-8 && delta[1]<8 && delta[1]>-8) { // minimum filter
	    			mPinchRelDistance = [0,0];
	    		} else {
		    		mPinchRelDistance = delta;
		    		mPinchInitDistance = newDistance;
	    		}
			} else {
				mPinching = 1;
	    		mPinchInitDistance = [Math.abs(e0x-e1x),Math.abs(e0y-e1y)];
				mPinchRelDistance = [0,0];			
			}
		} else {
			mPinching = 0;
			mPinchInitDistance = [0,0];
		}
		
		// touch locations 
		var locations = getEventLocations(event);

		// last and previous location		
		mPreviousPoint = mLastPoint;
		mLastPoint = locations[locations.length-1];
		
		if (mMouseIsDown) { // Mouse or finger(s) is down (drag)
			if (mOnlyTarget != null) { // whether mOnlyTarget exists, only one touch and target
				// get element or group
				var element = mOnlyTarget.getElement();
				if (mOnlyTarget.getAffectsGroup()) {
					var group = element.getGroup();
					if (group != null) element = group;
				}
				// register properties changed
				var point = mPanel.toPanelPosition(mLastPoint);
				var refpoint = mPanel.toPanelPosition(mPreviousPoint);
				propertiesChanged(element, mOnlyTarget, point, refpoint);
				// invoke actions
				element.getController().invokeAction("OnDrag");				
				if (mEnabled)// also onMove over panel
					mPanel.getController().invokeAction("OnMove");
			} else if(mIsPanelOnlyTarget) { // the only target is the panel
				if (mEnabled) { // onDrag over panel
					mPanel.getController().invokeAction("OnDrag");
				}
			} else { // multiple touches
				// invoke actions
				var targetHitList = [];
				for(var i=0; i<locations.length; i++) {					
					var targetHit = self.findInteractionTarget(mPanel.getElements(), locations[i]);
					if(targetHit != null) {
						targetHitList.push(targetHit);
						// get element or group
						var element = targetHit.getAffectsGroup() ? targetHit.getElement().getGroup() : targetHit.getElement();
						// register properties changed
						var point = mPanel.toPanelPosition(locations[i]);
						propertiesChanged(element, targetHit, point, targetHit.getPosition());
						// invoke actions
						element.getController().invokeAction("OnDrag");				
						if (mEnabled) // also onMove over panel
							mPanel.getController().invokeAction("OnMove");			
					}
				}
				// actions over panel
				if (mEnabled) { 
					if(mPinching == 0)
						mPanel.getController().invokeAction("OnDrag");
					else if (mPinching == 1 && targetHitList.length == 0) 
						mPanel.getController().invokeAction("OnZoom");						
				}
			}
				
		} else { // Mouse is up (move over panel and elements using mouse)
			var targetHit = self.findInteractionTarget(mPanel.getElements(), locations[0]);
			if (targetHit === null) { // no target
				if (mOnlyTarget != null)// whether mOnlyTarget exists
					mOnlyTarget.getElement().getController().invokeAction("OnExit");
				if (mEnabled) // onMove over panel
					mPanel.getController().invokeAction("OnMove");
				event.target.style.cursor = 'default';
			} else {
				if (mOnlyTarget != targetHit) {
					if (mOnlyTarget != null)// whether mOnlyTarget exists
						mOnlyTarget.getElement().getController().invokeAction("OnExit");
					targetHit.getElement().getController().invokeAction("OnEnter");
				}
				if (targetHit.isEnabled())
					event.target.style.cursor = mCursorTypeForMove;
				else
					event.target.style.cursor = 'default';
			}
			mOnlyTarget = targetHit;				
		}

		// all interactions are reported to View
		mPanel.getController().reportInteractions();
		
		return false;
	};

	/**
	 * Handler for mouse down event
	 * @method handleMouseDownEvent
	 * @param event
	 * @param location
	 */
	self.handleMouseDownEvent = function(event) {
		mMouseIsDown = true;

		if (mStopMoveEvents) {
			// for drag&drop 
			self.setHandler("move", self.handleMouseMoveEvent);
		}
					
		if(mStopGestures) {
		  // prevent the browser from doing its default thing (scroll, zoom)
		  event.preventDefault();
		  // prevent the browser from propagation
		  event.stopPropagation();
		}

		// number of fingers over screen	
		var nFingers = (typeof event.touches != "undefined")? event.touches.length:1;
			
		// manage pinch		 
		if(nFingers != 2) {
			mPinching = 0;
		}
		
		// touch locations 
		var locations = getEventLocations(event);

		// first, previous and last points
		mFirstPoint = locations[0]; // first location
		mPreviousPoint = mFirstPoint;
		mLastPoint = locations[locations.length-1];	// last location
				 
		// invoke actions
		var isPanelHit = false;
		var targetHit = null;
		for(var i=0; i<locations.length; i++) {
			// find target
			targetHit = self.findInteractionTarget(mPanel.getElements(), locations[i]);
			if (targetHit !== null) { // one target
				targetHit.getElement().getController().invokeAction("OnPress");
				event.target.style.cursor = mCursorTypeForMove;
			} else if (mEnabled) { // no target, then onPress over panel
				mPanel.getController().invokeAction("OnPress");
				isPanelHit = true;
			}
		}		
		
		// mOnlyTarget and mIsPanelOnlyTarget are usefull whether only one touch
		if(locations.length == 1 && nFingers == 1) { 					
			// targetHit is null if no target, so the target is the panel 
			mOnlyTarget = targetHit;	// last target
			mIsPanelOnlyTarget = isPanelHit;
		} else {
			// no only target
			mOnlyTarget = null;
			mIsPanelOnlyTarget = false;			
		}
		
		// all interactions are reported to View
		mPanel.getController().reportInteractions();		
		
		return false;
	};

	/**
	 * Handler for mouse up event
	 * @method handleMouseUpEvent
	 * @param event
	 * @param location
	 */
	self.handleMouseUpEvent = function(event) {
		mMouseIsDown = false;

		if (mStopMoveEvents) {
			// for drag&drop
			self.setHandler("move", (function() { return true; }));
		}

		if(mStopGestures) {
		  // prevent the browser from doing its default thing (scroll, zoom)
		  event.preventDefault();
		  // prevent the browser from propagation
		  event.stopPropagation();
		}

		// manage pinch		 
		if(mPinching != 0) { 
    		mPinching = 0;
    		mPinchRelDistance = [0,0];
    		mPinchInitDistance = [0,0];
		}

		// touch locations 
		var locations = getEventLocations(event);

		// last location
		mPreviousPoint = mLastPoint;
		mLastPoint = locations[locations.length-1];
			
		// whether event is dbclick
		var dblclick = (event.timeStamp - mLastTime < 500); // < 500 ms is dblclick
			
		if(mOnlyTarget != null) { // only one touch
			// mOnlyTarget is null if no element touched						
			mOnlyTarget.getElement().getController().invokeAction("OnRelease");
			if (dblclick) mOnlyTarget.getElement().getController().invokeAction("OnDoubleClick");
		} else {				
			// invoke actions		
			for(var i=0; i<locations.length; i++) {
				// find target
				var targetHit = self.findInteractionTarget(mPanel.getElements(), locations[i]);
				if (targetHit !== null) { // one target
					targetHit.getElement().getController().invokeAction("OnRelease");
					if (dblclick) targetHit.getElement().getController().invokeAction("OnDoubleClick");
				} else if (mEnabled) { // no target, then onRelease over panel
					mPanel.getController().invokeAction("OnRelease");
					if (dblclick) mPanel.getController().invokeAction("OnDoubleClick");
				}
			}
		}
				
		// last time to manage double click
		mLastTime = event.timeStamp; 

		// cursor style
		event.target.style.cursor = 'default';

		// all interactions are reported to View
		mPanel.getController().reportInteractions();
		
		return false;
	};

	/**
	 * Handler for mouse wheel event
	 * @method handleMouseWheelEvent
	 * @param event
	*/
	self.handleMouseWheelEvent = function(event) {
		// number of fingers over screen	
		var nFingers = (typeof event.touches != "undefined")? event.touches.length:0;
		
		if(nFingers == 0) {  	
			if(mStopGestures) {
			  // prevent the browser from doing its default thing (scroll, zoom)
			  event.preventDefault();
			  // prevent the browser from propagation
			  event.stopPropagation();
			}
	
			mZoomDelta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
			mPanel.getController().invokeAction("OnZoom");	
	        
		    // all interactions are reported to View
		    mPanel.getController().reportInteractions();
		}
	};
	
	/**
	 * Find target element for the position
	 * @method findInteractionTarget
	 * @param location double[]
	 * @return double[]
	 */
	self.findInteractionTarget = function(elements, location) {
	    var InteractionTarget = EJSS_DRAWING2D.InteractionTarget;
		var target = null;
		for (var i = elements.length - 1; i >= 0; i--) {
			var element = elements[i];
			if (element.isGroupVisible()) {
				if (element.getElements) {// array of elements
					target = self.findInteractionTarget(element.getElements(), location);
					if (target !== null)
						return target;
				} else {// one element
					// find interaction target for element
					var targets = element.getInteractionTargets();
					for (var j = 0; j < targets.length; j++) {
						if (targets[j].isEnabled()) {
							var sensitivity = targets[j].getSensitivity();
							if (sensitivity > 0) {
								var projectedPosition = targets[j].getPixelPosition();
								var selected = false;
								switch(targets[j].getSensitivityType()) {
								  case InteractionTarget.SENSITIVITY_HORIZONTAL :
								    selected = (Math.abs(projectedPosition[1] - location[1]) < sensitivity); 
								    break;
								  case InteractionTarget.SENSITIVITY_VERTICAL :
								    selected = (Math.abs(projectedPosition[0] - location[0]) < sensitivity); 
								    break;
								  case InteractionTarget.SENSITIVITY_RADIUS :
										dx = projectedPosition[0] - location[0];
										dy = projectedPosition[1] - location[1];
								    selected = (Math.sqrt(dx*dx+dy*dy) < sensitivity);
								    break;
									case InteractionTarget.SENSITIVITY_ANY :
										selected = (Math.abs(projectedPosition[0] - location[0]) < sensitivity || Math.abs(projectedPosition[1] - location[1]) < sensitivity);
										break;
									default : // InteractionTarget.SENSITIVITY_BOTH
								    selected = (Math.abs(projectedPosition[0] - location[0]) < sensitivity && Math.abs(projectedPosition[1] - location[1]) < sensitivity);
								    break;
								}
								if (selected) {
									mElementInteracted = i;
									// index of element into element array
									// var pp = element.toGroupSpace(element.getPosition(),true);
									// console.log('pp' + pp[0] + ' ' + pp[1]);
									
									return targets[j];
								}
							}
					    }
					}
					// maybe, any location into element (pinching?)
					if(mPinching != 0) {
						target = element.getInteractionTarget(PanelInteraction.TARGET_SIZE);
						if (target.isEnabled() && !target.getSensitivity()) {
							var rect = element.getAbsoluteBounds();
							var pos = mPanel.toPanelPosition(location);
							if ((rect.left < pos[0]) && (rect.right > pos[0]) && (rect.bottom < pos[1]) && (rect.top > pos[1])) {
								mElementInteracted = i;
								// index of element into element array
								mPinching = 2; 
								return target;
							}
						}
					} 
					// maybe, any location into element (position interaction?)
					target = element.getInteractionTarget(PanelInteraction.TARGET_POSITION);
					if (target.isEnabled() && !target.getSensitivity()) {
						var rect = element.getAbsoluteBounds();
						var pos = mPanel.toPanelPosition(location);
						if ((rect.left < pos[0]) && (rect.right > pos[0]) && (rect.bottom < pos[1]) && (rect.top > pos[1])) {
							mElementInteracted = i;
							// index of element into element array
							return target;
						}
					}						
				}
			}
		}
		return null;
	};

	/**
	 * Return index of interacted element into element array
	 */
	self.getIndexElement = function() {
		return mElementInteracted;
	};
	
	/**
	 * Register properties changed for a motion of the mouse when dragging the element
	 * For subclasses use only!
	 */
	function propertiesChanged(element, target, point, targetPosition) {
		var InteractionTarget = EJSS_DRAWING2D.InteractionTarget;
		var PanelInteraction = EJSS_DRAWING2D.PanelInteraction;

		// reports changes for element
		if (target.getType() === PanelInteraction.TARGET_POSITION) {
    	    var dx = point[0]-targetPosition[0];
    	    var dy = point[1]-targetPosition[1];
			switch (target.getMotionEnabled()) {
				case InteractionTarget.ENABLED_ANY :
					element.setX(element.getX() + dx);
					element.setY(element.getY() + dy);
					element.getController().propertiesChanged("Position", "X", "Y");
					target.getElement().setChanged(true);
					element.setChanged(true);
					break;
				case InteractionTarget.ENABLED_X :
					element.setX(element.getX() + dx);
					element.getController().propertiesChanged("Position", "X");
					target.getElement().setChanged(true);
					element.setChanged(true);
					break;
				case InteractionTarget.ENABLED_Y :
					element.setY(element.getY() + dy);
					element.getController().propertiesChanged("Position", "Y");
					target.getElement().setChanged(true);
					element.setChanged(true);
					break;
			}
		} else if (target.getType() === PanelInteraction.TARGET_SIZE) {
			if(mPinching == 2) {
				// not used targetPosition, directly we have the pinching distance
	    	    var diff = mPanel.toPanelMod(mPinchRelDistance);
	    	    var dx = diff[0];
	    	    var dy = -diff[1];		
			} else {
	    	    var dx = point[0]-targetPosition[0];
	    	    var dy = point[1]-targetPosition[1];
			}
				
		    var targetElement = target.getElement();
			var absSize = targetElement.getAbsoluteSize();
			if (absSize[0]===0) absSize[0] = 1.0e-5;
			if (absSize[1]===0) absSize[1] = 1.0e-5;
			var rsizes = [dx/absSize[0], dy/absSize[1]];
			
			var size = element.getSize();
			if (size[0]===0 && rsizes[0]!==0) size[0] = 1.0e-5;
			if (size[1]===0 && rsizes[0]!==0) size[1] = 1.0e-5;				

			switch (target.getMotionEnabled()) {
				case InteractionTarget.ENABLED_ANY :
					element.setSizeX(size[0] + size[0] * rsizes[0]);
					element.setSizeY(size[1] + size[1] * rsizes[1]);					
					element.getController().propertiesChanged("Size", "SizeX", "SizeY");
					target.getElement().setChanged(true);
					element.setChanged(true);
					break;
				case InteractionTarget.ENABLED_X :
					element.setSizeX(size[0] + size[0] * rsizes[0]);
					element.getController().propertiesChanged("Size", "SizeX");
					target.getElement().setChanged(true);
					element.setChanged(true);
					break;
				case InteractionTarget.ENABLED_Y :
					element.setSizeY(size[1] + size[1] * rsizes[1]);					
					element.getController().propertiesChanged("Size", "SizeY");
					target.getElement().setChanged(true);
					element.setChanged(true);
					break;
			}
		}
	}

	/**
	 * Get locations for the touch events
	 * @method getEventLocations
	 * @param {Object} e
	 */
	function getEventLocations(e) {
		var box = mPanel.getGraphics().getBox();
		var oleft = box.left; // offset left in pixels
		var otop = box.top; // offset top in pixels
		var locations = [];

		if ( typeof e.changedTouches != "undefined") {
			for(var i=0; i<e.changedTouches.length; i++) {
				locations[i] = [];		
				// ignored window.pageYOffset and window.pageXOffset
		    	locations[i][0] = e.changedTouches[i].clientX - oleft;
		    	locations[i][1] = e.changedTouches[i].clientY - otop;
		   }
		} else {
			locations[0] = [];
			locations[0][0] = (e.clientX || e.x) - oleft;
			locations[0][1] = (e.clientY || e.y) - otop;
		}

		return locations;
	}

	self.setHandler = function(type, handler) {
		var graphics = mPanel.getGraphics();
		var context = graphics.getEventContext();
		switch (type) {
			case "move" :
				context.addEventListener('mousemove', handler, false);
				context.addEventListener('touchmove', handler, false);
				break;
			case "down" :
				context.addEventListener('mousedown', handler, false);
				context.addEventListener('touchstart', handler, false);
				break;
			case "up" :
				context.addEventListener('mouseup', handler, false);
				context.addEventListener('touchend', handler, false);
				break;
		   	case "mousewheel" :
		      	context.addEventListener('mousewheel', handler, false);
			  	break;	      

			// not used 
			case "gesturestart" : // Sent when the second finger touches the surface.
				context.addEventListener('gesturestart', handler, false);
				break;
			case "gesturechange" : //  Sent when both fingers move while still touching the surface.
				context.addEventListener('gesturechange', handler, false);
				break;
			case "gestureend" : //  Sent when the second finger lifts from the surface.
				context.addEventListener('gestureend', handler, false);
				break;
			default :
		}
		return false;
	};
	
	// change orientation
	var _super_onorientationchange = window.onorientationchange; 
		
	window.onorientationchange = function() {		
		if(_super_onorientationchange) _super_onorientationchange();
		mPanel.getController().invokeAction("OnOrientationChange");
		mPanel.getController().reportInteractions();
	};
	
	// resize
	var _super_onresize = window.onresize; 
	window.onresize = function() {
		if(_super_onresize) _super_onresize();		
		mPanel.getController().invokeAction("OnResize");
		mPanel.getController().reportInteractions();
	};

	// for default, stop propagation
	self.setStopGestures(true);
	
	return self;
};
