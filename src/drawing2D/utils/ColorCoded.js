/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Creates a ColorCoded object for 2D drawing
 * ColorCoded is a utility class that allows drawing a collection of polygons using a color code.
 * Each point in the polygon has a double value associated to it and the drawer uses a color mapper to find color subpolygons 
 * for each of the levels of the mapper
 * 
 */
EJSS_DRAWING2D.colorCoded = function (mNumColors, mPalette) {
  var self = EJSS_DRAWING2D.colorMapper(mNumColors, mPalette);  
    
  // Configuration variables
  var mSymmetricZ = false;
      
  // -------------------------------------
  // Public methods for coloring the tiles
  // -------------------------------------
      
  /**
   * Forces the z-scale to be symmetric about zero.
   * Forces zmax to be positive and zmin=-zmax when in autoscale mode.
   * @method setSymmetricZ
   * @param symmetric
   */
  self.setSymmetricZ = function(symmetric) {
    mSymmetricZ = symmetric;
  }
 
  /**
   * Gets the symmetric z flag.
   * @method isSymmetricZ 
   */
  self.isSymmetricZ = function(){
    return mSymmetricZ;
  }
  
  // -------------------------------------
  // Utility methods
  // -------------------------------------

  /**
   * Computes the extrema and sets the scales for the ColorMapper
   * @method setAutoscale
   * @param values
   */
  self.setAutoscale = function(values) {
    var min = Number.MAX_VALUE;
    var max = Number.MIN_VALUE;
    for (var i=0, n=values.length; i<n; i++) {
//      for (var j=0,m=values[i].length; j<m; j++) {        
//        var value = values[i][j];
		var value = values[i];
        max = Math.max (max, value);
        min = Math.min (min, value);            
//      }
    }
    var ceil = max;
    var floor = min;
    if (mSymmetricZ) {
      ceil = Math.max(Math.abs(min),Math.abs(max));
      floor = -ceil;
    }
    self.setScale(floor, ceil);
  }
 
   /**
   * Computes the extrema and sets the scales for the ColorMapper
   * @method setAutoscale2
   * @param values double[][]
   */
  self.setAutoscaleArray2 = function(values) {
    var min = Number.MAX_VALUE;
    var max = Number.MIN_VALUE;
    for (var i=0, n=values.length; i<n; i++) {
      for (var j=0,m=values[i].length; j<m; j++) {        
        var value = values[i][j];
        max = Math.max (max, value);
        min = Math.min (min, value);            
      }
    }
    var ceil = max;
    var floor = min;
    if (mSymmetricZ) {
      ceil = Math.max(Math.abs(min),Math.abs(max));
      floor = -ceil;
    }
    self.setScale(floor, ceil);
  }
  
     /**
   * Computes the extrema and sets the scales for the ColorMapper
   * @method setAutoscale3
   * @param values double[][][]
   */
  self.setAutoscaleArray3 = function(values) {
    var min = Number.MAX_VALUE;
    var max = Number.MIN_VALUE;
    for (var i=0, n=values.length; i<n; i++) {
      for (var j=0,m=values[i].length; j<m; j++) {        
        for (var k=0,p=values[i][j].length; k<p; k++) {        
          var value = values[i][j][k];
          max = Math.max (max, value);
          min = Math.min (min, value);
        }            
      }
    }
    var ceil = max;
    var floor = min;
    if (mSymmetricZ) {
      ceil = Math.max(Math.abs(min),Math.abs(max));
      floor = -ceil;
    }
    self.setScale(floor, ceil);
  }
  
  return self;	
}