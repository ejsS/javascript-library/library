/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Style object for 2D drawing
 * @class Style 
 * @constructor  
 */
EJSS_DRAWING2D.Style = {  
  RENDER_AUTO : "auto",
  RENDER_OPTSPEED: "optimizeSpeed",
  RENDER_CRISPEDGES: "crispEdges",
  RENDER_GEOPRECISION: "geometricPrecision", 
  
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * Copies one element into another
   */
  copyTo : function(source, dest) {
    dest.setDrawLines(source.getDrawLines());
    dest.setLineColor(source.getLineColor());
    dest.setLineWidth(source.getLineWidth());
    dest.setDrawFill(source.getDrawFill());
    dest.setFillColor(source.getFillColor());
    dest.setAttributes(source.getAttributes());
    dest.setShapeRendering(source.getShapeRendering());
  }
  
};

/**
 * Creates a Style object for 2D drawing
 */
EJSS_DRAWING2D.style = function (mName) {
  var Style = EJSS_DRAWING2D.Style;
  var self = {};
   
  var mDrawLines = true;
  var mLineColor = 'black';
  var mLineWidth = 0.5;
  var mDrawFill = true;
  var mFillColor = 'none';
  var mShapeRendering = Style.RENDER_AUTO;
  var mChangeListener; 
  var mAttributes = {};     

  /**
   * Set a listener that will be called whenever there are style changes.
   * I.e. a call to listener("change"); will be issued
   */
  self.setChangeListener = function(listener) {
    mChangeListener = listener;
  };
  
  //---------------------------------
  // lines
  //---------------------------------

  /**
   * Whether to draw the lines of the element
   */
  self.setDrawLines = function(draw) {
    if (draw!=mDrawLines) {
      mDrawLines = draw;
      if (mChangeListener) mChangeListener("drawlines");
    }
  };
  
  /**
   * Get the draw lines flag
   */
  self.getDrawLines = function() { 
    return mDrawLines; 
  };
  
  /**
   * Set the line color of the element
   * @param color a stroke style
   */
  self.setLineColor = function(color) { 
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color!=mLineColor) {
      mLineColor = color; 
      if (mChangeListener) mChangeListener("linecolor");
    }
    return self;
  };
    
  /**
   * Get the line color
   */
  self.getLineColor = function() { 
    return mLineColor; 
  };

  /**
   * Set the line width of the element
   * @param width a stroke width (may be double, such as 0.5, the default)
   */
  self.setLineWidth = function(width) { 
    if (width!=mLineWidth) {
      mLineWidth = width; 
      if (mChangeListener) mChangeListener("linewidth");
    }
  };

  /**
   * Get the line width
   */
  self.getLineWidth = function() { return mLineWidth; };
  
  //---------------------------------
  // interior fill
  //---------------------------------

  /**
   * Whether to fill the interior of the element
   */
  self.setDrawFill = function(draw) {
    if (draw!=mDrawFill) {
      mDrawFill = draw;
      if (mChangeListener) mChangeListener("drawfill");
    }
  };
  
  /**
   * Get the draw fill flag
   */
  self.getDrawFill = function() { 
    return mDrawFill; 
  };

  /**
   * Set the fill color of the element
   * @param color a fill style
   */
  self.setFillColor = function(color) {
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color!=mFillColor) {
      mFillColor = color; 
      if (mChangeListener) mChangeListener("fillcolor");
    }
  };
  
  /**
   * Get the fill color
   */
  self.getFillColor = function() { 
    return mFillColor; 
  };

  /**
   * Sets shape rendering
   */
  self.setShapeRendering = function(rendering) {
    if (rendering.substring(0,6) == "RENDER") rendering = Style[rendering.toUpperCase()];
    if (mShapeRendering != rendering) {
      mShapeRendering = rendering;
      if (mChangeListener) mChangeListener("shaperendering");
    }
  };
  
  self.getShapeRendering = function() { 
    return mShapeRendering;
  };

  /**
   * Sets optional attributes used by the element (such as SVG attributes)
   */
  self.setAttributes = function(attr) {
    if (attr != mAttributes) {
      mAttributes = attr;
      if (mChangeListener) mChangeListener("attributes");
    }
  };
  
  self.getAttributes = function() { 
    return mAttributes;
  };
  
  /***
   * Get JSON object with private variables
   * @method serialize
   * @visibility private
   */
  self.serialize = function() {
  	return { 
		mDrawLines: mDrawLines, mLineColor: mLineColor, mLineWidth: mLineWidth,
		mDrawFill: mDrawFill, mFillColor: mFillColor, mShapeRendering: mShapeRendering,
		mAttributes: mAttributes
  	};
  }
  
  /***
   * Set JSON object with private variables
   * @method unserialize
   * @parem json JSON object
   * @visibility private
   */
  self.unserialize = function(json) {
	mDrawLines = json.mDrawLines, mLineColor = json.mLineColor, mLineWidth = json.mLineWidth,
	mDrawFill = json.mDrawFill, mFillColor = json.mFillColor, mShapeRendering = json.mShapeRendering,
	mAttributes = json.mAttributes;
  }  
  
  //---------------------------------
  // final initialization
  //---------------------------------
  
  return self;
};

