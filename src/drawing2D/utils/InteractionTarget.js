/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia and Félix J. García
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for 2D drawing.
 * @module 2Ddrawing
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * InteractionTarget class
 * @class InteractionTarget
 * @constructor
 */
EJSS_DRAWING2D.InteractionTarget = {
	ENABLED_FIXED: 0,
	ENABLED_NONE : 0,
	ENABLED_ANY : 1,
	ENABLED_X : 2,
	ENABLED_Y : 3,
	ENABLED_NO_MOVE : 4,

    SENSITIVITY_BOTH : 0,
    SENSITIVITY_HORIZONTAL : 1,
    SENSITIVITY_VERTICAL : 2,
    SENSITIVITY_ANY : 3,
    SENSITIVITY_RADIUS : 4,
    
    
	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

	/**
	 * Copies one element into another
	 */
	copyTo : function(source, dest) {
		dest.setMotionEnabled(source.getMotionEnabled());
		dest.setAffectsGroup(source.getAffectsGroup());
		dest.setActionCommand(source.getActionCommand());
		dest.setDataObject(source.getDataObject());
		dest.setSensitivity(source.getSensitivity());
	}
};

/**
 * Creates an interaction object for 2D interaction
 */
EJSS_DRAWING2D.interactionTarget = function(mElement, mType, mPosition) {
	var InteractionTarget = EJSS_DRAWING2D.InteractionTarget;
	var self = {};

	var Element = EJSS_DRAWING2D.Element;	// reference for Element

	var mMotionEnabled = InteractionTarget.ENABLED_NONE;
	var mAffectsGroup = false;
	var mCommand = null;
	var mDataObject = null;
	var mSensitivity = 20;		// reference to interact, distance to element center
	var mSensitivityType = InteractionTarget.SENSITIVITY_BOTH;

	/**
	 * Returns the owner element
	 */
	self.getElement = function() {
		return mElement;
	};

	/**
	 * Returns the type of target
	 */
	self.getType = function() {
		return mType;
	};

	/**
	 * Sets the motion capability of the target
	 * @param value One of:
	 * <ul>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.ENABLED_NONE: the target is not responsive</li>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.ENABLED_ANY: any motion (x,y) is allowed</li>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.ENABLED_X: the target only responds to motion in the X direction</li>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.ENABLED_Y: the target only responds to motion in the Y direction</li>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.ENABLED_NO_MOVE: the target fires events but cannot be moved</li>
	 * </ul>
	 * If not specified, then ENABLED_ANY is assumed.
	 */
	self.setMotionEnabled = function(motion) {
		//var result = InteractionTarget.ENABLED_ANY;
		if ( typeof motion == "string") {
			value = InteractionTarget[motion.toUpperCase()];
			mMotionEnabled = (typeof value === 'undefined')?InteractionTarget.ENABLED_NONE:value;
		}
	    else mMotionEnabled = motion;
	};

	/**
	 * Returns the (perhaps partial) interaction capability of the target
	 */
	self.getMotionEnabled = function() {
		return mMotionEnabled;
	};
	
	/**
	 * Sets the sensitivity type
	 * @param value One of:
	 * <ul>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.SENSITIVITY_ANY: the target is selected if (x,y) is close to its position (default)</li>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.SENSITIVITY_HORIZONTAL: the target is selected if x is close to its horizontal position</li>
	 *   <li> EJSS_DRAWING2D.InteractionTarget.SENSITIVITY_VERTICAL: the target is selected if y is close to its vertical position</li>
	 * </ul>
	 */
	self.setSensitivityType = function(sensitivityType) {
		if (typeof sensitivityType == "string")
			mSensitivityType = InteractionTarget[sensitivityType.toUpperCase()];
	    else mSensitivityType = sensitivityType;
	};

	/**
	 * Returns the (perhaps partial) interaction capability of the target
	 */
	self.getSensitivityType = function() {
		return mSensitivityType;
	};

	/**
	 * Returns the (perhaps partial) interaction capability of the target
	 */
	self.isEnabled = function() {
		return mMotionEnabled != InteractionTarget.ENABLED_NONE;
	};
	
	self.setPositionOffset = function(position) { 
		mPosition = position;
	}

	self.getPositionOffset = function() { 
		return mPosition;
	}

	/**
	 * Gets pixel position for interaction target
	 */
	self.getPixelPosition = function() {
		// get position of the element center
		var pos = mElement.getPixelPosition(true);
		var size = mElement.getPixelSizes(true);
		var offset = mElement.getRelativePositionOffset(size);
		var x = pos[0] + offset[0];
		var y = pos[1] + offset[1];

		// get target relative position
		var inverted = mElement.getPanel().getInvertedScaleY();
		var d = Element.getRelativePositionOffset(mPosition, size[0], size[1], inverted);

		return [x - d[0], y - d[1]];
	};
	
	/**
	 * Gets position for interaction target
	 */
	self.getPosition = function() {
		// get position of the element center
		var pos = mElement.getAbsolutePosition(true);
		var size = mElement.getAbsoluteSize();
		// get target relative position
		var inverted = mElement.getPanel().getInvertedScaleY();
		var d = Element.getRelativePositionOffset(mPosition, size[0], size[1], inverted);
		var offset = mElement.getRelativePositionOffset(size);
		var x = pos[0] + offset[0] - d[0];
		var y = pos[1] + offset[1] - d[1];
		return [x,y];
	};
	
	/**
	 * Whether the interaction with the target affects the top-level group
	 * of the element that contains it (instead of only affecting the element).
	 * Default is false.
	 * @param value boolean
	 */
	self.setAffectsGroup = function(value) {
		mAffectsGroup = value;
	};

	/**
	 * Whether the target affects the top-level group
	 * @return boolean
	 */
	self.getAffectsGroup = function() {
		return mAffectsGroup;
	};

	/**
	 * Sets the action command for this target
	 * @param command String
	 */
	self.setSensitivity = function(sense) {
		mSensitivity = sense;
	};

	/**
	 * Returns the action command of this target
	 * @return String
	 */
	self.getSensitivity = function() {
		return mSensitivity;
	};

	/**
	 * Sets the action command for this target
	 * @param command String
	 */
	self.setActionCommand = function(command) {
		mCommand = command;
	};

	/**
	 * Returns the action command of this target
	 * @return String
	 */
	self.getActionCommand = function() {
		return mCommand;
	};

	/**
	 * A place holder for data objects
	 * @param _object Object
	 */
	self.setDataObject = function(object) {
		mDataObject = object;
	};

	/**
	 * Returns the data object
	 * @return Object
	 */
	self.getDataObject = function() {
		return mDataObject;
	};

	return self;
};

