/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Image
 * @class Image 
 * @constructor  
 */
EJSS_DRAWING2D.Image = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy
  	
		dest.setImageUrl(source.getImageUrl());
  	},


    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

      controller.registerProperty("ImageUrl",element.setImageUrl);
      controller.registerProperty("Encode",element.setEncode);
    },
};

/**
 * Creates a 2D Segment
 * @method image
 */
EJSS_DRAWING2D.image = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  var mUrl = "";		// image url
  var mCode = "";
  var mChangedImage = false;	// indica si la fuente de la imagen ha cambiado 

  self.getClass = function() {
  	return "ElementImage";
  };

  self.setImageUrl = function(url) {
    var set = self.getSet(); 
  	var resPathFunction = (set!=null) ? set.getResourcePath : self.getResourcePath; 
  	if (resPathFunction!=null) {
  	  url = resPathFunction(url);
  	  //alert ("ImageUrl set to = "+url+"\n");
  	}
  	else console.log ("No getResourcePath function for "+self.getName()+". Texture = "+url);
  	if(mUrl != url) {
  		mUrl = url;
  		self.setChanged(true);
  		self.setChangedImage(true);
  	}
  }	;

  self.getImageUrl = function() {
  	return mUrl;
  };

  self.forceImageUrl = function(url) {
    console.log ("Setting image to " +url);
    mUrl = window.URL.createObjectURL(url);
  	self.setChanged(true);
  	self.setChangedImage(true);
  };
  
  self.setChangedImage = function(changed) {
  	mChangedImage = changed;
  };

  self.getChangedImage = function() {
  	return mChangedImage;
  };
  
  self.setEncode = function(code) {
  	if(mCode.length != code.length) { // less cost
  		mCode = code;
  		self.setChanged(true);
  		self.setChangedImage(true);
  	}
  };

  self.getEncode = function() {
  	return mCode;
  };
  
  self.getImageData = function(callback) {
    var size = self.getPixelSizes();     

  	var img = new Image();
  	img.src = mUrl;
  	
  	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');
	canvas.width = Math.abs(size[0])+1;
	canvas.height = Math.abs(size[1])+1;
	
	if (callback) {
	  	img.onload = function() {
			context.drawImage(img,  
			  		0, 0, img.width, img.height, 
			        0, 0, canvas.width, canvas.height);
		  	callback(context.getImageData(0, 0, canvas.width, canvas.height));
		}
  	} else {
		context.drawImage(img,  
		  		0, 0, img.width, img.height, 
		        0, 0, canvas.width, canvas.height);
		return context.getImageData(0, 0, canvas.width, canvas.height);  		
  	}
  }

  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Image.registerProperties(self,controller);
  };
  
  self.copyTo = function(element) {
    EJSS_DRAWING2D.Image.copyTo(self,element);
  };
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};



