/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * SetSquare
 * @class SetSquare 
 * @constructor  
 */
EJSS_DRAWING2D.SetSquare = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Ruler.registerProperties(element,controller); // super class

      controller.registerProperty("Height",element.setHeight);
    }

};

/**
 * Creates a 2D shape
 * @method shape
 */
EJSS_DRAWING2D.setSquare = function (mName) {
  var self = EJSS_DRAWING2D.ruler(mName);
  
  var mHeight = self.getLength();

  // Implementation variables
  // ----------------------------------------------------
  // public functions
  // ----------------------------------------------------

  // ----------------------------------------------------
  // Properties and copies
  // ----------------------------------------------------

  self.setHeight = function(value) { 
	  if (value!=mHeight) {
		  mHeight = value;
		  self.mustAdjust();
      }
  }
  self.getHeight = function() { return mHeight; }

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.SetSquare.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // private functions
  // ----------------------------------------------------

  self.createBody = function() {
	var body = EJSS_DRAWING2D.polygon(mName+".body");
	return body;
  }

  self.adjustBody = function() {
	    var w = self.getWidth(), l = self.getLength(), h = self.getHeight();
	    var hyp = Math.sqrt(l*l+h*h);
	    var l2 = (h-w)*l/h - w - w*hyp/h; // interior length
	    var h2 = (h-w) - w*h/l - w*hyp/l; // interior height
	    
	    var points = [[0,0,1],[l,0,1],[0,-h,1],[0,0,1],
	                 [w,-w,0],[w,-w-h2,1],[w+l2,-w,1],[w,-w,1]];
	    for (var i=0; i<points.length; i++) {
	    	points[i][0] -= l/2;
	    	points[i][1] += w/2;
	    };
		self.getElement('body').setPoints(points);
		
		self.getElement('left_handle').setPosition([w+l2/4-l/2,0]);
		self.getElement('right_handle').setPosition([w+3*l2/4-l/2,0]);
		self.getElement('left_rotate').setPosition([w/2-l/2,0]);
		self.getElement('right_rotate').setPosition([w+l2+w/2-l/2,0]);	  
  };
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
    self.setMaximum(0.99);
	self.setNumberOfMarks(100);

  return self;
};


