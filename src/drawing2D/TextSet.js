/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//TextSet
//---------------------------------

/**
 * TextSet
 * @class TextSet 
 * @constructor  
 */
EJSS_DRAWING2D.TextSet = {
	
    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING2D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      
      controller.registerProperty("Text", 
          function(v) { set.setToEach(function(element,value) { element.setText(value); }, v); }
      );
      controller.registerProperty("Framed", 
          function(v) { set.setToEach(function(element,value) { element.setFramed(value); }, v); }
      );           
      controller.registerProperty("WritingMode", 
          function(v) { set.setToEach(function(element,value) { element.setWritingMode(value); }, v); }
      );           
      controller.registerProperty("Font", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFont(value); }, v); }
      );          
      controller.registerProperty("FontFamily", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFontFamily(value); }, v); }
      );           
      controller.registerProperty("FontSize", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFontSize(value); }, v); }
      );           
      controller.registerProperty("LetterSpacing", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setLetterSpacing(value); }, v); }
      );           
      controller.registerProperty("OutlineColor", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setOutlineColor(value); }, v); }
      );           
      controller.registerProperty("FontWeight", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFontWeight(value); }, v); }
      );           
      controller.registerProperty("FillColor", 
          function(v) { set.setToEach(function(element,value) { element.getFont().setFillColor(value); }, v); }
      );           
    }         
    
};


/**
 * Creates a set of Texts
 * @method textSet
 * @param mName
 */
EJSS_DRAWING2D.textSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.text, mName);

  // Static references
  var TextSet = EJSS_DRAWING2D.TextSet;		// reference for TextSet

  /**
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    TextSet.registerProperties(self,controller);
  };

  return self;
};

