/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Spring
 * @class Spring 
 * @constructor  
 */
EJSS_DRAWING2D.Spring = {
  DEF_RADIUS : 0.05,
  DEF_LOOPS : 8,
  DEF_PPL : 15,

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy
  	
		dest.setRadius(source.getRadius());
		dest.setSolenoid(source.getSolenoid());
		dest.setThinExtremes(source.getThinExtremes());
		dest.setLoops(source.getLoops());
		dest.setPointsPerLoop(source.getPointsPerLoop());
  	},

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Radius", element.setRadius, element.getRadius);
		controller.registerProperty("Solenoid", element.setSolenoid, element.getSolenoid);
		controller.registerProperty("ThinExtremes", element.setThinExtremes, element.getThinExtremes);
		controller.registerProperty("Loops", element.setLoops, element.getLoops);
		controller.registerProperty("PointsPerLoop", element.setPointsPerLoop, element.getPointsPerLoop);
	},
};

/**
 * Creates a 2D Spring
 * @method spring
 */
EJSS_DRAWING2D.spring = function(name) {
	var self = EJSS_DRAWING2D.element(name);
  	
	  // Configuration variables
	var mRadius = EJSS_DRAWING2D.Spring.DEF_RADIUS;
	var mSolenoid = 0.0;
	var mThinExtremes = true;
	var mLoops;
	var mPointsPerLoop;

	self.getClass = function() {
		return "ElementSpring";
	}

	/***
	 * Set radius
	 * @method setRadius
 	 * @param radius
	 */
	self.setRadius = function(radius) {
		if(mRadius != radius) {
			mRadius = radius;
			self.setChanged(true);
		}		
	}

	self.getRadius = function() {
		return mRadius;
	}

	self.setSolenoid = function(solenoid) {
		if(mSolenoid != solenoid) {
			mSolenoid = solenoid;
			self.setChanged(true);
		}
	}

	self.getSolenoid = function() {
		return mSolenoid;
	}

	self.setThinExtremes = function(thinExtremes) {
		if(mThinExtremes != thinExtremes) {
			mThinExtremes = thinExtremes;
			self.setChanged(true);
		}
	}

	self.getThinExtremes = function() {
		return mThinExtremes;
	}

	self.setLoops = function(loops) {
		if(mLoops != loops) {
			mLoops = loops;
			self.setChanged(true);
		}
	}

	self.getLoops = function() {
		return mLoops;
	}

	self.setPointsPerLoop = function(pointsPerLoop) {
		if(mPointsPerLoop != pointsPerLoop) {
			mPointsPerLoop = pointsPerLoop;
			self.setChanged(true);
		}
	}

	self.getPointsPerLoop = function() {
		return mPointsPerLoop;
	}

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.Spring.registerProperties(self, controller);
	};

  	self.copyTo = function(element) {
    	EJSS_DRAWING2D.Spring.copyTo(self,element);
  	};

	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

  	self.setSize([0.1,0.1]);
  	self.setLoops(EJSS_DRAWING2D.Spring.DEF_LOOPS);
  	self.setPointsPerLoop(EJSS_DRAWING2D.Spring.DEF_PPL);
	self.setRelativePosition("SOUTH_WEST");
	
	return self;
};

