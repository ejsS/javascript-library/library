/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A Trail is a two-dimensional drawable that displays a collection of points in the plane. 
 * The points are added to the trail either through the <tt>InputX</tt> and <tt>InputY</tt>
 * properties, or using the element’s <tt>addPoint</tt> or <tt>moveToPoint</tt> functions. 
 * The points are displayed using lines which connect the points, each with the previous one, 
 * thus forming a polygonal line (which looks like a trail). The trail can be disconnected 
 * at some points if the <tt>Connected</tt> property is set temporarily to false, or if 
 * the <tt>moveToPoint</tt> function is used.<br/> 
 * The number of points in a trail can be limited through the <tt>Maximum</tt> property. 
 * Adding points past the limit causes the trail to remove the first points in it.<br/>
 *<br/>
 * Trails can be broken into several segments using the <tt>newSegment</tt> function. 
 * Each segment behaves like a sub-trail, but segments can have different drawing 
 * styles (line width and color). Also, the user can delete the points in the last 
 * segment without affecting the previous segments.
 * @class EJSS_DRAWING2D.Trail 
 * @parent EJSS_DRAWING2D.Element
 * @constructor  
 */
EJSS_DRAWING2D.Trail = {
  NO_CONNECTION : 0, 	// The next point will not be connected to the previous one
  LINE_CONNECTION : 1,	// The next point will be connected to the previous one by a segment

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy  	
  		
		dest.setActive(source.getActive());
		dest.setNoRepeat(source.getNoRepeat());
		dest.setClearAtInput(source.getClearAtInput());
		dest.setSkip(source.getSkip());
		dest.setInputLabels(source.getInputLabels());
		dest.setMaximumPoints(source.getMaximumPoints());
		dest.setConnectionType(source.getConnectionType());
		dest.setPoints(source.getPoints());
  	},
  	
	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

	 /*** 
	  * Whether the trail should accept input points. 
	  * @property Active
	  * @type boolean
	  * @default true 
	  */  
		controller.registerProperty("Active", element.setActive, element.getActive);

	 /*** 
	  * Whether the trail ignores repeated input points 
	  * @property NoRepeat
	  * @type boolean
	  * @default false 
	  */  
		controller.registerProperty("NoRepeat", element.setNoRepeat, element.getNoRepeat);

	 /*** 
	  * Whether the trail clears all points when receiving new input. (Useful if input is an array.)
	  * @property ClearAtInput
	  * @type boolean
	  * @default false 
	  */  
		controller.registerProperty("ClearAtInput", element.setClearAtInput, element.getClearAtInput);

	 /*** 
	  * Whether the trail should display only one oout of many inpout points. 
	  * If Skip is positive, it indicates how many points the trail will take to display one.
	  * @property Skip
	  * @type int
	  * @default 0 
	  */  
		controller.registerProperty("Skip", element.setSkip, element.getSkip);

		controller.registerProperty("ColumnNames", element.setInputLabels, element.getInputLabels);

	 /*** 
	  * The maximum number of points the trail accepts. If more input is received, the trail
	  * will discard the corresponding first points.
	  * @property Maximum
	  * @type int
	  * @default 0 indicating no limit
	  */  
		controller.registerProperty("Maximum", element.setMaximumPoints, element.getMaximumPoints);

		controller.registerProperty("ConnectionType", element.setConnectionType, element.getConnectionType);		

	 /*** 
	  * Whether the next input point should be connected to the previous one. 
	  * @property Connected
	  * @type boolean
	  * @default true 
	  */  		
	  controller.registerProperty("Connected", element.setConnected, element.isConnected);		
		
		controller.registerProperty("Points", element.setPoints, element.getPoints);		
		controller.registerProperty("LastPoint", element.addPoint, element.getLastPoint);

	 /*** 
	  * The next input point for the trail. 
	  * @property Input
	  * @type int[2]|double[2] An array with a pair of [x,y] coordinates, 
	  * or int[n][2]|double[n][2] a double array with n pairs of [x,y] coordinates. 
	  * @default "none" 
	  */ 
	  controller.registerProperty("Input", element.addPoints);

	 /*** 
	  * The X coordinate for the next input point for the trail. 
	  * @property InputX
	  * @type int|double The X coordinate for the input point 
	  * or int[n]|double[n] a double array with the X coordinates for n input points. 
	  * @default "none" 
	  */ 
	  controller.registerProperty("InputX", element.addXPoints);				

	 /*** 
	  * The Y coordinate for the next input point for the trail. 
	  * @property InputY
	  * @type int|double The Y coordinate for the input point 
	  * or int[n]|double[n] a double array with the Y coordinates for n input points. 
	  * @default "none" 
	  */ 
	  controller.registerProperty("InputY", element.addYPoints);
	}
			
};

/**
 * Creates a 2D Trail
 * Trail implements dataCollected(), which makes it a Collector of data 
 * @see _view._collectData
 * @method trail
 */
EJSS_DRAWING2D.trail = function(name) {
	var self = EJSS_DRAWING2D.element(name);
 
    // Configuration variables
	var mActive = true;
	var mNoRepeat = false;
	var mClearAtInput = false;
	var mSkip = 0;
	var mInputLabels = ["x", "y"];	  
	var mMaximum = 0;
	var mConnectionType = EJSS_DRAWING2D.Trail.LINE_CONNECTION;
	
  	// Implementation variables
  	var mCounter = 0;
  	var mIsEmpty = true;
	//  private int firstPoint = 0; // the first point of the current path 
  	var mLastPoint = 0;	
  	var mFlushPoint = 0; // This is the last point which was not added because of the skip parameter  	
  	var mCurrentList = []; 	// The current list of points  	
  	var mSegmentList = []; // The list of past Segments, if any  	
  	var mSegmentStyle = []; // The list of past segment styles, if any
	var mTmpList = []; // The temporal list of new points
	var mTmpLastXAdded = 0;
	var mTmpLastYAdded = 0;
	 
	self.getClass = function() {
		return "ElementTrail";
	}

  /***
   * Sets the active state of the trail. An inactive trail ignores all input. 
   * @method setActive(active)
   * @param active boolean
   * @visibility public
   */
  self.setActive = function(active) {
    if(mActive != active) {
    	mActive = active;
    	self.setChanged(true);
    }
  }
  
  /***
   * Whether the trail is in active mode.
   * @method getActive()
   * @return boolean
   */
  self.getActive = function() { 
  	return mActive; 
  }
  
  /***
   * Sets the no repeat state of the trail.
   * When set, a trail will ignore (x,y) points which equal the last added point.
   * @method setNoRepeat(noRepeat)
   * @param noRepeat boolean
   * @visibility public
   */
  self.setNoRepeat = function (noRepeat) {
    if(mNoRepeat != noRepeat) {
    	mNoRepeat = noRepeat;
    	self.setChanged(true);
    }
  }
  
  /***
   * Whether the trail is in no repeat mode.
   * @method getNoRepeat()
   * @return boolean
   */
  self.getNoRepeat = function() { 
  	return mNoRepeat; 
  }

  /***
   * Sets the trail to clear existing points when receiving 
   * a new point or array of points.
   * @method setClearAtInput(clear)
   * @param clear boolean
   * @visibility public
   */
  self.setClearAtInput = function(clear) {
    if(mClearAtInput != clear) {
    	mClearAtInput = clear;
    	self.setChanged(true);
    }
  }
  
  /***
   * Whether the trail is in clear at input mode.
   * @method getClearAtInput()
   * @return boolean
   * @visibility public
   */
  self.getClearAtInput = function() { 
  	return mClearAtInput; 
  }

  /***
   * Sets the skip parameter. When the skip parameter is larger than zero,
   * the trail only considers one of every 'skip' points. That is, if skip is 3, 
   * the trail will consider only every third point sent to it. 
   * The default is zero, meaning all points must be considered.
   * @method setSkip(skip)
   * @param skip int
   * @visibility public
   */
  self.setSkip = function (skip) {
    if (mSkip != skip) {
      mSkip = skip;
      mCounter = 0;
      self.setChanged(true);
    }
  }
  
  /***
   * Returns the skip parameter of the trail.
   * @method getSkip()
   * @return int
   * @visibility public
   */
  self.getSkip = function () { 
  	return mSkip; 
  }

  /**
   * Sets the labels of the X,Y coordinates when the data is displayed in a table
   * @method setInputLabels(label)
   * @param label
   */
  self.setInputLabels = function (label) {
  	if((mInputLabels[0] != label[0]) || (mInputLabels[1] != label[1])) { 
  		mInputLabels[0] = label[0]; 
  		mInputLabels[1] = label[1]; 
  		self.setChanged(true);
  	}
  }

  /**
   * Returns the labels of the X,Y coordinates
   * @method getInputLabels()
   * @return labels
   */
  self.getInputLabels = function () { 
  	return mInputLabels; 
  }
  
  /***
   * Sets the maximum number of points for the trail.
   * Once the maximum is reached, adding a new point will cause
   * remotion of the first one. This is useful to keep trails
   * down to a reasonable size, since very long trails can slow
   * down the rendering (in certain implementations).
   * If the value is 0 (the default) the trail grows forever
   * without discarding old points.
   * @method setMaximumPoints(maximum)
   * @param maximum int
   * @visibility public
   */
  self.setMaximumPoints = function(maximum) {
  	var max = Math.max(maximum, 2);
  	if(mMaximum != max) {
    	mMaximum = max;
    	self.setChanged(true);
    }
  }

  /***
   * Returns the maximum number of points allowed for the trail
   * @method getMaximumPoints()
   * @return int
   * @visibility public
   */
  self.getMaximumPoints = function() {
    return mMaximum;
  }

  /***
   * Whether to connect next input point with the previous one
   * @method setConnected(connected)
   * @param connected boolean
   * @visibility public
   */
  self.setConnected = function (connected) {
   if (connected) self.setConnectionType(EJSS_DRAWING2D.Trail.LINE_CONNECTION);
   else self.setConnectionType(EJSS_DRAWING2D.Trail.NO_CONNECTION);
  }

  /***
   * Gets the connection state.
   * @method getConnected()
   * @return boolean
   * @visibility public
   */
  self.isConnected = function() {
    return mConnectionType===EJSS_DRAWING2D.Trail.LINE_CONNECTION;
  }

  /**
   * Sets the type of connection for the next point.
   * @method setConnectionType(type)
   * @param type int
   */
  self.setConnectionType = function (type) {
    if (typeof type == "string") type = EJSS_DRAWING2D.Trail[type.toUpperCase()];      	
    if(mConnectionType != type) {
    	mConnectionType = type;
    	self.setChanged(true);
    }
  }

  /**
   * Gets the connection type.
   * @method getConnectionType()
   * @see #setConnectionType(int)
   */
  self.getConnectionType = function() {
    return mConnectionType;
  }

  /***
   * Adds a new point to the trail.
   * @method addPoint(x,y,style)
   * @param x double The X coordinate of the point 
   * 		or point double[] The double[2] array with the coordinates of the point.
   * @param y double The Y coordinate of the point.
   * @param style int an optional connection style: 0 = EJSS_DRAWING2D.Trail.NO_CONNECTION, 1 = EJSS_DRAWING2D.Trail.LINE_CONNECTION
   * @visibility public
   */
  self.addPoint = function (x, y, style) {
    if (!mActive) return;
  	if (mClearAtInput) self.initialize();
    if(x instanceof Array)    	
    	addPoint(x[0], x[1], x[2]);
    else 
    	addPoint (x,y,style);
    self.setChanged(true);
  }


  /***
   * Adds an array of points to the trail.
   * @method addPoints(x,y)
   * @param x double The double[] array with the X coordinates of the points.
   * 	or  point double[][] The double[nPoints][2] array with the coordinates of the points.
   * @param y double The double[] array with the Y coordinates of the points.
   * @visibility public
   * @param len int The number of points to copy.
   * @visibility public
   */
  self.addPoints  = function(x, y, len) {
    if (!mActive) return;
  	if (mClearAtInput) self.initialize();
  	// number of points to copy
  	if (typeof len == "undefined") len = Number.MAX_SAFE_INTEGER;
	if ((typeof y == "undefined") || (y === null)) {
      if(x[0] instanceof Array) {    
    	for (var i=0,n=Math.min(len,x.length); i<n; i++) addPoint (x[i][0],x[i][1],x[i][2]);  		
  	  }
  	  else addPoint (x[0],x[1]);
	}
	else { // y is defined
    	for (var i=0,n=Math.min(len,x.length,y.length); i<n; i++) addPoint (x[i],y[i]);
  	}
    self.setChanged(true);
  }

  /**
   * Sets the array of X points for the input of the trail.
   * @method addXPoints(x)
   */
  self.addXPoints  = function(x) {
    if (!mActive) return;
  	var i = 0;
  	if(x instanceof Array) {
  		// fill points with the values  		
    	for (var n=x.length; i<n; i++) {
    		if(mTmpList[i]) mTmpList[i] = [x[i],mTmpList[i][1],mTmpList[i][2]];
    		else mTmpList[i] = [x[i],mTmpLastYAdded,mConnectionType];
    	}  		
    	mTmpLastXAdded = x[i];
  	}
    else { // only one value
    	if(mTmpList[i]) mTmpList[i] = [x,mTmpList[i][1],mTmpList[i][2]];
    	else mTmpList[i] = [x,mTmpLastYAdded,mConnectionType];
    	i++;
    	mTmpLastXAdded = x;    	
    }

	// fill points with the last value
	while (mTmpList[i]) {
		mTmpList[i] = [mTmpLastXAdded,mTmpList[i][1],mTmpList[i][2]];
		i++;
	}    	
  }

  /**
   * Adds an array of Y points to the trail.
   * @method addYPoints(y)
   */
  self.addYPoints  = function(y) {
    if (!mActive) return;
  	var i = 0;
  	if(y instanceof Array) {
  		// fill points with the values  		
    	for (var n=y.length; i<n; i++) {
    		if(mTmpList[i]) mTmpList[i] = [mTmpList[i][0],y[i],mTmpList[i][2]];
    		else mTmpList[i] = [mTmpLastXAdded,y[i],mConnectionType];
    	}  		
    	mTmpLastYAdded = y[i];
  	}
    else { // only one value
    	if(mTmpList[i]) mTmpList[i] = [mTmpList[i][0],y,mTmpList[i][2]];
    	else mTmpList[i] = [mTmpLastXAdded,y,mConnectionType];
    	i++;
    	mTmpLastYAdded = y;    	
    }

	// fill points with the last value
	while (mTmpList[i]) {
		mTmpList[i] = [mTmpList[i][0],mTmpLastYAdded,mTmpList[i][2]];
		i++;
	}    	
  }

  /**
   * Adds the temporal array of point to the trail.
   * @method dataCollected()
   */
  self.dataCollected  = function() {
  	if (mTmpList.length > 0) {
  	  self.addPoints(mTmpList);
      mTmpList = [];
  	  self.setChanged(true);
  	}
  }  

  /**
   * Sets an array of points to the trail. Equivalent to clear() + addPoints(x,y)
   * @method setPoints(x,y)
   * @param xInput double The double[] array with the X coordinates of the points.
   * 	or  point double[][] The double[nPoints][2] array with the coordinates of the points.
   * @param yInput double The double[] array with the Y coordinates of the points.
   */
  self.setPoints  = function(x, y) {
    if (!mActive) return;
  	self.clear();
  	self.addPoints(x,y);
  	self.setChanged(true);
  }
    
  /***
   * Moves to the new point without drawing.
   * (Equivalent to setting the connection type
   * to NO_CONNECTION and adding one single point, then setting the 
   * type back to its previous value.)
   * @method moveToPoint(x,y)
   * @param x double The X coordinate of the point.
   * 	or point double[] The double[2] array with the coordinates of the point.
   * @param y double The Y coordinate of the point.
   * @visibility public
   */
  self.moveToPoint = function(x, y) { 
    if (!mActive) return;
  	if (mClearAtInput) self.initialize();
  	if(x instanceof Array) 
    	addPoint(x[0], x[1], EJSS_DRAWING2D.Trail.NO_CONNECTION);    
    else	
    	addPoint(x,y, EJSS_DRAWING2D.Trail.NO_CONNECTION);
    self.setChanged(true);
  }

  self.getSegmentsCount = function() {
    return mSegmentList.length; 
  }

  self.getSegmentPoints = function(index) {
    return mSegmentList[index]; 
  }

  self.getSegmentStyle = function(index) {
    return mSegmentStyle[index]; 
  }

  /**
   * Returns current points.
   * @method getCurrentPoints
   * @return points
   */
  self.getCurrentPoints = function () {
    return mCurrentList;
  }
  
  /**
   * Returns all points.
   * @method getPoints
   * @return points
   */
  self.getPoints = function () {
    var points = [];
    var size = mSegmentList.length;          	
	for(var i=0; i<size; i++ )
		points = points.concat(mSegmentList[i]);
	points = points.concat(mCurrentList);

    return points; 
  }

  
  /**
   * Gets last point.
   * @method getLastPoint
   * @return point
   */
  self.getLastPoint = function () {
    return mLastPoint;
  }

  /***
   * Same as clear
   * @method reset()
   * @visibility public
   */
  self.reset = function() {
    self.clear();
  }

  /***
   * Clears all points from all segments of the trail.
   * @method clear()
   * @visibility public
   */
  self.clear = function() {
    mSegmentList = [];
    mSegmentStyle = [];
    self.initialize();
  }

  /***
   * Clears all points from the last segment of the trail.
   * @method clearLastSegment()
   * @visibility public
   */
  self.clearLastSegment = function() {
	if (mCurrentList.length<=0) {
      mSegmentList.pop();
      mSegmentStyle.pop();
	  self.setChanged(true);
    }
	self.initialize();
  }
  
  /***
   * Clears all points from the last segment of the trail, 
   * respecting previous segments.
   * @method initialize()
   * @visibility public
   */
  self.initialize  = function() {
  	  mTmpList = [];
	  mCurrentList = [];	  
	  mFlushPoint = 0;
	  mIsEmpty = true;
	  mCounter = 0;
	  mLastPoint = 0;
	  self.setChanged(true);
  }

  /***
   * Creates a new segment of the trail.
   * @method newSegment()
   * @visibility public
   */
  self.newSegment = function() {
    if (mFlushPoint != 0) {
    	var size = mCurrentList.length;
    	mCurrentList[size] = self.formatPoint(mFlushPoint);
    }
    var size = mSegmentList.length;
    mSegmentList[size] = mCurrentList;
    var segmentStyle = EJSS_DRAWING2D.style(self.getName()+" #"+size);
    EJSS_DRAWING2D.Style.copyTo(self.getStyle(),segmentStyle);
    mSegmentStyle[size] = segmentStyle;

	self.initialize();
  }  
  
  /**
   * Returns bounds for an element
   * @override
   * @method getBounds
   * @return Object{left,rigth,top,bottom}
   */
  self.getBounds = function(element) {
  	var xmin, xmax, ymin, ymax;
  	var points = self.getPoints();
  	
  	// add mTmpList
  	//if (mTmpList.length > 0) points = points.concat(mTmpList);
  	
  	  	
    var len = points.length;
    if(len == 0) {
    	xmin = xmax = ymin = ymax = 0;
    }             
    else {
		xmax = xmin = points[0][0];
		ymax = ymin = points[0][1];    	
		for(var j=1; j<len; j++) {
			var x = points[j][0], y = points[j][1];
			if(x>xmax) xmax=x; if(y>ymax) ymax=y; if(x<xmin) xmin=x; if(y<ymin) ymin=y;
		}
	}    
    var x = self.getX(), y = self.getY();
    var sx = self.getSizeX(), sy = self.getSizeY();
  	var mx = sx/2, my = sy/2;  	
  	var d = self.getRelativePositionOffset(sx,sy);
  	
	return {
		left: ((x+d[0])-mx)+xmin*sx,
		right: ((x+d[0])-mx)+xmax*sx,
		top: ((y+d[1])-my)+ymax*sy,
		bottom: ((y+d[1])-my)+ymin*sy
	}
  };  
  
  function addPoint(_x, _y, _type) {
    //if (!mActive) return;
    if (isNaN(_x) || isNaN(_y)) { 
    	mIsEmpty = true; 
    	return; 
    }
    if (mNoRepeat && mLastPoint[0]==_x && mLastPoint[1]==_y) 
    	return;
	if ((typeof _type == "undefined") || (_type === null))
		_type = mConnectionType;

    mFlushPoint = 0;
    mLastPoint = self.formatPoint([_x,_y,_type]);
    
    if (mSkip > 0) { // Only if the counter is 0      
      if(mCounter > 0) mFlushPoint = self.formatPoint([_x, _y, _type]);      
      if(++mCounter >= mSkip) mCounter = 0;	        
    }
    
    if (mFlushPoint == 0) {
	    if (mMaximum > 2 && mCurrentList.length >= mMaximum) {
	    	mCurrentList.splice(0,1) // remove the firstPoint
	    }
	    
	    mCurrentList[mCurrentList.length] = self.formatPoint([_x, _y, _type]);
	    mIsEmpty = false;
	}
  }

	// override for subclases (for example Trace)
   self.formatPoint = function(src) {
   		return src;   		
   }

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.Trail.registerProperties(self, controller);
	};
	
  	self.copyTo = function(element) {
    	EJSS_DRAWING2D.Trail.copyTo(self,element);
  	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	self.setSize([1,1]);
  /*** @property RelativePosition @default "SOUTH_WEST" */  
	self.setRelativePosition("SOUTH_WEST");

	return self;
};

