/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia and Félix J. García 
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for 2D drawing.
 * @module 2Ddrawing
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A PlottingPanel is a 2D drawing panel with added decoration. The decoration includes axes, gutters and titles.
 * @class EJSS_DRAWING2D.PlottingPanel
 * @parent EJSS_DRAWING2D.DrawingPanel
 * @constructor
 */

EJSS_DRAWING2D.PlottingPanel = {

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.DrawingPanel.registerProperties(element, controller);
		// super class

		// title (text and font)
     /*** 
	  * The title at the top center of the panel
	  * @property Title
	  * @type String
	  * @default "Plot"
	  */ 
		controller.registerProperty("Title", element.getTitle().setText);
		controller.registerProperty("TitleFont", element.getTitle().getFont().setFont);
		controller.registerProperty("TitleColor", element.getTitle().getFont().setFillColor);
		controller.registerProperty("TitleMargin", element.getTitle().setMarginY);

		controller.registerProperty("TitleX", element.getTitleX().setText);
		controller.registerProperty("TitleXFont", element.getTitleX().getFont().setFont);
		controller.registerProperty("TitleXColor", element.getTitleX().getFont().setFillColor);
		controller.registerProperty("TitleXMargin", element.getTitleX().setMarginY);

		controller.registerProperty("TitleY", element.getTitleY().setText);
		controller.registerProperty("TitleYFont", element.getTitleY().getFont().setFont);
		controller.registerProperty("TitleYColor", element.getTitleY().getFont().setFillColor);
		controller.registerProperty("TitleYMargin", element.getTitleY().setMarginX);
 
		// Grid
		controller.registerProperty("GridXShow", element.getGrid().setShowX, element.getGrid().getShowX);
		controller.registerProperty("GridXTicks", element.getGrid().setTicksX, element.getGrid().getTicksX);
		controller.registerProperty("GridXStep", element.getGrid().setStepX, element.getGrid().getStepX);
		controller.registerProperty("GridXAutoStepMin", element.getGrid().setAutoStepXMin, element.getGrid().getAutoStepXMin);
		controller.registerProperty("GridXAutoTicksRange", element.getGrid().setAutoTicksXRange, element.getGrid().getAutoTicksXRange);
		controller.registerProperty("GridXScale", element.getGrid().setScaleX, element.getGrid().getScaleX);
		controller.registerProperty("GridXScalePrecision", element.getGrid().setScalePrecisionX, element.getGrid().getScalePrecisionX);
		controller.registerProperty("GridXFixedTick", element.getGrid().setFixedTickX, element.getGrid().getFixedTickX);				
		controller.registerProperty("GridXAutoTicks", element.getGrid().setAutoTicksX, element.getGrid().getAutoTicksX);
		controller.registerProperty("GridXLineColor", element.getGrid().setLineColorX);
		controller.registerProperty("GridXLineWidth", element.getGrid().setLineWidthX);
		controller.registerProperty("GridXShapeRendering", element.getGrid().setShapeRenderingX);

		controller.registerProperty("GridYShow", element.getGrid().setShowY, element.getGrid().getShowY);
		controller.registerProperty("GridYTicks", element.getGrid().setTicksY, element.getGrid().getTicksY);
		controller.registerProperty("GridYStep", element.getGrid().setStepY, element.getGrid().getStepY);
		controller.registerProperty("GridYAutoStepMin", element.getGrid().setAutoStepYMin, element.getGrid().getAutoStepYMin);
		controller.registerProperty("GridYAutoTicksRange", element.getGrid().setAutoTicksYRange, element.getGrid().getAutoTicksYRange);
		controller.registerProperty("GridYScale", element.getGrid().setScaleY, element.getGrid().getScaleY);
		controller.registerProperty("GridYScalePrecision", element.getGrid().setScalePrecisionY, element.getGrid().getScalePrecisionY);		
		controller.registerProperty("GridYFixedTick", element.getGrid().setFixedTickY, element.getGrid().getFixedTickY);
		controller.registerProperty("GridYAutoTicks", element.getGrid().setAutoTicksY, element.getGrid().getAutoTicksY);
		controller.registerProperty("GridYLineColor", element.getGrid().setLineColorY);
		controller.registerProperty("GridYLineWidth", element.getGrid().setLineWidthY);
		controller.registerProperty("GridYShapeRendering", element.getGrid().setShapeRenderingY);

		// axis
 		controller.registerProperty("AxisXShow", element.getAxisX().setShow);
		controller.registerProperty("AxisXLineColor", element.getAxisX().getStyle().setLineColor);
		controller.registerProperty("AxisXLineWidth", element.getAxisX().getStyle().setLineWidth);
		controller.registerProperty("AxisXShapeRendering", element.getAxisX().getStyle().setShapeRendering);		
		controller.registerProperty("AxisXFixedTick", element.getAxisX().setFixedTick, element.getAxisX().getFixedTick);
		controller.registerProperty("AxisXAutoTicks", element.getAxisX().setAutoTicks, element.getAxisX().getAutoTicks);
		controller.registerProperty("AxisXTicks", element.getAxisX().setTicks, element.getAxisX().getTicks);
		controller.registerProperty("AxisXStep", element.getAxisX().setStep, element.getAxisX().getStep);
		controller.registerProperty("AxisXTickStep", element.getAxisX().setTickStep, element.getAxisX().getTickStep);
		controller.registerProperty("AxisXAutoStepMin", element.getAxisX().setAutoStepMin, element.getAxisX().getAutoStepMin);
		controller.registerProperty("AxisXAutoTicksRange", element.getAxisX().setAutoTicksRange, element.getAxisX().getAutoTicksRange);
		controller.registerProperty("AxisXScale", element.getAxisX().setScale, element.getAxisX().getScale);
		controller.registerProperty("AxisXScalePrecision", element.getAxisX().setScalePrecision, element.getAxisX().getScalePrecision);	    
	    controller.registerProperty("AxisXFont", element.getAxisX().getFont().setFont);
		controller.registerProperty("AxisXFontColor", element.getAxisX().getFont().setFillColor);

 		controller.registerProperty("AxisYShow", element.getAxisY().setShow);
		controller.registerProperty("AxisYLineColor", element.getAxisY().getStyle().setLineColor);
		controller.registerProperty("AxisYLineWidth", element.getAxisY().getStyle().setLineWidth);
		controller.registerProperty("AxisYShapeRendering", element.getAxisY().getStyle().setShapeRendering);
		controller.registerProperty("AxisYFixedTick", element.getAxisY().setFixedTick, element.getAxisY().getFixedTick);
		controller.registerProperty("AxisYAutoTicks", element.getAxisY().setAutoTicks, element.getAxisY().getAutoTicks);
		controller.registerProperty("AxisYTicks", element.getAxisY().setTicks, element.getAxisY().getTicks);
		controller.registerProperty("AxisYStep", element.getAxisY().setStep, element.getAxisY().getStep);
		controller.registerProperty("AxisYTickStep", element.getAxisY().setTickStep, element.getAxisY().getTickStep);
		controller.registerProperty("AxisYAutoStepMin", element.getAxisY().setAutoStepMin, element.getAxisY().getAutoStepMin);
		controller.registerProperty("AxisYAutoTicksRange", element.getAxisY().setAutoTicksRange, element.getAxisY().getAutoTicksRange);
		controller.registerProperty("AxisYScale", element.getAxisY().setScale, element.getAxisY().getScale);
		controller.registerProperty("AxisYScalePrecision", element.getAxisY().setScalePrecision, element.getAxisY().getScalePrecision);
		controller.registerProperty("AxisYFont", element.getAxisY().getFont().setFont);
		controller.registerProperty("AxisYFontColor", element.getAxisY().getFont().setFillColor);

		// Axis and Grid
		controller.registerProperty("XFixedTick", function(v) {
			element.getAxisX().setFixedTick(v);
			element.getGrid().setFixedTickX(v);
		});
		controller.registerProperty("YFixedTick", function(v) {
			element.getAxisY().setFixedTick(v);
			element.getGrid().setFixedTickY(v);
		});

		//controller.registerProperty("GridXTicks", element.getGrid().setTicksX, element.getGrid().getTicksX);
		//controller.registerProperty("AxisXTicks", element.getAxisX().setTicks, element.getAxisX().getTicks);
		controller.registerProperty("XTicks", function(v) {
			element.getAxisX().setTicks(v);
			element.getGrid().setTicksX(v);
		});
		controller.registerProperty("YTicks", function(v) {
			element.getAxisY().setTicks(v);
			element.getGrid().setTicksY(v);
		});

		//controller.registerProperty("GridXStep", element.getGrid().setStepX, element.getGrid().getStepX);
		//controller.registerProperty("AxisXStep", element.getAxisX().setStep, element.getAxisX().getStep);
		controller.registerProperty("XStep", function(v) {
			element.getAxisX().setStep(v);
			element.getGrid().setStepX(v);
		});
		controller.registerProperty("YStep", function(v) {
			element.getAxisY().setStep(v);
			element.getGrid().setStepY(v);
		});

		controller.registerProperty("XTickStep", function(v) {
			element.getAxisX().setTickStep(v);
			element.getGrid().setTickStepX(v);
		});
		controller.registerProperty("YTickStep", function(v) {
			element.getAxisY().setTickStep(v);
			element.getGrid().setTickStepY(v);
		});

		//controller.registerProperty("GridXAutoStepMin", element.getGrid().setAutoStepXMin, element.getGrid().getAutoStepXMin);
		//controller.registerProperty("AxisXAutoStepMin", element.getAxisX().setAutoStepMin, element.getAxisX().getAutoStepMin);
		controller.registerProperty("XAutoStepMin", function(v) {
			element.getAxisX().setAutoStepMin(v);
			element.getGrid().setAutoStepXMin(v);
		});
		controller.registerProperty("YAutoStepMin", function(v) {
			element.getAxisY().setAutoStepMin(v);
			element.getGrid().setAutoStepYMin(v);
		});

		//controller.registerProperty("GridXAutoTicksRange", element.getGrid().setAutoTicksXRange, element.getGrid().getAutoTicksXRange);
		//controller.registerProperty("AxisXAutoTicksRange", element.getAxisX().setAutoTicksRange, element.getAxisX().getAutoTicksRange);
		controller.registerProperty("XAutoTicksRange", function(v) {
			element.getAxisX().setAutoTicksRange(v);
			element.getGrid().setAutoTicksXRange(v);
		});
		controller.registerProperty("YAutoTicksRange", function(v) {
			element.getAxisY().setAutoTicksRange(v);
			element.getGrid().setAutoTicksyRange(v);
		});

		//controller.registerProperty("GridXScale", element.getGrid().setScaleX, element.getGrid().getScaleX);
		//controller.registerProperty("AxisXScale", element.getAxisX().setScale, element.getAxisX().getScale);
		controller.registerProperty("XScale", function(v) {
			element.getAxisX().setScale(v);
			element.getGrid().setScaleX(v);
		});
		controller.registerProperty("YScale", function(v) {
			element.getAxisY().setScale(v);
			element.getGrid().setScaleY(v);
		});

		//controller.registerProperty("GridXScalePrecision", element.getGrid().setScalePrecisionX, element.getGrid().getScalePrecisionX);
		//controller.registerProperty("AxisXScalePrecision", element.getAxisX().setScalePrecision, element.getAxisX().getScalePrecision);
		controller.registerProperty("XScalePrecision", function(v) {
			element.getAxisX().setScalePrecision(v);
			element.getGrid().setScalePrecisionX(v);
		});
		controller.registerProperty("YScalePrecision", function(v) {
			element.getAxisY().setScalePrecision(v);
			element.getGrid().setScalePrecisionY(v);
		});

		//controller.registerProperty("GridXAutoTicks", element.getGrid().setAutoTicksX, element.getGrid().getAutoTicksX);
		//controller.registerProperty("AxisXAutoTicks", element.getAxisX().setAutoTicks, element.getAxisX().getAutoTicks);
		controller.registerProperty("XAutoTicks", function(v) {
			element.getAxisX().setAutoTicks(v);
			element.getGrid().setAutoTicksX(v);
		});
		controller.registerProperty("YAutoTicks", function(v) {
			element.getAxisY().setAutoTicks(v);
			element.getGrid().setAutoTicksY(v);
		});
		
	}
};

EJSS_DRAWING2D.plottingPanel = function(mName,mGraphicsMode) {
	var self = EJSS_DRAWING2D.drawingPanel(mName,mGraphicsMode);
	
	/**
	 * @method getGrid
	 * @return grid element
	 */
	self.getGrid = function() {
		return mGrid;
	};
	
	/***
	 * @method getAxisX()
	 * @return the axis element for the X dimension
     * @visibility public
	 * @see EJSS_DRAWING2D.Axis
	 */
	self.getAxisX = function() {
		return mAxisX;
	};
	
	/***
	 * @method getAxisY()
	 * @return the axis element for the Y dimension
     * @visibility public
	 * @see EJSS_DRAWING2D.Axis
	 */
	self.getAxisY = function() {
		return mAxisY;
	};
	
	/**
	 * @method getTitleX
	 * @return axis X title element
	 */
	self.getTitleX = function() {
		return mTitleX;
	};
	
	/**
	 * @method getTitleY
	 * @return axis Y title element
	 */
	self.getTitleY = function() {
		return mTitleY;
	};
	
	/**
	 * @method getTitle
	 * @return plot title element
	 */
	self.getTitle = function() {
		return mTitle;
	};
	
	/**
	 * @method getFixedTickX
	 * @return x fixed tick
	 */
	self.getFixedTickX = function() {
		return getGrid().getFixedTickX();
	};

	/**
	 * @method getFixedTickY
	 * @return y fixed tick
	 */
	self.getFixedTickY = function() {
		return getGrid().getFixedTickY();
	};

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.PlottingPanel.registerProperties(self, controller);
	};
	
	var mBottomGroup = EJSS_DRAWING2D.group(mName + ".bottomgroup");
	mBottomGroup.setRelativePosition("SOUTH_WEST");
	mBottomGroup.panelChangeListener = function(event) {
		if (event == "bounds") {
			var xmin = self.getRealWorldXMin();
			var xmax = self.getRealWorldXMax();
			var ymin = self.getRealWorldYMin();
			var ymax = self.getRealWorldYMax();
			var sx = Math.abs(xmax - xmin); 
			var sy = Math.abs(ymax - ymin);
			mBottomGroup.setSize([sx, sy]);
			mBottomGroup.setPosition([xmin, ymin]);
			return true;
		}
	};
	mBottomGroup.panelChangeListener("bounds");

	var mGrid = EJSS_DRAWING2D.grid(mName + ".grid");
	mGrid.setGroup(mBottomGroup);
	mGrid.setRelativePosition("SOUTH_WEST");
	mGrid.setLineColorX("lightgray");
	mGrid.setShapeRenderingX("RENDER_CRISPEDGES");
	mGrid.setLineColorY("lightgray");
	mGrid.setShapeRenderingY("RENDER_CRISPEDGES");
	mGrid.setSize([1,1]);
	mGrid.setPosition([0, 0]);
	mGrid.panelChangeListener = function(event) {
		if (event == "bounds") {
			var xmin = self.getRealWorldXMin();
			var xmax = self.getRealWorldXMax();
			mGrid.setScaleX([xmin, xmax]);
			mGrid.setTicksXMode(self.getTypeScaleX());
			var ymin = self.getRealWorldYMin();
			var ymax = self.getRealWorldYMax();
			mGrid.setScaleY([ymin, ymax]);
			mGrid.setTicksYMode(self.getTypeScaleY());		
			
			// mGrid.setInvertedScaleY(self.getInvertedScaleY());
			return true;
		}
	};
	var mTopGroup = EJSS_DRAWING2D.group(mName + ".topgroup");
	mTopGroup.setRelativePosition("SOUTH_WEST");
	mTopGroup.panelChangeListener = function(event) {
		if (event == "bounds") {
			var xmin = self.getRealWorldXMin();
			var xmax = self.getRealWorldXMax();
			var ymin = self.getRealWorldYMin();
			var ymax = self.getRealWorldYMax();
			var sx = Math.abs(xmax - xmin); 
			var sy = Math.abs(ymax - ymin);
			mTopGroup.setSize([sx, sy]);
			mTopGroup.setPosition([xmin, ymin]);
			return true;
		}
	};
	mTopGroup.panelChangeListener("bounds");

	var mAxisX = EJSS_DRAWING2D.axis(mName + ".axisX");
	mAxisX.setGroup(mTopGroup);
	mAxisX.setRelativePosition("WEST");
	mAxisX.getStyle().setLineColor("black");
	mAxisX.getStyle().setShapeRendering("RENDER_CRISPEDGES");
	mAxisX.getFont().setFontSize(10);
	mAxisX.setSize([1, 1]);
	mAxisX.setPosition([0, 0]);
	mAxisX.setOrient("AXIS_HORIZONTAL");
	mAxisX.panelChangeListener = function(event) {
		if (event == "bounds") {
			var xmin = self.getRealWorldXMin();
			var xmax = self.getRealWorldXMax();
			mAxisX.setScale([xmin, xmax]);
			mAxisX.setTicksMode(self.getTypeScaleX());
			
			if(self.getInvertedScaleY()) {
				mAxisX.setTextPosition("TICKS_DOWN");
				mTitleX.setMarginY(-30);
				mTitle.setMarginY(20);
			} else {
				mAxisX.setTextPosition("TICKS_UP");
				mTitleX.setMarginY(30);
				mTitle.setMarginY(-20);
			}
			return true;
		}
	};
	mAxisX.setTicksMode(self.getTypeScaleX());
	
	
	var mAxisY = EJSS_DRAWING2D.axis(mName + ".axisY");
	mAxisY.setGroup(mTopGroup);
	mAxisY.setRelativePosition("SOUTH");
	mAxisY.getStyle().setLineColor("black");
	mAxisY.getStyle().setShapeRendering("RENDER_CRISPEDGES");
	mAxisY.getFont().setFontSize(10);
	mAxisY.setSize([1, 1]);
	mAxisY.setPosition([0, 0]);
	mAxisY.setOrient("AXIS_VERTICAL");
	mAxisY.panelChangeListener = function(event) {
		if (event == "bounds") {
			var ymin = self.getRealWorldYMin();
			var ymax = self.getRealWorldYMax();
			mAxisY.setScale([ymin, ymax]);
			mAxisY.setTicksMode(self.getTypeScaleY());
			
			mAxisY.setInvertedScaleY(self.getInvertedScaleY());			
			return true;
		}
	};
	mAxisY.setTicksMode(self.getTypeScaleY());
	
	var mTitleX = EJSS_DRAWING2D.text(mName + ".titleX");
	mTitleX.setGroup(mTopGroup);
	mTitleX.setRelativePosition("CENTER");
	mTitleX.getFont().setFontSize(12);
	mTitleX.setPosition([0.5, 0]);
	mTitleX.setMarginY(30);
	mTitleX.setText("x");

	var mTitleY = EJSS_DRAWING2D.text(mName + ".titleY");
	mTitleY.setGroup(mTopGroup);
	mTitleY.setRelativePosition("CENTER");
	mTitleY.getFont().setFontSize(12);
	mTitleY.setWritingMode("MODE_DOWNTOP");
	mTitleY.setPosition([0, 0.5]);
	mTitleY.setMarginX(-40);
	mTitleY.setText("y");

	var mTitle = EJSS_DRAWING2D.text(mName + ".title");
	mTitle.setGroup(mTopGroup);
	mTitle.setRelativePosition("CENTER");
	mTitle.getFont().setFontSize(12);
	mTitle.setPosition([0.5, 1.0]);
	mTitle.setMarginY(-20);
	mTitle.setText("Plot");

	self.getStyle().setLineColor('black');
	self.getStyle().setFillColor('white');
	self.getStyle().setShapeRendering("RENDER_CRISPEDGES");

	self.setGutters([50, 50, 50, 50]);
	self.getGuttersStyle().setLineColor('black');
	self.getGuttersStyle().setFillColor('rgb(211,216,255)');
	self.getGuttersStyle().setShapeRendering("RENDER_CRISPEDGES");

	self.addDecoration(mBottomGroup);
	self.addDecoration(mGrid);
	self.addDecoration(mTopGroup, -1, true);
	self.addDecoration(mAxisX, -1, true);
	self.addDecoration(mAxisY, -1, true);
	self.addDecoration(mTitleX, -1, true);
	self.addDecoration(mTitleY, -1, true);
	self.addDecoration(mTitle, -1, true);

	return self;
};
