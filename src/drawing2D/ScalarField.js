/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * ScalarField
 * @class ScalarField 
 * @constructor  
 */
EJSS_DRAWING2D.ScalarField = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

		controller.registerProperty("ZData", element.setData, element.getData);
		controller.registerProperty("MinimumZ", element.setMinimumZ, element.getMinimumZ);
		controller.registerProperty("MaximumZ", element.setMaximumZ, element.getMaximumZ);
//		controller.registerProperty("AutoscaleZ", element.setAutoscaleZ, element.getAutoscaleZ);
//		controller.registerProperty("SymmetricZ", element.setSymmetricZ, element.getSymmetricZ);
		//controller.registerProperty("ExpandedZ", element.setMinimumX, element.getMinimumX);

//		controller.registerProperty("FloorColor", element.setFloorColor, element.getFloorColor);
//		controller.registerProperty("CeilColor", element.setCeilColor, element.getCeilColor);
		controller.registerProperty("NumColors", element.getColorMapper().setNumberOfColors);
		controller.registerProperty("Colors", element.getColorMapper().setColorPalette);
		controller.registerProperty("Palette", element.getColorMapper().setPaletteType);
//		controller.registerProperty("ShowGrid", element.setShowGrid, element.getShowGrid);

        controller.registerProperty("AutoUpdate", element.setAutoupdate);  

		controller.registerProperty("ParallelDrawing", element.setParallelDrawing);
    },
};

/**
 * Creates a 2D scalarField
 * @method ScalarField
 */
EJSS_DRAWING2D.scalarField = function (name) {
	var self = EJSS_DRAWING2D.element(name);
 
  	var mColorMapper = EJSS_DRAWING2D.colorCoded(16, EJSS_DRAWING2D.ColorMapper.SPECTRUM); 
	var mCeilColor = mColorMapper.getCeilColor();
	var mFloorColor = mColorMapper.getFloorColor();
 	var mShowGrid = true;
	var mData = [];
	var mAutoUpdate=true;
	var mzMax = 1;
	var mzMin = -1;
	var mAutoscaleZ = true;
	var mSymmetricZ = false;
	var mParallelDrawing = false;
 
	self.getClass = function() {
  		return "ElementScalarField";
	}

   	/**
   	* @method setParallelDrawing
   	* @param parallel
   	*/
  	self.setParallelDrawing = function (parallel) {
	  	if(mParallelDrawing != parallel) {
    		mParallelDrawing = parallel;
    		self.setChanged(true);
    	}
  	}
  
  	/**
   	* @method getParallelDrawing
   	* @return parallel
   	*/
  	self.getParallelDrawing = function() {
    	return mParallelDrawing;
  	}

   	/**
   	* @method setFloorColor
   	* @param color
   	*/
  	self.setFloorColor = function (color) {
	  	if(mFloorColor != color) {
    		mFloorColor = color;
    		mColorMapper.setFloorCeilColor(mFloorColor,mCeilColor);
    		self.setChanged(true);
    	}
  	}
  
  	/**
   	* @method getFloorColor
   	* @return color
   	*/
  	self.getFloorColor = function() {
    	return mFloorColor;
  	}

   	/**
   	* @method setCeilColor
   	* @param color
   	*/
  	self.setCeilColor = function (color) {
	  	if(mCeilColor != color) {
    		mCeilColor = color;
    		mColorMapper.setFloorCeilColor(mFloorColor,mCeilColor);
    		self.setChanged(true);
    	}
  	}
  
  	/**
   	* @method getCeilColor
   	* @return color
   	*/
  	self.getCeilColor = function() {
    	return mCeilColor;
  	}

	/** 
	 * @method setData
	 * @param data
	 */
	self.setData = function (data) {
	  if (mAutoUpdate || mData != data) {
	    mData = data;
		mDataChanged = true;
		self.setChanged(true);
	  }
	}
	  
	/**
	 * @method getData
	 * @return
	 */
	self.getData = function() { 
		return mData; 
	}
	 
	self.setAutoupdate= function (auto) {
	  mAutoUpdate = auto;
      if (mAutoUpdate) mDataChanged = true;
    }
    
	/** 
	 * @method getMaximumZ
	 * @return max
	 */
  	self.getMaximumZ = function() {
  		return mzMax;
  	}

	/** 
	 * Sets ceiling value for the colors 
	 *
	 * @method setMaximumZ
	 * @param max
	 */
  	self.setMaximumZ = function(max) {
  		if(mzMax != max) {
  			mzMax = max;
	    	if (!mAutoscaleZ) {
	    		mColorMapper.setScale(mzMin, mzMax);		
	  		}
			self.setChanged(true);
  		};
  	}

	/** 
	 * @method getMinimumZ
	 * @return min
	 */
  	self.getMinimumZ = function() {
  		return mzMin;
  	}

	/** 
	 * Sets floor value for the colors 
	 *
	 * @method setMinimumZ
	 * @param min
	 */
  	self.setMinimumZ = function(min) {
  		if(mzMin != min) {
  			mzMin = min;
	    	if (!mAutoscaleZ) {
	    		mColorMapper.setScale(mzMin, mzMax);		
	  		}
			self.setChanged(true);
  		};
  	}

  	/**
   	* Sets the autoscale flag.
   	*
   	* If autoscaling is true, then the min and max values of z are span the colors.
   	*
   	* If autoscaling is false, then MaximumZ and MinimumZ values limit the colors.
   	* Values below min map to the first color; values above max map to the last color.
   	*
   	* @method setAutoscaleZ
   	* @param isAutoscale
   	*/
  	self.setAutoscaleZ = function (isAutoscale) {
	  	if(mAutoscaleZ != isAutoscale) {
	    	mAutoscaleZ = isAutoscale;
	    	self.setChanged(true);
	    }
  	}
  
  	/**
   	* Gets the autoscale flag for z.
   	*
   	* @method getAutoscaleZ
   	* @return boolean
   	*/
  	self.getAutoscaleZ = function() {
    	return mAutoscaleZ;
  	}

   	/**
   	* @method setSymmetricZ
   	* @param isAutoscale
   	*/
  	self.setSymmetricZ = function (symmetric) {
	  	if(mSymmetricZ != symmetric) {
    		mSymmetricZ = symmetric;
    		mColorMapper.setSymmetricZ(mSymmetricZ);
    		self.setChanged(true);
    	}
  	}
  
  	/**
   	* @method getSymmetricZ
   	* @return boolean
   	*/
  	self.getSymmetricZ = function() {
    	return mSymmetricZ;
  	}

	/** 
	 * @method setColorMapper
	 * @param colormapper
	 */
	self.setColorMapper = function (colormapper) {
	  	if(mColorMapper != colormapper) {
	  		mColorMapper = colormapper;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getColorMapper
	 * @return
	 */
	self.getColorMapper = function() { 
		return mColorMapper; 
	}
	 
	/** 
	 * @method setShowGrid
	 * @param showgrid
	 */
	self.setShowGrid = function (showgrid) {
	  	if(mShowGrid != showgrid) {
	  		mShowGrid = showgrid;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getShowGrid
	 * @return
	 */
	self.getShowGrid = function() { 
		return mShowGrid; 
	}

	self.registerProperties = function(controller) {
    	EJSS_DRAWING2D.ScalarField.registerProperties(self,controller);
	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	return self;
};



