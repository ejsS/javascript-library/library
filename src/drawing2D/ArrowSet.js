/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//ArrowSet
//---------------------------------

/***
 * ArrowSet is a set of Arrows
 * @class EJSS_DRAWING2D.ArrowSet 
 * @constructor  
 */
EJSS_DRAWING2D.ArrowSet = {

    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING2D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("Offset", 
          function(v) { set.setToEach(function(element,value) { element.setRelativePosition(value); }, v); }
      );
      controller.registerProperty("InteractionPosition", 
          function(v) { set.setToEach(function(element,value) { element.setInteractionPosition(value); }, v); }
      );      

      controller.registerProperty("MarkEnd", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEnd(value); }, v); }
      );
      controller.registerProperty("MarkEndWidth", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEndWidth(value); }, v); }
      );
      controller.registerProperty("MarkEndHeight", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEndHeight(value); }, v); }
      );
      controller.registerProperty("MarkEndDiameter", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEndDiameter(value); }, v); }
      );
      controller.registerProperty("MarkEndColor", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEndColor(value); }, v); }
      );
      controller.registerProperty("MarkEndStroke", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEndStroke(value); }, v); }
      );
      controller.registerProperty("MarkEndRotate", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEndRotate(value); }, v); }
      );
      /*** 
	  * Orientation of the marker. Values are "auto" or the angle to rotate the marker 
	  * @property MarkEndOrient 
	  * @type int|String
	  * @default "auto"
	  */ 
      controller.registerProperty("MarkEndOrient", 
          function(v) { set.setToEach(function(element,value) { element.setMarkEndOrient(value); }, v); }
      );
      

      controller.registerProperty("MarkStart", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStart(value); }, v); }
      );
      controller.registerProperty("MarkStartWidth", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStartWidth(value); }, v); }
      );
      controller.registerProperty("MarkStartHeight", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStartHeight(value); }, v); }
      );
      controller.registerProperty("MarkStartDiameter", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStartDiameter(value); }, v); }
      );
      controller.registerProperty("MarkStartColor", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStartColor(value); }, v); }
      );
      controller.registerProperty("MarkStartStroke", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStartStroke(value); }, v); }
      );
      controller.registerProperty("MarkStartRotate", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStartRotate(value); }, v); }
      );
      /*** 
	  * Orientation of the marker. Values are "auto" or the angle to rotate the marker 
	  * @property MarkStartOrient 
	  * @type int|String
	  * @default "auto"
	  */ 
      controller.registerProperty("MarkStartOrient", 
          function(v) { set.setToEach(function(element,value) { element.setMarkStartOrient(value); }, v); }
      );


      controller.registerProperty("MarkMiddle", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddle(value); }, v); }
      );
      controller.registerProperty("MarkMiddleWidth", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleWidth(value); }, v); }
      );
      controller.registerProperty("MarkMiddleHeight", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleHeight(value); }, v); }
      );
      controller.registerProperty("MarkMiddleDiameter", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleDiameter(value); }, v); }
      );
      controller.registerProperty("MarkMiddleColor", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleColor(value); }, v); }
      );
      controller.registerProperty("MarkMiddleColorSnd", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleColorSnd(value); }, v); }
      );
      controller.registerProperty("MarkMiddleStroke", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleStroke(value); }, v); }
      );
      controller.registerProperty("MarkMiddleRotate", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleRotate(value); }, v); }
      );
      controller.registerProperty("MarkProportion", 
          function(v) { set.setToEach(function(element,value) { element.setMarkProportion(value); }, v); }
      );
      /*** 
	  * Orientation of the marker. Values are "auto" or the angle to rotate the marker 
	  * @property MarkMiddleOrient 
	  * @type int|String
	  * @default "auto"
	  */ 
      controller.registerProperty("MarkMiddleOrient", 
          function(v) { set.setToEach(function(element,value) { element.setMarkMiddleOrient(value); }, v); }
      );


    }

};


/**
 * Creates a set of Segments
 * @method arrowSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.arrowSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.arrow, mName);

  // Static references
  var ArrowSet = EJSS_DRAWING2D.ArrowSet;		// reference for ArrowSet
  
  self.registerProperties = function(controller) {
    ArrowSet.registerProperties(self,controller);
  };


  return self;
};