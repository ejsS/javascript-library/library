/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Shape
 * @class Shape 
 * @constructor  
 */
EJSS_DRAWING2D.Meter = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

      controller.registerProperty("Value", element.setValue, element.getValue);
      controller.registerProperty("Minimum", element.setMinimum, element.getMinimum);
      controller.registerProperty("Maximum", element.setMaximum, element.getMaximum);

      controller.registerProperty("AngleForMinimum", element.setAngleForMinimum, element.getAngleForMinimum);
      controller.registerProperty("AngleForMaximum", element.setAngleForMaximum, element.getAngleForMaximum);

      controller.registerProperty("NumberOfMarks",element.setNumberOfMarks,element.getNumberOfMarks);
      controller.registerProperty("BigMark",element.setBigMark,element.getBigMark);
      controller.registerProperty("MediumMark",element.setMediumMark,element.getMediumMark);
      controller.registerProperty("Digits",element.setDigits,element.getDigits);

      controller.registerProperty("Radius", element.setRadius);
      controller.registerProperty("CircleColor",element.setCircleColor);
      controller.registerProperty("CircleWidth",element.setCircleWidth);
      controller.registerProperty("FillColor",element.setFillColor);
      
      controller.registerProperty("Foreground",element.setForeground);
      controller.registerProperty("LineWidth",element.setLineWidth);

      controller.registerProperty("ArrowColor",element.setArrowColor);
      controller.registerProperty("ArrowWidth",element.setArrowWidth);

      controller.registerProperty("DrawLines",element.setDrawLines);
      controller.registerProperty("DrawText",element.setDrawText);

      controller.registerProperty("Brand",element.setBrand);
      controller.registerProperty("Units",element.setUnits);
      
      controller.registerProperty("MarksFont",element.setMarksFont);
      controller.registerProperty("BrandFont",element.setBrandFont);
      controller.registerProperty("UnitsFont",element.setUnitsFont);
    }

};

/**
 * Creates a 2D circular meter
 * @method shape
 */
EJSS_DRAWING2D.meter = function (mName) {
  var self = EJSS_DRAWING2D.group(mName);

  // Configuration variables
  var mValue = 0.0;
  var mMinimum =   0;
  var mMaximum = 100;

  var mNumberOfMarks = 21;
  var mBigMark = 4;
  var mMediumMark = 2;
  var mDigits = 0;

  var mAngleForMinimum = Math.PI/2 + 1.5*Math.PI/2;
  var mAngleForMaximum = Math.PI/2 - 1.5*Math.PI/2;

  // Implementation variables
  var mArrow, mCircle, mSegments, mTexts, mBrand, mUnits;
  var mColor = "Black", mDangerColor = "Red";
  var mForegroundCreated = false;

  var mElements = [];
  
  self.getElement = function(keyword) {
	  return mElements[keyword];
  };
  
  // ----------------------------------------------------
  // public functions
  // ----------------------------------------------------
  
  /**
   * Sets the value of the element. 
   * @method setValue
   * @param value
   */
  self.setValue = function(value) {
    if (mValue!=value) {
    	mValue = value;
    	self.adjustPosition();
    }
  }
  
  /**
   * @method getValue
   * @return current value
   */
  self.getValue = function() { 
    return mValue; 
  };    

  /**
   * Sets the minimum value of the element. 
   * @method setMinimum
   * @param value
   */
  self.setMinimum = function(value) {
    if (mMinimum!=value) {
    	mMinimum = value;
        self.setChanged(true);
    }
  }
  
  /**
   * @method getMinimum
   * @return current minimum
   */
  self.getMinimum = function() { 
    return mMinimum; 
  };    

  /**
   * Sets the minimum value of the element. 
   * @method setMaximum
   * @param value
   */
  self.setMaximum = function(value) {
	if (mMaximum!=value) {
		mMaximum = value;
        self.setChanged(true);
	}
  }
  
  /**
   * @method getMaximum
   * @return current maximum
   */
  self.getMaximum = function() { 
    return mMaximum; 
  };    

  /**
   * Sets the angle at which to display the minimum value 
   * @method setAngleForMinimum
   * @param value
   */
  self.setAngleForMinimum = function(value) {
    if (mAngleForMinimum!=value) {
    	mAngleForMinimum = value;
        self.setChanged(true);
    }
  }
  
  /**
   * @method getAngleForMinimum
   * @return current angle at which to display the minimum value
   */
  self.getAngleForMinimum = function() { 
    return mAngleForMinimum; 
  };    

  /**
   * Sets the angle at which to display the maximum value 
   * @method setAngleForMaximum
   * @param value
   */
  self.setAngleForMaximum = function(value) {
	if (mAngleForMaximum!=value) {
		mAngleForMaximum = value;
        self.setChanged(true);
	}
  }
  
  /**
   * @method getAngleForMaximum
   * @return current angle at which to display the maximum value 
   */
  self.getAngleForMaximum = function() { 
    return mAngleForMaximum; 
  };    

  /***
   * Sets the number of marks
   * @method setNumberOfMarks(value)
   * @visibility public
   * @param value int
   */ 
  self.setNumberOfMarks = function(value) {
	  if (value!=mNumberOfMarks) {
		  mNumberOfMarks = value;
        self.setChanged(true);
	  }
  };

  /***
   * Gets the number of marks
   * @method getNumberOfMarks
   * @visibility public
   * @return int
   */
  self.getNumberOfMarks = function() {
  	return mNumberOfMarks;
  };

  /***
   * Sets the interval for big marks
   * @method setBigMark(value)
   * @visibility public
   * @param value int 
   */ 
  self.setBigMark = function(value) {
	  if (value!=mBigMark) {
        mBigMark = value;
        self.setChanged(true);
	  }
  };

  /***
   * Gets the interval for big marks
   * @method getBigMark
   * @visibility public
   * @return int
   */
  self.getBigMark = function() {
  	return mBigMark;
  };

  /***
   * Sets the interval for medium marks
   * @method setMediumMark(value)
   * @visibility public
   * @param value int
   */ 
  self.setMediumMark = function(value) {
	  if (value!=mMediumMark) {
		  mMediumMark = value;
        self.setChanged(true);
	  }
  };
 
  /***
   * Gets the interval for medium marks
   * @method getMediumMark
   * @visibility public
   * @return value int
   */ 
  self.getMediumMark = function() {
  	return mMediumMark;
  };


  /***
   * Sets the number of digits for the marks
   * @method setDigits(value)
   * @visibility public
   * @param value int 
   */ 
  self.setDigits = function(value) {
	  if (value!=mDigits) {
		  mDigits = value;
        self.setChanged(true);
	  }
  };

  /***
   * Gets the number of digits for the marks
   * @method getPrecision
   * @visibility public
   * @return int
   */
  self.getDigits = function() {
  	return mDigits;
  };
  
  // ----------------------------------------------------
  // Properties overwritten
  // ----------------------------------------------------

  /**
   * Sets the radius of the element. 
   * @method setRadius
   * @param value
   */
  self.setRadius = function(value) {
	  self.setSizeX(2*value);
	  self.setSizeY(2*value);
	  if (mCircle) mCircle.setChanged(true);
	  if (mArrow) mArrow.setChanged(true);
	  mSegments.setToEach(function(element) { element.setChanged(true); });
	  mTexts.setToEach(function(element) { element.setChanged(true); });
	  if (mBrand) mBrand.setChanged(true);
	  if (mUnits) mUnits.setChanged(true);
  };

  self.setCircleColor = function(value) { mCircle.getStyle().setLineColor(value); };
  
  self.setCircleWidth = function(value) { mCircle.getStyle().setLineWidth(value); };

  self.setFillColor = function(value) { mCircle.getStyle().setFillColor(value); };

  self.setArrowColor = function(value) {
	  if (value=="Red") mDangerColor = "Yellow";
	  else mDangerColor = "Red";
	  mColor = value;
	  mArrow.getStyle().setLineColor(value);
	  mArrow.getStyle().setFillColor(value);
  };

  self.setArrowWidth = function(value) { mArrow.getStyle().setLineWidth(value); }

  self.setForeground = function(value) { 
	  mSegments.setToEach(function(element) { element.getStyle().setLineColor(value); });
	  mTexts.setToEach(function(element) { element.getFont().setFillColor(value); });
	  mBrand.getFont().setFillColor(value);
	  mUnits.getFont().setFillColor(value);
}
  
  self.setLineWidth = function(value) { 
	  mSegments.setToEach(function(element) { element.getStyle().setLineWidth(value); });
  }

  self.setDrawLines = function(value) { 
	  mSegments.setToEach(function(element,value) { element.setVisible(value); },value);
  }
  
  self.setDrawText = function(value) { 
	  mTexts.setToEach(function(element,value) { element.setVisible(value); },value);
  }

  self.setFont = function(value) { 
	  mTexts.setToEach(function(element) { element.getFont().setFont(value); });
	  mBrand.getFont().setFont(value);
	  mUnits.getFont().setFont(value);
  }

  self.setFont = function(value) { 
	  mTexts.setToEach(function(element) { element.getFont().setFont(value); });
	  mBrand.getFont().setFont(value);
	  mUnits.getFont().setFont(value);
  }
  self.setMarksFont = function(value) { 
	  mTexts.setToEach(function(element) { element.getFont().setFont(value); });
  }
  self.setBrandFont = function(value) { mBrand.getFont().setFont(value); }
  self.setUnitsFont = function(value) { mUnits.getFont().setFont(value); }

  self.setBrand = function(value) { mBrand.setText(value); }
  self.setUnits= function(value) { mUnits.setText(value); }

  // ----------------------------------------------------
  // Properties and copies
  // ----------------------------------------------------

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Meter.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // private or protected functions
  // ----------------------------------------------------

  self.superSetParent = self.setParent;
  
  self.setParent = function(parent, sibling) {
	self.superSetParent(parent,sibling);
	if (!mForegroundCreated) {
	  self.createBasics();
	  self.createForeground();
	  mForegroundCreated = true;
    }
	self.addParticularChildren();
	self.adjustForeground();
  }

  self.dataCollected = function() {
	  if (self.isChanged()) {
		  self.adjustForeground();
		  self.setChanged(false);
	  }
  }; 
  
  self.addParticularChildren = function() {
		if (mCircle) mCircle.setParent(self);
		if (mSegments) mSegments.setToEach(function(element) { element.setParent(self); });
		if (mTexts) mTexts.setToEach(function(element) { element.setParent(self); });
		if (mBrand) mBrand.setParent(self);
		if (mUnits) mUnits.setParent(self);
		if (mArrow) mArrow.setParent(self);
  };

  self.createBasics = function() {
    mCircle = EJSS_DRAWING2D.shape(mName+".circle");
    mCircle.setSize([1,1]);
    mCircle.setShapeType("ELLIPSE");
    mCircle.getStyle().setLineWidth(3);
    mCircle.getStyle().setLineColor("Grey");
    mCircle.getStyle().setFillColor("White");
    mElements['circle'] = mCircle;

    mArrow = EJSS_DRAWING2D.arrow(mName+".arrow");
    mArrow.setSize([0.45,0]);
    mArrow.getStyle().setLineWidth(2);
    mArrow.setMarkStart("WEDGE");
    mArrow.setMarkEnd("POINTED");
    mElements['arrow'] = mArrow;
  };
  
  self.createForeground = function() {
    mSegments = EJSS_DRAWING2D.segmentSet(mName+".segments");
    mSegments.setToEach(function(element) { element.setRelativePosition("SOUTH_WEST"); });
    mElements['segments'] = mSegments;

    mTexts = EJSS_DRAWING2D.textSet(mName+".texts");
    mTexts.setToEach(function(element) { element.getFont().setFont("normal bold 12px \"Courier New\", Courier, monospace"); });
    mElements['texts'] = mTexts;

    mBrand = EJSS_DRAWING2D.text(mName+".brand");
    mBrand.setPosition([0,0.20]);
    mBrand.setText("Meter");
    mBrand.getFont().setFont("normal bold 12px \"Courier New\", Courier, monospace");
    mElements['brand'] = mBrand;

    mUnits = EJSS_DRAWING2D.text(mName+".Units");
    mUnits.setPosition([0,-0.25]);
    mUnits.setText("%");
    mUnits.getFont().setFont("normal bold 12px \"Courier New\", Courier, monospace");
    mElements['units'] = mUnits;
}
  
  self.interpolateAngle= function(value) {
	 return mAngleForMinimum + (value-mMinimum)/(mMaximum-mMinimum)*(mAngleForMaximum-mAngleForMinimum);
  };

  self.valueFromAngle= function(angle) {
	  angle = Math.max(Math.min(angle,mAngleForMinimum),mAngleForMaximum);
	  mValue = mMaximum - (angle-mAngleForMaximum)/(mAngleForMinimum-mAngleForMaximum)*(mMaximum-mMinimum);
      return angle;
  };

  self.adjustPosition = function() {
	  var actualValue = mValue;
	  if (mValue<mMinimum) {
		  actualValue = mMinimum;
		  mArrow.getStyle().setLineColor(mDangerColor);
		  mArrow.getStyle().setFillColor(mDangerColor);
	  }
	  else if (mValue>mMaximum) {
		  actualValue = mMaximum;
		  mArrow.getStyle().setLineColor(mDangerColor);
		  mArrow.getStyle().setFillColor(mDangerColor);
	  }
	  else {
		  actualValue = mValue;
		  mArrow.getStyle().setLineColor(mColor);
		  mArrow.getStyle().setFillColor(mColor);
	  }
	  var angle = mAngleForMinimum + (actualValue-mMinimum)/(mMaximum-mMinimum)*(mAngleForMaximum-mAngleForMinimum);
	  mArrow.setTransformation(angle);
  }

  self.foregroundIsInside = function() { return true; };
  
  self.adjustForeground = function() {
	  var MARK = 1.0/12;
	  
	  mSegments.setNumberOfElements(mNumberOfMarks);
	  var dAngle = (mAngleForMaximum-mAngleForMinimum)/(mNumberOfMarks-1);
	  var angle = mAngleForMinimum;
	  var mediumMarksCounter = 0;
	  var bigMarksCounter = 0;
	  var direction =  self.foregroundIsInside() ? -MARK : MARK;
	  
	  for (var i=0; i<mNumberOfMarks; i++) {
	    var segment = mSegments.getElement(i);
	    var xPos = Math.cos(angle)/2;
	    var yPos = Math.sin(angle)/2;
	    segment.setPosition([xPos,yPos]);
	    if (i%mBigMark==0) {
	      bigMarksCounter++;
	      segment.setSize([2*direction*xPos,2*direction*yPos]);
	    }
	    else if (i%mMediumMark==0) {
	      mediumMarksCounter++;
	      segment.setSize([1.5*direction*xPos,1.5*direction*yPos]);
	    }
	    else {
	      segment.setSize([direction*xPos,direction*yPos]);
	    }
	    angle += dAngle;
	  }
	  
	  mTexts.setNumberOfElements(bigMarksCounter); //+mediumMarksCounter);
	  var text_counter=0;
      var fontSize =  mTexts.getElement(0).getFont().getFontSize();
      direction =  self.foregroundIsInside() ? 1.7 : 2.5;
      if (fontSize>30) direction *= 1.5;
      else if (fontSize>20) direction *= 1.3;
      else if (fontSize>15) direction *= 1.1;
	  for (i=0; i<mNumberOfMarks; i++) {
	    var segment = mSegments.getElement(i);
	    if (i%mBigMark==0) { // || i%mediumMark==0) {
	      var text = mTexts.getElement(text_counter);
	      var pos = segment.getPosition();
	      var size = segment.getSize();
	      text.setPosition([pos[0]+direction*size[0],pos[1]+direction*size[1]]);
	      var mark_value = ((mNumberOfMarks-1-i)*mMinimum + i*mMaximum)/(mNumberOfMarks-1);
	      text.setText(""+mark_value.toFixed(mDigits));
	      text_counter++;
	    }
	  }
	  self.adjustPosition();  
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};


