/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A Histogram displays a diagram of frequencies of data
 * @class EJSS_DRAWING2D.Histogram 
 * @parent EJSS_DRAWING2D.Element
 * @constructor  
 */
EJSS_DRAWING2D.Histogram = {

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy  	
  		
		dest.setInput(source.getInput());
		dest.setEnabled(source.getEnabled());
		dest.setOccurrences(source.getOccurrences());
		dest.setClearAtInput(source.getClearAtInput());
		dest.setDiscrete(source.getDiscrete());
		dest.setNormalized(source.getNormalized());
		dest.setBinWidth(source.getBinWidth());
  	},
  	
  	
	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class
		
	 /*** 
	  * The input value to append
	  * @property PixelPosition 
	  * @type double or double[]
	  * @default false
	  */      		
	 	controller.registerProperty("Input", element.addInput);

	 /*** 
	  * Whether the histogram actually accepts input values
	  * @property Active 
	  * @type boolean
	  * @default true
	  */      		
	 	controller.registerProperty("Active", element.setActive, element.getActive);

	 /*** 
	  * The number of the occurrences of the input
	  * @property Occurrences 
	  * @type int or double
	  * @default 1
	  */      		
	 	controller.registerProperty("Occurrences", element.setOccurrences, element.getOccurrences);

	 /*** 
	  * Whether to clear old data whenever new data gets in
	  * @property ClearAtInput 
	  * @type boolean
	  * @default false
	  */      		
	 	controller.registerProperty("ClearAtInput", element.setClearAtInput, element.getClearAtInput);

	 /*** 
	  * Whether the bins are discrete or continuous. A discrete bin looks like a thin vertical line, 
	  * a continuous one looks like a bar with the width of the bin
	  * @property Discrete 
	  * @type boolean
	  * @default false
	  */      		
	 	controller.registerProperty("Discrete", element.setDiscrete, element.getDiscrete);

	 /*** 
	  * Whether the data are normalized to one
	  * @property Normalized 
	  * @type boolean
	  * @default false
	  */      		
		controller.registerProperty("Normalized", element.setNormalized, element.getNormalized);

	 /*** 
	  * The width of the bins. This is the maximum distance that makes two inputs contribute 
	  * to the same bin height. As an example, is the with is 0.5, the inputs 1.1 and 1.4 
	  * will add their occurrences to the same bin
	  * @property BinWidth 
	  * @type int or double
	  * @default 1
	  */      		
	 	controller.registerProperty("BinWidth", element.setBinWidth, element.getBinWidth);

	 /*** 
	  * Fix bin in the input 
	  * @property FixBin 
	  * @type double
	  * @default 0
	  */      		
	 	controller.registerProperty("FixBin", element.setFixBin, element.getFixBin);
	 	
	}
};

/**
 * Creates a 2D Histogram
 * @method Histogram
 */
EJSS_DRAWING2D.histogram = function(mName) {
	var self = EJSS_DRAWING2D.element(mName);
 
    // Instance variables
	var mCurrentInput = [];
	var mActive = true;
	var mOccurrences = 1;
	var mClearAtInput = false;
	var mDiscrete = false;
    var mNormalized = false;    
    var mBinWidth = 1;
    var mFixBin = 0;
    var mBars = null;
		 
	self.getClass = function() {
		return "ElementHistogram";
	}

  /**
   * Makes it a collector for the view
   * @method dataCollected()
   * @see EJSS_CORE.view
   */
  self.dataCollected  = function() { }  
  
	self.getInput = function() { 
	  	return mCurrentInput; 
	};

	self.addInput = function(input) {
	    if (!mActive) return;
	  	if (mClearAtInput) self.clear();
	  	for (var i=0; i<mOccurrences; i++) {
			if(input instanceof Array) mCurrentInput = mCurrentInput.concat(input);  		
			else mCurrentInput.push(input);
		}
		mBars = null;
	    self.setChanged(true);
	};

	self.clear = function() {
		mCurrentInput = [];
		mBars = null;
		self.setChanged(true);
	}

	self.getClearAtInput = function() { 
	 	return mClearAtInput; 
	}

	self.setClearAtInput = function(clear) {
		if(mClearAtInput != clear) {
	   		mClearAtInput = clear;
	  		self.setChanged(true);
		}
	}

	self.getActive = function() { 
	 	return mActive; 
	}

	self.setActive = function(active) {
		if(mActive != active) {
	   		mActive = active;
	  		self.setChanged(true);
		}
	}

	self.getOccurrences = function() { 
	 	return mOccurrences; 
	}

	self.setOccurrences = function(occurrences) {
		if(mOccurrences != occurrences) {
	   		mOccurrences = occurrences;
	  		self.setChanged(true);
		}
	}

	self.getDiscrete = function() { 
	 	return mDiscrete; 
	}

	self.setDiscrete = function(discrete) {
		if(mDiscrete != discrete) {
	   		mDiscrete = discrete;
	  		self.setChanged(true);
		}
	}
	  
	self.getNormalized = function() { 
	  	return mNormalized; 
	};

	self.setNormalized = function(normalized) {
	  	if(mNormalized != normalized) {
	  		mNormalized = normalized;
	  		mBars = null;
	  		self.setChanged(true);
	  	}
	};

	self.getBinWidth = function() {
	  	return mBinWidth;
	};

	self.setBinWidth = function(width) {
	  	if(mBinWidth != width) {
	  		mBinWidth = width;
	  		mBars = null;
	  		self.setChanged(true);
	  	}
	};

	self.getFixBin = function() {
	  	return mFixBin;
	};

	self.setFixBin = function(fixbin) {
	  	if(mFixBin != fixbin) {
	  		mFixBin = fixbin;
	  		mBars = null;
	  		self.setChanged(true);
	  	}
	};
		
	/**
	 * Computes the histogram as a sequence of [x,height]
	 */      
    self.getBars = function() {
      if (mBars!=null) return mBars;
      if (mCurrentInput.length==0) {
        mBars = [[0,0]];
        return mBars;
      }
      
	  mBars = [];
	  mCurrentInput = mCurrentInput.sort(function(a, b){return a-b});
      // minimum bin
	  var minbin;
	  var halfBin = mBinWidth/2;
	
	  if (mBinWidth == 0) minbin = mCurrentInput[0];
	  else if (mFixBin-halfBin > mCurrentInput[0]) minbin = mFixBin - (Math.floor((mFixBin-halfBin-mCurrentInput[0])/mBinWidth) + 1) * mBinWidth;
	  else if (mFixBin+halfBin < mCurrentInput[0]) minbin = mFixBin + (Math.floor((mCurrentInput[0]-mFixBin-halfBin)/mBinWidth)    ) * mBinWidth;
	  else minbin = mFixBin;
		
		//console.log ("inputs = "+input);
		//console.log ("minbin = "+minbin);
      // iterator
	  var cbin = minbin;	
	  var count = 0;
	  var nInput = mCurrentInput.length;
	
      for (var j=0; j < nInput; j++) {
	    if (mCurrentInput[j] <= cbin + halfBin) count ++
	    else {
		  if (count > 0) {
			if (mNormalized) count /= nInput;
			mBars.push([cbin,count]);
		  }
		  if (mBinWidth == 0) cbin = mCurrentInput[j];
		  else cbin = cbin + (Math.floor((mCurrentInput[j]-cbin-halfBin)/mBinWidth) + 1) * mBinWidth;
		  count = 1;
	    }
      }
      // last bar
      if (count > 0) {
		if (mNormalized) count /= nInput;
		mBars.push([cbin,count]);
	  }
	  return mBars;
	}
	     
	/**
	 * Returns bounds for an element
	 * @override
	 * @method getBounds
	 * @return Object{left,rigth,top,bottom}
	 */
	self.getBounds = function(element) {
	  	self.getBars();
	  	var nBars = mBars.length;
		var xmin = mBars[0][0]; 
		var xmax = mBars[nBars-1][0];
		var ymin = 0; 
		var ymax = 0;
		for (var j=0; j<nBars; j++) ymax = Math.max(ymax,mBars[j][1]);

	    var x = self.getX(), y = self.getY();
	    var sx = self.getSizeX(), sy = self.getSizeY();
	  	var mx = sx/2, my = sy/2;  	
	  	var d = self.getRelativePositionOffset(sx,sy);
	  	
		return {
			left: ((x+d[0])-mx)+xmin*sx,
			right: ((x+d[0])-mx)+xmax*sx,
			top: ((y+d[1])-my)+ymax*sy,
			bottom: ((y+d[1])-my)+ymin*sy
		}
	};  
	
	/**
	 * Returns bounds for an element
	 * @override
	 * @method getBounds
	 * @return Object{left,rigth,top,bottom}
	 */
	self.getBoundsOld = function(element) {
	  	var xmin, xmax, ymin, ymax;
	  	  	
	    var len = mCurrentInput.length;
	    if(len == 0) {
	    	xmin = xmax = ymin = ymax = 0;
	    }             
	    else {
	    	// order and get min and max
	    	console.log ("mCurrentInput="+mCurrentInput);
	    	var ordered = mCurrentInput.sort(function(a, b){return a-b});
	    	var nInput = ordered.length;
			xmin = ordered[0]; xmax = ordered[nInput-1];
			
			// count elements
			ymax = 0; ymin = 0;    	
			var counts = [];
			ordered.forEach(function(x) { counts[x] = (counts[x] || 0)+1; });
			if (mNormalized) {
			  for(var y in counts) {
			    counts[y] /= nInput;
			  }
			} 
			 
			for(var y in counts) { 
				if(counts[y]>ymax) ymax = counts[y]; 
			}									
		}    
	    var x = self.getX(), y = self.getY();
	    var sx = self.getSizeX(), sy = self.getSizeY();
	  	var mx = sx/2, my = sy/2;  	
	  	var d = self.getRelativePositionOffset(sx,sy);
	  	
		return {
			left: ((x+d[0])-mx)+xmin*sx,
			right: ((x+d[0])-mx)+xmax*sx,
			top: ((y+d[1])-my)+ymax*sy,
			bottom: ((y+d[1])-my)+ymin*sy
		}
	};  
              
	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.Histogram.registerProperties(self, controller);
	};
  
  	self.copyTo = function(element) {
    	EJSS_DRAWING2D.Histogram.copyTo(self,element);
  	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	self.getStyle().setFillColor("Blue");
	self.getStyle().setLineColor("Black");
  	self.setRelativePosition("SOUTH_WEST");

	return self;
};

