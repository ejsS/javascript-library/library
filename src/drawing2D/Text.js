/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A Text is a 2D drawing element that displays a text 
 * @class EJSS_DRAWING2D.Text
 * @parent EJSS_DRAWING2D.Element
 */
EJSS_DRAWING2D.Text = {
	MODE_TOPDOWN : 0,
	MODE_RIGTHLEFT : 1,
	MODE_DOWNTOP : 2,
	MODE_LEFTRIGHT: 3,
	
	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy  	
  		EJSS_DRAWING2D.Font.copyTo(source.getFont(),dest.getFont());
  		
		dest.setText(source.getText());
		dest.setWritingMode(source.getWritingMode());
  	},
  	
	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

	 	/*** 
	  	* Text
	  	* @property Text 
	  	* @type String
	  	* @default "" 
	  	*/  
		controller.registerProperty("Text", element.setText);
		controller.registerProperty("Framed", element.setFramed);
		controller.registerProperty("WritingMode", element.setWritingMode);

	 	/*** 
	  	* Font description in one declaration
	  	* @property Font 
	  	* @type String - https://www.w3schools.com/cssref/pr_font_font.asp
	  	* @default "20px Arial" 
	  	*/  
		controller.registerProperty("Font", function (e) {
				element.setDrawingSize([-1,-1]);
				element.getFont().setFont(e);
			});

	 	/*** 
	  	* Font family
	  	* @property FontFamily 
	  	* @type String
	  	* @default "Arial" 
	  	*/  
		controller.registerProperty("FontFamily", element.getFont().setFontFamily);

	 	/*** 
	  	* Font size
	  	* @property FontSize 
	  	* @type String
	  	* @default "20" 
	  	*/  
		controller.registerProperty("FontSize", function (e) {
				element.setDrawingSize([-1,-1]);
				element.getFont().setFontSize(e);
			});

		controller.registerProperty("LetterSpacing", element.getFont().setLetterSpacing);

	 	/*** 
	  	* Outline color
	  	* @property OutlineColor 
	  	* @type String
	  	* @default "none" 
	  	*/  
		controller.registerProperty("OutlineColor", element.getFont().setOutlineColor);

		controller.registerProperty("FontWeight", element.getFont().setFontWeight);

	 	/*** 
	  	* Fill color
	  	* @property FillColor 
	  	* @type String
	  	* @default "none" 
	  	*/  
		controller.registerProperty("FillColor", element.getFont().setFillColor);

	 	/*** 
	  	* Font style
	  	* @property FontStyle 
	  	* @type String
	  	* @default "normal" 
	  	*/  
		controller.registerProperty("FontStyle", element.getFont().setFontStyle);

		controller.registerProperty("MarginX", element.getMarginX().setMarginX);
		controller.registerProperty("MarginY", element.getMarginY().setMarginY);
	},
};

/**
 * Creates a 2D Text
 * @method text
 */
EJSS_DRAWING2D.text = function(mName) {
	var self = EJSS_DRAWING2D.element(mName);

	var mText = "";
	var mWritingMode = EJSS_DRAWING2D.Text.MODE_LEFTRIGHT;
	var mFont = EJSS_DRAWING2D.font(mName);	// font for text
	var mFramed = false; 
	var mMarginX = 0;
	var mMarginY = 0;
	
	var mDrawingSize = [-1,-1]; // autocalculated 

	self.getClass = function() {
		return "ElementText";
	}

  	/***
   	* Sets text
   	* @method setText
   	* @visibility public
   	* @param string
   	*/
	self.setText = function(text) {
		text = text + ""; // convert text to string
		if(mText != text) {
			mText = text;
			self.setChanged(true);
			mDrawingSize = [-1,-1];
		}
	}

  	/***
   	* Gets text
   	* @method getText
   	* @visibility public
   	* @return string
   	*/
	self.getText = function() {
		return mText;
	}

	self.setDrawingSize = function(drawingSize) {
		mDrawingSize = drawingSize;
	}

	self.getDrawingSize = function() {
		return mDrawingSize;
	}

	self.getSizeX = function() {		
		return mDrawingSize[0]; 
	}

	self.getSizeY = function() {
		return mDrawingSize[1];
	}

	self.setMarginX = function(margin) {
		if(mMarginX != margin) {
			mMarginX = margin;
			self.setChanged(true);
		}
	}

	self.getMarginX = function() {
		return mMarginX;
	}

	self.setMarginY = function(margin) {
		if(mMarginY != margin) {
			mMarginY = margin;
			self.setChanged(true);
		}
	}

	self.getMarginY = function() {
		return mMarginY;
	}

	self.getFont = function() {
		return mFont;
	}

	self.setFramed = function(framed) {
		if(mFramed != framed) {
			mFramed = framed;
			self.setChanged(true);
		}
	}

	self.getFramed = function() {
		return mFramed;
	}

	self.setWritingMode = function(writemode) {
	    if (typeof writemode == "string") writemode = EJSS_DRAWING2D.Text[writemode.toUpperCase()];
	    if (mWritingMode != writemode) {
	      mWritingMode = writemode;
	      self.setChanged(true);
	    }
	}

	self.getWritingMode = function() {
		return mWritingMode;
	}

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.Text.registerProperties(self, controller);
	};

  	self.copyTo = function(element) {
    	EJSS_DRAWING2D.Text.copyTo(self,element);
  	};

	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

  	mFont.setChangeListener(function (change) { self.setChanged(true); });
	self.setPixelSize(true); 
	return self;
};

