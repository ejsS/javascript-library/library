/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Cursor
 * @class Cursor 
 * @constructor  
 */
EJSS_DRAWING2D.Cursor = {
    HORIZONTAL : 0,
    VERTICAL : 1,
    CROSSHAIR : 2,
    
	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class
		
		controller.registerProperty("CursorType", element.setCursorType, element.getCursorType);
	},
};

/**
 * Creates a 2D Cursor
 * @method cursor
 */
EJSS_DRAWING2D.cursor = function(name) {
	var self = EJSS_DRAWING2D.element(name);	
	var Cursor = EJSS_DRAWING2D.Cursor;
	
	var mCursorType;

	self.getClass = function() {
		return "ElementCursor";
	}
	
	self.setCursorType = function(cursorType) {
	  var InteractionTarget = EJSS_DRAWING2D.InteractionTarget;
	  var target = self.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION);
      if (typeof cursorType === 'string') 
        mCursorType = EJSS_DRAWING2D.Cursor[cursorType.toUpperCase()];
      else mCursorType = cursorType;
      switch (mCursorType) {
        default : target.setSensitivityType(InteractionTarget.SENSITIVITY_ANY);break;
        case Cursor.HORIZONTAL : target.setSensitivityType(InteractionTarget.SENSITIVITY_HORIZONTAL);break;
        case Cursor.VERTICAL : target.setSensitivityType(InteractionTarget.SENSITIVITY_VERTICAL);break;
     }   
	}

	self.getCursorType = function() {
	  return mCursorType;
	}

	self.isChanged = function() {
		return true;
	}

	self.registerProperties = function(controller) {
		Cursor.registerProperties(self, controller);
	};

	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	//self.setSize([0.1,0.1]);
	self.setMeasured(false);
	mCursorType = Cursor.CROSSHAIR;
	self.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setSensitivityType(EJSS_DRAWING2D.InteractionTarget.SENSITIVITY_ANY);

	return self;
};

