/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Grid
 * @class Grid 
 * @constructor  
 */
EJSS_DRAWING2D.Grid = {	
	SCALE_NUM	: 0,	// decimal
	SCALE_LOG	: 1,	// logarithmic
	
	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("StepX", element.setStepX, element.getStepX);
		controller.registerProperty("StepY", element.setStepY, element.getStepY);
		controller.registerProperty("TicksX", element.setTicksX, element.getTicksX);
		controller.registerProperty("TicksY", element.setTicksY, element.getTicksY);
		controller.registerProperty("TickStepX", element.setTickStepX, element.getTickStepX);
		controller.registerProperty("TickStepY", element.setTickStepY, element.getTickStepY);
		controller.registerProperty("TicksXMode", element.setTicksXMode, element.getTicksXMode);
		controller.registerProperty("TicksYMode", element.setTicksYMode, element.getTicksYMode);

		controller.registerProperty("ShowX", element.setShowX, element.getShowX);
		controller.registerProperty("ShowY", element.setShowY, element.getShowY);

		controller.registerProperty("FixedTickX", element.setFixedTickX, element.getFixedTickX);				
		controller.registerProperty("FixedTickY", element.setFixedTickY, element.getFixedTickY);				
		controller.registerProperty("ScaleX", element.setScaleX, element.getScaleX);				
		controller.registerProperty("ScaleY", element.setScaleY, element.getScaleY);				
		
		controller.registerProperty("ScalePrecisionX", element.setScalePrecisionX, element.getScalePrecisionX);		
		controller.registerProperty("ScalePrecisionY", element.setScalePrecisionY, element.getScalePrecisionY);		
		
		// auto ticks
		controller.registerProperty("AutoTicksX", element.setAutoTicksX, element.getAutoTicksX);				
		controller.registerProperty("AutoTicksY", element.setAutoTicksY, element.getAutoTicksY);				
		controller.registerProperty("AutoStepXMin", element.setAutoStepXMin, element.getAutoStepXMin);				
		controller.registerProperty("AutoStepYMin", element.setAutoStepYMin, element.getAutoStepYMin);				
		controller.registerProperty("AutoTicksXRange", element.setAutoTicksXRange, element.getAutoTicksXRange);				
		controller.registerProperty("AutoTicksYRange", element.setAutoTicksYRange, element.getAutoTicksYRange);
		
		// style
		controller.registerProperty("LineColorX", element.setLineColorX, element.getLineColorX);
		controller.registerProperty("LineWidthX", element.setLineWidthX, element.getLineWidthX);
		controller.registerProperty("ShapeRenderingX", element.setShapeRenderingX, element.getShapeRenderingX);
		controller.registerProperty("LineColorY", element.setLineColorY, element.getLineColorY);
		controller.registerProperty("LineWidthY", element.setLineWidthY, element.getLineWidthY);
		controller.registerProperty("ShapeRenderingY", element.setShapeRenderingY, element.getShapeRenderingY);						
	}
			
};

/**
 * Creates a 2D Grid
 * @method grid
 */
EJSS_DRAWING2D.grid = function(name) {
	var self = EJSS_DRAWING2D.element(name);
 
  	// drawing priority: mAutoTicks - mTicks - mStep
 	var mAutoTicksX = true;			// auto-ticks in X
 	var mAutoTicksY = true;			// auto-ticks in Y
 	var mTicksX = 0;				// number of ticks in X
 	var mTicksY = 0;				// number of ticks in Y
 	var mStepX = 0;					// step between ticks in pixels in X
 	var mStepY = 0;					// step between ticks in pixels in Y
    var mTickStepX = 0;
    var mTickStepY = 0;
	var mShowX = true;				// show X
	var mShowY = true;				// show Y

	var mFixedTickX = Number.NaN;			// ticks fixed in axis X	
	var mFixedTickY = Number.NaN;			// ticks fixed in axis Y	
	var mScaleX = [-1,1];			// X axis scale
	var mScaleY = [-1,1];			// Y axis scale
	var mScalePrecisionX = 1;		// number of decimals X
	var mScalePrecisionY = 1;		// number of decimals Y
	
	// ticks properties
 	var mTicksXMode = EJSS_DRAWING2D.Grid.SCALE_NUM;		// X axis scale 
 	var mTicksYMode = EJSS_DRAWING2D.Grid.SCALE_NUM;		// Y axis scale
 
	// auto ticks 	
	var mAutoStepXMin = 40;		// step x minimun in pixels
	var mAutoStepYMin = 40;		// step y minimun in pixels
	var mAutoTicksXRange = [5,10,20];		// ticks x range
	var mAutoTicksYRange = [5,10,20];		// ticks y range
											//   is the step minimun possible in mAutoTicksRange[length-1]?
											//   and in mAutoTicksRange[length-2]? ... 
											//   then mAutoTicksRange[length-2] is the number of ticks   

	// styles
	var mLineColorX = 'black';
	var mLineWidthX = 0.5;
	var mShapeRenderingX = EJSS_DRAWING2D.Style.RENDER_AUTO;
	var mLineColorY = 'black';
	var mLineWidthY = 0.5;
	var mShapeRenderingY = EJSS_DRAWING2D.Style.RENDER_AUTO;

	self.getClass = function() {
		return "ElementGrid";
	}

	/** 
	 * @method setAutoTicksRangeY
	 * @param range
	 */
	self.setAutoTicksRangeY = function (range) {
	  	if(mAutoTicksYRange != range) {
	  		mAutoTicksYRange = range;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoTicksRangeY
	 * @return
	 */
	self.getAutoTicksRangeY = function() { 
		return mAutoTicksYRange; 
	}

	/** 
	 * @method setAutoTicksRangeX
	 * @param range
	 */
	self.setAutoTicksRangeX = function (range) {
	  	if(mAutoTicksXRange != range) {
	  		mAutoTicksXRange = range;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoTicksRangeX
	 * @return
	 */
	self.getAutoTicksRangeX = function() { 
		return mAutoTicksXRange; 
	}

	/** 
	 * @method setAutoStepYMin
	 * @param min
	 */
	self.setAutoStepYMin = function (min) {
	  	if(mAutoStepYMin != min) {
	  		mAutoStepYMin = min;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoStepYMin
	 * @return
	 */
	self.getAutoStepYMin = function() { 
		return mAutoStepYMin; 
	}

	/** 
	 * @method setAutoStepXMin
	 * @param min
	 */
	self.setAutoStepXMin = function (min) {
	  	if(mAutoStepXMin != min) {
	  		mAutoStepXMin = min;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoStepXMin
	 * @return
	 */
	self.getAutoStepXMin = function() { 
		return mAutoStepXMin; 
	}

	/** 
	 * @method setScaleY
	 * @param scale
	 */
	self.setScaleY = function (scale) {
	  	if(mScaleY != scale) {
	  		mScaleY = scale;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getScaleY
	 * @return
	 */
	self.getScaleY = function() { 
		return mScaleY; 
	}
	
	/** 
	 * @method setScaleX
	 * @param scale
	 */
	self.setScaleX = function (scale) {
	  	if(mScaleX != scale) {
	  		mScaleX = scale;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getScaleX
	 * @return
	 */
	self.getScaleX = function() { 
		return mScaleX; 
	}
	
	/** 
	 * @method setScalePrecisionX
	 * @param scalePrecisionX
	 */
	self.setScalePrecisionX = function (scalePrecisionX) {
		if(mScalePrecisionX != scalePrecisionX) {
	  		mScalePrecisionX = scalePrecisionX;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getScalePrecisionX
	 * @return
	 */
	self.getScalePrecisionX = function() { 
		return mScalePrecisionX; 
	}

	/** 
	 * @method setScalePrecisionY
	 * @param scalePrecisionY
	 */
	self.setScalePrecisionY = function (scalePrecisionY) {
		if(mScalePrecisionY != scalePrecisionY) {
	  		mScalePrecisionY = scalePrecisionY;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getScalePrecisionY
	 * @return
	 */
	self.getScalePrecisionY = function() { 
		return mScalePrecisionY; 
	}

	/** 
	 * @method setFixedTickX
	 * @param fixed
	 */
	self.setFixedTickX = function (fixed) {
	  	if(mFixedTickX != fixed) {
	  		mFixedTickX = fixed;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getFixedTickX
	 * @return
	 */
	self.getFixedTickX = function() { 
		return mFixedTickX; 
	}

	/** 
	 * @method setFixedTickY
	 * @param fixed
	 */
	self.setFixedTickY = function (fixed) {
	  	if(mFixedTickY != fixed) {
	  		mFixedTickY = fixed;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getFixedTickY
	 * @return
	 */
	self.getFixedTickY = function() { 
		return mFixedTickY; 
	}
			
	/** 
	 * @method setAutoTicksX
	 * @param auto
	 */
	self.setAutoTicksX = function (auto) {
	  	if(mAutoTicksX != auto) {
	  		mAutoTicksX = auto;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoTicksX
	 * @return
	 */
	self.getAutoTicksX = function() { 
		return mAutoTicksX; 
	}

	/** 
	 * @method setAutoTicksY
	 * @param auto
	 */
	self.setAutoTicksY = function (auto) {
	  	if(mAutoTicksY != auto) {
	  		mAutoTicksY = auto;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoTicksY
	 * @return
	 */
	self.getAutoTicksY = function() { 
		return mAutoTicksY; 
	}
	 
	/** 
	 * @method setStepX
	 * @param stetX
	 */
	self.setStepX = function (stepX) {
	  	if(mStepX != stepX) {
	  		mStepX = stepX;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getStepX
	 * @return
	 */
	self.getStepX = function() { 
		return mStepX; 
	}

	/** 
	 * @method setStepY
	 * @param stetY
	 */
	self.setStepY = function (stepY) {
		if(mStepY != stepY) {
	  		mStepY = stepY;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getStepY
	 * @return
	 */
	self.getStepY = function() { 
		return mStepY; 
	}

	/** 
	 * @method setTickStepX
	 * @param stetX
	 */
	self.setTickStepX = function (TickStepX) {
	  	if(mTickStepX != TickStepX) {
	  		mTickStepX = TickStepX;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getTickStepX
	 * @return
	 */
	self.getTickStepX = function() { 
		return mTickStepX; 
	}

	/** 
	 * @method setTickStepY
	 * @param stetY
	 */
	self.setTickStepY = function (TickStepY) {
		if(mTickStepY != TickStepY) {
	  		mTickStepY = TickStepY;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getTickStepY
	 * @return
	 */
	self.getTickStepY = function() { 
		return mTickStepY; 
	}
	
	/** 
	 * @method setTicksX
	 * @param stetX
	 */
	self.setTicksX = function (ticksX) {
		if(mTicksX != ticksX) {			
	  		mTicksX = ticksX;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getTicksX
	 * @return
	 */
	self.getTicksX = function() { 
		return mTicksX; 
	}

	/** 
	 * @method setTicksY
	 * @param stetY
	 */
	self.setTicksY = function (ticksY) {
	  	if(mTicksY != ticksY) {
	  		mTicksY = ticksY;
	  		self.setChanged(true);
	  	}
	}
	  	  
	/**
	 * @method getTicksY
	 * @return
	 */
	self.getTicksY = function() { 
		return mTicksY; 
	}

	/** 
	 * @method setTicksXMode
	 * @param ticksXMode
	 */
	self.setTicksXMode = function (ticksXMode) {
    	if (typeof ticksXMode == "string") ticksXMode = EJSS_DRAWING2D.Grid[ticksXMode.toUpperCase()];
    	if(mTicksXMode != ticksXMode) {
    		mTicksXMode = ticksXMode;	
    		self.setChanged(true);
    	}
	}
	  
	/**
	 * @method getTicksXMode
	 * @return
	 */
	self.getTicksXMode = function() { 
		return mTicksXMode; 
	}

	/** 
	 * @method setTicksYMode
	 * @param ticksYMode
	 */
	self.setTicksYMode = function (ticksYMode) {
    	if (typeof ticksYMode == "string") ticksXMode = EJSS_DRAWING2D.Grid[ticksYMode.toUpperCase()];
    	if(mTicksYMode != ticksYMode) {
    		mTicksYMode = ticksYMode;
    		self.setChanged(true);
    	}	
	}

	/** 
	 * @method setShowX
	 * @param show
	 */
	self.setShowX = function (show) {
	  	if(mShowX != show) {
	  		mShowX = show;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getShowX
	 * @return
	 */
	self.getShowX = function() { 
		return mShowX; 
	}

	/** 
	 * @method setShowY
	 * @param show
	 */
	self.setShowY = function (show) {
	  	if(mShowY != show) {
	  		mShowY = show;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getShowY
	 * @return
	 */
	self.getShowY = function() { 
		return mShowY; 
	}
	  
	/**
	 * @method getTicksYMode
	 * @return
	 */
	self.getTicksYMode = function() { 
		return mTicksYMode; 
	}

	/**
	* Set the line color of the element
	* @param color a stroke style
	*/
  	self.setLineColorX = function(color) { 
	    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
	    if (color!=mLineColorX) {
	      mLineColorX = color; 
		  self.setChanged(true);
	    }
	    return self;
  	};
    
	/**
	* Get the line color
	*/
	self.getLineColorX = function() { 
    	return mLineColorX; 
  	};

	/**
	* Set the line width of the element
	* @param width a stroke width (may be double, such as 0.5, the default)
	*/
	self.setLineWidthX = function(width) { 
	    if (width!=mLineWidthX) {
	      mLineWidthX = width; 
		  self.setChanged(true);
	    }
	};

	/**
	* Get the line width
	*/
  	self.getLineWidthX = function() { 
  		return mLineWidthX; 
  	};

	/**
	* Sets shape rendering
	*/
	self.setShapeRenderingX = function(rendering) {
	    if (rendering.substring(0,6) == "RENDER") rendering = EJSS_DRAWING2D.Style[rendering.toUpperCase()];
	    if (mShapeRenderingX != rendering) {
	      mShapeRenderingX = rendering;
		  self.setChanged(true);
	    }
	};
  
  	self.getShapeRenderingX = function() { 
    	return mShapeRenderingX;
  	};

	/**
	* Set the line color of the element
	* @param color a stroke style
	*/
  	self.setLineColorY = function(color) { 
	    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
	    if (color!=mLineColorY) {
	      mLineColorY = color; 
		  self.setChanged(true);
	    }
	    return self;
  	};
    
	/**
	* Get the line color
	*/
	self.getLineColorY = function() { 
    	return mLineColorY; 
  	};

	/**
	* Set the line width of the element
	* @param width a stroke width (may be double, such as 0.5, the default)
	*/
	self.setLineWidthY = function(width) { 
	    if (width!=mLineWidthY) {
	      mLineWidthY = width; 
		  self.setChanged(true);
	    }
	};

	/**
	* Get the line width
	*/
  	self.getLineWidthY = function() { 
  		return mLineWidthY; 
  	};

	/**
	* Sets shape rendering
	*/
	self.setShapeRenderingY = function(rendering) {
	    if (rendering.substring(0,6) == "RENDER") rendering = EJSS_DRAWING2D.Style[rendering.toUpperCase()];
	    if (mShapeRenderingY != rendering) {
	      mShapeRenderingY = rendering;
		  self.setChanged(true);
	    }
	};
  
  	self.getShapeRenderingY = function() { 
    	return mShapeRenderingY;
  	};

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.Grid.registerProperties(self, controller);
	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	return self;
};

