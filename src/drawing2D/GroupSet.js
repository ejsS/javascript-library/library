/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//GroupSet
//---------------------------------

/**
 * GroupSet
 * @class GroupSet 
 * @constructor  
 */
EJSS_DRAWING2D.GroupSet = {
    
};


/**
 * Creates a set of Segments
 * @method groupSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.groupSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.group, mName);

  return self;
};
