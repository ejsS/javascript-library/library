/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//SegmentSet
//---------------------------------

/**
 * SegmentSet
 * @class SegmentSet 
 * @constructor  
 */
EJSS_DRAWING2D.SegmentSet = {
    
    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING2D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("Offset", 
          function(v) { set.setToEach(function(element,value) { element.setRelativePosition(value); }, v); }
      );
    }
    
};


/**
 * Creates a set of Segments
 * @method segmentSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.segmentSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.segment, mName);

  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.SegmentSet.registerProperties(self,controller);
  };
  
  return self;
};