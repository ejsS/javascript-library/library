/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Group
 * @class Group 
 * @constructor  
 */
EJSS_DRAWING2D.Group = {

};

/**
 * Creates a group
 * @method group
 */
EJSS_DRAWING2D.group = function (name) {
  var self = EJSS_DRAWING2D.element(name);
  
//  var mChildren = [];

  self.getClass = function() {
  	return "ElementGroup";
  }
  
  self.isGroup = function() {
  	return true;
  }
  
//  self.addChild = function(child) {
//	  EJSS_TOOLS.addToArray(mChildren,child);
//  }
//  
//  self.removeChild = function(child) {
//	  EJSS_TOOLS.removeFromArray(mChildren,child);
//  }
//  
//  self.superSetChanged = self.setChanged;
//  
//  self.setChanged = function(changed) {
//	self.superSetChanged(changed);
//	if (changed) for (var i=0,n=mChildren.length; i<n; i++) mChildren[i].setChanged(changed);
//  }
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
};



