/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * InfoText
 * @class InfoText 
 * @constructor  
 */
EJSS_DRAWING2D.InfoText = {

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Text.registerProperties(element, controller);
		// super class
		
		controller.registerProperty("Info",  element.setInfo);
		controller.registerProperty("Format",  element.setFormat);		
	},
};

/**
 * Creates a 2D InfoText
 * @method infoText
 */
EJSS_DRAWING2D.infoText = function(name) {
	var self = EJSS_DRAWING2D.text(name);
	var super_getText = self.getText;
	
	var mInfo;
	var mFormat;			// replace ".^", ".##" por los números pasados

	/**
	 * Set info function or element with getInfo function
	 */
	self.setInfo = function(info) {
		mInfo = info;
	}

	self.getText = function() {
		if(typeof mInfo != "undefined") {	// exits info function 
			if(mInfo.getInfo) 
				return mInfo.getInfo();
			else if (typeof mInfo == "function") 
				return mInfo();
		} else { // uses text with the format
			var text = super_getText();			
			// process text
			return fillString(mFormat,text);
		}
	}

	self.setFormat = function(format) {
		mFormat = format;
	}

	self.isChanged = function() {
		return true;
	}

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.InfoText.registerProperties(self, controller);
	};

	/*
	 * Fill formatted string
	 */
	function fillString(format, text) {
		var t = "";
		var txt = format;
		var numbers = format.match(/[0-9]*\.(\^|#)+/g);
		if (numbers==null) return text;
		if(!Array.isArray(text)) {
			text = JSON.parse("["+text+"]");
		}

		for (var i=0; i<numbers.length; i++) {
			var precision = numbers[i].split("#").length - 1;
			// check text
			if(isNaN(text[i])) 
				t = text[i];
			else
				t = text[i].toFixed(precision)
			txt = txt.replace(numbers[i], t);
		}
		
		return txt;		
	}
	
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	return self;
};

