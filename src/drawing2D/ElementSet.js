/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * ElementSet
 * @class ElementSet 
 * @constructor  
 */
EJSS_DRAWING2D.ElementSet = {
    
    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * Registers properties in a ControlElement
     * @method 
     * @param element The element with the properties
     * @param controller A ControlElement that becomes the element controller
     */
    registerProperties : function(set,controller) {
      var TARGET_POSITION = EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION;
      var TARGET_SIZE = EJSS_DRAWING2D.PanelInteraction.TARGET_SIZE;

      set.setController(controller); // remember it, in case you change the number of elements
      set.setToEach(function(element,value) { element.setController(value); }, controller); // make all elements in the set report to the same controller

      controller.registerProperty("Parent", 
      	  function(panel) {
      	  	set.setParent(panel); 
      		set.setToEach(function(element,value) { element.setParent(value); }, set); 
      	  } );

      controller.registerProperty("NumberOfElements", set.setNumberOfElements);

      controller.registerProperty("ElementInteracted", set.setElementInteracted, set.getElementInteracted);

//      controller.registerProperty("x",function(v) { set.foreach("setX",v); }, function() { return set.getall("getX"); });
      controller.registerProperty("X",
          function(v) { set.setToEach(function(element,value) { element.setX(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getX(); } ); }      
          );
      controller.registerProperty("Y",
          function(v) { set.setToEach(function(element,value) { element.setY(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getY(); } ); }      
      );
      controller.registerProperty("Position",
          function(v) { set.setToEach(function(element,value) { element.setPosition(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getPosition(); } ); }      
      );
      controller.registerProperty("PixelPosition", 
          function(v) { set.setToEach(function(element,value) { element.setPixelPosition(value); }, v); }
      );

      controller.registerProperty("Diameter",
          function(v) { set.setToEach(function(element,value) { element.setSize([value,value]); }, v); }
          );
      controller.registerProperty("Radius",
          function(v) { set.setToEach(function(element,value) { element.setSize([value*2,value*2]); }, v); }
          );
      controller.registerProperty("SizeX",
          function(v) { set.setToEach(function(element,value) { element.setSizeX(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getSizeX(); } ); }      
          );
      controller.registerProperty("SizeY",
          function(v) { set.setToEach(function(element,value) { element.setSizeY(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getSizeY(); } ); }      
      );
      controller.registerProperty("Size",
          function(v) { set.setToEach(function(element,value) { element.setSize(value); }, v); },
          function()  { return set.getFromEach(function(element) { return element.getSize(); } ); }      
      );
      controller.registerProperty("PixelSize", 
          function(v) { set.setToEach(function(element,value) { element.setPixelSize(value); }, v); }
      );

      controller.registerProperty("Bounds",function(bounds) {
        var setBounds = function(element,bound) {
          element.setX(bound[0]);
          element.setY(bound[2]); 
          element.setSizeX(bound[1]-bound[0]);
          element.setSizeY(bound[3]-bound[2]);
        };
        set.setToEach(setBounds, bounds);
      },
      function() {
        var getBounds = function(element) {
          return [element.getX(),element.getX()+element.getSizeX(),
                  element.getY(),element.getY()+element.getSizeY()]; 
        };
        return set.getFromEach(getBounds);
      });

      controller.registerProperty("Visibility", 
          function(v) { set.setToEach(function(element,value) { element.setVisible(value); }, v); }
      );
      controller.registerProperty("Measured", 
          function(v) { set.setToEach(function(element,value) { element.setMeasured(value); }, v); }
      );

      controller.registerProperty("Transformation",
      	  function(v) { set.setToEach(function(element,value) { element.setTransformation(value); }, v);
      });

      controller.registerProperty("Rotate",
      	  function(v) { set.setToEach(function(element,value) { element.setRotate(value); }, v);
      });

      controller.registerProperty("ScaleX",
      	  function(v) { set.setToEach(function(element,value) { element.setScaleX(value); }, v);
      });

      controller.registerProperty("ScaleY",
      	  function(v) { set.setToEach(function(element,value) { element.setScaleY(value); }, v);
      });

      controller.registerProperty("SkewX",
      	  function(v) { set.setToEach(function(element,value) { element.setSkewX(value); }, v);
      });

      controller.registerProperty("SkewY",
      	  function(v) { set.setToEach(function(element,value) { element.setSkewY(value); }, v);
      });

      controller.registerProperty("RelativePosition", 
          function(v) { set.setToEach(function(element,value) { element.setRelativePosition(value); }, v); }
      );

      controller.registerProperty("LineColor", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setLineColor(value); }, v); }
      );
      controller.registerProperty("LineWidth", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setLineWidth(value); }, v); }
      );
      controller.registerProperty("DrawLines", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setDrawLines(value); }, v); }
      );
      controller.registerProperty("FillColor", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setFillColor(value); }, v); }
      );
      controller.registerProperty("DrawFill", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setDrawFill(value); }, v); }
      );
      controller.registerProperty("Attributes", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setAttributes(value); }, v); }
      );
      controller.registerProperty("ShapeRendering", 
          function(v) { set.setToEach(function(element,value) { element.getStyle().setShapeRendering(value); }, v); }
      );

      controller.registerProperty("EnabledPosition", 
          function(v) { set.setToEach(function(element,value) { element.getInteractionTarget(TARGET_POSITION).setMotionEnabled(value); }, v); }
      );
      controller.registerProperty("MovesGroup", 
          function(v) { set.setToEach(function(element,value) { element.getInteractionTarget(TARGET_POSITION).setAffectsGroup(value); }, v); }
      );
      controller.registerProperty("EnabledSize", 
          function(v) { set.setToEach(function(element,value) { element.getInteractionTarget(TARGET_SIZE).setMotionEnabled(value); }, v); }
      );
      controller.registerProperty("ResizesGroup", 
          function(v) { set.setToEach(function(element,value) { element.getInteractionTarget(TARGET_SIZE).setAffectsGroup(value); }, v); }
      );
      controller.registerProperty("Sensitivity", 
          function(v) { set.setToEach(function(element,value) { 
            element.getInteractionTarget(TARGET_POSITION).setSensitivity(value); 
            element.getInteractionTarget(TARGET_SIZE).setSensitivity(value); 
          }, v); }
      );

      var dataFunction = function() { 
        //var index = set.getGroupPanel().getPanelInteraction().getIndexElement();
        //set.setElementInteracted(index);
        //return { index: index, position: set.getElements()[index].getPosition() };
        var panel = set.getGroupPanel();
        var index = panel.getPanelInteraction().getIndexElement();
        var element = panel.getElements()[index];
        var elementIndex = element.getSetIndex(); 
        set.setElementInteracted(elementIndex);
  	    controller.propertiesChanged("ElementInteracted");
	    //controller.reportInteractions();	 
        return { index: elementIndex, position: element.getPosition() };   
      };
      
      // Actions
      controller.registerAction("OnDoubleClick",   dataFunction);
      controller.registerAction("OnEnter",   dataFunction);
      controller.registerAction("OnExit",    dataFunction);
      controller.registerAction("OnPress",   dataFunction);
      controller.registerAction("OnDrag",    dataFunction);
      controller.registerAction("OnRelease", dataFunction);
    }
};

/**
 * Element set
 * Creates a basic abstract ElementSet
 * @method elementSet
 * @param mConstructor the function that creates new elements (will be used as element = mConstructor(name))
 * @returns An abstract 2D element set
 */
EJSS_DRAWING2D.elementSet = function (mConstructor, mName) {  
  var self = EJSS_DRAWING2D.group(mName);

  // Static references
  var ElementSet = EJSS_DRAWING2D.ElementSet;		// reference for ElementSet
  
  // Configuration variables
  var mElementList = []; 
  var mNumberOfElementsSet = false;

  // Implementation variables  
  var mElementInteracted = -1;

  // Last list of removed elements
  var mLastElementList = [];

  // ----------------------------------------
  // Configuration methods
  // ----------------------------------------

  /**
   * Sets the number of element of this set
   * @method setNumberOfElements
   * @param numberOfElements the number of elements, must be >= 1
   */
  self.setNumberOfElements = function(numberOfElements) {
	mNumberOfElementsSet = true;
	adjustNumberOfElements(numberOfElements);
  };

  /*
   * Adjusts the number of element of this set
   * @method adjustNumberOfElements
   * @param numberOfElements the number of elements, must be >= 1
   */
  function adjustNumberOfElements(numberOfElements) {
    // keep original settings for the new elements
    var name = self.getName ? self.getName() : "unnamed";
    numberOfElements = Math.max(1,numberOfElements);
    var diff = mElementList.length-numberOfElements;
    if (diff > 0) {
    	mLastElementList = mElementList.splice(numberOfElements, diff);
		for (var j = 0; j < mLastElementList.length; j++) { 
			var panel = mLastElementList[j].getPanel(); // remove element from panel
			if(panel) panel.removeElement(mLastElementList[j])
		} 
    } else if (diff < 0) {
    	mLastElementList = [];
    	var controller = self.getController();
    	var oldElement = mElementList[mElementList.length-1];
		for (var i = mElementList.length; i < numberOfElements; i++) {
			var element = mConstructor(name+"["+i+"]"); // new element			
			element.setSet(self,i);
			oldElement.copyTo(element);
			element.setController(controller);
		  	mElementList.push(element);	
		} 
    }
  };

  /**
   * Returns the array with all elements in the set
   * @method getElements
   * @return array of Elements
   */
  self.getElements = function() {
    return mElementList;
  };

  /**
   * Returns the element in the set at the given index
   * @method getElement
   * @return Element
   */
  self.getElement = function(index) {
    return mElementList[index];
  };
  
  /**
   * Returns last list of removed elements and reset the value
   * @method getLastElements
   * @return last list of Elements
   */
  self.getLastElements = function() {
  	var ret = mLastElementList.slice();
  	mLastElementList = [];
    return ret;
  };
  
  self.setElementInteracted = function(index) {
    mElementInteracted = index;
  };

  self.getElementInteracted = function() {
    return mElementInteracted;
  };
  
  // ----------------------------------------
  // Relation to its panel
  // ----------------------------------------
  
  var super_setPanel = self.setPanel;

  self.setPanel = function(panel) {
    super_setPanel(panel);
    for (var i=0,n=mElementList.length;i<n;i++) mElementList[i].setPanel(panel);
  };

  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------

  /**
   * Applies to each element in the set the function f with argument v, or v[i], if it is an array
   * @method setToEach
   * @param f function
   * @param v arguments
   */
  self.setToEach = function(f,v) {
    if (Array.isArray(v)) {
      if (!mNumberOfElementsSet) { 
        if (mElementList.length < v.length) adjustNumberOfElements(v.length);
      }
      for (var i=0,n=Math.min(mElementList.length,v.length);i<n;i++) f(mElementList[i],v[i]);
    }
    else {
      for (var i=0,n=mElementList.length;i<n;i++) f(mElementList[i],v);
    }
  };

  /**
   * Returns an array with the result of applying the function to each element of the set
   * @method getFromEach
   * @param f function
   * @return f function return 
   */
  self.getFromEach = function(f) {
    var value = [];
    for (var i=0, n=mElementList.length;i<n;i++) value[i] = f(mElementList[i]);
    return value;
  };

  /**
   * Registers properties in a ControlElement
   * @method registerProperties
   * @param controller A ControlElement that becomes the element controller
   */
  self.registerProperties = function(controller) {
    ElementSet.registerProperties(self,controller);
  };
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
	
  var element = mConstructor(mName + "[0]"); // new element			
  element.setController(self.getController());
  element.setSet(self,0);
  mElementList.push(element);
  	
  return self;
};
