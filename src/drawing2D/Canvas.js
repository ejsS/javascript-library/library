/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Canvas
 * @class Canvas 
 * @constructor  
 */
EJSS_DRAWING2D.Canvas = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy
  	
  		dest.setDimensions(source.getDimensions());
  	},


    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

		controller.registerProperty("Dimensions", element.setDimensions, element.getDimensions);

    },
};

/**
 * Creates a 2D Segment
 * @method Canvas
 */
EJSS_DRAWING2D.canvas = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  var mDrawables = [];
  var mxMin = -10;
  var mxMax = 10;
  var myMin = -10;
  var myMax = 10;

  self.getClass = function() {
  	return "ElementCanvas";
  };

  // xMin, xMax, yMin, yMax
  self.setDimensions = function(dimensions) {
  	xMin = dimensions[0];
  	xMax = dimensions[1];
  	yMin = dimensions[2];
  	yMax = dimensions[3];

  	self.setChanged(true);
  }
  
  self.getDimensions = function() {
  	return [xMin,xMax,yMin,yMax];
  }

  self.addDrawable = function(drawable) {
    mDrawables.push(drawable);
  	self.setChanged(true);
  }

  self.addImageField = function(data, xMin, xMax, xPoints, yMin, yMax, yPoints, autoscale, zMin, zMax) {
  	var imgField = function() {
  	  var shape = {};
  	  shape.imageField = true;
	  shape.data = data || undefined;
	  shape.xMin = xMin === undefined ? -5: xMin;
	  shape.xMax = xMax === undefined ? 5: xMax;
	  shape.nx = xPoints || 128;
	  shape.yMin = yMin === undefined ? -5: yMin;
	  shape.yMax = yMax === undefined ? 5: yMax;
	  shape.ny = yPoints || 128;
	  shape.autoscale = autoscale || true;
	  shape.lower = zMin === undefined ? -1: zMin;
	  shape.upper = zMax === undefined ? 1: zMax;
	  shape.center = (shape.lower+shape.upper)/2;  // usually zero
	  shape.maskRadius=0;  // no circular mask if radius=0

	  function arrayCoordinates(min, max, points) {
	    var coordinates =[];
	    var step = (max - min) /(points -1)
	    var val = min;
	    for (var i = 0; i < points; i++) {
	        //pushes in each of the x values
	        coordinates.push(val);
	        val += step;
	    }
	    return coordinates;
	  }
	 
	  shape.xPos = arrayCoordinates(shape.xMin, shape.xMax, shape.nx);
	  shape.yPos = arrayCoordinates(shape.yMin, shape.yMax, shape.ny); 	  
	 
	  shape.updateData = function (newData) {
	    //updates the contour lines if data are changed
	    shape.data = newData || shape.data;
	    shape.xPos = arrayCoordinates(shape.xMin, shape.xMax, shape.nx);
	    shape.yPos = arrayCoordinates(shape.yMin, shape.yMax, shape.ny);
	    
	    self.setChanged(true);
	  }
	
	  shape.setThreshold = function (lower, upper) {
	    shape.lower = lower === undefined ? shape.lower: lower;
	    shape.upper = upper === undefined ? shape.upper: upper;
	    
	    self.setChanged(true);
	  }
	  
	  // Draw a curcular image
	  shape.setCircularMask = function (radius) {
	    shape.maskRadius = radius;
	    
	    self.setChanged(true);
	  }
 
 	  return shape;
  	}();
  	
  	self.addDrawable(imgField);
  	return imgField;
  }
 
  self.getDrawables = function() {
  	return mDrawables;
  }

  this.clearObjects = function () {
    mDrawables =[];
  	self.setChanged(true);
  }
  
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Canvas.registerProperties(self,controller);
  };
  
  self.copyTo = function(element) {
    EJSS_DRAWING2D.Canvas.copyTo(self,element);
  };
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};



