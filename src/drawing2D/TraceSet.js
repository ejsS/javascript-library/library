/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//TraceSet
//---------------------------------

/**
 * TraceSet
 * @class TraceSet 
 * @constructor  
 */
EJSS_DRAWING2D.TraceSet = {
  
    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var TrailSet = EJSS_DRAWING2D.TrailSet;
      
      TrailSet.registerProperties(set,controller);
      
	 /*** 
	  * Type of marker to draw
	  * @property MarkType 
	  * @type int|String
	  * @values 0="ELLIPSE", 1="RECTANGLE", 2:"AREA", 3:"BAR" 
	  * @default "ELLIPSE" 
	  */ 
	  controller.registerProperty("MarkType", 
          function(v) { set.setToEach(function(element,value) { element.setMarkType(value); }, v); }
      );
      
	 /*** 
	  * Size of the marker to draw
	  * @property MarkSize 
	  * @type int[2] providing the width and height in pixels
	  * @default [0,0] 
	  */ 
	  controller.registerProperty("MarkSize", 
          function(v) { set.setToEach(function(element,value) { element.setMarkSize(value); }, v); }
      );           
      
	 /*** 
	  * Color for the lines of the markers
	  * @property MarkLineColor
	  * @type String
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Black"
	  */ 
	  controller.registerProperty("MarkLineColor", 
          function(v) { set.setToEach(function(element,value) { element.getMarkStyle().setLineColor(value); }, v); }
      );   
      
      /*** 
	  * Marker stroke width
	  * @property MarkLineWidth 
	  * @type double
	  * @default 0.5
	  */         
      controller.registerProperty("MarkLineWidth", 
          function(v) { set.setToEach(function(element,value) { element.getMarkStyle().setLineWidth(value); }, v); }
      );           
      
      /*** 
	  * Whether the marker lines are drawn
	  * @property MarkDrawLines 
	  * @type boolean
	  * @default true 
	  */
      controller.registerProperty("MarkDrawLines", 
          function(v) { set.setToEach(function(element,value) { element.getMarkStyle().setDrawLines(value); }, v); }
      );           
      
      /*** 
	  * The fill color for the markers
	  * @property MarkFillColor 
	  * @type String
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Blue"
	  */
	  controller.registerProperty("MarkFillColor", 
          function(v) { set.setToEach(function(element,value) { element.getMarkStyle().setFillColor(value); }, v); }
      );           
      
      /*** 
	  * Whether the marker are filled
	  * @property MarkDrawFill 
	  * @type boolean
	  * @default true
	  */
	  controller.registerProperty("MarkDrawFill", 
          function(v) { set.setToEach(function(element,value) { element.getMarkStyle().setDrawFill(value); }, v); }
      );           
      
      /*** 
	  * Position of the marker relative to the point 
	  * @property MarkRelativePosition 
	  * @type int|String
	  * @values "CENTER":0,"NORTH":1,"SOUTH":2,"EAST":3,"WEST":4,"NORTH_EAST":5,"NORTH_WEST":6,
	  * "SOUTH_EAST":7,"SOUTH_WEST":8 
	  * @default "CENTER
	  */
	  controller.registerProperty("MarkRelativePosition", 
          function(v) { set.setToEach(function(element,value) { element.setMarkRelativePosition(value); }, v); }
      );           
    }    
};


/**
 * Creates a set of Segments
 * @method traceSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.traceSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.trace, mName);

  // Static references
  var TraceSet = EJSS_DRAWING2D.TraceSet;		// reference for TraceSet
  
  self.registerProperties = function(controller) {
    TraceSet.registerProperties(self,controller);
  };

  return self;
};