/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for interface element.
 * @module Drawing2D 
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Knob
 * @class Knob 
 * @constructor  
 */
EJSS_DRAWING2D.KnobOld = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
	EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

	controller.registerProperty("Value", element.setValue, element.getValue);
    controller.registerProperty("Minimum", element.setMinimum, element.getMinimum);
    controller.registerProperty("Maximum", element.setMaximum, element.getMaximum);

    controller.registerAction("OnPress" ,element.getValue); 
    controller.registerAction("OnRelease" ,element.getValue); 
    controller.registerAction("OnChange" ,element.getValue); 
    
    controller.registerProperty("Size", element.setSize);
	
  }

};

/**
 * Knob function
 * Creates a 2D element that looks and behaves like a knob
 * @method knob
 * @param mName the name of the element
 * @returns An abstract interface element
 */
EJSS_DRAWING2D.knobOld = function (mName) {
  var self = EJSS_DRAWING2D.group(mName);
  
  // Configuration variables
  var mValue = 0.0;
  var mMaximum =  1.0;
  var mMinimum = -1.0;

  var mNumberOfMarks = 21;
  var mBigMark = 4;
  var mMediumMark = 2;
  var mDigits = 0;

  var mMinimumAngle = Math.PI/2 + 1.5*Math.PI/2;
  var mMaximumAngle = Math.PI/2 - 1.5*Math.PI/2;

  var mMinAngle = -Math.PI*0.7;
  var mMaxAngle =  Math.PI*0.7;
  var mThreshold = Math.PI/5;

  // Implementation variables
  var mKnobExterior,mKnobMiddle,mKnobInterior;
  var mSegments, mTexts, mBrand, mUnits;

  var mDummyContainer;
  var mSVGDefs = 
	  "<svg xmlns='http://www.w3.org/2000/svg' version='1.1'>"+
      "  <defs>"+
      "    <linearGradient id='KnobExteriorGradient' x1='0%' y1='0%' x2='100%' y2='100%'>"+
      "      <stop offset='0%'   stop-color='rgb(250,250,250)' />"+
      "      <stop offset='100%' stop-color='rgb(100,100,100)' />"+
      "    </linearGradient>"+
      "    <linearGradient id='KnobMiddleGradient' x1='0%' y1='0%' x2='100%' y2='100%'>"+
      "      <stop offset='0%'   stop-color='rgb(200,200,200)' />"+
      "      <stop offset='100%' stop-color='rgb(150,150,150)' />"+
      "    </linearGradient>"+
      "    <linearGradient id='KnobInteriorGradient' x1='0%' y1='0%' x2='100%' y2='100%'>"+
      "      <stop offset='0%'   stop-color='rgb(127,127,127)' />"+
      "      <stop offset='100%' stop-color='rgb(250,250,250)' />"+
      "    </linearGradient>"+
      "    <filter id='KnobFilter' x='0' y='0' width='200%' height='200%'>"+
      "      <feOffset result='offOut' in='SourceGraphic' dx='1' dy='1' />"+
      "      <feGaussianBlur result='blurOut' in='offOut' stdDeviation='2' />"+
      "      <feBlend in='SourceGraphic' in2='blurOut' mode='normal' />"+
      "    </filter>"+
      "  </defs>"+
      "</svg>";

  
  // ----------------------------------------------------
  // Properties
  // ----------------------------------------------------
  
  /**
   * Sets the value of the element. 
   * @method setValue
   * @param value
   */
  self.setValue = function(value) {
    if (mValue!=value) {
    	mValue = value;
    	adjustPosition();
    }
  }
  
  /**
   * @method getValue
   * @return current value
   */
  self.getValue = function() { 
    return mValue; 
  };    

  /**
   * Sets the minimum value of the element. 
   * @method setMinimum
   * @param value
   */
  self.setMinimum = function(value) {
    if (mMinimum!=value) {
    	mMinimum = value;
    	adjustPosition();
    }
  }
  
  /**
   * @method getMinimum
   * @return current value
   */
  self.getMinimum = function() { 
    return mMinimum; 
  };    

  /**
   * Sets the minimum value of the element. 
   * @method setMaximum
   * @param value
   */
  self.setMaximum = function(value) {
	if (mMaximum!=value) {
		mMaximum = value;
		adjustPosition();		  
	}
  }
  
  /**
   * @method getMaximum
   * @return current value
   */
  self.getMaximum = function() { 
    return mMaximum; 
  };    

  /**
   * Sets the size of the element. 
   * @method setSize
   * @param value
   */
  self.setSize = function(value) {
	  self.setSizeX(value);
	  self.setSizeY(value);
  }
  
  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
	createKnob();
	EJSS_DRAWING2D.Knob.registerProperties(self,controller);
  };
  
  self.superSetParent = self.setParent;

  self.setParent = function(parent, sibling) {
		self.superSetParent(parent,sibling);
		mKnobExterior.setParent(self);
		mKnobMiddle.setParent(self);
		mKnobInterior.setParent(self);
	  }

  
  function createKnob() {
	  /*
	  var stop1 = document.createElementNS("http://www.w3.org/2000/svg", "stop");
	  stop1.setAttribute("offset","0%");
	  stop1.setAttribute("stop-color","rgb(250,250,250)");

	  var stop2 = document.createElementNS("http://www.w3.org/2000/svg", "stop");
	  stop2.setAttribute("offset","100%");
	  stop2.setAttribute("stop-color","rgb(100,100,100)");

	  var linearGradient = document.createElementNS("http://www.w3.org/2000/svg", "linearGradient");
	  linearGradient.setAttribute("id", "#KnobExteriorGradient");
	  linearGradient.setAttribute("x1","0%");
	  linearGradient.setAttribute("y1","0%");
	  linearGradient.setAttribute("x2","100%");
	  linearGradient.setAttribute("y2","100%");
	  linearGradient.appendChild(stop1);
	  linearGradient.appendChild(stop2);

	  var feOffset = document.createElementNS("http://www.w3.org/2000/svg", "feOffset");
	  feOffset.setAttribute("result","offOut");
	  feOffset.setAttribute("in","SourceGraphic");
	  feOffset.setAttribute("dx","1");
	  feOffset.setAttribute("dy","1");

	  var gaussianFilter = document.createElementNS("http://www.w3.org/2000/svg", "feGaussianBlur");
	  gaussianFilter.setAttribute("result","blurOut");
	  gaussianFilter.setAttribute("in","offOut");
	  gaussianFilter.setAttribute("stdDeviation","2");

	  var feBlend = document.createElementNS("http://www.w3.org/2000/svg", "feBlend");
	  feOffset.setAttribute("in","SourceGraphic");
	  feOffset.setAttribute("in2","blurOut");
	  feOffset.setAttribute("mode","normal");

	  var filter = document.createElementNS("http://www.w3.org/2000/svg", "filter");
	  filter.setAttribute("id","#KnobFilter");
	  filter.setAttribute("x","0");
	  filter.setAttribute("y","0");
	  filter.setAttribute("width","200%");
	  filter.setAttribute("height","200%");
	  filter.appendChild(feOffset);
	  filter.appendChild(gaussianFilter);
	  filter.appendChild(feBlend);
	  
	  /*
	  var defs = self.getParentPanel()getGraphics().getDefs(); // document.createElementNS("http://www.w3.org/2000/svg", "defs");
	  defs.appendChild(linearGradient);
	  defs.appendChild(filter);
	  
	 // self.getGraphics().appendChild(defs);
	  */
	  
	  mDummyContainer = document.createElement('div');
	  document.body.appendChild(mDummyContainer);
	  mDummyContainer.innerHTML = mSVGDefs;
	  
	  mKnobExterior = EJSS_DRAWING2D.shape(mName+".exterior_shape");
	  mKnobExterior.setSize([1,1]);
	  mKnobExterior.getStyle().setDrawLines(false);
	  mKnobExterior.getStyle().setAttributes({ fill: "url(#KnobExteriorGradient)", filter:"url(#KnobFilter)"});

	  mKnobMiddle = EJSS_DRAWING2D.shape(mName+".middle_shape");
	  mKnobMiddle.setSize([0.9,0.9]);
	  mKnobMiddle.getStyle().setDrawLines(false);
	  mKnobMiddle.getStyle().setAttributes({ fill: "url(#KnobMiddleGradient)"});

	  mKnobInterior = EJSS_CORE.promoteToControlElement(
			  EJSS_DRAWING2D.shape(mName+".inner_shape"),self.getView(),mName+".inner_shape");
	  mKnobInterior.setY(0.3);
	  mKnobInterior.setSize([0.2,0.2]);
	  mKnobInterior.getStyle().setDrawLines(false);
	  mKnobInterior.getStyle().setAttributes({ fill: "url(#KnobInteriorGradient)"});

	  mKnobInterior.setProperty("EnabledPosition","ENABLED_NO_MOVE");
      mKnobInterior.setProperty("Sensitivity",10);
	  mKnobInterior.setProperty("OnDrag",dragged);
	  mKnobInterior.setProperty("OnPress",pressed);
	  mKnobInterior.setProperty("OnRelease",released);

//	  self.addElement(mKnobExterior);
//	  self.addElement(mKnobMiddle);
//	  self.addElement(mKnobInterior);


  }
    
  function adjustPosition() {
	  mValue = Math.max(Math.min(mMaximum,mValue),mMinimum);
	  var angle = mMaxAngle - (mValue-mMinimum)/(mMaximum-mMinimum)*(mMaxAngle-mMinAngle) + Math.PI/2;
	  mKnobInterior.setPosition([0.3*Math.cos(angle),0.3*Math.sin(angle)]);
  }

  function pressed(point,info) {
	  var controller = self.getController();    		
	  if (controller) controller.invokeImmediateAction("OnPress");
  }

  function released(point,info) {
	  var controller = self.getController();    		
	  if (controller) controller.invokeImmediateAction("OnRelease");
  }

  function dragged(point,info) {
	  var point = info.point;
	  var element = info.element;
	  var group_pos = self.getAbsolutePosition(true);
	  point[0] -= group_pos[0];
	  point[1] -= group_pos[1];
	  var angle = mMaxAngle - (mValue-mMinimum)/(mMaximum-mMinimum)*(mMaxAngle-mMinAngle);
	  var rotation = Math.atan2(-point[0],point[1]);
	  if (Math.abs(rotation-angle)>mThreshold) return;
	  rotation = Math.min(Math.max(rotation,mMinAngle),mMaxAngle);
	  mValue = mMaximum - (rotation-mMinAngle)/(mMaxAngle-mMinAngle)*(mMaximum-mMinimum);
	  var angle = rotation+Math.PI/2;
	  element.setPosition([0.3*Math.cos(angle),0.3*Math.sin(angle)]);
	  var controller = self.getController();    		
	  if (controller) {
		controller.immediatePropertyChanged("Value");
		controller.invokeImmediateAction("OnChange");
	  }	        

  }
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  return self;
};

