/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A Custom element is a 2D drawing element that executes a user function for drawing 
 * @class EJSS_DRAWING2D.Custom
 * @parent EJSS_DRAWING2D.Element
 * @constructor  
 */
EJSS_DRAWING2D.Custom = {

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class
    },  


};

/**
 * Creates a Custom 2D Element
 * @method custom
 */
EJSS_DRAWING2D.custom = function (name) {
  var self = EJSS_DRAWING2D.element(name);
  var mFunction;
  
  self.getClass = function() {
  	return "ElementCustom";
  };
  
    self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Custom.registerProperties(self,controller);
  };


  /**
  * A function to call when the element must draw.
  * The function will receive the graphic context of the panel in which it draws
  */
  self.setFunction = function(customFunction) {
    mFunction = customFunction;
  };
  
  self.getFunction = function() {
    return mFunction;
  };
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1]);

  return self;
};



