/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A Trace is a sophistication of the Trail element. 
 * The extra features are the possibility to specify markers that will be displayed at 
 * each point of the trace.
 * @class EJSS_DRAWING2D.Trace 
 * @parent EJSS_DRAWING2D.Trail
 * @constructor  
 */
EJSS_DRAWING2D.Trace = {
  ELLIPSE : 0, 	
  RECTANGLE : 1,	
  AREA : 2,	
  BAR : 3,

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy  	
  		EJSS_DRAWING2D.Trail.copyTo(source,dest);
  		EJSS_DRAWING2D.Style.copyTo(source.getMarkStyle(),dest.getMarkStyle());
  		
		dest.setMarkType(source.getMarkType());
		dest.setMarkSize(source.getMarkSize());
  	},
  	
	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Trail.registerProperties(element, controller);
		// super class

	 /*** 
	  * Type of marker to draw
	  * @property MarkType 
	  * @type int|String
	  * @values 0="ELLIPSE", 1="RECTANGLE", 2:"AREA", 3:"BAR" 
	  * @default "ELLIPSE" 
	  */ 
	  controller.registerProperty("MarkType", element.setMarkType, element.getMarkType);

	 /*** 
	  * Axis for bar or area markers
	  * @property MarkAxisY 
	  * @type int
	  * @default 0 
	  */ 
	  controller.registerProperty("MarkAxisY", element.setMarkAxisY, element.getMarkAxisY);

	 /*** 
	  * Size of the marker to draw
	  * @property MarkSize 
	  * @type int[2] providing the width and height in pixels
	  * @default [0,0] 
	  */ 
		controller.registerProperty("MarkSize", element.setMarkSize, element.getMarkSize);

	 /*** 
	  * Color for the lines of the markers
	  * @property MarkLineColor
	  * @type String
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Black"
	  */ 
      	controller.registerProperty("MarkLineColor",  element.getMarkStyle().setLineColor);

	 /*** 
	  * Marker stroke width
	  * @property MarkLineWidth 
	  * @type double
	  * @default 0.5
	  */ 
      	controller.registerProperty("MarkLineWidth",  element.getMarkStyle().setLineWidth);

	 /*** 
	  * Whether the marker lines are drawn
	  * @property MarkDrawLines 
	  * @type boolean
	  * @default true 
	  */ 
      	controller.registerProperty("MarkDrawLines",  element.getMarkStyle().setDrawLines);

	 /*** 
	  * The fill color for the markers
	  * @property MarkFillColor 
	  * @type String
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Blue"
	  */ 
      	controller.registerProperty("MarkFillColor",  element.getMarkStyle().setFillColor);

	 /*** 
	  * Whether the marker are filled
	  * @property MarkDrawFill 
	  * @type boolean
	  * @default true
	  */ 
      	controller.registerProperty("MarkDrawFill",   element.getMarkStyle().setDrawFill);

	}

};

/**
 * Creates a 2D Trace
 * @method trace
 */
EJSS_DRAWING2D.trace = function(mName) {
	var self = EJSS_DRAWING2D.trail(mName);
 
    // Instance variables
	var mMarkType = EJSS_DRAWING2D.Trace.ELLIPSE;
    var mMarkStyle = EJSS_DRAWING2D.style(mName);	// style for mark
    var mMarkRelativePosition = EJSS_DRAWING2D.Element.CENTER;  
	var mMarkSizeX = 0;
	var mMarkSizeY = 0;
	var mMarkAxisY = 0;
	var mMarkStyleList = [];
		 
	self.getClass = function() {
		return "ElementTrace";
	}

	/***
	 * Set a list of styles
	 * @param list
     * @visibility public
	 */
	self.setMarkStyleList = function(list) {
	    if (typeof list == "undefined" || list === null) { 
	    	mMarkStyleList = [];
	    } else if (Array.isArray(list)) {  // list
	      	mMarkStyleList = list.slice();
	      	self.setChanged(true);
	    } 
	};
	
	/***
	 * Get the list of styles
	 * @method getMarkStyleList()
	 * @return list
     * @visibility public
	 */
	self.getMarkStyleList = function() {
	  return mMarkStyleList;
	};

	/**
	 * Sets the mark relative position of the element with respect to its (x,y) coordinates.
	 */
	self.setMarkRelativePosition = function(position) {
	  if (typeof position == "string") position = EJSS_DRAWING2D.Element[position.toUpperCase()];
	  if (mMarkRelativePosition != position) {
	    mMarkRelativePosition = position;
	  	self.setChanged(true);
	  }
	};
	  
	self.getMarkRelativePosition = function() { 
	  return mMarkRelativePosition;
	};

	/***
	 * Return the style of the mark
	 * @method getMarkStyle() 
     * @visibility public
	 */
	self.getMarkStyle = function() { 
	  return mMarkStyle; 
	};

	/***
	 * Set the size of the mark
	 * @method setMarkSize(size)
	 * @param size int[2]|double[2] array with the size in pixels
     * @visibility public
	 */
	self.setMarkSize = function(size) {
	  if((mMarkSizeX != size[0]) || (mMarkSizeY != size[1])) { 
	  	mMarkSizeX = size[0];
	  	mMarkSizeY = size[1];
	  	self.setChanged(true);
	  }
	};
	
	/***
	 * Get the sizes of the mark
	 * @method getMarkSize()
	 * @return double[]
     * @visibility public
	 */
	self.getMarkSize = function() {
	  return [mMarkSizeX, mMarkSizeY];
	};

	/***
	 * Set the type of the mark
	 * @method setMarkType(type)
	 * @param type int|String One of: "CENTER":0,"NORTH":1,"SOUTH":2,"EAST":3,"WEST":4,"NORTH_EAST":5,"NORTH_WEST":6,
	 *                 "SOUTH_EAST":7,"SOUTH_WEST":8 
     * @visibility public
	 */
	self.setMarkType = function(type) {
	  if (typeof type == "string") type = EJSS_DRAWING2D.Trace[type.toUpperCase()];
	  if(mMarkType != type) {
	  	mMarkType = type;
	  	self.setChanged(true);
	  }
	};
	
	/***
	 * Get the type of the mark
	 * @method getMarkType()
	 * @return int
     * @visibility public
	 */
	self.getMarkType = function() {
	  return mMarkType;
	};

	/***
	 * Set the limit axis for bar or area marks
	 * @method setMarkAxisY()
	 * @param int
     * @visibility public
	 */
	self.setMarkAxisY = function(axisY) {
	  if(mMarkAxisY != axisY) {
	  	mMarkAxisY = axisY;
	  	self.setChanged(true);
	  }
	};
  
	/***
	 * Get the limit axis for bar or area marks
	 * @method getMarkAxisY()
	 * @return int
     * @visibility public
	 */
	self.getMarkAxisY = function() {
	  return mMarkAxisY;
	};
  
   self.formatPoint = function(src) {
   		// override
   		var size = src.length;
   		src[size++] = mMarkType;
   		src[size] = EJSS_DRAWING2D.style();
   		EJSS_DRAWING2D.Style.copyTo(mMarkStyle,src[size++]);
		src[size++] = mMarkSizeX;
		src[size++] = mMarkSizeY;
		src[size] = mMarkAxisY;
		return src;
   }
  
	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.Trace.registerProperties(self, controller);
	};
  
  	self.copyTo = function(element) {
    	EJSS_DRAWING2D.Trace.copyTo(self,element);
  	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	return self;
};

