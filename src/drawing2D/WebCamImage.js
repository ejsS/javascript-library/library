/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

/**
 * EJSS framework for drawing2D elements
 * @module drawing2D 
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * WebCamImage
 * @class WebCamImage 
 * @extends Image
 * @constructor  
 */
EJSS_DRAWING2D.WebCamImage = {
  // ----------------------------------------------------
  // Static methods
  // ----------------------------------------------------

  /**
   * static registerProperties method
   */ 
   registerProperties : function(element,controller) {
    EJSS_DRAWING2D.Image.registerProperties(element,controller); // super class

    controller.registerProperty("Url",element.setUrl);
    controller.registerProperty("On",element.setOnOff, element.isOnOff)

  },

};

/**
 * motionJPEG function
 * Creates a basic MotionJPEG image element
 * @method motionJPEG
 * @param name the name of the element
 * @returns A MotionJPEG image element
 */
EJSS_DRAWING2D.webCamImage = function (mName) {
  var self = EJSS_DRAWING2D.image(mName);
  navigator.getUserMedia  = navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia;
  
  var mPlaying = true;
  var mUrl;
  var mPreviousUrl;
  
  self.setUrl = function(url) {
    //if (self.getResourcePath) url = self.getResourcePath(url);
    mUrl = url;
  }	

  self.getUrl = function() {
  	return mUrl;
  }	

  self.setOnOff = function(on) {
	if (on) {
	  if (!mPlaying) self.play();
	}
	else {
	  if (mPlaying) self.pause();
	}
  }

  self.isOnOff = function() {
	return mPlaying;
  }

  self.play = function() {
	if (mUrl) {
      if (mUrl != self.getImageUrl()) {
        mPreviousUrl = self.getImageUrl();
        if (mUrl=="local:") {
          if (navigator.getUserMedia) {
            navigator.getUserMedia({video: true}, 
              function(stream) { self.forceImageUrl(stream); }, 
              errorCallback);
          } 
          else self.setImageUrl(mPreviousUrl);
        }
        else self.setImageUrl(mUrl);
	  }
	}
	mPlaying = true;
  }

  self.pause = function() {
	mPlaying = false;
	if (mPreviousUrl) self.setImageUrl(mPreviousUrl);
  }

/*
  self.dataCollectedno = function() {
  	if (mPlaying && mUrl) {
    	// self.setChanged(true);
    	self.setChangedImage(true);
    }
  }
  
*/

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.WebCamImage.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};
