/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia and Félix J. García 
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*/

/**
 * Framework for 2D drawing.
 * @module 2Ddrawing
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A DrawingPanel is a 2D drawing panel with no decoration. 
 * @class EJSS_DRAWING2D.DrawingPanel
 * @see EJSS_DRAWING2D.PlottingPanel
 * @constructor
 */
EJSS_DRAWING2D.DrawingPanel = {
	MOUSE_ENTERED : 0,
	MOUSE_EXITED : 1,
	MOUSE_PRESSED : 2,
	MOUSE_DRAGGED : 3,
	MOUSE_RELEASED : 4,

	GRAPHICS2D_SVG : 0,
	GRAPHICS2D_CANVAS : 1,

	SCALE_NUM : 0,
	SCALE_LOG : 1,
	
	ENABLED_NONE : 0,
	ENABLED_ANY : 1,
	ENABLED_X : 2,
	ENABLED_Y : 3,
	ENABLED_NO_MOVE : 4,

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		// No super class
		// EJSS_INTERFACE.Element.registerProperties(element.getGraphics(), controller);

		element.setController(controller);

	  	controller.registerProperty("AutoScale", element.setAutoScale, element.getAutoScale);
	 /*** 
	  * Whether the panel should autoscale in the X dimension
	  * @property AutoScaleX 
	  * @type boolean
	  * @default "false"
	  */ 
		controller.registerProperty("AutoScaleX", element.setAutoScaleX, element.getAutoScaleX);
	 /*** 
	  * Whether the panel should autoscale in the Y dimension
	  * @property AutoScaleY 
	  * @type boolean
	  * @default "false"
	  */ 
		controller.registerProperty("AutoScaleY", element.setAutoScaleY, element.getAutoScaleY);		
		controller.registerProperty("InvertedScaleY", element.setInvertedScaleY, element.getInvertedScaleY);
	 /*** 
	  * The minimum value for the X coordinates of elements. 
	  * If specified, the minimum is respected even if AutoscaleX is true and there are no elements close to the minimum.   
	  * @property MinimumX 
	  * @type int or double
	  * @default "-1"
	  */ 
		controller.registerProperty("MinimumX", element.setWorldXMin, element.getWorldXMin);
	 /*** 
	  * The maximum value for the X coordinates of elements. 
	  * If specified, the maximum is respected even if AutoscaleX is true and there are no elements close to the maximum.   
	  * @property MaximumX 
	  * @type int or double
	  * @default "+1"
	  */ 
		controller.registerProperty("MaximumX", element.setWorldXMax, element.getWorldXMax);
	 /*** 
	  * The minimum value for the Y coordinates of elements. 
	  * If specified, the minimum is respected even if AutoscaleY is true and there are no elements close to the minimum.   
	  * @property MinimumY 
	  * @type int or double
	  * @default "-1"
	  */ 
		controller.registerProperty("MinimumY", element.setWorldYMin, element.getWorldYMin);
	 /*** 
	  * The maximum value for the Y coordinates of elements. 
	  * If specified, the maximum is respected even if AutoscaleY is true and there are no elements close to the maximum.   
	  * @property MaximumXY
	  * @type int or double
	  * @default "+1"
	  */ 
		controller.registerProperty("MaximumY", element.setWorldYMax, element.getWorldYMax);
		controller.registerProperty("Bounds", element.setWorldCoordinates, element.getWorldCoordinates);
	 /*** 
	  * The type of scale for the X axis 
	  * @property ScaleXType
	  * @type int or String. One of: <ul>
	  *   <li>EJSS_DRAWING2D.DrawingPanel.SCALE_NUM ("SCALE_NUM" for short)</li>
	  *   <li>EJSS_DRAWING2D.DrawingPanel.SCALE_LOG ("SCALE_LOG" for short)</li>
	  * </ul>
	  * @default "SCALE_NUM"
	  */ 
		controller.registerProperty("ScaleXType", function(v) {
			element.setTypeScaleX(v);
			element.scale();
		}, element.getTypeScaleX);
	 /*** 
	  * The type of scale for the Y axis 
	  * @property ScaleYType
	  * @type int or String. One of: <ul>
	  *   <li>EJSS_DRAWING2D.DrawingPanel.SCALE_NUM ("SCALE_NUM" for short)</li>
	  *   <li>EJSS_DRAWING2D.DrawingPanel.SCALE_LOG ("SCALE_LOG" for short)</li>
	  * </ul>
	  * @default "SCALE_NUM"
	  */ 
		controller.registerProperty("ScaleYType", function(v) {
			element.setTypeScaleY(v);
			element.scale();
		}, element.getTypeScaleY);
	 /*** 
	  * When autoscaling the X axis, the percentage of the X range that should be left (on both sides) around the elements 
	  * @property MarginX
	  * @type int or double (in the range [0,100])
	  * @default "0"
	  */ 
		controller.registerProperty("MarginX", element.setMarginX,element.getMarginX);
	 /*** 
	  * When autoscaling the Y axis, the percentage of the Y range that should be left (on both sides) around the elements 
	  * @property MarginY
	  * @type int or double (in the range [0,100])
	  * @default "0"
	  */ 
		controller.registerProperty("MarginY", element.setMarginY,element.getMarginY);

		controller.registerProperty("Parent", element.getGraphics().setParent, element.getGraphics().getParent);
	 /*** 
	  * A classname for the element that can be used in CSS files 
	  * @property ClassName
	  * @type String
	  * @default "undefined"
	  */ 
        controller.registerProperty("ClassName", element.getGraphics().setClassName);

	 /*** 
	  * The width of the HTML5 element.
	  * See <a href="http://www.w3schools.com/cssref/pr_dim_width.asp">this link</a> for possible values.
	  * @property Width
	  * @type int or String 
	  * @default "auto"
	  */ 
		controller.registerProperty("Width", function(v) {
			if (element.getGraphics().setWidth(v))
				element.scale();
		}, element.getGraphics().getBox().width);
	 /*** 
	  * The height of the HTML5 element.
	  * See <a href="http://www.w3schools.com/cssref/pr_dim_width.asp">this link</a> for possible values.
	  * @property Height
	  * @type int or String 
	  * @default "auto"
	  */ 
		controller.registerProperty("Height", function(v) {
			if (element.getGraphics().setHeight(v))
				element.scale();
		}, element.getGraphics().getBox().height);

		//    controller.registerProperty("X",null,function() { return mInteraction.lastPoint[0]; });
		//    controller.registerProperty("Y",null,function() { return mInteraction.lastPoint[1]; });

	 /*** 
	  * Whether the panel should modify its scales to keep a 1:1 aspect ratio. 
	  * @property SquareAspect
	  * @type boolean 
	  * @default "false"
	  */ 
		controller.registerProperty("SquareAspect", element.setSquareAspect);
	 /*** 
	  * The type of graphics implementation of the panel. 
	  * Changing the graphics mode may influence what elements are actually displayed and the interaction response of the panel.
	  * But it may also influence the rendering speed.   
	  * @property GraphicsMode
	  * @type int or String. One of: <ul>
	  *   <li>EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_SVG ("SVG" for short): SVG based drawing, full interaction, may be slower</li> 
	  *   <li>EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_CANVAS ("CANVAS" for short): Canvas-based drawing, reduced interaction, usually faster</li>
	  * </ul>
	  * @default "SCALE_NUM"
	  */ 
		controller.registerProperty("GraphicsMode", element.setGraphicsMode);
	 /*** 
	  * Whether the panel should respond to user interaction.
	  * This is set to "false" by default, since 'listening' to interaction slows down the rendering.  
	  * @property Enabled
	  * @type boolean 
	  * @default "false"
	  */ 
		controller.registerProperty("Enabled", element.getPanelInteraction().setEnabled);
	 /*** 
	  * Whether to enable the move event listener.
	  * This is set to "false" by default and is ignored when Enabled is set to "false".  
	  * @property StopMoveEvents
	  * @type boolean 
	  * @default "false"
	  */ 
		controller.registerProperty("StopMoveEvents", element.getPanelInteraction().setStopMoveEvents);
	 /*** 
	  * Whether to propagate events and gestures to the containing HTML element.
	  * This may be useful, for instance, to prevent ePub readers from reacting to what should be just an interaction with the panel. 
	  * @property StopEventPropagation
	  * @type boolean 
	  * @default "true"
	  */ 
		controller.registerProperty("StopEventPropagation", element.getPanelInteraction().setStopGestures);

	 /*** 
	  * The dimensions of a strip around the main panel drawing area
	  * @property Gutters
	  * @type int[4] representing [left,top,right,bottom], the size of the strip (in pixels) in that part of the panel  
	  * @default "[0,0,0,0] for DrawingPanels, [50,50,50,50] for PlottingPanels"
	  */ 
		controller.registerProperty("Gutters", element.setGutters);
	 /*** 
	  * The stroke color for the outer border of the panel (exterior border of the gutter).
	  * See <a href="http://www.w3schools.com/cssref/css_colornames.asp">this link</a> for possible values.
	  * @property GuttersLineColor 
	  * @type string Any valid CSS color name. "none" for no color
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Black"
	  */                          
		controller.registerProperty("GuttersLineColor", element.getGuttersStyle().setLineColor);
	 /*** 
	  * The stroke width for the outer border of the panel (exterior border of the gutter).
	  * @property GuttersLineWidth 
	  * @type int width in pixels
	  * @default "1"
	  */                          
		controller.registerProperty("GuttersLineWidth", element.getGuttersStyle().setLineWidth);
	 /*** 
	  * Whether to draw the outer border of the panel (exterior border of the gutter).
	  * @property GuttersDrawLines 
	  * @type boolean
	  * @default "true"
	  */                          
		controller.registerProperty("GuttersDrawLines", element.getGuttersStyle().setDrawLines);
	 /*** 
	  * The fill color for the gutter.
	  * See <a href="http://www.w3schools.com/cssref/css_colornames.asp">this link</a> for possible values.
	  * @property GuttersColor 
	  * @type string Any valid CSS color name. "none" for no color
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "rgb(239,239,255) for DrawingPanels, rgb(211,216,255) for PlottingPanels"
	  */                          
		controller.registerProperty("GuttersColor",     element.getGuttersStyle().setFillColor);
	 /*** 
	  * Whether to fill the gutter.
	  * @property GuttersFill 
	  * @type boolean
	  * @default "true"
	  */                          
		controller.registerProperty("GuttersFill",      element.getGuttersStyle().setDrawFill);
	 /*** 
	  * SVG shape rendering for the gutters
	  * @property GuttersRendering 
	  * @type string
	  * @values "auto","optimizeSpeed","crispEdges","geometricPrecision"  
	  * @default "auto"
	  */                          
		controller.registerProperty("GuttersRendering", element.getStyle().setShapeRendering);

	 /*** 
	  * The fill color for the main drawing area of the panel
	  * See <a href="http://www.w3schools.com/cssref/css_colornames.asp">this link</a> for possible values.
	  * @property Background 
	  * @type string Any valid CSS color name. "none" for no color
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "rgb(239,239,255) for DrawingPanels, White for PlottingPanels"
	  */                          
		controller.registerProperty("Background", element.getStyle().setFillColor);
	 /*** 
	  * The stroke color for the border of the main drawing area of the panel (inner border of the gutters)
	  * See <a href="http://www.w3schools.com/cssref/css_colornames.asp">this link</a> for possible values.
	  * @property Foreground 
	  * @type string Any valid CSS color name. "none" for no color
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Black"
	  */                          
		controller.registerProperty("Foreground", element.getStyle().setLineColor);
	 /*** 
	  * The stroke color for the border of the main drawing area of the panel (inner border of the gutters).
	  * Same as "Foreground".
	  * See <a href="http://www.w3schools.com/cssref/css_colornames.asp">this link</a> for possible values.
	  * @property LineColor 
	  * @type string Any valid CSS color name. "none" for no color
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "Black"
	  */                          
		controller.registerProperty("LineColor", element.getStyle().setLineColor);
	 /***
	  * The stroke width for the border of the main drawing area of the panel (inner border of the gutters).
	  * @property LineWidth 
	  * @type int width in pixels
	  * @default "1"
	  */                          
		controller.registerProperty("LineWidth", element.getStyle().setLineWidth);
	 /*** 
	  * Whether to draw the inner border of the panel (inside border of the gutter).
	  * @property DrawLines 
	  * @type boolean
	  * @default "true"
	  */                          
		controller.registerProperty("DrawLines", element.getStyle().setDrawLines);
	 /*** 
	  * The fill color for the main drawing area of the panel.
	  * Same as "Background"
	  * See <a href="http://www.w3schools.com/cssref/css_colornames.asp">this link</a> for possible values.
	  * @property FillColor 
	  * @type string Any valid CSS color name. "none" for no color
	  * @see http://www.w3schools.com/cssref/css_colornames.asp
	  * @default "rgb(239,239,255) for DrawingPanels, White for PlottingPanels"
	  */                          
		controller.registerProperty("FillColor", element.getStyle().setFillColor);
	 /*** 
	  * Whether to fill the main drawing area of the panel.
	  * @property DrawFill 
	  * @type boolean
	  * @default "true"
	  */                          
		controller.registerProperty("DrawFill", element.getStyle().setDrawFill);
	 /*** 
	  * SVG shape rendering for the gutters
	  * @property GuttersRendering 
	  * @type string
	  * @values "auto","optimizeSpeed","crispEdges","geometricPrecision"  
	  * @default "auto"
	  */                          
		controller.registerProperty("ShapeRendering", element.getStyle().setShapeRendering);

	 /*** 
	  * Whether the element is visible
	  * @property Visibility 
	  * @type boolean
	  * @default true
	  */                          
	    controller.registerProperty("Visibility", element.getGraphics().getStyle().setVisibility);      
	 /*** 
	  * The CSS display property of the element
	  * See <a href="http://www.w3schools.com/cssref/pr_class_display.asp">this link</a> for possible values.
	  * @property Display 
	  * @type string
	  * @default "inline"
	  */                          
        controller.registerProperty("Display", element.getGraphics().getStyle().setDisplay); 
	 /*** 
	  * An object with one or more CSS properties.
	  * Example { "background-color" : "red", "float" : "right" }
	  * @property CSS 
	  * @type Object 
	  * @default "inline"
	  */                          
		controller.registerProperty("CSS", element.getGraphics().getStyle().setCSS);

		controller.registerProperty("TRMessage", element.getMessageDecoration("TR").setText);
		controller.registerProperty("TRMessageFont", element.getMessageDecoration("TR").getFont().setFont);
		controller.registerProperty("TRMessageColor", element.getMessageDecoration("TR").getFont().setFillColor);
		controller.registerProperty("TRMessageFillColor", element.getMessageDecoration("TR").getStyle().setFillColor);
		controller.registerProperty("TRMessageLineColor", element.getMessageDecoration("TR").getStyle().setLineColor);		
		
		controller.registerProperty("TLMessage", element.getMessageDecoration("TL").setText);
		controller.registerProperty("TLMessageFont", element.getMessageDecoration("TL").getFont().setFont);
		controller.registerProperty("TLMessageColor", element.getMessageDecoration("TL").getFont().setFillColor);
		controller.registerProperty("TLMessageFillColor", element.getMessageDecoration("TL").getStyle().setFillColor);
		controller.registerProperty("TLMessageLineColor", element.getMessageDecoration("TL").getStyle().setLineColor);		

		controller.registerProperty("BRMessage", element.getMessageDecoration("BR").setText);
		controller.registerProperty("BRMessageFont", element.getMessageDecoration("BR").getFont().setFont);
		controller.registerProperty("BRMessageColor", element.getMessageDecoration("BR").getFont().setFillColor);
		controller.registerProperty("BRMessageFillColor", element.getMessageDecoration("BR").getStyle().setFillColor);
		controller.registerProperty("BRMessageLineColor", element.getMessageDecoration("BR").getStyle().setLineColor);		

		controller.registerProperty("BLMessage", element.getMessageDecoration("BL").setText);
		controller.registerProperty("BLMessageFont", element.getMessageDecoration("BL").getFont().setFont);
		controller.registerProperty("BLMessageColor", element.getMessageDecoration("BL").getFont().setFillColor);
		controller.registerProperty("BLMessageFillColor", element.getMessageDecoration("BL").getStyle().setFillColor);
		controller.registerProperty("BLMessageLineColor", element.getMessageDecoration("BL").getStyle().setLineColor);		

		controller.registerProperty("CoordinatesFormat", element.getCoordinates().setFormat);
		controller.registerProperty("ShowCoordinates", element.setShowCoordinates);
		controller.registerProperty("ShowAreaRectangle", element.setShowAreaRectangle);
		controller.registerProperty("EnabledZooming", element.setEnabledZooming);
		controller.registerProperty("EnabledDragging", element.setEnabledDragging);

	 /*** 
	  * The type of cursor when an element is moved.
	  * See <a href="http://www.w3schools.com/cssref/pr_class_cursor.asp">this link</a> for possible values.
	  * @property CursorTypeForMove 
	  * @type String
	  * @default "move"
	  */ 
		controller.registerProperty("CursorTypeForMove", element.getInteraction().setCursorTypeForMove);

        controller.registerAction("OnDoubleClick", element.getInteraction().getInteractionPoint);      
		controller.registerAction("OnMove", element.getInteraction().getInteractionPoint, element.getOnMoveHandler);
		controller.registerAction("OnPress", element.getInteraction().getInteractionPoint, element.getOnPressHandler);
		controller.registerAction("OnDrag", element.getInteraction().getInteractionPoint, element.getOnDragHandler);
		controller.registerAction("OnRelease", element.getInteraction().getInteractionBounds, element.getOnReleaseHandler);
		controller.registerAction("OnResize", element.getInteraction().getInteractionBounds, element.scale);
		controller.registerAction("OnOrientationChange", element.getInteraction().getOrientation, element.scale);
		controller.registerAction("OnZoom", element.getInteraction().getPinchDistance, element.getOnZoomHandler);
	}
};

/**
 * Constructor for DrawingPanel
 * @method drawingPanel
 * @param mName string
 * @returns An abstract 2D drawing panel
 */
EJSS_DRAWING2D.drawingPanel = function(mName,mGraphicsMode) {
	var self = {}; // reference returned	

	// Graphics implementation
	var mGraphics;

	// Instance variables
	var mStyle = EJSS_DRAWING2D.style(mName);	// style for panel
	var mBottomDecorations = [];				// decorations list for panel
	var mElements = [];							// elements list for panel
	var mElementsChanged = false;				// whether elements list has changed
	var mAutoScaleX = false;					// whether auto-scale with measure elements in X
	var mAutoScaleY = false;					// whether auto-scale with measure elements in Y
	var mInvertedScaleY = false;				// whether inverted scale in Y
	var mTypeScaleX = 0;						// type of scale in X
	var mTypeScaleY = 0;						// type of scale in Y
	var mTopDecorations = [];					// top decorations list for panel
    var mShowCoordinates = true;				// whether coordinates in panel
    var mShowAreaRectangle = true;				// whether area rectangle in panel
	var mEnabledDragging = false;				// whether dragging panel
	var mEnabledZooming = false;				// whether scaling panel
	var mZoomRate = 1.10;						// scaling rate
	var mZoomLimits = [0.1,1000];				// zooming limits (width min and max)
	var mCollectersList = [];		            // Array of all control elements that need a call to dataCollected() after data collection

	// Configuration variables	
	var xmindef = -1, xmaxdef = +1, ymindef = -1, ymaxdef = +1;  // default dimensions
	var mWorld = {
		// preferred dimensions
		xminPreferred : NaN, xmaxPreferred : NaN, yminPreferred : NaN, ymaxPreferred : NaN,
		// measured dimensions
		xminMeasured : NaN, xmaxMeasured : NaN, yminMeasured : NaN, ymaxMeasured : NaN,
		// margin
		xmargin : 0, ymargin : 0,
		// real dimensions after scalation, based on preferred dimensions and added margin
		xmin : -1, xmax : +1, ymin : -1, ymax : +1,
		// keep square aspect
		squareAspect : false,
		// origin in panel
		xorigin : 0, yorigin : 0,
		// pixel per unit for panel
		xscale : 1, yscale : 1		
	};

	// Gutters for panel
	var mGutters = {
		left : 0, right : 0, top : 0, bottom : 0, // sizes
		visible : false, // whether the gutters are visible
	};
	var mGuttersStyle = EJSS_DRAWING2D.style(mName + " gutters")	// style

	// Implementation variables
	var mPanelChanged = true;	// whether panel changed (style, decorations, gutters)
	var mMustScale = true;		// whether panel must scale
	var mEnabledRedering = true;// whether redering is enabled
	
	var mController = {// dummy controller object
		propertiesChanged : function() { },
		invokeAction : function() { }
	};

	// ----------------------------------------
	// Instance functions
	// ----------------------------------------

	/***
	 * Get name for drawing panel
	 * @method getName
	 * @return string
	 */
	self.getName = function() {
		return mName;
	};

	/***
	 * Returns the graphics implementation
	 * @method getGraphics
	 * @return Graphics
	 */
	self.getGraphics = function() {
		return mGraphics;
	};

	/***
	 * Returns the svg image in Base64 format
	 * @method importGraphics
	 * @param callback
	 * @return string 
	 */
    self.importGraphics = function(callback) {
    	if(mGraphics.importSVG)
    		return mGraphics.importSVG(callback);
    	return null;
    }
    
	/***
	 * Return the drawing style of the inner rectangle for panel
	 * @method getStyle
	 * @return Style
	 */
	self.getStyle = function() {
		return mStyle;
	};


	/***
	 * Get the graphics mode
	 * @method getGraphicsMode
	 * @return One of the possible graphics implementation mode: EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_SVG or EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_CANVAS
	 */
	self.getGraphicsMode = function() {
	  return mGraphicsMode;
	};
	
	/***
	 * Get the graphics mode
	 * @method getGraphicsModeName
	 * @return The implementation mode as a string: either "SVG" or "CANVAS"
	 */
	self.getGraphicsModeName = function() {
	  switch (mGraphicsMode) {
	    case EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_CANVAS : return "CANVAS";
	    default:
	    case EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_SVG : return "SVG";
	  }
	};


	/***
	 * Set graphics
	 * @method setGraphicsMode
	 * @param mode One of the possible graphics implementation mode: 
	 *   EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_SVG or EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_CANVAS. Or simply "CANVAS" or "SVG"
	 */
	self.setGraphicsMode = function(mode) {
		// get mode
    	if (typeof mode === 'string') {
    	    if (mode.indexOf("GRAPHICS2D_")!=0) mode = "GRAPHICS2D_" + mode;
      		mode = EJSS_DRAWING2D.DrawingPanel[mode.toUpperCase()] | EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_SVG;
      	} else if(typeof mode === 'undefined') {
			mode = EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_SVG;      		
      	}
    	
      	if (mode == mGraphicsMode) return;
      	mGraphicsMode = mode; 
		var exists = typeof mGraphics !== 'undefined';
		
		if(exists) {
			// get params
			var parent = mGraphics.getParent();
			var width = mGraphics.getWidth();
			var height = mGraphics.getHeight();
			var style = mGraphics.getEventContext().getAttribute("style");
			
			// remove current graphics
			var ele = document.getElementById(mName);
	   		ele.parentNode.removeChild(ele);			
		}
		
		// create new graphics
		if (mode == EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_SVG) {
			mGraphics = EJSS_GRAPHICS.svgGraphics(mName);
		} else if (mode == EJSS_DRAWING2D.DrawingPanel.GRAPHICS2D_CANVAS) {
			mGraphics = EJSS_GRAPHICS.canvasGraphics(mName);
		} else {
			console.log("WARNING: setGraphics() - Graphics not supported");
		}
		
		if(exists) {
			// set params
			mGraphics.setParent(parent);
			mGraphics.setWidth(width);	
			mGraphics.setHeight(height);	
			mGraphics.getEventContext().setAttribute("style",style);	
			mElementsChanged = true;
			
			// interactions
			mInteraction.reload();			
		}
	};
	
	/***
	 * Get auto-scale
	 * @method getAutoScale
	 * @return boolean
	 */
	self.getAutoScale = function() {
		return (mAutoScaleX || mAutoScaleY);
	};

	/***
	 * Set auto-scale
	 * @method setAutoScale
	 * @param auto
	 */
	self.setAutoScale = function(autoscale) {		
		self.setAutoScaleX(autoscale);
		self.setAutoScaleY(autoscale);
	};

	/***
	 * Set auto-scale in X
	 * @method setAutoScaleX
	 * @param auto
	 */
	self.setAutoScaleX = function(autoscale) {
		mAutoScaleX = autoscale;
	};

	/***
	 * Get auto-scale in X
	 * @method getAutoScaleX
	 * @return boolean
	 */
	self.getAutoScaleX = function() {
		return mAutoScaleX;
	};

	/***
	 * Set auto-scale in Y
	 * @method setAutoScaleY
	 * @param auto
	 */
	self.setAutoScaleY = function(autoscale) {
		mAutoScaleY = autoscale;
	};

	/***
	 * Get auto-scale in Y
	 * @method getAutoScaleY
	 * @return boolean
	 */
	self.getAutoScaleY = function() {
		return mAutoScaleY;
	};

	/***
	 * Set inverted scale in Y
	 * @method setInvertedScaleY
	 * @param inverted
	 */
	self.setInvertedScaleY = function(invertedscale) {
		if(mInvertedScaleY != invertedscale) {
			mInvertedScaleY = invertedscale;
			
			// InvertedScaleY ONLY supports mTypeScaleY NUM
			if(mInvertedScaleY) mTypeScaleY = EJSS_DRAWING2D.DrawingPanel.SCALE_NUM;
			
			// report decorations
			var changed = self.reportDecorations("bounds");
			// decorations may request for a repaint
			if (changed) mPanelChanged = true;			
		}
	};

	/***
	 * Get inverted scale in Y
	 * @method getInvertedScaleY
	 * @return boolean
	 */
	self.getInvertedScaleY = function() {
		return mInvertedScaleY;
	};

	/***
	 * Set type of scale in X
	 * @method setTypeScaleX
	 * @param type
	 */
	self.setTypeScaleX = function(typescale) {
    	if (typeof typescale == "string") typescale = EJSS_DRAWING2D.DrawingPanel[typescale.toUpperCase()];
		mTypeScaleX = typescale;
	};

	/***
	 * Get type of scale in X
	 * @method getTypeScaleX
	 * @return int
	 */
	self.getTypeScaleX = function() {
		return mTypeScaleX;
	};

	/***
	 * Set type of scale in Y
	 * @method setTypeScaleY
	 * @param type
	 */
	self.setTypeScaleY = function(typescale) {
		if (typeof typescale == "string") typescale = EJSS_DRAWING2D.DrawingPanel[typescale.toUpperCase()];
		mTypeScaleY = typescale;
		
		// InvertedScaleY ONLY supports mTypeScaleY NUM
		if (mTypeScaleY == EJSS_DRAWING2D.DrawingPanel.SCALE_LOG) mInvertedScaleY = false;	
	};

	/***
	 * Get type of scale in Y
	 * @method getTypeScaleY
	 * @return int
	 */
	self.getTypeScaleY = function() {
		return mTypeScaleY;
	};

	// ----------------------------------------
	// World coordinates
	// ----------------------------------------

	/***
	 * Sets the preferred minimum X coordinate for the panel
	 * @method setWorldXMin
	 * @param xmin
	 */
	self.setWorldXMin = function(xmin) {
		if (xmin !== mWorld.xminPreferred) {
			mWorld.xminPreferred = xmin;
		}
	};

	/***
	 * Returns the preferred minimum X coordinate for the panel
	 * @method getWorldXMin
	 * @return double
	 */
	self.getWorldXMin = function() {
		return mWorld.xminPreferred;
	};

	/***
	 * Sets the preferred maximum X coordinate for the panel
	 * @method setWorldXMax
	 * @param xmax
	 */
	self.setWorldXMax = function(xmax) {
		if (xmax !== mWorld.xmaxPreferred) {
			mWorld.xmaxPreferred = xmax;
		}
	};

	/***
	 * Returns the preferred maximum X coordinate for the panel
	 * @method getWorldXMax
	 * @return double
	 */
	self.getWorldXMax = function() {
		return mWorld.xmaxPreferred;
	};

	/***
	 * Sets the preferred minimum Y coordinate for the panel
	 * @method setWorldYMin
	 * @param ymin
	 */
	self.setWorldYMin = function(ymin) {
		if (ymin !== mWorld.yminPreferred) {
			mWorld.yminPreferred = ymin;
		}
	};

	/***
	 * Returns the preferred minimum Y coordinate for the panel
	 * @method getWorldYMin
	 * @return double
	 */
	self.getWorldYMin = function() {
		return mWorld.yminPreferred;
	};

	/***
	 * Sets the preferred maximum Y coordinate for the panel
	 * @method setWorldYMax
	 * @param ymax
	 */
	self.setWorldYMax = function(ymax) {
		if (ymax !== mWorld.ymaxPreferred) {
			mWorld.ymaxPreferred = ymax;
		}
	};

	/***
	 * Returns the preferred maximum Y coordinate for the panel
	 * @method getWorldYMax
	 * @return double
	 */
	self.getWorldYMax = function() {
		return mWorld.ymaxPreferred;
	};

	/***
	 * Sets the preferred user coordinates for the panel
	 * @method setWorldCoordinates
	 * @param bounds
	 */
	self.setWorldCoordinates = function(bounds) {
		self.setWorldXMin(bounds[0]);
		self.setWorldXMax(bounds[1]);
		self.setWorldYMin(bounds[2]);
		self.setWorldYMax(bounds[3]);
	};

	/***
	 * Gets the preferred user coordinates for the panel
	 * @method getWorldCoordinates
	 * @return bounds
	 */
	self.getWorldCoordinates = function() {
		return [self.getWorldXMin(), self.getWorldXMax(), self.getWorldYMin(), self.getWorldYMax()];
	};

	/***
	 * Gets the measured coordinates for the panel
	 * @method getMeasuredCoordinates
	 * @return bounds
	 */
	self.getMeasuredCoordinates = function() {
		return [mWorld.xminMeasured, mWorld.xmaxMeasured, mWorld.yminMeasured, mWorld.ymaxMeasured];
	};

	/***
	 * Gets the user coordinates for the panel
	 * @method getRealWorldCoordinates
	 * @return bounds
	 */
	self.getRealWorldCoordinates = function() {
		return [mWorld.xmin, mWorld.xmax, mWorld.ymin, mWorld.ymax];
	};

	/***
	 * Returns the minimum X coordinate for the panel
	 * @method getRealWorldXMin
	 * @return double
	 */
	self.getRealWorldXMin = function() {
		return mWorld.xmin;
	};

	/***
	 * Returns the maximum X coordinate for the panel
	 * @method getRealWorldXMax
	 * @return double
	 */
	self.getRealWorldXMax = function() {
		return mWorld.xmax;
	};

	/***
	 * Returns the minimum Y coordinate for the panel
	 * @method getRealWorldYMin
	 * @return double
	 */
	self.getRealWorldYMin = function() {
		return mWorld.ymin;
	};

	/***
	 * Returns the maximum Y coordinate for the panel
	 * @method getRealWorldYMax
	 * @return double
	 */
	self.getRealWorldYMax = function() {
		return mWorld.ymax;
	};

	/***
	 * Whether the panel should keep a 1:1 aspect ratio between X and Y coordinates
	 * @method setSquareAspect
	 * @param boolean
	 */
	self.setSquareAspect = function(square) {
		if (square !== mWorld.squareAspect) {
			mWorld.squareAspect = square;
			mMustScale = true;
		}
	};

	/***
	 * Set margin X
	 * @method setMarginX
	 * @param margin
	 */
	self.setMarginX = function(margin) {
		if(mWorld.xmargin != margin) {
			mWorld.xmargin = margin;
			mMustScale = true;			
		}		
	};
	
	/***
	 * Get margin X
	 * @method getMarginX
	 * @return margin
	 */	
	self.getMarginX = function() {
		return mWorld.xmargin;
	}

	/***
	 * Set margin Y
	 * @method setMarginY
	 * @param margin
	 */
	self.setMarginY = function(margin) {
		if(mWorld.ymargin != margin) {
			mWorld.ymargin = margin;
			mMustScale = true;			
		}		
	};
	
	/***
	 * Get margin Y
	 * @method getMarginY
	 * @return margin
	 */	
	self.getMarginY = function() {
		return mWorld.ymargin;
	}

	// ----------------------------------------
	// Decorations and elements
	// ----------------------------------------

	/***
	 * Allow to drag panel
	 * @method setEnabledDragging
	 * @parem motion ENABLED_NONE: 0, ENABLED_ANY: 1, ENABLED_X: 2, ENABLED_Y: 3, ENABLED_NO_MOVE: 4
	 */
	self.setEnabledDragging = function(motion) {
		if ( typeof motion == "string") {
			value = EJSS_DRAWING2D.DrawingPanel[motion.toUpperCase()];
			mEnabledDragging = (typeof value === 'undefined')?EJSS_DRAWING2D.DrawingPanel.ENABLED_NONE:value;
		}
	    else mEnabledDragging = motion;
	}

	/***
	 * Type of motion allowed to drag panel
	 * @method getEnabledDragging
	 * @return motion
	 */
	self.getEnabledDragging = function() {
		return mEnabledDragging;
	}

	/***
	 * Allow to scale panel
	 * @method setEnabledZooming
	 * @param allowed
	 */
	self.setEnabledZooming = function(allowed) {
		mEnabledZooming = allowed;
	}

	/***
	 * Whether panal scaling is allowed
	 * @method getEnabledZooming
	 * @return boolean
	 */
	self.getEnabledZooming = function() {
		return mEnabledZooming;
	}

	/***
	 * Set zoom rate
	 * @method setZoomRate
	 * @param rate
	 */
	self.setZoomRate = function(rate) {
		mZoomRate = rate;
	}

	/***
	 * Get zoom rate
	 * @method getZoomRate
	 * @return rate
	 */
	self.getZoomRate = function() {
		return mZoomRate;
	}

	/***
	 * Set zoom limits
	 * @method setZoomLimits
	 * @param limits [min,max]
	 */
	self.setZoomLimits = function(limits) {
		mZoomLimits = limits;
	}

	/***
	 * Get zoom limits
	 * @method getZoomLimits
	 * @return limits [min,max]
	 */
	self.getZoomLimits = function() {
		return mZoomLimits;
	}
	
	/***
	 * Show coordinates in panel
	 * @method setShowCoordinates
	 * @param boolean
	 */
	self.setShowCoordinates = function(show) {
		mShowCoordinates = show;
	}

	/***
	 * Whether coordinates in panel are showed
	 * @method getShowCoordinates
	 * @return boolean
	 */
	self.getShowCoordinates = function() {
		return mShowCoordinates;
	}

	/***
	 * Show area rectangle in panel
	 * @method setShowAreaRectangle
	 * @param boolean
	 */
	self.setShowAreaRectangle = function(show) {
		mShowAreaRectangle = show;
	}

	/***
	 * Show area rectangle in panel
	 * @method getShowAreaRectangle
	 * @param boolean
	 */
	self.getShowAreaRectangle = function() {
		return mShowAreaRectangle;
	}

	/***
	 * Adds a decoration to the panel. Decorations are drawn before any other elements.
	 * @method addDecoration
	 * @param drawable decoration element
	 * @param position integer
	 * @param istop top decoration
	 */
	self.addDecoration = function(drawable, position, istop) {
		if (istop)
			EJSS_TOOLS.addToArray(mTopDecorations, drawable, position);
		else
			EJSS_TOOLS.addToArray(mBottomDecorations, drawable, position);
		if (drawable.setPanel)// set this panel to decoration element
			drawable.setPanel(self);
		return self;
	};

	/***
	 * Removes a decoration
	 * @method removeDecoration
	 * @param drawable decoration element
	 */
	self.removeDecoration = function(drawable) {
		EJSS_TOOLS.removeFromArray(mBottomDecorations, drawable);
		EJSS_TOOLS.removeFromArray(mTopDecorations, drawable);
		if (drawable.setPanel)// remove this panel to decoration element
			drawable.setPanel(null);
		return self;
	};

	/***
	 * Add a element to the panel. Elements are asked to draw themselves
	 * whenever the panel needs to render. For this purpose, they will receive a
	 * calls to draw().
	 * Elements are reported of changes in the world coordinates of the panel, in case
	 * they need to recalculate themselves.
	 * @method addElement
	 * @param element Element
	 * @param position int
	 */
	self.addElement = function(element, sibling) {
		if(typeof sibling === 'undefined') {
			EJSS_TOOLS.addToArray(mElements, element);			
		} else if (typeof sibling === 'object') { // sibling object
			var index = EJSS_TOOLS.arrayObjectIndexOf(mElements,sibling);
			EJSS_TOOLS.addToArray(mElements, element, index);
		} else { // position
			EJSS_TOOLS.addToArray(mElements, element, sibling);
		}
		// set this panel to decoration element
		element.setPanel(self);
		if (element.dataCollected) mCollectersList.push(element);
		// elements list has changed
		mElementsChanged = true;
	};

	/***
	 * Remove a element to the panel.
	 * @method removeElement
	 * @param element Element
	 */
	self.removeElement = function(element) {
		EJSS_TOOLS.removeFromArray(mElements, element);
		mInteraction.clearInteractionElement(element);
		element.setPanel(null);
		if (element.dataCollected) EJSS_TOOLS.removeFromArray(mCollectersList, element);
		// elements list has changed
		mElementsChanged = true;
	};

	/***
	 * Return the array of a elements.
	 * @method getElements
	 * @return Elements
	 */
	self.getElements = function() {
		return mElements;
	};

	/***
	 * Return the position of a element.
	 * @method indexOfElement
	 * @param element Element
	 * @return integer
	 */
	self.indexOfElement = function(element) {
		return mElements.indexOf(element);
	};

	// ----------------------------------------
	// Gutters (empty or decorated area around the drawing area)
	// ----------------------------------------

	/***
	 * Sets the gutters dimensions
	 * @method setGutters
	 * @param rect the number of pixels for the drawing area
	 */
	self.setGutters = function(rect) {
		var left = rect[0], top = rect[1], right = rect[2], bottom = rect[3];

		var changed = false;
		if (left !== mGutters.left) {
			mGutters.left = left;
			changed = true;
		}
		if (top !== mGutters.top) {
			mGutters.top = top;
			changed = true;
		}
		if (right !== mGutters.right) {
			mGutters.right = right;
			changed = true;
		}
		if (bottom !== mGutters.bottom) {
			mGutters.bottom = bottom;
			changed = true;
		}
		if (changed) {
			mGutters.visible = (mGutters.left > 0 || mGutters.top > 0 || mGutters.right > 0 || mGutters.bottom > 0);
			mPanelChanged = true;
			mMustScale = true;
		}		
	};

	/***
	 * Return the bounding gutters
	 * @method getGutters
	 * @return Gutters
	 */
	self.getGutters = function() {
		return mGutters;
	};

	/***
	 * Return the drawing style of the bounding gutters
	 * @method getGuttersStyle
	 * @return Style
	 */
	self.getGuttersStyle = function() {
		return mGuttersStyle;
	};

	// ----------------------------------------
	// Apply transformations and conversions
	// ----------------------------------------

	/***
	 * Converts a Y pixel value so that 0 is at the bottom
	 * @method toPixelAxisY
	 * @param y double
	 */
	self.toPixelAxisY = function(y) {
		return (mWorld.yorigin - y) - (mWorld.yscale * mWorld.ymin);;
	};

	/***
	 * Converts a X pixel value so that 0 is at the left
	 * @method toPixelAxisX
	 * @param x double
	 */
	self.toPixelAxisX = function(x) {
		return (mWorld.xorigin + x) - (mWorld.xscale * mWorld.xmin);
	};

	/***
	 * To be used only after a call to render()!
	 * Projects a point from world coordinates to pixel coordinates.
	 * @method toPixelPosition
	 * @param point double[] The original coordinates
	 * @param scale double[] The type of scale (LOG or NUM)
	 * @return double[] An array with the result
	 */
	self.toPixelPosition = function(point,scale) {
		var scaleX = (typeof scale != "undefined")? scale : mTypeScaleX; // if scale is undefined, it uses the panel scale
		var scaleY = (typeof scale != "undefined")? scale : mTypeScaleY; // if scale is undefined, it uses the panel scale
		var pos = [];
		if(scaleX == EJSS_DRAWING2D.DrawingPanel.SCALE_LOG) // LOG
	    	pos[0] = self.toPixelLogScale(point[0], mWorld.xorigin, mWorld.xmin, mWorld.xmax, mWorld.xscale); 						    		   		
		else // NUM
			pos[0] = mWorld.xorigin + mWorld.xscale * (point[0] - mWorld.xmin);		
		
		if(scaleY == EJSS_DRAWING2D.DrawingPanel.SCALE_LOG) // LOG
			pos[1] = self.toPixelLogScale(point[1], mWorld.yorigin, mWorld.ymin, mWorld.ymax, mWorld.yscale);		
		else { // NUM
			if(!mInvertedScaleY)
				pos[1] = mWorld.yorigin + mWorld.yscale * (point[1] - mWorld.ymin);
			else
				pos[1] = mGutters.top + mWorld.yscale * (mWorld.ymin - point[1]);			
		}
			
		return pos;
	};

	/***
	 * To be used only after a call to render()!
	 * Projects a module from world coordinates to pixel coordinates
	 * @method toPixelMod
	 * @param point double[] The original module
	 * @return double[] The same array once transformed
	 */
	self.toPixelMod = function(mod) {
		var pmod = [];
		pmod[0] = mod[0] * mWorld.xscale;
		pmod[1] = mod[1] * mWorld.yscale;		
		return pmod;
	};

	/***
	 * To be used only after a call to render()!
	 * Projects a point from pixel coordinates to world coordinates
	 * @method toPanelPosition
	 * @param point double[] The original coordinates
	 * @param scale double[] The type of scale (LOG or NUM)
	 * @return double[] The same array once transformed
	 */
	self.toPanelPosition = function(point,scale) {	
		var scaleX = (typeof scale != "undefined")? scale : mTypeScaleX; // if scale is undefined, it uses the panel scale
		var scaleY = (typeof scale != "undefined")? scale : mTypeScaleY; // if scale is undefined, it uses the panel scale
		var pos = [];
		if(scaleX == EJSS_DRAWING2D.DrawingPanel.SCALE_LOG)  // LOG 
	    	pos[0] = self.toPanelLogScale(point[0], mWorld.xorigin, mWorld.xmin, mWorld.xmax, mWorld.xscale); 						
		 else  // NUM 
			pos[0] = mWorld.xmin + (point[0] - mWorld.xorigin) / mWorld.xscale;
		
		if(scaleY == EJSS_DRAWING2D.DrawingPanel.SCALE_LOG)  // LOG
			pos[1] = self.toPanelLogScale(point[1], mWorld.yorigin, mWorld.ymin, mWorld.ymax, mWorld.yscale);
		else { // NUM
			if(!mInvertedScaleY)
				pos[1] = mWorld.ymin + (point[1] - mWorld.yorigin) / mWorld.yscale;
			else
				pos[1] = mWorld.ymin - (point[1] - mGutters.top) / mWorld.yscale;			
		}
		return pos;
	};

	/***
	 * To be used only after a call to render()!
	 * Projects a module from pixel coordinates to world coordinates
	 * @method toPanelMod
	 * @param point double[] The original module
	 * @return double[] The same array once transformed
	 */
	self.toPanelMod = function(mod) {
		var pmod = [];
		pmod[0] = ((mWorld.xscale == 0)? 0 : mod[0]/mWorld.xscale);
		pmod[1] = ((mWorld.yscale == 0)? 0 : mod[1]/mWorld.yscale);		
		return pmod;
	};

	/***
	 * Projects a value in world coordinates from a decimal scale 
	 * (defined by origin, min, max and pixratio) to a log scale in pixel coordinates
	 * @method toPixelLogScale
	 * @param value double The original value
	 * @param origin double The origin in panel (pixels)
	 * @param min double The min value in world coordinates
	 * @param max double The max value in world coordinates
	 * @param pixratio double The pixels per unit for panel
	 * @return double The value transformed
	 */
	self.toPixelLogScale = function(value, origin, min, max, pixratio) {
    	var minscale = (min <= 0)? 1 : Math.log(min)/Math.log(10);		// min log value
    	var maxscale = (max <= 0)? 1 : Math.log(max)/Math.log(10);		// max log value
		var segsize = (max - min) * pixratio;
    	var scalesize = maxscale - minscale;			// size of log scale
    	var sizeratio = segsize / scalesize;			// pixel per log value
			
   		var step = value <= 0? 0 : Math.log(value) / Math.log(10);
   		return origin + sizeratio * (step - minscale);   		
	}

	/***
	 * Projects a value in pixel coordinates from a decimal scale 
	 * (defined by origin, min, max and pixratio) to a log scale in world coordinates
	 * @method toPanelLogScale
	 * @param value double The original value
	 * @param origin double The origin in panel (pixels)
	 * @param min double The min value in world coordinates
	 * @param max double The max value in world coordinates
	 * @param pixratio double The pixels per unit for panel
	 * @return double The value transformed
	 */
	self.toPanelLogScale = function(value, origin, min, max, pixratio) {
    	var minscale = (min < 0)? 0 : Math.log(min)/Math.log(10);		// min log value
    	var maxscale = (max < 0)? 0 : Math.log(max)/Math.log(10);		// max log value
		var segsize = (max - min) * pixratio;
    	var scalesize = maxscale - minscale;			// size of log scale
    	var sizeratio = segsize / scalesize;			// pixel per log value
		
		return Math.pow(10, minscale + (value - origin) / sizeratio); 								
	}

	/***
	 * Get pixel position of the origin
	 * @method getPixelPositionWorldOrigin
	 * @return array pixel position
	 */
	self.getPixelPositionWorldOrigin = function() {
		return [mWorld.xorigin,mWorld.yorigin];
	}

	/***
	 * Return drawing box (excluding gutters)
	 * @method getInnerRect
	 * @return box  
	 */
	self.getInnerRect = function() {
	 	var bounds = self.getRealWorldCoordinates();
	    if(!self.getInvertedScaleY())
			var pos = self.toPixelPosition([bounds[0],bounds[3]]);
		else
			var pos = self.toPixelPosition([bounds[0],bounds[2]]);		    
	    var size = self.toPixelMod([bounds[1]-bounds[0],bounds[3]-bounds[2]]);     
	    
	    return {x: pos[0], width: Math.abs(size[0]), y: pos[1], height: Math.abs(size[1])}
	},

	/***
	 * Force scale again
	 * @method scale
	 */
	self.scale = function() {
		mMustScale = true;
		mElementsChanged = true;
	};
	
	/***
	 * Refresh all elements
	 * @method touch
	 */
	self.touch = function() {
		mMustScale = true;
		mElementsChanged = true;
		for (var i = 0, n = mElements.length; i < n; i++)
			mElements[i].setChanged(true);
	};
	
	/***
	 * Recomputes the scales of the panel.
	 * @method recomputeScales
	 */
	self.recomputeScales = function() {	
		var newWorld = false;
		
		// start with the preferred min-max values plus margin.
		var rangeX = (mWorld.xmaxMeasured-mWorld.xminMeasured)/2;
		var rangeY = (mWorld.ymaxMeasured-mWorld.yminMeasured)/2;
		var xmin = mWorld.xminMeasured - Math.abs(rangeX*mWorld.xmargin/100); 
		var xmax = mWorld.xmaxMeasured + Math.abs(rangeX*mWorld.xmargin/100);
		var ymin = mWorld.yminMeasured - Math.abs(rangeY*mWorld.ymargin/100); 
		var ymax = mWorld.ymaxMeasured + Math.abs(rangeY*mWorld.ymargin/100);

		// get sizes and scale
		var box = mGraphics.getBox();
		var width = box.width - (mGutters.left + mGutters.right); // width in pixels
		var height = box.height - (mGutters.bottom + mGutters.top); // height in pixels
		var xPixPerUnit = width / (xmax - xmin); // the x scale in pixels
		var yPixPerUnit = height / (ymax - ymin); // the y scale in pixels
		var stretch, diff;
		if (mWorld.squareAspect) {// keep square aspect
			stretch = Math.abs(xPixPerUnit / yPixPerUnit);
			// relation between x scale and y scale
			if (stretch >= 1) {// make the x range bigger so that aspect ratio is one
				stretch = Math.min(stretch, width);
				// limit the stretch
				diff = (xmax - xmin) * (stretch - 1) / 2.0;
				xmin -= diff;
				xmax += diff;
				xPixPerUnit = width / (xmax - xmin);
				//Math.max(width-leftGutter-rightGutter, 1)/(xmax-xmin);  // the x scale in pixels per unit
			} else {// make the y range bigger so that aspect ratio is one
				stretch = Math.max(stretch, 1.0 / height);
				// limit the stretch
				diff = (ymax - ymin) * (1.0 / stretch - 1) / 2.0;
				ymin -= diff;
				ymax += diff;
				yPixPerUnit = height / (ymax - ymin);
				//Math.max(height-bottomGutter-topGutter, 1)/(ymax-ymin); // the y scale in pixels per unit
			}
		}
		// new world, because new scale
		newWorld = (mWorld.xscale != xPixPerUnit || mWorld.yscale != -yPixPerUnit);
		
		// centered
		mWorld.xscale = xPixPerUnit;
		mWorld.yscale = -yPixPerUnit;
		mWorld.xorigin = mGutters.left + 0.5;
		mWorld.yorigin = height + mGutters.top + 0.5;
		mMustScale = false;
		
		// Check for actual changes
		if ((mWorld.xmin !== xmin) || (mWorld.xmax !== xmax) || (mWorld.ymin !== ymin) || (mWorld.ymax !== ymax)) {
			mWorld.xmin = xmin;
			mWorld.xmax = xmax;
			mWorld.ymin = ymin;
			mWorld.ymax = ymax;
			// report decorations
			var changed = self.reportDecorations("bounds");
			// decorations may request for a repaint
			if (changed)
				mPanelChanged = true;
			// new World
			newWorld = true;
		}
		
		// if new world, then report elements
		if(newWorld) {			
			for (var i = 0, n = mElements.length; i < n; i++)
				mElements[i].setMustProject(true);
		}
	};

	/***
	 * Report event to decoration elements
	 * @method reportDecorations
	 * @param event
	 */
    self.reportDecorations = function(event) {
		var changed = false, i, n;
		// bottom group
		for (i = 0, n = mBottomDecorations.length; i < n; i++) {
			changed |= (mBottomDecorations[i].panelChangeListener && mBottomDecorations[i].panelChangeListener("bounds"));
		}
		// top group
		for (i = 0, n = mTopDecorations.length; i < n; i++) {
			if (mTopDecorations[i].panelChangeListener)
				mTopDecorations[i].panelChangeListener("bounds");
		}
		return changed;    	
    }

	/***
	 * Recalculate world dimensions using measure elements
	 * @method checkMeasure
	 * @return boolean whether dimesions update
	 */
	self.checkMeasure = function() {
		// init measured dimensions 
		var xminMeasured = Number.MAX_VALUE;
		var xmaxMeasured = -Number.MAX_VALUE;
		var yminMeasured = Number.MAX_VALUE;
		var ymaxMeasured = -Number.MAX_VALUE;

		// if autoscale, get measured dimensions		
		if(self.getAutoScaleY() || self.getAutoScaleX()) {							
			for (var i = 0, n = mElements.length; i < n; i++) {
				// considering measured elements (no groups) 
				if (mElements[i].isMeasured() && !mElements[i].isGroup() && 
					(mElements[i].getGroup() == null || mElements[i].getGroup().isMeasured())) {					
					if(mElements[i].isPixelSize()) { // for PixelSize, only the position
						var pos = mElements[i].getPosition();					 
						xminMeasured = Math.min(xminMeasured, pos[0]);
						xmaxMeasured = Math.max(xmaxMeasured, pos[0]);
						yminMeasured = Math.min(yminMeasured, pos[1]);
						ymaxMeasured = Math.max(ymaxMeasured, pos[1]);							
					} else { // get bounds
						var bounds = mElements[i].getAbsoluteBounds();					 
						xminMeasured = Math.min(xminMeasured, bounds.left);
						xmaxMeasured = Math.max(xmaxMeasured, bounds.right);
						yminMeasured = Math.min(yminMeasured, bounds.bottom);
						ymaxMeasured = Math.max(ymaxMeasured, bounds.top);
					}
				}
			}
		}
				
		// update meausured dimensions
		var updatedX = false;
		if (self.getAutoScaleX())  { // auto-scale X
			// measured dimensions bigger than preferred dimensions  
			if(!isNaN(mWorld.xminPreferred)) xminMeasured = Math.min(xminMeasured,mWorld.xminPreferred);
			if(!isNaN(mWorld.xmaxPreferred)) xmaxMeasured = Math.max(xmaxMeasured,mWorld.xmaxPreferred);
			// measured dimensions always positive  
      		if (xmaxMeasured <= xminMeasured) {
				// measuared dimensions not valid values for X
				updatedX = ((mWorld.xminMeasured != xmindef) || (mWorld.xmaxMeasured != xmaxdef));
				mWorld.xminMeasured = xmindef;
				mWorld.xmaxMeasured = xmaxdef;							
      		} else {
				// update measured dimensions
				updatedX = ((mWorld.xminMeasured != xminMeasured) || (mWorld.xmaxMeasured != xmaxMeasured)); 
				mWorld.xminMeasured = xminMeasured;
				mWorld.xmaxMeasured = xmaxMeasured;
			}			
		} else if (!isNaN(mWorld.xminPreferred) && !isNaN(mWorld.xmaxPreferred)) { // preferred X
			// update measured dimensions 
			updatedX = ((mWorld.xminMeasured != mWorld.xminPreferred) || (mWorld.xmaxMeasured != mWorld.xmaxPreferred));
			mWorld.xminMeasured = mWorld.xminPreferred;
			mWorld.xmaxMeasured = mWorld.xmaxPreferred;			
		} else { // no values for X
			updatedX = ((mWorld.xminMeasured != xmindef) || (mWorld.xmaxMeasured != xmaxdef));
			mWorld.xminMeasured = xmindef;
			mWorld.xmaxMeasured = xmaxdef;													
		}
		
		var updatedY = false;
		if (self.getAutoScaleY())  { // auto-scale Y
			// measured dimensions bigger than preferred dimensions  
			if(!isNaN(mWorld.yminPreferred)) yminMeasured = Math.min(yminMeasured,mWorld.yminPreferred);
			if(!isNaN(mWorld.ymaxPreferred)) ymaxMeasured = Math.max(ymaxMeasured,mWorld.ymaxPreferred);
			// measured dimensions always positive  
      		if (ymaxMeasured <= yminMeasured) {
				// measuared dimensions not valid values for Y
				updatedY = ((mWorld.yminMeasured != ymindef) || (mWorld.ymaxMeasured != ymaxdef));
				mWorld.yminMeasured = ymindef;
				mWorld.ymaxMeasured = ymaxdef;							
      		} else {
				// update measured dimensions
				updatedY = ((mWorld.yminMeasured != yminMeasured) || (mWorld.ymaxMeasured != ymaxMeasured)); 
				mWorld.yminMeasured = yminMeasured;
				mWorld.ymaxMeasured = ymaxMeasured;
			}			
		} else if (!isNaN(mWorld.yminPreferred) && !isNaN(mWorld.ymaxPreferred)) { // preferred Y
			// update measured dimensions 
			updatedY = ((mWorld.yminMeasured != mWorld.yminPreferred) || (mWorld.ymaxMeasured != mWorld.ymaxPreferred));
			mWorld.yminMeasured = mWorld.yminPreferred;
			mWorld.ymaxMeasured = mWorld.ymaxPreferred;			
		} else { // no values for Y
			updatedY = ((mWorld.yminMeasured != ymindef) || (mWorld.ymaxMeasured != ymaxdef));
			mWorld.yminMeasured = ymindef;
			mWorld.ymaxMeasured = ymaxdef;													
		}
		
		return (updatedX || updatedY);
	};
	
	// ----------------------------------------
	// Drawing functions
	// ----------------------------------------

	/***
	 * Reset the scene
	 * @method reset
	 */
	self.reset = function() {
		mGraphics.reset();
	};

	/***
	 * Disable rendering
	 * @method disable
	 */
	self.disable = function() {
		mEnabledRedering = false;
	}
	
	/***
	 * Enable rendering
	 * @method enable
	 */
	self.enable = function() {
		mEnabledRedering = true;
	}	
	
	/***
	 * Render the scene
	 * @method render
	 */
	self.render = function() {
		if(mEnabledRedering) {
			var reseted = false;
			if (mGraphics.setImageData || mElementsChanged) { 
				// whether canvas or elements added or removed, reset the scene
				mGraphics.reset();
				reseted = true;
				mElementsChanged = false;
			}
	
	        // check for data collection
			for (var i = 0, n = mCollectersList.length; i < n; i++)
				mCollectersList[i].dataCollected();
				
			// get measured dimensions
			var measuredWorld = self.checkMeasure();
	
			if (mMustScale || measuredWorld) // recompute scales
				self.recomputeScales();
	
			if (mPanelChanged || reseted) {// whether panel changed or reseted
				// draw self
				mGraphics.drawPanel(self);
			}
	
			// draw the bottom decorations
			mGraphics.draw(mBottomDecorations);
	
			// draw visible elements
			mGraphics.draw(mElements, reseted);
	
			if (mPanelChanged || reseted) {// whether panel changed or reseted
				// draw the gutter region
				mGraphics.drawGutters(self);
			}
	
			// draw the top decorations
			mGraphics.draw(mTopDecorations);
	
			// set changed to false
			mPanelChanged = false;
			for (var i = 0, n = mElements.length; i < n; i++)
				mElements[i].setChanged(false);
		}
	};

	// ----------------------------------------
	// Interaction
	// ----------------------------------------

	/***
	 * Return Panel Interaction
	 * @method getPanelInteraction
	 * @param panel interaction
	 */
	self.getPanelInteraction = function() {
		return mInteraction;
	};

	/***
	 * Returns the controller object
	 * @method getController
	 * @return Controller
	 */
	self.getController = function() {
		return mController;
	};

	/***
	 * Set the controller
	 * @method setController
	 * @param Controller
	 */
	self.setController = function(controller) {
		mController = controller;
	};

	// ----------------------------------------------------
	// Properties
	// ----------------------------------------------------

	self.getOnMoveHandler = null;

	self.getOnZoomHandler = function() {
		if (mEnabledZooming) {
			var delta = mInteraction.getInteractionZoomDelta();
			var bounds = self.getWorldCoordinates();
			// if not world coordinates, then get real world coordinates
			if(isNaN(bounds[0]) || isNaN(bounds[1]) || isNaN(bounds[2]) || isNaN(bounds[3]))
				bounds = self.getMeasuredCoordinates();
			var incw = +((1 - mZoomRate) * (bounds[1]-bounds[0]) * delta / 2);
			var inch = +((1 - mZoomRate) * (bounds[3]-bounds[2]) * delta / 2);
			// check limits
			var neww = Math.abs(bounds[0] - bounds[1]) -  2*incw;
			if ( neww > mZoomLimits[0] && neww < mZoomLimits[1]) {				
				// set new world coordinates
				self.setWorldCoordinates([
					bounds[0] + incw, bounds[1] - incw,
					bounds[2] + inch, bounds[3] - inch			 
				]);			
				self.getController().propertiesChanged("MinimumX","MaximumX","MinimumY","MaximumY","Bounds");
			}
		}
	}

	self.getOnPressHandler = function() {
		// info text decoration
		if (mShowCoordinates) {
			var point = mInteraction.getInteractionPoint();
			var bounds = self.getRealWorldCoordinates();
			if ((bounds[0] < point[0]) && (bounds[1] > point[0]) &&
				(bounds[3] > point[1]) && (bounds[2] < point[1])) { // whether event location over panel
				mCoorDecoration.setText(point);
				mCoorDecoration.setVisible(true);
				mCoorDecoration.getController().propertiesChanged("Visible", "Text");	
			}
		}		
	};

	self.getOnDragHandler = function() {
		// info text decoration
		if (mShowCoordinates) {
			var point = mInteraction.getInteractionPoint();
			var bounds = self.getRealWorldCoordinates();
			if ((bounds[0] < point[0]) && (bounds[1] > point[0]) &&
				(bounds[3] > point[1]) && (bounds[2] < point[1])) { // whether event location over panel
				mCoorDecoration.setText(point);
				mCoorDecoration.setVisible(true);
				mCoorDecoration.getController().propertiesChanged("Visible", "Text");			
			}
		}
		
		// update drag decoration
		if (mShowAreaRectangle) {
			var boundsInter = mInteraction.getInteractionBounds();
			if(boundsInter.length > 0 && mTypeScaleX == EJSS_DRAWING2D.DrawingPanel.SCALE_NUM && 
					mTypeScaleY == EJSS_DRAWING2D.DrawingPanel.SCALE_NUM) {
				mDragDecoration.setBounds(boundsInter);
				mDragDecoration.setVisible(true);
				mDragDecoration.getController().propertiesChanged("Position", "X", "Y", "Size", "SizeX", "SizeY", "Visible");
			} else {
				mDragDecoration.setVisible(false);		
				mDragDecoration.getController().propertiesChanged("Visible");			
			}
		}
		
		// drag drawing
		if (mEnabledDragging != 0 &&  mEnabledDragging != 4) {
			var distance = mInteraction.getInteractionDistance();
			if(distance.length > 0 && mTypeScaleX == EJSS_DRAWING2D.DrawingPanel.SCALE_NUM && 
					mTypeScaleY == EJSS_DRAWING2D.DrawingPanel.SCALE_NUM) {
				var bounds = self.getWorldCoordinates();
				// if not world coordinates, then get measured coordinates
				if(isNaN(bounds[0]) || isNaN(bounds[1]) || isNaN(bounds[2]) || isNaN(bounds[3]))
					bounds = self.getMeasuredCoordinates();				
				if (mEnabledDragging == 1) {
					self.setWorldCoordinates([ bounds[0] + distance[0], bounds[1] + distance[0],
						bounds[2] + distance[1], bounds[3] + distance[1]]); 					
				} else if (mEnabledDragging == 2) {
					self.setWorldCoordinates([bounds[0] + distance[0],bounds[1] + distance[0], bounds[2], bounds[3]]); 
				} else {
					self.setWorldCoordinates([bounds[0], bounds[1], bounds[2] + distance[1], bounds[3] + distance[1]]); 
				}
				self.getController().propertiesChanged("MinimumX","MaximumX","MinimumY","MaximumY","Bounds");
			}					
		}
	};

	self.getOnReleaseHandler = function() {
		// info text decoration
		if (mShowCoordinates) {
			mCoorDecoration.setVisible(false);
			mCoorDecoration.getController().propertiesChanged("Visible");
		}
				
		// hidden drag decoration
		if (mShowAreaRectangle) {
			mDragDecoration.setVisible(false);		
			mDragDecoration.getController().propertiesChanged("Visible");
		}
	};

	self.getMessageDecoration = function(whichOne) {
		switch(whichOne) {
			case "TL" :
				return mTLMessageDecoration;
			case "BR" :
				return mBRMessageDecoration;
			case "BL" :
				return mBLMessageDecoration;
		}
		return mTRMessageDecoration;
	};

	self.getCoordinates = function() {
		return mCoorDecoration;
	};

	self.getInteraction = function() {
		return mInteraction;
	};

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.DrawingPanel.registerProperties(self, controller);
	};

  	/***
   	* Get JSON object with private variables
   	* @method serialize
   	* @visibility private
   	*/
  	self.serialize = function() {
  	  return { 
		mStyle: mStyle.serialize(),
		
		mTopDecorations: mTopDecorations,
		mBottomDecorations: mBottomDecorations,
		mElements: mElements,
		mCollectersList: mCollectersList,
		
		mAutoScaleX: mAutoScaleX, mAutoScaleY: mAutoScaleY,
		mInvertedScaleY: mInvertedScaleY, mTypeScaleX: mTypeScaleX,
		mTypeScaleY: mTypeScaleY, mShowCoordinates: mShowCoordinates, 
		
		xmindef: xmindef, xmaxdef: xmaxdef, ymindef: ymindef, ymaxdef: ymaxdef,
		
		mWorld: mWorld,		
		mGutters: mGutters,
		mGuttersStyle: mGuttersStyle.serialize(),
				
		// mInteraction: mInteraction.serialize() 
	  };
  	}
  
  	/***
   	* Set JSON object with private variables
   	* @method unserialize
   	* @parem json JSON object
   	* @visibility private
   	*/
  	self.unserialize = function(json) {
		mStyle.unserialize(json.mStyle), 

		// not support references changing
		// mBottomDecorations = json.mBottomDecorations,
		// mTopDecorations = json.mTopDecorations,
		// mElements = json.mElements,
		// mCollectersList = json.mCollectersList,
		
		mAutoScaleX = json.mAutoScaleX, mAutoScaleY = json.mAutoScaleY,
		mInvertedScaleY = json.mInvertedScaleY, mTypeScaleX = json.mTypeScaleX,
		mTypeScaleY = json.mTypeScaleY, mShowCoordinates = json.mShowCoordinates, 
		
		xmindef = json.xmindef, xmaxdef = json.xmaxdef, ymindef = json.ymindef, ymaxdef = json.ymaxdef,
		
		mWorld = json.mWorld,		
		mGutters = json.mGutters,
		mGuttersStyle.unserialize(json.mGuttersStyle),
		
		// mInteraction = json.mInteraction 
	
		mPanelChanged = true;
  	}

	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	// set graphics mode
	self.setGraphicsMode(mGraphicsMode);

	// style
	mStyle.setLineColor('black');
	mStyle.setFillColor('rgb(239,239,255)');
	mStyle.setChangeListener(function(change) {
		mPanelChanged = true;
	});

	mGuttersStyle.setChangeListener(function(reporter) {
		mPanelChanged = true;
	});

	var mDragDecoration = EJSS_DRAWING2D.shape(mName + "__cursorBox__");
	mDragDecoration.setShapeType(EJSS_DRAWING2D.Shape.RECTANGLE);
	mDragDecoration.getStyle().setDrawFill(false);
	mDragDecoration.getStyle().setLineColor("black");
	mDragDecoration.setRelativePosition("CENTER");
	mDragDecoration.setVisible(false);
	self.addDecoration(mDragDecoration, 0, true);

	var mTLMessageDecoration = EJSS_DRAWING2D.text(mName + "__tlmessage__");
	mTLMessageDecoration.setRelativePosition("NORTH_WEST");
	mTLMessageDecoration.getStyle().setDrawLines(true);
	mTLMessageDecoration.getStyle().setDrawFill(true);
	mTLMessageDecoration.getStyle().setFillColor("yellow");
	mTLMessageDecoration.getStyle().setLineColor("black");
	mTLMessageDecoration.getFont().setFontSize(12);
	mTLMessageDecoration.setPosition([mWorld.xmin, mWorld.ymax]);
	mTLMessageDecoration.setFramed(true);
	mTLMessageDecoration.panelChangeListener = function(event) {
		if (event == "bounds") {
			if(self.getInvertedScaleY())
				mTLMessageDecoration.setPosition([mWorld.xmin, mWorld.ymin]);
			else
				mTLMessageDecoration.setPosition([mWorld.xmin, mWorld.ymax]);	
		}			
	};
	self.addDecoration(mTLMessageDecoration, 0, true);

	var mBLMessageDecoration = EJSS_DRAWING2D.text(mName + "__blmessage__");
	mBLMessageDecoration.setRelativePosition("SOUTH_WEST");
	mBLMessageDecoration.getStyle().setDrawLines(true);
	mBLMessageDecoration.getStyle().setDrawFill(true);
	mBLMessageDecoration.getStyle().setFillColor("yellow");
	mBLMessageDecoration.getStyle().setLineColor("black");
	mBLMessageDecoration.getFont().setFontSize(12);
	mBLMessageDecoration.setPosition([mWorld.xmin, mWorld.ymin]);
	mBLMessageDecoration.setFramed(true);
	mBLMessageDecoration.panelChangeListener = function(event) {
		if (event == "bounds")
			if(self.getInvertedScaleY())
				mBLMessageDecoration.setPosition([mWorld.xmin, mWorld.ymax]);
			else		
				mBLMessageDecoration.setPosition([mWorld.xmin, mWorld.ymin]);
	};
	self.addDecoration(mBLMessageDecoration, 0, true);

	var mBRMessageDecoration = EJSS_DRAWING2D.text(mName + "__brmessage__");
	mBRMessageDecoration.setRelativePosition("SOUTH_EAST");
	mBRMessageDecoration.getStyle().setDrawLines(true);
	mBRMessageDecoration.getStyle().setDrawFill(true);
	mBRMessageDecoration.getStyle().setFillColor("yellow");
	mBRMessageDecoration.getStyle().setLineColor("black");
	mBRMessageDecoration.getFont().setFontSize(12);
	mBRMessageDecoration.setPosition([mWorld.xmax, mWorld.ymin]);
	mBRMessageDecoration.setFramed(true);
	mBRMessageDecoration.panelChangeListener = function(event) {
		if (event == "bounds")
			if(self.getInvertedScaleY())
				mBRMessageDecoration.setPosition([mWorld.xmax, mWorld.ymax]);
			else
				mBRMessageDecoration.setPosition([mWorld.xmax, mWorld.ymin]);
	};
	self.addDecoration(mBRMessageDecoration, 0, true);

	var mTRMessageDecoration = EJSS_DRAWING2D.text(mName + "__trmessage__");
	mTRMessageDecoration.setRelativePosition("NORTH_EAST");
	mTRMessageDecoration.getStyle().setDrawLines(true);
	mTRMessageDecoration.getStyle().setDrawFill(true);
	mTRMessageDecoration.getStyle().setFillColor("yellow");
	mTRMessageDecoration.getStyle().setLineColor("black");
	mTRMessageDecoration.getFont().setFontSize(12);
	mTRMessageDecoration.setPosition([mWorld.xmax, mWorld.ymax]);
	mTRMessageDecoration.setFramed(true);
	mTRMessageDecoration.panelChangeListener = function(event) {
		if (event == "bounds")
			if(self.getInvertedScaleY())
				mTRMessageDecoration.setPosition([mWorld.xmax, mWorld.ymin]);
			else
				mTRMessageDecoration.setPosition([mWorld.xmax, mWorld.ymax]);
	};
	self.addDecoration(mTRMessageDecoration, 0, true);

	var mCoorDecoration = EJSS_DRAWING2D.infoText(mName + "__coor__");
	mCoorDecoration.setRelativePosition("SOUTH_WEST");
	mCoorDecoration.getFont().setFontSize(12);
	mCoorDecoration.getStyle().setFillColor("yellow");
	mCoorDecoration.setPosition([mWorld.xmin, mWorld.ymin]);
	mCoorDecoration.setFormat("x:0.##,y:0.##");
	mCoorDecoration.setFramed(true);
	mCoorDecoration.setVisible(false);
	mCoorDecoration.panelChangeListener = function(event) {
		if (event == "bounds")
			if(self.getInvertedScaleY())
				mCoorDecoration.setPosition([mWorld.xmin, mWorld.ymax]);
			else		
				mCoorDecoration.setPosition([mWorld.xmin, mWorld.ymin]);
	};
	self.addDecoration(mCoorDecoration, 0, true);

	self.getGuttersStyle().setLineColor('black');
	self.getGuttersStyle().setFillColor('rgb(239,239,255)');
	self.getGuttersStyle().setShapeRendering("RENDER_CRISPEDGES");

	// interactions handler
	var mInteraction = EJSS_DRAWING2D.panelInteraction(self);	

//	mGraphics.getEventContext().addEventListener("dblclick", function(event) { event.preventDefault(); event.stopPropagation(); return false; } ); 
    mGraphics.getEventContext().addEventListener("dblclick", function(event){ event.preventDefault(); event.stopPropagation(); return false; }, true);
//	window.document.addEventListener("dblclick", function(event) { event.preventDefault(); event.stopPropagation(); return false; } ); 

	return self;
};

