/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * Arrow is a class to display vectors
 * @class EJSS_DRAWING2D.Arrow 
 * @parent EJSS_DRAWING2D.Element
 * @constructor  
 */
EJSS_DRAWING2D.Arrow = {
    NONE : 0,
    ANGLE : 1,
    CIRCLE : 2,
    RECTANGLE : 3,
    LINE : 4,
    CURVE : 5,
    TRIANGLE: 6,
    DIAMOND: 7,     
    WEDGE: 8,
	POINTED: 9,
	INVANGLE: 10,
    INVTRIANGLE: 11,
    
    SPLINE_NONE : 0,
    SPLINE_CUBIC : 1,
    SPLINE_QUADRATIC : 2,
    SPLINE_CUBIC_WITH_MIDDLE_POINT : 3,
    SPLINE_QUADRATIC_WITH_MIDDLE_POINT : 4,
    SPLINE_CUBIC_HORIZONTAL : 10,
    SPLINE_CUBIC_VERTICAL : 11,
    SPLINE_START: 20,
    SPLINE_START_HORIZONTAL: 21,
    SPLINE_START_VERTICAL: 22,
    SPLINE_END: 30,
    SPLINE_END_HORIZONTAL: 31,
    SPLINE_END_VERTICAL: 32,	   
 	
    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy
  	
      dest.setRelativePosition(source.getRelativePosition());
      dest.setMarkEnd(source.getMarkEnd());
      dest.setMarkEndWidth(source.getMarkEndWidth());
      dest.setMarkEndHeight(source.getMarkEndHeight());
      dest.setMarkEndColor(source.getMarkEndColor());
      dest.setMarkEndStroke(source.getMarkEndStroke());
      dest.setMarkEndOrient(source.getMarkEndOrient());
      dest.setMarkStart(source.getMarkStart());
      dest.setMarkStartWidth(source.getMarkStartWidth());
      dest.setMarkStartHeight(source.getMarkStartHeight());
	  dest.setMarkStartColor(source.getMarkStartColor());
	  dest.setMarkStartStroke(source.getMarkStartStroke());
      dest.setMarkStartOrient(source.getMarkStartOrient());

      dest.setMarkMiddle(source.getMarkMiddle());
      dest.setMarkMiddleWidth(source.getMarkMiddleWidth());
      dest.setMarkMiddleHeight(source.getMarkMiddleHeight());
      dest.setMarkMiddleColor(source.getMarkMiddleColor());
      dest.setMarkMiddleColorSnd(source.getMarkMiddleColorSnd());
      dest.setMarkMiddleStroke(source.getMarkMiddleStroke());
      dest.setMarkMiddleOrient(source.getMarkMiddleOrient());
      dest.setMarkProportion(source.getMarkProportion());

      dest.setSplineType(source.getSplineType());

  	},


    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class
      
      controller.registerProperty("SplineType",element.setSplineType);

      controller.registerProperty("InteractionPosition", element.setInteractionPosition);
      controller.registerProperty("Offset", element.setRelativePosition, element.getRelativePosition);
      controller.registerProperty("MarkEnd",element.setMarkEnd);
      controller.registerProperty("MarkEndWidth",element.setMarkEndWidth);
      controller.registerProperty("MarkEndHeight",element.setMarkEndHeight);
      controller.registerProperty("MarkEndDiameter",element.setMarkEndDiameter);
      controller.registerProperty("MarkEndColor",element.setMarkEndColor);     
      controller.registerProperty("MarkEndStroke",element.setMarkEndStroke);
      /*** 
	  * Orientation of the marker. Values are "auto" or the angle to rotate the marker 
	  * @property MarkEndOrient 
	  * @type int|String
	  * @default "auto"
	  */ 
      controller.registerProperty("MarkEndOrient",element.setMarkEndOrient);
      
      controller.registerProperty("MarkStart",element.setMarkStart);
      controller.registerProperty("MarkStartWidth",element.setMarkStartWidth);
      controller.registerProperty("MarkStartHeight",element.setMarkStartHeight);
      controller.registerProperty("MarkStartDiameter",element.setMarkStartDiameter);
	  controller.registerProperty("MarkStartColor",element.setMarkStartColor);
	  controller.registerProperty("MarkStartStroke",element.setMarkStartStroke);
      /*** 
	  * Orientation of the marker. Values are "auto" or the angle to rotate the marker 
	  * @property MarkStartOrient 
	  * @type int|String
	  * @default "auto"
	  */ 
	  controller.registerProperty("MarkStartOrient",element.setMarkStartOrient);

      controller.registerProperty("MarkMiddle",element.setMarkMiddle);
      controller.registerProperty("MarkMiddleWidth",element.setMarkMiddleWidth);
      controller.registerProperty("MarkMiddleHeight",element.setMarkMiddleHeight);
      controller.registerProperty("MarkMiddleDiameter",element.setMarkMiddleDiameter);
      controller.registerProperty("MarkMiddleColor",element.setMarkMiddleColor);
      controller.registerProperty("MarkMiddleColorSnd",element.setMarkMiddleColorSnd);
      controller.registerProperty("MarkMiddleStroke",element.setMarkMiddleStroke);
      /*** 
	  * Orientation of the marker. Values are "auto" or the angle to rotate the marker 
	  * @property MarkMiddleOrient 
	  * @type int|String
	  * @default "auto"
	  */ 
      controller.registerProperty("MarkMiddleOrient",element.setMarkMiddleOrient);
      
      controller.registerProperty("MarkProportion",element.setMarkProportion);
    },  
};

/**
 * Creates a 2D Arrow
 * @method arrow
 */
EJSS_DRAWING2D.arrow = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  // Implementation variables
  var mMarkProportion = 0.5;
  var mMarkStart = EJSS_DRAWING2D.Arrow.NONE;
  var mMarkStartColor = "none";
  var mMarkStartWidth = 12;
  var mMarkStartHeight = 10;
  var mMarkStartStroke = -1;
  var mMarkStartOrient = "auto";
  var mMarkEnd = EJSS_DRAWING2D.Arrow.ANGLE;
  var mMarkEndColor = "none";
  var mMarkEndWidth = 12;
  var mMarkEndHeight = 12;
  var mMarkEndStroke = -1;
  var mMarkEndOrient = "auto";
  var mMarkMiddle = EJSS_DRAWING2D.Arrow.NONE;
  var mMarkMiddleColor = "none";  
  var mMarkMiddleColorSnd = "none";  
  var mMarkMiddleWidth = 12;
  var mMarkMiddleHeight = 10;
  var mMarkMiddleStroke = -1;
  var mMarkMiddleOrient = "auto";
  var mSplineType = EJSS_DRAWING2D.Arrow.SPLINE_NONE;

  self.getClass = function() {
  	return "ElementArrow";
  }

  self.setInteractionPosition = function(position) {
    if (typeof position == "string") position = EJSS_DRAWING2D.Element[position.toUpperCase()];
    var inter = self.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION);
    if (inter.getPositionOffset() != position) {
      inter.setPositionOffset(position);
      self.setChanged(true);
    }	
  }

  self.getInteractionPosition = function() {
    var inter = self.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION);
    return inter.getPositionOffset();
  }
  
  self.setMarkProportion = function(prop) {
    if (mMarkProportion != prop) {
      mMarkProportion = prop;
      self.setChanged(true);
    }
  };

  self.getMarkProportion = function() {
  	return mMarkProportion;
  }	

  self.setMarkStart = function(mark) {
    if (typeof mark === 'string')
      mMarkStart = EJSS_DRAWING2D.Arrow[mark.toUpperCase()];
    else 
      mMarkStart = mark;
  };

  self.getMarkStart = function() {
  	return mMarkStart;
  }	

  self.setMarkStartColor = function(color) { 
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color!=mMarkStartColor) {
      mMarkStartColor = color; 
      self.setChanged(true);
    }
    return self;
  };
    
  self.getMarkStartColor = function() { 
    return mMarkStartColor; 
  };

  self.setMarkStartStroke = function(width) {
    if (mMarkStartStroke != width) {
      mMarkStartStroke = width;
      self.setChanged(true);
    }
  };

  self.getMarkStartStroke = function() {
  	return mMarkStartStroke;
  }	

  self.setMarkStartWidth = function(width) {
    if (mMarkStartWidth != width) {
      mMarkStartWidth = width;
      self.setChanged(true);
    }
  };

  self.getMarkStartWidth = function() {
  	return mMarkStartWidth;
  }	

  self.setMarkStartHeight = function(height) {
    if (mMarkStartHeight != height) {
      mMarkStartHeight = height;
      self.setChanged(true);
    }
  };

  self.getMarkStartHeight = function() {
  	return mMarkStartHeight;
  }	

  self.setMarkStartDiameter = function(diameter) {
    if ((mMarkStartHeight != diameter) || (mMarkStartWidth != diameter)) {
      mMarkStartHeight = diameter;
      mMarkStartWidth = diameter;
      self.setChanged(true);
    }
  };

  self.getMarkStartOrient = function() {
  	return mMarkStartOrient;
  }	

  self.setMarkStartOrient = function(orient) {
    if (mMarkStartOrient != orient) {
      mMarkStartOrient = orient;
      self.setChanged(true);
    }
  };

  self.setMarkEnd = function(mark) {
    if (typeof mark === 'string')
      mMarkEnd = EJSS_DRAWING2D.Arrow[mark.toUpperCase()];
    else 
      mMarkEnd = mark;
  };

  self.getMarkEnd = function() {
  	return mMarkEnd;
  }	

  self.setMarkEndColor = function(color) { 
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color!=mMarkEndColor) {
      mMarkEndColor = color; 
      self.setChanged(true);
    }
    return self;
  };
    
  self.getMarkEndColor = function() { 
    return mMarkEndColor; 
  };
  
  self.setMarkEndStroke = function(width) {
    if (mMarkEndStroke != width) {
      mMarkEndStroke = width;
      self.setChanged(true);
    }
  };

  self.getMarkEndStroke = function() {
  	return mMarkEndStroke;
  }	

  self.setMarkEndWidth = function(width) {
    if (mMarkEndWidth != width) {
      mMarkEndWidth = width;
      self.setChanged(true);
    }
  };

  self.getMarkEndWidth = function() {
  	return mMarkEndWidth;
  }	

  self.setMarkEndHeight = function(height) {
    if (mMarkEndHeight != height) {
      mMarkEndHeight = height;
      self.setChanged(true);
    }
  };

  self.getMarkEndHeight = function() {
  	return mMarkEndHeight;
  }	

  self.setMarkEndDiameter = function(diameter) {
    if ((mMarkEndHeight != diameter) || (mMarkEndWidth != diameter)) {
      mMarkEndHeight = diameter;
      mMarkEndWidth = diameter;
      self.setChanged(true);
    }
  };

  self.getMarkEndOrient = function() {
  	return mMarkEndOrient;
  }	

  self.setMarkEndOrient = function(orient) {
    if (mMarkEndOrient != orient) {
      mMarkEndOrient = orient;
      self.setChanged(true);
    }
  };

  self.setMarkMiddle = function(mark) {
    if (typeof mark === 'string')
      mMarkMiddle = EJSS_DRAWING2D.Arrow[mark.toUpperCase()];
    else 
      mMarkMiddle = mark;
  };

  self.getMarkMiddle = function() {
  	return mMarkMiddle;
  }	

  self.setMarkMiddleColor = function(color) { 
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color!=mMarkMiddleColor) {
      mMarkMiddleColor = color; 
      self.setChanged(true);
    }
    return self;
  };
    
  self.getMarkMiddleColor = function() { 
    return mMarkMiddleColor; 
  };

  self.setMarkMiddleStroke = function(width) {
    if (mMarkMiddleStroke != width) {
      mMarkMiddleStroke = width;
      self.setChanged(true);
    }
  };

  self.getMarkMiddleStroke = function() {
  	return mMarkMiddleStroke;
  }	

  self.setMarkMiddleColorSnd = function(color) { 
    if (typeof color !== "string") color = EJSS_TOOLS.DisplayColors.getLineColor(color);
    if (color!=mMarkMiddleColorSnd) {
      mMarkMiddleColorSnd = color; 
      self.setChanged(true);
    }
    return self;
  };
    
  self.getMarkMiddleColorSnd = function() { 
    return mMarkMiddleColorSnd; 
  };

  self.setMarkMiddleWidth = function(width) {
    if (mMarkMiddleWidth != width) {
      mMarkMiddleWidth = width;
      self.setChanged(true);
    }
  };

  self.getMarkMiddleWidth = function() {
  	return mMarkMiddleWidth;
  }	

  self.setMarkMiddleHeight = function(height) {
    if (mMarkMiddleHeight != height) {
      mMarkMiddleHeight = height;
      self.setChanged(true);
    }
  };

  self.getMarkMiddleHeight = function() {
  	return mMarkMiddleHeight;
  }	

  self.setMarkMiddleDiameter = function(diameter) {
    if ((mMarkMiddleHeight != diameter) || (mMarkMiddleWidth != diameter)) {
      mMarkMiddleHeight = diameter;
      mMarkMiddleWidth = diameter;
      self.setChanged(true);
    }
  };

  self.getMarkMiddleOrient = function() {
  	return mMarkMiddleOrient;
  }	

  self.setMarkMiddleOrient = function(orient) {
    if (mMarkMiddleOrient != orient) {
      mMarkMiddleOrient = orient;
      self.setChanged(true);
    }
  };

  // ----------------------------------------------------
  // SVG Splines
  // ----------------------------------------------------

  
  /**
   * 
   */
  self.setSplineType = function(spline) {
    if (mSplineType != spline) {
      mSplineType = spline;
      self.setChanged(true);
    }
  };

  self.getSplineType = function() {
    return mSplineType;
  } 

  // ----------------------------------------------------
  // Properties and copy
  // ----------------------------------------------------

  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Arrow.registerProperties(self,controller);
  };
  
  self.copyTo = function(element) {
    EJSS_DRAWING2D.Arrow.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([0.1,0.1]);
  self.setRelativePosition("SOUTH_WEST");
  // self.getInteractionTarget(EJSS_DRAWING2D.PanelInteraction.TARGET_POSITION).setPositionOffset("SOUTH_WEST");

  return self;
};

