/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * ByteRaster
 * @class ByteRaster 
 * @constructor  
 */
EJSS_DRAWING2D.ByteRaster = {	
	
	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Data", element.setData, element.getData);
		controller.registerProperty("NumColors", element.getColorMapper().setNumberOfColors);
		controller.registerProperty("Colors", element.getColorMapper().setColorPalette);
		controller.registerProperty("Palette", element.getColorMapper().setPaletteType);	
        controller.registerProperty("AutoUpdate", element.setAutoupdate);  
        controller.registerProperty("RGBData", element.setRGBData);  
	}
			
};

/**
 * Creates a 2D ByteRaster
 * @method byteRaster
 */
EJSS_DRAWING2D.byteRaster = function(name) {
	var self = EJSS_DRAWING2D.element(name);
 
 	var mColorMapper = EJSS_DRAWING2D.colorMapper(20, EJSS_DRAWING2D.ColorMapper.REDBLUE_SHADE); 	
	var mData = [];
	var mDataChanged = false;
	var mAutoUpdate=true;
	var mRGBData = false;
 
	self.getClass = function() {
		return "ElementByteRaster";
	}

	/** 
	 * @method setData
	 * @param data
	 */
	self.setData = function (data) {
	  if (mAutoUpdate || mData != data) {
	  		mData = data;
	  		mDataChanged = true;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getData
	 * @return
	 */
	self.getData = function() { 
		return mData; 
	}
	 
  self.setRGBData= function (rgbData) {
    mRGBData = rgbData;
    if (mAutoUpdate) mDataChanged = true;
  }

  self.isRGBData= function () {
    return mRGBData;
  }
	 
  self.setAutoupdate= function (auto) {
    mAutoUpdate = auto;
    if (mAutoUpdate) mDataChanged = true;
  }
	 
	/** 
	 * @method setColorMapper
	 * @param colormapper
	 */
	self.setColorMapper = function (colormapper) {
	  	if(mColorMapper != colormapper) {
	  		mColorMapper = colormapper;
	  		mDataChanged = true;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getColorMapper
	 * @return
	 */
	self.getColorMapper = function() { 
		return mColorMapper; 
	}
	 
	self.setIndexedColor = function (value) {
	  //console.log ("Value = "+value);
	  var color = value.color; 
	  var colorStr = (color[3]===undefined) ? "rgb("+color[0]+","+color[1]+","+color[2]+")" : "rgb("+color[0]+","+color[1]+","+color[2]+","+color[3]+")";
	  console.log(self.getName()+": setIndexedColor("+value.index+","+colorStr+")");
	  var colors = mColorMapper.getColors();
	  colors[value.index] = colorStr;  
	};
	
	self.getDataChanged = function() {
		return mDataChanged;
	}

	self.setDataChanged = function(dataChanged) {
		mDataChanged = dataChanged;
	}

  self.invalidate = function() {
    mDataChanged = true;
  }

	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.ByteRaster.registerProperties(self, controller);
	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	mColorMapper.setChangeListener(function (change) { self.setChanged(true); });
	return self;
};

 