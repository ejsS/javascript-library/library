/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

//---------------------------------
//TrailSet
//---------------------------------

/**
 * TrailSet
 * @class TrailSet 
 * @constructor  
 */
EJSS_DRAWING2D.TrailSet = {
  
    /**
     * static registerProperties method
     */
    registerProperties : function(set,controller) {
      var ElementSet = EJSS_DRAWING2D.ElementSet;
      
      ElementSet.registerProperties(set,controller);
      controller.registerProperty("Active", 
          function(v) { set.setToEach(function(element,value) { element.setActive(value); }, v); }
      );
      controller.registerProperty("NoRepeat", 
          function(v) { set.setToEach(function(element,value) { element.setNoRepeat(value); }, v); }
      );           
      controller.registerProperty("ClearAtInput", 
          function(v) { set.setToEach(function(element,value) { element.setClearAtInput(value); }, v); }
      );           
      controller.registerProperty("Skip", 
          function(v) { set.setToEach(function(element,value) { element.setSkip(value); }, v); }
      );           
      controller.registerProperty("ColumnNames", 
          function(v) { set.setToEach(function(element,value) { element.setInputLabels(value); }, v); }
      );           
      controller.registerProperty("Maximum", 
          function(v) { set.setToEach(function(element,value) { element.setMaximumPoints(value); }, v); }
      );           
      controller.registerProperty("ConnectionType", 
          function(v) { set.setToEach(function(element,value) { element.setConnectionType(value); }, v); }
      );           
      controller.registerProperty("Connected", 
          function(v) { set.setToEach(function(element,value) { element.setConnected(value); }, v); }
      );           
      controller.registerProperty("Points", 
          function(v) { set.setToEach(function(element,value) { element.setPoints(value); }, v); }
      );           
      controller.registerProperty("LastPoint", 
          function(v) { set.setToEach(function(element,value) { element.addPoint(value); }, v); }
      );           
      controller.registerProperty("Input", 
          function(v) { set.setToEach(function(element,value) { element.addPoints(value); }, v); }
      );           
      controller.registerProperty("InputX", 
          function(v) { set.setToEach(function(element,value) { element.addXPoints(value); }, v); }
      );           
      controller.registerProperty("InputY", 
          function(v) { set.setToEach(function(element,value) { element.addYPoints(value); }, v); }
      );           
      
    }    
};


/**
 * Creates a set of Segments
 * @method trailSet
 * @param mView
 * @param mName
 */
EJSS_DRAWING2D.trailSet = function (mName) {
  var self = EJSS_DRAWING2D.elementSet(EJSS_DRAWING2D.trail, mName);

  // Static references
  var TrailSet = EJSS_DRAWING2D.TrailSet;		// reference for TrailSet
  
  // ----------------------------------------------------
  // Set actions
  // ----------------------------------------------------

  /**
   * Resets all elements
   * @method reset
   */
  self.reset = function() {
    var elementList = self.getElements();
    for (var i=0,n=elementList.length;i<n;i++) elementList[i].reset();
  }
  
  /**
   * Initializes all elements
   * @method clear
   */
  self.initialize = function() {
    var elementList = self.getElements();
    for (var i=0,n=elementList.length;i<n;i++) elementList[i].initialize();
  }
  
  /**
   * Clears all elements
   * @method clear
   */
  self.clear = function() {
    var elementList = self.getElements();
    for (var i=0,n=elementList.length;i<n;i++) elementList[i].clear();
  }
  
  // ----------------------------------------------------
  // Custom methods
  // ----------------------------------------------------
  
  /**
   * Creates a new segment of one of the trails in the set
   * @method newSegment
   */
    self.newSegment = function(index) {
      self.getElements()[index].newSegment();
    }
  
  /**
   * Moves to the new point without drawing.
   * (Equivalent to setting the connection type
   * to NO_CONNECTION and adding one single point, then setting the 
   * type back to its previous value.)
   * @method moveToPoint
   * @param index int The index of the trail to move to 
   * @param x double The X coordinate of the point.
   * 	or point double[] The double[2] array with the coordinates of the point.
   * @param y double The Y coordinate of the point.
   */
   self.moveToPoint = function (index, x, y) {
      self.getElements()[index].moveToPoint(x,y);
    }
    
  /**
   * Adds a new point to one of the trails in the set
   * @method addPoint
   * @param index int The index of the trail to add to 
   * @param x double The X coordinate of the point 
   * 		or point double[] The double[2] array with the coordinates of the point.
   * @param y double The Y coordinate of the point.
   * @param style
   */
   self.addPoint = function (index, x, y, style) {
      self.getElements()[index].addPoint(x,y,style);
    }
  
  /**
   * Adds an array of points to one of the trails in the set
   * @method addPoints
   * @param index int The index of the trail to add to 
   * @param x double The double[] array with the X coordinates of the points.
   * 	or  point double[][] The double[nPoints][2] array with the coordinates of the points.
   * @param y double The double[] array with the Y coordinates of the points.
   */
    self.addPoints = function (index, x, y) {
      self.getElements()[index].addPoints(x,y);
    }
  

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------
  
  self.registerProperties = function(controller) {
    TrailSet.registerProperties(self,controller);
  };

  return self;
};