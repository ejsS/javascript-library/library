/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * An Axis is the 2D drawable used by PlottingPanels as decoration  
 * @class EJSS_DRAWING2D.Axis 
 * @parent EJSS_DRAWING2D.Segment
 * @constructor  
 */
EJSS_DRAWING2D.Axis = {
	// axis orientation 
	AXIS_VERTICAL   : 0,
	AXIS_HORIZONTAL : 1,
	// ticks position (reference for text)
	TICKS_UP   : 2,
	TICKS_DOWN : 3,
	// ticks increment
	SCALE_NUM	: 0,	// decimal
	SCALE_LOG	: 1,	// logarithmic

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

		// ticks
		controller.registerProperty("Step", element.setStep, element.getStep);
		controller.registerProperty("TickStep", element.setTickStep, element.getTickStep);
		controller.registerProperty("Ticks", element.setTicks, element.getTicks);
		controller.registerProperty("AutoTicks", element.setAutoTicks, element.getAutoTicks);				
		controller.registerProperty("FixedTick", element.setFixedTick, element.getFixedTick);				

		// axis properties
		controller.registerProperty("Orient", element.setOrient, element.getOrient);
		controller.registerProperty("Scale", element.setScale, element.getScale);
		
		// ticks properties
		controller.registerProperty("TicksMode", element.setTicksMode, element.getTicksMode);
		controller.registerProperty("TicksSize", element.setTicksSize, element.getTicksSize);
	
		// auto ticks		
		controller.registerProperty("AutoStepMin", element.setAutoStepMin, element.getAutoStepMin);				
		controller.registerProperty("AutoTicksRange", element.setAutoTicksRange, element.getAutoTicksRange);				
		
		// ticks text properties
		controller.registerProperty("TextPosition", element.setTextPosition, element.getTextPosition);
		controller.registerProperty("ScalePrecision", element.setScalePrecision, element.getScalePrecision);		
		controller.registerProperty("Font", element.getFont().setFont);
		controller.registerProperty("FontFamily", element.getFont().setFontFamily);
		controller.registerProperty("FontSize", element.getFont().setFontSize);
		controller.registerProperty("LetterSpacing", element.getFont().setLetterSpacing);
		controller.registerProperty("FontOutlineColor", element.getFont().setOutlineColor);
		controller.registerProperty("FontWeight", element.getFont().setFontWeight);
		controller.registerProperty("FontFillColor", element.getFont().setFillColor);		
		
		// show
		controller.registerProperty("Show", element.setShow, element.getShow);
	}
			
};

/**
 * Creates a 2D Axis
 * @method axis
 */
EJSS_DRAWING2D.axis = function(mName) {
	var self = EJSS_DRAWING2D.segment(mName);
 
 	// drawing priority: mAutoTicks - mTicks - mStep
 	var mAutoTicks = true;								// auto-ticks
 	var mTicks = 0;										// number of ticks
 	var mStep = 20; 									// step between ticks in pixels
 	var mTickStep = 0; 									// step between ticks in real coordinates
	var mFixedTick = Number.NaN;								// ticks fixed in axis	

	// axis properties	
	var mOrient = EJSS_DRAWING2D.Axis.AXIS_VERTICAL;	// axis orientation
	var mScale = [-1,1];									// axis scale
	var mInvertedScaleY = false;						// whether inverted scale in Y

	// ticks properties
 	var mTicksMode = EJSS_DRAWING2D.Axis.SCALE_NUM;		// axis scale
 	var mTextPosition = EJSS_DRAWING2D.Axis.TICKS_UP;	// tick orientation
	var mTicksSize = 10;								// tick pixel size

	// ticks text properties
	var mScalePrecision = 1;							// number of decimals
	var mFont = EJSS_DRAWING2D.font(mName);				// font for text
 
	// auto ticks 	
	var mAutoStepMin = 40;					// step minimun in pixels
	var mAutoTicksRange = [5,10,20];		// ticks range: 
											//   is the step minimun possible in mAutoTicksRange[length-1]?
											//   and in mAutoTicksRange[length-2]? ... 
											//   then mAutoTicksRange[length-2] is the number of ticks
	// show
	var mShow = true;
	   
	self.getClass = function() {
		return "ElementAxis";
	}

	self.setShow = function(show) {
		if(mShow != show) {
			mShow = show;
			self.setChanged(true);
		}
	}

	self.getShow = function() {
		return mShow;
	}

	self.getFont = function() {
		return mFont;
	}
		
	/** 
	 * @method setAutoTicksRangeX
	 * @param range
	 */
	self.setAutoTicksRange = function (range) {
	  	if(mAutoTicksRange != range) {
	  		mAutoTicksRange = range;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoTicksRange
	 * @return
	 */
	self.getAutoTicksRange = function() { 
		return mAutoTicksRange; 
	}

	/** 
	 * @method setAutoStepMin
	 * @param min
	 */
	self.setAutoStepMin = function (min) {
	  	if(mAutoStepMin != min) {
	  		mAutoStepMin = min;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getAutoStepMin
	 * @return
	 */
	self.getAutoStepMin = function() { 
		return mAutoStepMin; 
	}

	/** 
	 * @method setFixedTick
	 * @param fixed
	 */
	self.setFixedTick = function (fixed) {
	  	if(mFixedTick != fixed) {
	  		mFixedTick = fixed;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getFixedTick
	 * @return
	 */
	self.getFixedTick = function() { 
		return mFixedTick; 
	}
			
	/***
	 * @method setAutoTicks
	 * @param auto
	 */
	self.setAutoTicks = function (auto) {
	  	if(mAutoTicks != auto) {
	  		mAutoTicks = auto;
	  		self.setChanged(true);
	  	}
	}
	  
	/***
	 * @method getAutoTicks
	 * @return
	 */
	self.getAutoTicks = function() { 
		return mAutoTicks; 
	}
			 
	/** 
	 * @method setStep
	 * @param stet
	 */
	self.setStep = function (step) {
		if(mStep != step) {
	  		mStep = step;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getStep
	 * @return
	 */
	self.getStep = function() { 
		return mStep; 
	}

	/** 
	 * @method setTickStep
	 * @param stet
	 */
	self.setTickStep = function (TickStep) {
		if(mTickStep != TickStep) {
	  		mTickStep = TickStep;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getTickStep
	 * @return
	 */
	self.getTickStep = function() { 
		return mTickStep; 
	}
		 
	/** 
	 * @method setTicks
	 * @param ticks
	 */
	self.setTicks = function (ticks) {
	  	if(mTicks != ticks) {
	  		mTicks = ticks;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getTicks
	 * @return
	 */
	self.getTicks = function() { 
		return mTicks; 
	}

	/** 
	 * @method setTicksMode
	 * @param ticksMode
	 */
	self.setTicksMode = function (ticksMode) {
    	if (typeof ticksMode == "string") ticksMode = EJSS_DRAWING2D.Grid[ticksMode.toUpperCase()];
    	if(mTicksMode != ticksMode) {
    		mTicksMode = ticksMode;
    		self.setChanged(true);
    	}	
	}
	  
	/**
	 * @method getTicksMode
	 * @return
	 */
	self.getTicksMode = function() { 
		return mTicksMode; 
	}

	/** 
	 * @method setTextPosition
	 * @param textPosition
	 */
	self.setTextPosition = function (textPosition) {
    	if (typeof textPosition == "string") textPosition = EJSS_DRAWING2D.Axis[textPosition.toUpperCase()];
    	if(mTextPosition != textPosition) {
    		mTextPosition = textPosition;
    		self.setChanged(true);
    	}	
	}
	  
	/**
	 * @method getTextPosition
	 * @return
	 */
	self.getTextPosition = function() { 
		return mTextPosition; 
	}

	/** 
	 * @method setOrient
	 * @param orient
	 */
	self.setOrient = function (orient) {
    	if (typeof orient == "string") orient = EJSS_DRAWING2D.Axis[orient.toUpperCase()];
    	if (mOrient != orient) {
    		mOrient = orient;
    		self.setChanged(true);	
    	}	
	}
	  
	/**
	 * @method getOrient
	 * @return
	 */
	self.getOrient = function() { 
		return mOrient; 
	}

	/** 
	 * @method setScale
	 * @param scale
	 */
	self.setScale = function (scale) {
		if(mScale != scale) {
	  		mScale = scale;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getScale
	 * @return
	 */
	self.getScale = function() { 
		return mScale; 
	}

	/** 
	 * @method setScalePrecision
	 * @param scalePrecision
	 */
	self.setScalePrecision = function (scalePrecision) {
		if(mScalePrecision != scalePrecision) {
	  		mScalePrecision = scalePrecision;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getScalePrecision
	 * @return
	 */
	self.getScalePrecision = function() { 
		return mScalePrecision; 
	}

	/** 
	 * @method setTicksSize
	 * @param ticksSize
	 */
	self.setTicksSize = function (ticksSize) {
		if(mTicksSize != ticksSize) {
	  		mTicksSize = ticksSize;
	  		self.setChanged(true);
	  	}
	}
	  
	/**
	 * @method getTicksSize
	 * @return
	 */
	self.getTicksSize = function() { 
		return mTicksSize; 
	}
	
	/**
	 * Set inverted scale in Y
	 * @method setInvertedScaleY
	 */
	self.setInvertedScaleY = function(invertedscale) {
		if(mInvertedScaleY != invertedscale) {
			mInvertedScaleY = invertedscale;
	  		self.setChanged(true);
		}
	};

	/**
	 * Get inverted scale in Y
	 * @method getInvertedScaleY
	 * @return boolean
	 */
	self.getInvertedScaleY = function() {
		return mInvertedScaleY;
	};
	
	self.registerProperties = function(controller) {
		EJSS_DRAWING2D.Axis.registerProperties(self, controller);
	};
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	mFont.setChangeListener(function (change) { self.setChanged(true); });

	return self;
};

