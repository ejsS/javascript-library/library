/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/***
 * A Segment is a 2D drawing element that displays a single line 
 * @class EJSS_DRAWING2D.Segment
 * @parent EJSS_DRAWING2D.Element
 * @constructor  
 */
EJSS_DRAWING2D.Segment = {

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

      controller.registerProperty("Offset", element.setRelativePosition, element.getRelativePosition);
    },  


};

/**
 * Creates a 2D Segment
 * @method segment
 */
EJSS_DRAWING2D.segment = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  self.getClass = function() {
  	return "ElementSegment";
  };
  
    self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Segment.registerProperties(self,controller);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([0.1,0.1]);
  self.setRelativePosition("SOUTH_WEST");

  return self;
};



