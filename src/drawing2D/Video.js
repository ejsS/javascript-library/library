/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Video
 * @class Video 
 * @constructor  
 */
EJSS_DRAWING2D.Video = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

      controller.registerProperty("VideoUrl",element.setVideoUrl);
      controller.registerProperty("CurrentTime",element.setCurrentTime);
      controller.registerProperty("ShowControls",element.setControls);
      controller.registerProperty("AutoPlay",element.setAutoPlay);
      controller.registerProperty("Type",element.setType);
      controller.registerProperty("Poster",element.setPoster);
      controller.registerProperty("Loop",element.setLoop);
      controller.registerProperty("WebCam",element.setWebCam);      
    },
};

/**
 * Creates a 2D Segment
 * @method video
 */
EJSS_DRAWING2D.video = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  var mLoop = false;
  var mWebCam = false;
  var mType = "";
  var mPoster = "";
  var mAutoPlay = true;
  var mCurrentTime = 0;
  var mControls = true;
  var mPlay = true;
  var mUrl;		// Video url

  self.getClass = function() {
  	return "ElementVideo";
  }

  self.setLoop = function(loop) {
  	if(mLoop != loop) {
  		mLoop = loop;
  		self.setChanged(true);
  	}
  }	

  self.getLoop = function() {
  	return mLoop;
  }	

  self.setPoster = function(poster) {
  	if(mPoster != poster) {
  		mPoster = poster;
  		self.setChanged(true);
  	}
  }	

  self.getPoster = function() {
  	return mPoster;
  }	

  self.setType = function(type) {
  	if(mType != type) {
  		mType = type;
  		self.setChanged(true);
  	}
  }	

  self.getType = function() {
  	return mType;
  }	

  self.setAutoPlay = function(autoPlay) {
  	if(mAutoPlay != autoPlay) {
  		mAutoPlay = autoPlay;
  		mPlay = autoPlay;
  		self.setChanged(true);
  	}
  }	

  self.getAutoPlay = function() {
  	return mAutoPlay;
  }	

  self.setWebCam = function(webCam) {
  	if(mWebCam != webCam) {
  		mWebCam = webCam;
  		self.setChanged(true);
  	}
  }	

  self.getWebCam = function() {
  	return mWebCam;
  }	

// play,stop,controls

  self.setVideoUrl = function(url) {
  	if(mUrl != url) {
  		mUrl = url;
  		self.setChanged(true);
  	}
  }	

  self.getVideoUrl = function() {
  	return mUrl;
  }	

  self.setCurrentTime = function(currentTime) {
  	if(mCurrentTime != currentTime) {
  		mCurrentTime = currentTime;
  		self.setChanged(true);
  	}
  }	

  self.getCurrentTime = function() {
  	return mCurrentTime;
  }	

  self.setControls = function(controls) {
  	if(mControls != controls) {
  		mControls = controls;
  		self.setChanged(true);
  	}
  }	

  self.getControls = function() {
  	return mControls;
  }	

  self.play = function() {
  	if(!mPlay) {
	  	mPlay = true;
	  	self.setChanged(true);
	}
  }	

  self.stop = function() {
  	if(mPlay) {
	  	mPlay = false;
	  	mAutoPlay = false;
	  	self.setChanged(true);
	}
  }	
  
  self.isPlay = function() {
  	return mPlay;
  }

  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Video.registerProperties(self,controller);
  };
  
  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  return self;
};



