/*
 * Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
 * This code is part of the Easy Javascript Simulations authoring and simulation tool
 * 
 * This code is Open Source and is provided "as is".
 */

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Shape
 * @class Shape 
 * @constructor  
 */
EJSS_DRAWING2D.Pipe = {

    // ----------------------------------------------------
    // Static methods
    // ----------------------------------------------------

    /**
     * static registerProperties method
     */
    registerProperties : function(element,controller) {
      EJSS_DRAWING2D.Element.registerProperties(element,controller); // super class

      controller.registerProperty("Points",element.setPoints,element.getPoints);
      controller.registerProperty("PipeWidth",element.setPipeWidth,element.getPipeWidth);
    },

    /**
     * static copyTo method, to be used by sets
     */
    copyTo : function(source, dest) {
      EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy

      dest.setPoints(source.getPoints());
      dest.setPipeWidth(source.getPipeWidth());
    }

};

/**
 * Creates a 2D shape
 * @method shape
 */
EJSS_DRAWING2D.pipe = function (name) {
  var self = EJSS_DRAWING2D.element(name);

  var mPoints = [[0.1,0.2],[0.1,0.3]];
  var mPipeWidth = 5;

  self.getClass = function() {
  	return "ElementPipe";
  };

  // ----------------------------------------------------
  // public functions
  // ----------------------------------------------------

  self.setPoints = function(points) {
      mPoints = points;
      self.setChanged(true);
  };

  self.getPoints = function() {
  	return mPoints;
  };

  self.setPipeWidth = function(width) { 
    mPipeWidth = width; 
    self.setChanged(true);
  };
    
  self.getPipeWidth = function() { 
    return mPipeWidth; 
  };
  
  // ----------------------------------------------------
  // Properties and copies
  // ----------------------------------------------------

  /**
   * Extended registerProperties method. To be used by promoteToControlElement
   * @method registerProperties
   */
  self.registerProperties = function(controller) {
    EJSS_DRAWING2D.Pipe.registerProperties(self,controller);
  };

  /**
   * Copies itself to another element
   * Extended copyTo method. To be used by Sets
   * @method copyTo
   */
  self.copyTo = function(element) {
    EJSS_DRAWING2D.Pipe.copyTo(self,element);
  };

  // ----------------------------------------------------
  // Final start-up
  // ----------------------------------------------------

  self.setSize([1,1]);
  self.setRelativePosition("SOUTH_WEST");
  self.getStyle().setLineWidth(1);
  self.getStyle().setFillColor("Blue");
  return self;
};


