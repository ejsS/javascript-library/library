/*
* Copyright (C) 2014 Francisco Esquembre and Felix J. Garcia
* This code is part of the Easy Javascript Simulations authoring and simulation tool
*
* This code is Open Source and is provided "as is".
*/

var EJSS_DRAWING2D = EJSS_DRAWING2D || {};

/**
 * Polygon
 * @class Polygon 
 * @constructor  
 */
EJSS_DRAWING2D.Polygon = {
  	NO_CONNECTION : 0, 	// The next point will not be connected to the previous one
 	LINE_CONNECTION : 1,	// The next point will be connected to the previous one by a segment

	// ----------------------------------------------------
	// Static methods
	// ----------------------------------------------------

  	/**
   	* Copies one element into another
   	*/
  	copyTo : function(source, dest) {
      	EJSS_DRAWING2D.Element.copyTo(source,dest); // super class copy
  	
		dest.setPoints(source.getPoints());
  	},


	/**
	 * static registerProperties method
	 */
	registerProperties : function(element, controller) {
		EJSS_DRAWING2D.Element.registerProperties(element, controller);
		// super class

		controller.registerProperty("Points", element.setPoints, element.getPoints);
		controller.registerProperty("PointsX", element.setPointsX);
		controller.registerProperty("PointsY", element.setPointsY);
		controller.registerProperty("LastPoint", element.addPoint, element.getLastPoint);		
	}
			
};

/**
 * Creates a 2D Polygon
 * @method polygon
 */
EJSS_DRAWING2D.polygon = function(name) {
	var self = EJSS_DRAWING2D.element(name);

  	var mPointList = []; 	// The current list of points  	
	var mTempX = null;
	var mTempY = null; 
	 
	self.getClass = function() {
		return "ElementPolygon";
	}
		  
  /**
   * Adds a new point to the Polygon.
   * @method addPoint
   * @param x double The X coordinate of the point 
   * 		or point double[] The double[2] array with the coordinates of the point.
   * @param y double The Y coordinate of the point.
   * @param style connected or not.
   */
  self.addPoint = function (x, y, style) {
    if(x instanceof Array)    	
    	addPoint(x[0], x[1], x[2]);
    else 
    	addPoint (x, y, style);
    self.setChanged(true);
  }

  /**
   * Adds an array of points to the Polygon.
   * @method addPoints
   * @param xInput double The double[] array with the X coordinates of the points.
   * 	or  point double[][] The double[nPoints][2] array with the coordinates of the points.
   * @param yInput double The double[] array with the Y coordinates of the points.
   */
  self.addPoints  = function(x, y) {
    if (x==undefined || x==null || x[0]==undefined) return;
  	if(x[0] instanceof Array) {    
    	for (var i=0,n=x.length; i<n; i++) addPoint (x[i][0],x[i][1],x[i][2]);  		
  	}
    else {
    	var n = Math.min(x.length,y.length);
    	for (var i=0; i<n; i++) addPoint (x[i],y[i]);
    }
    self.setChanged(true);
  }

  /**
   * Sets an array of points to the Polygon.
   * @method setPoints
   * @param x double The double[] array with the X coordinates of the points.
   * 	or  point double[][] The double[nPoints][2] array with the coordinates of the points.
   * @param y double The double[] array with the Y coordinates of the points.
   */
  self.setPoints  = function(x, y) {
  	self.clear();
  	self.addPoints(x,y);
  	self.setChanged(true);
  }


  /**
   * Adds the X coordinates of an array of points to the Polygon.
   * @method addXPoints
   * @param x double The double[] array with the X coordinates of the points.
   */
  self.setPointsX  = function(x) {
    if (mTempY==null) mTempX = x;
    else {
      self.setPoints(x,mTempY);
      mTempY = null;
    }
  }

  /**
   * Adds the Y coordinates of an array of points to the Polygon.
   * @method addYPoints
   * @param y double The double[] array with the Y coordinates of the points.
   */
  self.setPointsY  = function(y) {
    if (mTempX==null) mTempY = y;
    else {
      self.setPoints(mTempX,y);
      mTempX = null;
    }
  }

  /**
   * Returns all points.
   * @method getPoints
   * @return points
   */
  self.getPoints = function () {
    return mPointList; 
  }

  /**
   * Gets last point.
   * @method getLastPoint
   * @return point
   */
  self.getLastPoint = function () {
  	if(mPointList.length > 0) 
    	return mPointList[mPointList.length-1];
    else return [];
  }

  /**
   * Clears all points from all segments of the Polygon.
   * @method clear
   */
  self.clear = function() {
    mPointList = [];
  	self.setChanged(true);
  }
  
  /**
   * Returns bounds for an element
   * @override
   * @method getBounds
   * @return Object{left,rigth,top,bottom}
   */
  self.getBounds = function(element) {
  	var xmin, xmax, ymin, ymax;
  	var points = self.getPoints();
    var len = points.length;
    if(len == 0) {
    	xmin = xmax = ymin = ymax = 0;
    }             
    else {
		xmax = xmin = points[0][0];
		ymax = ymin = points[0][1];    	
		for(var j=1; j<len; j++) {
			var x = points[j][0], y = points[j][1];
			if(x>xmax) xmax=x; if(y>ymax) ymax=y; if(x<xmin) xmin=x; if(y<ymin) ymin=y;
		}
	}    
    var x = self.getX(), y = self.getY();
    var sx = self.getSizeX(), sy = self.getSizeY();
  	var mx = sy/2, my = sy/2;  	
  	var d = self.getRelativePositionOffset(sx,sy);
	return {
		left: ((x+d[0])-mx)+xmin*sx,
		right: ((x+d[0])-mx)+xmax*sx,
		top: ((y+d[1])-my)+ymax*sy, 
		bottom: ((y+d[1])-my)+ymin*sy
	}
  };  
  
  function addPoint(_x, _y, _style) {
    if (isNaN(_x) || isNaN(_y)) { 
    	return; 
    }
    
    mPointList[mPointList.length] = [_x, _y, _style];
  }


  self.registerProperties = function(controller) {
	EJSS_DRAWING2D.Polygon.registerProperties(self, controller);
  };
  
  self.copyTo = function(element) {
    EJSS_DRAWING2D.Polygon.copyTo(self,element);
  };
  
	// ----------------------------------------------------
	// Final start-up
	// ----------------------------------------------------

	self.setSize([1,1]);
	self.setRelativePosition("SOUTH_WEST");
	
	return self;
};

