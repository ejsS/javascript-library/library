# Require node and uglify-js, optionally vows and jshint
NODE_EXEC = node
JS_COMPILER = terser
JS_TESTER = vows
JS_LINT = jshint
LOCALE ?= en_US

include files.mak

all: \
	import \
	ejsS.v1.max.js \
	ejsS.v1.min.js \
	ejsS.v1.max.doc.js

test: all
	@$(JS_TESTER)

lint: 
	-@$(JS_LINT) $(filter %.js,$(src))

ejsS.v1.min.js: import 
	@rm -f ../release/$@
	$(JS_COMPILER) $(filter %.js,$(src)) -o ../release/$@ -c -m

ejsS.v1.max.js: import 
	cat $(filter %.js,$(src)) > ../release/$@

ejsS.v1.max.doc.js: 
	$(NODE_EXEC) gendoc.js > ../documentation/$@
	$(NODE_EXEC) genhtmldoc.js > ../documentation/ejsS.v1.max.doc.html
	
import:
	@echo "src = \\" > files.mak
	@find ./src/ -type f -name "*.js" -not -path "*/tools.js" -print0 | sort -z | xargs -0 printf '%s ' >> files.mak
	@echo "./src/tools.js" >> files.mak
	@echo "var script = [" > test/import.js
	@find ./src/ -type f -name "*.js" -print | xargs -I {} echo "\"../{}\"," >> test/import.js
	@echo "];" >> test/import.js
	@echo "for(var i=0; i<script.length; i++) {" >> test/import.js
	@echo "	document.write('<script type=\"text/javascript\" src=\"' + script[i] + '\"></script>');" >> test/import.js
	@echo "}" >> test/import.js

clean:
	rm -f ../release/ejsS.v1.max.js ../release/ejsS.v1.min.js 
	rm -f ../documentation/ejsS.v1.max.doc.html ../documentation/ejsS.v1.max.doc.js

