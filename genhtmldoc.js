fs = require('fs')

function createCell(txt) {
	var ret = "";
	if (txt && txt.length>0) 
		ret = "<td>" + txt + "</td>";
	else
		ret = "<td>-</td>";
		
	return ret;
}

function createClassRow(des) {	
	var strclass = "<tr>";
	strclass += createCell(des.class);
	strclass += createCell(des.description);
	strclass += "</tr>\n";	
	return strclass;
}

function createParentRow(parent) {
	var strproperty = "<tr>";
	strproperty += createCell("<a href='#" + parent + "'>" + parent + "</a>");
	strproperty += "</tr>\n";
	return strproperty;	
}

function createPropertyRow(doc,cls,prop) {
	var description,type,def,values;
	var clsprop = doc[cls].properties[prop];
	if(clsprop) {
		if(clsprop.description) description = clsprop.description;
		if(clsprop.type) type = clsprop.type;
		if(clsprop.default) def = clsprop.default;
		if(clsprop.values) values = clsprop.values;			
	}
	
	var strproperty = "<tr>";
	strproperty += createCell(prop);
	strproperty += createCell(description);
	strproperty += createCell(type);
	strproperty += createCell(def);
	strproperty += createCell(values);
	strproperty += "</tr>\n";
	return strproperty;
}

function createActionRow(doc,cls,action) {
	var description;
	var clsact = doc[cls].actions[action];
	if(clsact) {
		if(clsact.description) description = clsact.description;
	}
		
	var straction = "<tr>";
	straction += createCell(action);
	straction += createCell(description);
	straction += "</tr>\n";
	return straction;
}

function createMethodRow(doc,cls,mth) {
	var description,ret,params,visibility;
	var clsmth = doc[cls].methods[mth];
	if(clsmth) {
		if(clsmth.description) description = clsmth.description;
		var i = 1;
		if(clsmth["param"+i]) {
			params = "";				
			do { params += clsmth["param"+(i++)] + "; "; } while (clsmth["param"+i]); 				
		}			
		if(clsmth.return) ret = clsmth.return;
		if(clsmth.visibility) visibility = clsmth.visibility;
	}
	
	var strmethod = "<tr>";
	strmethod += createCell(mth);
	strmethod += createCell(description);
	strmethod += createCell(params);
	strmethod += createCell(ret);
	strmethod += createCell(visibility);
	strmethod += "</tr>\n";
	return strmethod;
}

function toArray(json) {
	var ret = [];
	var i = 0;
	for(var key in json) ret[i++] = key;
	return ret.sort();
}

function createTable(doc, classname) {
	var strclass = ""; 
	var strparents = "";
	var strproperty = "";
	var straction = "";
	var strmethod = "";

	// get all elements
	var properties = toArray(doc[classname].properties);
	var methods = toArray(doc[classname].methods);
	var actions = toArray(doc[classname].actions);
	var parent = doc[classname].parent;

	// get class
	strclass = "<table><thead><th>Class</th><th>Description</th></thead><tbody>\n";
	strclass += createClassRow(doc[classname]);
	strclass += "</tbody></table>\n";

	// get parent
	if(typeof parent != 'undefined') {
		strparents = "<table><thead><th>Parent</th></thead><tbody>\n";
		strparents += createParentRow(parent); 
		strparents += "</tbody></table>\n";
	}
		
	// get properties	
	if(properties.length > 0) {
		strproperty = "<table><thead><th>Property</th><th>Description</th>" +
					"<th>Type</th><th>Default</th><th>Values</th></thead><tbody>\n";
		for (var prop in properties) {
			strproperty += createPropertyRow(doc,classname,properties[prop]);
		}
		strproperty += "</tbody></table>\n";	
	}	

	// get actions
	if(actions.length > 0) {
		straction = "<table><thead><th>Action</th><th>Description</th></thead><tbody>\n";
		for (var act in actions) {
			straction += createActionRow(doc,classname,actions[act]);
		}	
		straction += "</tbody></table>\n";
	}
	
	// get methods
	if(methods.length > 0) {
		strmethod = "<table><thead><th>Method</th><th>Description</th><th>Params</th>" +
						"<th>Return</th><th>Visibility</th></thead><tbody>\n";		
		for (var met in methods) {
			strmethod += createMethodRow(doc,classname,methods[met]);
		}	
		strmethod += "</tbody></table>\n";
	}
	
  	return strclass + "<br>" + strparents + "<br>" + strproperty + "<br>" + straction + "<br>" + strmethod;	
} 
      
// read file with library
fs.readFile('../documentation/ejsS.v1.max.doc.js', 'utf8', function (err,data) {
	if (err) {
    	return console.log(err);
  	}
	
	var doc = JSON.parse(data);

  	console.log("<html><head><link rel='stylesheet' href='ejsS.doc.css'></head><body>");

	// list of classes
	console.log("<h2>Classes</h2><ul>");
	for(var cls in doc) {		
		console.log("<li><a href='#" + cls + "'>" + cls + "</a></li>");
	}
	console.log("</ul>")
	
	// classes
	for(var cls in doc) {
		console.log("<h3><a name='" + cls + "'>" + cls + "</a></h3>");	
		var strtable = createTable(doc, cls);
		console.log(strtable);
	}

  	console.log("</body></html>");
});
