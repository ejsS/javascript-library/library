fs = require('fs')

var REGEX_START_COMMENT = /^\s*\/\*\*\*/,
    REGEX_END_COMMENT = /\*\/\s*$/,
    REGEX_LINES = /\r\n|\n/,
    REGEX_LINE_HEAD_CHAR = /^\s*\*/;
      
// create a json {class: ..., param: ..., descritipon: ..., othertag: ...} with comment data      
function handlecomment(comment) {
    var lines = comment.split(REGEX_LINES),
        len = lines.length, i,
        parts, part, peek, skip;

	var results = {};	
	var paramcount = 0; // count of params
	var lastclass; 	// save last class tag

	// trim leading line head char(star or harp) if there are any
    for (i = 0; i < len; i++) {
        lines[i] = lines[i].replace(REGEX_END_COMMENT, '')
        	.replace(REGEX_START_COMMENT, '')
        	.replace(REGEX_LINE_HEAD_CHAR, '');
    }

	// reconstitute and tokenize the comment block
    comment = lines.join('\n');
    // parts = comment.split(/(?:^|\n)\s*(@\w*)/);
    parts = comment.split(/\s*(@\w*)/);
    len = parts.length;
    for (i = 0; i < len; i++) {
        value = '';        
        part = parts[i];
        if (part === '') {
            continue;
        }
        skip = false;

		// the first token may be the description, otherwise it should be a tag
        if (i === 0 && part.substr(0, 1) !== '@') {
            if (part) {
                tag = '@description';
                value = part;
            } else {
                skip = true;
            }
        } else {
            tag = part;
            // lookahead for the tag value
            peek = parts[i + 1];
            if (peek) {
                value = peek;
                i++;
            }
        }
		
        if (!skip && tag) {
        	tag = tag.substr(1).toLowerCase();
        	value = value.replace(REGEX_LINES, '').trim();
            
            // param
            if (tag == "param") {
            	paramcount++;
            	tag = "param" + paramcount;
            }
            
            // save tag
            results[tag] = value;
            
            // save class tag
            if (tag == "class") {
            	lastclass = value;            	
            }
        }
    }

    return results;
};

// read file with library
fs.readFile('../release/ejsS.v1.max.js', 'utf8', function (err,data) {
	if (err) {
    	return console.log(err);
  	}
	
	var commentmap = {};	
	var classname = "undefined";
		  
    var commentlines, comment,
        lines = data.split(REGEX_LINES),
        len = lines.length, i, linenum;

	// process line by line
    for (i = 0; i < len; i++) {
        line = lines[i];
        
        // look for comments starting ***
        // any element (class, function, ...) needs to use this starting to create its documentation
        if(REGEX_START_COMMENT.test(line)) {
            commentlines = [];

            linenum = i + 1;

			// look for end comment
            while (i < len && (!REGEX_END_COMMENT.test(line))) {
                commentlines.push(line);
                i++;
                line = lines[i];
            }
            
            if(REGEX_END_COMMENT.test(line))
            	commentlines.push(line);

            comment = commentlines.join('\n');
                        
            // handle comment
            var element = handlecomment(comment); // get element with comments attrs
            if (element.class) {
            	// all classes have prop, methods and actions
            	classname = element.class;
            	commentmap[classname] = element;
            	commentmap[classname]["properties"] = {};
            	commentmap[classname]["methods"] = {};
            	commentmap[classname]["actions"] = {};
            }            
            if (element.property) {
            	// new property added to last class
              	//console.log ("classname = "+classname);
            	commentmap[classname]["properties"][element.property] = element;
             }
            if (element.method)
               	// new method added to last class
            	commentmap[classname]["methods"][element.method] = element;
            if (element.action)
               	// new action added to last class
            	commentmap[classname]["actions"][element.action] = element;
        }
    }
  
  	
  	console.log(JSON.stringify(commentmap));
});
