# Easy JavaScript Simulations Library (EjsS Library)

## Introduction

For the past years, the [Easy Java Simulation (Ejs) Modeling Tool](http://www.um.es/fem/EjsWiki/) has enabled students and teachers to create Java simulations by providing a modeling and authoring platform that lowers the barriers involved in programming. Moreover, the Ejs tool allows users to package these simulations as Java applets that could be published and run by a web browser. However, the tablets and smartphones do not support Java applets and so they require other types of technologies to show simulations. In this sense, our development effort has expanded to incorporate the creation of JavaScript simulations to our modeling tool. These new simulations share a JavaScript library, called Easy JavaScript Simulations (EjsS) library, that allows the simulations to run on mobile devices.

## MVC Design

The EjsS design is based on model-view-controller (MVC) that is a well-known software architectural pattern for implementing user interfaces. MVC divides a simulation into three interconnected parts, so as to separate internal representations of information from the ways that information is presented to or managed from the user. The central component, the model, consists of simulation data, physics rules, logic and functions. A view can be any output representation of information, such as a 2D diagram or a 3D scenario. The third part, the controller, accepts input and converts it to commands for the model or view. 

EjsS library is based on JavaScript/HTML5 and uses graphics technologies such as WebGL and SVG. Note it does not require other graphical libraries (e.g. Three.js) or UI library (e.g. jQuery). EjsS uses its own self-contained independent library that is optimized to show graphical physics simulations.

## Getting Started

We highly recommend that you familiarize yourself with JavaScript/HTML5 in order to develop your own simulations or examples. 

## Authors

* **Félix J. García-Clemente** - [CV] (https://webs.um.es/fgarcia/)
* **Francisco Esquembre** - [CV] (https://fem.um.es/)


